
![SolariumMain0.8.jpg](https://bitbucket.org/repo/xzG4Mz/images/2044987403-SolariumMain0.8.jpg)

### Solarium ###
An editor for the John Bowen Solaris synthesizer.

          Go to the Downloads section to download an executable for Windows

The Solaris has quite a lot going on under the hood. There are a lot of modules that can be connected in almost any way. Even though the Solaris front panel is a great interface for creating presets it's not always easy to get a quick picture of how everything's connected. Solarium is an editor with focus on showing you how things are connected. But it also aims to complement the Solaris in other ways as well.

I'd be interested in thoughts or suggestions on what you might like to see in Solarium:

* I think I want to move the parameters from the right-side panel to actual modules in an upper window pane, similar to the way Reaktor 6 blocks looks. Eventually...when I can find the time that is.

* I'd like to create a sub editor for the sequencer that makes it easier to create sequences. What should it look like? What features?

* One idea I had is preset subsets. That is, subsets of presets that can be imported into the preset you're working on. For example, every time you want to create an analog-style preset you might want to set up your custom "slop" for modulating the oscillators. Maybe it's an LFO through a lag processor and a few other things. What if you could have this sub section already made up and just import it into any preset, even presets you're in the middle of working on? Perhaps you can import pre-made key tables, sequences or arpeggios. Would you consider this a useful feature?

* Anything else?

Jim
(a.k.a. minorguy on the Solaris forum)

--------------------------------------------------------------
### Building the Source Code ###

Solarium is written in C++. Building the source code requires only the JUCE library. I've built the current executable using Visual Studio 2019 (ver 16.1.1) and JUCE v5.4.3.

The latest executable of Solarium is for Windows only (see the Downloads section) since I don't have a Mac system. But the code should be cross-platform. So if you're able to build a version for Mac I'd be glad to add it to the downloads section, and people on the Solaris forums may appreciate it.

--------------------------------------------------------------

### New in Version 0.13 ###

Updated to work with (only) Solaris OS 1.4.4.
The only changes made were those needed to update Solarium to work with Solaris OS version 1.4.4. This includes adding parameters for Patchain and SmpPool as well as additional category filter choices. Earlier versions of Solaris are not supported.
Also, updated for JUCE v5.4.3.

--------------------------------------------------------------

### New in Version 0.12 ###

Updated to work with (only) Solaris OS 1.4.1.
The only changes made were those needed to update Solarium to work with Solaris OS version 1.4.1. Earlier versions of Solaris are not supported.

--------------------------------------------------------------

### New in Version 0.11 ###

Updated to work with Solaris OS 1.4.0. In Solaris version 1.4.0, all parameters are supported over MIDI by way of the new system exclusive implementation. With this version of Solarium all I did was the minimum to get it working with Solaris 1.4.0.

Solarium 0.11 is updated to work with the 1.4.0 system exclusive and will not work with any earlier Solaris OS. Solaris 1.4.0 is required.

All parameters are now editable and full preset transfers work much faster (about 1 second over MIDI).

System and MIDI parameters can now be transferred.

FXBypass and Compare can be done remotely. (Though compare differences are not reflected in the Solarium UI)

Note: There is a known bug in the undo history after the history gets moderately long. Hopefully it will be fixed in a later version.

--------------------------------------------------------------

### New in Version 0.10 ###

This is a very minor update that fixes the following:

Sequencer steps for tracks B, C, and D and also BPM now sync with the Solaris. NOTE: this requires Solaris OS version 1.3.1 to work!

Fixed Looping EG so that time divisions are used when sync is enabled.

Now loads ribbon parameters from the preset file.

--------------------------------------------------------------

### New in Version 0.9 ###

Branching Undo.

Bookmarks.

Destructive undo.

Bug fixes.

--------------------------------------------------------------

### New in Version 0.8 ###

Multiple Undo!
(Remember, of course, that this is limited only to those parameters currently supported by Solaris MIDI.)

Elements can now be muted. This effectively just mutes all its output connections.

Several bug fixes.

--------------------------------------------------------------

### New in Version 0.7 ###

Version 0.7 now includes an arpeggiator editor. (Hit CTRL+e) All preset parameters should now be represented.

Oscillator sync is now represented graphically by a connection between oscillator elements.

--------------------------------------------------------------

### New in Version 0.6 ###

Version 0.6 adds two new features: Wire muting and wire transparency. I'd be interesting in getting feedback about whether you think these are worthwhile features to have or not.

Wire Muting

Wire muting lets you mute a connecting wire so that it behaves as if the wire is disconnected. But you can easily unmute it again. This is probably better than having to disconnect and reconnect wires just to see how something would sound if you did. 
I was also thinking of allowing elements to be muted. So that muting an element would actually mute/unmute _all_ the connecting wires coming from its output. This is quicker than doing them one at a time. Does this seem like a worthwhile feature to add?

Wire Transparency (or Opacity if you like your glass half full)

Presets on the Solaris can get pretty complex with wires looking like spaghetti (check out Bank 6!). If too many wires are crossing over an element it can become hard to select that element. I was considering how to handle this. I noticed that Reaktor 6 runs wires behind the components with a little curve at the ends. I didn't think this was the best solution for Solarium where you can have so many wires you lose track of which is which when they go behind. 

Instead I use wire transparency. Using the slider on the lower left you can adjust the transparency of the wires so it's easier to see the elements behind. What's more, once the transparency fall below a certain level the wires are no longer selectable, making it easier to select the elements behind. This level is user configurable in the preference dialog. I was also thinking of allowing the user to switch between two preset transparency levels using a keypress. But this might be feature overkill. What do you think?