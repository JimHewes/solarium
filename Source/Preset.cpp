/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <algorithm>
#include <string>
#include <memory>
#include <ostream>
#include <unordered_map>
#include <cassert>
#include "gsl_util.h"
#include "Preset.h"
#include "ParameterList.h"
#include "Command.h"
#include "HistoryListClient.h"

#if 0
#include "../JuceLibraryCode/JuceHeader.h"	// for Debug messages only!
#else
#define DBG(dbgtext)
#endif


using std::string;
using std::vector;
using std::list;
using std::make_unique;
using gsl::narrow;
using ::ElementType;


constexpr bool IsSource(const ParamInfo& paramInfo)
{
	return ((paramInfo.portType == PortType::Input || paramInfo.portType == PortType::Mod ||
		paramInfo.portType == PortType::Control) && paramInfo.subIndex == psiSource);
}

constexpr bool IsSource(const ParamID& paramID)
{
	return ((paramID.portType == PortType::Input || paramID.portType == PortType::Mod ||
		paramID.portType == PortType::Control) && paramID.subIndex == psiSource);
}

/** @brief	Determines whether a given parameter can be muted. (Note this is not "mutatable". Mutable means we can mute it.)

	@param[in] paramID	The ID of the parameter to test.
	@returns	true if the parameter can be muted. false otherwise.
*/
inline bool IsMutable(const ParamID& paramID) noexcept
{
	// The only parameters that can be muted are Input, Mode, and Control port sources.
	bool isMutable = (paramID.portType == PortType::Input || paramID.portType == PortType::Mod || paramID.portType == PortType::Control) && paramID.subIndex == psiSource;

	// Some Inputs are not mutable because they don't have an "Off" setting.
	if (paramID.portType == PortType::Input && (paramID.elemType == ElementType::InsertFX || paramID.elemType == ElementType::VCA || paramID.elemType == ElementType::VCAMaster))
	{
		isMutable = false;
	}
	return isMutable;
}

bool operator == (const ParamValue &lhs, const ParamValue &rhs)
{
	return	lhs.paramID == rhs.paramID && lhs.value == rhs.value;
}

bool operator == (const Connector& lhs,const Connector& rhs)
{
	return	lhs.startPort == rhs.startPort && 
			lhs.endPort == rhs.endPort;
}
bool operator < (const Connector& lhs,const Connector& rhs)	// required in order to put Connector into std:set (SelectionModel)
{
	if (lhs.startPort == rhs.startPort)
	{
		return lhs.endPort < rhs.endPort;
	}
	else
	{
		return lhs.startPort < rhs.startPort;
	}
}

const ElementItem& GetElementItem(ElementType elemType)
{
	auto iter = std::find_if(masterParamList.cbegin(), masterParamList.cend(), [elemType](const ElementItem& elemItem) {return elemItem.elementType == elemType;});
	assert(iter != masterParamList.end());
	return *iter;
}

const ParamInfo& GetParameterInfo(const ParamID& paramID)
{
	const ElementItem& elemItem = GetElementItem(paramID.elemType);
	auto iter = std::find_if(elemItem.elementParamList.cbegin(), elemItem.elementParamList.cend(), [&paramID](const ParamInfo& paramInfo) 
	{
		return paramInfo.portType == paramID.portType && paramInfo.portIndex == paramID.portIndex && paramInfo.subIndex == paramID.subIndex;
	});
	assert(iter != elemItem.elementParamList.end());
	return *iter;
}

SourceListType GetPortSourceList(const PortID & portID)
{
	Expects(portID.portType != PortType::Output && portID.portType != PortType::None);

	const SourceListType* pSourceList{};

	// Handle exceptions that the masterParamList doesn't account for.
	if (portID.portType == PortType::Input)
	{
		if (portID.elemID.elemType == ElementType::InsertFX)
		{
			switch (portID.elemID.elemIndex)
			{
			case 0:	pSourceList = &insertFx0SourceList;	break;
			case 1:	pSourceList = &insertFx1SourceList;	break;
			case 2:	pSourceList = &insertFx2SourceList;	break;
			case 3:	pSourceList = &insertFx3SourceList;	break;
			default: assert(false);
			};
		}
		else if (portID.elemID.elemType == ElementType::VCA)
		{
			switch (portID.elemID.elemIndex)
			{
			case 0:	pSourceList = &vca0SourceList;	break;
			case 1:	pSourceList = &vca1SourceList;	break;
			case 2:	pSourceList = &vca2SourceList;	break;
			case 3:	pSourceList = &vca3SourceList;	break;
			default: assert(false);
			};
		}
		else if (portID.elemID.elemType == ElementType::VCAMaster)
		{
			switch (portID.portIndex)
			{
			case 0:	pSourceList = &vcaMasterSourceList0;	break;
			case 1:	pSourceList = &vcaMasterSourceList1;	break;
			case 2:	pSourceList = &vcaMasterSourceList2;	break;
			case 3:	pSourceList = &vcaMasterSourceList3;	break;
			default: assert(false);
			};
		}
	}

	if (pSourceList == nullptr)
	{
		const ParamInfo& paramInfo = GetParameterInfo({ portID, 0 });
		pSourceList = paramInfo.srcList;
	}
	return (*pSourceList);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

struct PresetValue
{
	PresetValue(int32_t value_, bool isMuted_) : value(value_), isMuted(isMuted_) {}
	PresetValue() = default;
	PresetValue(const PresetValue&) = default;
	int32_t	value{};	///< Currently values are no larger than 16 bits. But I anticipate they will eventually become 32 bits in order to hold time and frequency values.
	bool	isMuted{false};
};
inline bool operator==(const PresetValue& lhs, const PresetValue& rhs)
{
	return lhs.value == rhs.value && rhs.isMuted == lhs.isMuted;
}
inline bool operator!=(const PresetValue& lhs, const PresetValue& rhs)
{
	return !(lhs==rhs);
}

using PresetValues = std::unordered_map<ParamID, PresetValue>;
using PresetValuesUPtr = std::unique_ptr<PresetValues>;

using ParamHistory = HistoryListSet<ParamID, PresetValue>;

using VisitedElements = std::unordered_map<ElementID, uint8_t>;

struct PresetImpl
{
	PresetImpl(CommandHistory* pCommandHistory);
	~PresetImpl();

	void InitializePreset(bool notify, Version version);
	int32_t GetParameterValue(const ParamID& paramID) const;
	void SetParameterValueNotify(const ParamID& paramID, int32_t newValue, bool notify, Version version);
	bool GetMuted(const ParamID& paramID) const;
	void SetMutedNotify(const ParamID& paramID, bool isMuted, bool notify, Version version);
	void SetSourceID(const ParamID& paramID, SourceID sourceID, Version version);
	SourceID GetSourceID(const ParamID & paramID) const;
	void GetEnabledElements(std::vector<ElementID> &elementList, ElementID elemID, uint8_t part);
	void ClearVisited();
	void SetPreset(const std::vector<ParamValue>& paramValues, Version version);
	void SetState(PresetState stateType, bool enabled);
	bool GetState(PresetState stateType);
	void Write(std::ostream& output);
	void Read(std::istream& input);

	void InterpolateKeytableValues(uint8_t keytableIndex, uint8_t lowKey, uint8_t highKey);
	void UpdateKeyTable(uint8_t tableIndex, int keyIndex);

	PresetValues	values;
	ParamHistory	paramHistory;
	bool			compareState;
	bool			fxBypassState;

	VisitedElements	visitedElements;

	Notifier<void(Version)> EnabledChangedEvent;
	Notifier<void(const ParamID& paramID, int value, bool isMuted)> ParameterChangedEvent;
	Notifier<void(Version, bool)> MultipleParametersChangedEvent;
	Notifier<void(PresetState stateType, bool enabled)> StateChangedEvent;

	CommandHistory::BranchAddedConnection m_branchAddedConnection;
	CommandHistory::BranchDeletedConnection m_branchDeletedConnection;
	CommandHistory::RemoveVersionConnection m_removeVersionConnection;
	CommandHistory::GoToVersionConnection m_goToVersionConnection;

	bool isModified{ false };	///< not currently used
};

PresetImpl::PresetImpl(CommandHistory* pCommandHistory)
{
	m_branchAddedConnection = pCommandHistory->BranchAddedEvent.Connect([this](BranchTag newBranchTag, Version parentVersion)
	{
		paramHistory.AddBranch(newBranchTag, parentVersion);
	});

	m_branchDeletedConnection = pCommandHistory->BranchDeletedEvent.Connect([this](BranchTag branchTag, Revision maxRevision)
	{
		paramHistory.DeleteBranch(branchTag, maxRevision);
	});

	m_goToVersionConnection = pCommandHistory->GoToVersionEvent.Connect([this](Version version)
	{
		for (auto& value : values)
		{
			PresetValue currentValue = value.second;
			PresetValue newValue = paramHistory.GetItemAtVersion(version, value.first);
			if (currentValue != newValue)
			{
				value.second = newValue;
				ParameterChangedEvent.Notify(value.first, newValue.value, newValue.isMuted);
			}
		}
		MultipleParametersChangedEvent.Notify(version, false);
	});

	m_removeVersionConnection = pCommandHistory->RemoveVersionEvent.Connect([this](Version version)
	{
		paramHistory.RemoveVersion(version);
	});

}

PresetImpl::~PresetImpl()
{
	m_branchAddedConnection->Disconnect();
	m_goToVersionConnection->Disconnect();
}


/** @brief	Sets all parameters to an initialized preset.

	@param[in] notify	If true, a MulitpleParametersChanged event is sent. If false, it is not.

	@internal This could have been done by putting default values in the masterParamList for each parameter.  
	But almost all default values are zero. So it seems more practical to just set the few non-zero parameters all in one place.
*/
void PresetImpl::InitializePreset(bool notify, Version version)
{
	for (auto& elemItem : masterParamList)
	{
		bool zeroIt = elemItem.initList.empty();
		for (int elemIndex = 0; elemIndex < elemItem.numElements; ++elemIndex)
		{
			int elemDefaultIndex = static_cast<int>(elemItem.initList.size()) < elemItem.numElements ? 0 : elemIndex;

			for (size_t i = 0; i < elemItem.elementParamList.size(); ++i)
			{
				auto& paramInfo = elemItem.elementParamList[i];
				ParamID paramID{ elemItem.elementType, elemIndex, paramInfo.portType, paramInfo.portIndex, paramInfo.subIndex };
				SetParameterValueNotify(paramID, zeroIt ? 0 : elemItem.initList[elemDefaultIndex][i], false, version);
				SetMutedNotify(paramID, false, false, version);
			}
		}
	}

	if (notify)
	{
		MultipleParametersChangedEvent.Notify(version, true);
	}
}


int32_t PresetImpl::GetParameterValue(const ParamID& paramID) const
{
	int32_t result;
	
	// Handle "pseudo" parameters as a special case. These are not really parameters because they 
	// have only one setting. But it can make it easier for the GUI to treat them like parameters.
	if (paramID.elemType == ElementType::Filter && paramID.portType == PortType::None &&
		(paramID.subIndex == filterSSMMode || paramID.subIndex == filterMiniMode || paramID.subIndex == filterBypassMode))
	{
		result = 0;
	}
	//else if (paramID.elemType == ElementType::Oscillator && paramID.portType == PortType::Mod && paramID.subIndex == psiAmount)
	//{
	//	result = values.at(paramID).value;

	//	ParamID destParamID = paramID;
	//	destParamID.subIndex = psiDest;
	//	int destIndex = GetParameterValue(destParamID);
	//	if (destIndex == 3)	// shape
	//	{
	//		// If the destination is shape, the caller is expecting a value between -100 and +100 so we want 
	//		// to translate to that from our internal range of -1000 to +1000.
	//		result /= 10;
	//	}
	//}
	else
	{	// general case
		result = values.at(paramID).value;
	}

	return result;
}

void PresetImpl::SetParameterValueNotify(const ParamID& paramID, int32_t newValue, bool notify, Version version)
{
#if 0
	// NO LONGER NEEDED
	//
	// First we may need to make an adjustment if the parameter is the mod amount of the Oscillator.
	// In the case of the Mod amount parameter, the allowable range is -100 to +100 when the destination is Shape 
	// and -1000 to +1000 when the destination is LinFM (or None). Note this is the SAME parameter---there are 
	// not two separate parameters here. 
	// In Solarium we always maintain this parameter with the range -1000 to +1000, even for destination Shape.
	// So when the destination is currently set to Shape, we multiply by 10 before storing the value and 
	// divide by 10 when the value gets retrieved. (See GetParameterValue())
	if (paramID.elemType == ElementType::Oscillator && (paramID.portType == PortType::Mod) && (paramID.subIndex == psiAmount))
	{
		ParamID destParamID = paramID;
		destParamID.subIndex = psiDest;
		int destIndex = GetParameterValue(destParamID);
		if (destIndex == 3)	// shape
		{
			newValue *= 10;
		}
	}
#endif

	// Ignore changes to oscNoTrack when oscClockSync is enabled.
	// Ignore changes to oscLow when oscNoTrack or oscClockSync is enabled.
	if ((paramID.elemType == ElementType::Oscillator || paramID.elemType == ElementType::Rotor) && paramID.portType == PortType::None)
	{

		ParamID other = paramID;
		int clockSync = (paramID.elemType == ElementType::Oscillator) ? oscClockSync : rotClockSync;
		other.subIndex = static_cast<uint8_t>(clockSync);
		bool clockSyncOn = GetParameterValue(other) != 0;
		int noTrack = (paramID.elemType == ElementType::Oscillator) ? oscNoTrack : rotNoTrack;

		// Ignore changes to NoTrack when ClockSync is enabled.
		if (paramID.subIndex == noTrack && clockSyncOn)
		{
			return;
		}

		other.subIndex = static_cast<uint8_t>(noTrack);
		bool noTrackOn = GetParameterValue(other) != 0;
		int low = (paramID.elemType == ElementType::Oscillator) ? oscLow : rotLow;

		// Ignore changes to Low when NoTrack or ClockSync is enabled.
		if(paramID.subIndex == low && (clockSyncOn || noTrackOn))
		{
			return;
		}

		// turn off Low if ClockSync or NoTrack is being turned on
		if ((paramID.subIndex == clockSync || paramID.subIndex == noTrack) && (newValue != 0))
		{
			other.subIndex = static_cast<uint8_t>(low);
			SetParameterValueNotify(other, 0, false, version);
		}

		// turn off NoTrack if ClockSync is being turned on
		if (paramID.subIndex == clockSync && (newValue != 0))
		{
			other.subIndex = static_cast<uint8_t>(noTrack);
			SetParameterValueNotify(other, 0, false, version);
		}

	}


	PresetValue& val = values.at(paramID);

	// Add the new value to the history if the value changed OR if there is no history for this parameter yet.
	// Note that even though the history list will avoid storing values that haven't changed, we have to deal with the special case 
	// of first-time initialization where the existing value of val.value is 0  and we're setting it to zero, but there 
	// isn't any history yet. So we need to save history even though technically the value is not changing.
	if (val.value != newValue || paramHistory.IsEmpty(paramID))
	{
		paramHistory.AddItem(version, paramID, PresetValue(newValue, val.isMuted));
	}

	if (val.value != newValue)
	{
		// Now set the new value if it's different.
		val.value = newValue;

		// For the key table, update interpolated values. This should be done before sending out notification.
		if (paramID.elemType == ElementType::KeyTable)
		{
			uint8_t tableIndex = paramID.elemIndex;
			int currentKey = paramID.subIndex;
			UpdateKeyTable(tableIndex, currentKey);
		}

		// Notify parameter changed.
		if (notify)
		{
			// I don't want to have a separate notification for when the muted state changes. So we use the 
			// ParameterChanged notification for both values and muted state. This requires that we get the muted state too.
			bool isMuted = GetMuted(paramID);
			ParameterChangedEvent.Notify(paramID, newValue, isMuted);

			// I want to send the notification for the osc mod amount change _after_ the notification for any osc mod destination change.
			// Ideally I'd like to send them both at the same time so that the GUI remains in a valid state, but there's currently no provision for doing that (in this synchronous design).
			// if (paramID.elemType == ElementType::Oscillator && paramID.portType == PortType::Mod && paramID.subIndex == psiDest && newValue == 3)
			// {
			// 	ParameterChangedEvent.Notify(amountParamID, values.at(amountParamID).value / 10, isMuted);
			// }
		}

		// Now make other changes as a result of the parameter above changing.
#if 0
		// ClkSync, NoTrack, and Low button have special interractive behavior.
		if ((paramID.elemType == ElementType::Oscillator || paramID.elemType == ElementType::Rotor) && paramID.portType == PortType::None)
		{
			ParamID other = paramID;
			int clockSync = (paramID.elemType == ElementType::Oscillator) ? oscClockSync : rotClockSync;
			int noTrack = (paramID.elemType == ElementType::Oscillator) ? oscNoTrack : rotNoTrack;
			int low = (paramID.elemType == ElementType::Oscillator) ? oscLow : rotLow;
			if (paramID.subIndex == clockSync && newValue != 0)
			{
				other.subIndex = static_cast<uint8_t>(noTrack);
				SetParameterValueNotify(other, (newValue == 0 ? 0 : 1), notify, version);	// when osc sync changes, NoTrack changes to match it. NoTrack LED stays off though.
				other.subIndex = static_cast<uint8_t>(low);
				SetParameterValueNotify(other, 0, notify, version);
			}
			else if (paramID.subIndex == noTrack && newValue != 0)
			{
				//other.subIndex = oscClockSync;
				//SetParameterValueNotify(other, 0, notify, version);
				other.subIndex = static_cast<uint8_t>(low);
				SetParameterValueNotify(other, 0, notify, version);
			}
			//else if (paramID.subIndex == oscLow && newValue != 0)
			//{
			//	other.subIndex = oscClockSync;
			//	SetParameterValueNotify(other, 0, notify, version);
			//	other.subIndex = oscNoTrack;
			//	SetParameterValueNotify(other, 0, notify, version);
			//}
		}
#endif

		if ((paramID.elemType == ElementType::FXChannel) && 
			(paramID.portType == PortType::None) && 
			(newValue != 0))
		{
			// In the FXChannels, only one instance of each effect type is allowed. So turn off all others.
			ParamID otherParamID = ParamID(ElementType::FXChannel, 0, PortType::None, 0, 0);
			for (uint8_t fxChannel = 0; fxChannel < 4; ++fxChannel)
			{
				otherParamID.elemIndex = fxChannel;
				for (uint8_t slot = 0; slot < 4; ++slot)
				{
					otherParamID.subIndex = slot;
					if (paramID != otherParamID)
					{
						if (GetParameterValue(otherParamID) == newValue)
						{
							SetParameterValueNotify(otherParamID, 0, notify, version);
						}
					}
				}
			}
		}
		// If any source changed notify -OR-
		// If Osc type changed then sync connection might need to change, so notify.
		if ((paramID.portType != PortType::None && paramID.subIndex == psiSource) ||
			(paramID.elemType == ElementType::Oscillator && paramID.subIndex == oscType))
		{
			if (notify)
			{
				EnabledChangedEvent.Notify(version);
			}
		}
	}
}


/** @brief	Gets the muted state of the specified parameter.

	@param[in] paramID	The ID of the parameter to check.
	@returns			true if the parameter is muted, false if the parameter is not muted or is one that cannot be muted.
*/
bool PresetImpl::GetMuted(const ParamID& paramID) const
{
	// No need to check if the parameter is mutable. If it's not, it will never have been changed from it's default value of 'false'.
	return values.at(paramID).isMuted;
}

/** @brief	Mutes or unmutes the specified parameter. This has no effect on parameters that cannot be muted.

	@param[in] paramID	The ID of the parameter to mute.
	@param[in] isMuted	Setting this to true will mute the parameter. Setting it to false will unmute it.
	@param[in] notify	Setting this to true notify listeners that muting has changed. Setting it to false will not.
*/
void PresetImpl::SetMutedNotify(const ParamID& paramID, bool isMuted, bool notify, Version version)
{
	if (IsMutable(paramID))
	{
		PresetValue& val = values.at(paramID);
		if (val.isMuted != isMuted)
		{
			val.isMuted = isMuted;
			paramHistory.AddItem(version, paramID, val);

			if (notify)
			{
				ParameterChangedEvent.Notify(paramID, val.value, isMuted);
			}
		}
	}
}

SourceID PresetImpl::GetSourceID(const ParamID & paramID) const
{
	Expects(IsSource(paramID));
	SourceListType sourceList = GetPortSourceList(PortID({ paramID.elemType, paramID.elemIndex }, paramID.portType, paramID.portIndex));
	return sourceList[values.at(paramID).value];
}

/** @brief SetSourceElement does a similar job as SetParameterValue except that it directly sets a SourceID rather than a value.

	In other words, if the parameter is some sort of an input/mod/control, the SourceID is the ID of the output port that feeds into it.

	@param[in] paramID	A reference to the ParamID to set. This parameter MUST have a portType of Input, Mod or Control.
						(If it is None or Output the results are undefined.) Also, the paramIndex must be 0, which is 
						the source.
	@param[in] sourceID	A ID of the Element whose output port feeds into sourceID. This can be ElementType::Off meaning that there is no source.
						Note that a sourceID specifies WHICH output port because the LoopEG has two output ports.
*/
void PresetImpl::SetSourceID(const ParamID & paramID, SourceID sourceID, Version version)
{
	Expects(IsSource(paramID));
	
	const ParamInfo& paramInfo = GetParameterInfo(paramID);

	// Convert the sourceID into a value (an index).
	// Note that if the elementType of the sourceID is ElementType::Off, then the intention 
	// was to "remove the connector". But for VCA, the connector cannot be removed since the 
	// source parameter cannot be set to OFF, only to Filter or InsertFX. There is no Element::Off 
	// in the srcList. So a match will not be found in the loop and nothing is changed.
	for (int16_t val = 0; val < narrow<int16_t>((*paramInfo.srcList).size()); ++val)
	{
		if ((*paramInfo.srcList)[val] == sourceID)
		{
			SetParameterValueNotify(paramID, val, true, version);
			break;
		}
	}
}

/** @brief	A recursive function used to traverse the tree of Elements to find those that contribute to the Preset sound.
			This is private and is called only by GetElementTree() which begins the traversal.

	Each part is a branch stemming off of a VCA. If we visit an element more than once in the same branch, 
	then it means there is a cycle and we need to stop there. But the same element may participate in 
	a different branch which means we do want to visited it again. It may have been disabled in the previous 
	branch but enabled in the current branch. The part parameter specifies which branch we're currently in.
	So if the visitedPart of the element is the same as the part parameter, it means we've already visited 
	this element in the same part branch and we can stop.\n
	\n
	Output elements are not included in the list if they have no input.

	@param[in,out] elementList	A list that enabled elements are added to. This is not cleared; elements are only added.
	@param[in] elemID			The current element.
	@param[in] part				The part index (0 - 3, that is, the current VCAMaster input) that is currently being traversed.
								Ignored for VCAMaster itself and elements that come after the VCAMaster.
*/
void PresetImpl::GetEnabledElements(std::vector<ElementID> &elementList, ElementID elemID, uint8_t part)
{
	if (visitedElements[elemID] != part)
	{
		// Here we do not count output elements if they have no input. They are not added to the list of enabled elements.
		if (elemID.elemType != ElementType::Output || GetParameterValue(ParamID(elemID, PortType::Input, 0, psiSource)) > 0)
		{
			visitedElements[elemID] = part;
			elementList.push_back(elemID);

			if (elemID.elemType == ElementType::VCAMaster)
			{
				ParamID inputParamID(elemID, PortType::Input, 0, psiSource);

				// Loop through each part. If the part is enabled, then recursively call GetEnabledElements() on the element feeding into the VCAMaster input port.
				for (uint8_t i = 0; i < 4; ++i)
				{
					inputParamID.portIndex = i;
					bool enablePartIsOn = 0 != GetParameterValue(inputParamID);
					SourceID sourceElement = GetSourceID(inputParamID);

					if (sourceElement.IsConnected() && enablePartIsOn)
					{
						GetEnabledElements(elementList, sourceElement.elemID, i);
					}
				}

				// VCAMaster only has one modulation source and that's EG6 (index 5). So EG6 is active and enabled as long as the VCAMaster is.
				elementList.push_back(ElementID(ElementType::EG, 5));
			}
			else // for elements that are not VCAMaster
			{
				// If any oscillator is enabled then so is the VibratoLFO because that is hard wired to all oscillators.
				if (elemID.elemType == ElementType::Oscillator)
				{
					//elementList.push_back({ElementType::VibratoLFO, 0});
					GetEnabledElements(elementList, { ElementType::VibratoLFO, 0 }, part);
				}

				// Loop through the parameters of the current element.
				// For all parameters of the current element that are source parameters, follow the source element recursuvely if it is connected.
				auto paramList = GetElementItem(elemID.elemType).elementParamList;
				for (auto& paramInfo : paramList)
				{
					ParamID paramID(elemID, paramInfo.portType, paramInfo.portIndex, paramInfo.subIndex);

					// If the element type is an oscillator and the port type is an input and the oscillator type is NOT MM1, then treat the port as not connected.
					// It's the sync input and we don't want to consider that to be in a part because unless the osc type is MM1 it doesn't contribute to the sound.
					if (
						(elemID.elemType == ElementType::Oscillator && paramInfo.portType == PortType::Input && (1 == GetParameterValue(ParamID(elemID, oscType))))
						||
						IsSource(paramInfo))
					{
						SourceID sourceElement = GetSourceID(paramID);
						if (sourceElement.IsConnected())
						{
							GetEnabledElements(elementList, sourceElement.elemID, part);
						}
					}
				}
			}
		}
	}
}

void PresetImpl::ClearVisited()
{
	for (auto& elem : visitedElements)
	{
		elem.second = 255;
	}
}

void PresetImpl::SetPreset(const std::vector<ParamValue>& paramValues, Version version)
{
	for (auto& paramValue : paramValues)
	{
		// Set parameters, but don't notify for each one. We'll do one notification at the end below.
		SetParameterValueNotify(paramValue.paramID, paramValue.value, false, version);
	}
	MultipleParametersChangedEvent.Notify(version, true);
}


void PresetImpl::Write(std::ostream& output)
{
	// Write preset parameters history
	auto paramIDWriter = [](std::ostream& output, ParamID paramID)
	{
		output.write(reinterpret_cast<const char*>(&paramID.elemType), sizeof(paramID.elemType));
		output.write(reinterpret_cast<const char*>(&paramID.elemIndex), sizeof(paramID.elemIndex));
		output.write(reinterpret_cast<const char*>(&paramID.portType), sizeof(paramID.portType));
		output.write(reinterpret_cast<const char*>(&paramID.portIndex), sizeof(paramID.portIndex));
		output.write(reinterpret_cast<const char*>(&paramID.subIndex), sizeof(paramID.subIndex));
	};
	auto presetValueWriter = [](std::ostream& output, PresetValue presetValue)
	{
		output.write(reinterpret_cast<const char*>(&presetValue.value), sizeof(presetValue.value));
		output.write(reinterpret_cast<const char*>(&presetValue.isMuted), sizeof(presetValue.isMuted));
	};

	paramHistory.Write(output, paramIDWriter, presetValueWriter);

}
void PresetImpl::Read(std::istream& input)
{
	auto paramIDReader = [](std::istream& input, ParamID& paramID)
	{
		input.read(reinterpret_cast<char*>(&paramID.elemType), sizeof(paramID.elemType));
		input.read(reinterpret_cast<char*>(&paramID.elemIndex), sizeof(paramID.elemIndex));
		input.read(reinterpret_cast<char*>(&paramID.portType), sizeof(paramID.portType));
		input.read(reinterpret_cast<char*>(&paramID.portIndex), sizeof(paramID.portIndex));
		input.read(reinterpret_cast<char*>(&paramID.subIndex), sizeof(paramID.subIndex));
	};
	auto presetValueReader = [](std::istream& input, PresetValue& presetValue)
	{
		input.read(reinterpret_cast<char*>(&presetValue.value), sizeof(presetValue.value));
		input.read(reinterpret_cast<char*>(&presetValue.isMuted), sizeof(presetValue.isMuted));
	};
	paramHistory.Read(input, paramIDReader, presetValueReader);

}


inline bool KeyValueIsFixed(int value)
{
	return (value & (1 << 13)) == 0;
}

void PresetImpl::InterpolateKeytableValues(uint8_t tableIndex, uint8_t lowKey, uint8_t highKey)
{
	assert(tableIndex < 4);
	assert(lowKey >= 0 && lowKey <= 127);
	assert(highKey >= 0 && highKey <= 127);
	assert(lowKey <= highKey);

	ParamID paramID(ElementType::KeyTable, tableIndex, PortType::None, 0, lowKey);

	// Get the value for the high and low keys. If they are at the ends of the range they may not be fixed.
	// If they're not, then the values are zero.
	int lowKeyValue = GetParameterValue(paramID);
	if (!KeyValueIsFixed(lowKeyValue))
	{
		lowKeyValue = 0;
	}

	paramID.subIndex = highKey;
	int highKeyValue = GetParameterValue(paramID);
	if (!KeyValueIsFixed(highKeyValue))
	{
		highKeyValue = 0;
	}

	// Do the interpolation from lowKey to highKey
	double delta = static_cast<double>(highKeyValue - lowKeyValue) / abs(highKey - lowKey);
	double interpolatedValueDbl = lowKeyValue;
	uint32_t interpolatedValueInt;

	for (uint8_t key = lowKey + 1; key < highKey; ++key)
	{
		interpolatedValueDbl += delta;
		interpolatedValueInt = static_cast<uint32_t>(interpolatedValueDbl + 0.5);
		interpolatedValueInt |= (1 << 13);	// mark as interpolated
		paramID.subIndex = key;
		values.at(paramID).value = interpolatedValueInt;
	}
}

/** @brief Updates interpolated values int a keytable.
*/
void PresetImpl::UpdateKeyTable(uint8_t tableIndex, int currentKey)
{
	ParamID paramID(ElementType::KeyTable, tableIndex, PortType::None, 0, currentKey);

	bool fixed = KeyValueIsFixed(GetParameterValue(paramID));

	// Find the previous fixed key.
	int previousFixedKey = currentKey - 1;
	while (previousFixedKey >= 0)
	{
		paramID.subIndex = static_cast<uint8_t>(previousFixedKey);
		if (KeyValueIsFixed(GetParameterValue(paramID)))
		{
			break;
		}
		--previousFixedKey;
	};
	if (previousFixedKey < 0)
	{
		previousFixedKey = 0;
	}

	// Find the next fixed key and its value.
	int nextFixedKey = currentKey + 1;
	int lastKeyIndex = Preset::GetNumberOfParametersInElement(ElementType::KeyTable) - 1;

	while (nextFixedKey <= lastKeyIndex)
	{
		paramID.subIndex = static_cast<uint8_t>(nextFixedKey);
		if (KeyValueIsFixed(GetParameterValue(paramID)))
		{
			break;
		}
		++nextFixedKey;
	}
	if (nextFixedKey > lastKeyIndex)
	{
		nextFixedKey = lastKeyIndex;
	}

	// If the current key is fixed, then we need to re-calculate all interpolated values betweeen the 
	// previousFixedKey and the currentKey and also the currentKey and the nextFixedKey.
	if (fixed)
	{
		InterpolateKeytableValues(tableIndex, static_cast<uint8_t>(previousFixedKey), static_cast<uint8_t>(currentKey));
		InterpolateKeytableValues(tableIndex, static_cast<uint8_t>(currentKey), static_cast<uint8_t>(nextFixedKey));
	}
	// If the current key was changed from fixed to interpolated, then we need to re-calculate all 
	// interpolated values betweeen the previousFixedKey and the nextFixedKey.
	else
	{
		InterpolateKeytableValues(tableIndex, static_cast<uint8_t>(previousFixedKey), static_cast<uint8_t>(nextFixedKey));
	}



}

/** @brief	Sets the current toggle state of Compare or FXBypass functions.

	These are not actually part of a preset but the states are kept here.

	@param[in] stateType	The state to set. One of the PresetState enumerations.
	@param[in] enabled		Set to true to enable the function. Set to false to disable it.
*/
void PresetImpl::SetState(PresetState stateType, bool enabled)
{
	switch (stateType)
	{
	case PresetState::Compare:
		if (compareState != enabled)
		{
			compareState = enabled;
			StateChangedEvent.Notify(stateType, enabled);
		}
		break;
	case PresetState::FXBypass:
		if (fxBypassState != enabled)
		{
			fxBypassState = enabled;
			StateChangedEvent.Notify(stateType, enabled);
		}
		break;
	}
}

/** @brief	Gets the current toggle state of Compare or FXBypass functions.

	These are not actually part of a preset but the states are kept here.
	@param[in] stateType	The state to get. One of the PresetState enumerations.
	@returns				True if the specified function is enabled, false otherwise.
*/
bool PresetImpl::GetState(PresetState stateType)
{
	bool result = false;
	switch (stateType)
	{
	case PresetState::Compare:
		result = compareState;
		break;
	case PresetState::FXBypass:
		result = fxBypassState;
		break;
	}
	return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


Preset::Preset(CommandHistory* pCommandHistory)
	:m_pImpl(make_unique<PresetImpl>(pCommandHistory))
{
	// initialize the m_values list.
	for (auto& elemItem : masterParamList)
	{
		for (int elemIndex = 0; elemIndex < elemItem.numElements; ++elemIndex)
		{
			for (size_t paramIndex = 0; paramIndex < elemItem.elementParamList.size(); ++paramIndex)
			{
				ParamInfo& pi = elemItem.elementParamList[paramIndex];
				m_pImpl->values[ParamID(elemItem.elementType, elemIndex, pi.portType, pi.portIndex, pi.subIndex)] = PresetValue();
			}
		}
	}

	for (auto& val : m_pImpl->values)
	{
		m_pImpl->paramHistory.AddItem(Version{0,0}, val.first, PresetValue());
	}

	// Generate the container that keeps track of the elements that were visited when figuring out the enabled elements.
	for (auto& elemItem : masterParamList)
	{
		for (int elemIndex = 0; elemIndex < elemItem.numElements; ++elemIndex)
		{
			m_pImpl->visitedElements.emplace(ElementID(elemItem.elementType, elemIndex ), false);
		}
	}

	// We can initialize the preset as version 0 here and if the command history send another version 0 command 
	// (such as a NewPreset) it won't hurt because these values will just get overwritten.
	m_pImpl->InitializePreset(false, Version{0,0});

}

// This definition is required here even though it's empty, because m_pImpl is a unique_ptr.
// Without this, the compiler will want a definition of the PresetImpl destructor earlier and won't find it.
// But with this, the compiler won't look for the PresetImpl destructor until here, and it's now been defined.
Preset::~Preset()
{
}

Preset::EnabledChangedConnection Preset::SubscribeToEnabledChangedEvent(EnabledChangedEventType::CallbackType callback)
{
	return m_pImpl->EnabledChangedEvent.Connect(callback);
}

Preset::ParameterChangedConnection Preset::SubscribeToParameterChangedEvent(ParameterChangedEventType::CallbackType callback)
{
	return m_pImpl->ParameterChangedEvent.Connect(callback);
}
Preset::MultipleParametersChangedConnection Preset::SubscribeToMultipleParametersChangedEvent(MultipleParametersChangedEventType::CallbackType callback)
{
	return m_pImpl->MultipleParametersChangedEvent.Connect(callback);
}
Preset::StateChangedConnection Preset::SubscribeToStateChangedEvent(StateChangedEventType::CallbackType callback)
{
	return m_pImpl->StateChangedEvent.Connect(callback);
}

string Preset::GetName() const
{
	//return m_pImpl->presetName;
	string s;

	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName1 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName2 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName3 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName4 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName5 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName6 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName7 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName8 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName9 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName10 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName11 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName12 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName13 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName14 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName15 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName16 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName17 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName18 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName19 }).value);
	s += static_cast<char>(m_pImpl->values.at({ ElementType::Home, 0, PortType::None, 0, gloName20 }).value);
	return s;
}


void Preset::SetPreset(const std::vector<ParamValue>& paramValues, Version version)
{
	m_pImpl->SetPreset(paramValues, version);
}

std::vector<ParamValue> Preset::GetPreset() const
{
	std::vector<ParamValue> paramValues;
	for (auto& value : m_pImpl->values)
	{
		paramValues.push_back({ value.first, value.second.value });
	}

	return paramValues;
}

int32_t Preset::GetParameterValue(const ParamID& paramID) const
{
	return m_pImpl->GetParameterValue(paramID);
}

void Preset::SetParameterValue(const ParamID& paramID, int32_t newValue, Version version)
{
	m_pImpl->SetParameterValueNotify(paramID, newValue, true, version);
}

int Preset::GetNumberOfPorts(ElementID elemID, PortType portType) const
{
	// There's one exception to what's in the masterParamList. An Oscillator has one Input when the osc type is MM1 (the gate input) 
	// and zero Inputs otherwise. So that is just handled separately by querying the Parameter value.

	int portCount{ 0 };

	if (portType == PortType::Output)
	{
		// Output ports are not represented in the master parameter table, so handle it here.
		switch (elemID.elemType)
		{
		case ElementType::LoopEG:	portCount = 2;	break;
		case ElementType::Home:
		case ElementType::System:
		case ElementType::SPDIF_Out:
		case ElementType::Output:	portCount = 0;	break;
		default:					portCount = 1;	break;
		}
	}
	else if (elemID.elemType != ElementType::Oscillator || portType != PortType::Input || GetParameterValue(ParamID(elemID, oscType)) == 1)
	{
		const ElementItem& elemItem = GetElementItem(elemID.elemType);
		portCount = std::count_if(elemItem.elementParamList.cbegin(), elemItem.elementParamList.cend(), [portType](const ParamInfo& paramInfo) { return paramInfo.portType == portType && paramInfo.subIndex == psiSource;});
	}
	return portCount;
}

/* @brief GetNumberOfParametersInElement is currently only used by the Keytable UI to find how many keytable entries there are.

	Perhaps this function is unnecessary and the UI can assume the number of keytable entries. But anyway, it's here to avoid that assumption.

	@param[in] elemType		The element type to get the number parameters for.
*/
int Preset::GetNumberOfParametersInElement(ElementType elemType)
{
	return GetElementItem(elemType).elementParamList.size();
}


::Range Preset::GetParameterRangeInfo(const ParamID& paramID) const
{
	::Range newRange = GetParameterInfo(paramID).range;

	// Special case
	// If this is an Oscillator normal mod amount (psiAmount) and the actual range and divisor depend on the mod destination.
	//if (paramID.elemType == ElementType::Oscillator && paramID.portType == PortType::Mod && paramID.subIndex == psiAmount)
	//{
	//	ParamID destParamID = ParamID(ElementType::Oscillator, paramID.elemIndex, PortType::Mod, paramID.portIndex, psiDest);	// form the correspoding destination parameter
	//	auto destIndex = GetParameterValue(destParamID);

	//	// If the destination index is 3, it means Shape, so use the special range info.
	//	if (destIndex == 3)
	//	{
	//		newRange = ::Range(-100, 100, divisor1);
	//	}
	//}

	return newRange;
}

vector<string> Preset::GetParameterListText(const ParamID& paramID)
{

	vector<string> result;
	// Special cases for enumerations that have only a single possible setting.
	// So they are not represented by a parameter and consequently are not in the master parameter table for us to read out.
	// But the single choice still has a name list with a single name.
	if (paramID.elemType == ElementType::Filter && paramID.portType == PortType::None && paramID.subIndex == filterBypassMode)
	{
		assert(BypassModeNames.size() == 1);
		result.push_back(BypassModeNames[0]);
	}
	else if (paramID.elemType == ElementType::Filter && paramID.portType == PortType::None && paramID.subIndex == filterSSMMode)
	{
		assert(SSMModeNames.size() == 1);
		result.push_back(SSMModeNames[0]);
	}
	else if (paramID.elemType == ElementType::Filter && paramID.portType == PortType::None && paramID.subIndex == filterMiniMode)
	{
		assert(MiniModeNames.size() == 1);
		result.push_back(MiniModeNames[0]);
	}

	else  // general case
	{
		const ParamInfo& paramInfo = GetParameterInfo(paramID);
		if (paramInfo.valueType == ValueType::Enum)
		{
			for (auto pName : *paramInfo.enumList)
			{
				result.push_back(pName);
			}
		}
	}

	return result;
}

/** @brief	Assumes this Parameter is a modulation destination and returns a list of names of the possible destination choices in order.

	This function should only be called if the parameter is known to be a modulation destination, otherwise the result is undefined.

	@param[in] elemID			The ID of the element to get the name List for. The reason you need to supply the element ID 
								and not just the element type is because the filter may have a different destination name list 
								depending on the filter type, and the filter type may be different for different filters.
	@param[in] modPortIndex		The zero-based index of the modulation port to get the name List for.

	@returns					A list of names of the possible destination choices. 
*/
vector<string> Preset::GetModDestinationNameList(ElementID elemID, unsigned int modPortIndex)
{
	vector<string> list;
	const EnumListType* pDestList{};

	// Special case for filter. The destination list depends on the current filter type.
	switch (elemID.elemType)
	{
	case ElementType::Filter:
		{
		auto fltType = GetParameterValue(ParamID(elemID, (uint8_t)filterType));
		pDestList = (fltType == 5) ? &combModDestList :		// comb filter type
			(fltType == 6) ? &vocalModDestList :	// vocal filter type
			&fltModDestList;
		}
		break;
	// Special cases for modulation ports that have only a single destination.
	// So they are not represented by a parameter and consequently are not in the master parameter table for us to read out.
	case ElementType::EG:
		{
		assert(modPortIndex < 4);
		const EnumListType* egDestList[] = { &egAttackDestList, &egDecayDestList, &egSustainDestList, &egReleaseDestList };
		pDestList = egDestList[modPortIndex];
		}
		break;
	case ElementType::Vector:
		assert(modPortIndex < 2);
		pDestList = (modPortIndex == 0) ? &vecModDestList0 : &vecModDestList1;
		break;
	case ElementType::AM:
		pDestList = &amModDestList;
		break;
	case ElementType::Mixer:
		assert(modPortIndex < 5);
		pDestList = (modPortIndex == 4) ? &outputDestList : &levelDestList;
		break;
	case ElementType::InsertFX:
		pDestList = &insertFXModDestList;
		break;
	case ElementType::VCA:
		assert(modPortIndex < 2);
		pDestList = modPortIndex == 0 ? &levelDestList : &panDestList;
		break;
	case ElementType::VCAMaster:
		pDestList = &levelDestList;
		break;
	case ElementType::LoopEG:
		pDestList = modPortIndex == 0 ? &loopEgModDest0 : &loopEgModDest1;
		break;
	default:
		{
		const ParamInfo& paramInfo = GetParameterInfo(ParamID(elemID, PortType::Mod, narrow<uint8_t>(modPortIndex), psiDest));
		pDestList = paramInfo.enumList;
		}
		break;
	};

	for (auto item : *pDestList)
	{
		list.push_back(string(item));
	}
	return list;
}

/** @brief	Mutes or unmutes the specified parameter. This has no effect on parameters that cannot be muted.

	Listeners of the Preset::ParameterChangedEvent will be notified that the muting status has changed.

	@param[in] paramID	The ID of the parameter to mute.
	@param[in] isMuted	Setting this to true will mute the parameter. Setting it to false will unmute it.
*/
void Preset::SetMuted(const ParamID& paramID, bool isMuted, Version version)
{
	m_pImpl->SetMutedNotify(paramID, isMuted, true, version);
}

/** @brief	Gets the muted state of the specified parameter.

	@param[in] paramID	The ID of the parameter to check.
	@returns			true if the parameter is muted, false if the parameter is not muted or is one that cannot be muted.
*/
bool Preset::GetMuted(const ParamID& paramID) const
{
	// No need to check if the parameter is mutable. If it's not, it will never have been changed from it's default value of 'false'.
	return m_pImpl->GetMuted(paramID);
}

/** @brief	Gets a list of all parameters that are currently in a muted state.
	@returns	A list if all parameters that are currently in a muted state.
*/
std::vector<ParamID> Preset::GetAllMuted()
{
	vector<ParamID> result;
	for (auto& val : m_pImpl->values)
	{
		if (val.second.isMuted)
		{
			result.push_back(val.first);
		}
	}

	return result;
}


/** Creates a new connector with the given starting and ending port IDs.

	This function assumes that the caller has already checked that the connection is a legal one 
	and so the connection can be made. It does not do the check and so it doesn't fail if parameters are not 
	valid port IDs.	If the port that is the input port already has a connection, that connection is removed 
	(because an input port can only have one connection).\n
	Connectors are not actual objects but just conceptual. In the model, they're really just represented by 
	setting some source parameter to refer to some other element. But since the Solaris is rather modular, 
	these can be thought of as connectors from one element to another.\n
	Note that startingPortID and endingPortID are really interchangable and there is no real significance to 
	the names. So far I have found no reason to designate one end as the starting end and the other as the 
	ending end. However one must be an output port type and the other must be one of the three input port types.\n
	\n
	The elements involved must already be active. This function does not make them active.\n
	\n
	CreateConnector() will act as RemoveConnector() if the output port given has an element type of ElementType::Off.
	\n
	@internal It may be better to have CreateConnector() accept a Connector object and return the existing connector.
	That way the GetConnector() function might not be needed. It is currently used for undo. then, the Connector 
	structure would just be created using a constructor or initializer. The same for RemoveConnector().

	@param[in] startingPortID	The ID of the port at one end of the connector.
	@param[in] endingPortID		The ID of the port at the other end of the connector.

	@returns	The Connector that was created.

*/
Connector Preset::CreateConnector(const PortID& startingPortID,const PortID& endingPortID, Version version)
{
	// One end of a connector is the input. And all inputs can only have one connector.
	// So when we add this connector we need to remove any existing connector that as the same input endpoint.
	const PortID& inputPortID = startingPortID.portType != PortType::Output ? startingPortID : endingPortID;
	const PortID& outputPortID = startingPortID.portType != PortType::Output ? endingPortID : startingPortID;

	ElementID inputElemID { inputPortID.elemID.elemType, inputPortID.elemID.elemIndex };
	ElementID outputElemID { outputPortID.elemID.elemType, outputPortID.elemID.elemIndex };
	
	ParamID inputParamID(inputPortID.elemID.elemType, inputPortID.elemID.elemIndex, inputPortID.portType, inputPortID.portIndex);
	m_pImpl->SetSourceID(inputParamID, outputPortID, version);
	
	Connector newConnector;
	newConnector.startPort = startingPortID;
	newConnector.endPort = endingPortID;

	return newConnector;
}

void Preset::RemoveConnector(const Connector& connectorToRemove, Version version)
{
	PortID inputPortID = connectorToRemove.startPort.portType == PortType::Output ? connectorToRemove.endPort : connectorToRemove.startPort;
	ElementID inputElemID { inputPortID.elemID.elemType, inputPortID.elemID.elemIndex };
	
	ParamID inputParamID(inputPortID.elemID.elemType, inputPortID.elemID.elemIndex, inputPortID.portType, inputPortID.portIndex);
	m_pImpl->SetSourceID(inputParamID, SourceID(), version);	// default SourceID is ElementType::OFF

}

/** @brief Returns a list of all the current connections in the preset.
	
	@returns	A list of Connectors. 
*/
std::vector<Connector> Preset::GetConnectorList() const
{
	std::vector<Connector> result;

	for (auto& val : m_pImpl->values)
	{
		if (IsSource(val.first))
		{
			// If the Element is an Oscillator, then we only count it if the oscilator is MM1. 
			// (This is the way it has worked originally, but should this function actually report the osc sync connection even when the osc type is not MM1? )
			ElementID sourceElemID{ val.first.elemType, val.first.elemIndex };
			if (val.first.elemType != ElementType::Oscillator || 
				val.first.portType == PortType::Mod || 
				val.first.portType == PortType::Control || 
				(val.first.portType == PortType::Input && GetParameterValue(ParamID(sourceElemID, oscType)) == 1))	// oscType == 1 means the MM1 type, the only type which has a gate input.
			{
				auto sourceID = m_pImpl->GetSourceID(val.first);
				if (sourceID.IsConnected())
				{
					result.push_back({ PortID(sourceID.elemID, PortType::Output, static_cast<uint8_t>(sourceID.outputPortIndex)), PortID(sourceElemID, val.first.portType, val.first.portIndex) });
				}
			}
		}
	}

	return result;
}

/** Determines if two given ports can be connected to each other.

	There is no difference between the two port parameters even though they 
	have difference names---sourcePortID and targetPortID. Those names may just reflect the 
	starting and ending point when a connector was drawn in the UI. But otherwise they could 
	just as easily be swapped.\n
	This function will return false of both ports are output types or both ports are NOT output 
	types. There must be one and only one output port.

	@param[in]	sourcePortID	One port involved in the connection.
	@param[in]	targetPortID	Another port involved in the connection.

	@returns	true if the two ports can be connected. False otherwise.
*/
bool Preset::CanBeConnected(const PortID& sourcePortID,const PortID& targetPortID)
{
	// Inputs cannot be connected to each other. Outputs cannot be connected to each other.
	const bool sourceIsOutput = sourcePortID.portType == PortType::Output;
	const bool targetIsOutput = targetPortID.portType == PortType::Output;
	if (sourceIsOutput == targetIsOutput)
	{
		return false;
	}
	const PortID& outputPortID = sourceIsOutput ? sourcePortID : targetPortID;
	const PortID& inputPortID = sourceIsOutput ? targetPortID : sourcePortID;

	SourceListType sourceList = GetPortSourceList(inputPortID);

	return (std::find(sourceList.begin(), sourceList.end(), outputPortID) != sourceList.end());
}

/** @brief	Sets all parameters to an initialized preset.

	MultipleParametersChanged event is sent.
*/
void Preset::InitializePreset(Version version)
{
	m_pImpl->InitializePreset(true, version);
}

/** Get a list of the Elements that are contributing to the Preset's sound starting from the outputs.

	Output elements are not included in the list if they have no input.

	@returns			A list of the Elements that are contributing to the Preset's sound.
*/
vector<ElementID> Preset::GetElementTree()
{
	m_pImpl->ClearVisited();
	std::vector<ElementID> enabledElements;
	ElementID elemID(ElementType::Output, 0);
	for (uint8_t i = 0; i < 4; ++i)
	{
		elemID.elemIndex = i;
		m_pImpl->GetEnabledElements(enabledElements, elemID, 0);
	}

	// For now, Home is always in the tree in order to keep it visible.
	// But in future versions this element may not be needed because those parameters 
	// should be displayed differently. Then this line could be removed.
	enabledElements.push_back(ElementID(ElementType::Home, 0));

	// sort and then remove duplicates.
	std::sort(enabledElements.begin(), enabledElements.end());
	auto last = std::unique(enabledElements.begin(), enabledElements.end());	// move duplicate elements to the end of the array
	enabledElements.erase(last, enabledElements.end());		// remove the duplicate elements from the end

	return enabledElements;
}

/** Get a list of the Elements that are contributing to the Preset's sound starting from a given Element.

	@param[in] elemID	The ID of the element to start from. The list returned contains all the Elements that 
						are "child" elements in the directed graph where those child element's outputs eventually
						feed into one of this element's inputs. If the elemID parameter is set to be an Output 
						of the synth, then all Elements that contribute to the sound of a Preset are included.
	@returns			A list of the Elements that are contributing to the Preset's sound starting from a given Element.
*/
vector<ElementID> Preset::GetElementTree(ElementID elemID)
{
	m_pImpl->ClearVisited();
	std::vector<ElementID> enabledElements;

	m_pImpl->GetEnabledElements(enabledElements, elemID, 0);

	// For now, Home is always in the tree in order to keep it visible.
	// But in future versions this element may not be needed because those parameters 
	// should be displayed differently. Then this line could be removed.
	enabledElements.push_back(ElementID(ElementType::Home, 0));

	// sort and then remove duplicates.
	std::sort(enabledElements.begin(), enabledElements.end());
	auto last = std::unique(enabledElements.begin(), enabledElements.end());	// move duplicate elements to the end of the array
	enabledElements.erase(last, enabledElements.end());		// remove the duplicate elements from the end

	return enabledElements;
}

/** @brief	Sets the toggle state of Compare or FXBypass functions.

	These are not actually part of a preset but the states are kept here.

	@param[in] stateType	The state to set. One of the PresetState enumerations.
	@param[in] enabled		Set to true to enable the function. Set to false to disable it.
*/
void Preset::SetState(PresetState stateType, bool enabled)
{
	m_pImpl->SetState(stateType, enabled);
}

/** @brief	Gets the current toggle state of Compare or FXBypass functions. 

	These are not actually part of a preset but the states are kept here.
	@param[in] stateType	The state to get. One of the PresetState enumerations.
	@returns				True if the specified function is enabled, false otherwise.
*/
bool Preset::GetState(PresetState stateType)
{
	return m_pImpl->GetState(stateType);
}

/** not currently used */
bool Preset::IsModified()
{
	return m_pImpl->isModified; 
}


/** Get a list of the output connectors connected to the given Element ID.

	@param[in] elemID	The ID of the element.
	@returns			A list of the output connectors connected to the given Element ID.
*/
std::vector<Connector> Preset::GetOutputConnectorsOfElement(const ElementID& elemID)
{
	// Get a list of all current connectors.
	std::vector<Connector> connectorList = GetConnectorList();
	
	// Filter out thsoe that aren't connected to the output of the spcified element.
	connectorList.erase(std::remove_if(connectorList.begin(), connectorList.end(), [&elemID](Connector& conn)
	{
		return ((conn.startPort.elemID != elemID || conn.startPort.portType != PortType::Output) &&
			(conn.endPort.elemID != elemID || conn.endPort.portType != PortType::Output));
	}), connectorList.end());
	return connectorList;
}

void Preset::Write(std::ostream& output)
{
	m_pImpl->Write(output);
}
void Preset::Read(std::istream& input)
{
	m_pImpl->Read(input);
}

#if defined(_DEBUG)
void Preset::Dump(std::ostream& outStr)
{
	using std::endl;

	for (auto& elemItem : masterParamList)
	{
		for (int elemIndex = 0; elemIndex < elemItem.numElements; ++elemIndex)
		{
			outStr << GetElementName({ elemItem.elementType, elemIndex }) << ":" << endl;
			outStr << "--------------------" << endl;

			if (elemItem.elementParamList.size() > 0)
			{
				for (size_t paramIndex = 0; paramIndex < elemItem.elementParamList.size(); ++paramIndex)
				{
					ParamID paramID = ParamID(	elemItem.elementType,
												elemIndex,
												elemItem.elementParamList[paramIndex].portType,
												elemItem.elementParamList[paramIndex].portIndex,
												elemItem.elementParamList[paramIndex].subIndex);
					std::string paramName = GetParameterName(paramID);
					outStr << paramName;
					for (int i = 0; i < std::max(0, static_cast<int>(20 - paramName.length())); ++i)
					{
						outStr << " ";
					}
					outStr << ": " << m_pImpl->values.at(paramID).value << endl;;
				}
			}
			outStr << endl;
		}
	}
}
#endif

