/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "TimeSlider.h"
#include <sstream>
#include <iomanip>
#include <cassert>

SliderTime::SliderTime()
{
	setSkewFactor(0.3);
}

 String SliderTime::getTextFromValue(double value)
{
	assert(value >= 0 && value <= 200000);

	std::ostringstream os;
	os << std::fixed << std::showpoint;
	if (value < 10000)
	{
		os << std::setprecision(1) << value / 10 << " ms";
	}
	else
	{
		os << std::setprecision(2) << value / 10000 << " sec";
	}
	return os.str();
}

 double SliderTime::getValueFromText(const String & text)
 {
	 double value = text.getDoubleValue();
	 if (text.contains("s"))	// seconds
	 {
		 value *= 10000;
	 }
	 else // milliseconds
	 {
		 value *= 10;
	 }
	 int valueInt = lrint(value);
	 return valueInt;
 }

 double SliderTime::getMouseWheelDelta(double value, double wheelAmount)
 {
	 const SliderStyle style = getSliderStyle();
	 double interval = (value >= 10000) ? 1000 : 100;

	 if (style == IncDecButtons)
		 return interval * wheelAmount;

	 const double proportionDelta = wheelAmount * 0.15f;
	 const double currentPos = valueToProportionOfLength(value);
	 return proportionOfLengthToValue(jlimit(0.0, 1.0, currentPos + proportionDelta)) - value;
 }
 void SliderTime::mouseWheelMove(const MouseEvent& e, const MouseWheelDetails& wheel)
 {
	 const SliderStyle style = getSliderStyle();
	 bool handled = false;
	 if (!e.mods.isShiftDown()
		 && style != TwoValueHorizontal
		 && style != TwoValueVertical)
	 {
		 // sometimes duplicate wheel events seem to be sent, so since we're going to
		 // bump the value by a minimum of the interval, avoid doing this twice..
		 if (e.eventTime != lastMouseWheelTime)
		 {
			 lastMouseWheelTime = e.eventTime;

			 if (getMaximum() > getMinimum() && !e.mods.isAnyMouseButtonDown())
			 {
				 HideEditor();

				 const double value = getValue();
				 double delta = getMouseWheelDelta(value, (std::abs(wheel.deltaX) > std::abs(wheel.deltaY)
					 ? -wheel.deltaX : wheel.deltaY)
					 * (wheel.isReversed ? -1.0f : 1.0f));

				 delta = (delta > 0) ? 1 : ((delta < 0) ? -1 : 0);
				 double interval = (value >= 10000) ? 1000 : 100;

				 if (e.mods.isCommandDown())
				 {
					 interval = (value >= 10000) ? 100 : 1;
				 }
				 if (delta != 0)
				 {
					 const double newValue = value + jmax(interval, std::abs(delta)) * (delta < 0 ? -1.0 : 1.0);

					 //DragInProgress drag(*this);
					 setValue(snapValue(newValue, notDragging), sendNotificationSync);
				 }
			 }
		 }

		 handled = true;
	 }

	 if (!(isEnabled() && handled))
	 {
		 Component::mouseWheelMove(e, wheel);
	 }
 }



String SliderFreq20KHz::getTextFromValue(double value)
{
	assert(value >= 0 && value <= 200000);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(1);
	os << value / 10 << "Hz";
	return os.str();
}

double SliderFreq20KHz::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	return value * 10;
}

String SliderFreq500Hz::getTextFromValue(double value)
{
	assert(value >= 0 && value <= 50000);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(2);
	os << value / 100 << "Hz";
	return os.str();
}

double SliderFreq500Hz::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	return value * 100;
}
