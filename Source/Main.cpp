/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainComponent.h"

ApplicationCommandManager& GetGlobalCommandManager();

//==============================================================================
class SolariumApplication  : public JUCEApplication
{
public:
    //==============================================================================
	SolariumApplication() {}

	const String getApplicationName() override       { return "Solarium";  /* return ProjectInfo::projectName; */ }
    const String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override       { return true; }
	ApplicationCommandManager& GetGlobalCommandManager() { return commandManager; }

    //==============================================================================
    void initialise (const String& commandLine) override
    {
        // This method is where you should put your application's initialisation code..
		commandLine;
        mainWindow = std::make_unique<MainWindow>(getApplicationName());
		mainWindow->setResizable(true, false);
    }

    void shutdown() override
    {
        // Add your application's shutdown code here..

        mainWindow = nullptr; // (deletes our window)
    }

    //==============================================================================
    void systemRequestedQuit() override
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }

    void anotherInstanceStarted (const String& commandLine) override
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.
		commandLine;
    }



    //==============================================================================
    /*
        This class implements the desktop window that contains an instance of
        our MainContentComponent class.
    */
    class MainWindow    : public DocumentWindow
    {
    public:
        MainWindow (String name)  : DocumentWindow (name,
                                                    Colours::lightgrey,
                                                    DocumentWindow::allButtons)
        {
			setUsingNativeTitleBar (true);
			MainContentComponent* pMainContentComponent = new MainContentComponent();
			setContentOwned(pMainContentComponent, true);
			setBackgroundColour(Colour(0xffE8E8E8));
            
			centreWithSize (getWidth(), getHeight());
            setVisible (true);

			// this lets the command manager use keypresses that arrive in our window to send out commands
			addKeyListener(::GetGlobalCommandManager().getKeyMappings());

			::GetGlobalCommandManager().registerAllCommandsForTarget(pMainContentComponent);
        }

		~MainWindow()
		{}

		void closeButtonPressed()
		{
			// This is called when the user tries to close this window. Here, we'll just
			// ask the app to quit when this happens, but you can change this to do
			// whatever you need.
			bool okToQuit = true;
			if (((MainContentComponent*)getContentComponent())->IsModified())	// IsModified() is not currently implemented, always returns false.
			{
				okToQuit = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
					"Changes not saved!",
					"Your current preset has not been saved since it was last modified. Are you sure you want to quit the program and lose your changes?",
					"Yes",
					"No",
					this,
					nullptr);
			}
			if (okToQuit)
			{
				JUCEApplication::getInstance()->systemRequestedQuit();
			}
		}
		

        /* Note: Be careful if you override any DocumentWindow methods - the base
           class uses a lot of them, so by overriding you might break its functionality.
           It's best to do all your work in your content component instead, but if
           you really have to override any DocumentWindow methods, make sure your
           subclass also calls the superclass's method.
        */

    private:
		TooltipWindow tooltipWindow;	// to add tooltips to an application, you
										// just need to create one of these and leave it
										// there to do its work..

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

private:
    std::unique_ptr<MainWindow> mainWindow;
	ApplicationCommandManager commandManager;

};

ApplicationCommandManager& GetGlobalCommandManager()
{
	return dynamic_cast<SolariumApplication*> (JUCEApplication::getInstance())->GetGlobalCommandManager();
}

//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (SolariumApplication)
