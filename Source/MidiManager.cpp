/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <vector>
#include <algorithm>
#include <functional>
#include <tuple>
#include <cstdint>
#include <iomanip>
#include <fenv.h>
#include "Preset.h"
#include "MidiManager.h"
#include "Properties.h"
#include "ParameterList.h"
#include "Command.h"
#include "SysExTables.h"


using std::uint8_t;
using std::vector;

const uint8_t SYSEX_BULK_DUMP_REQUEST = 0x10;
const uint8_t SYSEX_BULK_DUMP = 0x11;
const uint8_t SYSEX_REQUEST_PARAMETER = 0x12;
const uint8_t SYSEX_PARAMETER_CHANGED = 0x13;
const uint8_t SYSEX_STATE_CHANGED = 0x31;
enum {	SYSEX_PARAMETER_TIMEOUT = 5000 };


/** @brief	Iterates through all "Midi" parameters and executes the given function on them.  A "Midi" parameter is one
			that either has a non-zero NRPN number or is an Arp step.

	@param[in] func		A function that gets called on each valid parameter.
*/
void ForEachMidiParam(std::function<void(ElementItem& elemItem, int elemIndex, int paramIndex)> func)
{
	for (auto& elemItem : masterParamList)
	{
		for (int elemIndex = 0; elemIndex < elemItem.numElements; ++elemIndex)
		{
			for (size_t paramIndex = 0; paramIndex < elemItem.elementParamList.size(); ++paramIndex)
			{
				if (elemItem.paramAddressList[elemIndex][paramIndex].IsValid())
				{
					func(elemItem, elemIndex, paramIndex);
				}
			}
		}
	}
}


/** @brief	Returns the number of parameters that either have an NRPN number or are an Arp step.
			These are received (but not necessarily sent to the Solaris.)
	@returns The number of parameters.
*/
int GetNumberOfRecvParameters()
{
	int count{};
	ForEachMidiParam([&count](ElementItem&, int, int) { ++count;});
	return count;
}

MidiManager::MidiManager(Preset* pPreset, CommandHistory* pCommandHistory)
	: m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
{
	assert(pPropertiesFile);
	assert(m_pPreset);
	assert(m_pCommandHistory);

	// Note that the name may be empty, particularly the first time the program is run and there is no config file.
	String midiOutCurrentName = pPropertiesFile->getValue(propMidiOutDeviceName);
	m_deviceManager.setDefaultMidiOutputDevice(midiOutCurrentName);

	// Note that m_pMidiOutDevice may be null here, particularly the first time the program is run and there is no config file.
	m_pMidiOutDevice = m_deviceManager.getDefaultMidiOutput();

	m_deviceID = static_cast<uint8_t>(pPropertiesFile->getValue(propDeviceID, "0").getIntValue());
	assert(m_deviceID >= 0 && m_deviceID <= 15);

	if (pPropertiesFile)
	{
		pPropertiesFile->addChangeListener(this);	// listen for change to the midi devices or channel
	}

	m_deviceManager.addMidiInputDeviceCallback({}, this);

	m_pSysExParamTimer = std::make_unique<SysExParamTimer>(this);

	m_syncOn = (pPropertiesFile->getValue(propMidiSync) == "on");

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted) { HandleParameterChanged(paramID, value, isMuted); });

	m_stateChangedConnection = m_pPreset->SubscribeToStateChangedEvent(
		[this](PresetState stateType, bool enabled) { SendState(stateType, enabled); });


	// Count the number of parameters for setting the progress percentage.
	m_totalNumberOfParametersRecv = GetNumberOfRecvParameters();
	EnableInput(true);
}


MidiManager::~MidiManager()
{
	EnableInput(false);
}

void MidiManager::EnableInput(bool enable)
{
	if (pPropertiesFile)
	{
		m_midiInCurrentName = pPropertiesFile->getValue(propMidiInDeviceName);
		m_deviceManager.setMidiInputDeviceEnabled(m_midiInCurrentName, enable);
	}
}


void MidiManager::setSync(bool syncOn)
{
	m_syncOn = syncOn;
}

void MidiManager::SendParameterRequest(ParamAddress paramAddress)
{
	const uint8_t data[] =
	{
		0x00, 0x12, 0x34,
		m_deviceID,		// Device ID
		0x10,
		SYSEX_REQUEST_PARAMETER,
		paramAddress.hi,
		paramAddress.mid,
		paramAddress.lo,
	};

	SendSysexData(data, sizeof(data));
}

/** @brief	Sends a SysEx request for a single block.

	@param[in] baseAddress	The base address of the block. The "lo" part of the address should be 
							zero unless this is a frame start request.
*/
void MidiManager::SendBlockRequest(ParamAddress baseAddress)
{
	const uint8_t data[] =
	{
		0x00, 0x12, 0x34,
		m_deviceID,		// Device ID
		0x10,
		SYSEX_BULK_DUMP_REQUEST,
		baseAddress.hi,
		baseAddress.mid,
		baseAddress.lo
	};

	SendSysexData(data, sizeof(data));
}


void MidiManager::changeListenerCallback(ChangeBroadcaster *source)
{
	if (pPropertiesFile && (source == pPropertiesFile.get()))
	{
		m_syncOn = (pPropertiesFile->getValue(propMidiSync) == "on");

		// The midi channel may have been changed so get the new value.
		m_deviceID = static_cast<uint8_t>(pPropertiesFile->getValue(propDeviceID, "0").getIntValue());

		String midiOutCurrentName = pPropertiesFile->getValue(propMidiOutDeviceName);
		m_deviceManager.setDefaultMidiOutputDevice(midiOutCurrentName);

		// Note that m_pMidiOutDevice may be null here, particularly the first time the program is run and there is no config file.
		m_pMidiOutDevice = m_deviceManager.getDefaultMidiOutput();

		if (m_deviceManager.isMidiInputDeviceEnabled(m_midiInCurrentName))
		{
			m_midiInCurrentName = pPropertiesFile->getValue(propMidiInDeviceName);
			m_deviceManager.setMidiInputDeviceEnabled(m_midiInCurrentName, true);
		}
	}
}

bool checkSysExMessageIsSolarisID(const uint8* data)
{
	return ((data[0] == 0x00) && (data[1] == 0x12) &&
			(data[2] == 0x34) && (data[4] == 0x10));
}

bool checkIsForDeviceID(const uint8* data, uint8_t deviceID)
{
	return (data[3] == deviceID);
}

struct BlockInfo
{
	BlockInfo() : pByteCountTable(NULL), pParamIDTable(NULL), blockSize(0) {}
	uint8_t* pByteCountTable;
	ParamID* pParamIDTable;
	uint16_t blockSize;
	bool IsValid()
	{
		return pByteCountTable != NULL && pByteCountTable != NULL;
	}
};
BlockInfo GetMIDIBlockInfo(ParamAddress address);

/** Receives an incoming message.

	A MidiInput object will call this method when a midi event arrives. It'll be
	called on a high-priority system thread, so avoid doing anything time-consuming
	in here, and avoid making any UI calls. You might find the MidiBuffer class helpful
	for queueing incoming messages for use later.
	
	@param source   the MidiInput object that generated the message
	@param message  the incoming message. The message's timestamp is set to a value
					equivalent to (Time::getMillisecondCounter() / 1000.0) to specify the
					time when the message arrived.
*/
void MidiManager::handleIncomingMidiMessage(MidiInput * source, const MidiMessage & msg)
{
	source;	// avoid "unused" warning

	if (msg.isSysEx())
	{
		// int sz = msg.getSysExDataSize();		// for debugging
		const uint8_t* pData = msg.getSysExData();

		//if (checkSysExMessageIsSolarisID(pData))
		if (checkSysExMessageIsSolarisID(pData) && checkIsForDeviceID(pData, m_deviceID))
		{
			// Uncomment the line below to show the message bytes in the debug output (decimal).
			// {	String s; int i; for (i = 0; data[i] != 0xF7; ++i) { s += String(data[i]) + ", "; }	s += String(data[i]);  DBG(s); }

			switch (pData[5])
			{
			case SYSEX_PARAMETER_CHANGED:

				if (m_syncOn)	// sync only applies to single parameter message, not bulk dumps
				{
					ParamAddress paramAddress(pData[6], pData[7], pData[8]);

					int32_t value = (pData[9] & 0x7F) << 25;
					value += (pData[10] & 0x7F) << 18;
					value += (pData[11] & 0x7F) << 11;
					value >>= 11;

					///////////////////////////////////////////////////////////////////////////
					bool valid = false;
					ParamID paramID;
					BlockInfo bi = GetMIDIBlockInfo(paramAddress);
					assert(bi.IsValid());
					if (bi.IsValid())
					{
						int byteLength = bi.pByteCountTable[paramAddress.lo];
						if (byteLength > 0)
						{
							paramID = bi.pParamIDTable[paramAddress.lo];
							valid = true;
						}
					}
					assert(valid);
					/////////////////////////////////////////////////////////////////////////////////

					// Don't handle a time-consuming action in this MIDI thread. Instead, just post it to 
					// the GUI thread.
					(new IncomingSysExParamCallback(this, paramAddress, value))->post();
				}
				break;

			case SYSEX_BULK_DUMP:

				// The SysEx bytes that pData points to is copied in the IncomingBulkDumpCallback constructor.
				(new IncomingBulkDumpCallback(this, (uint8_t*)(pData + 6), msg.getSysExDataSize() - 6))->post();
				break;

			case SYSEX_STATE_CHANGED:
				PresetState stateType = (pData[6] == 1) ? PresetState::Compare : PresetState::FXBypass;
				bool enabled = (pData[7] == 0) ? false : true;
				(new IncomingStateChangeCallback(this, stateType, enabled))->post();
				break;
			};

		}
	}
}


//@{
/**	These tables are used to more quickly lookup the MIDI block info for incoming bulk dump requests. */
uint8_t* Hi10ByteCountTables[8] = { oscByteCount, oscByteCount, oscByteCount, oscByteCount, rotByteCount, rotByteCount, vecByteCount, vcaByteCount };
ParamID* Hi10ParamIndexTables[8] = { oscParamID[0], oscParamID[1],oscParamID[2],oscParamID[3], rotParamID[0], rotParamID[1], vecParamID, vcaParamID };
uint16_t Hi10ByteBlockSize[8] = { OSC_MIDI_BLOCK_SIZE, OSC_MIDI_BLOCK_SIZE, OSC_MIDI_BLOCK_SIZE, OSC_MIDI_BLOCK_SIZE,
									ROT_MIDI_BLOCK_SIZE, ROT_MIDI_BLOCK_SIZE, VEC_MIDI_BLOCK_SIZE, VCA_MIDI_BLOCK_SIZE };
//@}

BlockInfo GetMIDIBlockInfo(ParamAddress address)
{
	BlockInfo bi;
	int midHiNibble = address.mid >> 4;
	switch (address.hi)
	{
	case 0x01:
		bi.pByteCountTable = sysByteCount;
		bi.pParamIDTable = sysParamID;
		bi.blockSize = SYS_MIDI_BLOCK_SIZE;
		break;
	case 0x10:
		bi.pByteCountTable = Hi10ByteCountTables[midHiNibble];
		bi.pParamIDTable = Hi10ParamIndexTables[midHiNibble];
		bi.blockSize = Hi10ByteBlockSize[midHiNibble];
		break;
	case 0x11:
		if (midHiNibble < 4)
		{
			bi.pByteCountTable = filByteCount;
			bi.pParamIDTable = filParamID[midHiNibble];
			bi.blockSize = FIL_MIDI_BLOCK_SIZE;
		}
		else if (midHiNibble == 4)
		{
			bi.pByteCountTable = mixByteCount;
			bi.pParamIDTable = mixParamID;
			bi.blockSize = MIX_MIDI_BLOCK_SIZE;
		}
		else if (midHiNibble == 5)
		{
			bi.pByteCountTable = insByteCount;
			bi.pParamIDTable = insParamID;
			bi.blockSize = INS_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x12:
		if (midHiNibble < 5)
		{
			bi.pByteCountTable = lfoByteCount;
			bi.pParamIDTable = lfoParamID[midHiNibble];
			bi.blockSize = LFO_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x13:
		if (midHiNibble < 6)
		{
			bi.pByteCountTable = envByteCount;
			bi.pParamIDTable = envParamID[midHiNibble];
			bi.blockSize = ENV_MIDI_BLOCK_SIZE;
		}
		else if (midHiNibble == 7)
		{
			bi.pByteCountTable = legByteCount;
			bi.pParamIDTable = legParamID;
			bi.blockSize = LEG_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x14:
		if (midHiNibble < 2)
		{
			bi.pByteCountTable = (midHiNibble == 0) ? seqByteCount : seqStepByteCount;
			bi.pParamIDTable = (midHiNibble == 0) ? seqParamID : seqStepParamID;
			bi.blockSize = (midHiNibble == 0) ? SEQ_MIDI_BLOCK_SIZE : SEQSTEP_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x15:
		if (midHiNibble == 0)
		{
			bi.pByteCountTable = effByteCount;
			bi.pParamIDTable = effParamID;
			bi.blockSize = EFF_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x16:	// Key tables
		bi.pByteCountTable = keyByteCount;
		bi.pParamIDTable = keyParamID[midHiNibble >> 4];
		bi.blockSize = KEY_MIDI_BLOCK_SIZE;
		break;

	case 0x17:
		if (midHiNibble == 0)	// name
		{
			bi.pByteCountTable = namByteCount;
			bi.pParamIDTable = namParamID;
			bi.blockSize = NAME_MIDI_BLOCK_SIZE;
		}
		else if (midHiNibble == 1)	// common
		{
			bi.pByteCountTable = comByteCount;
			bi.pParamIDTable = comParamID;
			bi.blockSize = COM_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x18:
		if (midHiNibble == 0)
		{
			bi.pByteCountTable = arpByteCount;
			bi.pParamIDTable = arpParamID;
			bi.blockSize = ARP_MIDI_BLOCK_SIZE;
		}
		else if (midHiNibble == 1)
		{
			bi.pByteCountTable = arpStepByteCount;
			bi.pParamIDTable = arpStepParamID;
			bi.blockSize = ARPSTEP_MIDI_BLOCK_SIZE;
		}
		break;
	case 0x20:
		// Multi not implemented yet
		break;
	case 0x7E:	// Frame Start
	case 0x7F:	// Frame End
		bi.pByteCountTable = frameByteCount;
		bi.pParamIDTable = frameParamID;
		bi.blockSize = 0;
		break;
	};
	return bi;
}

ParamAddress frameStartBaseAddress = { 0x7E, 0x7F, 0x7F };
ParamAddress frameEndBaseAddress = { 0x7F, 0x7F, 0x7F };

/** @brief	This function is called when a SysEx bulk dump message is received.

	@param[in] bulkData		A reference to a vector that holds the three-byte base address followed by 
							the bulkdata and the checksum.
*/
void MidiManager::HandleBulkDumpOnMessageThread(std::vector<uint8_t>& bulkData)
{
	// Assume the last byte is a checksum. The sum of all the bytes (after the three-byte address) 
	// must have the lowest 7 bits set to zero.
	uint32_t sum = 0;
	for (size_t i = 0; i < bulkData.size(); ++i)
	{
		sum += bulkData[i];
	}
	if ((sum & 0x7F) != 0)
	{
		// Need some kind of error message to the user here.
		assert(false);
		return;
	}

	ParamAddress baseAddress(bulkData[0], bulkData[1], bulkData[2]);
	static int numberOfBlocksSoFar;

	if (baseAddress.hi == 0x7E)	// frame start
	{
		m_frameIsActive = true;
		m_frameBank = baseAddress.mid;
		m_framePreset = baseAddress.lo;
		m_paramValues.clear();
		numberOfBlocksSoFar = 0;
		m_pSysExParamTimer->stopTimer();

	}
	else if (baseAddress.hi == 0x7F)	// frame end
	{
		m_frameIsActive = false;
		m_frameBank = baseAddress.mid;
		m_framePreset = baseAddress.lo;

		if (m_midiProgressCallback)
		{
			m_midiProgressCallback(1.0, true);
			m_midiProgressCallback = nullptr;
		}
	}
	else if (baseAddress.hi == 0x01 && baseAddress.mid == 0 && baseAddress.lo == 0)
	{
		// System block. This comes by itself. So stop timer.
		m_paramValues.clear();
		m_pSysExParamTimer->stopTimer();
	}

	// When the frame bank and preset are both 0x7F it means the blocks within this frame are from the Solaris's edit buffer.
	// Blocks received outside of a frame are also assumed to be from the Solaris's edit buffer.
	// For now, Solarium only recognizes blocks from the edit buffer.
	if (!m_frameIsActive || (m_frameIsActive && m_frameBank == 0x7F && m_framePreset == 0x7F))
	{
		BlockInfo blockInfo = GetMIDIBlockInfo(baseAddress);
		if (blockInfo.IsValid())
		{
			if (m_midiProgressCallback && m_frameIsActive)
			{
				double percent = static_cast<double>(++numberOfBlocksSoFar) / m_totalNumberOfBlocksToRecv;
				if (!m_midiProgressCallback(percent, false))
				{
					CancelRequest();
				}
			}

			// If the block info is valid it means there are tables that we can use in a
			// common way to interpret the bulk data and set parameters. 

			// The size of the MIDI message must equal the size of the block associated with this base address.
			// (Subtrack 4 for base address and checksum)
			if (bulkData.size() - 4 == blockInfo.blockSize)
			{
				int offset = 0;
				long value = 0;
				uint8_t* pBlock = &bulkData[3];

				// Loop through the block one field at a time.
				while (offset < blockInfo.blockSize)
				{
					ParamID paramID = blockInfo.pParamIDTable[offset];
					uint8_t byteLength = blockInfo.pByteCountTable[offset];
					if (byteLength == 1)
					{
						value = (pBlock[offset++] & 0x7F) << 25;
						value >>= 25;
					}
					else if (byteLength == 2)
					{
						value = (pBlock[offset++] & 0x7F) << 25;
						value |= (pBlock[offset++] & 0x7F) << 18;
						value >>= 18;
					}
					else if (byteLength == 3)
					{
						value = (pBlock[offset++] & 0x7F) << 25;
						value |= (pBlock[offset++] & 0x7F) << 18;
						value |= (pBlock[offset++] & 0x7F) << 11;
						value >>= 11;
					}

					if (byteLength > 0)
					{
						// TO DO: At some point the value should be clamped to the valid range.
						m_paramValues.push_back({ paramID, static_cast<int>(value) });
					}
					else  // If byte length is zero, it's either a reserved address or not the start of a field. So just move on.
					{
						offset++;
					}
				}

				// If m_frameIsActive is false then either we've received a frame end message, or else 
				// we receive a block outside of a frame (which is not something Solarium currently asks for).
				// In either case we consider this a "receive preset" command.
				if (!m_frameIsActive && m_paramValues.size() > 0)
				{
					auto cmd = std::make_unique<CmdSetPreset>(std::move(m_paramValues));
					cmd->SetActionText("Receive Preset");
					bool syncSave = m_syncOn;
					m_syncOn = false;
					m_pCommandHistory->AddCommandAndExecute(std::move(cmd));
					m_syncOn = syncSave;
					m_paramValues.clear();
				}

			}
			else
			{
				// error
				assert(false);
			}
		}
	}
}

void MidiManager::HandleStateChangedOnMessageThread(PresetState stateType, bool enabled)
{
	m_pPreset->SetState(stateType, enabled);
}


/** @brief	Receives incoming single parameter changes on the GUI thread.

*/
void MidiManager::HandleSysExParamOnMessageThread(ParamAddress paramAddress, int32_t value)
{
	ParamID paramID;
	bool valid = false;
	// Find paramID from Address
	if (paramAddress.hi == 0x16)	// key tables
	{
		// handle key tables later

		paramID.elemType = ElementType::KeyTable;
		paramID.elemIndex = (paramAddress.mid >> 4) / 2;
		paramID.portType = PortType::None;
		paramID.portIndex = 0;
		paramID.subIndex = ((paramAddress.mid >> 4) & 1) ? paramAddress.lo/2 + 64 : paramAddress.lo/2;
		valid = true;
	}

	else
	{
		BlockInfo bi = GetMIDIBlockInfo(paramAddress);
		assert(bi.IsValid());
		if (bi.IsValid())
		{
			int byteLength = bi.pByteCountTable[paramAddress.lo];
			if (byteLength > 0)
			{
				paramID = bi.pParamIDTable[paramAddress.lo];
				valid = true;
			}
		}
	}

	assert(valid);

	if (m_presetReceiveInProgress)
	{
		m_paramValues.push_back({ paramID, value });

		m_numberOfParametersSoFar++;
		bool isDone = m_numberOfParametersSoFar == m_totalNumberOfParametersRecv;
		if (m_midiProgressCallback)
		{
			double percent = static_cast<double>(m_numberOfParametersSoFar) / m_totalNumberOfParametersRecv;
			if (!m_midiProgressCallback(percent, isDone))
			{
				CancelRequest();
			}
		}

		if (isDone)
		{
			auto cmd = std::make_unique<CmdSetPreset>(std::move(m_paramValues));
			cmd->SetActionText("Receive Preset");
			bool syncSave = m_syncOn;
			m_syncOn = false;
			m_pCommandHistory->AddCommandAndExecute(std::move(cmd));
			m_syncOn = syncSave;
			m_presetReceiveInProgress = false;
			m_paramValues.clear();
		}
	}
	else
	{
		bool syncSave = m_syncOn;
		m_syncOn = false;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
		m_syncOn = syncSave;
	}

}


SendThread::SendThread(MidiManager* pMidiManager, std::vector<std::vector<uint8_t>>&& presetBlocksToSend)
	:Thread("SendThread"), m_pMidiManager(pMidiManager), m_presetBlocksToSend(presetBlocksToSend)
{}

void SendThread::run()
{
	int numberOfBlocksSoFar = 0;
	for (auto& sp : m_presetBlocksToSend)
	{
		m_pMidiManager->SendSysexData(&sp[0], sp.size());

		// Post progress to the GUI thread
		(new MidiManager::SendPresetCallbackMessage(m_pMidiManager, numberOfBlocksSoFar++, false))->post();
		
		if (threadShouldExit())
		{
			break;
		}

		//sleep(100);
	}
	m_presetBlocksToSend.clear();

	(new MidiManager::SendPresetCallbackMessage(m_pMidiManager, numberOfBlocksSoFar, true))->post();

}



std::vector<ParamAddress> baseAddresses
{
	{ 0x7E, 0x7F, 0x7F },	// Frame Start
	{ 0x10, 0x00, 0x00 },	// Osc 1
	{ 0x10, 0x10, 0x00 },	// Osc 2
	{ 0x10, 0x20, 0x00 },	// Osc 3
	{ 0x10, 0x30, 0x00 },	// Osc 4
	{ 0x10, 0x40, 0x00 },	// Rot 1
	{ 0x10, 0x50, 0x00 },	// Rot 2
	{ 0x10, 0x60, 0x00 },	// VecAM
	{ 0x10, 0x70, 0x00 },	// VCA
	{ 0x11, 0x00, 0x00 },	// Filter 1				(10)
	{ 0x11, 0x10, 0x00 },	// Filter 2
	{ 0x11, 0x20, 0x00 },	// Filter 3
	{ 0x11, 0x30, 0x00 },	// Filter 4
	{ 0x11, 0x40, 0x00 },	// Mix
	{ 0x11, 0x50, 0x00 },	// Insert
	{ 0x12, 0x00, 0x00 },	// LFO 1
	{ 0x12, 0x10, 0x00 },	// LFO 2
	{ 0x12, 0x20, 0x00 },	// LFO 3
	{ 0x12, 0x30, 0x00 },	// LFO 4
	{ 0x12, 0x40, 0x00 },	// LFO 5				(20)
	{ 0x13, 0x00, 0x00 },	// Env 1
	{ 0x13, 0x10, 0x00 },	// Env 2
	{ 0x13, 0x20, 0x00 },	// Env 3
	{ 0x13, 0x30, 0x00 },	// Env 4
	{ 0x13, 0x40, 0x00 },	// Env 5				(25)
	{ 0x13, 0x50, 0x00 },	// Env 6 (VCA)
	{ 0x13, 0x70, 0x00 },	// Looping Env			(27)
	{ 0x14, 0x00, 0x00 },	// Sequencer Steps		(28)
	{ 0x14, 0x10, 0x00 },	// Sequencer Parameters
	{ 0x15, 0x00, 0x00 },	// Effects				(30)
	{ 0x16, 0x00, 0x00 },	// KeyTable 1A
	{ 0x16, 0x10, 0x00 },	// KeyTable 1B
	{ 0x16, 0x20, 0x00 },	// KeyTable 2A
	{ 0x16, 0x30, 0x00 },	// KeyTable 2B
	{ 0x16, 0x40, 0x00 },	// KeyTable 3A
	{ 0x16, 0x50, 0x00 },	// KeyTable 3B
	{ 0x16, 0x60, 0x00 },	// KeyTable 4A
	{ 0x16, 0x70, 0x00 },	// KeyTable 4B
	{ 0x17, 0x10, 0x00 },	// Common
	{ 0x18, 0x00, 0x00 },	// Arp Steps
	{ 0x18, 0x10, 0x00 },	// Arp Parameters
	{ 0x7F, 0x7F, 0x7F },	// Frame End
};


void MidiManager::DoReceiveSystem()
{
	ParamAddress baseAddress(0x01, 0x00, 0x00);
	SendBlockRequest(baseAddress);
	m_totalNumberOfBlocksToRecv = 1;
	m_pSysExParamTimer->startTimer(SYSEX_PARAMETER_TIMEOUT);

}

void MidiManager::DoReceivePreset(MidiProgressCallback progressCallback)
{
	ParamAddress baseAddress(0x7E, 0x7F, 0x7F);
	SendBlockRequest(baseAddress);
	m_totalNumberOfBlocksToRecv = baseAddresses.size();
	m_midiProgressCallback = progressCallback;

	m_pSysExParamTimer->startTimer(SYSEX_PARAMETER_TIMEOUT);
}


void MidiManager::DoSendSystem()
{
	m_presetBlocksToSend.resize(1);
	BuildBulkDumpBlock(ParamAddress(0x01, 0x00, 0x00), m_presetBlocksToSend[0]);
	m_totalNumberOfBlocksToSend = m_presetBlocksToSend.size();
	m_pSendThread = std::make_unique<SendThread>(this, std::move(m_presetBlocksToSend));
	m_pSendThread->startThread();
}

void MidiManager::DoSendPreset(MidiProgressCallback progressCallback)
{
	m_midiProgressCallback = progressCallback;
	m_midiProgressCallback(0.0, false);

	m_presetBlocksToSend.resize(baseAddresses.size());
	for (size_t i = 0; i < baseAddresses.size(); ++i)
	{
		BuildBulkDumpBlock(baseAddresses[i], m_presetBlocksToSend[i]);
	}
	m_totalNumberOfBlocksToSend = m_presetBlocksToSend.size();

	m_pSendThread = std::make_unique<SendThread>(this, std::move(m_presetBlocksToSend));
	m_pSendThread->startThread();

}

void MidiManager::SendState(PresetState stateType, bool enabled)
{
	const uint8_t data[] =
	{
		0x00, 0x12, 0x34,			// manufacturer ID
		m_deviceID,					// Device ID
		0x10,						// Solaris ID
		SYSEX_STATE_CHANGED,		// function
		static_cast<uint8_t>((stateType == PresetState::Compare) ? 0x01 : 0x02),	// Compare or FXBypass
		static_cast<uint8_t>(enabled ? 1 : 0)				// value
	};

	SendSysexData(data, sizeof(data));
}


/**	@brief	Writes all the fields of an outgoing MIDI dump block. This writes just the data and not any of the header or checksum.

	@param[in] blockInfo		A reference to the BlockInfo which specifies which block is to be written.
	@param[in] pBlock			A pointer to the start of the data block where the fields should be written.
								Must point to memory able to hold the data block expected.
	@returns					The number of bytes written, which depends on the particular block that's written.
*/
int MidiManager::WriteParameterBlock(BlockInfo& blockInfo, int8_t* pBlock)
{
	int index = 0;
	while (index < blockInfo.blockSize)
	{
		int byteLength = blockInfo.pByteCountTable[index];
		if (byteLength == 0)
		{
			// zero byte count means a reserved space. Just set it to zero and continue.
			pBlock[index++] = 0;
		}
		else
		{
			ParamID paramID = blockInfo.pParamIDTable[index];
			int32_t value = m_pPreset->GetParameterValue(paramID);

			if (byteLength == 1)
			{
				pBlock[index++] = value & 0x7F;
			}
			else if (byteLength == 2)
			{
				pBlock[index++] = (value >> 7) & 0x7F;
				pBlock[index++] = value & 0x7F;
			}
			else if (byteLength == 3)
			{
				pBlock[index++] = (value >> 14) & 0x7F;
				pBlock[index++] = (value >> 7) & 0x7F;
				pBlock[index++] = value & 0x7F;
			}
		}
	}

	return index;
}


void MidiManager::BuildBulkDumpBlock(ParamAddress baseAddress, std::vector<uint8_t>& blockOut)
{
	BlockInfo blockInfo = GetMIDIBlockInfo(baseAddress);

	// If the block info is valid it means there are tables that we can use in a 
	// common way to build the bulk data.
	if (blockInfo.IsValid())
	{
		int totalBlockSize = blockInfo.blockSize + 10;
		blockOut.resize(totalBlockSize);

		blockOut[0] = 0x00;			// manufacturer ID
		blockOut[1] = 0x12;			// manufacturer ID
		blockOut[2] = 0x34;			// manufacturer ID
		blockOut[3] = 0x00;			// Device ID (MIDI channel set below)
		blockOut[4] = 0x10;			// Solaris ID
		blockOut[5] = 0x11;			// bulk dump function

		blockOut[6] = baseAddress.hi;
		blockOut[7] = baseAddress.mid;
		blockOut[8] = baseAddress.lo;

		int dataBytesWritten = WriteParameterBlock(blockInfo, (int8_t*)&blockOut[9]);
		assert(dataBytesWritten + 10 == totalBlockSize);

		// Calculate checksum. This sums the address, all the data bytes, and the checksum itself. The checksum is the value that causes 
		// that sum have the lowest 7 bits as zero.
		uint32_t sum = 0;
		// The ending condition adds 3 for the address and 6 for the header
		for (int i = 6; i < dataBytesWritten + 9; ++i)
		{
			sum += blockOut[i];
		}

		blockOut[dataBytesWritten + 9] = (0x80 - (sum & 0x7F)) & 0x7F;	// checksum
	}
}


void MidiManager::SendSysexData(const uint8_t *pSysexData, int dataSizeInBytes)
{
	MidiMessage msg = MidiMessage::createSysExMessage(pSysexData, dataSizeInBytes);

	if (m_pMidiOutDevice)
	{
		m_pMidiOutDevice->sendMessageNow(msg);
	}
}

void MidiManager::SendParameter(ParamAddress paramAddress, int value)
{
	const uint8_t data[] =
	{
		0x00, 0x12, 0x34,			// manufacturer ID
		m_deviceID,					// Device ID
		0x10,						// Solaris ID
		SYSEX_PARAMETER_CHANGED,	// function
		paramAddress.hi,
		paramAddress.mid,
		paramAddress.lo,
		uint8_t((value >> 14) & 0x7F),
		uint8_t((value >> 7) & 0x7F),
		uint8_t(value & 0x7F)
	};

	SendSysexData(data, sizeof(data));
}

void MidiManager::CancelRequest()
{
	if (m_pSysExParamTimer)
	{
		m_pSysExParamTimer->stopTimer();
	}

	if (m_midiProgressCallback != nullptr)
	{
		m_midiProgressCallback(1.0, true);
		m_midiProgressCallback = nullptr;
	}
}


// Called by the SysExParamTimer when the time expires before being reset, which means that 
// the Solaris hasn't responded to a request in the alloted time.
void MidiManager::OnMidiRequestTimeout()
{
	CancelRequest();
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"Solarium",
		"Could not communicate with the Solaris. Please check the MIDI connection.",
		"OK");
}


void MidiManager::HandleParameterChanged(const ParamID & paramID, int32_t value, bool isMuted)
{
	if (m_syncOn)
	{
		ParamAddress paramAddress = GetSysExAddressFromParamID(paramID);
		SendParameter(paramAddress, isMuted ? 0 : value);
	}
}

