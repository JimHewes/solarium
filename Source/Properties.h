/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PROPERTIES_H_INCLUDED
#define PROPERTIES_H_INCLUDED

#include <memory>
#include "../JuceLibraryCode/JuceHeader.h"

#if !defined(IN_MAINCOMPONENT_CPP)
#define EXTERN extern
#else
#define EXTERN
#endif

EXTERN std::unique_ptr<PropertiesFile>	pPropertiesFile;

const juce::String propMainWindowWidth = "mainwindowwidth";
const juce::String propMainWindowHeight = "mainwindowheight";
const juce::String propZoom = "zoom";
const juce::String propMidiInDeviceName = "midiindevicename";
const juce::String propMidiInDeviceID = "midiindeviceid";
const juce::String propMidiOutDeviceName = "midioutdevicename";
const juce::String propMidiOutDeviceID = "midioutdeviceid";
const juce::String propDeviceID = "deviceid";
const juce::String propMidiSync = "midisync";
const juce::String propSliderReset = "sliderreset";
const juce::String propSliderEdit = "slideredit";
const juce::String propLastDirectory = "lastdirectory";
const juce::String propLastArpDirectory = "lastarpdirectory";
const juce::String propWireOpacity = "wireopacity";
const juce::String propWireThreshold = "wirethreshold";
const juce::String propArpWindowWidth = "arpwindowwidth";
const juce::String propArpWindowHeight = "arpwindowheight";
const juce::String propArpWindowTop = "arpwindowtop";
const juce::String propArpWindowLeft = "arpwindowleft";
const juce::String propHistoryWindowWidth = "historywindowwidth";
const juce::String propHistoryWindowHeight = "historywindowheight";
const juce::String propHistoryWindowTop = "historywindowtop";
const juce::String propHistoryWindowLeft = "historywindowleft";
const juce::String propHistoryWindowCol1Width = "historywindowcol1width";
const juce::String propHistoryWindowCol2Width = "historywindowcol2width";
const juce::String propHistoryWindowCol3Width = "historywindowcol3width";
const juce::String propHistoryWindowCol4Width = "historywindowcol4width";
const juce::String propHistoryWindowCol5Width = "historywindowcol5width";
const juce::String propHistoryWindowCol6Width = "historywindowcol6width";
const juce::String propHistoryWindowCol1Visible = "historywindowcol1visible";
const juce::String propHistoryWindowCol2Visible = "historywindowcol2visible";
const juce::String propHistoryWindowCol3Visible = "historywindowcol3visible";
const juce::String propHistoryWindowCol4Visible = "historywindowcol4visible";
const juce::String propHistoryWindowCol5Visible = "historywindowcol5visible";
const juce::String propHistoryWindowCol6Visible = "historywindowcol6visible";
#endif  // PROPERTIES_H_INCLUDED
