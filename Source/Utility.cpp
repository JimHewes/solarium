/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "Utility.h"
#include <cassert>
#include <cmath>	// needed for Mac

using std::vector;
using std::string;
namespace util {

	/**	@brief	Returns a string array that contains the substrings of a given string that are delimited by characters of a specified separator string.

	This function was inspired by the .NET function String.Split. (http://msdn.microsoft.com/en-us/library/b873y76a.aspx)

	@param[in] stringToSplit		The string that will be split into substrings.
	@param[in] separators			A string of one or more characters. Any of these characters will be considered a delimiter for dividing the stringToSplit.
	@param[in] removeEmptyEntries	Omit empty array elements from the array returned.

	@returns An array of strings that are the substrings of stringToSplit after the split.

	*/
	std::vector<std::string> SplitString(const std::string stringToSplit,const std::string separators,bool removeEmptyEntries)
	{
		std::vector<std::string> parts;
		std::string::size_type startIndex = 0;
		std::string::size_type index;

		do
		{
			index = stringToSplit.find_first_of(separators,startIndex);

			if (index == std::string::npos)
			{
				std::string tmp = stringToSplit.substr(startIndex);
				if (!removeEmptyEntries || !tmp.empty())
				{
					parts.push_back(tmp);
				}
			}
			else
			{
				std::string tmp = stringToSplit.substr(startIndex,index - startIndex);
				if (!removeEmptyEntries || !tmp.empty())
				{
					parts.push_back(tmp);
				}
			}
			startIndex = index + 1;

		} while (index != std::string::npos);

		return parts;

	}

	/** @brief	Determines whether one string starts with another string.

		This function looks to see if searchString begins with startString.

		@param searchString		The string to search to see if it starts with startString.
		@param startString		The starting string.
		@returns				true if searchString starts with startString.
		*/
	bool StartsWith(const std::string& searchString,const std::string& startString)
	{
		return (0 == searchString.compare(0,startString.length(),startString));
	}


	/** @brief	Determines whether one string ends with another string.

		This function looks to see if searchString ends with endString.

		@param searchString		The string to search to see if it starts with startString.
		@param endString		The starting string.
		@returns				true if searchString starts with startString.
		*/
	bool EndsWith(const std::string& searchString,const std::string& endString)
	{
		assert(searchString.length() >= (int)endString.length());
		bool result = false;
		if (searchString.length() >= (int)endString.length())
		{
			int startIndex = searchString.length() - endString.length();
			result = (0 == searchString.compare(startIndex,endString.length(),endString));
		}
		return result;
	}

	static char encoding_table[] = { 'A','B','C','D','E','F','G','H',
		'I','J','K','L','M','N','O','P',
		'Q','R','S','T','U','V','W','X',
		'Y','Z','a','b','c','d','e','f',
		'g','h','i','j','k','l','m','n',
		'o','p','q','r','s','t','u','v',
		'w','x','y','z','0','1','2','3',
		'4','5','6','7','8','9','+','/' };
	static int mod_table[] = { 0,2,1 };


	bool base64_encode(const std::vector<uint8_t>& input,std::string& output)
	{
		size_t outputLength = (size_t)(4.0 * ceil((double)input.size() / 3.0));

		std::vector<char> encodedData;
		encodedData.resize(outputLength + 1);

		for (size_t i = 0,j = 0; i < input.size();)
		{
			uint32_t octet_a = i < input.size() ? input[i++] : 0;
			uint32_t octet_b = i < input.size() ? input[i++] : 0;
			uint32_t octet_c = i < input.size() ? input[i++] : 0;

			uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

			encodedData[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
			encodedData[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
			encodedData[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
			encodedData[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
		}

		for (int i = 0; i < mod_table[input.size() % 3]; i++)
		{
			encodedData[outputLength - 1 - i] = '=';
		}

		encodedData[encodedData.size() - 1] = 0;	// zero terminator
		output = &encodedData[0];

		return true;
	}

	bool base64_decode(const std::string& input,std::vector<uint8_t>& output)
	{
		char decoding_table[256];
		for (int i = 0; i < 0x40; i++)
		{
			decoding_table[encoding_table[i]] = (char)i;
		}

		if (input.length() % 4 != 0)
		{
			assert(false);
			return false;
		}

		size_t outputLength = input.length() / 4 * 3;
		if (input[input.length() - 1] == '=')
		{
			outputLength--;
		}
		if (input[input.length() - 2] == '=')
		{
			outputLength--;
		}

		output.resize(outputLength);

		for (size_t i = 0,j = 0; i < input.length();)
		{
			uint32_t sextet_a = input[i] == '=' ? 0 & i++ : decoding_table[input[i++]];
			uint32_t sextet_b = input[i] == '=' ? 0 & i++ : decoding_table[input[i++]];
			uint32_t sextet_c = input[i] == '=' ? 0 & i++ : decoding_table[input[i++]];
			uint32_t sextet_d = input[i] == '=' ? 0 & i++ : decoding_table[input[i++]];

			uint32_t triple = (sextet_a << 3 * 6)
				+ (sextet_b << 2 * 6)
				+ (sextet_c << 1 * 6)
				+ (sextet_d << 0 * 6);

			if (j < outputLength)
				output[j++] = (triple >> 2 * 8) & 0xFF;
			if (j < outputLength)
				output[j++] = (triple >> 1 * 8) & 0xFF;
			if (j < outputLength)
				output[j++] = (triple >> 0 * 8) & 0xFF;
		}

		return true;
	}

} // namespace util
