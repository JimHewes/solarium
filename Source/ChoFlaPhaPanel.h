/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef CHOFLAPHAPANEL_H_INCLUDED
#define CHOFLAPHAPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
#include "Slider2.h"

using SliderFreq50Hz = SliderFreq500Hz;
class CommandHistory;

class ChoFlaPhaPanel :
	public Component,
	public Slider2::Listener,
	public Button::Listener
{
public:
	ChoFlaPhaPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID, int slot);
	virtual ~ChoFlaPhaPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_subtitle;
	ToggleButton	m_modeButton;

	Label			m_frequencyLabel;
	SliderFreq50Hz	m_frequencySlider;
	Label			m_depthLabel;
	SliderProp		m_depthSlider;
	Label			m_phaseLabel;
	SliderProp		m_phaseSlider;

	Label			m_offsetLabel;
	SliderProp		m_choFlaOffsetSlider;
	SliderFreq20KHz	m_phaOffsetSlider;
	SliderProp*		m_pOffsetSlider;

	Label			m_inLevelLabel;
	SliderProp		m_inLevelSlider;
	Label			m_feedbackLabel;
	SliderProp		m_feedbackSlider;
	Label			m_dryLabel;
	SliderProp		m_drySlider;
	Label			m_wetLabel;
	SliderProp		m_wetSlider;
	
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};



#endif  // CHOFLAPHAPANEL_H_INCLUDED
