/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "EGPanel.h"
#include <vector>
#include <sstream>
#include <iomanip>
#include <cassert>
#include "Command.h"

using std::vector;


String SlopeSlider::getTextFromValue(double value)
{
	if (value > 126.5)
	{
		return "exp";
	}
	else if (value < 0.5)
	{
		return "lin";
	}
	return Slider2::getTextFromValue(value);
}


String SliderSlopeSustain::getTextFromValue(double value)
{
	assert(value >= -20000 && value <= 20000);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(1);
	if (value == 0)
	{
		os << "Off";
	}
	else if (value > -1000 && value < 1000)
	{
		os << value << " ms";
	}
	else
	{
		os << value / 1000 << " sec";
	}
	return os.str();
}

static const juce::Identifier paramIndexID = "paramIndex";

EGPanel::EGPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	, m_pCommandHistory(pCommandHistory)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_delayLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_delayLabel.setText("Delay:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_delayLabel);
	m_delaySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_delaySlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egDelay));
	m_delaySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_delaySlider.getProperties().set(paramIndexID, egDelay);
	m_delaySlider.addListener(this);
	addAndMakeVisible(m_delaySlider);
	
	yPos += rowHeight;
	m_attackLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_attackLabel.setText("Attack:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_attackLabel);
	m_attackSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_attackSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egAttack));
	m_attackSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_attackSlider.getProperties().set(paramIndexID, egAttack);
	m_attackSlider.addListener(this);
	addAndMakeVisible(m_attackSlider);

	yPos += rowHeight;
	m_decayLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_decayLabel.setText("Decay:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_decayLabel);
	m_decaySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_decaySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egDecay));
	m_decaySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_decaySlider.getProperties().set(paramIndexID, egDecay);
	m_decaySlider.addListener(this);
	addAndMakeVisible(m_decaySlider);

	yPos += rowHeight;
	m_sustainLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_sustainLabel.setText("Sustain:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_sustainLabel);
	m_sustainSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_sustainSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egSustain));
	m_sustainSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_sustainSlider.getProperties().set(paramIndexID, egSustain);
	m_sustainSlider.addListener(this);
	addAndMakeVisible(m_sustainSlider);

	yPos += rowHeight;
	m_releaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_releaseLabel.setText("Release:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_releaseLabel);
	m_releaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_releaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egRelease));
	m_releaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_releaseSlider.getProperties().set(paramIndexID, egRelease);
	m_releaseSlider.addListener(this);
	addAndMakeVisible(m_releaseSlider);



	yPos += rowHeight;
	m_aSlopeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_aSlopeLabel.setText("A-Slope:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_aSlopeLabel);
	m_aSlopeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_aSlopeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egASlope));
	m_aSlopeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_aSlopeSlider.getProperties().set(paramIndexID, egASlope);
	m_aSlopeSlider.addListener(this);
	addAndMakeVisible(m_aSlopeSlider);

	yPos += rowHeight;
	m_dSlopeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_dSlopeLabel.setText("D-Slope:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_dSlopeLabel);
	m_dSlopeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_dSlopeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egDSlope));
	m_dSlopeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_dSlopeSlider.getProperties().set(paramIndexID, egDSlope);
	m_dSlopeSlider.addListener(this);
	addAndMakeVisible(m_dSlopeSlider);

	yPos += rowHeight;
	m_sSlopeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_sSlopeLabel.setText("S-Slope:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_sSlopeLabel);
	m_sSlopeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_sSlopeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egSSlope));
	m_sSlopeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_sSlopeSlider.getProperties().set(paramIndexID, egSSlope);
	m_sSlopeSlider.addListener(this);
	addAndMakeVisible(m_sSlopeSlider);

	yPos += rowHeight;
	m_rSlopeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_rSlopeLabel.setText("R-Slope:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_rSlopeLabel);
	m_rSlopeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_rSlopeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egRSlope));
	m_rSlopeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_rSlopeSlider.getProperties().set(paramIndexID, egRSlope);
	m_rSlopeSlider.addListener(this);
	addAndMakeVisible(m_rSlopeSlider);

	yPos += rowHeight;
	m_velocityLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_velocityLabel.setText("Velocity:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_velocityLabel);
	m_velocitySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_velocitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,egVelocity));
	m_velocitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_velocitySlider.getProperties().set(paramIndexID, egVelocity);
	m_velocitySlider.addListener(this);
	addAndMakeVisible(m_velocitySlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	Update();
}

EGPanel::~EGPanel()
{
	m_delaySlider.removeListener(this);
	m_attackSlider.removeListener(this);
	m_decaySlider.removeListener(this);
	m_sustainSlider.removeListener(this);
	m_releaseSlider.removeListener(this);
	m_aSlopeSlider.removeListener(this);
	m_dSlopeSlider.removeListener(this);
	m_sSlopeSlider.removeListener(this);
	m_rSlopeSlider.removeListener(this);
	m_velocitySlider.removeListener(this);
}

void EGPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void EGPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_delaySlider) ||
		(slider == &m_attackSlider) ||
		(slider == &m_decaySlider) ||
		(slider == &m_sustainSlider) ||
		(slider == &m_releaseSlider) ||
		(slider == &m_aSlopeSlider) ||
		(slider == &m_dSlopeSlider) ||
		(slider == &m_sSlopeSlider) ||
		(slider == &m_rSlopeSlider) ||
		(slider == &m_velocitySlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void EGPanel::Update()
{
	m_delaySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egDelay)), NotificationType::dontSendNotification);
	m_attackSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egAttack)), NotificationType::dontSendNotification);
	m_decaySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egDecay)), NotificationType::dontSendNotification);
	m_sustainSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egSustain)), NotificationType::dontSendNotification);
	m_releaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egRelease)), NotificationType::dontSendNotification);
	m_aSlopeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egASlope)), NotificationType::dontSendNotification);
	m_dSlopeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egDSlope)), NotificationType::dontSendNotification);
	m_sSlopeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egSSlope)), NotificationType::dontSendNotification);
	m_rSlopeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egRSlope)), NotificationType::dontSendNotification);
	m_velocitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,egVelocity)), NotificationType::dontSendNotification);
}
