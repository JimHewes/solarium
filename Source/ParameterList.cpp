/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <algorithm>
#include <iostream>
#include "gsl_util.h"
#include "ParameterList.h"
using gsl::narrow;


#pragma region OscillatorTables
const EnumListType oscTypeNames = { "Off","MM1","WT","CEM","WAV","VS","Mini" };
const EnumListType offWaveNames = { "None" };
const EnumListType MM1WaveNames = { "Sine","Tri","Ramp","Saw","Pulse","Noise","S/H","MorphSaw","MorphSquare","Jaws" };
const EnumListType CEMWaveNames = { "Off", "Saw","Tri","Pulse","Saw+Tri","Saw+Pulse","Tri+Pulse","S+T+P" };
const EnumListType WAVWaveNames = { "User" };
const EnumListType WTWaveNames =
{
	//	1				2				3					4				5				6				7				8				9				0			
	"1 Resonant",	"2 Resonant2",	"3 MalletSyn",		"4 Sqr-Sweep",	"5 Bellish",	"6 Pul-Sweep",	"7 Saw-Sweep",	"8 MellowSaw",	"9 Feedback",	"10 Add Harm",
	"11 Reso 3 HP",	"12 Wind Syn",	"13 Hight Harm",	"14 Clipper",	"15 Organ Syn",	"16 Square Saw","17 Formant 1",	"18 Polated",	"19 Transient",	"20 ElectricP",
	"21 Robotic",	"22 StrongHrm",	"23 PercOrgan",		"24 ClipSweep",	"25 ResoHarms",	"26 2 Echos",	"27 Formant 2",	"28 FmntVocal",	"29 MicroSync",	"30 Micro PWM",
	"31 Glassy",	"32 Square HP",	"33 SawSync 1",		"34 SawSync 2",	"35 SawSync 3",	"36 PulSync 1",	"37 PulSync 2",	"38 PulSync 3",	"39 SinSync 1",	"40 SinSync 2",
	"41 SinSync 3",	"42 PWM Pulse",	"43 PWM Saw",		"44 Fuzz Wave",	"45 Distorted",	"46 HeavyFuzz",	"47 Fuzz Sync",	"48 K+Strong1",	"49 K+Strong2",	"50 K+Strong3",
	"51 1-2-3-4-5",	"52 19/twenty",	"53 Wavetrip1",		"54 Wavetrip2",	"55 Wavetrip3",	"56 Wavetrip4",	"57 MaleVoice",	"58 Low Piano",	"59 ResoSweep",	"60 Xmas Bell",
	"61 FM Piano",	"62 Fat Organ",	"63 Vibes",			"64 Chorus 2"
};
const EnumListType VSWaveNames =
{
	//	1				2					3					4				5					6				7				8				9				0			
	"SineWave","Sawtooth","Square","Warm Bell","Random Bell","Random Bell2","Warm Bell 2","Formant Bell","Fuzzy Reed","Formant Aoh",
	"Formant Ahh","TriPlus","Dissonant Bell","Pulse 1","Pulse 2","Square Reed","Oohh","Eehh","Feedback","Piano 1",
	"E. Piano","Medium Harmonic","Hi Top","Warm Reed","3rd & 5th Harmonic","Hollow","Heavy 7","Bell Organ","Bass Bell","Tine 1",
	"Phase Square","Orient","High Pipe","Mass Organ","Reed Organ","Organ Ahh","Mellow Organ","Formant Organ","Clarinet","Ahh Female",
	"Ahh Homme","Ahh Bass","Reg Vox","Vocal 1","Vocal 2","High Ahh","Bass","Guitar","Nice","Woodwind",
	"Oboe","Harp","Pipe","Hack 1","Hack 2","Hack 3","Pinched 1","Bell Harmonic","Bell Vox","High Harmonic 1",
	"High Reed","Bell Reed","Warm Whistle","Wood","Pure","Medium Pure","High Harmonic 2","Full Bell","Bell 1","Pinched 2",
	"Cluster","Medium Pinched","Vox Pinched","Organ Pinched","Ahh Pinched","Piano Organ","Bright Reed","No Fundamental","Reed Harmonic","Light Fundamental",
	"Mello Organ","Bell 2","Bell 3","Saw 3rd & 5th","Sine 5ths","Sine 2 Octaves","Sine 4 Octaves","Saw 5thw","Saw 2 Octaves","Square 5ths",
	"Square Octave & 5th","Square 2 Octaves","Warm Low","Bells"
};
const EnumListType MiniWaveNames = { "Tri","Saw+Tri","Saw","Pulse1","Pulse2","Pulse3" };
#pragma endregion OscillatorTables


const EnumListType rotPhaseSyncNames = { "Off","Gate" };
const EnumListType amAlgorithmNames = { "Shift","Clip","Abs","Ring" };

#pragma region FilterTables
const EnumListType filterTypeNames = { "BYPASS", "MM1","SSM","Mini","Obie","Comb","Vocal" };
const EnumListType BypassModeNames = { "---" };
const EnumListType MM1ModeNames = { "LP4","LP3","LP2","LP1","HP4","HP3","HP2","HP1","BP4","BP2","BP2+LP1","BP2+LP2","BP2+HP1", "BP2+HP2",
												"BR4","BR2","BR2+LP1","BR2+LP2","BR2+HP1","BR2+HP2","AP3","AP3+LP1","AP3+HP1"};
const EnumListType SSMModeNames = { "LP 24dB" };
const EnumListType MiniModeNames = { "LP 24dB" };
const EnumListType ObieModeNames = { "LP","HP","BP","BR" };
const EnumListType CombModeNames = { "Tube","Comb" };
const EnumListType VocalVowelNames = { "A", "E", "I", "O", "U", "Y", "AA","AE","OE","UE"};
#pragma endregion FilterTables

const EnumListType VCATypeNames = { "linear","log","sigma" };
const EnumListType LFOTypeNames = { "Sine","Tri","Ramp","Saw", "Square", "S/H" };
const EnumListType InserFXModeNames = { "Off", "Decim","Bitchop","Distort" };

const EnumListType LoopEgSegments = { "1","2","3","4","5","6","7","8" };
const EnumListType LoopEgRepeat = { "Off", "1","2","3","4","5","6","7","8", "9", "Inf" };

const EnumListType oscModDestList = { "None", "Pitch", "LinFM", "Shape" };
const EnumListType rotModDestList = { "None", "Pitch", "XFade" };
const EnumListType amModDestList = { "Carrier" };
const EnumListType vecModDestList0 = { "X axis" };
const EnumListType vecModDestList1 = {  "Y axis" };
const EnumListType fltModDestList = { "None", "Cutoff", "Res" };
const EnumListType combModDestList = { "None", "Cutoff", "Res", "Damping" };
const EnumListType vocalModDestList = { "None", "Cutoff", "Res", "XFade" };
const EnumListType insertFXModDestList = { "Value" };
const EnumListType lfoModDestList = { "None", "Rate", "Level" };
const EnumListType egAttackDestList = { "Attack" };
const EnumListType egDecayDestList = { "Decay" };
const EnumListType egSustainDestList = { "Sustain" };
const EnumListType egReleaseDestList = { "Release" };
const EnumListType loopEgModDest0 = { "Level" };
const EnumListType loopEgModDest1 = { "Time" };

const EnumListType levelDestList = { "Level" };
const EnumListType panDestList = { "Pan" };
const EnumListType outputDestList = { "Output" };

const EnumListType seqModeNames = { "Normal","No Reset","No Gate","NG/NR","Key Step" };
const EnumListType clockDivisionNames = {	"1/32","1/16","1/16T",
											"1/8","1/8T","1/4",
											"1/4T","1/2","1/2T",
											"1/1","2/1","3/1",
											"4/1","6/1","8/1" };
const EnumListType effectNames = { "Off","ChorusFlanger","Phaser","Delay","EQ" };
const EnumListType DelayModeNames = { "Bypass","Delay","XDelay" };
const EnumListType delayClockDivisionNames = { "1/128","1/64T","1/64",
												"1/32T","1/32","1/32D",
												"1/16T","1/16","1/16D",
												"1/8T","1/8","1/8D",
												"1/4T","1/4","1/4D",
												"1/2T","1/2","1/2D",
												"1/1","2/1","3/1","4/1","5/1","6/1","7/1","8/1" };

const EnumListType ArpModeNames = { "Up", "Down", "Up/Down", "AsPlayed", "Random", "Chord", "Down2", "Up/Down2"};
const EnumListType ArpOctaveNames = { "1", "2", "3", "4" };
const EnumListType ArpVelocityNames = { "Pattern", "Keyboard", "Both" };

const EnumListType GlideTypeNames = { "Off","Porta","Gliss", "FingPort","FingGlis" };
const EnumListType GlideModeNames = { "C-Time","C-Rate","Exp" };
const EnumListType PlayModeNames = { "Poly","Mono" };
const EnumListType LegatoNames = { "Off","Reassign", "Retrig." };
const EnumListType UniVoiceNames = { "2","3","4","5","6","7","8","All","Chord" };
const EnumListType NotePriNames = { "Last","Low", "High" };
const EnumListType EgResetNames = { "Shutdown","Running" };
const EnumListType EffectInputNames = { "Off","Synth", "Ext-1/2", "Ext-3/4", "S/P-DIF", "Cho/Fla", "Phaser", "Delay", "EQ" };
const EnumListType AssignNames = { "GloGlide", "Glide 01", "Glide 02", "Glide 03", "Glide 04", "--", "--", "GlideAll", "RibHold", "ArpTran" };
const EnumListType AssignModeNames = { "Toggle", "Moment." };
const EnumListType ExpPedalNames = { "Off","Expr", "Pan" };
const EnumListType SusPedalNames = { "Off","Sosten.", "Sustain", "Rib Hold", "Seq On", "Arp On", "Arp Hold", "Arp Trans" };
const EnumListType Category1Names = { "-","Arpeggio", "Bass", "Drum", "Effect", "Keyboard", "Lead", "Pad", "Sequence", "Texture" , "Atmosphere", "Bells", "Mono",
				"Noise", "Organ", "Percussive", "Strings", "Synth", "Vocal", "User 1", "User 2" , "User 3" , "User 4" , "User 5" , "User 6" , "User 7" , "User 8" , 
				"User 9" , "User 10" };
const EnumListType Category2Names = { "-","Acoustic", "Aggressive", "Big", "Bright", "Chord", "Classic", "Dark", "Electric", "Moody", "Soft", "Short", "Synthetic", 
				"Upbeat", "Metallic", "Template", "User 1", "User 2" , "User 3" , "User 4" , "User 5" , "User 6" , "User 7" , "User 8" , "User 9" , "User 10" };
const EnumListType PedalPolarityNames = { "+","-", "Off" };
const EnumListType ExpPedalOverrideNames = { "Off","Expr", "Pan", "Preset" };
const EnumListType SusPedalOverrideNames = { "Off","Sosten.", "Sustain", "Rib Hold", "Seq On", "Arp On", "Arp Hold", "Arp Trans", "Preset" };
const EnumListType MidiClockSourceNames = { "Int","Ext", "Send", "Auto" };
const EnumListType RemapATNames = { "Off","AT->PlyAT", "PlyAT->AT" };


// Used to quickly convert audio source indexes into ElementIDs
const SourceListType audioSourceList
{
	{{ ElementType::Off,0 }, 0},
	{{ ElementType::Oscillator,0 }, 0 },
	{{ ElementType::Oscillator,1 }, 0 },
	{{ ElementType::Oscillator,2 }, 0 },
	{{ ElementType::Oscillator,3 }, 0 },
	{{ ElementType::Rotor,0 }, 0 },
	{{ ElementType::Rotor,1 }, 0 },
	{{ ElementType::AM,0 }, 0 },
	{{ ElementType::AM,1 }, 0 },
	{{ ElementType::Vector,0 }, 0 },
	{{ ElementType::Vector,1 }, 0 },		// 10
	{{ ElementType::Mixer,0 }, 0 },			// 11
	{{ ElementType::Mixer,1 }, 0 },
	{{ ElementType::Mixer,2 }, 0 },
	{{ ElementType::Mixer,3 }, 0 },			// 14
	{{ ElementType::Filter,0 }, 0 },		// 15
	{{ ElementType::Filter,1 }, 0 },
	{{ ElementType::Filter,2 }, 0 },
	{{ ElementType::Filter,3 }, 0 },
	{{ ElementType::InsertFX,0 }, 0 },		// 19
	{{ ElementType::InsertFX,1 }, 0 },
	{{ ElementType::InsertFX,2 }, 0 },
	{{ ElementType::InsertFX,3 }, 0 },
	{{ ElementType::VCA,0 }, 0 },			// 23
	{{ ElementType::VCA,1 }, 0 },
	{{ ElementType::VCA,2 }, 0 },
	{{ ElementType::VCA,3 }, 0 },
	{{ ElementType::WhiteNoise,0 }, 0 },
	{{ ElementType::PinkNoise,0 }, 0 },
	{{ ElementType::ExtInput,0 }, 0 },		// 29
	{{ ElementType::ExtInput,1 }, 0 },		// 
	{{ ElementType::ExtInput,2 }, 0 },
	{{ ElementType::ExtInput,3 }, 0 },
	{{ ElementType::SPDIF_In,0 }, 0 },
	{{ ElementType::SPDIF_In,1 }, 0 },		// 34
	//{{ ElementType::Off,0 }, 0 },			// 35 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 36 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 37 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 38 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 39 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 40 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 41 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 42 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 43 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 44 non-existent
	{{ ElementType::LFO,0 }, 0 },			// 45
	{{ ElementType::LFO,1 }, 0 },
	{{ ElementType::LFO,2 }, 0 },
	{{ ElementType::LFO,3 }, 0 },
	{{ ElementType::VibratoLFO,0 }, 0 },
	{{ ElementType::EG,0 }, 0 },			// 50
	{{ ElementType::EG,1 }, 0 },
	{{ ElementType::EG,2 }, 0 },
	{{ ElementType::EG,3 }, 0 },
	{{ ElementType::EG,4 }, 0 },
	{{ ElementType::EG,5 }, 0 },
	{{ ElementType::LoopEG,0 }, 0 },		// 56
	{{ ElementType::LoopEG,0 }, 1 },		// 57
	//{{ ElementType::Off,0 }, 0 },			// 58 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 59 non-existent
	{{ ElementType::Velocity,0 }, 0 },		// 60
	{{ ElementType::Aftertouch,0 }, 0 },	// 61
	{{ ElementType::Note,0 }, 0 },			// 62
	{{ ElementType::ModWheel,0 }, 0 },
	{{ ElementType::AT_ModWheel,0 }, 0 },
	{{ ElementType::Ribbon,0 }, 0 },		// 65
	{{ ElementType::Ribbon,1 }, 0 },
	{{ ElementType::JoyX,0 }, 0 },
	{{ ElementType::JoyY,0 }, 0 },
	{{ ElementType::CC,0 }, 0 },
	{{ ElementType::CC,1 }, 0 },
	{{ ElementType::CC,2 }, 0 },
	{{ ElementType::CC,3 }, 0 },
	{{ ElementType::CC,4 }, 0 },
	{{ ElementType::Seq,0 }, 0 },
	{{ ElementType::Seq,1 }, 0 },
	{{ ElementType::Seq,2 }, 0 },
	{{ ElementType::Seq,3 }, 0 },
	{{ ElementType::Pedal,0 }, 0 },
	{{ ElementType::Pedal,1 }, 0 },
	{{ ElementType::AssignButton,0 }, 0 },	// 80
	{{ ElementType::AssignButton,1 }, 0 },
	{{ ElementType::EnvFol,0 }, 0 },
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::KeyTable,0 }, 0 },
	{{ ElementType::KeyTable,1 }, 0 },
	{{ ElementType::KeyTable,2 }, 0 },
	{{ ElementType::KeyTable,3 }, 0 },
	{{ ElementType::PolyAT,0 }, 0 },
	{{ ElementType::Lag,0 }, 0 },
	{{ ElementType::Lag,1 }, 0 },			// 90
	{{ ElementType::Lag,2 }, 0 },
	{{ ElementType::Lag,3 }, 0 },
	{{ ElementType::Breath,0 }, 0 },		// 93
	//{{ ElementType::Off,0 }, 0 },			// 94 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 95 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 96 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 97 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 98 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 99 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 100 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 101 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 102 non-existent
	//{{ ElementType::Off,0 }, 0 },			// 103 non-existent
	{{ ElementType::MaxValue,0 }, 0}		// 104
};

// Used to quickly convert mod source indexes into ElementIDs
const SourceListType modSourceList
{
	{ { ElementType::Off,0 }, 0 },
	{ { ElementType::LFO,0 }, 0 },
	{ { ElementType::LFO,1 }, 0 },
	{ { ElementType::LFO,2 }, 0 },
	{ { ElementType::LFO,3 }, 0 },
	{ { ElementType::VibratoLFO,0 }, 0 },
	{ { ElementType::EG,0 }, 0 },
	{ { ElementType::EG,1 }, 0 },
	{ { ElementType::EG,2 }, 0 },
	{ { ElementType::EG,3 }, 0 },
	{ { ElementType::EG,4 }, 0 },
	{ { ElementType::EG,5 }, 0 },
	{ { ElementType::LoopEG,0 }, 0 },
	{ { ElementType::LoopEG,0 }, 1 },		// 13
	//{ { ElementType::Off,0 }, 0 },			// 14 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 15 non-existent
	{ { ElementType::Velocity,0 }, 0 },		// 16
	{ { ElementType::Aftertouch,0 }, 0 },
	{ { ElementType::Note,0 }, 0 },
	{ { ElementType::ModWheel,0 }, 0 },
	{ { ElementType::AT_ModWheel,0 }, 0 },
	{ { ElementType::Ribbon,0 }, 0 },
	{ { ElementType::Ribbon,1 }, 0 },
	{ { ElementType::JoyX,0 }, 0 },
	{ { ElementType::JoyY,0 }, 0 },
	{ { ElementType::CC,0 }, 0 },
	{ { ElementType::CC,1 }, 0 },
	{ { ElementType::CC,2 }, 0 },
	{ { ElementType::CC,3 }, 0 },
	{ { ElementType::CC,4 }, 0 },
	{ { ElementType::Seq,0 }, 0 },
	{ { ElementType::Seq,1 }, 0 },
	{ { ElementType::Seq,2 }, 0 },
	{ { ElementType::Seq,3 }, 0 },
	{ { ElementType::Pedal,0 }, 0 },
	{ { ElementType::Pedal,1 }, 0 },
	{ { ElementType::AssignButton,0 }, 0 },
	{ { ElementType::AssignButton,1 }, 0 },
	{ { ElementType::EnvFol,0 }, 0 },
	{ { ElementType::Off,0 }, 0 },
	{ { ElementType::KeyTable,0 }, 0 },
	{ { ElementType::KeyTable,1 }, 0 },
	{ { ElementType::KeyTable,2 }, 0 },
	{ { ElementType::KeyTable,3 }, 0 },
	{ { ElementType::PolyAT,0 }, 0 },
	{ { ElementType::Lag,0 }, 0 },
	{ { ElementType::Lag,1 }, 0 },
	{ { ElementType::Lag,2 }, 0 },
	{ { ElementType::Lag,3 }, 0 },
	{ { ElementType::Breath,0 }, 0 },		// 49
	//{ { ElementType::Off,0 }, 0 },			// 50 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 51 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 52 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 53 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 54 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 55 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 56 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 57 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 58 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 59 non-existent
	{ { ElementType::MaxValue,0 }, 0 },		// 60
	{ { ElementType::Oscillator,0 }, 0 },
	{ { ElementType::Oscillator,1 }, 0 },
	{ { ElementType::Oscillator,2 }, 0 },
	{ { ElementType::Oscillator,3 }, 0 },
	{ { ElementType::Rotor,0 }, 0 },
	{ { ElementType::Rotor,1 }, 0 },
	{ { ElementType::AM,0 }, 0 },
	{ { ElementType::AM,1 }, 0 },
	{ { ElementType::Vector,0 }, 0 },
	{ { ElementType::Vector,1 }, 0 },
	{ { ElementType::Mixer,0 }, 0 },
	{ { ElementType::Mixer,1 }, 0 },
	{ { ElementType::Mixer,2 }, 0 },
	{ { ElementType::Mixer,3 }, 0 },
	{ { ElementType::Filter,0 }, 0 },
	{ { ElementType::Filter,1 }, 0 },
	{ { ElementType::Filter,2 }, 0 },
	{ { ElementType::Filter,3 }, 0 },
	{ { ElementType::InsertFX,0 }, 0 },
	{ { ElementType::InsertFX,1 }, 0 },
	{ { ElementType::InsertFX,2 }, 0 },
	{ { ElementType::InsertFX,3 }, 0 },
	{ { ElementType::VCA,0 }, 0 },
	{ { ElementType::VCA,1 }, 0 },
	{ { ElementType::VCA,2 }, 0 },
	{ { ElementType::VCA,3 }, 0 },
	{ { ElementType::WhiteNoise,0 }, 0 },
	{ { ElementType::PinkNoise,0 }, 0 },
	{ { ElementType::ExtInput,0 }, 0 },
	{ { ElementType::ExtInput,1 }, 0 },
	{ { ElementType::ExtInput,2 }, 0 },
	{ { ElementType::ExtInput,3 }, 0 },
	{ { ElementType::SPDIF_In,0 }, 0 },
	{ { ElementType::SPDIF_In,1 }, 0 },		// 94

	// Rather than leave these unused indexes undefined, they're included so that the modSourceList is the 
	// same length as the audioSourceList and the code doesn't need to be special-cased.
	// (When sending to MIDI, range is 0-103 for each.)
	//{ { ElementType::Off,0 }, 0 },			// 95 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 96 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 97 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 98 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 99 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 100 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 101 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 102 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 103 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 104 non-existent
};

// Used to quickly convert mod source indexes into ElementIDs
const SourceListType ctlSourceList
{
	{ { ElementType::Off,0 }, 0 },
	{ { ElementType::LFO,0 }, 0 },
	{ { ElementType::LFO,1 }, 0 },
	{ { ElementType::LFO,2 }, 0 },
	{ { ElementType::LFO,3 }, 0 },
	{ { ElementType::VibratoLFO,0 }, 0 },
	{ { ElementType::EG,0 }, 0 },
	{ { ElementType::EG,1 }, 0 },
	{ { ElementType::EG,2 }, 0 },
	{ { ElementType::EG,3 }, 0 },
	{ { ElementType::EG,4 }, 0 },
	{ { ElementType::EG,5 }, 0 },
	{ { ElementType::LoopEG,0 }, 0 },		// 12
	{ { ElementType::LoopEG,0 }, 1 },		// 13
	//{ { ElementType::Off,0 }, 0 },			// 14 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 15 non-existent
	{ { ElementType::Velocity,0 }, 0 },		// 16
	{ { ElementType::Aftertouch,0 }, 0 },
	{ { ElementType::Note,0 }, 0 },
	{ { ElementType::ModWheel,0 }, 0 },
	{ { ElementType::AT_ModWheel,0 }, 0 },
	{ { ElementType::Ribbon,0 }, 0 },
	{ { ElementType::Ribbon,1 }, 0 },
	{ { ElementType::JoyX,0 }, 0 },
	{ { ElementType::JoyY,0 }, 0 },
	{ { ElementType::CC,0 }, 0 },
	{ { ElementType::CC,1 }, 0 },
	{ { ElementType::CC,2 }, 0 },
	{ { ElementType::CC,3 }, 0 },
	{ { ElementType::CC,4 }, 0 },
	{ { ElementType::Seq,0 }, 0 },
	{ { ElementType::Seq,1 }, 0 },
	{ { ElementType::Seq,2 }, 0 },
	{ { ElementType::Seq,3 }, 0 },
	{ { ElementType::Pedal,0 }, 0 },
	{ { ElementType::Pedal,1 }, 0 },
	{ { ElementType::AssignButton,0 }, 0 },
	{ { ElementType::AssignButton,1 }, 0 },
	{ { ElementType::EnvFol,0 }, 0 },
	{ { ElementType::Off,0 }, 0 },
	{ { ElementType::KeyTable,0 }, 0 },
	{ { ElementType::KeyTable,1 }, 0 },
	{ { ElementType::KeyTable,2 }, 0 },
	{ { ElementType::KeyTable,3 }, 0 },
	{ { ElementType::PolyAT,0 }, 0 },
	{ { ElementType::Lag,0 }, 0 },
	{ { ElementType::Lag,1 }, 0 },
	{ { ElementType::Lag,2 }, 0 },
	{ { ElementType::Lag,3 }, 0 },
	{ { ElementType::Breath,0 }, 0 },		// 49
	//{ { ElementType::Off,0 }, 0 },			// 50 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 51 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 52 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 53 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 54 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 55 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 56 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 57 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 58 non-existent
	//{ { ElementType::Off,0 }, 0 },			// 59 non-existent
	{ { ElementType::MaxValue,0 }, 0 }		// 60
};

// Used to quickly convert mod source indexes into ElementIDs
const SourceListType egModSourceList
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::Velocity,0 }, 0 },
	{{ ElementType::KeyTracking,0 }, 0 },
	{{ ElementType::ModWheel,0 }, 0 },
	{{ ElementType::CC,0 }, 0 },
	{{ ElementType::CC,1 }, 0 },
	{{ ElementType::CC,2 }, 0 },
	{{ ElementType::CC,3 }, 0 }
};


const SourceListType oscSyncSourceList
{
	{ { ElementType::Off,0 }, 0 },
	{ { ElementType::Gate,0 }, 0 },
	{ { ElementType::Oscillator,0 }, 0 },
	{ { ElementType::Oscillator,1 }, 0 },
	{ { ElementType::Oscillator,2 }, 0 },
	{ { ElementType::Oscillator,3 }, 0 }
};

const SourceListType vcaMasterSourceList0
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::VCA,0 }, 0}
};
const SourceListType vcaMasterSourceList1
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::VCA,1 }, 0 }
};

const SourceListType vcaMasterSourceList2
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::VCA,2 }, 0 }
};
const SourceListType vcaMasterSourceList3
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::VCA,3 }, 0 }
};
const SourceListType vcaMasterModSourceList
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::EG,5 }, 0 }
};

const SourceListType insertFx0SourceList
{
	{{ ElementType::Mixer,0 }, 0 },
	{{ ElementType::Filter,0 }, 0 }
};
const SourceListType insertFx1SourceList
{
	{{ ElementType::Mixer,1 }, 0 },
	{{ ElementType::Filter,1 }, 0 }
};
const SourceListType insertFx2SourceList
{
	{{ ElementType::Mixer,2 }, 0 },
	{{ ElementType::Filter,2 }, 0 }
};
const SourceListType insertFx3SourceList
{
	{{ ElementType::Mixer,3 }, 0 },
	{{ ElementType::Filter,3 }, 0 }
};

const SourceListType vca0SourceList
{
	{{ ElementType::Filter,0 }, 0 },
	{{ ElementType::InsertFX,0 }, 0 }
};
const SourceListType vca1SourceList
{
	{{ ElementType::Filter,1 }, 0 },
	{{ ElementType::InsertFX,1 }, 0 }
}; 
const SourceListType vca2SourceList
{
	{{ ElementType::Filter,2 }, 0 },
	{{ ElementType::InsertFX,2 }, 0 }
}; 
const SourceListType vca3SourceList
{
	{{ ElementType::Filter,3 }, 0 },
	{{ ElementType::InsertFX,3 }, 0 }
};

const SourceListType OutputSourceList
{
	{{ ElementType::Off,0 }, 0 },
	{{ ElementType::VCAMaster,0 }, 0 },
	{{ ElementType::ExtInput,4 }, 0 },
	{{ ElementType::ExtInput,5 }, 0 },
	{{ ElementType::SPDIF_In,2 }, 0 },
	{{ ElementType::FXChannel,0 }, 0 },
	{{ ElementType::FXChannel,1 }, 0 },
	{{ ElementType::FXChannel,2 }, 0 },
	{{ ElementType::FXChannel,3 }, 0}
};

ElementID::ElementID(ElementType aElemType, int aElemIndex)
{
	elemType = aElemType;
	elemIndex = narrow<uint8_t>(aElemIndex);
}

void ElementID::Write(std::ostream& output) const
{
	output.write(reinterpret_cast<const char *>(&elemType), sizeof(elemType));
	output.write(reinterpret_cast<const char *>(&elemIndex), sizeof(elemIndex));
}

void ElementID::Read(std::istream& input)
{
	input.read(reinterpret_cast<char*>(&elemType), sizeof(elemType));
	input.read(reinterpret_cast<char*>(&elemIndex), sizeof(elemIndex));
}

ParamID::ParamID(PortID portID, int subIndex_)
{
	elemType = portID.elemID.elemType;
	elemIndex = portID.elemID.elemIndex;
	portType = portID.portType;
	portIndex = portID.portIndex;
	subIndex = narrow<uint8_t>(subIndex_);
}
ParamID::ParamID(ElementType	aElemType, int aElemIndex, PortType aPortType, int aPortIndex, int subIndex_)
{
	elemType = aElemType;
	elemIndex = narrow<uint8_t>(aElemIndex);
	portType = aPortType;
	portIndex = narrow<uint8_t>(aPortIndex);
	subIndex = narrow<uint8_t>(subIndex_);
}
ParamID::ParamID(ElementID elemID, PortType aPortType, int aPortIndex, int subIndex_)
{
	elemType = elemID.elemType;
	elemIndex = elemID.elemIndex;
	portType = aPortType;
	portIndex = narrow<uint8_t>(aPortIndex);
	subIndex = narrow<uint8_t>(subIndex_);
}

ParamID::ParamID(ElementID elemID, int subIndex_)	// convenient for setting parameters in the panels
{
	elemType = elemID.elemType;
	elemIndex = elemID.elemIndex;
	portType = PortType::None;
	portIndex = 0;
	subIndex = narrow<uint8_t>(subIndex_);
}

void ParamID::Write(std::ostream& output) const
{
	output.write(reinterpret_cast<const char *>(&elemType), sizeof(elemType));
	output.write(reinterpret_cast<const char *>(&elemIndex), sizeof(elemIndex));
	output.write(reinterpret_cast<const char *>(&portType), sizeof(portType));
	output.write(reinterpret_cast<const char *>(&portIndex), sizeof(portIndex));
	output.write(reinterpret_cast<const char *>(&subIndex), sizeof(subIndex));
}

void ParamID::Read(std::istream& input)
{
	input.read(reinterpret_cast<char*>(&elemType), sizeof(elemType));
	input.read(reinterpret_cast<char*>(&elemIndex), sizeof(elemIndex));
	input.read(reinterpret_cast<char*>(&portType), sizeof(portType));
	input.read(reinterpret_cast<char*>(&portIndex), sizeof(portIndex));
	input.read(reinterpret_cast<char*>(&subIndex), sizeof(subIndex));
}

PortID::PortID(ElementID aElemID, PortType aPortType, int aPortIndex)
{
	elemID = aElemID;
	portType = aPortType;
	portIndex = narrow<uint8_t>(aPortIndex);
}

void PortID::Write(std::ostream& output) const
{
	elemID.Write(output);
	output.write(reinterpret_cast<const char *>(&portType), sizeof(portType));
	output.write(reinterpret_cast<const char *>(&portIndex), sizeof(portIndex));
}

void PortID::Read(std::istream& input)
{
	elemID.Read(input);
	input.read(reinterpret_cast<char*>(&portType), sizeof(portType));
	input.read(reinterpret_cast<char*>(&portIndex), sizeof(portIndex));
}


ParamInfo::ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, ::Range range_)
	:portType(portType_), portIndex((uint8_t)portIndex_), subIndex((uint8_t)subIndex_), valueType(valueType_), range(range_)
{}
ParamInfo::ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, EnumListType* enumList_)
	:portType(portType_), portIndex((uint8_t)portIndex_), subIndex((uint8_t)subIndex_), valueType(valueType_), enumList(enumList_)
{
	range.minValue = 0;
	range.maxValue = enumList_->size() - 1;
	range.divisor = divisor1;
}
ParamInfo::ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, SourceListType* srcList_)
	:portType(portType_), portIndex((uint8_t)portIndex_), subIndex((uint8_t)subIndex_), valueType(valueType_), srcList(srcList_)
{
	range.minValue = 0; 
	range.maxValue = srcList_->size() - 1;
	range.divisor = divisor1;
}


bool operator == (const ElementID &lhs,const ElementID &rhs)
{
	return 	lhs.elemType == rhs.elemType && lhs.elemIndex == rhs.elemIndex;
}
bool operator < (const ElementID lhs,const ElementID rhs)	// required in order to put ElementIDs into std:set (SelectionModel)
{
	return (lhs.elemType == rhs.elemType) ?  lhs.elemIndex < rhs.elemIndex : lhs.elemType < rhs.elemType;
}

bool operator == (const PortID &lhs,const PortID &rhs)
{
	return	lhs.elemID == rhs.elemID && 
			lhs.portType == rhs.portType && 
			lhs.portIndex == rhs.portIndex;
}
bool operator < (const PortID& lhs,const PortID& rhs)	// required in order to put PortID into std:set 
{
	if (lhs.elemID == rhs.elemID)
	{
		if (lhs.portType == rhs.portType)
		{
			return lhs.portIndex < rhs.portIndex;
		}
		else
		{
			return lhs.portType < rhs.portType;
		}
	}
	else
	{
		return lhs.elemID < rhs.elemID;
	}
}

bool operator == (const SourceID& lhs, const SourceID& rhs)
{
	return	lhs.elemID == rhs.elemID && lhs.outputPortIndex == rhs.outputPortIndex;
}
bool operator != (const SourceID& lhs, const SourceID& rhs)
{
	return	!(lhs == rhs);
}


std::vector<ElementItem> masterParamList
{
	{
		ElementType::Oscillator, 4,
		{
			// PortType,		Index,	SubIndex,			ValueType,			Range/EnumList/SourceList/DestList

			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(EnumListType*)&oscModDestList		},	// mod 0 dest - Must retrieve dest before mod amount
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(EnumListType*)&oscModDestList		},	// mod 1 dest
			{ PortType::Mod,		2,	psiDest,			ValueType::Enum,	(EnumListType*)&oscModDestList		},	// mod 2 dest
			{ PortType::Mod,		3,	psiDest,			ValueType::Enum,	(EnumListType*)&oscModDestList		},	// mod 3 dest
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 level (strength)
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-1000, 1000, divisor10}			},	// mod 0 amount LinFM
			{ PortType::Control,	1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 1 level (strength)
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-1000, 1000, divisor10}			},	// mod 1 amount LinFM
			{ PortType::Control,	2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 2 level (strength)
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-1000, 1000, divisor10}			},	// mod 2 amount LinFM
			{ PortType::Control,	3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 3 level (strength)
			{ PortType::Mod,		3,	psiAmount,			ValueType::Num,		{-1000, 1000, divisor10}			},	// mod 3 amount LinFM
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Mod,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 3 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 0 source
			{ PortType::Control,	1,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 1 source
			{ PortType::Control,	2,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 2 source
			{ PortType::Control,	3,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 3 source
			{ PortType::None,		0,	oscLow,				ValueType::Num,		{0, 1}								},	// low (Note this is a Num type. 0 = Off, 1 = On)
			{ PortType::None,		0,	oscCoarseNoTrack,	ValueType::Num,		{0, 200000}							},	// coarse tuning whan NoTrack is enabled
			{ PortType::None,		0,	oscType,			ValueType::Enum,	(EnumListType*)&oscTypeNames		},	// type
			{ PortType::None,		0,	oscMM1Wave,			ValueType::Enum,	(EnumListType*)&MM1WaveNames		},	// MM1 wave
			{ PortType::None,		0,	oscFine,			ValueType::Num,		{-100, 100}							},	// fine
			{ PortType::None,		0,	oscShape,			ValueType::Num,		{ 0, 100 }							},	// shape
			{ PortType::None,		0,	oscPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	oscNoTrack,			ValueType::Num,		{0, 1}								},	// noTrack
			{ PortType::None,		0,	oscGlide,			ValueType::Num,		{0, 1}								},	// glide on/off
			{ PortType::None,		0,	oscGlideRate,		ValueType::Num,		{0, 200000}							},	// glide rate
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&oscSyncSourceList	},	// osc sync
			{ PortType::None,		0,	oscCoarse,			ValueType::Num,		{-60, 60}							},	// coarse
			{ PortType::None,		0,	oscClockSync,		ValueType::Num,		{0, 1}								},	// clock sync
			{ PortType::None,		0,	oscCoarseSync,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// coarse tuning whan ClockSync is enabled
			{ PortType::Mod,		0,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 0 amount pitch
			{ PortType::Mod,		1,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 1 amount pitch
			{ PortType::Mod,		2,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 2 amount pitch
			{ PortType::Mod,		3,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 3 amount pitch
			{ PortType::Mod,		0,	psiAmtShape,		ValueType::Num,		{-100, 100}							},	// mod 0 amount Shape
			{ PortType::Mod,		1,	psiAmtShape,		ValueType::Num,		{-100, 100}							},	// mod 1 amount Shape
			{ PortType::Mod,		2,	psiAmtShape,		ValueType::Num,		{-100, 100}							},	// mod 2 amount Shape
			{ PortType::Mod,		3,	psiAmtShape,		ValueType::Num,		{-100, 100}							},	// mod 3 amount Shape
			{ PortType::None,		0,	oscWTWave,			ValueType::Enum,	(EnumListType*)&WTWaveNames			},	// WT wave
			{ PortType::None,		0,	oscCEMWave,			ValueType::Enum,	(EnumListType*)&CEMWaveNames		},	// CEM wave
			{ PortType::None,		0,	oscWAVWave,			ValueType::Enum,	(EnumListType*)&WAVWaveNames		},	// WAV wave
			{ PortType::None,		0,	oscVSWave,			ValueType::Enum,	(EnumListType*)&VSWaveNames			},	// VS wave
			{ PortType::None,		0,	oscMiniWave,		ValueType::Enum,	(EnumListType*)&MiniWaveNames		}	// Mini wave
		},
		{
			// -------------  Osc 1 -----------------------
			{
				{ 0x10, 0x00, 0x22 },
				{ 0x10, 0x00, 0x2E },
				{ 0x10, 0x00, 0x3A },
				{ 0x10, 0x00, 0x46 },
				{ 0x10, 0x00, 0x20 },
				{ 0x10, 0x00, 0x25 },
				{ 0x10, 0x00, 0x2C },
				{ 0x10, 0x00, 0x31 },
				{ 0x10, 0x00, 0x38 },
				{ 0x10, 0x00, 0x3D },
				{ 0x10, 0x00, 0x44 },
				{ 0x10, 0x00, 0x49 },
				{ 0x10, 0x00, 0x1D },
				{ 0x10, 0x00, 0x29 },
				{ 0x10, 0x00, 0x35 },
				{ 0x10, 0x00, 0x41 },
				{ 0x10, 0x00, 0x1F },
				{ 0x10, 0x00, 0x2B },
				{ 0x10, 0x00, 0x37 },
				{ 0x10, 0x00, 0x43 },
				{ 0x10, 0x00, 0x19 },
				{ 0x10, 0x00, 0x0B },
				{ 0x10, 0x00, 0x00 },
				{ 0x10, 0x00, 0x01 },
				{ 0x10, 0x00, 0x08 },
				{ 0x10, 0x00, 0x0E },
				{ 0x10, 0x00, 0x10 },
				{ 0x10, 0x00, 0x18 },
				{ 0x10, 0x00, 0x13 },
				{ 0x10, 0x00, 0x14 },
				{ 0x10, 0x00, 0x12 },
				{ 0x10, 0x00, 0x07 },
				{ 0x10, 0x00, 0x17 },
				{ 0x10, 0x00, 0x0A },
				{ 0x10, 0x00, 0x23 },
				{ 0x10, 0x00, 0x2F },
				{ 0x10, 0x00, 0x3B },
				{ 0x10, 0x00, 0x47 },
				{ 0x10, 0x00, 0x27 },
				{ 0x10, 0x00, 0x33 },
				{ 0x10, 0x00, 0x3F },
				{ 0x10, 0x00, 0x4B },
				{ 0x10, 0x00, 0x02 },
				{ 0x10, 0x00, 0x03 },
				{ 0x10, 0x00, 0x04 },
				{ 0x10, 0x00, 0x05 },
				{ 0x10, 0x00, 0x06 },
			},

			// -------------  Osc 2 -----------------------
			{
				{ 0x10, 0x10, 0x22 },
				{ 0x10, 0x10, 0x2E },
				{ 0x10, 0x10, 0x3A },
				{ 0x10, 0x10, 0x46 },
				{ 0x10, 0x10, 0x20 },
				{ 0x10, 0x10, 0x25 },
				{ 0x10, 0x10, 0x2C },
				{ 0x10, 0x10, 0x31 },
				{ 0x10, 0x10, 0x38 },
				{ 0x10, 0x10, 0x3D },
				{ 0x10, 0x10, 0x44 },
				{ 0x10, 0x10, 0x49 },
				{ 0x10, 0x10, 0x1D },
				{ 0x10, 0x10, 0x29 },
				{ 0x10, 0x10, 0x35 },
				{ 0x10, 0x10, 0x41 },
				{ 0x10, 0x10, 0x1F },
				{ 0x10, 0x10, 0x2B },
				{ 0x10, 0x10, 0x37 },
				{ 0x10, 0x10, 0x43 },
				{ 0x10, 0x10, 0x19 },
				{ 0x10, 0x10, 0x0B },
				{ 0x10, 0x10, 0x00 },
				{ 0x10, 0x10, 0x01 },
				{ 0x10, 0x10, 0x08 },
				{ 0x10, 0x10, 0x0E },
				{ 0x10, 0x10, 0x10 },
				{ 0x10, 0x10, 0x18 },
				{ 0x10, 0x10, 0x13 },
				{ 0x10, 0x10, 0x14 },
				{ 0x10, 0x10, 0x12 },
				{ 0x10, 0x10, 0x07 },
				{ 0x10, 0x10, 0x17 },
				{ 0x10, 0x10, 0x0A },
				{ 0x10, 0x10, 0x23 },
				{ 0x10, 0x10, 0x2F },
				{ 0x10, 0x10, 0x3B },
				{ 0x10, 0x10, 0x47 },
				{ 0x10, 0x10, 0x27 },
				{ 0x10, 0x10, 0x33 },
				{ 0x10, 0x10, 0x3F },
				{ 0x10, 0x10, 0x4B },
				{ 0x10, 0x10, 0x02 },
				{ 0x10, 0x10, 0x03 },
				{ 0x10, 0x10, 0x04 },
				{ 0x10, 0x10, 0x05 },
				{ 0x10, 0x10, 0x06 },
			},

			// -------------  Osc 3 -----------------------
			{
				{ 0x10, 0x20, 0x22 },
				{ 0x10, 0x20, 0x2E },
				{ 0x10, 0x20, 0x3A },
				{ 0x10, 0x20, 0x46 },
				{ 0x10, 0x20, 0x20 },
				{ 0x10, 0x20, 0x25 },
				{ 0x10, 0x20, 0x2C },
				{ 0x10, 0x20, 0x31 },
				{ 0x10, 0x20, 0x38 },
				{ 0x10, 0x20, 0x3D },
				{ 0x10, 0x20, 0x44 },
				{ 0x10, 0x20, 0x49 },
				{ 0x10, 0x20, 0x1D },
				{ 0x10, 0x20, 0x29 },
				{ 0x10, 0x20, 0x35 },
				{ 0x10, 0x20, 0x41 },
				{ 0x10, 0x20, 0x1F },
				{ 0x10, 0x20, 0x2B },
				{ 0x10, 0x20, 0x37 },
				{ 0x10, 0x20, 0x43 },
				{ 0x10, 0x20, 0x19 },
				{ 0x10, 0x20, 0x0B },
				{ 0x10, 0x20, 0x00 },
				{ 0x10, 0x20, 0x01 },
				{ 0x10, 0x20, 0x08 },
				{ 0x10, 0x20, 0x0E },
				{ 0x10, 0x20, 0x10 },
				{ 0x10, 0x20, 0x18 },
				{ 0x10, 0x20, 0x13 },
				{ 0x10, 0x20, 0x14 },
				{ 0x10, 0x20, 0x12 },
				{ 0x10, 0x20, 0x07 },
				{ 0x10, 0x20, 0x17 },
				{ 0x10, 0x20, 0x0A },
				{ 0x10, 0x20, 0x23 },
				{ 0x10, 0x20, 0x2F },
				{ 0x10, 0x20, 0x3B },
				{ 0x10, 0x20, 0x47 },
				{ 0x10, 0x20, 0x27 },
				{ 0x10, 0x20, 0x33 },
				{ 0x10, 0x20, 0x3F },
				{ 0x10, 0x20, 0x4B },
				{ 0x10, 0x20, 0x02 },
				{ 0x10, 0x20, 0x03 },
				{ 0x10, 0x20, 0x04 },
				{ 0x10, 0x20, 0x05 },
				{ 0x10, 0x20, 0x06 },
			},

			// -------------  Osc 4 -----------------------
			{
				{ 0x10, 0x30, 0x22 },
				{ 0x10, 0x30, 0x2E },
				{ 0x10, 0x30, 0x3A },
				{ 0x10, 0x30, 0x46 },
				{ 0x10, 0x30, 0x20 },
				{ 0x10, 0x30, 0x25 },
				{ 0x10, 0x30, 0x2C },
				{ 0x10, 0x30, 0x31 },
				{ 0x10, 0x30, 0x38 },
				{ 0x10, 0x30, 0x3D },
				{ 0x10, 0x30, 0x44 },
				{ 0x10, 0x30, 0x49 },
				{ 0x10, 0x30, 0x1D },
				{ 0x10, 0x30, 0x29 },
				{ 0x10, 0x30, 0x35 },
				{ 0x10, 0x30, 0x41 },
				{ 0x10, 0x30, 0x1F },
				{ 0x10, 0x30, 0x2B },
				{ 0x10, 0x30, 0x37 },
				{ 0x10, 0x30, 0x43 },
				{ 0x10, 0x30, 0x19 },
				{ 0x10, 0x30, 0x0B },
				{ 0x10, 0x30, 0x00 },
				{ 0x10, 0x30, 0x01 },
				{ 0x10, 0x30, 0x08 },
				{ 0x10, 0x30, 0x0E },
				{ 0x10, 0x30, 0x10 },
				{ 0x10, 0x30, 0x18 },
				{ 0x10, 0x30, 0x13 },
				{ 0x10, 0x30, 0x14 },
				{ 0x10, 0x30, 0x12 },
				{ 0x10, 0x30, 0x07 },
				{ 0x10, 0x30, 0x17 },
				{ 0x10, 0x30, 0x0A },
				{ 0x10, 0x30, 0x23 },
				{ 0x10, 0x30, 0x2F },
				{ 0x10, 0x30, 0x3B },
				{ 0x10, 0x30, 0x47 },
				{ 0x10, 0x30, 0x27 },
				{ 0x10, 0x30, 0x33 },
				{ 0x10, 0x30, 0x3F },
				{ 0x10, 0x30, 0x4B },
				{ 0x10, 0x30, 0x02 },
				{ 0x10, 0x30, 0x03 },
				{ 0x10, 0x30, 0x04 },
				{ 0x10, 0x30, 0x05 },
				{ 0x10, 0x30, 0x06 },
			},
		},
		{ 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 3, 1, 1, 2, 2, 2, 1, 1, 3, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, },
		{
			{ 3, 1, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, 21, 30, 0, 0, 0, 0, 0, 0, 300, 1, 4, 0, 50, 0, 0, 0, 0, 0, 0, 0, 1, 0, 240, 1200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3 },
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Rotor
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Rotor, 2,
		{
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 level (strength)
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 level (amount)
			{ PortType::Control,	1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 1 level (strength)
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 level (amount)
			{ PortType::Control,	2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 2 level (strength)
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 2 level (amount)
			{ PortType::Control,	3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 3 level (strength)
			{ PortType::Mod,		3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 3 level (amount)
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Mod,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 3 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 0 source
			{ PortType::Control,	1,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 1 source
			{ PortType::Control,	2,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 2 source
			{ PortType::Control,	3,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 3 source
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(EnumListType*)&rotModDestList		},	// mod 0 dest
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(EnumListType*)&rotModDestList		},	// mod 1 dest
			{ PortType::Mod,		2,	psiDest,			ValueType::Enum,	(EnumListType*)&rotModDestList		},	// mod 2 dest
			{ PortType::Mod,		3,	psiDest,			ValueType::Enum,	(EnumListType*)&rotModDestList		},	// mod 3 dest
			{ PortType::Input,		0,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 0 level (amount)
			{ PortType::Input,		1,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 1 level (amount)
			{ PortType::Input,		2,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 2 level (amount)
			{ PortType::Input,		3,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 3 level (amount)
			{ PortType::None,		0,	rotCoarseNoTrack,	ValueType::Num,		{0, 200000, divisor10}				},	// coarse tuning when NoTrack is enabled
			{ PortType::None,		0,	rotFine,			ValueType::Num,		{-100, 100}							},	// fine
			{ PortType::None,		0,	rotXFade,			ValueType::Num,		{0, 127}							},	// xFade
			{ PortType::None,		0,	rotPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	rotNoTrack,			ValueType::Num,		{0, 1}								},	// noTrack (Note this is a Num type. 0 = Off, 1 = On)
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 0 source
			{ PortType::Input,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 1 source
			{ PortType::Input,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 2 source
			{ PortType::Input,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 3 source
			{ PortType::None,		0,	rotPhaseSync,		ValueType::Enum,	(EnumListType*)&rotPhaseSyncNames	},	// phaseSync
			{ PortType::None,		0,	rotCoarse,			ValueType::Num,		{-60, 60}							},	// coarse tuning (when NoTrack is not enabled)
			{ PortType::None,		0,	rotLow,				ValueType::Num,		{0, 1}								},	// low (Note this is a Num type. 0 = Off, 1 = On)
			{ PortType::None,		0,	rotClockSync,		ValueType::Num,		{0, 1}								},	// clock sync (Note this is a Num type. 0 = Off, 1 = On)
			{ PortType::None,		0,	rotCoarseSync,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// coarse tuning when clock sync is enabled
			{ PortType::Mod,		0,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 0 amount pitch
			{ PortType::Mod,		1,	psiAmtPitch, 		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 1 amount pitch
			{ PortType::Mod,		2,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 2 amount pitch
			{ PortType::Mod,		3,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			}	// mod 3 amount pitch
		},
		{
			// -------------  Rotor 1 -----------------------
			{
				{ 0x10, 0x40, 0x1E },
				{ 0x10, 0x40, 0x23 },
				{ 0x10, 0x40, 0x28 },
				{ 0x10, 0x40, 0x2D },
				{ 0x10, 0x40, 0x32 },
				{ 0x10, 0x40, 0x37 },
				{ 0x10, 0x40, 0x3C },
				{ 0x10, 0x40, 0x41 },
				{ 0x10, 0x40, 0x1B },
				{ 0x10, 0x40, 0x25 },
				{ 0x10, 0x40, 0x2F },
				{ 0x10, 0x40, 0x39 },
				{ 0x10, 0x40, 0x1D },
				{ 0x10, 0x40, 0x27 },
				{ 0x10, 0x40, 0x31 },
				{ 0x10, 0x40, 0x3B },
				{ 0x10, 0x40, 0x20 },
				{ 0x10, 0x40, 0x2A },
				{ 0x10, 0x40, 0x34 },
				{ 0x10, 0x40, 0x3E },
				{ 0x10, 0x40, 0x02 },
				{ 0x10, 0x40, 0x05 },
				{ 0x10, 0x40, 0x08 },
				{ 0x10, 0x40, 0x0B },
				{ 0x10, 0x40, 0x10 },
				{ 0x10, 0x40, 0x0D },
				{ 0x10, 0x40, 0x13 },
				{ 0x10, 0x40, 0x16 },
				{ 0x10, 0x40, 0x19 },
				{ 0x10, 0x40, 0x00 },
				{ 0x10, 0x40, 0x03 },
				{ 0x10, 0x40, 0x06 },
				{ 0x10, 0x40, 0x09 },
				{ 0x10, 0x40, 0x15 },
				{ 0x10, 0x40, 0x0C },
				{ 0x10, 0x40, 0x1A },
				{ 0x10, 0x40, 0x18 },
				{ 0x10, 0x40, 0x0F },
				{ 0x10, 0x40, 0x21 },
				{ 0x10, 0x40, 0x2B },
				{ 0x10, 0x40, 0x35 },
				{ 0x10, 0x40, 0x3F },
			},
			// -------------  Rotor 2 -----------------------
			{
				{ 0x10, 0x50, 0x1E },
				{ 0x10, 0x50, 0x23 },
				{ 0x10, 0x50, 0x28 },
				{ 0x10, 0x50, 0x2D },
				{ 0x10, 0x50, 0x32 },
				{ 0x10, 0x50, 0x37 },
				{ 0x10, 0x50, 0x3C },
				{ 0x10, 0x50, 0x41 },
				{ 0x10, 0x50, 0x1B },
				{ 0x10, 0x50, 0x25 },
				{ 0x10, 0x50, 0x2F },
				{ 0x10, 0x50, 0x39 },
				{ 0x10, 0x50, 0x1D },
				{ 0x10, 0x50, 0x27 },
				{ 0x10, 0x50, 0x31 },
				{ 0x10, 0x50, 0x3B },
				{ 0x10, 0x50, 0x20 },
				{ 0x10, 0x50, 0x2A },
				{ 0x10, 0x50, 0x34 },
				{ 0x10, 0x50, 0x3E },
				{ 0x10, 0x50, 0x02 },
				{ 0x10, 0x50, 0x05 },
				{ 0x10, 0x50, 0x08 },
				{ 0x10, 0x50, 0x0B },
				{ 0x10, 0x50, 0x10 },
				{ 0x10, 0x50, 0x0D },
				{ 0x10, 0x50, 0x13 },
				{ 0x10, 0x50, 0x16 },
				{ 0x10, 0x50, 0x19 },
				{ 0x10, 0x50, 0x00 },
				{ 0x10, 0x50, 0x03 },
				{ 0x10, 0x50, 0x06 },
				{ 0x10, 0x50, 0x09 },
				{ 0x10, 0x50, 0x15 },
				{ 0x10, 0x50, 0x0C },
				{ 0x10, 0x50, 0x1A },
				{ 0x10, 0x50, 0x18 },
				{ 0x10, 0x50, 0x0F },
				{ 0x10, 0x50, 0x21 },
				{ 0x10, 0x50, 0x2B },
				{ 0x10, 0x50, 0x35 },
				{ 0x10, 0x50, 0x3F },
			},
		},
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 2, 2, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, },
		{
			{ 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 63, 63, 63, 5, 0, 127, 0, 1, 1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	AM
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::AM, 2,
		{
			{ PortType::None,		0,	amAlgorithm,		ValueType::Enum,	(EnumListType*)&amAlgorithmNames	},	// algorithm
			{ PortType::None,		0,	amOffset,			ValueType::Num,		{-63, 63}							},	// offset
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// ctl 0 level (strength)
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 level (amount)
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 0 source
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		}	// ctl 0 source
		},
		{
			// -------------  AM 1 -----------------------

			{
				{ 0x10, 0x60, 0x34 },
				{ 0x10, 0x60, 0x3B },
				{ 0x10, 0x60, 0x3D },
				{ 0x10, 0x60, 0x39 },
				{ 0x10, 0x60, 0x35 },
				{ 0x10, 0x60, 0x37 },
				{ 0x10, 0x60, 0x3C },
			},
			// -------------  AM 2 -----------------------

			{
				{ 0x10, 0x60, 0x3F },
				{ 0x10, 0x60, 0x46 },
				{ 0x10, 0x60, 0x48 },
				{ 0x10, 0x60, 0x44 },
				{ 0x10, 0x60, 0x40 },
				{ 0x10, 0x60, 0x42 },
				{ 0x10, 0x60, 0x47 },
			},
		},
		{ 1, 1, 2, 2, 2, 2, 1, },
		{
			{ 3, 0, 0, 100, 1, 62, 0 },
			{ 0, 0, 0, 100, 3, 64, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Vector
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Vector, 2,
		{
			{ PortType::Input,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// inp 0 level (amount)
			{ PortType::Input,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// inp 1 level (amount)
			{ PortType::Input,		2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// inp 2 level (amount)
			{ PortType::Input,		3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// inp 3 level (amount)
			{ PortType::None,		0,	vecXOffset,			ValueType::Num,		{-63, 63}							},	// X offset
			{ PortType::None,		0,	vecYOffset,			ValueType::Num,		{-63, 63}							},	// Y offset
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 level (amount)
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 level (amount)
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 0 source
			{ PortType::Input,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 1 source
			{ PortType::Input,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 2 source
			{ PortType::Input,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 3 source
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
		},
		{
			// -------------  Vector 1 -----------------------

			{
				{ 0x10, 0x60, 0x02 },
				{ 0x10, 0x60, 0x06 },
				{ 0x10, 0x60, 0x0A },
				{ 0x10, 0x60, 0x0E },
				{ 0x10, 0x60, 0x14 },
				{ 0x10, 0x60, 0x19 },
				{ 0x10, 0x60, 0x12 },
				{ 0x10, 0x60, 0x17 },
				{ 0x10, 0x60, 0x00 },
				{ 0x10, 0x60, 0x04 },
				{ 0x10, 0x60, 0x08 },
				{ 0x10, 0x60, 0x0C },
				{ 0x10, 0x60, 0x10 },
				{ 0x10, 0x60, 0x15 },
			},
			// -------------  Vector 2 -----------------------

			{
				{ 0x10, 0x60, 0x1C },
				{ 0x10, 0x60, 0x20 },
				{ 0x10, 0x60, 0x24 },
				{ 0x10, 0x60, 0x28 },
				{ 0x10, 0x60, 0x2E },
				{ 0x10, 0x60, 0x33 },
				{ 0x10, 0x60, 0x2C },
				{ 0x10, 0x60, 0x31 },
				{ 0x10, 0x60, 0x1A },
				{ 0x10, 0x60, 0x1E },
				{ 0x10, 0x60, 0x22 },
				{ 0x10, 0x60, 0x26 },
				{ 0x10, 0x60, 0x2A },
				{ 0x10, 0x60, 0x2F },
			},
		},
		{ 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, },
		{
			{ 100, 100, 100, 100, 0, 0, 100, 100, 1, 2, 3, 4, 23, 24 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Mixer
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Mixer, 4,
		{
			{ PortType::Input,		0,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 0 level
			{ PortType::Input,		1,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 1 level
			{ PortType::Input,		2,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 2 level
			{ PortType::Input,		3,	psiAmount,			ValueType::Num,		{-63, 63}							},	// inp 3 level
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-63, 63}							},	// mod 0 level (amount)
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-63, 63}							},	// mod 1 level (amount)
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-63, 63}							},	// mod 2 level (amount)
			{ PortType::Mod,		3,	psiAmount,			ValueType::Num,		{-63, 63}							},	// mod 3 level (amount)
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 0 source
			{ PortType::Input,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 1 source
			{ PortType::Input,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 2 source
			{ PortType::Input,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 3 source
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Mod,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 3 source
			{ PortType::None,		0,	mixOutputLevel,		ValueType::Num,		{0, 127}							},	// output level
			{ PortType::Mod,		4,	psiAmount,			ValueType::Num,		{-63, 63}							},	// mod 4 level (amount)
			{ PortType::Mod,		4,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		}	// mod 4 source
		},
		{
			// -------------  Mixer 1 -----------------------

			{
				{ 0x11, 0x40, 0x02 },
				{ 0x11, 0x40, 0x08 },
				{ 0x11, 0x40, 0x0E },
				{ 0x11, 0x40, 0x14 },
				{ 0x11, 0x40, 0x05 },
				{ 0x11, 0x40, 0x0B },
				{ 0x11, 0x40, 0x11 },
				{ 0x11, 0x40, 0x17 },
				{ 0x11, 0x40, 0x00 },
				{ 0x11, 0x40, 0x06 },
				{ 0x11, 0x40, 0x0C },
				{ 0x11, 0x40, 0x12 },
				{ 0x11, 0x40, 0x03 },
				{ 0x11, 0x40, 0x09 },
				{ 0x11, 0x40, 0x0F },
				{ 0x11, 0x40, 0x15 },
				{ 0x11, 0x40, 0x18 },
				{ 0x11, 0x40, 0x1C },
				{ 0x11, 0x40, 0x1A },
			},
			// -------------  Mixer 2 -----------------------

			{
				{ 0x11, 0x40, 0x1F },
				{ 0x11, 0x40, 0x25 },
				{ 0x11, 0x40, 0x2B },
				{ 0x11, 0x40, 0x31 },
				{ 0x11, 0x40, 0x22 },
				{ 0x11, 0x40, 0x28 },
				{ 0x11, 0x40, 0x2E },
				{ 0x11, 0x40, 0x34 },
				{ 0x11, 0x40, 0x1D },
				{ 0x11, 0x40, 0x23 },
				{ 0x11, 0x40, 0x29 },
				{ 0x11, 0x40, 0x2F },
				{ 0x11, 0x40, 0x20 },
				{ 0x11, 0x40, 0x26 },
				{ 0x11, 0x40, 0x2C },
				{ 0x11, 0x40, 0x32 },
				{ 0x11, 0x40, 0x35 },
				{ 0x11, 0x40, 0x39 },
				{ 0x11, 0x40, 0x37 },
			},
			// -------------  Mixer 3 -----------------------

			{
				{ 0x11, 0x40, 0x3C },
				{ 0x11, 0x40, 0x42 },
				{ 0x11, 0x40, 0x48 },
				{ 0x11, 0x40, 0x4E },
				{ 0x11, 0x40, 0x3F },
				{ 0x11, 0x40, 0x45 },
				{ 0x11, 0x40, 0x4B },
				{ 0x11, 0x40, 0x51 },
				{ 0x11, 0x40, 0x3A },
				{ 0x11, 0x40, 0x40 },
				{ 0x11, 0x40, 0x46 },
				{ 0x11, 0x40, 0x4C },
				{ 0x11, 0x40, 0x3D },
				{ 0x11, 0x40, 0x43 },
				{ 0x11, 0x40, 0x49 },
				{ 0x11, 0x40, 0x4F },
				{ 0x11, 0x40, 0x52 },
				{ 0x11, 0x40, 0x56 },
				{ 0x11, 0x40, 0x54 },
			},
			// -------------  Mixer 4 -----------------------

			{
				{ 0x11, 0x40, 0x59 },
				{ 0x11, 0x40, 0x5F },
				{ 0x11, 0x40, 0x65 },
				{ 0x11, 0x40, 0x6B },
				{ 0x11, 0x40, 0x5C },
				{ 0x11, 0x40, 0x62 },
				{ 0x11, 0x40, 0x68 },
				{ 0x11, 0x40, 0x6E },
				{ 0x11, 0x40, 0x57 },
				{ 0x11, 0x40, 0x5D },
				{ 0x11, 0x40, 0x63 },
				{ 0x11, 0x40, 0x69 },
				{ 0x11, 0x40, 0x5A },
				{ 0x11, 0x40, 0x60 },
				{ 0x11, 0x40, 0x66 },
				{ 0x11, 0x40, 0x6C },
				{ 0x11, 0x40, 0x6F },
				{ 0x11, 0x40, 0x73 },
				{ 0x11, 0x40, 0x71 },
			},
		},
		{ 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, },
		{
			{ 63, 63, 63, 63, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 127, 0, 0 },
			{ 63, 63, 63, 63, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 127, 0, 0 },
			{ 63, 63, 63, 63, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 127, 0, 0 },
			{ 63, 63, 63, 63, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 127, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Filter
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Filter, 4,
		{
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 level (strength)
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 amount for resonance/none
			{ PortType::Control,	1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 1 level (strength)
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 amount for resonance/none
			{ PortType::Control,	2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 2 level (strength)
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 2 amount for resonance/none
			{ PortType::Control,	3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 3 level (strength)
			{ PortType::Mod,		3,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 3 amount for resonance/none
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Mod,		3,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 3 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 0 source
			{ PortType::Control,	1,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 1 source
			{ PortType::Control,	2,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 2 source
			{ PortType::Control,	3,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 3 source
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(EnumListType*)&fltModDestList		},	// mod 0 dest
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(EnumListType*)&fltModDestList		},	// mod 1 dest
			{ PortType::Mod,		2,	psiDest,			ValueType::Enum,	(EnumListType*)&fltModDestList		},	// mod 2 dest
			{ PortType::Mod,		3,	psiDest,			ValueType::Enum,	(EnumListType*)&fltModDestList		},	// mod 3 dest
			{ PortType::None,		0,	filterType,			ValueType::Enum,	(EnumListType*)&filterTypeNames		},
			{ PortType::None,		0,	filterMM1Mode,		ValueType::Enum,	(EnumListType*)&MM1ModeNames		},
			{ PortType::None,		0,	filterVowel1,		ValueType::Enum,	(EnumListType*)&VocalVowelNames		},
			{ PortType::None,		0,	filterVowel2,		ValueType::Enum,	(EnumListType*)&VocalVowelNames		},
			{ PortType::None,		0,	filterVowel3,		ValueType::Enum,	(EnumListType*)&VocalVowelNames		},
			{ PortType::None,		0,	filterVowel4,		ValueType::Enum,	(EnumListType*)&VocalVowelNames		},
			{ PortType::None,		0,	filterVowel5,		ValueType::Enum,	(EnumListType*)&VocalVowelNames		},
			{ PortType::None,		0,	filterResonance,	ValueType::Num,		{0, 100}							},
			{ PortType::None,		0,	filterDamping,		ValueType::Num,		{0, 100}							},
			{ PortType::None,		0,	filterCutoff,		ValueType::Num,		{0, 1260, divisor10}				},
			{ PortType::None,		0,	filterKeycenter,	ValueType::Num,		{-63, 63}							},
			{ PortType::None,		0,	filterKeytracking,	ValueType::Num,		{-200, 200}							},
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	},	// inp 0 source
			{ PortType::None,		0,	filterXFade,		ValueType::Num,		{0, 100}							},
			{ PortType::Mod,		0,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 0 amount for cutoff
			{ PortType::Mod,		1,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 1 amount for cutoff
			{ PortType::Mod,		2,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 2 amount for cutoff
			{ PortType::Mod,		3,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 3 amount for cutoff
			{ PortType::None,		0,	filterObieMode,		ValueType::Enum,	(EnumListType*)&ObieModeNames		},
			{ PortType::None,		0,	filterCombMode,		ValueType::Enum,	(EnumListType*)&CombModeNames		}
		},
		{
			// -------------  Filter 1 -----------------------

			{
				{ 0x11, 0x00, 0x19 },
				{ 0x11, 0x00, 0x1E },
				{ 0x11, 0x00, 0x23 },
				{ 0x11, 0x00, 0x28 },
				{ 0x11, 0x00, 0x2D },
				{ 0x11, 0x00, 0x32 },
				{ 0x11, 0x00, 0x37 },
				{ 0x11, 0x00, 0x3C },
				{ 0x11, 0x00, 0x16 },
				{ 0x11, 0x00, 0x20 },
				{ 0x11, 0x00, 0x2A },
				{ 0x11, 0x00, 0x34 },
				{ 0x11, 0x00, 0x18 },
				{ 0x11, 0x00, 0x22 },
				{ 0x11, 0x00, 0x2C },
				{ 0x11, 0x00, 0x36 },
				{ 0x11, 0x00, 0x1B },
				{ 0x11, 0x00, 0x25 },
				{ 0x11, 0x00, 0x2F },
				{ 0x11, 0x00, 0x39 },
				{ 0x11, 0x00, 0x00 },
				{ 0x11, 0x00, 0x01 },
				{ 0x11, 0x00, 0x04 },
				{ 0x11, 0x00, 0x05 },
				{ 0x11, 0x00, 0x06 },
				{ 0x11, 0x00, 0x07 },
				{ 0x11, 0x00, 0x08 },
				{ 0x11, 0x00, 0x0B },
				{ 0x11, 0x00, 0x0D },
				{ 0x11, 0x00, 0x09 },
				{ 0x11, 0x00, 0x15 },
				{ 0x11, 0x00, 0x13 },
				{ 0x11, 0x00, 0x11 },
				{ 0x11, 0x00, 0x0F },
				{ 0x11, 0x00, 0x1C },
				{ 0x11, 0x00, 0x26 },
				{ 0x11, 0x00, 0x30 },
				{ 0x11, 0x00, 0x3A },
				{ 0x11, 0x00, 0x02 },
				{ 0x11, 0x00, 0x03 },

			},
			// -------------  Filter 2 -----------------------

			{
				{ 0x11, 0x10, 0x19 },
				{ 0x11, 0x10, 0x1E },
				{ 0x11, 0x10, 0x23 },
				{ 0x11, 0x10, 0x28 },
				{ 0x11, 0x10, 0x2D },
				{ 0x11, 0x10, 0x32 },
				{ 0x11, 0x10, 0x37 },
				{ 0x11, 0x10, 0x3C },
				{ 0x11, 0x10, 0x16 },
				{ 0x11, 0x10, 0x20 },
				{ 0x11, 0x10, 0x2A },
				{ 0x11, 0x10, 0x34 },
				{ 0x11, 0x10, 0x18 },
				{ 0x11, 0x10, 0x22 },
				{ 0x11, 0x10, 0x2C },
				{ 0x11, 0x10, 0x36 },
				{ 0x11, 0x10, 0x1B },
				{ 0x11, 0x10, 0x25 },
				{ 0x11, 0x10, 0x2F },
				{ 0x11, 0x10, 0x39 },
				{ 0x11, 0x10, 0x00 },
				{ 0x11, 0x10, 0x01 },
				{ 0x11, 0x10, 0x04 },
				{ 0x11, 0x10, 0x05 },
				{ 0x11, 0x10, 0x06 },
				{ 0x11, 0x10, 0x07 },
				{ 0x11, 0x10, 0x08 },
				{ 0x11, 0x10, 0x0B },
				{ 0x11, 0x10, 0x0D },
				{ 0x11, 0x10, 0x09 },
				{ 0x11, 0x10, 0x15 },
				{ 0x11, 0x10, 0x13 },
				{ 0x11, 0x10, 0x11 },
				{ 0x11, 0x10, 0x0F },
				{ 0x11, 0x10, 0x1C },
				{ 0x11, 0x10, 0x26 },
				{ 0x11, 0x10, 0x30 },
				{ 0x11, 0x10, 0x3A },
				{ 0x11, 0x10, 0x02 },
				{ 0x11, 0x10, 0x03 },
			},
			// -------------  Filter 3 -----------------------

			{
				{ 0x11, 0x20, 0x19 },
				{ 0x11, 0x20, 0x1E },
				{ 0x11, 0x20, 0x23 },
				{ 0x11, 0x20, 0x28 },
				{ 0x11, 0x20, 0x2D },
				{ 0x11, 0x20, 0x32 },
				{ 0x11, 0x20, 0x37 },
				{ 0x11, 0x20, 0x3C },
				{ 0x11, 0x20, 0x16 },
				{ 0x11, 0x20, 0x20 },
				{ 0x11, 0x20, 0x2A },
				{ 0x11, 0x20, 0x34 },
				{ 0x11, 0x20, 0x18 },
				{ 0x11, 0x20, 0x22 },
				{ 0x11, 0x20, 0x2C },
				{ 0x11, 0x20, 0x36 },
				{ 0x11, 0x20, 0x1B },
				{ 0x11, 0x20, 0x25 },
				{ 0x11, 0x20, 0x2F },
				{ 0x11, 0x20, 0x39 },
				{ 0x11, 0x20, 0x00 },
				{ 0x11, 0x20, 0x01 },
				{ 0x11, 0x20, 0x04 },
				{ 0x11, 0x20, 0x05 },
				{ 0x11, 0x20, 0x06 },
				{ 0x11, 0x20, 0x07 },
				{ 0x11, 0x20, 0x08 },
				{ 0x11, 0x20, 0x0B },
				{ 0x11, 0x20, 0x0D },
				{ 0x11, 0x20, 0x09 },
				{ 0x11, 0x20, 0x15 },
				{ 0x11, 0x20, 0x13 },
				{ 0x11, 0x20, 0x11 },
				{ 0x11, 0x20, 0x0F },
				{ 0x11, 0x20, 0x1C },
				{ 0x11, 0x20, 0x26 },
				{ 0x11, 0x20, 0x30 },
				{ 0x11, 0x20, 0x3A },
				{ 0x11, 0x20, 0x02 },
				{ 0x11, 0x20, 0x03 },
			},
			// -------------  Filter 4 -----------------------

			{
				{ 0x11, 0x30, 0x19 },
				{ 0x11, 0x30, 0x1E },
				{ 0x11, 0x30, 0x23 },
				{ 0x11, 0x30, 0x28 },
				{ 0x11, 0x30, 0x2D },
				{ 0x11, 0x30, 0x32 },
				{ 0x11, 0x30, 0x37 },
				{ 0x11, 0x30, 0x3C },
				{ 0x11, 0x30, 0x16 },
				{ 0x11, 0x30, 0x20 },
				{ 0x11, 0x30, 0x2A },
				{ 0x11, 0x30, 0x34 },
				{ 0x11, 0x30, 0x18 },
				{ 0x11, 0x30, 0x22 },
				{ 0x11, 0x30, 0x2C },
				{ 0x11, 0x30, 0x36 },
				{ 0x11, 0x30, 0x1B },
				{ 0x11, 0x30, 0x25 },
				{ 0x11, 0x30, 0x2F },
				{ 0x11, 0x30, 0x39 },
				{ 0x11, 0x30, 0x00 },
				{ 0x11, 0x30, 0x01 },
				{ 0x11, 0x30, 0x04 },
				{ 0x11, 0x30, 0x05 },
				{ 0x11, 0x30, 0x06 },
				{ 0x11, 0x30, 0x07 },
				{ 0x11, 0x30, 0x08 },
				{ 0x11, 0x30, 0x0B },
				{ 0x11, 0x30, 0x0D },
				{ 0x11, 0x30, 0x09 },
				{ 0x11, 0x30, 0x15 },
				{ 0x11, 0x30, 0x13 },
				{ 0x11, 0x30, 0x11 },
				{ 0x11, 0x30, 0x0F },
				{ 0x11, 0x30, 0x1C },
				{ 0x11, 0x30, 0x26 },
				{ 0x11, 0x30, 0x30 },
				{ 0x11, 0x30, 0x3A },
				{ 0x11, 0x30, 0x02 },
				{ 0x11, 0x30, 0x03 },
			}, 
		},
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 1, },
		{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 630, 0, 0, 11, 0, 1060, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 630, 0, 0, 12, 0, 1060, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 630, 0, 0, 13, 0, 1060, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 630, 0, 0, 14, 0, 1060, 0, 0, 0, 0, 0 }
		}

	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	InsertFX
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::InsertFX, 4,
		{
			{ PortType::None,		0,	insFxMode,			ValueType::Enum,	(EnumListType*)&InserFXModeNames	},	// mode
			{ PortType::None,		0,	insFxValue, 		ValueType::Num,		{-63, 63}							},	// Value
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 level (amount)
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 level (amount)
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&insertFx0SourceList },	// inp 0 source
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		}	// control 0 source
		},
		{
			// -------------  InsertFX 1 -----------------------

			{
				{ 0x11, 0x50, 0x00 },
				{ 0x11, 0x50, 0x03 },
				{ 0x11, 0x50, 0x09 },
				{ 0x11, 0x50, 0x06 },
				{ 0x11, 0x50, 0x01 },
				{ 0x11, 0x50, 0x04 },
				{ 0x11, 0x50, 0x08 },
			},
			// -------------  InsertFX 2 -----------------------

			{
				{ 0x11, 0x50, 0x0B },
				{ 0x11, 0x50, 0x0E },
				{ 0x11, 0x50, 0x14 },
				{ 0x11, 0x50, 0x11 },
				{ 0x11, 0x50, 0x0C },
				{ 0x11, 0x50, 0x0F },
				{ 0x11, 0x50, 0x13 },
			},
			// -------------  InsertFX 3 -----------------------

			{
				{ 0x11, 0x50, 0x16 },
				{ 0x11, 0x50, 0x19 },
				{ 0x11, 0x50, 0x1F },
				{ 0x11, 0x50, 0x1C },
				{ 0x11, 0x50, 0x17 },
				{ 0x11, 0x50, 0x1A },
				{ 0x11, 0x50, 0x1E },
			},
			// -------------  InsertFX 4 -----------------------

			{
				{ 0x11, 0x50, 0x21 },
				{ 0x11, 0x50, 0x24 },
				{ 0x11, 0x50, 0x2A },
				{ 0x11, 0x50, 0x27 },
				{ 0x11, 0x50, 0x22 },
				{ 0x11, 0x50, 0x25 },
				{ 0x11, 0x50, 0x29 },
			},
		},
		{ 1, 1, 2, 2, 2, 2, 1, },

	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	VCA
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::VCA, 4,
		{
			{ PortType::None,		0,	vcaType,			ValueType::Enum,	(EnumListType*)&VCATypeNames		},	// type
			{ PortType::None,		0,	vcaBoost,			ValueType::Num,		{0, 127}							},	// boost
			{ PortType::None,		0,	vcaLevel,			ValueType::Num,		{0, 100}							},	// level
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 level (amount)
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&vca0SourceList	},	// inp 0 source
			{ PortType::None,		0,	vcaInitPan,			ValueType::Num,		{-63, 63}							},	// initial pan
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 level (amount)
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(SourceListType*)&levelDestList		},	// mod 0 dest (level)
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(SourceListType*)&panDestList		}	// mod 1 dest (pan)
		},
		{
			// -------------  VCA 1 -----------------------

			{
				{ 0x10, 0x70, 0x02 },
				{ 0x10, 0x70, 0x03 },
				{ 0x10, 0x70, 0x05 },
				{ 0x10, 0x70, 0x0A },
				{ 0x10, 0x70, 0x08 },
				{ 0x10, 0x70, 0x00 },
				{ 0x10, 0x70, 0x07 },
				{ 0x10, 0x70, 0x0E },
				{ 0x10, 0x70, 0x0C },
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
			},
			// -------------  VCA 2 -----------------------

			{
				{ 0x10, 0x70, 0x12 },
				{ 0x10, 0x70, 0x13 },
				{ 0x10, 0x70, 0x15 },
				{ 0x10, 0x70, 0x1A },
				{ 0x10, 0x70, 0x18 },
				{ 0x10, 0x70, 0x10 },
				{ 0x10, 0x70, 0x17 },
				{ 0x10, 0x70, 0x1E },
				{ 0x10, 0x70, 0x1C },
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
			},
			// -------------  VCA 3 -----------------------

			{
				{ 0x10, 0x70, 0x22 },
				{ 0x10, 0x70, 0x23 },
				{ 0x10, 0x70, 0x25 },
				{ 0x10, 0x70, 0x2A },
				{ 0x10, 0x70, 0x28 },
				{ 0x10, 0x70, 0x20 },
				{ 0x10, 0x70, 0x27 },
				{ 0x10, 0x70, 0x2E },
				{ 0x10, 0x70, 0x2C },
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
			},
			// -------------  VCA 4 -----------------------

			{
				{ 0x10, 0x70, 0x32 },
				{ 0x10, 0x70, 0x33 },
				{ 0x10, 0x70, 0x35 },
				{ 0x10, 0x70, 0x3A },
				{ 0x10, 0x70, 0x38 },
				{ 0x10, 0x70, 0x30 },
				{ 0x10, 0x70, 0x37 },
				{ 0x10, 0x70, 0x3E },
				{ 0x10, 0x70, 0x3C },
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
				{ 0xFF, 0xFF, 0xFF },	// There is no actual mod destination parameter since the destination is fixed.
			}
		},
		{ 1, 2, 2, 2, 2, 2, 1, 2, 2, 0, 0, },
		{
			{ 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	VCA Master
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::VCAMaster, 1,
		{
			// We treat an Enable Part parameter as an input source to the VCA Master because that's essentually what it is.
			// Toggling Enable Part connects or disconnects the VCA from the VCA Master.
			//
			// It's not a mistake that the VCAMaster input sources value types are Num here even though they have a list of sources.
			// They have to be Num to get interpreted correctly when retrieved from the Solaris.
			{ PortType::Input,		0,	psiSource,			ValueType::Num,		(SourceListType*)&vcaMasterSourceList0		},	// inp 0 source (enable part 1)
			{ PortType::Input,		0,	psiAmount,			ValueType::Num,		{0, 0}										},	// inp 0 level
			{ PortType::Input,		1,	psiSource,			ValueType::Num,		(SourceListType*)&vcaMasterSourceList1		},	// inp 1 source (enable part 2)
			{ PortType::Input,		1,	psiAmount,			ValueType::Num,		{0, 0}										},	// inp 1 level
			{ PortType::Input,		2,	psiSource,			ValueType::Num,		(SourceListType*)&vcaMasterSourceList2		},	// inp 2 source (enable part 3)
			{ PortType::Input,		2,	psiAmount,			ValueType::Num,		{0, 0}										},	// inp 2 level
			{ PortType::Input,		3,	psiSource,			ValueType::Num,		(SourceListType*)&vcaMasterSourceList3		},	// inp 3 source (enable part 4)
			{ PortType::Input,		3,	psiAmount,			ValueType::Num,		{0, 0}										},	// inp 3 level
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&vcaMasterModSourceList	},	// mod 0 source
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{0, 0}										},	// mod 0 level (amount)
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(SourceListType*)&levelDestList				},	// mod 0 dest
		},
		{
			{
				{ 0x10, 0x70, 0x40 },
				{ 0xFF, 0xFF, 0xFF },
				{ 0x10, 0x70, 0x41 },
				{ 0xFF, 0xFF, 0xFF },
				{ 0x10, 0x70, 0x42 },
				{ 0xFF, 0xFF, 0xFF },
				{ 0x10, 0x70, 0x43 },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, },
		{
			{ 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 } 
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	LFO
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::LFO, 4,
		{
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 strength
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 amount
			{ PortType::Control,	1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 1 strength
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 amount
			{ PortType::Control,	2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 2 strength
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 2 amount
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 0 source
			{ PortType::Control,	1,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 1 source
			{ PortType::Control,	2,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 2 source
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 0 dest
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 1 dest
			{ PortType::Mod,		2,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 2 dest
			{ PortType::None,		0,	lfoRate,			ValueType::Num,		{0, 50000, divisor100}				},	// rate
			{ PortType::None,		0,	lfoShape,			ValueType::Enum,	(EnumListType*)&LFOTypeNames		},	// shape
			{ PortType::None,		0,	lfoPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	lfoRetrigger,		ValueType::Num,		{0, 1}								},	// retrigger
			{ PortType::None,		0,	lfoFadeIn,			ValueType::Num,		{0, 100, divisor10}				},	// fade in
			{ PortType::None,		0,	lfoFadeOut,			ValueType::Num,		{0, 100, divisor10}				},	// fade out
			{ PortType::None,		0,	lfoDelayStart,		ValueType::Num,		{0, 100, divisor10}				},	// delay start
			{ PortType::None,		0,	lfoOffset,			ValueType::Num,		{0, 1}								},	// offset
			{ PortType::None,		0,	lfoLevel,			ValueType::Num,		{0, 100}							},	// level
			{ PortType::None,		0,	lfoClockSync,		ValueType::Num,		{0, 1}								},	// clock sync
			{ PortType::None,		0,	lfoRateSync,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames},	// rate when ClockSync is enabled
			{ PortType::Mod,		0,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 0 amount pitch
			{ PortType::Mod,		1,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 1 amount pitch
			{ PortType::Mod,		2,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			}	// mod 2 amount pitch

		},
		{
			// -------------  LFO 1 -----------------------

			{
				{ 0x12, 0x00, 0x15 },
				{ 0x12, 0x00, 0x1A },
				{ 0x12, 0x00, 0x1F },
				{ 0x12, 0x00, 0x24 },
				{ 0x12, 0x00, 0x29 },
				{ 0x12, 0x00, 0x2E },
				{ 0x12, 0x00, 0x12 },
				{ 0x12, 0x00, 0x1C },
				{ 0x12, 0x00, 0x26 },
				{ 0x12, 0x00, 0x14 },
				{ 0x12, 0x00, 0x1E },
				{ 0x12, 0x00, 0x28 },
				{ 0x12, 0x00, 0x17 },
				{ 0x12, 0x00, 0x21 },
				{ 0x12, 0x00, 0x2B },
				{ 0x12, 0x00, 0x01 },
				{ 0x12, 0x00, 0x00 },
				{ 0x12, 0x00, 0x05 },
				{ 0x12, 0x00, 0x11 },
				{ 0x12, 0x00, 0x09 },
				{ 0x12, 0x00, 0x0B },
				{ 0x12, 0x00, 0x07 },
				{ 0x12, 0x00, 0x10 },
				{ 0x12, 0x00, 0x0D },
				{ 0x12, 0x00, 0x0F },
				{ 0x12, 0x00, 0x04 },
				{ 0x12, 0x00, 0x18 },
				{ 0x12, 0x00, 0x22 },
				{ 0x12, 0x00, 0x2C },
			},
			// -------------  LFO 2 -----------------------

			{
				{ 0x12, 0x10, 0x15 },
				{ 0x12, 0x10, 0x1A },
				{ 0x12, 0x10, 0x1F },
				{ 0x12, 0x10, 0x24 },
				{ 0x12, 0x10, 0x29 },
				{ 0x12, 0x10, 0x2E },
				{ 0x12, 0x10, 0x12 },
				{ 0x12, 0x10, 0x1C },
				{ 0x12, 0x10, 0x26 },
				{ 0x12, 0x10, 0x14 },
				{ 0x12, 0x10, 0x1E },
				{ 0x12, 0x10, 0x28 },
				{ 0x12, 0x10, 0x17 },
				{ 0x12, 0x10, 0x21 },
				{ 0x12, 0x10, 0x2B },
				{ 0x12, 0x10, 0x01 },
				{ 0x12, 0x10, 0x00 },
				{ 0x12, 0x10, 0x05 },
				{ 0x12, 0x10, 0x11 },
				{ 0x12, 0x10, 0x09 },
				{ 0x12, 0x10, 0x0B },
				{ 0x12, 0x10, 0x07 },
				{ 0x12, 0x10, 0x10 },
				{ 0x12, 0x10, 0x0D },
				{ 0x12, 0x10, 0x0F },
				{ 0x12, 0x10, 0x04 },
				{ 0x12, 0x10, 0x18 },
				{ 0x12, 0x10, 0x22 },
				{ 0x12, 0x10, 0x2C },
			},
			// -------------  LFO 3 -----------------------

			{
				{ 0x12, 0x20, 0x15 },
				{ 0x12, 0x20, 0x1A },
				{ 0x12, 0x20, 0x1F },
				{ 0x12, 0x20, 0x24 },
				{ 0x12, 0x20, 0x29 },
				{ 0x12, 0x20, 0x2E },
				{ 0x12, 0x20, 0x12 },
				{ 0x12, 0x20, 0x1C },
				{ 0x12, 0x20, 0x26 },
				{ 0x12, 0x20, 0x14 },
				{ 0x12, 0x20, 0x1E },
				{ 0x12, 0x20, 0x28 },
				{ 0x12, 0x20, 0x17 },
				{ 0x12, 0x20, 0x21 },
				{ 0x12, 0x20, 0x2B },
				{ 0x12, 0x20, 0x01 },
				{ 0x12, 0x20, 0x00 },
				{ 0x12, 0x20, 0x05 },
				{ 0x12, 0x20, 0x11 },
				{ 0x12, 0x20, 0x09 },
				{ 0x12, 0x20, 0x0B },
				{ 0x12, 0x20, 0x07 },
				{ 0x12, 0x20, 0x10 },
				{ 0x12, 0x20, 0x0D },
				{ 0x12, 0x20, 0x0F },
				{ 0x12, 0x20, 0x04 },
				{ 0x12, 0x20, 0x18 },
				{ 0x12, 0x20, 0x22 },
				{ 0x12, 0x20, 0x2C },
			},
			// -------------  LFO 4 -----------------------

			{
				{ 0x12, 0x30, 0x15 },
				{ 0x12, 0x30, 0x1A },
				{ 0x12, 0x30, 0x1F },
				{ 0x12, 0x30, 0x24 },
				{ 0x12, 0x30, 0x29 },
				{ 0x12, 0x30, 0x2E },
				{ 0x12, 0x30, 0x12 },
				{ 0x12, 0x30, 0x1C },
				{ 0x12, 0x30, 0x26 },
				{ 0x12, 0x30, 0x14 },
				{ 0x12, 0x30, 0x1E },
				{ 0x12, 0x30, 0x28 },
				{ 0x12, 0x30, 0x17 },
				{ 0x12, 0x30, 0x21 },
				{ 0x12, 0x30, 0x2B },
				{ 0x12, 0x30, 0x01 },
				{ 0x12, 0x30, 0x00 },
				{ 0x12, 0x30, 0x05 },
				{ 0x12, 0x30, 0x11 },
				{ 0x12, 0x30, 0x09 },
				{ 0x12, 0x30, 0x0B },
				{ 0x12, 0x30, 0x07 },
				{ 0x12, 0x30, 0x10 },
				{ 0x12, 0x30, 0x0D },
				{ 0x12, 0x30, 0x0F },
				{ 0x12, 0x30, 0x04 },
				{ 0x12, 0x30, 0x18 },
				{ 0x12, 0x30, 0x22 },
				{ 0x12, 0x30, 0x2C },
			}, 
		},
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 2, 2, },
		{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 100, 0, 4, 0, 0, 0 }
		}

	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Vibrato LFO
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::VibratoLFO, 1,
		{
			{ PortType::Control,	0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 0 strength
			{ PortType::Mod,		0,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 0 amount
			{ PortType::Control,	1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 1 strength
			{ PortType::Mod,		1,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 1 amount
			{ PortType::Control,	2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// control 2 strength
			{ PortType::Mod,		2,	psiAmount,			ValueType::Num,		{-100, 100}							},	// mod 2 amount
			{ PortType::Mod,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,			ValueType::Enum,	(SourceListType*)&modSourceList		},	// mod 2 source
			{ PortType::Control,	0,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 0 source
			{ PortType::Control,	1,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 1 source
			{ PortType::Control,	2,	psiSource,			ValueType::Enum,	(SourceListType*)&ctlSourceList		},	// control 2 source
			{ PortType::Mod,		0,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 0 dest
			{ PortType::Mod,		1,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 1 dest
			{ PortType::Mod,		2,	psiDest,			ValueType::Enum,	(EnumListType*)&lfoModDestList		},	// mod 2 dest
			{ PortType::None,		0,	lfoRate,			ValueType::Num,		{0, 50000, divisor100}				},	// rate
			{ PortType::None,		0,	lfoShape,			ValueType::Enum,	(EnumListType*)&LFOTypeNames		},	// shape
			{ PortType::None,		0,	lfoPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	lfoRetrigger,		ValueType::Num,		{0, 1}								},	// retrigger
			{ PortType::None,		0,	lfoFadeIn,			ValueType::Num,		{0, 100, divisor10}				},	// fade in
			{ PortType::None,		0,	lfoFadeOut,			ValueType::Num,		{0, 100, divisor10}				},	// fade out
			{ PortType::None,		0,	lfoDelayStart,		ValueType::Num,		{0, 100, divisor10}				},	// delay start
			{ PortType::None,		0,	lfoOffset,			ValueType::Num,		{0, 1}								},	// offset
			{ PortType::None,		0,	lfoLevel,			ValueType::Num,		{0, 100}							},	// level
			{ PortType::None,		0,	lfoClockSync,		ValueType::Num,		{0, 1}								},	// clock sync
			{ PortType::None,		0,	lfoRateSync,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames},	// rate when ClockSync is enabled
			{ PortType::Mod,		0,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 0 amount pitch
			{ PortType::Mod,		1,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 1 amount pitch
			{ PortType::Mod,		2,	psiAmtPitch,		ValueType::Num,		{-1200, 1200, divisor10}			},	// mod 2 amount pitch

			{ PortType::None,		0,	vlfoModWheel,		ValueType::Num,		{0, 1}								},	// ModWhl
			{ PortType::None,		0,	vlfoModWheelMax,	ValueType::Num,		{-100, 100}							}	// ModWhlMax


		},
		{
			{
				{ 0x12, 0x40, 0x15 },
				{ 0x12, 0x40, 0x1A },
				{ 0x12, 0x40, 0x1F },
				{ 0x12, 0x40, 0x24 },
				{ 0x12, 0x40, 0x29 },
				{ 0x12, 0x40, 0x2E },
				{ 0x12, 0x40, 0x12 },
				{ 0x12, 0x40, 0x1C },
				{ 0x12, 0x40, 0x26 },
				{ 0x12, 0x40, 0x14 },
				{ 0x12, 0x40, 0x1E },
				{ 0x12, 0x40, 0x28 },
				{ 0x12, 0x40, 0x17 },
				{ 0x12, 0x40, 0x21 },
				{ 0x12, 0x40, 0x2B },
				{ 0x12, 0x40, 0x01 },
				{ 0x12, 0x40, 0x00 },
				{ 0x12, 0x40, 0x05 },
				{ 0x12, 0x40, 0x11 },
				{ 0x12, 0x40, 0x09 },
				{ 0x12, 0x40, 0x0B },
				{ 0x12, 0x40, 0x07 },
				{ 0x12, 0x40, 0x10 },
				{ 0x12, 0x40, 0x0D },
				{ 0x12, 0x40, 0x0F },
				{ 0x12, 0x40, 0x04 },
				{ 0x12, 0x40, 0x18 },
				{ 0x12, 0x40, 0x22 },
				{ 0x12, 0x40, 0x2C },
				{ 0x17, 0x00, 0x32 },
				{ 0x17, 0x00, 0x33 },
			}, 
		},
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 2, },
		{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 600, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 7, 50 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	EG
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::EG, 6,
		{
			{ PortType::None,		0,	egDelay,		ValueType::Num,		{0, 200000, divisor10000}			},	// delay
			{ PortType::None,		0,	egAttack,		ValueType::Num,		{0, 200000, divisor10000}			},	// attack
			{ PortType::None,		0,	egDecay,		ValueType::Num,		{0, 200000, divisor10000}			},	// decay
			{ PortType::None,		0,	egSustain,		ValueType::Num,		{0, 127}							},	// sustain
			{ PortType::None,		0,	egRelease,		ValueType::Num,		{0, 200000, divisor10000}			},	// release
			{ PortType::None,		0,	egASlope,		ValueType::Num,		{0, 127}							},	// a-slope
			{ PortType::None,		0,	egDSlope,		ValueType::Num,		{0, 127}							},	// d-slope
			{ PortType::None,		0,	egSSlope,		ValueType::Num,		{-20000, 20000, divisor1000}		},	// s-slope
			{ PortType::None,		0,	egRSlope,		ValueType::Num,		{0, 127}							},	// r-slope
			{ PortType::None,		0,	egVelocity,		ValueType::Num,		{0, 127}							},	// velocity
			{ PortType::Mod,		0,	psiAmount,		ValueType::Num,		{-63, 63}							},	// mod 0 level (amount)
			{ PortType::Mod,		1,	psiAmount,		ValueType::Num,		{-63, 63}							},	// mod 1 level (amount)
			{ PortType::Mod,		2,	psiAmount,		ValueType::Num,		{-63, 63}							},	// mod 2 level (amount)
			{ PortType::Mod,		3,	psiAmount,		ValueType::Num,		{-63, 63}							},	// mod 3 level (amount)
			{ PortType::Mod,		0,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList	},	// mod 0 source
			{ PortType::Mod,		1,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList	},	// mod 1 source
			{ PortType::Mod,		2,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList	},	// mod 2 source
			{ PortType::Mod,		3,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList	},	// mod 3 source
		},
		{
			// -------------  EG 1 -----------------------
			{
				{ 0x13, 0x00, 0x00 },
				{ 0x13, 0x00, 0x03 },
				{ 0x13, 0x00, 0x06 },
				{ 0x13, 0x00, 0x09 },
				{ 0x13, 0x00, 0x0B },
				{ 0x13, 0x00, 0x0E },
				{ 0x13, 0x00, 0x10 },
				{ 0x13, 0x00, 0x12 },
				{ 0x13, 0x00, 0x15 },
				{ 0x13, 0x00, 0x1F },
				{ 0x13, 0x00, 0x1B },
				{ 0x13, 0x00, 0x1C },
				{ 0x13, 0x00, 0x1D },
				{ 0x13, 0x00, 0x1E },
				{ 0x13, 0x00, 0x17 },
				{ 0x13, 0x00, 0x18 },
				{ 0x13, 0x00, 0x19 },
				{ 0x13, 0x00, 0x1A },
			},
			// -------------  EG 2 -----------------------
			{
				{ 0x13, 0x10, 0x00 },
				{ 0x13, 0x10, 0x03 },
				{ 0x13, 0x10, 0x06 },
				{ 0x13, 0x10, 0x09 },
				{ 0x13, 0x10, 0x0B },
				{ 0x13, 0x10, 0x0E },
				{ 0x13, 0x10, 0x10 },
				{ 0x13, 0x10, 0x12 },
				{ 0x13, 0x10, 0x15 },
				{ 0x13, 0x10, 0x1F },
				{ 0x13, 0x10, 0x1B },
				{ 0x13, 0x10, 0x1C },
				{ 0x13, 0x10, 0x1D },
				{ 0x13, 0x10, 0x1E },
				{ 0x13, 0x10, 0x17 },
				{ 0x13, 0x10, 0x18 },
				{ 0x13, 0x10, 0x19 },
				{ 0x13, 0x10, 0x1A },
			},
			// -------------  EG 3 -----------------------
			{
				{ 0x13, 0x20, 0x00 },
				{ 0x13, 0x20, 0x03 },
				{ 0x13, 0x20, 0x06 },
				{ 0x13, 0x20, 0x09 },
				{ 0x13, 0x20, 0x0B },
				{ 0x13, 0x20, 0x0E },
				{ 0x13, 0x20, 0x10 },
				{ 0x13, 0x20, 0x12 },
				{ 0x13, 0x20, 0x15 },
				{ 0x13, 0x20, 0x1F },
				{ 0x13, 0x20, 0x1B },
				{ 0x13, 0x20, 0x1C },
				{ 0x13, 0x20, 0x1D },
				{ 0x13, 0x20, 0x1E },
				{ 0x13, 0x20, 0x17 },
				{ 0x13, 0x20, 0x18 },
				{ 0x13, 0x20, 0x19 },
				{ 0x13, 0x20, 0x1A },
			},
			// -------------  EG 4 -----------------------
			{
				{ 0x13, 0x30, 0x00 },
				{ 0x13, 0x30, 0x03 },
				{ 0x13, 0x30, 0x06 },
				{ 0x13, 0x30, 0x09 },
				{ 0x13, 0x30, 0x0B },
				{ 0x13, 0x30, 0x0E },
				{ 0x13, 0x30, 0x10 },
				{ 0x13, 0x30, 0x12 },
				{ 0x13, 0x30, 0x15 },
				{ 0x13, 0x30, 0x1F },
				{ 0x13, 0x30, 0x1B },
				{ 0x13, 0x30, 0x1C },
				{ 0x13, 0x30, 0x1D },
				{ 0x13, 0x30, 0x1E },
				{ 0x13, 0x30, 0x17 },
				{ 0x13, 0x30, 0x18 },
				{ 0x13, 0x30, 0x19 },
				{ 0x13, 0x30, 0x1A },
			},
			// -------------  EG 5 -----------------------
			{
				{ 0x13, 0x40, 0x00 },
				{ 0x13, 0x40, 0x03 },
				{ 0x13, 0x40, 0x06 },
				{ 0x13, 0x40, 0x09 },
				{ 0x13, 0x40, 0x0B },
				{ 0x13, 0x40, 0x0E },
				{ 0x13, 0x40, 0x10 },
				{ 0x13, 0x40, 0x12 },
				{ 0x13, 0x40, 0x15 },
				{ 0x13, 0x40, 0x1F },
				{ 0x13, 0x40, 0x1B },
				{ 0x13, 0x40, 0x1C },
				{ 0x13, 0x40, 0x1D },
				{ 0x13, 0x40, 0x1E },
				{ 0x13, 0x40, 0x17 },
				{ 0x13, 0x40, 0x18 },
				{ 0x13, 0x40, 0x19 },
				{ 0x13, 0x40, 0x1A },
			},
			// -------------  EG 6 -----------------------

			{
				{ 0x13, 0x50, 0x00 },
				{ 0x13, 0x50, 0x03 },
				{ 0x13, 0x50, 0x06 },
				{ 0x13, 0x50, 0x09 },
				{ 0x13, 0x50, 0x0B },
				{ 0x13, 0x50, 0x0E },
				{ 0x13, 0x50, 0x10 },
				{ 0x13, 0x50, 0x12 },
				{ 0x13, 0x50, 0x15 },
				{ 0x13, 0x50, 0x1F },
				{ 0x13, 0x50, 0x1B },
				{ 0x13, 0x50, 0x1C },
				{ 0x13, 0x50, 0x1D },
				{ 0x13, 0x50, 0x1E },
				{ 0x13, 0x50, 0x17 },
				{ 0x13, 0x50, 0x18 },
				{ 0x13, 0x50, 0x19 },
				{ 0x13, 0x50, 0x1A },
			},
		},
		{ 3, 3, 3, 2, 3, 2, 2, 3, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, },
		{
			{ 0, 0, 20000, 20, 5100, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 127, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 127, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 127, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 95, 90, 5000, 0, 127, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Loop EG
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::LoopEG, 1,
		{
			{ PortType::None,		0,	legTime1,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 1
			{ PortType::None,		0,	legXLevel1,		ValueType::Num,		{-127,127}								},	// level X1
			{ PortType::None,		0,	legYLevel1,		ValueType::Num,		{-127,127}								},	// level Y1

			{ PortType::None,		0,	legTime2,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 2
			{ PortType::None,		0,	legXLevel2,		ValueType::Num,		{-127,127}								},	// level X2
			{ PortType::None,		0,	legYLevel2,		ValueType::Num,		{-127,127}								},	// level Y2

			{ PortType::None,		0,	legTime3,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 3
			{ PortType::None,		0,	legXLevel3,		ValueType::Num,		{-127,127}								},	// level X3
			{ PortType::None,		0,	legYLevel3,		ValueType::Num,		{-127,127}								},	// level Y3

			{ PortType::None,		0,	legTime4,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 4
			{ PortType::None,		0,	legXLevel4,		ValueType::Num,		{-127,127}								},	// level X4
			{ PortType::None,		0,	legYLevel4,		ValueType::Num,		{-127,127}								},	// level Y4

			{ PortType::None,		0,	legTime5,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 5
			{ PortType::None,		0,	legXLevel5,		ValueType::Num,		{-127,127}								},	// level X5
			{ PortType::None,		0,	legYLevel5,		ValueType::Num,		{-127,127}								},	// level Y5

			{ PortType::None,		0,	legTime6,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 6
			{ PortType::None,		0,	legXLevel6,		ValueType::Num,		{-127,127}								},	// level X6
			{ PortType::None,		0,	legYLevel6,		ValueType::Num,		{-127,127}								},	// level Y6

			{ PortType::None,		0,	legTime7,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 7
			{ PortType::None,		0,	legXLevel7,		ValueType::Num,		{-127,127}								},	// level X7
			{ PortType::None,		0,	legYLevel7,		ValueType::Num,		{-127,127}								},	// level Y7

			{ PortType::None,		0,	legTime8,		ValueType::Num,		{0, 200000, divisor10000}				},	// time 8
			{ PortType::None,		0,	legXLevel8,		ValueType::Num,		{-127,127}								},	// level X8
			{ PortType::None,		0,	legYLevel8,		ValueType::Num,		{-127,127}								},	// level Y8

			{ PortType::None,		0,	legSlope,		ValueType::Num,		{0, 127}								},	// slope
			{ PortType::None,		0,	legLoop,		ValueType::Enum,	{0, 1}									},	// loop
			{ PortType::None,		0,	legRepeat,		ValueType::Enum,	(EnumListType*)&LoopEgRepeat			},	// repeat
			{ PortType::None,		0,	legStart,		ValueType::Enum,	(EnumListType*)&LoopEgSegments			},	// start
			{ PortType::None,		0,	legKeyOff,		ValueType::Enum,	(EnumListType*)&LoopEgSegments			},	// KeyOff

			{ PortType::Mod,		0,	psiAmount,		ValueType::Num,		{-300, 300}								},	// mod 0 amount (level)
			{ PortType::Mod,		1,	psiAmount,		ValueType::Num,		{-300, 300}								},	// mod 1 amount (time)
			{ PortType::Mod,		0,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList		},	// mod 0 source (level)
			{ PortType::Mod,		1,	psiSource,		ValueType::Enum,	(SourceListType*)&egModSourceList		},	// mod 1 source (time)
			{ PortType::None,		0,	legSync,		ValueType::Enum,	{0, 1}									},	// sync

			{ PortType::None,		0,	legTime1Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 1 with sync
			{ PortType::None,		0,	legTime2Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 2 with sync
			{ PortType::None,		0,	legTime3Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 3 with sync
			{ PortType::None,		0,	legTime4Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 4 with sync
			{ PortType::None,		0,	legTime5Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 5 with sync
			{ PortType::None,		0,	legTime6Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 6 with sync
			{ PortType::None,		0,	legTime7Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// time 7 with sync
			{ PortType::None,		0,	legTime8Sync,	ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	}	// time 8 with sync
		},
		{
			{
				{ 0x13, 0x70, 0x0E },
				{ 0x13, 0x70, 0x12 },
				{ 0x13, 0x70, 0x14 },
				{ 0x13, 0x70, 0x16 },
				{ 0x13, 0x70, 0x1A },
				{ 0x13, 0x70, 0x1C },
				{ 0x13, 0x70, 0x1E },
				{ 0x13, 0x70, 0x22 },
				{ 0x13, 0x70, 0x24 },
				{ 0x13, 0x70, 0x26 },
				{ 0x13, 0x70, 0x2A },
				{ 0x13, 0x70, 0x2C },
				{ 0x13, 0x70, 0x2E },
				{ 0x13, 0x70, 0x32 },
				{ 0x13, 0x70, 0x34 },
				{ 0x13, 0x70, 0x36 },
				{ 0x13, 0x70, 0x3A },
				{ 0x13, 0x70, 0x3C },
				{ 0x13, 0x70, 0x3E },
				{ 0x13, 0x70, 0x42 },
				{ 0x13, 0x70, 0x44 },
				{ 0x13, 0x70, 0x46 },
				{ 0x13, 0x70, 0x4A },
				{ 0x13, 0x70, 0x4C },
				{ 0x13, 0x70, 0x02 },
				{ 0x13, 0x70, 0x07 },
				{ 0x13, 0x70, 0x05 },
				{ 0x13, 0x70, 0x00 },
				{ 0x13, 0x70, 0x01 },
				{ 0x13, 0x70, 0x09 },
				{ 0x13, 0x70, 0x0C },
				{ 0x13, 0x70, 0x08 },
				{ 0x13, 0x70, 0x0B },
				{ 0x13, 0x70, 0x04 },
				{ 0x13, 0x70, 0x11 },
				{ 0x13, 0x70, 0x19 },
				{ 0x13, 0x70, 0x21 },
				{ 0x13, 0x70, 0x29 },
				{ 0x13, 0x70, 0x31 },
				{ 0x13, 0x70, 0x39 },
				{ 0x13, 0x70, 0x41 },
				{ 0x13, 0x70, 0x49 },
			},
		},
		{ 3, 2, 2, 3, 2, 2, 3, 2, 2, 3, 2, 2, 3, 2, 2, 3, 2, 2, 3, 2, 2, 3, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, },
		{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		}

	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Lag
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Lag, 4,
		{
			{ PortType::None,		0,	lagTime,		ValueType::Num,		{0, 100000, divisor10000}			},		// time
			{ PortType::Input,		0,	psiSource,		ValueType::Enum,	(SourceListType*)&modSourceList	}		// inp 0 source
		},
		{
			// -------------  Lag 1 -----------------------

			{
				{ 0x10, 0x70, 0x46 },
				{ 0x10, 0x70, 0x44 },
			},
			// -------------  Lag 2 -----------------------

			{
				{ 0x10, 0x70, 0x4B },
				{ 0x10, 0x70, 0x49 },
			},
			// -------------  Lag 3 -----------------------

			{
				{ 0x10, 0x70, 0x50 },
				{ 0x10, 0x70, 0x4E },
			},
			// -------------  Lag 4 -----------------------

			{
				{ 0x10, 0x70, 0x55 },
				{ 0x10, 0x70, 0x53 },
			},
		},
		{ 3, 2, },
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Envelope Follower
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::EnvFol, 1,
		{
			{ PortType::None,		0,	envFolInput,		ValueType::Num,		{0, 100}							},		// input level
			{ PortType::None,		0,	envFolOutput,		ValueType::Num,		{0, 100}							},		// output level
			{ PortType::None,		0,	envFolAttack,		ValueType::Num,		{0, 200000, divisor10000}			},		// attack time
			{ PortType::None,		0,	envFolRelease,		ValueType::Num,		{0, 200000, divisor10000}			},		// release time
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&audioSourceList	}		// inp 0 source
		},
		{
			{
				{ 0x10, 0x70, 0x5A },
				{ 0x10, 0x70, 0x5C },
				{ 0x10, 0x70, 0x5E },
				{ 0x10, 0x70, 0x61 },
				{ 0x10, 0x70, 0x58 },
			},
		},
		{ 2, 2, 3, 3, 2, },
		{
			{ 100, 100, 0, 7000, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	FX Channel
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::FXChannel, 4,
		{
			{ PortType::None,		0,	fxChannelSlot1,		ValueType::Enum,	(EnumListType*)&effectNames				},	// Slot 1
			{ PortType::None,		0,	fxChannelSlot2,		ValueType::Enum,	(EnumListType*)&effectNames				},	// Slot 2
			{ PortType::None,		0,	fxChannelSlot3,		ValueType::Enum,	(EnumListType*)&effectNames				},	// Slot 3
			{ PortType::None,		0,	fxChannelSlot4,		ValueType::Enum,	(EnumListType*)&effectNames				},	// Slot 4
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&OutputSourceList		}	// inp 0 source
		},
		{
			// -------------  FXChannel 1 -----------------------

			{
				{ 0x15, 0x00, 0x01 },
				{ 0x15, 0x00, 0x02 },
				{ 0x15, 0x00, 0x03 },
				{ 0x15, 0x00, 0x04 },
				{ 0x15, 0x00, 0x00 },
			},
			// -------------  FXChannel 2 -----------------------

			{
				{ 0x15, 0x00, 0x06 },
				{ 0x15, 0x00, 0x07 },
				{ 0x15, 0x00, 0x08 },
				{ 0x15, 0x00, 0x09 },
				{ 0x15, 0x00, 0x05 },
			},
			// -------------  FXChannel 3 -----------------------

			{
				{ 0x15, 0x00, 0x0B },
				{ 0x15, 0x00, 0x0C },
				{ 0x15, 0x00, 0x0D },
				{ 0x15, 0x00, 0x0E },
				{ 0x15, 0x00, 0x0A },
			},
			// -------------  FXChannel 4 -----------------------

			{
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 1, 1, 1, 1, 1, },
		{
			{ 1, 2, 3, 4, 1},
			{ 0, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0}
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Outputs
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Output, 4,
		{
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&OutputSourceList	}	// inp 0 source
		},
		{
			// -------------  Output 1 -----------------------
			{
				{ 0x17, 0x00, 0x63 },
			},
			// -------------  Output 2 -----------------------
			{
				{ 0x17, 0x00, 0x64 },
			},
			// -------------  Output 3 -----------------------
			{
				{ 0x17, 0x00, 0x65 },
			},
			// -------------  Output 4 -----------------------
			{
				{ 0x17, 0x00, 0x66 },
			},
		},
		{ 1 },
		{
			{ 5 }, { 0 }, { 0 }, { 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	S/P-DIF
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::SPDIF_Out, 1,
		{
			{ PortType::Input,		0,	psiSource,			ValueType::Enum,	(SourceListType*)&OutputSourceList }	// inp 0 source
		},
		{
			{
				{ 0x17, 0x00, 0x67 },
			},
		},
		{ 1 },
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Ribbon
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Ribbon, 1,
		{
			{ PortType::None,		0,	ribIntensity,		ValueType::Num,		{0, 200}							},	// intensity
			{ PortType::None,		0,	ribOffset,			ValueType::Num,		{0, 100}							},	// offset
			{ PortType::None,		0,	ribHold,			ValueType::Num,		{0, 1}								},	// hold
			{ PortType::None,		0,	ribTouchOffset,		ValueType::Num,		{0, 1}								}	// touch offset
		},
		{
			{
				{ 0x17, 0x00, 0x3F },
				{ 0x17, 0x00, 0x3D },
				{ 0x17, 0x00, 0x41 },
				{ 0x17, 0x00, 0x42 },
			},
		},
		{ 0, 3, 0, 2, },
		{
			{ 100, 0, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Chorus/Flanger
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::ChorusFlanger, 1,
		{
			{ PortType::None,		0,	cfpFrequency,		ValueType::Num,		{0, 5000, divisor100}				},	// frequency
			{ PortType::None,		0,	cfpMode,			ValueType::Num,		{0, 1}								},	// mode
			{ PortType::None,		0,	cfpDepth,			ValueType::Num,		{0, 100}							},	// depth
			{ PortType::None,		0,	cfpOffset,			ValueType::Num,		{0, 127}							},	// offset
			{ PortType::None,		0,	cfpInLevel,			ValueType::Num,		{0, 100}							},	// in level
			{ PortType::None,		0,	cfpFeedback,		ValueType::Num,		{-100, 100}							},	// feedback
			{ PortType::None,		0,	cfpDry,				ValueType::Num,		{0, 100}							},	// dry
			{ PortType::None,		0,	cfpWet,				ValueType::Num,		{-100, 100}							},	// wet
			{ PortType::None,		0,	cfpPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	cfpInput,			ValueType::Enum,	(EnumListType*)&EffectInputNames	}	// inp 0 source
		},
		{
			{
				{ 0x15, 0x00, 0x15 },
				{ 0x15, 0x00, 0x14 },
				{ 0x15, 0x00, 0x17 },
				{ 0x15, 0x00, 0x1B },
				{ 0x15, 0x00, 0x1D },
				{ 0x15, 0x00, 0x1F },
				{ 0x15, 0x00, 0x21 },
				{ 0x15, 0x00, 0x23 },
				{ 0x15, 0x00, 0x19 },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 2, 1, 2, 2, 2, 2, 2, 2, 2, 0, },
		{
			{ 20, 0, 100, 64, 100, 0, 100, 100, 180, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Phaser
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Phaser, 1,
		{
			{ PortType::None,		0,	cfpFrequency,		ValueType::Num,		{0, 5000, divisor100}				},	// frequency
			{ PortType::None,		0,	cfpMode,			ValueType::Num,		{0, 1}								},	// mode
			{ PortType::None,		0,	cfpDepth,			ValueType::Num,		{0, 100}							},	// depth
			{ PortType::None,		0,	cfpOffset,			ValueType::Num,		{0, 200000, divisor10}				},	// offset
			{ PortType::None,		0,	cfpInLevel,			ValueType::Num,		{0, 100}							},	// in level
			{ PortType::None,		0,	cfpFeedback,		ValueType::Num,		{0, 100}							},	// feedback
			{ PortType::None,		0,	cfpDry,				ValueType::Num,		{0, 100}							},	// dry
			{ PortType::None,		0,	cfpWet,				ValueType::Num,		{0, 100}							},	// wet
			{ PortType::None,		0,	cfpPhase,			ValueType::Num,		{-180, 180}							},	// phase
			{ PortType::None,		0,	cfpInput,			ValueType::Enum,	(EnumListType*)&EffectInputNames	}	// inp 0 source
		},
		{
			{
				{ 0x15, 0x00, 0x26 },
				{ 0x15, 0x00, 0x25 },
				{ 0x15, 0x00, 0x28 },
				{ 0x15, 0x00, 0x2C },
				{ 0x15, 0x00, 0x2F },
				{ 0x15, 0x00, 0x31 },
				{ 0x15, 0x00, 0x33 },
				{ 0x15, 0x00, 0x35 },
				{ 0x15, 0x00, 0x2A },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 2, 1, 2, 3, 2, 2, 2, 2, 2, 0, },
		{
			{ 50, 0, 100, 3009, 100, 0, 0, 90, 155, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Delay
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Delay, 1,
		{

			{ PortType::None,		0,	delMode,			ValueType::Enum,	(EnumListType*)&DelayModeNames			},	// mode
			{ PortType::None,		0,	delTimeSecondsL,	ValueType::Num,		{0, 200000, divisor10}					},	// Time Seconds L
			{ PortType::None,		0,	delTimeSecondsR,	ValueType::Num,		{0, 200000, divisor10}					},	// Time Seconds R
			{ PortType::None,		0,	delTimeBeatsL,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// Time Beats L
			{ PortType::None,		0,	delTimeBeatsR,		ValueType::Enum,	(EnumListType*)&delayClockDivisionNames	},	// Time Beats R
			{ PortType::None,		0,	delFeedL,			ValueType::Num,		{0, 100}								},	// Feed L 
			{ PortType::None,		0,	delFeedR,			ValueType::Num,		{0, 100}								},	// Feed R
			{ PortType::None,		0,	delDamping,			ValueType::Num,		{0, 100}								},	// Damping
			{ PortType::None,		0,	delDry,				ValueType::Num,		{0, 100}								},	// Dry
			{ PortType::None,		0,	delWet,				ValueType::Num,		{0, 100}								},	// Wet
			{ PortType::None,		0,	delMidiClk,			ValueType::Num,		{0, 1}									},	// MIDI clock
			{ PortType::None,		0,	delInput,			ValueType::Enum,	(EnumListType*)&EffectInputNames		}	// inp source
		},
		{
			{
				{ 0x15, 0x00, 0x37 },
				{ 0x15, 0x00, 0x38 },
				{ 0x15, 0x00, 0x3B },
				{ 0x15, 0x00, 0x3E },
				{ 0x15, 0x00, 0x3F },
				{ 0x15, 0x00, 0x40 },
				{ 0x15, 0x00, 0x42 },
				{ 0x15, 0x00, 0x44 },
				{ 0x15, 0x00, 0x46 },
				{ 0x15, 0x00, 0x48 },
				{ 0x15, 0x00, 0x4A },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 1, 3, 3, 1, 1, 2, 2, 2, 2, 0, 1, 0, },
		{
			{ 0, 2600, 2500, 0, 0, 0, 5, 0, 100, 16, 0, 0 }
		}
	},


	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	EQ
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::EQ, 1,
		{
			{ PortType::None,		0,	eqMode,			ValueType::Num,		{0, 1}								},		// mode
			{ PortType::None,		0,	eqFreq1,		ValueType::Num,		{0, 200000, divisor10}				},		// Frequency 1
			{ PortType::None,		0,	eqQ1,			ValueType::Num,		{47, 256, divisor10}				},		// Q 1
			{ PortType::None,		0,	eqGain1,		ValueType::Num,		{-120, 120, divisor10}				},		// Gain 1
			{ PortType::None,		0,	eqFreq2,		ValueType::Num,		{0, 200000, divisor10}				},		// Frequency 2
			{ PortType::None,		0,	eqQ2,			ValueType::Num,		{47, 256, divisor10}				},		// Q 2
			{ PortType::None,		0,	eqGain2,		ValueType::Num,		{-120, 120, divisor10}				},		// Gain 2
			{ PortType::None,		0,	eqFreq3,		ValueType::Num,		{0, 200000, divisor10}				},		// Frequency 3
			{ PortType::None,		0,	eqQ3,			ValueType::Num,		{47, 256, divisor10}				},		// Q 3
			{ PortType::None,		0,	eqGain3,		ValueType::Num,		{-120, 120, divisor10}				},		// Gain 3
			{ PortType::None,		0,	eqInput,		ValueType::Enum,	(EnumListType*)&EffectInputNames	},		// inp source
		},
		{
			{
				{ 0x15, 0x00, 0x4B },
				{ 0x15, 0x00, 0x4C },
				{ 0x15, 0x00, 0x4F },
				{ 0x15, 0x00, 0x51 },
				{ 0x15, 0x00, 0x53 },
				{ 0x15, 0x00, 0x56 },
				{ 0x15, 0x00, 0x58 },
				{ 0x15, 0x00, 0x5A },
				{ 0x15, 0x00, 0x5D },
				{ 0x15, 0x00, 0x5F },
				{ 0xFF, 0xFF, 0xFF },
			},
		},
		{ 1, 3, 2, 2, 3, 2, 2, 3, 2, 2, 0, },
		{
			{ 0, 0, 47, 0, 0, 47, 0, 0, 47, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Sequencer Common
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::SeqGlobal, 1,
		{

			{ PortType::None,		0,	seqMode,		ValueType::Enum,	(EnumListType*)&seqModeNames		},	// Mode
			{ PortType::None,		0,	seqDivision,	ValueType::Enum,	(EnumListType*)&clockDivisionNames	},	// Clock Division
			{ PortType::None,		0,	seqSwing,		ValueType::Num,		{0, 375, divisor5}					},	// swing
			{ PortType::None,		0,	seqPattern,		ValueType::Num,		{0, 63}								}	// pattern
		},
		{
			{
				{ 0x14, 0x00, 0x00 },
				{ 0x14, 0x00, 0x04 },
				{ 0x14, 0x00, 0x02 },
				{ 0x14, 0x00, 0x01 },
			},
		},
		{ 1, 1, 2, 1, },
		{
			{0, 1, 0, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Sequencer
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Seq, 4,
		{
			{ PortType::None,		0,	seqPatternLength,	ValueType::Num,	{1, 16}			},	// pattern length
			{ PortType::None,		0,	seqStep1,			ValueType::Num,	{-127, 127}		},	// Step 1
			{ PortType::None,		0,	seqStep2,			ValueType::Num,	{-127, 127}		},	// Step 2
			{ PortType::None,		0,	seqStep3,			ValueType::Num,	{-127, 127}		},	// Step 3
			{ PortType::None,		0,	seqStep4,			ValueType::Num,	{-127, 127}		},	// Step 4
			{ PortType::None,		0,	seqStep5,			ValueType::Num,	{-127, 127}		},	// Step 5
			{ PortType::None,		0,	seqStep6,			ValueType::Num,	{-127, 127}		},	// Step 6
			{ PortType::None,		0,	seqStep7,			ValueType::Num,	{-127, 127}		},	// Step 7
			{ PortType::None,		0,	seqStep8,			ValueType::Num,	{-127, 127}		},	// Step 8
			{ PortType::None,		0,	seqStep9,			ValueType::Num,	{-127, 127}		},	// Step 9
			{ PortType::None,		0,	seqStep10,			ValueType::Num,	{-127, 127}		},	// Step 10
			{ PortType::None,		0,	seqStep11,			ValueType::Num,	{-127, 127}		},	// Step 11
			{ PortType::None,		0,	seqStep12,			ValueType::Num,	{-127, 127}		},	// Step 12
			{ PortType::None,		0,	seqStep13,			ValueType::Num,	{-127, 127}		},	// Step 13
			{ PortType::None,		0,	seqStep14,			ValueType::Num,	{-127, 127}		},	// Step 14
			{ PortType::None,		0,	seqStep15,			ValueType::Num,	{-127, 127}		},	// Step 15
			{ PortType::None,		0,	seqStep16,			ValueType::Num,	{-127, 127}		}	// Step 16
		},
		{
			// -------------  Seq A -----------------------

			{
				{ 0x14, 0x00, 0x05 },
				{ 0x14, 0x10, 0x00 },
				{ 0x14, 0x10, 0x02 },
				{ 0x14, 0x10, 0x04 },
				{ 0x14, 0x10, 0x06 },
				{ 0x14, 0x10, 0x08 },
				{ 0x14, 0x10, 0x0A },
				{ 0x14, 0x10, 0x0C },
				{ 0x14, 0x10, 0x0E },
				{ 0x14, 0x10, 0x10 },
				{ 0x14, 0x10, 0x12 },
				{ 0x14, 0x10, 0x14 },
				{ 0x14, 0x10, 0x16 },
				{ 0x14, 0x10, 0x18 },
				{ 0x14, 0x10, 0x1A },
				{ 0x14, 0x10, 0x1C },
				{ 0x14, 0x10, 0x1E },
			},
			// -------------  Seq B -----------------------

			{
				{ 0x14, 0x00, 0x06 },
				{ 0x14, 0x10, 0x20 },
				{ 0x14, 0x10, 0x22 },
				{ 0x14, 0x10, 0x24 },
				{ 0x14, 0x10, 0x26 },
				{ 0x14, 0x10, 0x28 },
				{ 0x14, 0x10, 0x2A },
				{ 0x14, 0x10, 0x2C },
				{ 0x14, 0x10, 0x2E },
				{ 0x14, 0x10, 0x30 },
				{ 0x14, 0x10, 0x32 },
				{ 0x14, 0x10, 0x34 },
				{ 0x14, 0x10, 0x36 },
				{ 0x14, 0x10, 0x38 },
				{ 0x14, 0x10, 0x3A },
				{ 0x14, 0x10, 0x3C },
				{ 0x14, 0x10, 0x3E },
			},
			// -------------  Seq C -----------------------

			{
				{ 0x14, 0x00, 0x07 },
				{ 0x14, 0x10, 0x40 },
				{ 0x14, 0x10, 0x42 },
				{ 0x14, 0x10, 0x44 },
				{ 0x14, 0x10, 0x46 },
				{ 0x14, 0x10, 0x48 },
				{ 0x14, 0x10, 0x4A },
				{ 0x14, 0x10, 0x4C },
				{ 0x14, 0x10, 0x4E },
				{ 0x14, 0x10, 0x50 },
				{ 0x14, 0x10, 0x52 },
				{ 0x14, 0x10, 0x54 },
				{ 0x14, 0x10, 0x56 },
				{ 0x14, 0x10, 0x58 },
				{ 0x14, 0x10, 0x5A },
				{ 0x14, 0x10, 0x5C },
				{ 0x14, 0x10, 0x5E },
			},
			// -------------  Seq D -----------------------

			{
				{ 0x14, 0x00, 0x08 },
				{ 0x14, 0x10, 0x60 },
				{ 0x14, 0x10, 0x62 },
				{ 0x14, 0x10, 0x64 },
				{ 0x14, 0x10, 0x66 },
				{ 0x14, 0x10, 0x68 },
				{ 0x14, 0x10, 0x6A },
				{ 0x14, 0x10, 0x6C },
				{ 0x14, 0x10, 0x6E },
				{ 0x14, 0x10, 0x70 },
				{ 0x14, 0x10, 0x72 },
				{ 0x14, 0x10, 0x74 },
				{ 0x14, 0x10, 0x76 },
				{ 0x14, 0x10, 0x78 },
				{ 0x14, 0x10, 0x7A },
				{ 0x14, 0x10, 0x7C },
				{ 0x14, 0x10, 0x7E },
			},
		},
		{ 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, },
		{
			{ 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Arpeggiator
	//
	////////////////////////////////////////////////////////////////////////////////////////

	/*
		Arp Steps X 32:   volume, length, enable
		There are actually 96 parameters for arp steps, not 32, because there are separate 
		parameters for volume, gate length, and gate enable. But the 32 rows below are just used 
		for requesting steps over MIDI and so 32 are enough.
	*/
	{
		ElementType::Arpeggiator, 1,
		{
			{ PortType::None,	0,	0,		ValueType::Num,		{0, 127}	},	// Arp volume step 1
			{ PortType::None,	0,	1,		ValueType::Num,		{0, 127}	},	// Arp volume step 2
			{ PortType::None,	0,	2,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	3,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	4,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	5,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	6,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	7,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	8,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	9,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	10,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	11,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	12,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	13,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	14,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	15,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	16,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	17,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	18,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	19,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	20,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	21,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	22,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	23,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	24,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	25,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	26,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	27,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	28,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	29,		ValueType::Num,		{0, 127}	},
			{ PortType::None,	0,	30,		ValueType::Num,		{0, 127}	},	// Arp volume step 31
			{ PortType::None,	0,	31,		ValueType::Num,		{0, 127}	},	// Arp volume step 32

			{ PortType::None,	0,	32+0,	ValueType::Num,		{1, 40}		},	// Arp gate length step 1
			{ PortType::None,	0,	32+1,	ValueType::Num,		{1, 40}		},	// Arp gate length step 2
			{ PortType::None,	0,	32+2,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+3,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+4,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+5,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+6,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+7,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+8,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+9,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+10,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+11,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+12,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+13,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+14,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+15,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+16,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+17,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+18,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+19,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+20,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+21,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+22,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+23,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+24,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+25,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+26,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+27,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+28,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+29,	ValueType::Num,		{1, 40}		},
			{ PortType::None,	0,	32+30,	ValueType::Num,		{1, 40}		},	// Arp gate length step 31
			{ PortType::None,	0,	32+31,	ValueType::Num,		{1, 40}		},	// Arp gate length step 32

			{ PortType::None,	0,	64+0,	ValueType::Num,		{0, 1}		},	// Arp gate enable step 1
			{ PortType::None,	0,	64+1,	ValueType::Num,		{0, 1}		},	// Arp gate enable step 2
			{ PortType::None,	0,	64+2,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+3,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+4,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+5,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+6,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+7,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+8,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+9,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+10,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+11,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+12,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+13,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+14,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+15,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+16,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+17,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+18,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+19,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+20,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+21,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+22,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+23,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+24,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+25,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+26,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+27,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+28,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+29,	ValueType::Num,		{0, 1}		},
			{ PortType::None,	0,	64+30,	ValueType::Num,		{0, 1}		},	// Arp gate enable step 31
			{ PortType::None,	0,	64+31,	ValueType::Num,		{0, 1}		},	// Arp gate enable step 32

			{ PortType::None,	0,	arpVelocity,		ValueType::Enum,	(EnumListType*)&ArpVelocityNames	},	// Arp Velocity
			{ PortType::None,	0,	arpMode,			ValueType::Enum,	(EnumListType*)&ArpModeNames		},	// Arp Mode
			{ PortType::None,	0,	arpOctave,			ValueType::Enum,	(EnumListType*)&ArpOctaveNames		},	// Arp Octaves
			{ PortType::None,	0,	arpSwing,			ValueType::Num,		{0, 375, divisor5}					},	// Arp Swing, Range: 0.0% to 75.0%   Unit: 0.2%
			{ PortType::None,	0,	arpResolution,		ValueType::Enum,	(EnumListType*)&clockDivisionNames	},	// Arp Resolution
			{ PortType::None,	0,	arpNoteLength,		ValueType::Num,		{-63, 63}							},	// Arp Note Length
			{ PortType::None,	0,	arpHold,			ValueType::Enum,	{0, 1}								},	// Arp Hold
			{ PortType::None,	0,	arpPatternLength,	ValueType::Enum,	{0, 31}								},	// Arp Pattern Length
			{ PortType::None,	0,	arpPattern,			ValueType::Enum,	{0, 63}								}	// Arp Pattern
		},
		{
			{
				{ 0x18, 0x10, 0x00 },
				{ 0x18, 0x10, 0x02 },
				{ 0x18, 0x10, 0x04 },
				{ 0x18, 0x10, 0x06 },
				{ 0x18, 0x10, 0x08 },
				{ 0x18, 0x10, 0x0A },
				{ 0x18, 0x10, 0x0C },
				{ 0x18, 0x10, 0x0E },
				{ 0x18, 0x10, 0x10 },
				{ 0x18, 0x10, 0x12 },
				{ 0x18, 0x10, 0x14 },
				{ 0x18, 0x10, 0x16 },
				{ 0x18, 0x10, 0x18 },
				{ 0x18, 0x10, 0x1A },
				{ 0x18, 0x10, 0x1C },
				{ 0x18, 0x10, 0x1E },
				{ 0x18, 0x10, 0x20 },
				{ 0x18, 0x10, 0x22 },
				{ 0x18, 0x10, 0x24 },
				{ 0x18, 0x10, 0x26 },
				{ 0x18, 0x10, 0x28 },
				{ 0x18, 0x10, 0x2A },
				{ 0x18, 0x10, 0x2C },
				{ 0x18, 0x10, 0x2E },
				{ 0x18, 0x10, 0x30 },
				{ 0x18, 0x10, 0x32 },
				{ 0x18, 0x10, 0x34 },
				{ 0x18, 0x10, 0x36 },
				{ 0x18, 0x10, 0x38 },
				{ 0x18, 0x10, 0x3A },
				{ 0x18, 0x10, 0x3C },
				{ 0x18, 0x10, 0x3E },

				{ 0x18, 0x10, 0x40 },
				{ 0x18, 0x10, 0x41 },
				{ 0x18, 0x10, 0x42 },
				{ 0x18, 0x10, 0x43 },
				{ 0x18, 0x10, 0x44 },
				{ 0x18, 0x10, 0x45 },
				{ 0x18, 0x10, 0x46 },
				{ 0x18, 0x10, 0x47 },
				{ 0x18, 0x10, 0x48 },
				{ 0x18, 0x10, 0x49 },
				{ 0x18, 0x10, 0x4A },
				{ 0x18, 0x10, 0x4B },
				{ 0x18, 0x10, 0x4C },
				{ 0x18, 0x10, 0x4D },
				{ 0x18, 0x10, 0x4E },
				{ 0x18, 0x10, 0x4F },
				{ 0x18, 0x10, 0x50 },
				{ 0x18, 0x10, 0x51 },
				{ 0x18, 0x10, 0x52 },
				{ 0x18, 0x10, 0x53 },
				{ 0x18, 0x10, 0x54 },
				{ 0x18, 0x10, 0x55 },
				{ 0x18, 0x10, 0x56 },
				{ 0x18, 0x10, 0x57 },
				{ 0x18, 0x10, 0x58 },
				{ 0x18, 0x10, 0x59 },
				{ 0x18, 0x10, 0x5A },
				{ 0x18, 0x10, 0x5B },
				{ 0x18, 0x10, 0x5C },
				{ 0x18, 0x10, 0x5D },
				{ 0x18, 0x10, 0x5E },
				{ 0x18, 0x10, 0x5F },

				{ 0x18, 0x10, 0x60 },
				{ 0x18, 0x10, 0x61 },
				{ 0x18, 0x10, 0x62 },
				{ 0x18, 0x10, 0x63 },
				{ 0x18, 0x10, 0x64 },
				{ 0x18, 0x10, 0x65 },
				{ 0x18, 0x10, 0x66 },
				{ 0x18, 0x10, 0x67 },
				{ 0x18, 0x10, 0x68 },
				{ 0x18, 0x10, 0x69 },
				{ 0x18, 0x10, 0x6A },
				{ 0x18, 0x10, 0x6B },
				{ 0x18, 0x10, 0x6C },
				{ 0x18, 0x10, 0x6D },
				{ 0x18, 0x10, 0x6E },
				{ 0x18, 0x10, 0x6F },
				{ 0x18, 0x10, 0x70 },
				{ 0x18, 0x10, 0x71 },
				{ 0x18, 0x10, 0x72 },
				{ 0x18, 0x10, 0x73 },
				{ 0x18, 0x10, 0x74 },
				{ 0x18, 0x10, 0x75 },
				{ 0x18, 0x10, 0x76 },
				{ 0x18, 0x10, 0x77 },
				{ 0x18, 0x10, 0x78 },
				{ 0x18, 0x10, 0x79 },
				{ 0x18, 0x10, 0x7A },
				{ 0x18, 0x10, 0x7B },
				{ 0x18, 0x10, 0x7C },
				{ 0x18, 0x10, 0x7D },
				{ 0x18, 0x10, 0x7E },
				{ 0x18, 0x10, 0x7F },


				{ 0x18, 0x00, 0x06 },
				{ 0x18, 0x00, 0x00 },
				{ 0x18, 0x00, 0x05 },
				{ 0x18, 0x00, 0x03 },
				{ 0x18, 0x00, 0x01 },
				{ 0x18, 0x00, 0x07 },
				{ 0x18, 0x00, 0x09 },
				{ 0x18, 0x00, 0x08 },
				{ 0x18, 0x00, 0x02 },
			},
		},
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, },
		{
			///< defaults
			{ 127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
			  10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
			  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			  0, 0, 0, 0, 1, 65, 0, 31, 0 }
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Key Table
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::KeyTable, 4,
		{
			{ PortType::None,	0,	0,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	1,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	2,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	3,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	4,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	5,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	6,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	7,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	8,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	9,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	10,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	11,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	12,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	13,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	14,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	15,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	16,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	17,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	18,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	19,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	20,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	21,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	22,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	23,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	24,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	25,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	26,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	27,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	28,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	29,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	30,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	31,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	32,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	33,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	34,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	35,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	36,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	37,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	38,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	39,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	40,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	41,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	42,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	43,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	44,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	45,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	46,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	47,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	48,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	49,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	50,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	51,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	52,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	53,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	54,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	55,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	56,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	57,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	58,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	59,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	60,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	61,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	62,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	63,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	64,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	65,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	66,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	67,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	68,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	69,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	70,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	71,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	72,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	73,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	74,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	75,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	76,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	77,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	78,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	79,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	80,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	81,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	82,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	83,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	84,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	85,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	86,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	87,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	88,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	89,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	90,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	91,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	92,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	93,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	94,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	95,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	96,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	97,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	98,		ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	99,		ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	100,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	101,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	102,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	103,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	104,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	105,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	106,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	107,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	108,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	109,	ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	110,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	111,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	112,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	113,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	114,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	115,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	116,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	117,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	118,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	119,	ValueType::Num,{ 0, 1000 } },

			{ PortType::None,	0,	120,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	121,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	122,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	123,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	124,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	125,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	126,	ValueType::Num,{ 0, 1000 } },
			{ PortType::None,	0,	127,	ValueType::Num,{ 0, 1000 } },
		},
		{
			{
				{ 0x16, 0x00, 0x00 },
				{ 0x16, 0x00, 0x02 },
				{ 0x16, 0x00, 0x04 },
				{ 0x16, 0x00, 0x06 },
				{ 0x16, 0x00, 0x08 },
				{ 0x16, 0x00, 0x0A },
				{ 0x16, 0x00, 0x0C },
				{ 0x16, 0x00, 0x0E },
				{ 0x16, 0x00, 0x10 },
				{ 0x16, 0x00, 0x12 },
				{ 0x16, 0x00, 0x14 },
				{ 0x16, 0x00, 0x16 },
				{ 0x16, 0x00, 0x18 },
				{ 0x16, 0x00, 0x1A },
				{ 0x16, 0x00, 0x1C },
				{ 0x16, 0x00, 0x1E },
				{ 0x16, 0x00, 0x20 },
				{ 0x16, 0x00, 0x22 },
				{ 0x16, 0x00, 0x24 },
				{ 0x16, 0x00, 0x26 },
				{ 0x16, 0x00, 0x28 },
				{ 0x16, 0x00, 0x2A },
				{ 0x16, 0x00, 0x2C },
				{ 0x16, 0x00, 0x2E },
				{ 0x16, 0x00, 0x30 },
				{ 0x16, 0x00, 0x32 },
				{ 0x16, 0x00, 0x34 },
				{ 0x16, 0x00, 0x36 },
				{ 0x16, 0x00, 0x38 },
				{ 0x16, 0x00, 0x3A },
				{ 0x16, 0x00, 0x3C },
				{ 0x16, 0x00, 0x3E },
				{ 0x16, 0x00, 0x40 },
				{ 0x16, 0x00, 0x42 },
				{ 0x16, 0x00, 0x44 },
				{ 0x16, 0x00, 0x46 },
				{ 0x16, 0x00, 0x48 },
				{ 0x16, 0x00, 0x4A },
				{ 0x16, 0x00, 0x4C },
				{ 0x16, 0x00, 0x4E },
				{ 0x16, 0x00, 0x50 },
				{ 0x16, 0x00, 0x52 },
				{ 0x16, 0x00, 0x54 },
				{ 0x16, 0x00, 0x56 },
				{ 0x16, 0x00, 0x58 },
				{ 0x16, 0x00, 0x5A },
				{ 0x16, 0x00, 0x5C },
				{ 0x16, 0x00, 0x5E },
				{ 0x16, 0x00, 0x60 },
				{ 0x16, 0x00, 0x62 },
				{ 0x16, 0x00, 0x64 },
				{ 0x16, 0x00, 0x66 },
				{ 0x16, 0x00, 0x68 },
				{ 0x16, 0x00, 0x6A },
				{ 0x16, 0x00, 0x6C },
				{ 0x16, 0x00, 0x6E },
				{ 0x16, 0x00, 0x70 },
				{ 0x16, 0x00, 0x72 },
				{ 0x16, 0x00, 0x74 },
				{ 0x16, 0x00, 0x76 },
				{ 0x16, 0x00, 0x78 },
				{ 0x16, 0x00, 0x7A },
				{ 0x16, 0x00, 0x7C },
				{ 0x16, 0x00, 0x7E },
				{ 0x16, 0x00, 0x7F },

				{ 0x16, 0x10, 0x00 },
				{ 0x16, 0x10, 0x02 },
				{ 0x16, 0x10, 0x04 },
				{ 0x16, 0x10, 0x06 },
				{ 0x16, 0x10, 0x08 },
				{ 0x16, 0x10, 0x0A },
				{ 0x16, 0x10, 0x0C },
				{ 0x16, 0x10, 0x0E },
				{ 0x16, 0x10, 0x10 },
				{ 0x16, 0x10, 0x12 },
				{ 0x16, 0x10, 0x14 },
				{ 0x16, 0x10, 0x16 },
				{ 0x16, 0x10, 0x18 },
				{ 0x16, 0x10, 0x1A },
				{ 0x16, 0x10, 0x1C },
				{ 0x16, 0x10, 0x1E },
				{ 0x16, 0x10, 0x20 },
				{ 0x16, 0x10, 0x22 },
				{ 0x16, 0x10, 0x24 },
				{ 0x16, 0x10, 0x26 },
				{ 0x16, 0x10, 0x28 },
				{ 0x16, 0x10, 0x2A },
				{ 0x16, 0x10, 0x2C },
				{ 0x16, 0x10, 0x2E },
				{ 0x16, 0x10, 0x30 },
				{ 0x16, 0x10, 0x32 },
				{ 0x16, 0x10, 0x34 },
				{ 0x16, 0x10, 0x36 },
				{ 0x16, 0x10, 0x38 },
				{ 0x16, 0x10, 0x3A },
				{ 0x16, 0x10, 0x3C },
				{ 0x16, 0x10, 0x3E },
				{ 0x16, 0x10, 0x40 },
				{ 0x16, 0x10, 0x42 },
				{ 0x16, 0x10, 0x44 },
				{ 0x16, 0x10, 0x46 },
				{ 0x16, 0x10, 0x48 },
				{ 0x16, 0x10, 0x4A },
				{ 0x16, 0x10, 0x4C },
				{ 0x16, 0x10, 0x4E },
				{ 0x16, 0x10, 0x50 },
				{ 0x16, 0x10, 0x52 },
				{ 0x16, 0x10, 0x54 },
				{ 0x16, 0x10, 0x56 },
				{ 0x16, 0x10, 0x58 },
				{ 0x16, 0x10, 0x5A },
				{ 0x16, 0x10, 0x5C },
				{ 0x16, 0x10, 0x5E },
				{ 0x16, 0x10, 0x60 },
				{ 0x16, 0x10, 0x62 },
				{ 0x16, 0x10, 0x64 },
				{ 0x16, 0x10, 0x66 },
				{ 0x16, 0x10, 0x68 },
				{ 0x16, 0x10, 0x6A },
				{ 0x16, 0x10, 0x6C },
				{ 0x16, 0x10, 0x6E },
				{ 0x16, 0x10, 0x70 },
				{ 0x16, 0x10, 0x72 },
				{ 0x16, 0x10, 0x74 },
				{ 0x16, 0x10, 0x76 },
				{ 0x16, 0x10, 0x78 },
				{ 0x16, 0x10, 0x7A },
				{ 0x16, 0x10, 0x7C },
				{ 0x16, 0x10, 0x7E },
				{ 0x16, 0x10, 0x7F },
			},
			{
				{ 0x16, 0x20, 0x00 },
				{ 0x16, 0x20, 0x02 },
				{ 0x16, 0x20, 0x04 },
				{ 0x16, 0x20, 0x06 },
				{ 0x16, 0x20, 0x08 },
				{ 0x16, 0x20, 0x0A },
				{ 0x16, 0x20, 0x0C },
				{ 0x16, 0x20, 0x0E },
				{ 0x16, 0x20, 0x10 },
				{ 0x16, 0x20, 0x12 },
				{ 0x16, 0x20, 0x14 },
				{ 0x16, 0x20, 0x16 },
				{ 0x16, 0x20, 0x18 },
				{ 0x16, 0x20, 0x1A },
				{ 0x16, 0x20, 0x1C },
				{ 0x16, 0x20, 0x1E },
				{ 0x16, 0x20, 0x20 },
				{ 0x16, 0x20, 0x22 },
				{ 0x16, 0x20, 0x24 },
				{ 0x16, 0x20, 0x26 },
				{ 0x16, 0x20, 0x28 },
				{ 0x16, 0x20, 0x2A },
				{ 0x16, 0x20, 0x2C },
				{ 0x16, 0x20, 0x2E },
				{ 0x16, 0x20, 0x30 },
				{ 0x16, 0x20, 0x32 },
				{ 0x16, 0x20, 0x34 },
				{ 0x16, 0x20, 0x36 },
				{ 0x16, 0x20, 0x38 },
				{ 0x16, 0x20, 0x3A },
				{ 0x16, 0x20, 0x3C },
				{ 0x16, 0x20, 0x3E },
				{ 0x16, 0x20, 0x40 },
				{ 0x16, 0x20, 0x42 },
				{ 0x16, 0x20, 0x44 },
				{ 0x16, 0x20, 0x46 },
				{ 0x16, 0x20, 0x48 },
				{ 0x16, 0x20, 0x4A },
				{ 0x16, 0x20, 0x4C },
				{ 0x16, 0x20, 0x4E },
				{ 0x16, 0x20, 0x50 },
				{ 0x16, 0x20, 0x52 },
				{ 0x16, 0x20, 0x54 },
				{ 0x16, 0x20, 0x56 },
				{ 0x16, 0x20, 0x58 },
				{ 0x16, 0x20, 0x5A },
				{ 0x16, 0x20, 0x5C },
				{ 0x16, 0x20, 0x5E },
				{ 0x16, 0x20, 0x60 },
				{ 0x16, 0x20, 0x62 },
				{ 0x16, 0x20, 0x64 },
				{ 0x16, 0x20, 0x66 },
				{ 0x16, 0x20, 0x68 },
				{ 0x16, 0x20, 0x6A },
				{ 0x16, 0x20, 0x6C },
				{ 0x16, 0x20, 0x6E },
				{ 0x16, 0x20, 0x70 },
				{ 0x16, 0x20, 0x72 },
				{ 0x16, 0x20, 0x74 },
				{ 0x16, 0x20, 0x76 },
				{ 0x16, 0x20, 0x78 },
				{ 0x16, 0x20, 0x7A },
				{ 0x16, 0x20, 0x7C },
				{ 0x16, 0x20, 0x7E },
				{ 0x16, 0x20, 0x7F },

				{ 0x16, 0x30, 0x00 },
				{ 0x16, 0x30, 0x02 },
				{ 0x16, 0x30, 0x04 },
				{ 0x16, 0x30, 0x06 },
				{ 0x16, 0x30, 0x08 },
				{ 0x16, 0x30, 0x0A },
				{ 0x16, 0x30, 0x0C },
				{ 0x16, 0x30, 0x0E },
				{ 0x16, 0x30, 0x10 },
				{ 0x16, 0x30, 0x12 },
				{ 0x16, 0x30, 0x14 },
				{ 0x16, 0x30, 0x16 },
				{ 0x16, 0x30, 0x18 },
				{ 0x16, 0x30, 0x1A },
				{ 0x16, 0x30, 0x1C },
				{ 0x16, 0x30, 0x1E },
				{ 0x16, 0x30, 0x20 },
				{ 0x16, 0x30, 0x22 },
				{ 0x16, 0x30, 0x24 },
				{ 0x16, 0x30, 0x26 },
				{ 0x16, 0x30, 0x28 },
				{ 0x16, 0x30, 0x2A },
				{ 0x16, 0x30, 0x2C },
				{ 0x16, 0x30, 0x2E },
				{ 0x16, 0x30, 0x30 },
				{ 0x16, 0x30, 0x32 },
				{ 0x16, 0x30, 0x34 },
				{ 0x16, 0x30, 0x36 },
				{ 0x16, 0x30, 0x38 },
				{ 0x16, 0x30, 0x3A },
				{ 0x16, 0x30, 0x3C },
				{ 0x16, 0x30, 0x3E },
				{ 0x16, 0x30, 0x40 },
				{ 0x16, 0x30, 0x42 },
				{ 0x16, 0x30, 0x44 },
				{ 0x16, 0x30, 0x46 },
				{ 0x16, 0x30, 0x48 },
				{ 0x16, 0x30, 0x4A },
				{ 0x16, 0x30, 0x4C },
				{ 0x16, 0x30, 0x4E },
				{ 0x16, 0x30, 0x50 },
				{ 0x16, 0x30, 0x52 },
				{ 0x16, 0x30, 0x54 },
				{ 0x16, 0x30, 0x56 },
				{ 0x16, 0x30, 0x58 },
				{ 0x16, 0x30, 0x5A },
				{ 0x16, 0x30, 0x5C },
				{ 0x16, 0x30, 0x5E },
				{ 0x16, 0x30, 0x60 },
				{ 0x16, 0x30, 0x62 },
				{ 0x16, 0x30, 0x64 },
				{ 0x16, 0x30, 0x66 },
				{ 0x16, 0x30, 0x68 },
				{ 0x16, 0x30, 0x6A },
				{ 0x16, 0x30, 0x6C },
				{ 0x16, 0x30, 0x6E },
				{ 0x16, 0x30, 0x70 },
				{ 0x16, 0x30, 0x72 },
				{ 0x16, 0x30, 0x74 },
				{ 0x16, 0x30, 0x76 },
				{ 0x16, 0x30, 0x78 },
				{ 0x16, 0x30, 0x7A },
				{ 0x16, 0x30, 0x7C },
				{ 0x16, 0x30, 0x7F },
			},
			{
				{ 0x16, 0x40, 0x00 },
				{ 0x16, 0x40, 0x02 },
				{ 0x16, 0x40, 0x04 },
				{ 0x16, 0x40, 0x06 },
				{ 0x16, 0x40, 0x08 },
				{ 0x16, 0x40, 0x0A },
				{ 0x16, 0x40, 0x0C },
				{ 0x16, 0x40, 0x0E },
				{ 0x16, 0x40, 0x10 },
				{ 0x16, 0x40, 0x12 },
				{ 0x16, 0x40, 0x14 },
				{ 0x16, 0x40, 0x16 },
				{ 0x16, 0x40, 0x18 },
				{ 0x16, 0x40, 0x1A },
				{ 0x16, 0x40, 0x1C },
				{ 0x16, 0x40, 0x1E },
				{ 0x16, 0x40, 0x20 },
				{ 0x16, 0x40, 0x22 },
				{ 0x16, 0x40, 0x24 },
				{ 0x16, 0x40, 0x26 },
				{ 0x16, 0x40, 0x28 },
				{ 0x16, 0x40, 0x2A },
				{ 0x16, 0x40, 0x2C },
				{ 0x16, 0x40, 0x2E },
				{ 0x16, 0x40, 0x30 },
				{ 0x16, 0x40, 0x32 },
				{ 0x16, 0x40, 0x34 },
				{ 0x16, 0x40, 0x36 },
				{ 0x16, 0x40, 0x38 },
				{ 0x16, 0x40, 0x3A },
				{ 0x16, 0x40, 0x3C },
				{ 0x16, 0x40, 0x3E },
				{ 0x16, 0x40, 0x40 },
				{ 0x16, 0x40, 0x42 },
				{ 0x16, 0x40, 0x44 },
				{ 0x16, 0x40, 0x46 },
				{ 0x16, 0x40, 0x48 },
				{ 0x16, 0x40, 0x4A },
				{ 0x16, 0x40, 0x4C },
				{ 0x16, 0x40, 0x4E },
				{ 0x16, 0x40, 0x50 },
				{ 0x16, 0x40, 0x52 },
				{ 0x16, 0x40, 0x54 },
				{ 0x16, 0x40, 0x56 },
				{ 0x16, 0x40, 0x58 },
				{ 0x16, 0x40, 0x5A },
				{ 0x16, 0x40, 0x5C },
				{ 0x16, 0x40, 0x5E },
				{ 0x16, 0x40, 0x60 },
				{ 0x16, 0x40, 0x62 },
				{ 0x16, 0x40, 0x64 },
				{ 0x16, 0x40, 0x66 },
				{ 0x16, 0x40, 0x68 },
				{ 0x16, 0x40, 0x6A },
				{ 0x16, 0x40, 0x6C },
				{ 0x16, 0x40, 0x6E },
				{ 0x16, 0x40, 0x70 },
				{ 0x16, 0x40, 0x72 },
				{ 0x16, 0x40, 0x74 },
				{ 0x16, 0x40, 0x76 },
				{ 0x16, 0x40, 0x78 },
				{ 0x16, 0x40, 0x7A },
				{ 0x16, 0x40, 0x7C },
				{ 0x16, 0x40, 0x7E },
				{ 0x16, 0x40, 0x7F },

				{ 0x16, 0x50, 0x00 },
				{ 0x16, 0x50, 0x02 },
				{ 0x16, 0x50, 0x04 },
				{ 0x16, 0x50, 0x06 },
				{ 0x16, 0x50, 0x08 },
				{ 0x16, 0x50, 0x0A },
				{ 0x16, 0x50, 0x0C },
				{ 0x16, 0x50, 0x0E },
				{ 0x16, 0x50, 0x10 },
				{ 0x16, 0x50, 0x12 },
				{ 0x16, 0x50, 0x14 },
				{ 0x16, 0x50, 0x16 },
				{ 0x16, 0x50, 0x18 },
				{ 0x16, 0x50, 0x1A },
				{ 0x16, 0x50, 0x1C },
				{ 0x16, 0x50, 0x1E },
				{ 0x16, 0x50, 0x20 },
				{ 0x16, 0x50, 0x22 },
				{ 0x16, 0x50, 0x24 },
				{ 0x16, 0x50, 0x26 },
				{ 0x16, 0x50, 0x28 },
				{ 0x16, 0x50, 0x2A },
				{ 0x16, 0x50, 0x2C },
				{ 0x16, 0x50, 0x2E },
				{ 0x16, 0x50, 0x30 },
				{ 0x16, 0x50, 0x32 },
				{ 0x16, 0x50, 0x34 },
				{ 0x16, 0x50, 0x36 },
				{ 0x16, 0x50, 0x38 },
				{ 0x16, 0x50, 0x3A },
				{ 0x16, 0x50, 0x3C },
				{ 0x16, 0x50, 0x3E },
				{ 0x16, 0x50, 0x40 },
				{ 0x16, 0x50, 0x42 },
				{ 0x16, 0x50, 0x44 },
				{ 0x16, 0x50, 0x46 },
				{ 0x16, 0x50, 0x48 },
				{ 0x16, 0x50, 0x4A },
				{ 0x16, 0x50, 0x4C },
				{ 0x16, 0x50, 0x4E },
				{ 0x16, 0x50, 0x50 },
				{ 0x16, 0x50, 0x52 },
				{ 0x16, 0x50, 0x54 },
				{ 0x16, 0x50, 0x56 },
				{ 0x16, 0x50, 0x58 },
				{ 0x16, 0x50, 0x5A },
				{ 0x16, 0x50, 0x5C },
				{ 0x16, 0x50, 0x5E },
				{ 0x16, 0x50, 0x60 },
				{ 0x16, 0x50, 0x62 },
				{ 0x16, 0x50, 0x64 },
				{ 0x16, 0x50, 0x66 },
				{ 0x16, 0x50, 0x68 },
				{ 0x16, 0x50, 0x6A },
				{ 0x16, 0x50, 0x6C },
				{ 0x16, 0x50, 0x6E },
				{ 0x16, 0x50, 0x70 },
				{ 0x16, 0x50, 0x72 },
				{ 0x16, 0x50, 0x74 },
				{ 0x16, 0x50, 0x76 },
				{ 0x16, 0x50, 0x78 },
				{ 0x16, 0x50, 0x7A },
				{ 0x16, 0x50, 0x7C },
				{ 0x16, 0x50, 0x7F },
			},
			{
				{ 0x16, 0x60, 0x00 },
				{ 0x16, 0x60, 0x02 },
				{ 0x16, 0x60, 0x04 },
				{ 0x16, 0x60, 0x06 },
				{ 0x16, 0x60, 0x08 },
				{ 0x16, 0x60, 0x0A },
				{ 0x16, 0x60, 0x0C },
				{ 0x16, 0x60, 0x0E },
				{ 0x16, 0x60, 0x10 },
				{ 0x16, 0x60, 0x12 },
				{ 0x16, 0x60, 0x14 },
				{ 0x16, 0x60, 0x16 },
				{ 0x16, 0x60, 0x18 },
				{ 0x16, 0x60, 0x1A },
				{ 0x16, 0x60, 0x1C },
				{ 0x16, 0x60, 0x1E },
				{ 0x16, 0x60, 0x20 },
				{ 0x16, 0x60, 0x22 },
				{ 0x16, 0x60, 0x24 },
				{ 0x16, 0x60, 0x26 },
				{ 0x16, 0x60, 0x28 },
				{ 0x16, 0x60, 0x2A },
				{ 0x16, 0x60, 0x2C },
				{ 0x16, 0x60, 0x2E },
				{ 0x16, 0x60, 0x30 },
				{ 0x16, 0x60, 0x32 },
				{ 0x16, 0x60, 0x34 },
				{ 0x16, 0x60, 0x36 },
				{ 0x16, 0x60, 0x38 },
				{ 0x16, 0x60, 0x3A },
				{ 0x16, 0x60, 0x3C },
				{ 0x16, 0x60, 0x3E },
				{ 0x16, 0x60, 0x40 },
				{ 0x16, 0x60, 0x42 },
				{ 0x16, 0x60, 0x44 },
				{ 0x16, 0x60, 0x46 },
				{ 0x16, 0x60, 0x48 },
				{ 0x16, 0x60, 0x4A },
				{ 0x16, 0x60, 0x4C },
				{ 0x16, 0x60, 0x4E },
				{ 0x16, 0x60, 0x50 },
				{ 0x16, 0x60, 0x52 },
				{ 0x16, 0x60, 0x54 },
				{ 0x16, 0x60, 0x56 },
				{ 0x16, 0x60, 0x58 },
				{ 0x16, 0x60, 0x5A },
				{ 0x16, 0x60, 0x5C },
				{ 0x16, 0x60, 0x5E },
				{ 0x16, 0x60, 0x60 },
				{ 0x16, 0x60, 0x62 },
				{ 0x16, 0x60, 0x64 },
				{ 0x16, 0x60, 0x66 },
				{ 0x16, 0x60, 0x68 },
				{ 0x16, 0x60, 0x6A },
				{ 0x16, 0x60, 0x6C },
				{ 0x16, 0x60, 0x6E },
				{ 0x16, 0x60, 0x70 },
				{ 0x16, 0x60, 0x72 },
				{ 0x16, 0x60, 0x74 },
				{ 0x16, 0x60, 0x76 },
				{ 0x16, 0x60, 0x78 },
				{ 0x16, 0x60, 0x7A },
				{ 0x16, 0x60, 0x7C },
				{ 0x16, 0x60, 0x7F },

				{ 0x16, 0x70, 0x00 },
				{ 0x16, 0x70, 0x02 },
				{ 0x16, 0x70, 0x04 },
				{ 0x16, 0x70, 0x06 },
				{ 0x16, 0x70, 0x08 },
				{ 0x16, 0x70, 0x0A },
				{ 0x16, 0x70, 0x0C },
				{ 0x16, 0x70, 0x0E },
				{ 0x16, 0x70, 0x10 },
				{ 0x16, 0x70, 0x12 },
				{ 0x16, 0x70, 0x14 },
				{ 0x16, 0x70, 0x16 },
				{ 0x16, 0x70, 0x18 },
				{ 0x16, 0x70, 0x1A },
				{ 0x16, 0x70, 0x1C },
				{ 0x16, 0x70, 0x1E },
				{ 0x16, 0x70, 0x20 },
				{ 0x16, 0x70, 0x22 },
				{ 0x16, 0x70, 0x24 },
				{ 0x16, 0x70, 0x26 },
				{ 0x16, 0x70, 0x28 },
				{ 0x16, 0x70, 0x2A },
				{ 0x16, 0x70, 0x2C },
				{ 0x16, 0x70, 0x2E },
				{ 0x16, 0x70, 0x30 },
				{ 0x16, 0x70, 0x32 },
				{ 0x16, 0x70, 0x34 },
				{ 0x16, 0x70, 0x36 },
				{ 0x16, 0x70, 0x38 },
				{ 0x16, 0x70, 0x3A },
				{ 0x16, 0x70, 0x3C },
				{ 0x16, 0x70, 0x3E },
				{ 0x16, 0x70, 0x40 },
				{ 0x16, 0x70, 0x42 },
				{ 0x16, 0x70, 0x44 },
				{ 0x16, 0x70, 0x46 },
				{ 0x16, 0x70, 0x48 },
				{ 0x16, 0x70, 0x4A },
				{ 0x16, 0x70, 0x4C },
				{ 0x16, 0x70, 0x4E },
				{ 0x16, 0x70, 0x50 },
				{ 0x16, 0x70, 0x52 },
				{ 0x16, 0x70, 0x54 },
				{ 0x16, 0x70, 0x56 },
				{ 0x16, 0x70, 0x58 },
				{ 0x16, 0x70, 0x5A },
				{ 0x16, 0x70, 0x5C },
				{ 0x16, 0x70, 0x5E },
				{ 0x16, 0x70, 0x60 },
				{ 0x16, 0x70, 0x62 },
				{ 0x16, 0x70, 0x64 },
				{ 0x16, 0x70, 0x66 },
				{ 0x16, 0x70, 0x68 },
				{ 0x16, 0x70, 0x6A },
				{ 0x16, 0x70, 0x6C },
				{ 0x16, 0x70, 0x6E },
				{ 0x16, 0x70, 0x70 },
				{ 0x16, 0x70, 0x72 },
				{ 0x16, 0x70, 0x74 },
				{ 0x16, 0x70, 0x76 },
				{ 0x16, 0x70, 0x78 },
				{ 0x16, 0x70, 0x7A },
				{ 0x16, 0x70, 0x7C },
				{ 0x16, 0x70, 0x7F },
			},
		},
		{
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2, 
		},
		{
			{
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,
				0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000,0x2000
			}
		}
	},


	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Home
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::Home, 1,
		{
			{ PortType::None,	0,	gloName1,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName2,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName3,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName4,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName5,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName6,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName7,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName8,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName9,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName10,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName11,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName12,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName13,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName14,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName15,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName16,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName17,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName18,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName19,					ValueType::Num,		{ 32, 127 }						},
			{ PortType::None,	0,	gloName20,					ValueType::Num,		{ 32, 127 }						},

			{ PortType::None,	0,	gloPitchWheelUpRange,		ValueType::Num,		{-63, 63}						},	// PW Up
			{ PortType::None,	0,	gloPitchWheelDownRange,		ValueType::Num,		{-63, 63}						},	// PW Down
			{ PortType::None,	0,	gloBPM,						ValueType::Num,		{10, 300}						},	// BPM
			{ PortType::None,	0,	gloGldMode,					ValueType::Enum,	(EnumListType*)&GlideModeNames	},	// GldMode
			{ PortType::None,	0,	gloGldType,					ValueType::Enum,	(EnumListType*)&GlideTypeNames	},	// GldType
			{ PortType::None,	0,	gloGldTime,					ValueType::Num,		{0, 100000}						},	// GldTime , when Glide mode is C-Time and Exp
			{ PortType::None,	0,	gloGldRate,					ValueType::Num,		{0, 100}						},	// GldRate , when Glide mode is C-Rate
			{ PortType::None,	0,	gloGldRange,				ValueType::Num,		{0, 100}						},	// GldRange
			{ PortType::None,	0,	gloPlayMode,				ValueType::Enum,	(EnumListType*)&PlayModeNames	},	// PlayMode
			{ PortType::None,	0,	gloLegato,					ValueType::Enum,	(EnumListType*)&LegatoNames		},	// Legato
			{ PortType::None,	0,	gloUniVoice,				ValueType::Enum,	(EnumListType*)&UniVoiceNames	},	// UniVoice
			{ PortType::None,	0,	gloNotePriority,			ValueType::Enum,	(EnumListType*)&NotePriNames	},	// Note Pri
			{ PortType::None,	0,	gloEgReset,					ValueType::Enum,	(EnumListType*)&EgResetNames	},	// EgReset
			{ PortType::None,	0,	gloUniTune,					ValueType::Num,		{-100, 100}						},	// Unitune
			{ PortType::None,	0,	gloRndTune,					ValueType::Num,		{-1, 100}						},	// RndTune Home (-1 means System RndTune takes precedence)

			{ PortType::None,	0,	gloUnisonButton,			ValueType::Num,		{0, 1}							},	// Unison Button
			{ PortType::None,	0,	gloArpButton,				ValueType::Num,		{0, 1}							},	// ArpOn Button
			{ PortType::None,	0,	gloSeqButton,				ValueType::Num,		{0, 1}							},	// SeqOn Button
			{ PortType::None,	0,	gloAssignState1,			ValueType::Num,		{0, 1}							},	// Assign1 Button. Assign button state is not saved to preset file, but can be retrieved by SysEx.
			{ PortType::None,	0,	gloAssignState2, 			ValueType::Num,		{0, 1}							},	// Assign2 Button. Assign button state is not saved to preset file, but can be retrieved by SysEx.
			{ PortType::None,	0,	gloExpPedal,				ValueType::Enum,	(EnumListType*)&ExpPedalNames	},	// 
			{ PortType::None,	0,	gloSusPedal1,				ValueType::Enum,	(EnumListType*)&SusPedalNames	},	// 
			{ PortType::None,	0,	gloSusPedal2,				ValueType::Enum,	(EnumListType*)&SusPedalNames	},	// 
			{ PortType::None,	0,	gloVTIntens, 				ValueType::Num,		{ 0, 100 }						},
			{ PortType::None,	0,	gloVTOffset,				ValueType::Num,		{ 0, 100 }						},
			{ PortType::None,	0,	gloATIntens,				ValueType::Num,		{ 0, 100 }						},
			{ PortType::None,	0,	gloATOffset,				ValueType::Num,		{ 0, 100 }						},
			{ PortType::None,	0,	gloAssignFunc1,				ValueType::Enum,	(EnumListType*)&AssignNames		},
			{ PortType::None,	0,	gloAssignFunc2,				ValueType::Enum,	(EnumListType*)&AssignNames		},
			{ PortType::None,	0,	gloAssignMode1,				ValueType::Enum,	(EnumListType*)&AssignModeNames	},
			{ PortType::None,	0,	gloAssignMode2,				ValueType::Enum,	(EnumListType*)&AssignModeNames	},
			{ PortType::None,	0,	gloSamplePool,				ValueType::Num,		{ 0, 16383 }					},
			{ PortType::None,	0,	gloPatchChain,				ValueType::Num,		{ 0, 1 }						},

			{ PortType::None,	0,	gloCategory1,				ValueType::Enum,	(EnumListType*)&Category1Names	},
			{ PortType::None,	0,	gloCategory2,				ValueType::Enum,	(EnumListType*)&Category2Names	},
			{ PortType::None,	0,	gloPerfKnob1Amt,			ValueType::Num,		{-100, 100}						},
			{ PortType::None,	0,	gloPerfKnob2Amt,			ValueType::Num,		{-100, 100}						},
			{ PortType::None,	0,	gloPerfKnob3Amt,			ValueType::Num,		{-100, 100}						},
			{ PortType::None,	0,	gloPerfKnob4Amt,			ValueType::Num,		{-100, 100}						},
			{ PortType::None,	0,	gloPerfKnob5Amt,			ValueType::Num,		{-100, 100}						},
			{ PortType::None,	0,	gloPerfKnob1Src,			ValueType::Num,		{0, 0x4E8D}						},
			{ PortType::None,	0,	gloPerfKnob2Src,			ValueType::Num,		{0, 0x4E8D}						},
			{ PortType::None,	0,	gloPerfKnob3Src,			ValueType::Num,		{0, 0x4E8D}						},
			{ PortType::None,	0,	gloPerfKnob4Src,			ValueType::Num,		{0, 0x4E8D}						},
			{ PortType::None,	0,	gloPerfKnob5Src,			ValueType::Num,		{0, 0x4E8D}						},

			{ PortType::None,	0,	gloUnisonStackNumberOfNotes,	ValueType::Num,	{ 0, 10 }						},
			{ PortType::None,	0,	gloUnisonStackNote0,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote1,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote2,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote3,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote4,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote5,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote6,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote7,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote8,			ValueType::Num, { 0, 127 }						},
			{ PortType::None,	0,	gloUnisonStackNote9,			ValueType::Num,	{ 0, 127 }						},

		},
		{
			{
				{ 0x17, 0x00, 0x00 },
				{ 0x17, 0x00, 0x02 },
				{ 0x17, 0x00, 0x04 },
				{ 0x17, 0x00, 0x06 },
				{ 0x17, 0x00, 0x08 },
				{ 0x17, 0x00, 0x0A },
				{ 0x17, 0x00, 0x0C },
				{ 0x17, 0x00, 0x0E },
				{ 0x17, 0x00, 0x10 },
				{ 0x17, 0x00, 0x12 },
				{ 0x17, 0x00, 0x14 },
				{ 0x17, 0x00, 0x16 },
				{ 0x17, 0x00, 0x18 },
				{ 0x17, 0x00, 0x1A },
				{ 0x17, 0x00, 0x1C },
				{ 0x17, 0x00, 0x1E },
				{ 0x17, 0x00, 0x20 },
				{ 0x17, 0x00, 0x22 },
				{ 0x17, 0x00, 0x24 },
				{ 0x17, 0x00, 0x26 },
				{ 0x17, 0x10, 0x19 },
				{ 0x17, 0x10, 0x1A },
				{ 0x17, 0x10, 0x42 },
				{ 0x17, 0x10, 0x25 },
				{ 0x17, 0x10, 0x24 },
				{ 0x17, 0x10, 0x26 },
				{ 0x17, 0x10, 0x29 },
				{ 0x17, 0x10, 0x2B },
				{ 0x17, 0x10, 0x2D },
				{ 0x17, 0x10, 0x2E },
				{ 0x17, 0x10, 0x31 },
				{ 0x17, 0x10, 0x2F },
				{ 0x17, 0x10, 0x30 },
				{ 0x17, 0x10, 0x32 },
				{ 0x17, 0x10, 0x35 },
				{ 0x17, 0x10, 0x34 },
				{ 0x17, 0x10, 0x37 },
				{ 0x17, 0x10, 0x38 },
				{ 0x17, 0x10, 0x40 },
				{ 0x17, 0x10, 0x41 },
				{ 0x17, 0x10, 0x39 },
				{ 0x17, 0x10, 0x3A },
				{ 0x17, 0x10, 0x3B },
				{ 0x17, 0x10, 0x44 },
				{ 0x17, 0x10, 0x46 },
				{ 0x17, 0x10, 0x48 },
				{ 0x17, 0x10, 0x4A },
				{ 0x17, 0x10, 0x3C },
				{ 0x17, 0x10, 0x3D },
				{ 0x17, 0x10, 0x3E },
				{ 0x17, 0x10, 0x3F },
				{ 0x17, 0x10, 0x51 },
				{ 0x17, 0x10, 0x53 },
				{ 0x17, 0x00, 0x28 },
				{ 0x17, 0x00, 0x29 },
				{ 0x17, 0x10, 0x0F },
				{ 0x17, 0x10, 0x11 },
				{ 0x17, 0x10, 0x13 },
				{ 0x17, 0x10, 0x15 },
				{ 0x17, 0x10, 0x17 },
				{ 0x17, 0x10, 0x00 },
				{ 0x17, 0x10, 0x03 },
				{ 0x17, 0x10, 0x06 },
				{ 0x17, 0x10, 0x09 },
				{ 0x17, 0x10, 0x0C },
				{ 0x17, 0x10, 0x54 },
				{ 0x17, 0x10, 0x55 },
				{ 0x17, 0x10, 0x57 },
				{ 0x17, 0x10, 0x59 },
				{ 0x17, 0x10, 0x5B },
				{ 0x17, 0x10, 0x5D },
				{ 0x17, 0x10, 0x5F },
				{ 0x17, 0x10, 0x61 },
				{ 0x17, 0x10, 0x63 },
				{ 0x17, 0x10, 0x65 },
				{ 0x17, 0x10, 0x67 },
			}, 
		},
			{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 1, 1, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{
			{ 'I', 'N', 'I', 'T', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 7, -5, 120, 2, 0, 1000, 0, 100, 
			0, 0, 1, 0, 0, 15, 0,	// playmode, etc
			0, 0, 0, 0, 0, 0, 2, 0, 40, 0, 10, 0, 5, 5, 0, 0, 0, 0,	// Unison button, etc.
			0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0,	// category, perf
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }	// chord stack
		}
	},

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//	System
	//
	////////////////////////////////////////////////////////////////////////////////////////
	{
		ElementType::System, 1,
		{
			{ PortType::None,	0,	sysTranspose,				ValueType::Num,		{ -63, 63 }								},
			{ PortType::None,	0,	sysFineTune,				ValueType::Num,		{ -100, 100 }							},
			{ PortType::None,	0,	sysBPMOverride,				ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysOutputsOverride,			ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysExpPedalPolarity,		ValueType::Enum,	(EnumListType*)&PedalPolarityNames		},
			{ PortType::None,	0,	sysSusPedal1Polarity,		ValueType::Enum,	(EnumListType*)&PedalPolarityNames		},
			{ PortType::None,	0,	sysSusPedal2Polarity,		ValueType::Enum,	(EnumListType*)&PedalPolarityNames		},
			{ PortType::None,	0,	sysExpPedalTargetOverride,	ValueType::Enum,	(EnumListType*)&ExpPedalOverrideNames	},
			{ PortType::None,	0,	sysSusPedalTargetOverride,	ValueType::Enum,	(EnumListType*)&SusPedalOverrideNames	},
			{ PortType::None,	0,	sysRndTune,					ValueType::Num,		{ 0, 100 }								},
			{ PortType::None,	0,	sysVTIntens, 				ValueType::Num,		{ -1, 100 }								},
			{ PortType::None,	0,	sysVTOffset,				ValueType::Num,		{ -1, 100 }								},
			{ PortType::None,	0,	sysATIntens,				ValueType::Num,		{ -1, 100 }								},
			{ PortType::None,	0,	sysATOffset,				ValueType::Num,		{ -1, 100 }								},
			{ PortType::None,	0,	sysLoadSample,				ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysMidiChannel,				ValueType::Num,		{ 0, 15 }								},
			{ PortType::None,	0,	sysProgramChangeAllowed,	ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysSendArp,					ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysOmni,					ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysLocal,					ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysTransmitSysEx,			ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysMidiClockSource,			ValueType::Enum,	(EnumListType*)&MidiClockSourceNames	},
			{ PortType::None,	0,	sysMidiVolumeAllowed,		ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysMidiRealTime,			ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysPolychain,				ValueType::Num,		{ 0, 1 }								},
			{ PortType::None,	0,	sysDeviceID,				ValueType::Num,		{ 0, 16 }								},
			{ PortType::None,	0,	sysMidiCC1,					ValueType::Num,		{ 0, 127 }								},
			{ PortType::None,	0,	sysMidiCC2,					ValueType::Num,		{ 0, 127 }								},
			{ PortType::None,	0,	sysMidiCC3,					ValueType::Num,		{ 0, 127 }								},
			{ PortType::None,	0,	sysMidiCC4,					ValueType::Num,		{ 0, 127 }								},
			{ PortType::None,	0,	sysMidiCC5,					ValueType::Num,		{ 0, 127 }								},
			{ PortType::None,	0,	sysRemapAT,					ValueType::Enum,	(EnumListType*)&RemapATNames			},

		},
		{
			{
				{ 0x01, 0x00, 0x00 },
				{ 0x01, 0x00, 0x01 },
				{ 0x01, 0x00, 0x03 },
				{ 0x01, 0x00, 0x04 },
				{ 0x01, 0x00, 0x05 },
				{ 0x01, 0x00, 0x06 },
				{ 0x01, 0x00, 0x07 },
				{ 0x01, 0x00, 0x08 },
				{ 0x01, 0x00, 0x09 },
				{ 0x01, 0x00, 0x0A },
				{ 0x01, 0x00, 0x0C },
				{ 0x01, 0x00, 0x0E },
				{ 0x01, 0x00, 0x10 },
				{ 0x01, 0x00, 0x12 },
				{ 0x01, 0x00, 0x14 },
				{ 0x01, 0x00, 0x15 },
				{ 0x01, 0x00, 0x16 },
				{ 0x01, 0x00, 0x17 },
				{ 0x01, 0x00, 0x18 },
				{ 0x01, 0x00, 0x19 },
				{ 0x01, 0x00, 0x1A },
				{ 0x01, 0x00, 0x1B },
				{ 0x01, 0x00, 0x1C },
				{ 0x01, 0x00, 0x1D },
				{ 0x01, 0x00, 0x1E },
				{ 0x01, 0x00, 0x1F },
				{ 0x01, 0x00, 0x20 },
				{ 0x01, 0x00, 0x22 },
				{ 0x01, 0x00, 0x24 },
				{ 0x01, 0x00, 0x26 },
				{ 0x01, 0x00, 0x28 },
				{ 0x01, 0x00, 0x2A },
			}
		},
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1},
		{
			{ 0, 0, 1, 1, 2, 2, 2, 3, 8, 0, 40, 0, 10, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		}
	},


	// This is defined simply because ElementType::Off is a valid element type in the enumeration and so it allows functions that search to succeed using any valid element type.
	{	ElementType::Off, 0, { }, { }, { } },
		
	// Elements with no parameters
	{ ElementType::Aftertouch, 1, { }, { }, { } },
	{ ElementType::AssignButton, 1, { }, { }, { } },
	{ ElementType::AT_ModWheel, 1, { },{ },{ } },
	{ ElementType::Breath, 1, { },{ },{ } },
	{ ElementType::CC, 1, { },{ },{ } },
	{ ElementType::ExtInput, 1, { },{ },{ } },
	{ ElementType::Gate, 1, { },{ },{ } },
	{ ElementType::JoyX, 1, { },{ },{ } },
	{ ElementType::JoyY, 1, { },{ },{ } },
	{ ElementType::KeyTracking, 1, { },{ },{ } },
	{ ElementType::MaxValue, 1, { },{ },{ } },
	{ ElementType::ModWheel, 1, { },{ },{ } },
	{ ElementType::Note, 1, { },{ },{ } },
	{ ElementType::Pedal, 1, { },{ },{ } },
	{ ElementType::PinkNoise, 1, { },{ },{ } },
	{ ElementType::PolyAT, 1, { },{ },{ } },
	{ ElementType::SPDIF_In, 1, { },{ },{ } },
	{ ElementType::Velocity, 1, { },{ },{ } },
	{ ElementType::WhiteNoise, 1, { },{ },{ } }
};

/*
Exceptions/Dependencies in the above list.

Oscillator:
1. The precision of the mod amount can change depending on the mod destination. LinFM and Shape have different representations.

Filter:
1. The mod destination changes depending on the filter type selected.  fltModDestList/combModDestList/vocalModDestList
   Thus the allowble range of values can change.

InsertFX:
1. The source list depends on the Element index. InsertFX can only source the same numbered mixer or filter.

VCA:
1. The source list depends on the Element index. VCA can only source the same numbered InsertFX or filter.

FXChannel:
1. The effect chosen for a slot depends on what effects are already used in other slots (and slots in other channels).

*/

const ParamInfo* GetParamInfo(const ParamID& paramID)
{
	ParamInfo* pResult{ nullptr };

	auto elemItemIter = std::find_if(masterParamList.begin(), masterParamList.end(), [&paramID](auto& elemItem)
	{ return paramID.elemType == elemItem.elementType;});

	assert(elemItemIter != masterParamList.end());
	if (elemItemIter != masterParamList.end())
	{
		auto paramIter = std::find_if(elemItemIter->elementParamList.begin(), elemItemIter->elementParamList.end(),
			[&paramID](auto& paramInfo)
		{
			return paramInfo.portType == paramID.portType && paramInfo.portIndex == paramID.portIndex && paramInfo.subIndex == paramID.subIndex;
		});
		assert(paramIter != elemItemIter->elementParamList.end());
		if (paramIter != elemItemIter->elementParamList.end())
		{
			pResult = &(*paramIter);
		}
	}

	return pResult;
}


ParamAddress GetSysExAddressFromParamID(const ParamID& paramID)
{
	ParamAddress result;

	auto elemItemIter = std::find_if(masterParamList.cbegin(), masterParamList.cend(), [&paramID](auto& elemItem)
	{ return paramID.elemType == elemItem.elementType;});

	assert(elemItemIter != masterParamList.cend());
	if (elemItemIter != masterParamList.cend())
	{
		auto paramIter = std::find_if(elemItemIter->elementParamList.cbegin(), elemItemIter->elementParamList.cend(),
			[&paramID](auto& paramInfo)
		{
			return paramInfo.portType == paramID.portType && paramInfo.portIndex == paramID.portIndex && paramInfo.subIndex == paramID.subIndex;
		});
		assert(paramIter != elemItemIter->elementParamList.cend());
		if (paramIter != elemItemIter->elementParamList.cend())
		{
			auto index = paramIter - elemItemIter->elementParamList.cbegin();
			assert(paramID.elemIndex < elemItemIter->paramAddressList.size());
			assert((size_t)index < elemItemIter->paramAddressList[paramID.elemIndex].size());

			result = elemItemIter->paramAddressList[paramID.elemIndex][index];
		}
	}

	return result;
}
