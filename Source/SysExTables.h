#pragma once
/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <stdint.h>
#include "ParameterList.h"



const uint16_t SYS_MIDI_BLOCK_SIZE = 43;
const uint16_t OSC_MIDI_BLOCK_SIZE = 77;
const uint16_t ROT_MIDI_BLOCK_SIZE = 67;
const uint16_t VEC_MIDI_BLOCK_SIZE = 74;
const uint16_t VCA_MIDI_BLOCK_SIZE = 100;
const uint16_t FIL_MIDI_BLOCK_SIZE = 62;
const uint16_t MIX_MIDI_BLOCK_SIZE = 116;
const uint16_t INS_MIDI_BLOCK_SIZE = 44;
const uint16_t LFO_MIDI_BLOCK_SIZE = 48;
const uint16_t ENV_MIDI_BLOCK_SIZE = 33;
const uint16_t LEG_MIDI_BLOCK_SIZE = 78;
const uint16_t NAME_MIDI_BLOCK_SIZE = 42;
const uint16_t COM_MIDI_BLOCK_SIZE = 105;
const uint16_t EFF_MIDI_BLOCK_SIZE = 97;
const uint16_t ARP_MIDI_BLOCK_SIZE = 10;
const uint16_t ARPSTEP_MIDI_BLOCK_SIZE = 128;
const uint16_t SEQ_MIDI_BLOCK_SIZE = 9;
const uint16_t SEQSTEP_MIDI_BLOCK_SIZE = 128;
const uint16_t KEY_MIDI_BLOCK_SIZE = 128;
const uint16_t LARGEST_MIDI_BLOCK_SIZE = 128;


extern uint8_t sysByteCount[SYS_MIDI_BLOCK_SIZE];
extern ParamID sysParamID[SYS_MIDI_BLOCK_SIZE];
extern uint8_t oscByteCount[OSC_MIDI_BLOCK_SIZE];
extern ParamID oscParamID[4][OSC_MIDI_BLOCK_SIZE];
extern uint8_t rotByteCount[ROT_MIDI_BLOCK_SIZE];
extern ParamID rotParamID[2][ROT_MIDI_BLOCK_SIZE];
extern uint8_t vecByteCount[VEC_MIDI_BLOCK_SIZE];
extern ParamID vecParamID[VEC_MIDI_BLOCK_SIZE];
extern uint8_t vcaByteCount[VCA_MIDI_BLOCK_SIZE];
extern ParamID vcaParamID[VCA_MIDI_BLOCK_SIZE];
extern uint8_t filByteCount[FIL_MIDI_BLOCK_SIZE];
extern ParamID filParamID[4][FIL_MIDI_BLOCK_SIZE];
extern uint8_t mixByteCount[MIX_MIDI_BLOCK_SIZE];
extern ParamID mixParamID[MIX_MIDI_BLOCK_SIZE];
extern uint8_t insByteCount[INS_MIDI_BLOCK_SIZE];
extern ParamID insParamID[INS_MIDI_BLOCK_SIZE];
extern uint8_t lfoByteCount[LFO_MIDI_BLOCK_SIZE];
extern ParamID lfoParamID[5][LFO_MIDI_BLOCK_SIZE];
extern uint8_t envByteCount[ENV_MIDI_BLOCK_SIZE];
extern ParamID envParamID[6][ENV_MIDI_BLOCK_SIZE];
extern uint8_t legByteCount[LEG_MIDI_BLOCK_SIZE];
extern ParamID legParamID[LEG_MIDI_BLOCK_SIZE];
extern uint8_t seqByteCount[SEQ_MIDI_BLOCK_SIZE];
extern ParamID seqParamID[SEQ_MIDI_BLOCK_SIZE];
extern uint8_t seqStepByteCount[SEQSTEP_MIDI_BLOCK_SIZE];
extern ParamID seqStepParamID[SEQSTEP_MIDI_BLOCK_SIZE];
extern uint8_t namByteCount[NAME_MIDI_BLOCK_SIZE];
extern ParamID namParamID[NAME_MIDI_BLOCK_SIZE];
extern uint8_t comByteCount[COM_MIDI_BLOCK_SIZE];
extern ParamID comParamID[COM_MIDI_BLOCK_SIZE];
extern uint8_t effByteCount[EFF_MIDI_BLOCK_SIZE];
extern ParamID effParamID[EFF_MIDI_BLOCK_SIZE];
extern uint8_t arpByteCount[ARP_MIDI_BLOCK_SIZE];
extern ParamID arpParamID[ARP_MIDI_BLOCK_SIZE];
extern uint8_t arpStepByteCount[ARPSTEP_MIDI_BLOCK_SIZE];
extern ParamID arpStepParamID[ARPSTEP_MIDI_BLOCK_SIZE];
extern uint8_t keyByteCount[KEY_MIDI_BLOCK_SIZE];
extern ParamID keyParamID[8][KEY_MIDI_BLOCK_SIZE];
extern uint8_t frameByteCount[1];
extern ParamID frameParamID[1];

