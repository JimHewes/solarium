/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <sstream>
#include <iomanip>
#include <array>
#include <cassert>
#include "EQPanel.h"
#include "Command.h"

std::array<double, 210> qvalues 
{ 
	/* 47  */	0.70,  0.70,  0.73,	 0.76,  0.79,  0.83,  0.86,  0.89,  0.92, 0.96, 0.99,
	/* 58  */	1.03,  1.06,  1.10,	 1.14,  1.17,  1.21,  1.25,  1.29,  1.33, 1.37, 1.41, 1.45, 1.50, 1.54, 1.58, 1.63, 1.67, 1.72, 1.76, 1.81, 1.86, 1.90, 1.95,
	/* 81  */	2.00,  2.05,  2.10,	 2.15,  2.20,  2.26,  2.31,  2.36,  2.42, 2.47, 2.53, 2.58, 2.64, 2.70, 2.75, 2.81, 2.87, 2.93, 2.99,
	/* 100 */	3.05,  3.11,  3.18,	 3.24,  3.30,  3.36,  3.43,  3.49,  3.56, 3.63, 3.69, 3.76, 3.83, 3.90, 3.97,
	/* 115 */	4.04,  4.11,  4.18,	 4.25,  4.32,  4.39,  4.47,  4.54,  4.62, 4.69, 4.77, 4.84, 4.92,
	/* 128 */	5.00,  5.08,  5.16,	 5.24,  5.32,  5.40,  5.48,  5.56,  5.64, 5.73, 5.81, 5.90, 5.98,
	/* 141 */	6.07,  6.15,  6.24,	 6.33,  6.42,  6.51,  6.59,  6.68,  6.78, 6.87, 6.96,
	/* 152 */	7.05,  7.14,  7.24,	 7.33,  7.43,  7.52,  7.62,  7.72,  7.81, 7.91,
	/* 162 */	8.01,  8.11,  8.21,	 8.31,  8.41,  8.51,  8.61,  8.72,  8.82, 8.92,
	/* 172 */	9.03,  9.13,  9.24,	 9.35,  9.45,  9.56,  9.67,  9.78,  9.89,
	/* 181 */	10.00, 10.11, 10.22, 10.33, 10.44, 10.56, 10.67, 10.79, 10.90,
	/* 190 */	11.02, 11.13, 11.25, 11.37, 11.49, 11.60, 11.72, 11.84, 11.96,
	/* 199 */	12.09, 12.21, 12.33, 12.45, 12.58, 12.70, 12.83, 12.95,
	/* 207 */	13.08, 13.20, 13.33, 13.46, 13.59, 13.72, 13.85, 13.98,
	/* 215 */	14.11, 14.24, 14.37, 14.50, 14.64, 14.77, 14.91,
	/* 222 */	15.04, 15.18, 15.31, 15.45, 15.59, 15.73, 15.86,
	/* 229 */	16.00, 16.14, 16.28, 16.43, 16.57, 16.71, 16.85,
	/* 236 */	17.00, 17.14, 17.29, 17.43, 17.58, 17.72, 17.87,
	/* 243 */	18.02, 18.17, 18.32, 18.47, 18.62, 18.77, 18.92,
	/* 250 */	19.07, 19.23, 19.38, 19.53, 19.69, 19.84,
	/* 256 */	20.00
} ;


String QSlider::getTextFromValue(double value)
{
	// Value range from 47 to 256 in increasing step size.
	assert(value >= 47 && value <= 256);

	int valueInt = std::min(256L, std::max(47L, lrint(value)));
	int index = valueInt - 47;
	int fieldWidth = (value >= 181) ? 4 : 3;

	std::ostringstream os;
	os << std::fixed << std::setw(fieldWidth) << std::setprecision(2) << qvalues[index];
	
	return os.str().c_str();
}

double QSlider::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	size_t i;
	for (i = 0; i < qvalues.size(); ++i)
	{
		if (value >= qvalues[i])
		{
			break;
		}
	}

	return i + 47;
}

static const juce::Identifier paramIndexID = "paramIndex";

EQPanel::EQPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID,int slot)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	,m_pCommandHistory(pCommandHistory)
{

	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_subtitle.setFont(Font(17.0f));
	m_subtitle.setBounds(leftX,yPos,rightX + rightWidth,ctrlHeight);
	static char text[] = "Slot n: EQ";
	text[5] = char(0x31 + slot);
	m_subtitle.setText(text,NotificationType::dontSendNotification);
	addAndMakeVisible(m_subtitle);

	yPos += rowHeight;
	m_modeButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_modeButton.setButtonText("Mode");
	m_modeButton.addListener(this);
	addAndMakeVisible(m_modeButton);

	yPos += rowHeight + 10;
	m_freq1Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_freq1Label.setText("Freq 1:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_freq1Label);
	m_freq1Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_freq1Slider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqFreq1));
	m_freq1Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_freq1Slider.setTextValueSuffix(" Hz");
	m_freq1Slider.getProperties().set(paramIndexID, eqFreq1);
	m_freq1Slider.addListener(this);
	addAndMakeVisible(m_freq1Slider);

	yPos += rowHeight;
	m_q1Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_q1Label.setText("Q 1:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_q1Label);
	m_q1Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_q1Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqQ1));
	m_q1Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_q1Slider.getProperties().set(paramIndexID, eqQ1);
	m_q1Slider.addListener(this);
	addAndMakeVisible(m_q1Slider);

	yPos += rowHeight;
	m_gain1Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_gain1Label.setText("Gain 1:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_gain1Label);
	m_gain1Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_gain1Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqGain1));
	m_gain1Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_gain1Slider.getProperties().set(paramIndexID, eqGain1);
	m_gain1Slider.addListener(this);
	addAndMakeVisible(m_gain1Slider);


	yPos += rowHeight + 10;
	m_freq2Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_freq2Label.setText("Freq 2:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_freq2Label);
	m_freq2Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_freq2Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqFreq2));
	m_freq2Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_freq2Slider.setTextValueSuffix(" Hz");
	m_freq2Slider.getProperties().set(paramIndexID, eqFreq2);
	m_freq2Slider.addListener(this);
	addAndMakeVisible(m_freq2Slider);

	yPos += rowHeight;
	m_q2Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_q2Label.setText("Q 2:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_q2Label);
	m_q2Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_q2Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqQ2));
	m_q2Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_q2Slider.getProperties().set(paramIndexID, eqQ2);
	m_q2Slider.addListener(this);
	addAndMakeVisible(m_q2Slider);

	yPos += rowHeight;
	m_gain2Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_gain2Label.setText("Gain 2:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_gain2Label);
	m_gain2Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_gain2Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqGain2));
	m_gain2Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_gain2Slider.getProperties().set(paramIndexID, eqGain2);
	m_gain2Slider.addListener(this);
	addAndMakeVisible(m_gain2Slider);


	yPos += rowHeight + 10;
	m_freq3Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_freq3Label.setText("Freq 3:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_freq3Label);
	m_freq3Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_freq3Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqFreq3));
	m_freq3Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_freq3Slider.setTextValueSuffix(" Hz");
	m_freq3Slider.getProperties().set(paramIndexID, eqFreq3);
	m_freq3Slider.addListener(this);
	addAndMakeVisible(m_freq3Slider);

	yPos += rowHeight;
	m_q3Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_q3Label.setText("Q 3:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_q3Label);
	m_q3Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_q3Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqQ3));
	m_q3Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_q3Slider.getProperties().set(paramIndexID, eqQ3);
	m_q3Slider.addListener(this);
	addAndMakeVisible(m_q3Slider);

	yPos += rowHeight;
	m_gain3Label.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_gain3Label.setText("Gain 3:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_gain3Label);
	m_gain3Slider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_gain3Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,eqGain3));
	m_gain3Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_gain3Slider.getProperties().set(paramIndexID, eqGain3);
	m_gain3Slider.addListener(this);
	addAndMakeVisible(m_gain3Slider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}


EQPanel::~EQPanel()
{
	m_modeButton.removeListener(this);
	m_freq1Slider.removeListener(this);
	m_q1Slider.removeListener(this);
	m_gain1Slider.removeListener(this);
	m_freq2Slider.removeListener(this);
	m_q2Slider.removeListener(this);
	m_gain2Slider.removeListener(this);
	m_freq3Slider.removeListener(this);
	m_q3Slider.removeListener(this);
	m_gain3Slider.removeListener(this);
}

void EQPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_freq1Slider) ||
		(slider == &m_q1Slider) ||
		(slider == &m_gain1Slider) ||
		(slider == &m_freq2Slider) ||
		(slider == &m_q2Slider) ||
		(slider == &m_gain2Slider) ||
		(slider == &m_freq3Slider) ||
		(slider == &m_q3Slider) ||
		(slider == &m_gain3Slider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}


void EQPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}


// This function gets called after the button has been clicked and the state is changed.
void EQPanel::buttonClicked(Button* button)
{
	if (button == &m_modeButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, eqMode), m_modeButton.getToggleState() ? 1 : 0));
	}
}

void EQPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_modeButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,eqMode)) != 0,NotificationType::dontSendNotification);
	m_freq1Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqFreq1)), NotificationType::dontSendNotification);
	m_q1Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqQ1)), NotificationType::dontSendNotification);
	m_gain1Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqGain1)), NotificationType::dontSendNotification);
	m_freq2Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqFreq2)), NotificationType::dontSendNotification);
	m_q2Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqQ2)), NotificationType::dontSendNotification);
	m_gain2Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqGain2)), NotificationType::dontSendNotification);
	m_freq3Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqFreq3)), NotificationType::dontSendNotification);
	m_q3Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqQ3)), NotificationType::dontSendNotification);
	m_gain3Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,eqGain3)), NotificationType::dontSendNotification);

}
