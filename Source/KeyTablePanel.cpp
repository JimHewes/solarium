/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <sstream>
#include <iomanip>
#include <cassert>
#include "KeyTablePanel.h"
#include "Command.h"


String SliderKeyTable::getTextFromValue(double value)
{
	assert(value >= -1 && value <= 1000);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(1);
	os << value / 10.0;

	return os.str();
}

double SliderKeyTable::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	value = (value * 10);
	return std::max(0.0, std::min(1000.0, value));
}

KeyTablePanel::KeyTablePanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	, m_pCommandHistory(pCommandHistory)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 4;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_prevLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_prevLabel.setText("Prev:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_prevLabel);
	m_prevData.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	addAndMakeVisible(m_prevData);

	yPos += rowHeight;
	m_prevValueLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_prevValueLabel.setText("Value:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_prevValueLabel);
	m_prevValueData.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	addAndMakeVisible(m_prevValueData);


	yPos += rowHeight + 12;
	m_currentKeyLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_currentKeyLabel.setText("Current:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_currentKeyLabel);
	m_currentKeySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_currentKeySlider.setSliderStyle(Slider2::SliderStyle::IncDecButtons);
	m_currentKeySlider.setTextBoxStyle(Slider2::TextEntryBoxPosition::TextBoxLeft,false,36,ctrlHeight);
	m_currentKeySlider.setRange(0, m_pPreset->GetNumberOfParametersInElement(ElementType::KeyTable) - 1,1);
	m_currentKeySlider.addListener(this);
	addAndMakeVisible(m_currentKeySlider);


	yPos += rowHeight;
	m_currentValueLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_currentValueLabel.setText("Fixed:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_currentValueLabel);

	m_currentValueSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_currentValueSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,0));
	m_currentValueSlider.setRange(0.0, rangeInfo.maxValue , 1);
	m_currentValueSlider.setTextValueSuffix("%");
	m_currentValueSlider.addListener(this);
	addAndMakeVisible(m_currentValueSlider);

	m_initCurrentValueButton.setButtonText("Clear");
	m_initCurrentValueButton.setBounds(rightX + 42 ,yPos + ctrlHeight + 2,rightWidth - 42,17);
	m_initCurrentValueButton.setTooltip("Clears the fixed value of the current key so it becomes interpolated.");
	m_initCurrentValueButton.addListener(this);
	addAndMakeVisible(m_initCurrentValueButton);


	yPos += rowHeight + 22;
	m_nextLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_nextLabel.setText("Next:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_nextLabel);
	m_nextData.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	addAndMakeVisible(m_nextData);

	yPos += rowHeight;
	m_nextValueLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_nextValueLabel.setText("Value:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_nextValueLabel);
	m_nextValueData.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	addAndMakeVisible(m_nextValueData);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool) { Update(); });

	Update();
}

KeyTablePanel::~KeyTablePanel()
{
	m_multipleParametersChangedConnection->Disconnect();
	m_currentKeySlider.removeListener(this);
	m_currentValueSlider.removeListener(this);
	m_initCurrentValueButton.removeListener(this);
}

void KeyTablePanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void KeyTablePanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_currentKeySlider)
	{
		Update();
	}

}
void KeyTablePanel::sliderDragEnded(Slider2* slider)
{
	if (slider == &m_currentValueSlider)
	{
		ParamID paramID(m_elemID, static_cast<int>(m_currentKeySlider.getValue()));
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, static_cast<int>(m_currentValueSlider.getValue())));

		m_currentValueLabel.setText("Fixed:",NotificationType::dontSendNotification);
	}
}

void KeyTablePanel::buttonClicked(Button *button)
{
	if (button == &m_initCurrentValueButton)
	{
		int currentKey = static_cast<int>(m_currentKeySlider.getValue());
		ParamID paramID(m_elemID, currentKey);
		int currentValue = m_pPreset->GetParameterValue(paramID);
		if (currentValue >= 0)
		{
			m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, currentKey), (1 << 13) ));
			Update();
		}
	}
}

inline bool KeyValueIsFixed(int value)
{
	return (value & (1 << 13)) == 0;
}

void KeyTablePanel::Update()
{

	int currentKey = static_cast<int>(m_currentKeySlider.getValue());
	ParamID paramID(m_elemID, currentKey);
	int currentValue = m_pPreset->GetParameterValue(paramID);
	if (KeyValueIsFixed(currentValue))
	{
		m_currentValueLabel.setText("Fixed:",NotificationType::dontSendNotification);
	}
	else
	{
		m_currentValueLabel.setText("Interpol:",NotificationType::dontSendNotification);
	}
	currentValue &= 0x3FF;	// Mask off the fixed/interpolated flag. The value is in the low 10 bits.
	m_currentValueSlider.setValue(currentValue, NotificationType::dontSendNotification);


	std::ostringstream os;

	
	int previousFixedKey = currentKey - 1;;
	uint32_t previousValue = 0;
	while (previousFixedKey >= 0)
	{
		paramID.subIndex = static_cast<uint8_t>(previousFixedKey);
		previousValue = m_pPreset->GetParameterValue(paramID);
		if (KeyValueIsFixed(previousValue))
		{
			break;
		}
		--previousFixedKey;
	};
	if (previousFixedKey < 0)
	{
		m_prevData.setText("---", NotificationType::dontSendNotification);
		m_prevValueData.setText("0.0%", NotificationType::dontSendNotification);
		previousFixedKey = 0;
		previousValue = 0;
	}
	else
	{
		os << previousFixedKey;
		m_prevData.setText(os.str(), NotificationType::dontSendNotification);
		os.str("");
		os << std::fixed << std::setprecision(1) << previousValue / 10.0 << "%";
		m_prevValueData.setText(os.str(), NotificationType::dontSendNotification);
	}

	int nextFixedKey = currentKey + 1;;
	uint32_t nextValue = 0;
	int lastIndex = m_pPreset->GetNumberOfParametersInElement(ElementType::KeyTable) - 1;
	while (nextFixedKey <= lastIndex)
	{
		paramID.subIndex = static_cast<uint8_t>(nextFixedKey);
		nextValue = m_pPreset->GetParameterValue(paramID);
		if (KeyValueIsFixed(nextValue))
		{
			break;
		}
		++nextFixedKey;
	};

	if (nextFixedKey > lastIndex)
	{
		m_nextData.setText("---", NotificationType::dontSendNotification);
		m_nextValueData.setText("0.0%", NotificationType::dontSendNotification);
		nextFixedKey = lastIndex;
		nextValue = 0;
	}
	else
	{
		os.str("");
		os << nextFixedKey;
		m_nextData.setText(os.str(), NotificationType::dontSendNotification);
		os.str("");
		os << std::fixed << std::setprecision(1) << nextValue / 10.0 << "%";
		m_nextValueData.setText(os.str(), NotificationType::dontSendNotification);
	}

}


