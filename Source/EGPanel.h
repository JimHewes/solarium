/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef EGPANEL_H_INCLUDED
#define EGPANEL_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "Slider2.h"
#include "TimeSlider.h"

class CommandHistory;

class SlopeSlider : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};
class SliderSlopeSustain : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class EGPanel :
	public Component,
	public Slider2::Listener
{
public:
	EGPanel(Preset* pPreset, CommandHistory* pCpmmandHistory, ElementID elemID);
	virtual ~EGPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label		m_delayLabel;
	SliderTime	m_delaySlider;
	Label		m_attackLabel;
	SliderTime	m_attackSlider;
	Label		m_decayLabel;
	SliderTime	m_decaySlider;
	Label		m_sustainLabel;
	SliderProp	m_sustainSlider;
	Label		m_releaseLabel;
	SliderTime	m_releaseSlider;

	Label				m_aSlopeLabel;
	SlopeSlider			m_aSlopeSlider;
	Label				m_dSlopeLabel;
	SlopeSlider			m_dSlopeSlider;
	Label				m_sSlopeLabel;
	SliderSlopeSustain	m_sSlopeSlider;
	Label				m_rSlopeLabel;
	SlopeSlider			m_rSlopeSlider;
	Label				m_velocityLabel;
	SliderProp			m_velocitySlider;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};

#endif  // EGPANEL_H_INCLUDED
