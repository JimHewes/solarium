/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

//-------------------------------------------------------------------------------
//
//	NOTICE that this file is just a near copy of the JUCE Slider class. It is 
//	copied here in order to modify the mouse wheel behavior which could not be 
//	done by just overriding a function because the needed function is not virtual.
//	It's a lot of overkill for a small change, but it was a quick way to do it.
//
//	Since LookAndFeel is tied in with the standard components, some of the L&F 
//	functions had to be copied to here. This means that Slider2 is hardcoded to 
//	use the appearance of LookAndFeel_V2.
//-------------------------------------------------------------------------------


#include "Slider2.h"

namespace LookAndFeelHelpers
{
	static Colour createBaseColour(Colour buttonColour,
		bool hasKeyboardFocus,
		bool isMouseOverButton,
		bool isButtonDown) noexcept
	{
		const float sat = hasKeyboardFocus ? 1.3f : 0.9f;
		const Colour baseColour(buttonColour.withMultipliedSaturation(sat));

		if (isButtonDown)      return baseColour.contrasting(0.2f);
		if (isMouseOverButton) return baseColour.contrasting(0.1f);

		return baseColour;
	}
#if 0
	static TextLayout layoutTooltipText(const String& text, Colour colour) noexcept
	{
		const float tooltipFontSize = 13.0f;
		const int maxToolTipWidth = 400;

		AttributedString s;
		s.setJustification(Justification::centred);
		s.append(text, Font(tooltipFontSize, Font::bold), colour);

		TextLayout tl;
		tl.createLayoutWithBalancedLineLengths(s, (float)maxToolTipWidth);
		return tl;
	}
#endif
}

int getSliderThumbRadius(Slider2& slider)
{
	return jmin(7,
		slider.getHeight() / 2,
		slider.getWidth() / 2) + 2;
}

//==============================================================================
void drawShinyButtonShape(Graphics& g,
	float x, float y, float w, float h,
	float maxCornerSize,
	const Colour& baseColour,
	const float strokeWidth,
	const bool flatOnLeft,
	const bool flatOnRight,
	const bool flatOnTop,
	const bool flatOnBottom) noexcept
{
	if (w <= strokeWidth * 1.1f || h <= strokeWidth * 1.1f)
		return;

	const float cs = jmin(maxCornerSize, w * 0.5f, h * 0.5f);

	Path outline;
	outline.addRoundedRectangle(x, y, w, h, cs, cs,
		!(flatOnLeft || flatOnTop),
		!(flatOnRight || flatOnTop),
		!(flatOnLeft || flatOnBottom),
		!(flatOnRight || flatOnBottom));

	ColourGradient cg(baseColour, 0.0f, y,
		baseColour.overlaidWith(Colour(0x070000ff)), 0.0f, y + h,
		false);

	cg.addColour(0.5, baseColour.overlaidWith(Colour(0x33ffffff)));
	cg.addColour(0.51, baseColour.overlaidWith(Colour(0x110000ff)));

	g.setGradientFill(cg);
	g.fillPath(outline);

	g.setColour(Colour(0x80000000));
	g.strokePath(outline, PathStrokeType(strokeWidth));
}

//==============================================================================
void drawLinearSliderBackground(Graphics& g, int x, int y, int width, int height,
	float /*sliderPos*/,
	float /*minSliderPos*/,
	float /*maxSliderPos*/,
	const Slider2::SliderStyle /*style*/, Slider2& slider)
{
	const float sliderRadius = (float)(getSliderThumbRadius(slider) - 2);

	const Colour trackColour(slider.findColour(Slider2::trackColourId));
	const Colour gradCol1(trackColour.overlaidWith(Colours::black.withAlpha(slider.isEnabled() ? 0.25f : 0.13f)));
	const Colour gradCol2(trackColour.overlaidWith(Colour(0x14000000)));
	Path indent;

	if (slider.isHorizontal())
	{
		const float iy = y + height * 0.5f - sliderRadius * 0.5f;
		const float ih = sliderRadius;

		g.setGradientFill(ColourGradient(gradCol1, 0.0f, iy,
			gradCol2, 0.0f, iy + ih, false));

		indent.addRoundedRectangle(x - sliderRadius * 0.5f, iy,
			width + sliderRadius, ih,
			5.0f);
	}
	else
	{
		const float ix = x + width * 0.5f - sliderRadius * 0.5f;
		const float iw = sliderRadius;

		g.setGradientFill(ColourGradient(gradCol1, ix, 0.0f,
			gradCol2, ix + iw, 0.0f, false));

		indent.addRoundedRectangle(ix, y - sliderRadius * 0.5f,
			iw, height + sliderRadius,
			5.0f);
	}

	g.fillPath(indent);

	g.setColour(Colour(0x4c000000));
	g.strokePath(indent, PathStrokeType(0.5f));
}

//==============================================================================
void drawGlassSphere(Graphics& g, const float x, const float y,
	const float diameter, const Colour& colour,
	const float outlineThickness) noexcept
{
	if (diameter <= outlineThickness)
		return;

	Path p;
	p.addEllipse(x, y, diameter, diameter);

	{
		ColourGradient cg(Colours::white.overlaidWith(colour.withMultipliedAlpha(0.3f)), 0, y,
			Colours::white.overlaidWith(colour.withMultipliedAlpha(0.3f)), 0, y + diameter, false);

		cg.addColour(0.4, Colours::white.overlaidWith(colour));

		g.setGradientFill(cg);
		g.fillPath(p);
	}

	g.setGradientFill(ColourGradient(Colours::white, 0, y + diameter * 0.06f,
		Colours::transparentWhite, 0, y + diameter * 0.3f, false));
	g.fillEllipse(x + diameter * 0.2f, y + diameter * 0.05f, diameter * 0.6f, diameter * 0.4f);

	ColourGradient cg(Colours::transparentBlack,
		x + diameter * 0.5f, y + diameter * 0.5f,
		Colours::black.withAlpha(0.5f * outlineThickness * colour.getFloatAlpha()),
		x, y + diameter * 0.5f, true);

	cg.addColour(0.7, Colours::transparentBlack);
	cg.addColour(0.8, Colours::black.withAlpha(0.1f * outlineThickness));

	g.setGradientFill(cg);
	g.fillPath(p);

	g.setColour(Colours::black.withAlpha(0.5f * colour.getFloatAlpha()));
	g.drawEllipse(x, y, diameter, diameter, outlineThickness);
}

//==============================================================================
void drawGlassPointer(Graphics& g,
	const float x, const float y, const float diameter,
	const Colour& colour, const float outlineThickness,
	const int direction) noexcept
{
	if (diameter <= outlineThickness)
		return;

	Path p;
	p.startNewSubPath(x + diameter * 0.5f, y);
	p.lineTo(x + diameter, y + diameter * 0.6f);
	p.lineTo(x + diameter, y + diameter);
	p.lineTo(x, y + diameter);
	p.lineTo(x, y + diameter * 0.6f);
	p.closeSubPath();

	p.applyTransform(AffineTransform::rotation(direction * (float_Pi * 0.5f), x + diameter * 0.5f, y + diameter * 0.5f));

	{
		ColourGradient cg(Colours::white.overlaidWith(colour.withMultipliedAlpha(0.3f)), 0, y,
			Colours::white.overlaidWith(colour.withMultipliedAlpha(0.3f)), 0, y + diameter, false);

		cg.addColour(0.4, Colours::white.overlaidWith(colour));

		g.setGradientFill(cg);
		g.fillPath(p);
	}

	ColourGradient cg(Colours::transparentBlack,
		x + diameter * 0.5f, y + diameter * 0.5f,
		Colours::black.withAlpha(0.5f * outlineThickness * colour.getFloatAlpha()),
		x - diameter * 0.2f, y + diameter * 0.5f, true);

	cg.addColour(0.5, Colours::transparentBlack);
	cg.addColour(0.7, Colours::black.withAlpha(0.07f * outlineThickness));

	g.setGradientFill(cg);
	g.fillPath(p);

	g.setColour(Colours::black.withAlpha(0.5f * colour.getFloatAlpha()));
	g.strokePath(p, PathStrokeType(outlineThickness));
}


void drawLinearSliderThumb(Graphics& g, int x, int y, int width, int height,
	float sliderPos, float minSliderPos, float maxSliderPos,
	const Slider2::SliderStyle style, Slider2& slider)
{
	const float sliderRadius = (float)(getSliderThumbRadius(slider) - 2);

	Colour knobColour(LookAndFeelHelpers::createBaseColour(slider.findColour(Slider2::thumbColourId),
		slider.hasKeyboardFocus(false) && slider.isEnabled(),
		slider.isMouseOverOrDragging() && slider.isEnabled(),
		slider.isMouseButtonDown() && slider.isEnabled()));

	const float outlineThickness = slider.isEnabled() ? 0.8f : 0.3f;

	if (style == Slider2::LinearHorizontal || style == Slider2::LinearVertical)
	{
		float kx, ky;

		if (style == Slider2::LinearVertical)
		{
			kx = x + width * 0.5f;
			ky = sliderPos;
		}
		else
		{
			kx = sliderPos;
			ky = y + height * 0.5f;
		}

		drawGlassSphere(g,
			kx - sliderRadius,
			ky - sliderRadius,
			sliderRadius * 2.0f,
			knobColour, outlineThickness);
	}
	else
	{
		if (style == Slider2::ThreeValueVertical)
		{
			drawGlassSphere(g, x + width * 0.5f - sliderRadius,
				sliderPos - sliderRadius,
				sliderRadius * 2.0f,
				knobColour, outlineThickness);
		}
		else if (style == Slider2::ThreeValueHorizontal)
		{
			drawGlassSphere(g, sliderPos - sliderRadius,
				y + height * 0.5f - sliderRadius,
				sliderRadius * 2.0f,
				knobColour, outlineThickness);
		}

		if (style == Slider2::TwoValueVertical || style == Slider2::ThreeValueVertical)
		{
			const float sr = jmin(sliderRadius, width * 0.4f);

			drawGlassPointer(g, jmax(0.0f, x + width * 0.5f - sliderRadius * 2.0f),
				minSliderPos - sliderRadius,
				sliderRadius * 2.0f, knobColour, outlineThickness, 1);

			drawGlassPointer(g, jmin(x + width - sliderRadius * 2.0f, x + width * 0.5f), maxSliderPos - sr,
				sliderRadius * 2.0f, knobColour, outlineThickness, 3);
		}
		else if (style == Slider2::TwoValueHorizontal || style == Slider2::ThreeValueHorizontal)
		{
			const float sr = jmin(sliderRadius, height * 0.4f);

			drawGlassPointer(g, minSliderPos - sr,
				jmax(0.0f, y + height * 0.5f - sliderRadius * 2.0f),
				sliderRadius * 2.0f, knobColour, outlineThickness, 2);

			drawGlassPointer(g, maxSliderPos - sliderRadius,
				jmin(y + height - sliderRadius * 2.0f, y + height * 0.5f),
				sliderRadius * 2.0f, knobColour, outlineThickness, 4);
		}
	}
}

/** @brief The code for this function is taken from LookAndFeel_V3 rather than LookAndFeel_V3 because we 
			don't want the "shiny" look of V2. It doesn't work well for vertical sliders.
*/
void drawLinearSlider(Graphics& g, int x, int y, int width, int height,
	float sliderPos, float minSliderPos, float maxSliderPos,
	const Slider2::SliderStyle style, Slider2& slider)
{
	g.fillAll(slider.findColour(Slider::backgroundColourId));

	if (style == Slider2::LinearBar || style == Slider2::LinearBarVertical)
	{
		const float fx = (float)x, fy = (float)y, fw = (float)width, fh = (float)height;

		Path p;

		if (style == Slider2::LinearBarVertical)
			p.addRectangle(fx, sliderPos, fw, 1.0f + fh - sliderPos);
		else
			p.addRectangle(fx, fy, sliderPos - fx, fh);

		Colour baseColour(slider.findColour(Slider::thumbColourId)
			.withMultipliedSaturation(slider.isEnabled() ? 1.0f : 0.5f)
			.withMultipliedAlpha(0.8f));

		g.setGradientFill(ColourGradient(baseColour.brighter(0.08f), 0.0f, 0.0f,
			baseColour.darker(0.08f), 0.0f, (float)height, false));
		g.fillPath(p);

		g.setColour(baseColour.darker(0.2f));

		if (style == Slider2::LinearBarVertical)
			g.fillRect(fx, sliderPos, fw, 1.0f);
		else
			g.fillRect(sliderPos, fy, 1.0f, fh);
	}
	else
	{
		drawLinearSliderBackground(g, x, y, width, height, sliderPos, minSliderPos, maxSliderPos, style, slider);
		drawLinearSliderThumb(g, x, y, width, height, sliderPos, minSliderPos, maxSliderPos, style, slider);
	}
}

void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
	const float rotaryStartAngle, const float rotaryEndAngle, Slider2& slider)
{
	const float radius = jmin(width / 2, height / 2) - 2.0f;
	const float centreX = x + width * 0.5f;
	const float centreY = y + height * 0.5f;
	const float rx = centreX - radius;
	const float ry = centreY - radius;
	const float rw = radius * 2.0f;
	const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
	const bool isMouseOver = slider.isMouseOverOrDragging() && slider.isEnabled();

	if (radius > 12.0f)
	{
		if (slider.isEnabled())
			g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
		else
			g.setColour(Colour(0x80808080));

		const float thickness = 0.7f;

		{
			Path filledArc;
			filledArc.addPieSegment(rx, ry, rw, rw, rotaryStartAngle, angle, thickness);
			g.fillPath(filledArc);
		}

		{
			const float innerRadius = radius * 0.2f;
			Path p;
			p.addTriangle(-innerRadius, 0.0f,
				0.0f, -radius * thickness * 1.1f,
				innerRadius, 0.0f);

			p.addEllipse(-innerRadius, -innerRadius, innerRadius * 2.0f, innerRadius * 2.0f);

			g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
		}

		if (slider.isEnabled())
			g.setColour(slider.findColour(Slider::rotarySliderOutlineColourId));
		else
			g.setColour(Colour(0x80808080));

		Path outlineArc;
		outlineArc.addPieSegment(rx, ry, rw, rw, rotaryStartAngle, rotaryEndAngle, thickness);
		outlineArc.closeSubPath();

		g.strokePath(outlineArc, PathStrokeType(slider.isEnabled() ? (isMouseOver ? 2.0f : 1.2f) : 0.3f));
	}
	else
	{
		if (slider.isEnabled())
			g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
		else
			g.setColour(Colour(0x80808080));

		Path p;
		p.addEllipse(-0.4f * rw, -0.4f * rw, rw * 0.8f, rw * 0.8f);
		PathStrokeType(rw * 0.1f).createStrokedPath(p, p);

		p.addLineSegment(Line<float>(0.0f, 0.0f, 0.0f, -radius), rw * 0.2f);

		g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
	}
}


//==============================================================================
Slider2::SliderLayout getSliderLayout(Slider2& slider)
{
	// 1. compute the actually visible textBox size from the slider textBox size and some additional constraints

	int minXSpace = 0;
	int minYSpace = 0;

	Slider2::TextEntryBoxPosition textBoxPos = slider.getTextBoxPosition();

	if (textBoxPos == Slider2::TextBoxLeft || textBoxPos == Slider2::TextBoxRight)
		minXSpace = 30;
	else
		minYSpace = 15;

	Rectangle<int> localBounds = slider.getLocalBounds();

	const int textBoxWidth = jmax(0, jmin(slider.getTextBoxWidth(), localBounds.getWidth() - minXSpace));
	const int textBoxHeight = jmax(0, jmin(slider.getTextBoxHeight(), localBounds.getHeight() - minYSpace));

	Slider2::SliderLayout layout;

	// 2. set the textBox bounds

	if (textBoxPos != Slider2::NoTextBox)
	{
		if (slider.isBar())
		{
			layout.textBoxBounds = localBounds;
		}
		else
		{
			layout.textBoxBounds.setWidth(textBoxWidth);
			layout.textBoxBounds.setHeight(textBoxHeight);

			if (textBoxPos == Slider2::TextBoxLeft)           layout.textBoxBounds.setX(0);
			else if (textBoxPos == Slider2::TextBoxRight)     layout.textBoxBounds.setX(localBounds.getWidth() - textBoxWidth);
			else /* above or below -> centre horizontally */ layout.textBoxBounds.setX((localBounds.getWidth() - textBoxWidth) / 2);

			if (textBoxPos == Slider2::TextBoxAbove)          layout.textBoxBounds.setY(0);
			else if (textBoxPos == Slider2::TextBoxBelow)     layout.textBoxBounds.setY(localBounds.getHeight() - textBoxHeight);
			else /* left or right -> centre vertically */    layout.textBoxBounds.setY((localBounds.getHeight() - textBoxHeight) / 2);
		}
	}

	// 3. set the slider bounds

	layout.sliderBounds = localBounds;

	if (slider.isBar())
	{
		layout.sliderBounds.reduce(1, 1);   // bar border
	}
	else
	{
		if (textBoxPos == Slider2::TextBoxLeft)       layout.sliderBounds.removeFromLeft(textBoxWidth);
		else if (textBoxPos == Slider2::TextBoxRight) layout.sliderBounds.removeFromRight(textBoxWidth);
		else if (textBoxPos == Slider2::TextBoxAbove) layout.sliderBounds.removeFromTop(textBoxHeight);
		else if (textBoxPos == Slider2::TextBoxBelow) layout.sliderBounds.removeFromBottom(textBoxHeight);

		const int thumbIndent = getSliderThumbRadius(slider);

		if (slider.isHorizontal())    layout.sliderBounds.reduce(thumbIndent, 0);
		else if (slider.isVertical()) layout.sliderBounds.reduce(0, thumbIndent);
	}

	return layout;
}

class SliderLabelComp : public Label
{
public:
	SliderLabelComp() : Label() {}

	void mouseWheelMove(const MouseEvent&, const MouseWheelDetails&) {}
};

Label* createSliderTextBox(Slider2& slider)
{
	Label* const l = new SliderLabelComp();

	l->setJustificationType(Justification::centred);
	l->setKeyboardType(TextInputTarget::decimalKeyboard);

	l->setColour(Label::textColourId, slider.findColour(Slider::textBoxTextColourId));
	l->setColour(Label::backgroundColourId,
		(slider.getSliderStyle() == Slider2::LinearBar || slider.getSliderStyle() == Slider2::LinearBarVertical)
		? Colours::transparentBlack
		: slider.findColour(Slider::textBoxBackgroundColourId));
	l->setColour(Label::outlineColourId, slider.findColour(Slider::textBoxOutlineColourId));
	l->setColour(TextEditor::textColourId, slider.findColour(Slider::textBoxTextColourId));
	l->setColour(TextEditor::backgroundColourId,
		slider.findColour(Slider::textBoxBackgroundColourId)
		.withAlpha((slider.getSliderStyle() == Slider2::LinearBar || slider.getSliderStyle() == Slider2::LinearBarVertical)
			? 0.7f : 1.0f));
	l->setColour(TextEditor::outlineColourId, slider.findColour(Slider::textBoxOutlineColourId));
	l->setColour(TextEditor::highlightColourId, slider.findColour(Slider::textBoxHighlightColourId));

	return l;
}

Button* createSliderButton(Slider2&, const bool isIncrement)
{
	return new TextButton(isIncrement ? "+" : "-", {});
}

ImageEffectFilter* getSliderEffect(Slider2&)
{
	return nullptr;
}

Font getSliderPopupFont(Slider2&)
{
	return Font(15.0f, Font::bold);
}

int getSliderPopupPlacement(Slider2&)
{
	return BubbleComponent::above
		| BubbleComponent::below
		| BubbleComponent::left
		| BubbleComponent::right;
}



class Slider2::Pimpl : public AsyncUpdater,
	public Button::Listener,
	public Label::Listener,
	public Value::Listener
{
public:
	Pimpl(Slider2& s, SliderStyle sliderStyle, TextEntryBoxPosition textBoxPosition)
		: owner(s),
		style(sliderStyle),
		lastCurrentValue(0), lastValueMin(0), lastValueMax(0),
		minimum(0), maximum(10), interval(0), defaultValue(0),
		skewFactor(1.0), velocityModeSensitivity(1.0),
		velocityModeOffset(0.0), velocityModeThreshold(1),
		rotaryStart(float_Pi * 1.2f),
		rotaryEnd(float_Pi * 2.8f),
		sliderRegionStart(0), sliderRegionSize(1), sliderBeingDragged(-1),
		pixelsForFullDragExtent(250),
		textBoxPos(textBoxPosition),
		numDecimalPlaces(7),
		textBoxWidth(80), textBoxHeight(20),
		incDecButtonMode(incDecButtonsNotDraggable),
		ctrlCmdClickToDefault(true),	// new variable for Slider2
		ctrlCmdClickToEdit(false),		// changed from base class Slider
		doubleClickToDefault(false),	// changed from base class 
		doubleClickToEdit(true),		// new variable for Slider2
		isVelocityBased(false),
		userKeyOverridesVelocity(true),
		rotaryStop(true),
		incDecButtonsSideBySide(false),
		sendChangeOnlyOnRelease(false),
		popupDisplayEnabled(false),
		menuEnabled(false),
		useDragEvents(false),
		scrollWheelEnabled(true),
		snapsToMousePos(false),	// changed from base class Slider
		snapsToMousePosSave(snapsToMousePos),
		snapsToMousePosOnDrag(true),	// new variable for Slider2
		parentForPopupDisplay(nullptr),
		reverseMode(false)	// new variable for Slider2
	{
	}

	~Pimpl()
	{
		currentValue.removeListener(this);
		valueMin.removeListener(this);
		valueMax.removeListener(this);
		popupDisplay = nullptr;
	}

	//==============================================================================
	void registerListeners()
	{
		currentValue.addListener(this);
		valueMin.addListener(this);
		valueMax.addListener(this);
	}

	bool isHorizontal() const noexcept
	{
		return style == LinearHorizontal
			|| style == LinearBar
			|| style == TwoValueHorizontal
			|| style == ThreeValueHorizontal;
	}

	bool isVertical() const noexcept
	{
		return style == LinearVertical
			|| style == LinearBarVertical
			|| style == TwoValueVertical
			|| style == ThreeValueVertical;
	}

	bool isRotary() const noexcept
	{
		return style == Rotary
			|| style == RotaryHorizontalDrag
			|| style == RotaryVerticalDrag
			|| style == RotaryHorizontalVerticalDrag;
	}

	bool isBar() const noexcept
	{
		return style == LinearBar
			|| style == LinearBarVertical;
	}

	bool incDecDragDirectionIsHorizontal() const noexcept
	{
		return incDecButtonMode == incDecButtonsDraggable_Horizontal
			|| (incDecButtonMode == incDecButtonsDraggable_AutoDirection && incDecButtonsSideBySide);
	}

	float getPositionOfValue(const double value) const
	{
		if (isHorizontal() || isVertical())
			return getLinearSliderPos(value);

		jassertfalse; // not a valid call on a slider that doesn't work linearly!
		return 0.0f;
	}

	void setRange(const double newMin, const double newMax, const double newInt)
	{
		if (minimum != newMin || maximum != newMax || interval != newInt)
		{
			minimum = newMin;
			maximum = newMax;
			interval = newInt;

			// figure out the number of DPs needed to display all values at this
			// interval setting.
			numDecimalPlaces = 7;

			if (newInt != 0)
			{
				int v = std::abs(roundToInt(newInt * 10000000));

				while ((v % 10) == 0)
				{
					--numDecimalPlaces;
					v /= 10;
				}
			}

			// keep the current values inside the new range..
			if (style != TwoValueHorizontal && style != TwoValueVertical)
			{
				setValue(getValue(), dontSendNotification);
			}
			else
			{
				setMinValue(getMinValue(), dontSendNotification, false);
				setMaxValue(getMaxValue(), dontSendNotification, false);
			}

			updateText();
		}
	}

	double getValue() const
	{
		// for a two-value style slider, you should use the getMinValue() and getMaxValue()
		// methods to get the two values.
		jassert(style != TwoValueHorizontal && style != TwoValueVertical);

		return currentValue.getValue();
	}

	void setValue(double newValue, const NotificationType notification)
	{
		// for a two-value style slider, you should use the setMinValue() and setMaxValue()
		// methods to set the two values.
		jassert(style != TwoValueHorizontal && style != TwoValueVertical);

		newValue = constrainedValue(newValue);

		if (style == ThreeValueHorizontal || style == ThreeValueVertical)
		{
			jassert((double)valueMin.getValue() <= (double)valueMax.getValue());

			newValue = jlimit((double)valueMin.getValue(),
				(double)valueMax.getValue(),
				newValue);
		}

		if (newValue != lastCurrentValue)
		{
			if (valueBox != nullptr)
				valueBox->hideEditor(true);

			lastCurrentValue = newValue;

			// (need to do this comparison because the Value will use equalsWithSameType to compare
			// the new and old values, so will generate unwanted change events if the type changes)
			if (currentValue != newValue)
				currentValue = newValue;

			updateText();
			owner.repaint();

			if (popupDisplay != nullptr)
				popupDisplay->updatePosition(owner.getTextFromValue(newValue));

			triggerChangeMessage(notification);
		}
	}

	void setMinValue(double newValue, const NotificationType notification,
		const bool allowNudgingOfOtherValues)
	{
		// The minimum value only applies to sliders that are in two- or three-value mode.
		jassert(style == TwoValueHorizontal || style == TwoValueVertical
			|| style == ThreeValueHorizontal || style == ThreeValueVertical);

		newValue = constrainedValue(newValue);

		if (style == TwoValueHorizontal || style == TwoValueVertical)
		{
			if (allowNudgingOfOtherValues && newValue > (double)valueMax.getValue())
				setMaxValue(newValue, notification, false);

			newValue = jmin((double)valueMax.getValue(), newValue);
		}
		else
		{
			if (allowNudgingOfOtherValues && newValue > lastCurrentValue)
				setValue(newValue, notification);

			newValue = jmin(lastCurrentValue, newValue);
		}

		if (lastValueMin != newValue)
		{
			lastValueMin = newValue;
			valueMin = newValue;
			owner.repaint();

			if (popupDisplay != nullptr)
				popupDisplay->updatePosition(owner.getTextFromValue(newValue));

			triggerChangeMessage(notification);
		}
	}

	void setMaxValue(double newValue, const NotificationType notification,
		const bool allowNudgingOfOtherValues)
	{
		// The maximum value only applies to sliders that are in two- or three-value mode.
		jassert(style == TwoValueHorizontal || style == TwoValueVertical
			|| style == ThreeValueHorizontal || style == ThreeValueVertical);

		newValue = constrainedValue(newValue);

		if (style == TwoValueHorizontal || style == TwoValueVertical)
		{
			if (allowNudgingOfOtherValues && newValue < (double)valueMin.getValue())
				setMinValue(newValue, notification, false);

			newValue = jmax((double)valueMin.getValue(), newValue);
		}
		else
		{
			if (allowNudgingOfOtherValues && newValue < lastCurrentValue)
				setValue(newValue, notification);

			newValue = jmax(lastCurrentValue, newValue);
		}

		if (lastValueMax != newValue)
		{
			lastValueMax = newValue;
			valueMax = newValue;
			owner.repaint();

			if (popupDisplay != nullptr)
				popupDisplay->updatePosition(owner.getTextFromValue(valueMax.getValue()));

			triggerChangeMessage(notification);
		}
	}

	void setMinAndMaxValues(double newMinValue, double newMaxValue, const NotificationType notification)
	{
		// The maximum value only applies to sliders that are in two- or three-value mode.
		jassert(style == TwoValueHorizontal || style == TwoValueVertical
			|| style == ThreeValueHorizontal || style == ThreeValueVertical);

		if (newMaxValue < newMinValue)
			std::swap(newMaxValue, newMinValue);

		newMinValue = constrainedValue(newMinValue);
		newMaxValue = constrainedValue(newMaxValue);

		if (lastValueMax != newMaxValue || lastValueMin != newMinValue)
		{
			lastValueMax = newMaxValue;
			lastValueMin = newMinValue;
			valueMin = newMinValue;
			valueMax = newMaxValue;
			owner.repaint();

			triggerChangeMessage(notification);
		}
	}

	double getMinValue() const
	{
		// The minimum value only applies to sliders that are in two- or three-value mode.
		jassert(style == TwoValueHorizontal || style == TwoValueVertical
			|| style == ThreeValueHorizontal || style == ThreeValueVertical);

		return valueMin.getValue();
	}

	double getMaxValue() const
	{
		// The maximum value only applies to sliders that are in two- or three-value mode.
		jassert(style == TwoValueHorizontal || style == TwoValueVertical
			|| style == ThreeValueHorizontal || style == ThreeValueVertical);

		return valueMax.getValue();
	}

	void triggerChangeMessage(const NotificationType notification)
	{
		if (notification != dontSendNotification)
		{
			owner.valueChanged();

			if (notification == sendNotificationSync)
				handleAsyncUpdate();
			else
				triggerAsyncUpdate();
		}
	}

	void handleAsyncUpdate() override
	{
		cancelPendingUpdate();

		Component::BailOutChecker checker(&owner);
		Slider2* slider = &owner; // (must use an intermediate variable here to avoid a VS2005 compiler bug)
		listeners.callChecked(checker, &Slider2Listener::sliderValueChanged, slider);  // (can't use Slider::Listener due to idiotic VC2005 bug)
	}

	void sendDragStart()
	{
		owner.startedDragging();

		Component::BailOutChecker checker(&owner);
		Slider2* slider = &owner; // (must use an intermediate variable here to avoid a VS2005 compiler bug)
		listeners.callChecked(checker, &Slider2Listener::sliderDragStarted, slider);
	}

	void sendDragEnd()
	{
		owner.stoppedDragging();

		sliderBeingDragged = -1;

		Component::BailOutChecker checker(&owner);
		Slider2* slider = &owner; // (must use an intermediate variable here to avoid a VS2005 compiler bug)
		listeners.callChecked(checker, &Slider2Listener::sliderDragEnded, slider);
	}

	struct DragInProgress
	{
		DragInProgress(Pimpl& p) : owner(p) { owner.sendDragStart(); }
		~DragInProgress() { owner.sendDragEnd(); }

		Pimpl& owner;

		JUCE_DECLARE_NON_COPYABLE(DragInProgress)
	};

	void buttonClicked(Button* button) override
	{
		if (style == IncDecButtons)
		{
			const double delta = (button == incButton.get()) ? interval : -interval;

			DragInProgress drag(*this);
			setValue(owner.snapValue(getValue() + delta, notDragging), sendNotificationSync);
		}
	}

	void valueChanged(Value& value) override
	{
		if (value.refersToSameSourceAs(currentValue))
		{
			if (style != TwoValueHorizontal && style != TwoValueVertical)
				setValue(currentValue.getValue(), dontSendNotification);
		}
		else if (value.refersToSameSourceAs(valueMin))
			setMinValue(valueMin.getValue(), dontSendNotification, true);
		else if (value.refersToSameSourceAs(valueMax))
			setMaxValue(valueMax.getValue(), dontSendNotification, true);
	}

	void labelTextChanged(Label* label) override
	{
		const double newValue = owner.snapValue(owner.getValueFromText(label->getText()), notDragging);

		if (newValue != (double)currentValue.getValue())
		{
			DragInProgress drag(*this);
			setValue(newValue, sendNotificationSync);
		}

		updateText(); // force a clean-up of the text, needed in case setValue() hasn't done this.
	}

	void updateText()
	{
		if (valueBox != nullptr)
		{
			String newValue(owner.getTextFromValue(currentValue.getValue()));

			if (newValue != valueBox->getText())
				valueBox->setText(newValue, dontSendNotification);
		}
	}

	double constrainedValue(double value) const
	{
		if (interval > 0)
			value = minimum + interval * std::floor((value - minimum) / interval + 0.5);

		if (value <= minimum || maximum <= minimum)
			value = minimum;
		else if (value >= maximum)
			value = maximum;

		return value;
	}

	float getLinearSliderPos(const double value) const
	{
		double pos;

		if (maximum <= minimum)
			pos = 0.5;
		else if (value < minimum)
			pos = 0.0;
		else if (value > maximum)
			pos = 1.0;
		else
			pos = owner.valueToProportionOfLength(value);

		if (isVertical() || style == IncDecButtons)
			pos = 1.0 - pos;

		jassert(pos >= 0 && pos <= 1.0);
		return (float)(sliderRegionStart + pos * sliderRegionSize);
	}

	void setSliderStyle(const SliderStyle newStyle)
	{
		if (style != newStyle)
		{
			style = newStyle;
			owner.repaint();
			owner.lookAndFeelChanged();
		}
	}

	void setRotaryParameters(const float startAngleRadians,
		const float endAngleRadians,
		const bool stopAtEnd)
	{
		// make sure the values are sensible..
		jassert(startAngleRadians >= 0 && endAngleRadians >= 0);
		jassert(startAngleRadians < float_Pi * 4.0f && endAngleRadians < float_Pi * 4.0f);
		jassert(startAngleRadians < endAngleRadians);

		rotaryStart = startAngleRadians;
		rotaryEnd = endAngleRadians;
		rotaryStop = stopAtEnd;
	}

	void setVelocityModeParameters(const double sensitivity, const int threshold,
		const double offset, const bool userCanPressKeyToSwapMode)
	{
		velocityModeSensitivity = sensitivity;
		velocityModeOffset = offset;
		velocityModeThreshold = threshold;
		userKeyOverridesVelocity = userCanPressKeyToSwapMode;
	}

	void setSkewFactorFromMidPoint(const double sliderValueToShowAtMidPoint)
	{
		if (maximum > minimum)
			skewFactor = log(0.5) / log((sliderValueToShowAtMidPoint - minimum)
				/ (maximum - minimum));
	}

	void setIncDecButtonsMode(const IncDecButtonMode mode)
	{
		if (incDecButtonMode != mode)
		{
			incDecButtonMode = mode;
			owner.lookAndFeelChanged();
		}
	}

	void setTextBoxStyle(const TextEntryBoxPosition newPosition,
		const bool isReadOnly,
		const int textEntryBoxWidth,
		const int textEntryBoxHeight)
	{
		if (textBoxPos != newPosition
			|| (ctrlCmdClickToEdit || doubleClickToEdit) != (!isReadOnly)
			|| textBoxWidth != textEntryBoxWidth
			|| textBoxHeight != textEntryBoxHeight)
		{
			textBoxPos = newPosition;
			doubleClickToEdit = !isReadOnly;
			ctrlCmdClickToEdit = false;	// the default
			textBoxWidth = textEntryBoxWidth;
			textBoxHeight = textEntryBoxHeight;

			owner.repaint();
			owner.lookAndFeelChanged();
		}
	}

	void setTextBoxIsEditable(bool shouldBeEditableByDoubleClick, bool shouldBeEditableByCtrlClick = false)
	{
		doubleClickToEdit = shouldBeEditableByDoubleClick;
		ctrlCmdClickToEdit = shouldBeEditableByCtrlClick;
		updateTextBoxEnablement();
	}

	void showTextBox()
	{
		jassert(ctrlCmdClickToEdit || doubleClickToEdit); // this should probably be avoided in read-only sliders.

		if (valueBox != nullptr)
			valueBox->showEditor();
	}

	void hideTextBox(const bool discardCurrentEditorContents)
	{
		if (valueBox != nullptr)
		{
			valueBox->hideEditor(discardCurrentEditorContents);

			if (discardCurrentEditorContents)
				updateText();
		}
	}

	void setTextValueSuffix(const String& suffix)
	{
		if (textSuffix != suffix)
		{
			textSuffix = suffix;
			updateText();
		}
	}

	void updateTextBoxEnablement()
	{
		if (valueBox != nullptr)
		{
			//const bool shouldBeEditable = editableText && owner.isEnabled();

			//if (valueBox->isEditable() != shouldBeEditable) // (to avoid changing the single/double click flags unless we need to)
				valueBox->setEditable(owner.isEnabled() && ctrlCmdClickToEdit, owner.isEnabled() && doubleClickToEdit);
		}
	}

	void lookAndFeelChanged(LookAndFeel& lf)
	{
		lf;

		if (textBoxPos != NoTextBox)
		{
			const String previousTextBoxContent(valueBox != nullptr ? valueBox->getText()
				: owner.getTextFromValue(currentValue.getValue()));

			valueBox.reset(createSliderTextBox(owner));
			owner.addAndMakeVisible(valueBox.get());

			valueBox->setWantsKeyboardFocus(false);
			valueBox->setText(previousTextBoxContent, dontSendNotification);
			valueBox->setTooltip(owner.getTooltip());
			updateTextBoxEnablement();
			valueBox->addListener(this);

			if (style == LinearBar || style == LinearBarVertical)
			{
				valueBox->addMouseListener(&owner, false);
				valueBox->setMouseCursor(MouseCursor::ParentCursor);
			}
		}
		else
		{
			valueBox = nullptr;
		}

		if (style == IncDecButtons)
		{
			incButton.reset(createSliderButton(owner, true));
			owner.addAndMakeVisible(incButton.get());
			incButton->addListener(this);

			decButton.reset(createSliderButton(owner, false));
			owner.addAndMakeVisible(decButton.get());
			decButton->addListener(this);

			if (incDecButtonMode != incDecButtonsNotDraggable)
			{
				incButton->addMouseListener(&owner, false);
				decButton->addMouseListener(&owner, false);
			}
			else
			{
				incButton->setRepeatSpeed(300, 100, 20);
				decButton->setRepeatSpeed(300, 100, 20);
			}

			const String tooltip(owner.getTooltip());
			incButton->setTooltip(tooltip);
			decButton->setTooltip(tooltip);
		}
		else
		{
			incButton = nullptr;
			decButton = nullptr;
		}

		owner.setComponentEffect(getSliderEffect(owner));

		owner.resized();
		owner.repaint();
	}

	void showPopupMenu()
	{
		PopupMenu m;
		m.setLookAndFeel(&owner.getLookAndFeel());
		m.addItem(1, TRANS("Velocity-sensitive mode"), true, isVelocityBased);
		m.addSeparator();

		if (isRotary())
		{
			PopupMenu rotaryMenu;
			rotaryMenu.addItem(2, TRANS("Use circular dragging"), true, style == Rotary);
			rotaryMenu.addItem(3, TRANS("Use left-right dragging"), true, style == RotaryHorizontalDrag);
			rotaryMenu.addItem(4, TRANS("Use up-down dragging"), true, style == RotaryVerticalDrag);
			rotaryMenu.addItem(5, TRANS("Use left-right/up-down dragging"), true, style == RotaryHorizontalVerticalDrag);

			m.addSubMenu(TRANS("Rotary mode"), rotaryMenu);
		}

		m.showMenuAsync(PopupMenu::Options(),
			ModalCallbackFunction::forComponent(sliderMenuCallback, &owner));
	}

	static void sliderMenuCallback(const int result, Slider2* slider)
	{
		if (slider != nullptr)
		{
			switch (result)
			{
			case 1:   slider->setVelocityBasedMode(!slider->getVelocityBasedMode()); break;
			case 2:   slider->setSliderStyle(Rotary); break;
			case 3:   slider->setSliderStyle(RotaryHorizontalDrag); break;
			case 4:   slider->setSliderStyle(RotaryVerticalDrag); break;
			case 5:   slider->setSliderStyle(RotaryHorizontalVerticalDrag); break;
			default:  break;
			}
		}
	}

	int getThumbIndexAt(const MouseEvent& e)
	{
		const bool isTwoValue = (style == TwoValueHorizontal || style == TwoValueVertical);
		const bool isThreeValue = (style == ThreeValueHorizontal || style == ThreeValueVertical);

		if (isTwoValue || isThreeValue)
		{
			const float mousePos = isVertical() ? e.position.y : e.position.x;

			const float normalPosDistance = std::abs(getLinearSliderPos(currentValue.getValue()) - mousePos);
			const float minPosDistance = std::abs(getLinearSliderPos(valueMin.getValue()) - 0.1f - mousePos);
			const float maxPosDistance = std::abs(getLinearSliderPos(valueMax.getValue()) + 0.1f - mousePos);

			if (isTwoValue)
				return maxPosDistance <= minPosDistance ? 2 : 1;

			if (normalPosDistance >= minPosDistance && maxPosDistance >= minPosDistance)
				return 1;

			if (normalPosDistance >= maxPosDistance)
				return 2;
		}

		return 0;
	}

	//==============================================================================
	void handleRotaryDrag(const MouseEvent& e)
	{
		const float dx = e.position.x - sliderRect.getCentreX();
		const float dy = e.position.y - sliderRect.getCentreY();

		if (dx * dx + dy * dy > 25.0f)
		{
			double angle = std::atan2((double)dx, (double)-dy);
			while (angle < 0.0)
				angle += double_Pi * 2.0;

			if (rotaryStop && !e.mouseWasClicked())
			{
				if (std::abs(angle - lastAngle) > double_Pi)
				{
					if (angle >= lastAngle)
						angle -= double_Pi * 2.0;
					else
						angle += double_Pi * 2.0;
				}

				if (angle >= lastAngle)
					angle = jmin(angle, (double)jmax(rotaryStart, rotaryEnd));
				else
					angle = jmax(angle, (double)jmin(rotaryStart, rotaryEnd));
			}
			else
			{
				while (angle < rotaryStart)
					angle += double_Pi * 2.0;

				if (angle > rotaryEnd)
				{
					if (smallestAngleBetween(angle, rotaryStart)
						<= smallestAngleBetween(angle, rotaryEnd))
						angle = rotaryStart;
					else
						angle = rotaryEnd;
				}
			}

			const double proportion = (angle - rotaryStart) / (rotaryEnd - rotaryStart);
			valueWhenLastDragged = owner.proportionOfLengthToValue(jlimit(0.0, 1.0, proportion));
			lastAngle = angle;
		}
	}

	void handleAbsoluteDrag(const MouseEvent& e)
	{
		const float mousePos = (isHorizontal() || style == RotaryHorizontalDrag) ? e.position.x : e.position.y;
		double newPos = 0;

		if (style == RotaryHorizontalDrag
			|| style == RotaryVerticalDrag
			|| style == IncDecButtons
			|| ((style == LinearHorizontal || style == LinearVertical || style == LinearBar || style == LinearBarVertical)
				&& !snapsToMousePos ))
		{
			const float mouseDiff = (style == RotaryHorizontalDrag
				|| style == LinearHorizontal
				|| style == LinearBar
				|| (style == IncDecButtons && incDecDragDirectionIsHorizontal()))
				? e.position.x - mouseDragStartPos.x
				: mouseDragStartPos.y - e.position.y;

			newPos = owner.valueToProportionOfLength(valueOnMouseDown)
				+ mouseDiff * (1.0 / pixelsForFullDragExtent);
			
			if (snapsToMousePosOnDrag)
			{
				snapsToMousePosSave = snapsToMousePos;	// save in order to restore later
				snapsToMousePos = true;
			}

			if (style == IncDecButtons)
			{
				incButton->setState(mouseDiff < 0 ? Button::buttonNormal : Button::buttonDown);
				decButton->setState(mouseDiff > 0 ? Button::buttonNormal : Button::buttonDown);
			}
		}
		else if (style == RotaryHorizontalVerticalDrag)
		{
			const float mouseDiff = (e.position.x - mouseDragStartPos.x)
				+ (mouseDragStartPos.y - e.position.y);

			newPos = owner.valueToProportionOfLength(valueOnMouseDown)
				+ mouseDiff * (1.0 / pixelsForFullDragExtent);
		}
		else
		{
			newPos = (mousePos - sliderRegionStart) / (double)sliderRegionSize;

			if (isVertical())
				newPos = 1.0 - newPos;
		}

		valueWhenLastDragged = owner.proportionOfLengthToValue(jlimit(0.0, 1.0, newPos));
	}

	void handleVelocityDrag(const MouseEvent& e)
	{
		const float mouseDiff = style == RotaryHorizontalVerticalDrag
			? (e.position.x - mousePosWhenLastDragged.x) + (mousePosWhenLastDragged.y - e.position.y)
			: (isHorizontal()
				|| style == RotaryHorizontalDrag
				|| (style == IncDecButtons && incDecDragDirectionIsHorizontal()))
			? e.position.x - mousePosWhenLastDragged.x
			: e.position.y - mousePosWhenLastDragged.y;

		const double maxSpeed = jmax(200, sliderRegionSize);
		double speed = jlimit(0.0, maxSpeed, (double)std::abs(mouseDiff));

		if (speed != 0)
		{
			speed = 0.2 * velocityModeSensitivity
				* (1.0 + std::sin(double_Pi * (1.5 + jmin(0.5, velocityModeOffset
					+ jmax(0.0, (double)(speed - velocityModeThreshold))
					/ maxSpeed))));

			if (mouseDiff < 0)
				speed = -speed;

			if (isVertical() || style == RotaryVerticalDrag
				|| (style == IncDecButtons && !incDecDragDirectionIsHorizontal()))
				speed = -speed;

			const double currentPos = owner.valueToProportionOfLength(valueWhenLastDragged);

			valueWhenLastDragged = owner.proportionOfLengthToValue(jlimit(0.0, 1.0, currentPos + speed));

			e.source.enableUnboundedMouseMovement(true, false);
		}
	}

	void mouseDown(const MouseEvent& e)
	{
		incDecDragged = false;
		useDragEvents = false;
		mouseDragStartPos = mousePosWhenLastDragged = e.position;
		currentDrag = nullptr;

		if (owner.isEnabled())
		{
			if (e.mods.isPopupMenu() && menuEnabled)
			{
				showPopupMenu();
			}
			else if (canDoubleClickToValue()
				&& e.mods.withoutMouseButtons() == ModifierKeys(ModifierKeys::altModifier))
			{
				mouseDoubleClick();
			}
			else if (maximum > minimum)
			{
				useDragEvents = true;

				if (valueBox != nullptr)
					valueBox->hideEditor(true);

				sliderBeingDragged = getThumbIndexAt(e);

				minMaxDiff = (double)valueMax.getValue() - (double)valueMin.getValue();

				lastAngle = rotaryStart + (rotaryEnd - rotaryStart)
					* owner.valueToProportionOfLength(currentValue.getValue());

				valueWhenLastDragged = (sliderBeingDragged == 2 ? valueMax
					: (sliderBeingDragged == 1 ? valueMin
						: currentValue)).getValue();
				valueOnMouseDown = valueWhenLastDragged;

				if (popupDisplayEnabled)
				{
					PopupDisplayComponent* const popup = new PopupDisplayComponent(owner);
					popupDisplay.reset(popup);

					if (parentForPopupDisplay != nullptr)
						parentForPopupDisplay->addChildComponent(popup);
					else
						popup->addToDesktop(0);

					popup->setVisible(true);
				}
				
				if (ctrlCmdClickToDefault && e.mods.isCommandDown())
				{
					setValue(defaultValue, sendNotificationSync);
				}
				if (valueBox != nullptr)
				{
					// This doesn't make the edit box visible, but it just makes it able to become visible.
					// The Label class in the mouse handler function is where it becomes visible.
					valueBox->setEditable(ctrlCmdClickToEdit && e.mods.isCommandDown(), doubleClickToEdit);
				}

				currentDrag = std::make_unique<DragInProgress>(*this);
				mouseDrag(e);
			}
		}
	}

	void mouseDrag(const MouseEvent& e)
	{
		if (useDragEvents && maximum > minimum
			&& !((style == LinearBar || style == LinearBarVertical)
				&& e.mouseWasClicked() && valueBox != nullptr && valueBox->isEditable()))
		{
			DragMode dragMode = notDragging;

			if (style == Rotary)
			{
				handleRotaryDrag(e);
			}
			else
			{
				if (style == IncDecButtons && !incDecDragged)
				{
					if (e.getDistanceFromDragStart() < 10 || e.mouseWasClicked())
						return;

					incDecDragged = true;
					mouseDragStartPos = e.position;
				}

				if (isAbsoluteDragMode(e.mods) || (maximum - minimum) / sliderRegionSize < interval)
				{
					dragMode = absoluteDrag;
					handleAbsoluteDrag(e);
				}
				else
				{
					dragMode = velocityDrag;
					handleVelocityDrag(e);
				}
			}

			valueWhenLastDragged = jlimit(minimum, maximum, valueWhenLastDragged);

			if (sliderBeingDragged == 0)
			{
				setValue(owner.snapValue(valueWhenLastDragged, dragMode),
					sendChangeOnlyOnRelease ? dontSendNotification : sendNotificationSync);
			}
			else if (sliderBeingDragged == 1)
			{
				setMinValue(owner.snapValue(valueWhenLastDragged, dragMode),
					sendChangeOnlyOnRelease ? dontSendNotification : sendNotificationAsync, true);

				if (e.mods.isShiftDown())
					setMaxValue(getMinValue() + minMaxDiff, dontSendNotification, true);
				else
					minMaxDiff = (double)valueMax.getValue() - (double)valueMin.getValue();
			}
			else if (sliderBeingDragged == 2)
			{
				setMaxValue(owner.snapValue(valueWhenLastDragged, dragMode),
					sendChangeOnlyOnRelease ? dontSendNotification : sendNotificationAsync, true);

				if (e.mods.isShiftDown())
					setMinValue(getMaxValue() - minMaxDiff, dontSendNotification, true);
				else
					minMaxDiff = (double)valueMax.getValue() - (double)valueMin.getValue();
			}

			mousePosWhenLastDragged = e.position;
		}
	}

	void mouseUp()
	{
		if (owner.isEnabled()
			&& useDragEvents
			&& (maximum > minimum)
			&& (style != IncDecButtons || incDecDragged))
		{
			restoreMouseIfHidden();

			if (sendChangeOnlyOnRelease && valueOnMouseDown != (double)currentValue.getValue())
				triggerChangeMessage(sendNotificationAsync);

			//if (canCtrlCmdClickToValue() && valueOnMouseDown == (double)currentValue.getValue())
			//{
			//	setValue(defaultValue, sendNotificationSync);
			//}

			currentDrag = nullptr;
			popupDisplay = nullptr;

			if (style == IncDecButtons)
			{
				incButton->setState(Button::buttonNormal);
				decButton->setState(Button::buttonNormal);
			}
			if (valueBox != nullptr)
			{
				updateTextBoxEnablement();
			}
			snapsToMousePos = snapsToMousePosSave;	// restore original value of snapsToMousePos

		}
		else if (popupDisplay != nullptr)
		{
			popupDisplay->startTimer(2000);
		}

		currentDrag = nullptr;
	}

	bool canDoubleClickToValue() const
	{
		return doubleClickToDefault
			&& style != IncDecButtons
			&& minimum <= defaultValue
			&& maximum >= defaultValue;
	}

	bool canCtrlCmdClickToValue() const
	{
		return ctrlCmdClickToDefault
			&& style != IncDecButtons
			&& minimum <= defaultValue
			&& maximum >= defaultValue;
	}

	void mouseDoubleClick()
	{
		if (canDoubleClickToValue())
		{
			DragInProgress drag(*this);
			setValue(defaultValue, sendNotificationSync);
		}
	}

	double getMouseWheelDelta(double value, double wheelAmount)
	{
		if (style == IncDecButtons)
			return interval * wheelAmount;

		const double proportionDelta = wheelAmount * 0.15f;
		const double currentPos = owner.valueToProportionOfLength(value);
		return owner.proportionOfLengthToValue(jlimit(0.0, 1.0, currentPos + proportionDelta)) - value;
	}

	void HideEditor()
	{
		if (valueBox != nullptr)
			valueBox->hideEditor(false);
	}


	bool mouseWheelMove(const MouseEvent& e, const MouseWheelDetails& wheel)
	{
		if (scrollWheelEnabled
			&& !e.mods.isShiftDown()
			&& style != TwoValueHorizontal
			&& style != TwoValueVertical)
		{
			// sometimes duplicate wheel events seem to be sent, so since we're going to
			// bump the value by a minimum of the interval, avoid doing this twice..
			if (e.eventTime != lastMouseWheelTime)
			{
				lastMouseWheelTime = e.eventTime;

				if (maximum > minimum && !e.mods.isAnyMouseButtonDown())
				{
					if (valueBox != nullptr)
						valueBox->hideEditor(false);

					const double value = (double)currentValue.getValue();
					 double delta = getMouseWheelDelta(value, (std::abs(wheel.deltaX) > std::abs(wheel.deltaY)
						? -wheel.deltaX : wheel.deltaY)
						* (wheel.isReversed ? -1.0f : 1.0f));

					if (e.mods.isCommandDown())
					{
						delta = (delta > 0) ? 1 : (delta < 0) ? -1 : 0;
					}
					if (delta != 0)
					{
						const double newValue = value + jmax(interval, std::abs(delta)) * (delta < 0 ? -1.0 : 1.0);

						DragInProgress drag(*this);
						setValue(owner.snapValue(newValue, notDragging), sendNotificationSync);
					}
				}
			}

			return true;
		}

		return false;
	}

	void modifierKeysChanged(const ModifierKeys& modifiers)
	{
		if (style != IncDecButtons && style != Rotary && isAbsoluteDragMode(modifiers))
			restoreMouseIfHidden();
	}

	bool isAbsoluteDragMode(ModifierKeys mods) const
	{
		return isVelocityBased == (userKeyOverridesVelocity
			&& mods.testFlags(ModifierKeys::ctrlAltCommandModifiers));
	}

	void restoreMouseIfHidden()
	{
		for (auto& ms : Desktop::getInstance().getMouseSources())
		{
			if (ms.isUnboundedMouseMovementEnabled())
			{
				ms.enableUnboundedMouseMovement(false);

				const double pos = sliderBeingDragged == 2 ? getMaxValue()
					: (sliderBeingDragged == 1 ? getMinValue()
						: (double)currentValue.getValue());
				Point<float> mousePos;

				if (isRotary())
				{
					mousePos = ms.getLastMouseDownPosition();

					const float delta = (float)(pixelsForFullDragExtent * (owner.valueToProportionOfLength(valueOnMouseDown)
						- owner.valueToProportionOfLength(pos)));

					if (style == RotaryHorizontalDrag)      mousePos += Point<float>(-delta, 0.0f);
					else if (style == RotaryVerticalDrag)   mousePos += Point<float>(0.0f, delta);
					else                                    mousePos += Point<float>(delta / -2.0f, delta / 2.0f);

					mousePos = owner.getScreenBounds().reduced(4).toFloat().getConstrainedPoint(mousePos);
					mouseDragStartPos = mousePosWhenLastDragged = owner.getLocalPoint(nullptr, mousePos);
					valueOnMouseDown = valueWhenLastDragged;
				}
				else
				{
					const float pixelPos = (float)getLinearSliderPos(pos);

					mousePos = owner.localPointToGlobal(Point<float>(isHorizontal() ? pixelPos : (owner.getWidth() / 2.0f),
						isVertical() ? pixelPos : (owner.getHeight() / 2.0f)));
				}

				const_cast <MouseInputSource&> (ms).setScreenPosition(mousePos);
			}
		}
	}

	//==============================================================================
	void paint(Graphics& g, LookAndFeel& lf)
	{
		lf;
		if (style != IncDecButtons)
		{
			if (isRotary())
			{
				const float sliderPos = (float)owner.valueToProportionOfLength(lastCurrentValue);
				jassert(sliderPos >= 0 && sliderPos <= 1.0f);

				drawRotarySlider(g,
					sliderRect.getX(), sliderRect.getY(),
					sliderRect.getWidth(), sliderRect.getHeight(),
					sliderPos, rotaryStart, rotaryEnd, owner);
			}
			else
			{
				drawLinearSlider(g,
					sliderRect.getX(), sliderRect.getY(),
					sliderRect.getWidth(), sliderRect.getHeight(),
					getLinearSliderPos(lastCurrentValue),
					getLinearSliderPos(lastValueMin),
					getLinearSliderPos(lastValueMax),
					style, owner);
			}

			if ((style == LinearBar || style == LinearBarVertical) && valueBox == nullptr)
			{
				g.setColour(owner.findColour(Slider2::textBoxOutlineColourId));
				g.drawRect(0, 0, owner.getWidth(), owner.getHeight(), 1);
			}
		}
	}

	//==============================================================================
	void resized(LookAndFeel& lf)
	{
		lf;
		SliderLayout layout = getSliderLayout(owner);

		sliderRect = layout.sliderBounds;

		if (valueBox != nullptr)
			valueBox->setBounds(layout.textBoxBounds);

		if (isHorizontal())
		{
			sliderRegionStart = layout.sliderBounds.getX();
			sliderRegionSize = layout.sliderBounds.getWidth();
		}
		else if (isVertical())
		{
			sliderRegionStart = layout.sliderBounds.getY();
			sliderRegionSize = layout.sliderBounds.getHeight();
		}
		else if (style == IncDecButtons)
		{
			resizeIncDecButtons();
		}
	}

	//==============================================================================

	void resizeIncDecButtons()
	{
		Rectangle<int> buttonRect(sliderRect);

		if (textBoxPos == TextBoxLeft || textBoxPos == TextBoxRight)
			buttonRect.expand(-2, 0);
		else
			buttonRect.expand(0, -2);

		incDecButtonsSideBySide = buttonRect.getWidth() > buttonRect.getHeight();

		if (incDecButtonsSideBySide)
		{
			decButton->setBounds(buttonRect.removeFromLeft(buttonRect.getWidth() / 2));
			decButton->setConnectedEdges(Button::ConnectedOnRight);
			incButton->setConnectedEdges(Button::ConnectedOnLeft);
		}
		else
		{
			decButton->setBounds(buttonRect.removeFromBottom(buttonRect.getHeight() / 2));
			decButton->setConnectedEdges(Button::ConnectedOnTop);
			incButton->setConnectedEdges(Button::ConnectedOnBottom);
		}

		incButton->setBounds(buttonRect);
	}

	//==============================================================================
	void ReverseMode(bool reverseMode_)
	{
		reverseMode = reverseMode_;
	}

	//==============================================================================
	Slider2& owner;
	SliderStyle style;

	ListenerList <Slider2Listener> listeners;
	Value currentValue, valueMin, valueMax;
	double lastCurrentValue, lastValueMin, lastValueMax;
	double minimum, maximum, interval, defaultValue;
	double valueWhenLastDragged, valueOnMouseDown, skewFactor, lastAngle;
	double velocityModeSensitivity, velocityModeOffset, minMaxDiff;
	int velocityModeThreshold;
	float rotaryStart, rotaryEnd;
	Point<float> mouseDragStartPos, mousePosWhenLastDragged;
	int sliderRegionStart, sliderRegionSize;
	int sliderBeingDragged;
	int pixelsForFullDragExtent;
	Time lastMouseWheelTime;
	Rectangle<int> sliderRect;
	std::unique_ptr<DragInProgress> currentDrag;

	TextEntryBoxPosition textBoxPos;
	String textSuffix;
	int numDecimalPlaces;
	int textBoxWidth, textBoxHeight;
	IncDecButtonMode incDecButtonMode;

	bool doubleClickToDefault;
	bool doubleClickToEdit;
	bool ctrlCmdClickToDefault;
	bool ctrlCmdClickToEdit;
	bool isVelocityBased;
	bool userKeyOverridesVelocity;
	bool rotaryStop;
	bool incDecButtonsSideBySide;
	bool sendChangeOnlyOnRelease;
	bool popupDisplayEnabled;
	bool menuEnabled;
	bool useDragEvents;
	bool incDecDragged;
	bool scrollWheelEnabled;
	bool snapsToMousePos;
	bool snapsToMousePosSave;
	bool snapsToMousePosOnDrag;
	bool reverseMode;

	std::unique_ptr<Label> valueBox;
	std::unique_ptr<Button> incButton, decButton;

	//==============================================================================
	class PopupDisplayComponent : public BubbleComponent,
		public Timer
	{
	public:
		PopupDisplayComponent(Slider2& s)
			: owner(s),
			font(getSliderPopupFont(s))
		{
			setAlwaysOnTop(true);
			setAllowedPlacement(getSliderPopupPlacement(s));
			setLookAndFeel(&s.getLookAndFeel());
		}

		void paintContent(Graphics& g, int w, int h) override
		{
			g.setFont(font);
			g.setColour(owner.findColour(TooltipWindow::textColourId, true));
			g.drawFittedText(text, Rectangle<int>(w, h), Justification::centred, 1);
		}

		void getContentSize(int& w, int& h) override
		{
			w = font.getStringWidth(text) + 18;
			h = (int)(font.getHeight() * 1.6f);
		}

		void updatePosition(const String& newText)
		{
			text = newText;
			BubbleComponent::setPosition(&owner);
			repaint();
		}

		void timerCallback() override
		{
			owner.pimpl->popupDisplay = nullptr;
		}

	private:
		Slider2& owner;
		Font font;
		String text;

		JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(PopupDisplayComponent)
	};

	std::unique_ptr<PopupDisplayComponent> popupDisplay;
	Component* parentForPopupDisplay;

	//==============================================================================
	static double smallestAngleBetween(const double a1, const double a2) noexcept
	{
		return jmin(std::abs(a1 - a2),
			std::abs(a1 + double_Pi * 2.0 - a2),
			std::abs(a2 + double_Pi * 2.0 - a1));
	}
};


//==============================================================================
Slider2::Slider2()
{
	init(LinearHorizontal, TextBoxLeft);
}

Slider2::Slider2(const String& name) : Component(name)
{
	init(LinearHorizontal, TextBoxLeft);
}

Slider2::Slider2(SliderStyle style, TextEntryBoxPosition textBoxPos)
{
	init(style, textBoxPos);
}

void Slider2::init(SliderStyle style, TextEntryBoxPosition textBoxPos)
{
	setWantsKeyboardFocus(false);
	setRepaintsOnMouseActivity(true);

	pimpl = std::make_unique<Pimpl>(*this, style, textBoxPos);

	Slider2::lookAndFeelChanged();
	updateText();

	if (getWidth() > 0)
	{
		setMouseDragSensitivity(getWidth());
	}

	pimpl->registerListeners();
}

Slider2::~Slider2() {}

//==============================================================================
void Slider2::addListener(Slider2Listener* const listener) { pimpl->listeners.add(listener); }
void Slider2::removeListener(Slider2Listener* const listener) { pimpl->listeners.remove(listener); }

//==============================================================================
Slider2::SliderStyle Slider2::getSliderStyle() const noexcept { return pimpl->style; }
void Slider2::setSliderStyle(const SliderStyle newStyle) { pimpl->setSliderStyle(newStyle); }

void Slider2::setRotaryParameters(const float startAngleRadians, const float endAngleRadians, const bool stopAtEnd)
{
	pimpl->setRotaryParameters(startAngleRadians, endAngleRadians, stopAtEnd);
}

void Slider2::setVelocityBasedMode(bool vb) { pimpl->isVelocityBased = vb; }
bool Slider2::getVelocityBasedMode() const noexcept { return pimpl->isVelocityBased; }
bool Slider2::getVelocityModeIsSwappable() const noexcept { return pimpl->userKeyOverridesVelocity; }
int Slider2::getVelocityThreshold() const noexcept { return pimpl->velocityModeThreshold; }
double Slider2::getVelocitySensitivity() const noexcept { return pimpl->velocityModeSensitivity; }
double Slider2::getVelocityOffset() const noexcept { return pimpl->velocityModeOffset; }

void Slider2::setVelocityModeParameters(const double sensitivity, const int threshold,
	const double offset, const bool userCanPressKeyToSwapMode)
{
	jassert(threshold >= 0);
	jassert(sensitivity > 0);
	jassert(offset >= 0);

	pimpl->setVelocityModeParameters(sensitivity, threshold, offset, userCanPressKeyToSwapMode);
}

double Slider2::getSkewFactor() const noexcept { return pimpl->skewFactor; }
void Slider2::setSkewFactor(const double factor) { pimpl->skewFactor = factor; }

void Slider2::setSkewFactorFromMidPoint(const double sliderValueToShowAtMidPoint)
{
	pimpl->setSkewFactorFromMidPoint(sliderValueToShowAtMidPoint);
}

int Slider2::getMouseDragSensitivity() const noexcept { return pimpl->pixelsForFullDragExtent; }

void Slider2::setMouseDragSensitivity(const int distanceForFullScaleDrag)
{
	jassert(distanceForFullScaleDrag > 0);

	pimpl->pixelsForFullDragExtent = distanceForFullScaleDrag;
}

void Slider2::setIncDecButtonsMode(const IncDecButtonMode mode) { pimpl->setIncDecButtonsMode(mode); }

Slider2::TextEntryBoxPosition Slider2::getTextBoxPosition() const noexcept { return pimpl->textBoxPos; }
int Slider2::getTextBoxWidth() const noexcept { return pimpl->textBoxWidth; }
int Slider2::getTextBoxHeight() const noexcept { return pimpl->textBoxHeight; }

void Slider2::setTextBoxStyle(const TextEntryBoxPosition newPosition, const bool isReadOnly,
	const int textEntryBoxWidth, const int textEntryBoxHeight)
{
	pimpl->setTextBoxStyle(newPosition, isReadOnly, textEntryBoxWidth, textEntryBoxHeight);
}

bool Slider2::isTextBoxEditable() const noexcept { return pimpl->ctrlCmdClickToEdit || pimpl->doubleClickToEdit; }
void Slider2::setTextBoxIsEditable(bool shouldBeEditableByDoubleClick, bool shouldBeEditableByCtrlClick) { pimpl->setTextBoxIsEditable(shouldBeEditableByDoubleClick, shouldBeEditableByCtrlClick ); }
void Slider2::showTextBox() { pimpl->showTextBox(); }
void Slider2::hideTextBox(const bool discardCurrentEditorContents) { pimpl->hideTextBox(discardCurrentEditorContents); }

void Slider2::setChangeNotificationOnlyOnRelease(bool onlyNotifyOnRelease)
{
	pimpl->sendChangeOnlyOnRelease = onlyNotifyOnRelease;
}

bool Slider2::getSliderSnapsToMousePosition() const noexcept { return pimpl->snapsToMousePos; }
void Slider2::setSliderSnapsToMousePosition(const bool shouldSnapToMouse) { pimpl->snapsToMousePos = shouldSnapToMouse; }

void Slider2::setPopupDisplayEnabled(const bool enabled, Component* const parentComponentToUse)
{
	pimpl->popupDisplayEnabled = enabled;
	pimpl->parentForPopupDisplay = parentComponentToUse;
}

Component* Slider2::getCurrentPopupDisplay() const noexcept { return pimpl->popupDisplay.get(); }

//==============================================================================
void Slider2::colourChanged() { lookAndFeelChanged(); }
void Slider2::lookAndFeelChanged() { pimpl->lookAndFeelChanged(getLookAndFeel()); }
void Slider2::enablementChanged() { repaint(); pimpl->updateTextBoxEnablement(); }

//==============================================================================
double Slider2::getMaximum() const noexcept { return pimpl->maximum; }
double Slider2::getMinimum() const noexcept { return pimpl->minimum; }
double Slider2::getInterval() const noexcept { return pimpl->interval; }

void Slider2::setRange(double newMin, double newMax, double newInt)
{
	pimpl->setRange(newMin, newMax, newInt);
}

Value& Slider2::getValueObject() noexcept { return pimpl->currentValue; }
Value& Slider2::getMinValueObject() noexcept { return pimpl->valueMin; }
Value& Slider2::getMaxValueObject() noexcept { return pimpl->valueMax; }

double Slider2::getValue() const { return pimpl->getValue(); }

void Slider2::setValue(double newValue, const NotificationType notification)
{
	pimpl->setValue(newValue, notification);
}

double Slider2::getMinValue() const { return pimpl->getMinValue(); }
double Slider2::getMaxValue() const { return pimpl->getMaxValue(); }

void Slider2::setMinValue(double newValue, const NotificationType notification, bool allowNudgingOfOtherValues)
{
	pimpl->setMinValue(newValue, notification, allowNudgingOfOtherValues);
}

void Slider2::setMaxValue(double newValue, const NotificationType notification, bool allowNudgingOfOtherValues)
{
	pimpl->setMaxValue(newValue, notification, allowNudgingOfOtherValues);
}

void Slider2::setMinAndMaxValues(double newMinValue, double newMaxValue, const NotificationType notification)
{
	pimpl->setMinAndMaxValues(newMinValue, newMaxValue, notification);
}

void Slider2::setToDefaultOnDoubleClick(bool enableDoubleClick)
{
	pimpl->doubleClickToDefault = enableDoubleClick;
}

void Slider2::setToDefaultOnCtrlCmdClick(bool enableCtrlCmdClick)
{
	pimpl->ctrlCmdClickToDefault = enableCtrlCmdClick;
}

void Slider2::setDefaultValue(double defaultValue)
{
	pimpl->defaultValue = defaultValue;
}


double Slider2::getDefaultValue() const noexcept { return pimpl->defaultValue; }
bool Slider2::isDoubleClickToDefaultEnabled() const noexcept { return pimpl->doubleClickToDefault; }

void Slider2::setReverseMode(bool reverseMode) noexcept { pimpl->ReverseMode(reverseMode); }

void Slider2::updateText()
{
	pimpl->updateText();
}

void Slider2::setTextValueSuffix(const String& suffix)
{
	pimpl->setTextValueSuffix(suffix);
}

String Slider2::getTextValueSuffix() const
{
	return pimpl->textSuffix;
}

String Slider2::getTextFromValue(double v)
{
	if (getNumDecimalPlacesToDisplay() > 0)
		return String(v, getNumDecimalPlacesToDisplay()) + getTextValueSuffix();

	return String(roundToInt(v)) + getTextValueSuffix();
}

double Slider2::getValueFromText(const String& text)
{
	String t(text.trimStart());

	if (t.endsWith(getTextValueSuffix()))
		t = t.substring(0, t.length() - getTextValueSuffix().length());

	while (t.startsWithChar('+'))
		t = t.substring(1).trimStart();

	return t.initialSectionContainingOnly("0123456789.,-")
		.getDoubleValue();
}

double Slider2::proportionOfLengthToValue(double proportion)
{
	const double skew = getSkewFactor();

	if (skew != 1.0 && proportion > 0.0)
		proportion = exp(log(proportion) / skew);

	return getMinimum() + (getMaximum() - getMinimum()) * proportion;
}

double Slider2::valueToProportionOfLength(double value)
{
	const double n = (value - getMinimum()) / (getMaximum() - getMinimum());
	const double skew = getSkewFactor();

	return skew == 1.0 ? n : pow(n, skew);
}

double Slider2::snapValue(double attemptedValue, DragMode)
{
	return attemptedValue;
}

int Slider2::getNumDecimalPlacesToDisplay() const noexcept { return pimpl->numDecimalPlaces; }

//==============================================================================
int Slider2::getThumbBeingDragged() const noexcept { return pimpl->sliderBeingDragged; }

void Slider2::startedDragging() {}
void Slider2::stoppedDragging() {}
void Slider2::valueChanged() {}

//==============================================================================
void Slider2::setPopupMenuEnabled(const bool menuEnabled) { pimpl->menuEnabled = menuEnabled; }
void Slider2::setScrollWheelEnabled(const bool enabled) { pimpl->scrollWheelEnabled = enabled; }

bool Slider2::isHorizontal() const noexcept { return pimpl->isHorizontal(); }
bool Slider2::isVertical() const noexcept { return pimpl->isVertical(); }
bool Slider2::isRotary() const noexcept { return pimpl->isRotary(); }
bool Slider2::isBar() const noexcept { return pimpl->isBar(); }

float Slider2::getPositionOfValue(const double value) const { return pimpl->getPositionOfValue(value); }

//==============================================================================
void Slider2::paint(Graphics& g) { pimpl->paint(g, getLookAndFeel()); }
void Slider2::resized() { if(getWidth() > 0) setMouseDragSensitivity(getWidth()); pimpl->resized(getLookAndFeel()); }

void Slider2::focusOfChildComponentChanged(FocusChangeType) { repaint(); }

void Slider2::mouseDown(const MouseEvent& e) { pimpl->mouseDown(e); }
void Slider2::mouseUp(const MouseEvent&) { pimpl->mouseUp(); }

void Slider2::modifierKeysChanged(const ModifierKeys& modifiers)
{
	if (isEnabled())
		pimpl->modifierKeysChanged(modifiers);
}

void Slider2::mouseDrag(const MouseEvent& e)
{
	if (isEnabled())
		pimpl->mouseDrag(e);
}

void Slider2::mouseDoubleClick(const MouseEvent&)
{
	if (isEnabled())
		pimpl->mouseDoubleClick();
}

void Slider2::mouseWheelMove(const MouseEvent& e, const MouseWheelDetails& wheel)
{
	if (!(isEnabled() && pimpl->mouseWheelMove(e, wheel)))
		Component::mouseWheelMove(e, wheel);
}
void Slider2::HideEditor()
{
	if (isEnabled())
		pimpl->HideEditor();
}

//===========================================================================================
//
//
//
//===========================================================================================
#include "Properties.h"

SliderProp::SliderProp()
{
	if (pPropertiesFile)
	{
		Update();

	// Subscribe to the properties file
		pPropertiesFile->addChangeListener(this);	// listen for change to the midi devices or channel
	}
}

SliderProp::~SliderProp()
{
	if (pPropertiesFile)
	{
		pPropertiesFile->removeChangeListener(this);
	}
}

void SliderProp::Update()
{
	int sliderReset = pPropertiesFile->getIntValue(propSliderReset);
	int sliderEdit = pPropertiesFile->getIntValue(propSliderEdit);
	setTextBoxIsEditable(sliderEdit == 2, sliderEdit == 1);
	setToDefaultOnCtrlCmdClick(sliderReset == 1);
	setToDefaultOnDoubleClick(sliderReset == 2);
}

void SliderProp::changeListenerCallback(ChangeBroadcaster*)
{
	if (pPropertiesFile)
	{
		Update();
	}
}
