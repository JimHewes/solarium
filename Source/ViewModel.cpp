/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <vector>
#include <list>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <cassert>
#include "gsl_util.h"
#include "ViewModel.h"


using std::vector;
using std::list;
using std::string;

static vector<InvItemView> InvItemsView
{
	{ "Aftertouch",ElementType::Aftertouch,1,1 },
	{ "AM",ElementType::AM,2,1 },
	{ "Assignable Button",ElementType::AssignButton,2,1 },
	{ "AT + ModWheel",ElementType::AT_ModWheel,1,1 },
	{ "Breath",ElementType::Breath,1,1 },
	{ "CC",ElementType::CC,5,1 },
	{ "EG",ElementType::EG,6,1 },
	{ "Env Follower",ElementType::EnvFol,1,1 },
	{ "Ext Input",ElementType::ExtInput,6,1 },
	{ "Filter",ElementType::Filter,4,1 },
	{ "FX Channel",ElementType::FXChannel,4,1 },
	{ "Gate",ElementType::Gate,1, 1 },
	{ "Insert FX",ElementType::InsertFX,4,1 },
	{ "Joystick X",ElementType::JoyX,1,1 },
	{ "Joystick Y",ElementType::JoyY,1,1 },
	{ "Key Table",ElementType::KeyTable,4,1 },
	{ "Key Tracking",ElementType::KeyTracking,1,1 },
	{ "Lag",ElementType::Lag,4,1 },
	{ "LFO",ElementType::LFO,4,1 },
	{ "Loop EG",ElementType::LoopEG,1,2 },
	{ "Max Value",ElementType::MaxValue,1,1 },
	{ "Mixer",ElementType::Mixer,4,1 },
	{ "Mod Wheel",ElementType::ModWheel,1,1 },
	{ "Note",ElementType::Note,1,1 },
	{ "Oscillator",ElementType::Oscillator,4,1 },
	{ "Output",ElementType::Output,4, 0 },
	{ "Pedal",ElementType::Pedal,2,1 },
	{ "Pink Noise",ElementType::PinkNoise,1,1 },
	{ "Poly AT",ElementType::PolyAT,1,1 },
	{ "Ribbon",ElementType::Ribbon,2,1 },
	{ "Rotor",ElementType::Rotor,2,1 },
	{ "Seq",ElementType::Seq,4,1 },
	{ "S/PDIF Out",ElementType::SPDIF_Out,1,0 },
	{ "S/PDIF In",ElementType::SPDIF_In,3,1 },
	{ "VCA",ElementType::VCA,4,1 },
	{ "VCA Master",ElementType::VCAMaster,1,1 },
	{ "Vector",ElementType::Vector,2,1 },
	{ "Velocity",ElementType::Velocity,1,1 },
	{ "Vibrato LFO",ElementType::VibratoLFO,1,1 },
	{ "White Noise",ElementType::WhiteNoise,1,1 },
	{ "Home",ElementType::Home,1,0 },
};


inline bool operator==(const ElementHistoryItem& lhs, const ElementHistoryItem& rhs)
{
	return lhs.state == rhs.state && lhs.location.x == rhs.location.x && lhs.location.y == rhs.location.y;
}
inline bool operator!=(const ElementHistoryItem& lhs, const ElementHistoryItem& rhs)
{
	return !(lhs == rhs);
}


/** @brief Builds a master list of all element items that is used to create a popup menu for adding elements.

	This function is called once at the start of the program to build a master list of all element items.
	Then when a popup menu is needed for adding more elements, it builds it using the elements from this
	list that are not currently visible.

	@returns	A list of ElemItem. An ElemItem represents (and contains the state for) an element that is displayed in the UI.
*/
list<ElemItem> GenerateElemItemList()
{
	static const char* letters[4] = { "A","B","C","D" };
	static const char* numbers[7] = { "1","2","3","4","5","6" };
	static const char* output[4] = { "1/2","3/4","5/6","7/8" };
	static const char* spdifIn[3] = { "L","R", "L&R" };
	static const char* extIn[6] = { "1","2","3","4","1/2","3/4" };

	list<ElemItem> elemItemList;

	for (auto& invItem : InvItemsView)
	{
		ElemItem elemItem;
		elemItem.visible = false;
		elemItem.added = false;
		elemItem.elemID.elemType = invItem.elemType;

		for (int i = 0; i < invItem.count; ++i)
		{
			if (invItem.count > 1)
			{
				elemItem.groupFriendlyname = invItem.elemTypeFriendlyname;
				elemItem.itemFriendlyname = (invItem.elemType == ElementType::Seq) ? letters[i] :
					(invItem.elemType == ElementType::SPDIF_In) ? spdifIn[i] :
					(invItem.elemType == ElementType::ExtInput) ? extIn[i] :
					(invItem.elemType == ElementType::Output) ? output[i] :
					numbers[i];
			}
			else
			{
				elemItem.groupFriendlyname = "";
				elemItem.itemFriendlyname = invItem.elemTypeFriendlyname;
			}

			elemItem.elemID.elemIndex = gsl::narrow<uint8_t>(i);
			elemItemList.push_back(elemItem);
		}
	}
	return elemItemList;
}

ViewModel::ViewModel(Preset * pPreset, CommandHistory* pCommandHistory)
	:m_pPreset(pPreset)
	,m_focusedSlots(4)
{
	Initialize();

	m_branchAddedConnection = pCommandHistory->BranchAddedEvent.Connect([this](BranchTag newBranchTag, Version parentVersion)
	{
		m_elementHistory.AddBranch(newBranchTag, parentVersion);
	});

	m_branchDeletedConnection = pCommandHistory->BranchDeletedEvent.Connect([this](BranchTag branchTag, Revision maxRevision)
	{
		m_elementHistory.DeleteBranch(branchTag, maxRevision);
	});

	m_goToVersionConnection = pCommandHistory->GoToVersionEvent.Connect([this](Version version)
	{
		for (auto& elemItem : m_elemItemList)
		{
			ElementHistoryItem elemHistory = m_elementHistory.GetItemAtVersion( version, elemItem.elemID);
			elemItem.added = elemHistory.GetAdded();
			elemItem.ephemeral = elemHistory.GetEphemeral();
		}
		// It seems like we should call UpdateEnabled() here, but we expect to also get a notification for 
		// MultipleParametersChangedEvent so that will take care of it. (Maybe a better combination of events 
		// from the Preset could be devised but I haven't thought of it yet.)
		UpdateVisible();
	});

	m_enabledchangedConnection = m_pPreset->SubscribeToEnabledChangedEvent([this](Version version)
	{
		// when a parameter changes that is a source, it signals an EnabledChangedEvent.
		UpdateEnabled();
		UpdateEphemeral(version);
		UpdateVisible();
	});

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version version, bool clear) 
	{
		if (clear)
		{
			ClearAddedAndEphemeralElements(version);
		}
		UpdateEnabled();
		UpdateVisible();
	});

	m_parameterChangedConnection = m_pPreset->SubscribeToParameterChangedEvent([this](const ParamID& paramID, int value, bool isMuted)
	{
		value; isMuted;

		// When source parameters change (connectors are added or removed) we just want to update what's visible.
		if (paramID.portType != PortType::None && paramID.subIndex == psiSource)
		{
			UpdateVisible();
		}
	});

	m_removeVersionConnection = pCommandHistory->RemoveVersionEvent.Connect([this](Version version)
	{
		m_elementHistory.RemoveVersion(version);
	});
	UpdateEnabled();
	UpdateVisible();
}

void ViewModel::Initialize()
{
	m_elemItemList = GenerateElemItemList();
	ConnectorSelectionModel().clear();
	ElementViewSelectionModel().clear();
	
	for (auto& elemItem : m_elemItemList)
	{
		m_elementHistory.AddItem(Version(0,0), elemItem.elemID, ElementHistoryItem(false, false));
	}
}


/** "Adds" an Element to the model, which really just means the element is set to the "visible" state.

	Generally, visible elements are displayed by the UI whereas inactive elements are not.

	@param[in] elemID		The ID of the element to make visible. The element must not already be visible.
	@param[in] location		The x/y location in the UI for the added element. the model doesn't care about
							this value. It's really just a convenient storage space for the UI to use.

	@throws std::runtime_error	if the element is already visible.
	@see IsElementVisible
*/
void ViewModel::AddElement(ElementID elemID, Location location, Version version)
{
	ElemItem* pElemItem = GetElement(elemID);

	// confirm that this node isn't already visible
	if (pElemItem->visible)
	{
		throw std::runtime_error("Tried to add a node that is already visible: " + pElemItem->itemFriendlyname);
	}

	pElemItem->location = location;
	pElemItem->added = true;
	pElemItem->visible = true;
	m_elementHistory.AddItem(version, elemID, ElementHistoryItem(true, false, location));
	UpdateEphemeral(version);	// Update ephemeral here because the element added may have more elements input to it.
	UpdateVisible();
}

/** @brief	Removes an Element.

	When an element is removed, all its output connection are also removed. So this may result in parameters being changed.

	@elemID		The ID of the element to remove.
	@returns	A list of the connectors that were attached to the output port of the element and which were also removed.
*/
void ViewModel::RemoveElement(ElementID elementToRemove, Version version)
{
	auto connectorList = m_pPreset->GetOutputConnectorsOfElement(elementToRemove);
	for (auto& conn : connectorList)
	{
		m_pPreset->RemoveConnector(conn, version);
	}

	ElemItem* pElem = GetElement(elementToRemove);
	if (pElem)
	{
		pElem->visible = pElem->added = pElem->ephemeral = pElem->enabled = false;
		m_elementHistory.AddItem(version, pElem->elemID, ElementHistoryItem(false, false, pElem->location));
		VisibleElementsChangedEvent.Notify();
	}
	else
	{
		assert(false);
	}
}

/** @brief	This function exists to support the case of a connector to an element which 
			cannot be removed or muted because there is no OFF setting in the element's input.

	The connector cannot be removed if the element is visible and the input has no OFF setting.

	@param[in] connector	The connector to be removed.
	@returns				True if it can be removed or false if not.
*/
bool ViewModel::CanBeRemoved(Connector connector)
{
	auto checkPort = [this](PortID& portID) -> bool
	{
		bool result = true;
		if (
			((portID.portType == PortType::Input) &&
				(portID.elemID.elemType == ElementType::VCA || portID.elemID.elemType == ElementType::InsertFX || portID.elemID.elemType == ElementType::VCAMaster)) ||
			((portID.portType == PortType::Mod) &&
				(portID.elemID.elemType == ElementType::VCAMaster))
			)
		{
			ElemItem* pElem = GetElement(portID.elemID);
			assert(pElem);
			if (pElem->visible)
			{
				result = false;
			}
		}
		return result;
	};
	
	return checkPort(connector.endPort) && checkPort(connector.startPort);
}

/** @brief	This function exists to support special cases where an element should not be removed  
			because it's necessarily a part of a preset or because it's output connections cannot all be removed.
			
	Examples are Filter, InsertFX, VCAMaster, and EG6.

	@param[in] elemID	The ID of the element to be removed.
	@returns			True if it can be removed or false if not.
*/
bool ViewModel::CanBeRemoved(const ElementID & elemID)
{
	bool result = true;
	// These two are always there and cannot be removed.
	if (elemID.elemType == ElementType::VCAMaster || (elemID.elemType == ElementType::EG && elemID.elemIndex == 5))
	{
		result = false;
	}
	else
	{
		auto connList = m_pPreset->GetOutputConnectorsOfElement(elemID);
		// Look for an output connector that cannot be removed.
		if (connList.end() != std::find_if(connList.begin(), connList.end(), [this](auto& conn) { return !CanBeRemoved(conn);}))
		{
			result = false;
		}
	}

	return result;
}


/** @brief Builds a list of all elements items that are not yet visible (displayed) so it can be used to create a popup menu for adding elements.

	The list has a two-level hierarchy, intended for a two-level menu. If MenuItem::groupFriendlyname string is
	not empty, then it is a first-level menu item. If it is empty then it's a second level item meant for a submenu
	of the most recent group. So that means the list is in a specific order.

	@returns a list of MenuItems representing all elements that are not currently visible.
*/
list<MenuItem> ViewModel::GenerateMenuItemList()
{
	list<MenuItem> list;
	MenuItem mi;

	for (auto& e : m_elemItemList)
	{
		if (!e.visible)
		{
			mi.groupFriendlyname = e.groupFriendlyname;
			mi.itemFriendlyname = e.itemFriendlyname;
			mi.elemID = e.elemID;
			list.push_back(mi);
		}
	}

	return list;
}

bool ViewModel::IsElementVisible(ElementID elemID)
{
	ElemItem* pElem = GetElement(elemID);
	assert(pElem);
	return pElem->visible;
}

std::vector<ElementID> ViewModel::GetVisibleElementList() const
{
	std::vector<ElementID> result;
	for (auto& elemItem : m_elemItemList)
	{
		if (elemItem.visible)
		{
			result.push_back(elemItem.elemID);
		}
	}
	return result;
}

void ViewModel::ClearVisibleElements()
{
	m_elementViewSelectionModel.clear();
	m_connectorSelectionModel.clear();
	for (auto& elemItem : m_elemItemList)
	{
		elemItem.visible = false;
	}
}

void ViewModel::ClearAddedAndEphemeralElements(Version version)
{
	for (auto& elemItem : m_elemItemList)
	{
		elemItem.added = elemItem.ephemeral = false;
		m_elementHistory.AddItem(version, elemItem.elemID, ElementHistoryItem(false, false, elemItem.location));
	}
}

void ViewModel::UpdateEphemeral(Version version)
{
	// A container used to mark the ephemeral state of all elements
	std::unordered_map<ElementID, bool> ephemeralState;

	// first clear all ephemeral state
	for (auto& elemItem : m_elemItemList)
	{
		ephemeralState[elemItem.elemID] = false;
	}

	for (auto& elemItem : m_elemItemList)
	{
		if (elemItem.added)
		{
			vector<ElementID> list = m_pPreset->GetElementTree(elemItem.elemID);
			for (auto elemID : list)
			{
				// For each elementID in the tree, find it in the master elementItem list and set the ephemeral flag.
				auto iter = std::find_if(m_elemItemList.begin(), m_elemItemList.end(), [elemID](ElemItem& ei) { return ei.elemID == elemID;});
				if (iter != m_elemItemList.end())
				{
				//	iter->ephemeral = true;
					ephemeralState.at(iter->elemID) = true;	// mark for use below
				}
			};
		}
	}
	
	// Now if the new ephemeral state is different, change it in the master element item list and also save it to undo history for that element.
	for (auto& elemItem : m_elemItemList)
	{
		bool newState = ephemeralState.at(elemItem.elemID);
		if (elemItem.ephemeral != newState)
		{
			elemItem.ephemeral = newState;
			m_elementHistory.AddItem(version, elemItem.elemID, ElementHistoryItem(elemItem.added, newState, elemItem.location));
		}
	}
}

void ViewModel::UpdateEnabled()
{
	vector<ElementID> list = m_pPreset->GetElementTree();
	for(auto& elemItem: m_elemItemList)
	{
		auto iter = std::find_if(list.begin(), list.end(), [&elemItem](ElementID& elemID) { return elemID == elemItem.elemID;});
		elemItem.enabled = (iter != list.end());

		// Once elements have been in an enabled path, they are no longer considered "added" so that 
		// they are now rendered invisible when the part is disabled.
		if (elemItem.enabled)
		{
			elemItem.added = false;
		}
	}
}

void ViewModel::UpdateVisible()
{
	for (auto& elemItem : m_elemItemList)
	{
		elemItem.visible = elemItem.enabled || elemItem.added || elemItem.ephemeral;
		// Only deselect items that aren't visible
		if (!elemItem.visible)
		{
			auto iter = std::find_if(m_elementViewSelectionModel.begin(), m_elementViewSelectionModel.end(),
				[&elemItem](const ElementID& elemID)	{	return elemID == elemItem.elemID; });

			if (iter != m_elementViewSelectionModel.end())
			{
				m_elementViewSelectionModel.DeSelect((*iter));
			}
		}
	}
	VisibleElementsChangedEvent.Notify();
}

std::string ViewModel::GetElementFriendlyName(const ElementID& elemID)
{
	ElemItem* pElemItem = GetElement(elemID);
	if (pElemItem->groupFriendlyname.empty())
	{
		return pElemItem->itemFriendlyname;
	}
	else
	{
		return pElemItem->groupFriendlyname + " " + pElemItem->itemFriendlyname;
	}
}


ElemItem* ViewModel::GetElement(ElementID elemID)
{
	auto iter = find_if(m_elemItemList.begin(), m_elemItemList.end(), [&elemID](const ElemItem& item) {return elemID == item.elemID; });
	assert(iter != m_elemItemList.end());
	return &(*iter);
}


void ViewModel::SetLocation(ElementID elemID, Location location)
{
	GetElement(elemID)->location = location;
}

Location ViewModel::GetLocation(ElementID elemID)
{
	return GetElement(elemID)->location;
}

void ViewModel::SetFocusedSlot(int channelIndex, int slotIndex)
{
	assert(channelIndex >= 0 && channelIndex < 4);
	assert(slotIndex >= 0 && slotIndex < 4);
	m_focusedSlots[channelIndex] = slotIndex;
	FxFocusedSlotChangedEvent.Notify(channelIndex, slotIndex);
}

int ViewModel::GetFocusedSlot(int channelIndex)
{
	assert(channelIndex >= 0 && channelIndex < 4);
	return m_focusedSlots[channelIndex];
}


void ViewModel::Write(std::ostream& output)
{
	auto elementIDWriter = [](std::ostream& output, ElementID elemID)
	{
		output.write(reinterpret_cast<const char *>(&elemID.elemType), sizeof(elemID.elemType));
		output.write(reinterpret_cast<const char *>(&elemID.elemIndex), sizeof(elemID.elemIndex));
	};
	auto elementHistoryItemWriter = [](std::ostream& output, ElementHistoryItem item)
	{
		output.write(reinterpret_cast<const char*>(&item.location.x), sizeof(item.location.x));
		output.write(reinterpret_cast<const char*>(&item.location.y), sizeof(item.location.y));
		output.write(reinterpret_cast<const char*>(&item.state), sizeof(item.state));
	};
	m_elementHistory.Write(output, elementIDWriter, elementHistoryItemWriter);

}
void ViewModel::Read(std::istream& input)
{
	auto elementIDReader = [](std::istream& input, ElementID& elemID)
	{
		input.read(reinterpret_cast<char *>(&elemID.elemType), sizeof(elemID.elemType));
		input.read(reinterpret_cast<char *>(&elemID.elemIndex), sizeof(elemID.elemIndex));
	};
	auto elementHistoryItemReader = [](std::istream& input, ElementHistoryItem& item)
	{
		input.read(reinterpret_cast<char*>(&item.location.x), sizeof(item.location.x));
		input.read(reinterpret_cast<char*>(&item.location.y), sizeof(item.location.y));
		input.read(reinterpret_cast<char*>(&item.state), sizeof(item.state));
	};
	m_elementHistory.Read(input, elementIDReader, elementHistoryItemReader);
}