/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MIDIMANAGER_H_INCLUDED
#define MIDIMANAGER_H_INCLUDED

#include <deque>
#include <memory>
#include "../JuceLibraryCode/JuceHeader.h"
#include "ParameterList.h"	// needed for definition of ParamAddress

class Preset;
class MidiManager;
class CommandHistory;

struct SolarisParam
{
	ParamAddress paramAddress;
	int32_t value;
};


class SendThread : public Thread
{
public:
	SendThread(MidiManager* pMidiManager, std::vector<std::vector<uint8_t>>&& presetBlocksToSend);
	void run() override;
private:
	MidiManager* m_pMidiManager;
	std::vector<std::vector<uint8_t>> m_presetBlocksToSend;
};

using SendThreadUPtr = std::unique_ptr<SendThread>;
using MidiProgressCallback = std::function<bool(double progress, bool isDone)>;
struct BlockInfo;

class MidiManager :	public MidiInputCallback,
					public ChangeListener

{
public:
	MidiManager(Preset* pPreset, CommandHistory* pCommandHistory);
	~MidiManager();

	void EnableInput(bool enable = true);
	void setSync(bool syncOn);

	void DoSendPreset(MidiProgressCallback progressCallback);
	void DoReceivePreset(MidiProgressCallback progressCallback);
	void DoSendSystem();
	void DoReceiveSystem();

private:
	void SendParameter(ParamAddress paramAddress, int value);
	void SendParameterRequest(ParamAddress paramAddress);
	void SendBlockRequest(ParamAddress paramAddress);
	void SendSysexData(const uint8_t *pSysexData, int dataSizeInBytes);
	void SendState(PresetState stateType, bool enabled);
	int WriteParameterBlock(BlockInfo& blockInfo, int8_t* pBlock);
	void BuildBulkDumpBlock(ParamAddress baseAddress, std::vector<uint8_t>& block);
	void handleIncomingMidiMessage(MidiInput *source, const MidiMessage &msg) override;
	void changeListenerCallback(ChangeBroadcaster *source) override;
	void HandleSysExParamOnMessageThread(ParamAddress paramAddress, int32_t value);
	void HandleBulkDumpOnMessageThread(std::vector<uint8_t>& bulkData);
	void HandleStateChangedOnMessageThread(PresetState stateType, bool enabled);
	void OnMidiRequestTimeout();
	void CancelRequest();
	void HandleParameterChanged(const ParamID& paramID, int32_t value, bool isMuted);


	// This is used to dispach an incoming SysEx response to the GUI (OS message) thread.
	// The only difference between this an the IncomingNRPNCallback is that SysEx doesn't 
	// have an NRPN index (because it is not included in the SysEx response).
	struct IncomingSysExParamCallback : public CallbackMessage
	{
		IncomingSysExParamCallback(MidiManager* pMidiManager, ParamAddress paramAddress, int32_t value)
			: m_pMidiManager(pMidiManager), m_paramAddress(paramAddress), m_value(value) {}

		void messageCallback() override
		{
			//DBG("   ...callback message. Forwarding to handler.");
			if (m_pMidiManager != nullptr)
			{
				m_pMidiManager->HandleSysExParamOnMessageThread(m_paramAddress, m_value);
			}
		}

		MidiManager* m_pMidiManager;;
		int32_t m_value;
		ParamAddress m_paramAddress;
	};

	struct IncomingBulkDumpCallback : public CallbackMessage
	{
		IncomingBulkDumpCallback(MidiManager* pMidiManager, uint8_t* pBulkData, int32_t bulkDataSize)
			: m_pMidiManager(pMidiManager)
		{
			m_bulkData.resize(bulkDataSize);
			memcpy(&m_bulkData[0], pBulkData, bulkDataSize);
		}

		void messageCallback() override
		{
			//DBG("   ...callback message. Forwarding to handler.");
			if (m_pMidiManager != nullptr)
			{
				m_pMidiManager->HandleBulkDumpOnMessageThread(m_bulkData);
			}
		}

		MidiManager* m_pMidiManager;;
		std::vector<uint8_t> m_bulkData;
	};

	struct IncomingStateChangeCallback : public CallbackMessage
	{
		IncomingStateChangeCallback(MidiManager* pMidiManager, PresetState stateType, bool enabled)
			: m_pMidiManager(pMidiManager), m_stateType(stateType), m_enabled(enabled)
		{}

		void messageCallback() override
		{
			//DBG("   ...callback message. Forwarding to handler.");
			if (m_pMidiManager != nullptr)
			{
				m_pMidiManager->HandleStateChangedOnMessageThread(m_stateType, m_enabled);
			}
		}

		MidiManager* m_pMidiManager;;
		PresetState m_stateType;
		bool m_enabled;
	};

	void SendPresetCallback(int numberOfBlocksSoFar, bool isDone)
	{
		if (m_midiProgressCallback && !m_midiProgressCallback(static_cast<double>(numberOfBlocksSoFar) / m_totalNumberOfBlocksToSend, isDone))
		{
			// kill send thread
			m_pSendThread->signalThreadShouldExit();
			m_midiProgressCallback = nullptr;
		}
	}


	class SendPresetCallbackMessage : public CallbackMessage
	{
	public:
		SendPresetCallbackMessage(MidiManager* pMidiManager, int numberOfParametersSoFar, bool isDone)
			: m_pMidiManager(pMidiManager), m_numberOfParametersSoFar(numberOfParametersSoFar), m_isDone(isDone) {}

		void messageCallback() override
		{
			if (m_pMidiManager != nullptr)
				m_pMidiManager->SendPresetCallback(m_numberOfParametersSoFar, m_isDone);
		}

		MidiManager*	m_pMidiManager;;
		int			m_numberOfParametersSoFar;
		bool		m_isDone;
	};


	class SysExParamTimer : public Timer
	{
	public:
		SysExParamTimer(MidiManager* pMidimanager)
			:m_pMidimanager(pMidimanager) {}
		void timerCallback() override
		{
			assert(m_pMidimanager);
			if (m_pMidimanager)
			{
				m_pMidimanager->OnMidiRequestTimeout();
			}
		}
	private:
		MidiManager * m_pMidimanager;
	};
	std::unique_ptr<SysExParamTimer>	m_pSysExParamTimer;


	MidiProgressCallback m_midiProgressCallback;
	SendThreadUPtr		m_pSendThread;
	friend SendThread;

	Preset*				m_pPreset;
	CommandHistory*		m_pCommandHistory;
	AudioDeviceManager	m_deviceManager;
	MidiOutput*			m_pMidiOutDevice;
	uint8_t				m_deviceID{ 0 };
	String				m_midiInCurrentName;
	bool				m_syncOn{ false };
	int					m_numberOfParametersSoFar;
	int					m_totalNumberOfBlocksToSend;
	int					m_totalNumberOfBlocksToRecv;
	int					m_totalNumberOfParametersRecv;
	bool				m_presetReceiveInProgress{ false };
	bool				m_frameIsActive{ false };	//!< When true, a frame start message has been received but not yet a frame end.
	uint8_t				m_frameBank;
	uint8_t				m_framePreset;
	std::vector<ParamValue>					m_paramValues;
	std::vector<std::vector<uint8_t>>		m_presetBlocksToSend;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
	Preset::StateChangedConnection			m_stateChangedConnection;
};



#endif  // MIDIMANAGER_H_INCLUDED
