/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <typeinfo>
#include <sstream>
#include <iomanip>
#include <string>
#include "gsl_util.h"
#include "SettingsPanel.h"
#include "OscillatorPanel.h"
#include "RotorPanel.h"
#include "LFOPanel.h"
#include "EGPanel.h"
#include "LoopEGPanel.h"
#include "KeyTablePanel.h"
#include "SequencerPanel.h"
#include "FXChannelPanel.h"
#include "Preset.h"
#include "ElementView.h"
#include "Properties.h"
#include "Command.h"


using std::vector;
using std::string;
using gsl::narrow;
static const juce::Identifier paramIndexID = "paramIndex";


String SliderCutoff::getTextFromValue(double value)
{
	assert(value >= 0 && value <= 1260);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(1);
	os << value / 10.0;

	return os.str();
}

double SliderCutoff::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	value = value * 10;
	return std::max(0.0, std::min(126.0, value));
}

String SliderDeviceID::getTextFromValue(double value)
{
	if (value > 15)
	{
		return "All";
	}
	return SliderProp::getTextFromValue(value);
}

String SliderInitPan::getTextFromValue(double valueDbl)
{
	int value = lrint(valueDbl);
	assert(value >= -63.0 && value <= 63.0);
	std::ostringstream os;
	// os << std::fixed << std::showpoint << std::setprecision(0);
	switch(value)
	{
	case -63:	os << "Left";	break;
	case 0:		os << "Center";	break;
	case 63:	os << "Right";	break;
	default:
		if (value < 0) 
		{	os << -value << " (L)";	}
		else 
		{	os << value << " (R)";	}
		break;
	};

	return os.str();
}

SettingsPanel::SettingsPanel(Preset* pPreset, CommandHistory* pCommandHistory, ViewModel* pViewModel, SelectionModel<ElementID>* pSelectionModel)
	:m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	,m_pViewModel(pViewModel)
	,m_pSelectionModel(pSelectionModel)
{

	m_titleLabel.setFont(Font(22.0f));
	m_titleLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_titleLabel);

	m_wiresLabel.setBounds(10, getLocalBounds().getHeight() - 136, 40, 26);
	m_wiresLabel.setText("wires:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_wiresLabel);

	m_wireSlider.setBounds(10, getLocalBounds().getHeight() - 120, 170, 40);
	m_wireSlider.setRange(0.0f, 100.0f, 1);
	m_wireSlider.setValue(pPropertiesFile->getIntValue(propWireOpacity, 100), NotificationType::dontSendNotification);
	m_wireSlider.setTextValueSuffix("%");
	m_wireSlider.setTextBoxStyle(Slider2::TextBoxAbove, false, 50, 40);
	m_wireSlider.addListener(this);
	addAndMakeVisible(m_wireSlider);

	m_zoomLabel.setBounds(10, getLocalBounds().getHeight() - 66, 40, 26);
	m_zoomLabel.setText("zoom:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_zoomLabel);

	m_zoomSlider.setBounds(10, getLocalBounds().getHeight() - 60, 170, 40);
	m_zoomSlider.setRange(50.0f, 100.0f, 1);
	m_zoomSlider.setValue(pPropertiesFile->getValue(propZoom, "100.0").getDoubleValue(), NotificationType::dontSendNotification);
	m_zoomSlider.setTextValueSuffix("%");
	m_zoomSlider.setTextBoxStyle(Slider2::TextBoxAbove, false, 50, 40);
	m_zoomSlider.addListener(this);
	addAndMakeVisible(m_zoomSlider);

	m_selectionConnection = m_pSelectionModel->selectionChangedEvent.Connect([this](){ OnSelectionChanged(); });

	m_viewport.setScrollBarsShown(true,false);

	resized();

//	m_pPanel = std::make_unique<Component>()
}

SettingsPanel::~SettingsPanel()
{
	m_wireSlider.removeListener(this);
	m_zoomSlider.removeListener(this);
	m_selectionConnection->Disconnect();
}

void SettingsPanel::sliderValueChanged(Slider2 *slider)
{
	if (slider == &m_zoomSlider)
	{
		double zoom = slider->getValue();
		ZoomChangedEvent.Notify(zoom);
		pPropertiesFile->setValue(propZoom, zoom);
	}
	else if (slider == &m_wireSlider)
	{
		double opacity = m_wireSlider.getValue();
		pPropertiesFile->setValue(propWireOpacity, opacity);
	}

}

void SettingsPanel::resized()
{
	m_titleLabel.setBounds(0,0,getWidth(),60);


	auto lb = getLocalBounds();
	lb.removeFromBottom(130);
	lb.removeFromTop(60);
	lb.removeFromLeft(4);
	lb.removeFromRight(4);
	m_viewport.setBounds(lb);

	m_wiresLabel.setBounds(10, getHeight() - 110, 50, 26);
	m_wireSlider.setBounds(10, getHeight() - 110, getWidth() - 20, 40);

	m_zoomLabel.setBounds(10, getHeight() - 60, 50, 26);
	m_zoomSlider.setBounds(10, getHeight() - 60, getWidth() - 20, 40);
}
void SettingsPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffC0C0C0));
//	g.setColour(Colour(0xFF6FB9FF));
	g.setColour(Colour(0xff606060));
	g.drawRect(getLocalBounds(), 4);


}


using PanelItem = struct
{
	ElementType	elemType;
	std::function<std::unique_ptr<Component>(Preset*, CommandHistory*,ElementID, ViewModel*)>	panelFactory;
};

vector<PanelItem> PanelItems
{
	{ ElementType::Oscillator,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<OscillatorPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Rotor,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<RotorPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::AM,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<AMPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Vector,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<VectorPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Mixer,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<MixerPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Filter,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<FilterPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::InsertFX,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<InsertFXPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::VCA,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<VCAPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::LFO,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<LFOPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::VibratoLFO,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<VibratoLFOPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::EG,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<EGPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Lag,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<LagPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::EnvFol,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<EnvFolPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::KeyTable,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<KeyTablePanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Seq,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<SequencerPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::LoopEG,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<LoopEGPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::Ribbon,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*){ return std::make_unique<RibbonPanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::FXChannel,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel* pViewModel) { return std::make_unique<FXChannelPanel>(pPreset,pCommandHistory,elemID, pViewModel); } },

	{ ElementType::Home,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*) { return std::make_unique<HomePanel>(pPreset,pCommandHistory,elemID); } },
	{ ElementType::System,[](Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID, ViewModel*) { return std::make_unique<SystemPanel>(pPreset,pCommandHistory,elemID); } },
};


void SettingsPanel::OnSelectionChanged()
{
	bool clearPanel = true;
	if (m_pSelectionModel->size() >= 0)
	{
		if (m_pPanel)
		{
			removeChildComponent(m_pPanel.get());
			m_pPanel.reset();
		}
		
		//if (m_pSelectionModel->size() == 0)
		//{
		//	m_titleLabel.setText("Home", NotificationType::dontSendNotification);

		//	ElementID elemID(ElementType::Home, 0);

		//	m_pPanel = std::make_unique<HomePanel>(m_pPreset, m_pCommandHistory, elemID);
		//}
		//else
		if (m_pSelectionModel->size() == 1)
		{
			ElementID elemID = m_pSelectionModel->GetFirstSelection();
			auto iter = std::find_if(PanelItems.begin(),PanelItems.end(),[&elemID](const PanelItem& item) { return item.elemType == elemID.elemType; });
			if (iter != PanelItems.end())
			{
				m_pPanel = iter->panelFactory(m_pPreset, m_pCommandHistory, m_pSelectionModel->GetFirstSelection(), m_pViewModel);
			}

			m_titleLabel.setText(m_pViewModel->GetElementFriendlyName(elemID),NotificationType::dontSendNotification);
		}
		else
		{
			vector<string> elementNames;

			std::for_each(m_pSelectionModel->begin(),m_pSelectionModel->end(),
				[&elementNames,this](const ElementID& elemID)
			{
				elementNames.push_back(this->m_pViewModel->GetElementFriendlyName(elemID));
			});

			std::sort(elementNames.begin(),elementNames.end());
			m_pPanel = std::make_unique<MultiSelectPanel>(elementNames);

			m_titleLabel.setText("",NotificationType::dontSendNotification);
		}

		if (m_pPanel)
		{
			resized();
			auto lb = getLocalBounds();
			lb.setHeight(m_pPanel->getHeight());
			m_pPanel->setBounds(lb);
			m_viewport.setViewedComponent(m_pPanel.get(),false);
			addAndMakeVisible(m_viewport);
		}
		clearPanel = false;
	}
	else
	{
		m_titleLabel.setText("",NotificationType::dontSendNotification);
	}

	if (clearPanel && m_pPanel)
	{
		m_viewport.setViewedComponent(nullptr,false);
		removeChildComponent(&m_viewport);
	}
}

MultiSelectPanel::MultiSelectPanel(std::vector<std::string>& elementNames)
	:m_elemLabels(elementNames.size())
{
	const int leftX = 10;
	const int leftWidth = 160;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = 10 - rowHeight;

	for (size_t i = 0; i < elementNames.size(); ++i)
	{
		yPos += rowHeight;

		m_elemLabels[i].setBounds(leftX,yPos,leftWidth,ctrlHeight);
		m_elemLabels[i].setText(elementNames[i],NotificationType::dontSendNotification);
		addAndMakeVisible(m_elemLabels[i]);
	}

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);
}
void MultiSelectPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}


AMPanel::AMPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_algorithmLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_algorithmLabel.setText("Algorithm:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_algorithmLabel);
	m_algorithmCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> algorithmList = m_pPreset->GetParameterListText(ParamID(m_elemID,amAlgorithm));;
	for (size_t i = 0; i < algorithmList.size(); ++i)
	{
		m_algorithmCombo.addItem(algorithmList[i],i + 1);
	}
	m_algorithmCombo.addListener(this);
	addAndMakeVisible(m_algorithmCombo);

	yPos += rowHeight;
	m_offsetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_offsetLabel.setText("Offset:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_offsetLabel);
	m_offsetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_offsetSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,amOffset));
	m_offsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_offsetSlider.addListener(this);
	addAndMakeVisible(m_offsetSlider);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	Update();
}

AMPanel::~AMPanel()
{
	m_algorithmCombo.removeListener(this);
	m_offsetSlider.removeListener(this);
}

void AMPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void AMPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_algorithmCombo)
	{ 
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, amAlgorithm), m_algorithmCombo.getSelectedId() - 1));
	}
}

void AMPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_offsetSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, amOffset), narrow<int16_t>(lrint(m_offsetSlider.getValue()))));
	}
}

void AMPanel::Update()
{
	m_algorithmCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,amAlgorithm)) + 1, NotificationType::dontSendNotification);
	m_offsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,amOffset)), NotificationType::dontSendNotification);
}


VectorPanel::VectorPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_xOffsetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_xOffsetLabel.setText("X-Offset:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_xOffsetLabel);
	m_xOffsetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_xOffsetSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vecXOffset));
	m_xOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_xOffsetSlider.addListener(this);
	addAndMakeVisible(m_xOffsetSlider);

	yPos += rowHeight;
	m_yOffsetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_yOffsetLabel.setText("Y-Offset:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_yOffsetLabel);
	m_yOffsetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_yOffsetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vecYOffset));
	m_yOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_yOffsetSlider.addListener(this);
	addAndMakeVisible(m_yOffsetSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

VectorPanel::~VectorPanel()
{
	m_xOffsetSlider.removeListener(this);
	m_yOffsetSlider.removeListener(this);
}

void VectorPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void VectorPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_xOffsetSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vecXOffset), lrint(m_xOffsetSlider.getValue())));
	}
	else if (slider == &m_yOffsetSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vecYOffset), lrint(m_yOffsetSlider.getValue())));
	}
}

void VectorPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_xOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vecXOffset)), NotificationType::dontSendNotification);
	m_yOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vecYOffset)), NotificationType::dontSendNotification);
}

MixerPanel::MixerPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	int yPos = 10;

	m_OutputLevelLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_OutputLevelLabel.setText("MixOut:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_OutputLevelLabel);
	m_OutputLevelSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_OutputLevelSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,mixOutputLevel));
	m_OutputLevelSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_OutputLevelSlider.addListener(this);
	addAndMakeVisible(m_OutputLevelSlider);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	Update();
}

MixerPanel::~MixerPanel()
{
	m_OutputLevelSlider.removeListener(this);
}

void MixerPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void MixerPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_OutputLevelSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, mixOutputLevel), lrint(m_OutputLevelSlider.getValue())));
	}
}

void MixerPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_OutputLevelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,mixOutputLevel)), NotificationType::dontSendNotification);
}

FilterPanel::FilterPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
	, m_modeCombos(6)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_filterTypeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_filterTypeLabel.setText("Type:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_filterTypeLabel);
	m_filterTypeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> filterTypeList = m_pPreset->GetParameterListText(ParamID(elemID,filterType));
	for (size_t i = 0; i < filterTypeList.size(); ++i)
	{
		m_filterTypeCombo.addItem(filterTypeList[i],i + 1);
	}
	m_filterTypeCombo.getProperties().set(paramIndexID, filterType);
	m_filterTypeCombo.addListener(this);
	addAndMakeVisible(m_filterTypeCombo);

	yPos += rowHeight;
	m_modeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_modeLabel.setText("Mode:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_modeLabel);

	// m_modeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	// std::vector<std::string> modeList = m_pPreset->GetParameterListText(ParamID(elemID,filterMM1Mode));
	// for (size_t i = 0; i < modeList.size(); ++i)
	// {
	// 	m_modeCombo.addItem(modeList[i],i + 1);
	// }
	// m_modeCombo.addListener(this);
	// addAndMakeVisible(m_modeCombo);

	// It might seem better to have only one combobox for the modes and populate it depending on the current filter type.
	// But it seems that when you clear, repopulate, and then select an item in the combobox, the combobox triggers a 
	// juce notification that we don't want to happen, even though we specified not to send one. That in turn causes 
	// a superflous CmdSetParameterValue command to be sent and added to the edit history.
	// So, that's why there is one combobox for each type of folter here.
	// m_waveCombos.resize(oscTypeList.size());	// create one wave combobox for each oscillator type
	m_modeCombos[0].setBounds(rightX, yPos, rightWidth, ctrlHeight);	// First initialize the BYPASS option
	m_modeCombos[0].addItem("---", 1);
	m_modeCombos[0].setSelectedId(1, NotificationType::dontSendNotification);
	m_modeCombos[0].addListener(this);
	addChildComponent(m_modeCombos[0]);
	for (size_t i = 1; i < filterTypeList.size() - 1; ++i)	// now do the remaining filter types, except for Vocal since there is no mode for Vocal.
	{
		m_modeCombos[i].setBounds(rightX, yPos, rightWidth, ctrlHeight);
		std::vector<std::string> modeList = m_pPreset->GetParameterListText(ParamID(m_elemID, i));
		for (size_t j = 0; j < modeList.size(); ++j)
		{
			m_modeCombos[i].addItem(modeList[j], j + 1);
		}
		m_modeCombos[i].getProperties().set(paramIndexID, static_cast<int>(i));
		m_modeCombos[i].addListener(this);
		addChildComponent(m_modeCombos[i]);
	}

	yPos += rowHeight;
	m_cutoffLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_cutoffLabel.setText("Cutoff:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_cutoffLabel);
	m_cutoffSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_cutoffSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterCutoff));
	m_cutoffSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cutoffSlider.getProperties().set(paramIndexID, filterCutoff);
	m_cutoffSlider.addListener(this);
	addAndMakeVisible(m_cutoffSlider);

	yPos += rowHeight;
	m_resonanceLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_resonanceLabel.setText("Res:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_resonanceLabel);
	m_resonanceSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_resonanceSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterResonance));
	m_resonanceSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_resonanceSlider.setTextValueSuffix(" %");
	m_resonanceSlider.getProperties().set(paramIndexID, filterResonance);
	m_resonanceSlider.addListener(this);
	addAndMakeVisible(m_resonanceSlider);

	yPos += rowHeight;
	m_dampingLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_dampingLabel.setText("Damping:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_dampingLabel);
	m_dampingSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_dampingSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterDamping));
	m_dampingSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_dampingSlider.getProperties().set(paramIndexID, filterDamping);
	m_dampingSlider.addListener(this);
	addAndMakeVisible(m_dampingSlider);

	//yPos += rowHeight;
	m_xFadeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_xFadeLabel.setText("xFade:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_xFadeLabel);
	m_xFadeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_xFadeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterXFade));
	m_xFadeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_xFadeSlider.getProperties().set(paramIndexID, filterXFade);
	m_xFadeSlider.addListener(this);
	addAndMakeVisible(m_xFadeSlider);

	yPos += rowHeight;
	m_keytrackingLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_keytrackingLabel.setText("KeyTrk:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_keytrackingLabel);
	m_keytrackingSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_keytrackingSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterKeytracking));
	m_keytrackingSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_keytrackingSlider.setTextValueSuffix(" %");
	m_keytrackingSlider.getProperties().set(paramIndexID, filterKeytracking);
	m_keytrackingSlider.addListener(this);
	addAndMakeVisible(m_keytrackingSlider);

	yPos += rowHeight;
	m_keycenterLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_keycenterLabel.setText("KeyCntr:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_keycenterLabel);
	m_keycenterSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_keycenterSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,filterKeycenter));
	m_keycenterSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_keycenterSlider.getProperties().set(paramIndexID, filterKeycenter);
	m_keycenterSlider.addListener(this);
	addAndMakeVisible(m_keycenterSlider);

	// vowels
	std::ostringstream os;
	for (int i = 0; i < 5; ++i)
	{
		yPos += rowHeight;
		m_vowelLabels.push_back(std::make_unique<Label>());
		m_vowelLabels[i]->setBounds(leftX, yPos, leftWidth, ctrlHeight);
		os.str("");
		os << "Vowel" << i + 1 << ":";
		m_vowelLabels[i]->setText(os.str().c_str(), NotificationType::dontSendNotification);
		addAndMakeVisible(m_vowelLabels[i].get());

		m_vowelComboboxes.push_back(std::make_unique<ComboBox>());
		m_vowelComboboxes[i]->setBounds(rightX, yPos, rightWidth, ctrlHeight);
		std::vector<std::string> vowelList = m_pPreset->GetParameterListText(ParamID(elemID, filterVowel1 + i));	// note: depends on vowel parameters being defined sequentially
		for (size_t v = 0; v < vowelList.size(); ++v)
		{
			m_vowelComboboxes[i]->addItem(vowelList[v], v + 1);
		}
		m_vowelComboboxes[i]->getProperties().set(paramIndexID, filterVowel1 + i);
		m_vowelComboboxes[i]->addListener(this);
		addAndMakeVisible(m_vowelComboboxes[i].get());
	}

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent(
		[this](Version, bool)
	{
		Update();
	});

	Update();
}

FilterPanel::~FilterPanel()
{
	m_filterTypeCombo.removeListener(this);
	m_cutoffSlider.removeListener(this);
	m_resonanceSlider.removeListener(this);
	m_dampingSlider.removeListener(this);
	m_xFadeSlider.removeListener(this);
	m_keytrackingSlider.removeListener(this);
	m_keycenterSlider.removeListener(this);
	for (size_t i = 0; i < m_vowelComboboxes.size(); ++i)
	{
		m_vowelComboboxes[i]->removeListener(this);
	}

	std::vector<std::string> filterTypeList = m_pPreset->GetParameterListText(ParamID(m_elemID, oscType));;
	for (size_t i = 0; i < filterTypeList.size() - 1; ++i)	// Don't include Vocal filter in this loop
	{
		m_modeCombos[i].removeListener(this);
	}
}

void FilterPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void FilterPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	assert(
		(comboBoxThatHasChanged == &m_filterTypeCombo) ||
		(comboBoxThatHasChanged == &m_modeCombos[0]) ||
		(comboBoxThatHasChanged == &m_modeCombos[1]) ||
		(comboBoxThatHasChanged == &m_modeCombos[2]) ||
		(comboBoxThatHasChanged == &m_modeCombos[3]) ||
		(comboBoxThatHasChanged == &m_modeCombos[4]) ||
		(comboBoxThatHasChanged == &m_modeCombos[5]) ||
		(comboBoxThatHasChanged == m_vowelComboboxes[0].get()) ||
		(comboBoxThatHasChanged == m_vowelComboboxes[1].get()) ||
		(comboBoxThatHasChanged == m_vowelComboboxes[2].get()) ||
		(comboBoxThatHasChanged == m_vowelComboboxes[3].get()) ||
		(comboBoxThatHasChanged == m_vowelComboboxes[4].get()));

	// Extract the parameter index from the slider control.
	var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = comboBoxThatHasChanged->getSelectedId() - 1;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void FilterPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_cutoffSlider) ||
		(slider == &m_resonanceSlider) ||
		(slider == &m_dampingSlider) ||
		(slider == &m_xFadeSlider) ||
		(slider == &m_keytrackingSlider) ||
		(slider == &m_keycenterSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void FilterPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	int currentFilterType = m_pPreset->GetParameterValue(ParamID(m_elemID,filterType));
	m_filterTypeCombo.setSelectedId(currentFilterType + 1, NotificationType::dontSendNotification);

	int currentFilterMode = filterMM1Mode;
	switch (m_filterTypeCombo.getSelectedId() - 1)
	{
	case 0: currentFilterMode = 0; break;
	case 1: currentFilterMode = filterMM1Mode; break;
	case 2: currentFilterMode = filterSSMMode; break;
	case 3: currentFilterMode = filterMiniMode; break;
	case 4: currentFilterMode = filterObieMode; break;
	case 5: currentFilterMode = filterCombMode; break;
	default: currentFilterMode = -1; break;
	};
	std::vector<std::string> filterTypeList = m_pPreset->GetParameterListText(ParamID(m_elemID, filterType));
	for (int i = 0; i < static_cast<int>(filterTypeList.size() - 1); ++i)	// Don't include Vocal filter in this loop
	{
		m_modeCombos[i].setVisible(currentFilterMode == i);
		m_modeCombos[i].setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, i)) + 1, NotificationType::dontSendNotification);
	}

	m_cutoffSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, filterCutoff)), NotificationType::dontSendNotification);
	m_resonanceSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,filterResonance)), NotificationType::dontSendNotification);
	m_dampingSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,filterDamping)), NotificationType::dontSendNotification);
	m_xFadeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,filterXFade)), NotificationType::dontSendNotification);
	m_keytrackingSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,filterKeytracking)), NotificationType::dontSendNotification);
	m_keycenterSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,filterKeycenter)), NotificationType::dontSendNotification);

	m_dampingLabel.setVisible(currentFilterType == 5);
	m_dampingSlider.setVisible(currentFilterType == 5);
	m_xFadeLabel.setVisible(currentFilterType == 6);
	m_xFadeSlider.setVisible(currentFilterType == 6);
	m_modeLabel.setVisible(currentFilterType != 6);
	//m_modeCombo.setVisible(currentFilterType != 6);

	for (size_t i = 0; i < m_vowelComboboxes.size(); ++i)
	{
		int vowel = m_pPreset->GetParameterValue(ParamID(m_elemID, filterVowel1 + i));
		m_vowelComboboxes[i]->setSelectedId(vowel + 1, NotificationType::dontSendNotification);

		m_vowelLabels[i]->setVisible(currentFilterType == 6);
		m_vowelComboboxes[i]->setVisible(currentFilterType == 6);
	}

}



InsertFXPanel::InsertFXPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_modeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_modeLabel.setText("Mode:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_modeLabel);
	m_modeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> insertFXModeList = m_pPreset->GetParameterListText(ParamID(m_elemID,insFxMode));;
	for (size_t i = 0; i < insertFXModeList.size(); ++i)
	{
		m_modeCombo.addItem(insertFXModeList[i],i + 1);
	}
	m_modeCombo.addListener(this);
	addAndMakeVisible(m_modeCombo);

	yPos += rowHeight;
	m_valueLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_valueLabel.setText("Value:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_valueLabel);
	m_valueSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_valueSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, insFxValue));
	m_valueSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_valueSlider.addListener(this);
	addAndMakeVisible(m_valueSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

InsertFXPanel::~InsertFXPanel()
{
	m_modeCombo.removeListener(this);
	m_valueSlider.removeListener(this);
}

void InsertFXPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void InsertFXPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_modeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, insFxMode), m_modeCombo.getSelectedId() - 1));
	}
}

void InsertFXPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_valueSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, insFxValue), lrint(m_valueSlider.getValue())));
	}
}

void InsertFXPanel::Update()
{
	m_modeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,insFxMode)) + 1, NotificationType::dontSendNotification);
	m_valueSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,insFxValue)), NotificationType::dontSendNotification);
}




VCAPanel::VCAPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_typeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_typeLabel.setText("Type:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_typeLabel);
	m_typeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> vcaTypeList = m_pPreset->GetParameterListText(ParamID(m_elemID,vcaType));;
	for (size_t i = 0; i < vcaTypeList.size(); ++i)
	{
		m_typeCombo.addItem(vcaTypeList[i],i + 1);
	}
	m_typeCombo.addListener(this);
	addAndMakeVisible(m_typeCombo);

	yPos += rowHeight;
	m_boostLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_boostLabel.setText("Boost:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_boostLabel);
	m_boostSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_boostSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vcaBoost));
	m_boostSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_boostSlider.addListener(this);
	addAndMakeVisible(m_boostSlider);	
	
	yPos += rowHeight;
	m_levelLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_levelLabel.setText("Level:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_levelLabel);
	m_levelSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_levelSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vcaLevel));
	m_levelSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_levelSlider.addListener(this);
	addAndMakeVisible(m_levelSlider);

	yPos += rowHeight;
	m_panLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_panLabel.setText("InitPan:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_panLabel);
	m_panSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_panSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vcaInitPan));
	m_panSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_panSlider.addListener(this);
	addAndMakeVisible(m_panSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

VCAPanel::~VCAPanel()
{
	m_typeCombo.removeListener(this);
	m_boostSlider.removeListener(this);
	m_levelSlider.removeListener(this);
	m_panSlider.removeListener(this);
}

void VCAPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void VCAPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_typeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vcaType), m_typeCombo.getSelectedId() - 1));
	}
}

void VCAPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_boostSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vcaBoost), lrint(m_boostSlider.getValue())));
	}
	if (slider == &m_levelSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vcaLevel), lrint(m_levelSlider.getValue())));
	}
	if (slider == &m_panSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vcaInitPan), lrint(m_panSlider.getValue())));
	}
}

void VCAPanel::Update()
{
	m_typeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,vcaType)) + 1, NotificationType::dontSendNotification);
	m_boostSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vcaBoost)), NotificationType::dontSendNotification);
	m_levelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vcaLevel)), NotificationType::dontSendNotification);
	m_panSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vcaInitPan)), NotificationType::dontSendNotification);
}


LagPanel::LagPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	int yPos = 10;

	m_timeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_timeLabel.setText("Time:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_timeLabel);
	m_timeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_timeSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lagTime));
	m_timeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_timeSlider.addListener(this);
	addAndMakeVisible(m_timeSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

LagPanel::~LagPanel()
{
	m_timeSlider.removeListener(this);
}

void LagPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void LagPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_timeSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lagTime), lrint(m_timeSlider.getValue())));
	}
}

void LagPanel::Update()
{
	m_timeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lagTime)), NotificationType::dontSendNotification);
}



EnvFolPanel::EnvFolPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_attackLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_attackLabel.setText("Attack:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_attackLabel);
	m_attackSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_attackSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,envFolAttack));
	m_attackSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_attackSlider.getProperties().set(paramIndexID, envFolAttack);
	m_attackSlider.addListener(this);
	addAndMakeVisible(m_attackSlider);

	yPos += rowHeight;
	m_releaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_releaseLabel.setText("Release:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_releaseLabel);
	m_releaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_releaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,envFolRelease));
	m_releaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_releaseSlider.getProperties().set(paramIndexID, envFolRelease);
	m_releaseSlider.addListener(this);
	addAndMakeVisible(m_releaseSlider);

	yPos += rowHeight;
	m_inputLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_inputLabel.setText("Input:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_inputLabel);
	m_inputSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_inputSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,envFolInput));
	m_inputSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_inputSlider.getProperties().set(paramIndexID, envFolInput);
	m_inputSlider.addListener(this);
	addAndMakeVisible(m_inputSlider);

	yPos += rowHeight;
	m_outputLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_outputLabel.setText("Output:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_outputLabel);
	m_outputSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_outputSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,envFolOutput));
	m_outputSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_outputSlider.getProperties().set(paramIndexID, envFolOutput);
	m_outputSlider.addListener(this);
	addAndMakeVisible(m_outputSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

EnvFolPanel::~EnvFolPanel()
{
	m_attackSlider.removeListener(this);
	m_releaseSlider.removeListener(this);
	m_inputSlider.removeListener(this);
	m_outputSlider.removeListener(this);
}

void EnvFolPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void EnvFolPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_attackSlider) ||
		(slider == &m_releaseSlider) ||
		(slider == &m_inputSlider) ||
		(slider == &m_outputSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void EnvFolPanel::Update()
{
	m_attackSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,envFolAttack)), NotificationType::dontSendNotification);
	m_releaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,envFolRelease)), NotificationType::dontSendNotification);
	m_inputSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,envFolInput)), NotificationType::dontSendNotification);
	m_outputSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,envFolOutput)), NotificationType::dontSendNotification);
}



RibbonPanel::RibbonPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{
	m_elemID.elemIndex = 0;		// Both ribbon elements affect the same settings

	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_offsetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_offsetLabel.setText("Offset:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_offsetLabel);
	m_offsetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_offsetSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,ribOffset));
	m_offsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_offsetSlider.setTextValueSuffix(" %");
	m_offsetSlider.addListener(this);
	addAndMakeVisible(m_offsetSlider);

	yPos += rowHeight;
	m_intensityLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_intensityLabel.setText("Intens:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_intensityLabel);
	m_intensitySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_intensitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,ribIntensity));
	m_intensitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_intensitySlider.setTextValueSuffix(" %");
	m_intensitySlider.addListener(this);
	addAndMakeVisible(m_intensitySlider);

	yPos += rowHeight;
	m_holdButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_holdButton.setButtonText("Hold");
	m_holdButton.addListener(this);
	addAndMakeVisible(m_holdButton);

	yPos += rowHeight;
	m_touchOffButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_touchOffButton.setButtonText("TouchOff");
	m_touchOffButton.addListener(this);
	addAndMakeVisible(m_touchOffButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

RibbonPanel::~RibbonPanel()
{
	m_offsetSlider.removeListener(this);
	m_intensitySlider.removeListener(this);
	m_holdButton.removeListener(this);
	m_touchOffButton.removeListener(this);
}

void RibbonPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void RibbonPanel::sliderValueChanged(Slider2* slider)
{
	if (slider == &m_offsetSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, ribOffset), lrint(m_offsetSlider.getValue())));
	}
	else if (slider == &m_intensitySlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, ribIntensity), lrint(m_intensitySlider.getValue())));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void RibbonPanel::buttonClicked(Button* button)
{
	if (button == &m_holdButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, ribHold), m_holdButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_touchOffButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, ribTouchOffset), m_touchOffButton.getToggleState() ? 1 : 0));
	}
}


void RibbonPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_offsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,ribOffset)), NotificationType::dontSendNotification);
	m_intensitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,ribIntensity)), NotificationType::dontSendNotification);
	m_holdButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,ribHold)) != 0,NotificationType::dontSendNotification);
	m_touchOffButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,ribTouchOffset)) != 0,NotificationType::dontSendNotification);

}

String RndTuneSlider::getTextFromValue(double value)
{
	if (value == -1)
	{
		return "Global";
	}
	return Slider2::getTextFromValue(value);
}

String GlobalAtVelSlider::getTextFromValue(double value)
{
	if (value == -1)
	{
		return "Preset";
	}
	return Slider2::getTextFromValue(value);
}


HomePanel::HomePanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID)
	:m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	, m_elemID(elemID)
	, m_perfLabels(5)
	, m_perfSliders(5)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_glideTypeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_glideTypeLabel.setText("GldType:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_glideTypeLabel);
	m_glideTypeCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> glidetypeList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloGldType));;
	for (size_t i = 0; i < glidetypeList.size(); ++i)
	{
		m_glideTypeCombo.addItem(glidetypeList[i], i + 1);
	}
	m_glideTypeCombo.getProperties().set(paramIndexID, gloGldType);
	m_glideTypeCombo.addListener(this);
	addAndMakeVisible(m_glideTypeCombo);

	yPos += rowHeight;

	m_glideModeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_glideModeLabel.setText("GldMode:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_glideModeLabel);
	m_glideModeCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> glideModeList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloGldMode));;
	for (size_t i = 0; i < glideModeList.size(); ++i)
	{
		m_glideModeCombo.addItem(glideModeList[i], i + 1);
	}
	m_glideModeCombo.getProperties().set(paramIndexID, gloGldMode);
	m_glideModeCombo.addListener(this);
	addAndMakeVisible(m_glideModeCombo);

	yPos += rowHeight;


	m_glideTimeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_glideTimeLabel.setText("GldTime:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_glideTimeLabel);
	m_glideTimeSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_glideTimeSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloGldTime));
	m_glideTimeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_glideTimeSlider.getProperties().set(paramIndexID, gloGldTime);
	m_glideTimeSlider.addListener(this);
	addAndMakeVisible(m_glideTimeSlider);

	m_glideRateLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_glideRateLabel.setText("GldRate:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_glideRateLabel);
	m_glideRateSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_glideRateSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloGldRate));
	m_glideRateSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_glideRateSlider.setTextValueSuffix(" %");
	m_glideRateSlider.getProperties().set(paramIndexID, gloGldRate);
	m_glideRateSlider.addListener(this);
	addAndMakeVisible(m_glideRateSlider);

	yPos += rowHeight;

	m_glideRangeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_glideRangeLabel.setText("GldRange:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_glideRangeLabel);
	m_glideRangeSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_glideRangeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloGldRange));
	m_glideRangeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_glideRangeSlider.setTextValueSuffix(" %");
	m_glideRangeSlider.getProperties().set(paramIndexID, gloGldRange);
	m_glideRangeSlider.addListener(this);
	addAndMakeVisible(m_glideRangeSlider);

	yPos += rowHeight;

	m_playModeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_playModeLabel.setText("PlayMode:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_playModeLabel);
	m_playModeCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> playModeList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloPlayMode));;
	for (size_t i = 0; i < playModeList.size(); ++i)
	{
		m_playModeCombo.addItem(playModeList[i], i + 1);
	}
	m_playModeCombo.getProperties().set(paramIndexID, gloPlayMode);
	m_playModeCombo.addListener(this);
	addAndMakeVisible(m_playModeCombo);

	yPos += rowHeight;

	m_legatoLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_legatoLabel.setText("Legato:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_legatoLabel);
	m_legatoCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> legatoList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloLegato));;
	for (size_t i = 0; i < legatoList.size(); ++i)
	{
		m_legatoCombo.addItem(legatoList[i], i + 1);
	}
	m_legatoCombo.getProperties().set(paramIndexID, gloLegato);
	m_legatoCombo.addListener(this);
	addAndMakeVisible(m_legatoCombo);

	yPos += rowHeight;

	m_uniVoiceLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_uniVoiceLabel.setText("UniVoice:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_uniVoiceLabel);
	m_uniVoiceCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> uniVoiceList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloUniVoice));;
	for (size_t i = 0; i < uniVoiceList.size(); ++i)
	{
		m_uniVoiceCombo.addItem(uniVoiceList[i], i + 1);
	}
	m_uniVoiceCombo.getProperties().set(paramIndexID, gloUniVoice);
	m_uniVoiceCombo.addListener(this);
	addAndMakeVisible(m_uniVoiceCombo);

	yPos += rowHeight;

	m_uniTuneLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_uniTuneLabel.setText("UniTune:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_uniTuneLabel);
	m_uniTuneSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_uniTuneSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloUniTune));
	m_uniTuneSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_uniTuneSlider.getProperties().set(paramIndexID, gloUniTune);
	m_uniTuneSlider.addListener(this);
	addAndMakeVisible(m_uniTuneSlider);

	yPos += rowHeight;

	m_rndTuneLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_rndTuneLabel.setText("RndTune:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_rndTuneLabel);
	m_rndTuneSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_rndTuneSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloRndTune));
	m_rndTuneSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_rndTuneSlider.getProperties().set(paramIndexID, gloRndTune);
	m_rndTuneSlider.addListener(this);
	addAndMakeVisible(m_rndTuneSlider);

	yPos += rowHeight;

	m_egResetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_egResetLabel.setText("EgReset:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_egResetLabel);
	m_egResetCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> egResetList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloEgReset));;
	for (size_t i = 0; i < egResetList.size(); ++i)
	{
		m_egResetCombo.addItem(egResetList[i], i + 1);
	}
	m_egResetCombo.getProperties().set(paramIndexID, gloEgReset);
	m_egResetCombo.addListener(this);
	addAndMakeVisible(m_egResetCombo);

	yPos += rowHeight;

	m_notePriLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_notePriLabel.setText("NotePri:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_notePriLabel);
	m_notePriCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> notePriList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloNotePriority));;
	for (size_t i = 0; i < notePriList.size(); ++i)
	{
		m_notePriCombo.addItem(notePriList[i], i + 1);
	}
	m_notePriCombo.getProperties().set(paramIndexID, gloNotePriority);
	m_notePriCombo.addListener(this);
	addAndMakeVisible(m_notePriCombo);

	yPos += rowHeight;

	m_pitchWheelUpLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_pitchWheelUpLabel.setText("PW Up:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_pitchWheelUpLabel);
	m_pitchWheelUpSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_pitchWheelUpSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloPitchWheelUpRange));
	m_pitchWheelUpSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_pitchWheelUpSlider.getProperties().set(paramIndexID, gloPitchWheelUpRange);
	m_pitchWheelUpSlider.addListener(this);
	addAndMakeVisible(m_pitchWheelUpSlider);

	yPos += rowHeight;

	m_pitchWheelDownLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_pitchWheelDownLabel.setText("PW Down:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_pitchWheelDownLabel);
	m_pitchWheelDownSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_pitchWheelDownSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloPitchWheelDownRange));
	m_pitchWheelDownSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_pitchWheelDownSlider.getProperties().set(paramIndexID, gloPitchWheelDownRange);
	m_pitchWheelDownSlider.addListener(this);
	addAndMakeVisible(m_pitchWheelDownSlider);

	yPos += rowHeight;

	m_vtIntensityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_vtIntensityLabel.setText("VTCurve:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_vtIntensityLabel);
	m_vtIntensitySlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_vtIntensitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloVTIntens));
	m_vtIntensitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_vtIntensitySlider.setTextValueSuffix(" %");
	m_vtIntensitySlider.getProperties().set(paramIndexID, gloVTIntens);
	m_vtIntensitySlider.addListener(this);
	addAndMakeVisible(m_vtIntensitySlider);

	yPos += rowHeight;

	m_vtOffsetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_vtOffsetLabel.setText("VTOff:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_vtOffsetLabel);
	m_vtOffsetSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_vtOffsetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloVTOffset));
	m_vtOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_vtOffsetSlider.setTextValueSuffix(" %");
	m_vtOffsetSlider.getProperties().set(paramIndexID, gloVTOffset);
	m_vtOffsetSlider.addListener(this);
	addAndMakeVisible(m_vtOffsetSlider);

	yPos += rowHeight;

	m_atIntensityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_atIntensityLabel.setText("ATCurve:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_atIntensityLabel);
	m_atIntensitySlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_atIntensitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloATIntens));
	m_atIntensitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_atIntensitySlider.setTextValueSuffix(" %");
	m_atIntensitySlider.getProperties().set(paramIndexID, gloATIntens);
	m_atIntensitySlider.addListener(this);
	addAndMakeVisible(m_atIntensitySlider);

	yPos += rowHeight;

	m_atOffsetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_atOffsetLabel.setText("ATOff:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_atOffsetLabel);
	m_atOffsetSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_atOffsetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloATOffset));
	m_atOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_atOffsetSlider.setTextValueSuffix(" %");
	m_atOffsetSlider.getProperties().set(paramIndexID, gloATOffset);
	m_atOffsetSlider.addListener(this);
	addAndMakeVisible(m_atOffsetSlider);

	yPos += rowHeight;

	m_expPedalLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_expPedalLabel.setText("ExpPed:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_expPedalLabel);
	m_expPedalCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> expPedalList = m_pPreset->GetParameterListText(ParamID(m_elemID, gloExpPedal));;
	for (size_t i = 0; i < expPedalList.size(); ++i)
	{
		m_expPedalCombo.addItem(expPedalList[i], i + 1);
	}
	m_expPedalCombo.getProperties().set(paramIndexID, gloExpPedal);
	m_expPedalCombo.addListener(this);
	addAndMakeVisible(m_expPedalCombo);

	yPos += rowHeight;

	m_susPedal1Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_susPedal1Label.setText("SusPed1:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_susPedal1Label);
	m_susPedal1Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> susPedal1List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloSusPedal1));;
	for (size_t i = 0; i < susPedal1List.size(); ++i)
	{
		m_susPedal1Combo.addItem(susPedal1List[i], i + 1);
	}
	m_susPedal1Combo.getProperties().set(paramIndexID, gloSusPedal1);
	m_susPedal1Combo.addListener(this);
	addAndMakeVisible(m_susPedal1Combo);

	yPos += rowHeight;

	m_susPedal2Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_susPedal2Label.setText("SusPed2:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_susPedal2Label);
	m_susPedal2Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> susPedal2List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloSusPedal2));;
	for (size_t i = 0; i < susPedal2List.size(); ++i)
	{
		m_susPedal2Combo.addItem(susPedal2List[i], i + 1);
	}
	m_susPedal2Combo.getProperties().set(paramIndexID, gloSusPedal2);
	m_susPedal2Combo.addListener(this);
	addAndMakeVisible(m_susPedal2Combo);

	yPos += rowHeight;

	m_assign1Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_assign1Label.setText("Assign1:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_assign1Label);
	m_assign1Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> assign1List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloAssignFunc1));;
	for (size_t i = 0; i < assign1List.size(); ++i)
	{
		m_assign1Combo.addItem(assign1List[i], i + 1);
	}
	m_assign1Combo.getProperties().set(paramIndexID, gloAssignFunc1);
	m_assign1Combo.addListener(this);
	addAndMakeVisible(m_assign1Combo);

	yPos += rowHeight;

	m_assignMode1Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_assignMode1Label.setText("Mode 1:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_assignMode1Label);
	m_assignMode1Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> assignMode1List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloAssignMode1));;
	for (size_t i = 0; i < assignMode1List.size(); ++i)
	{
		m_assignMode1Combo.addItem(assignMode1List[i], i + 1);
	}
	m_assignMode1Combo.getProperties().set(paramIndexID, gloAssignMode1);
	m_assignMode1Combo.addListener(this);
	addAndMakeVisible(m_assignMode1Combo);

	yPos += rowHeight;

	m_assign2Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_assign2Label.setText("Assign2:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_assign2Label);
	m_assign2Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> assign2List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloAssignFunc2));;
	for (size_t i = 0; i < assign2List.size(); ++i)
	{
		m_assign2Combo.addItem(assign2List[i], i + 1);
	}
	m_assign2Combo.getProperties().set(paramIndexID, gloAssignFunc2);
	m_assign2Combo.addListener(this);
	addAndMakeVisible(m_assign2Combo);

	yPos += rowHeight;

	m_assignMode2Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_assignMode2Label.setText("Mode 2:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_assignMode2Label);
	m_assignMode2Combo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> assignMode2List = m_pPreset->GetParameterListText(ParamID(m_elemID, gloAssignMode2));;
	for (size_t i = 0; i < assignMode2List.size(); ++i)
	{
		m_assignMode2Combo.addItem(assignMode2List[i], i + 1);
	}
	m_assignMode2Combo.getProperties().set(paramIndexID, gloAssignMode2);
	m_assignMode2Combo.addListener(this);
	addAndMakeVisible(m_assignMode2Combo);

	yPos += rowHeight;

	m_patchChainButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_patchChainButton.setButtonText("Patch chain");
	m_patchChainButton.addListener(this);
	addAndMakeVisible(m_patchChainButton);

	yPos += rowHeight;


	m_samplePoolLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_samplePoolLabel.setText("SmpPool:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_samplePoolLabel);

	m_samplePoolEdit.setBounds(rightX, yPos, rightWidth - 26, ctrlHeight);
	m_samplePoolEdit.setMultiLine(false);
	m_samplePoolEdit.setReturnKeyStartsNewLine(false);
	m_samplePoolEdit.setReadOnly(false);
	m_samplePoolEdit.setScrollbarsShown(true);
	m_samplePoolEdit.setCaretVisible(true);
	m_samplePoolEdit.setPopupMenuEnabled(true);
	//m_samplePoolEdit.setInputFilter(new EditNameInputFilter(), true);
	m_samplePoolEdit.setInputRestrictions(5, "0123456789");
	m_samplePoolEdit.addListener(this);
	addAndMakeVisible(m_samplePoolEdit);


	for (int i = 0; i < 5; ++i)
	{
		yPos += rowHeight;
		m_perfLabels[i].setBounds(leftX, yPos, leftWidth + rightWidth, ctrlHeight);
		addAndMakeVisible(m_perfLabels[i]);

		yPos += rowHeight - 10;
		m_perfSliders[i].setBounds(leftX + 2, yPos, rightWidth, ctrlHeight);
		m_perfSliders[i].setSliderStyle(Slider2::LinearBar);
		rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, gloPerfKnob1Amt + i));
		m_perfSliders[i].setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_perfSliders[i].setTextValueSuffix(" %");
		m_perfSliders[i].getProperties().set(paramIndexID, gloPerfKnob1Amt + i);
		m_perfSliders[i].addListener(this);
		addAndMakeVisible(m_perfSliders[i]);
	}

	yPos += ctrlHeight + 10;
	setSize(getWidth(), yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool)
	{
		Update();
	});

	Update();
}

HomePanel::~HomePanel()
{
	m_glideTypeCombo.removeListener(this);
	m_glideModeCombo.removeListener(this);
	m_glideTimeSlider.removeListener(this);
	m_glideRateSlider.removeListener(this);
	m_glideRangeSlider.removeListener(this);
	m_playModeCombo.removeListener(this);
	m_legatoCombo.removeListener(this);
	m_uniVoiceCombo.removeListener(this);
	m_uniTuneSlider.removeListener(this);
	m_rndTuneSlider.removeListener(this);
	m_notePriCombo.removeListener(this);
	m_egResetCombo.removeListener(this);
	m_pitchWheelUpSlider.removeListener(this);
	m_pitchWheelDownSlider.removeListener(this);
	m_vtIntensitySlider.removeListener(this);
	m_vtOffsetSlider.removeListener(this);
	m_atIntensitySlider.removeListener(this);
	m_atOffsetSlider.removeListener(this);
	m_expPedalCombo.removeListener(this);
	m_susPedal1Combo.removeListener(this);
	m_susPedal2Combo.removeListener(this);
	m_assign1Combo.removeListener(this);
	m_assign2Combo.removeListener(this);
	m_assignMode1Combo.removeListener(this);
	m_assignMode2Combo.removeListener(this);
	m_patchChainButton.removeListener(this);
	m_samplePoolEdit.removeListener(this);
	for (int i = 0; i < 5; ++i)
	{
		m_perfSliders[i].removeListener(this);
	}
}

void HomePanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void HomePanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	assert(
		(comboBoxThatHasChanged == &m_glideTypeCombo) ||
		(comboBoxThatHasChanged == &m_glideModeCombo) ||
		(comboBoxThatHasChanged == &m_playModeCombo) ||
		(comboBoxThatHasChanged == &m_legatoCombo) ||
		(comboBoxThatHasChanged == &m_uniVoiceCombo) ||
		(comboBoxThatHasChanged == &m_notePriCombo) ||
		(comboBoxThatHasChanged == &m_egResetCombo) ||
		(comboBoxThatHasChanged == &m_expPedalCombo) ||
		(comboBoxThatHasChanged == &m_susPedal1Combo) ||
		(comboBoxThatHasChanged == &m_susPedal2Combo) ||
		(comboBoxThatHasChanged == &m_assign1Combo) ||
		(comboBoxThatHasChanged == &m_assignMode1Combo) ||
		(comboBoxThatHasChanged == &m_assign2Combo) ||
		(comboBoxThatHasChanged == &m_assignMode2Combo));

	// Extract the parameter index from the slider control.
	var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = comboBoxThatHasChanged->getSelectedId() - 1;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void HomePanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_glideTimeSlider) ||
		(slider == &m_glideRateSlider) ||
		(slider == &m_glideRangeSlider) ||
		(slider == &m_uniTuneSlider) ||
		(slider == &m_rndTuneSlider) ||
		(slider == &m_pitchWheelUpSlider) ||
		(slider == &m_pitchWheelDownSlider) ||
		(slider == &m_vtIntensitySlider) ||
		(slider == &m_vtOffsetSlider) ||
		(slider == &m_atIntensitySlider) ||
		(slider == &m_atOffsetSlider) ||
		(slider == &m_perfSliders[0]) ||
		(slider == &m_perfSliders[1]) ||
		(slider == &m_perfSliders[2]) ||
		(slider == &m_perfSliders[3]) ||
		(slider == &m_perfSliders[4]));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void HomePanel::buttonClicked(Button* button)
{
	if (button == &m_patchChainButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, gloPatchChain), m_patchChainButton.getToggleState() ? 1 : 0));
	}
}

void HomePanel::textEditorReturnKeyPressed(TextEditor &ed)
{
	if (&ed == &m_samplePoolEdit)
	{
		int value = std::stoi(ed.getText().toStdString());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, gloSamplePool), value));
	}
}


string GetPerfSourceString(int value);

void HomePanel::Update()
{
	int gldMode = m_pPreset->GetParameterValue(ParamID(m_elemID, gloGldMode));
	m_glideTypeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloGldType)) + 1, NotificationType::dontSendNotification);
	m_glideModeCombo.setSelectedId(gldMode + 1, NotificationType::dontSendNotification);
	m_glideTimeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloGldTime)), NotificationType::dontSendNotification);
	m_glideRateSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloGldRate)), NotificationType::dontSendNotification);
	m_glideRangeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloGldRange)), NotificationType::dontSendNotification);
	m_playModeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloPlayMode)) + 1, NotificationType::dontSendNotification);
	m_legatoCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloLegato)) + 1, NotificationType::dontSendNotification);
	m_uniVoiceCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloUniVoice)) + 1, NotificationType::dontSendNotification);
	m_uniTuneSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloUniTune)), NotificationType::dontSendNotification);
	m_rndTuneSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloRndTune)), NotificationType::dontSendNotification);
	m_pitchWheelUpSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloPitchWheelUpRange)), NotificationType::dontSendNotification);
	m_pitchWheelDownSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloPitchWheelDownRange)), NotificationType::dontSendNotification);
	m_notePriCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloNotePriority)) + 1, NotificationType::dontSendNotification);
	m_egResetCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloEgReset)) + 1, NotificationType::dontSendNotification);
	m_vtIntensitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloVTIntens)), NotificationType::dontSendNotification);
	m_vtOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloVTOffset)), NotificationType::dontSendNotification);
	m_atIntensitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloATIntens)), NotificationType::dontSendNotification);
	m_atOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloATOffset)), NotificationType::dontSendNotification);
	m_expPedalCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloExpPedal)) + 1, NotificationType::dontSendNotification);
	m_susPedal1Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloSusPedal1)) + 1, NotificationType::dontSendNotification);
	m_susPedal2Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloSusPedal2)) + 1, NotificationType::dontSendNotification);
	m_assign1Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloAssignFunc1)) + 1, NotificationType::dontSendNotification);
	m_assignMode1Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloAssignMode1)) + 1, NotificationType::dontSendNotification);
	m_assign2Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloAssignFunc2)) + 1, NotificationType::dontSendNotification);
	m_assignMode2Combo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, gloAssignMode2)) + 1, NotificationType::dontSendNotification);
	m_patchChainButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,gloPatchChain)) != 0,NotificationType::dontSendNotification);
	int samplePool = m_pPreset->GetParameterValue(ParamID(m_elemID, gloSamplePool));
	std::string str = std::to_string(samplePool);
	m_samplePoolEdit.setText(str, NotificationType::dontSendNotification);

	m_glideTimeLabel.setVisible(gldMode != 1);
	m_glideTimeSlider.setVisible(gldMode != 1);
	m_glideRateLabel.setVisible(gldMode == 1);
	m_glideRateSlider.setVisible(gldMode == 1);

	for (int i = 0; i < 5; ++i)
	{
		int value = m_pPreset->GetParameterValue(ParamID(m_elemID, gloPerfKnob1Src + i));
		string label = "P" + std::to_string(i + 1) + ": " + GetPerfSourceString(value);
		m_perfLabels[i].setText(label, NotificationType::dontSendNotification);

		m_perfSliders[i].setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, gloPerfKnob1Amt + i)), NotificationType::dontSendNotification);
	}
}

SystemPanel::SystemPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID)
	:m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	, m_elemID(elemID)
{
	int leftX = 10;
	const int leftWidth = 70;
	int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int spaceBetweenRows = 11;
	const int rowHeight = ctrlHeight + spaceBetweenRows;
	const int vertStart = 10;
	int yPos = vertStart;
	const int columnOffset = 200;
	const int extraSpaceBetweenSystemAndMidi = 20;


	m_transposeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_transposeLabel.setText("Transpose:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_transposeLabel);
	m_transposeSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_transposeSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysTranspose));
	m_transposeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_transposeSlider.getProperties().set(paramIndexID, sysTranspose);
	m_transposeSlider.addListener(this);
	addAndMakeVisible(m_transposeSlider);

	yPos += rowHeight;

	m_fineTuneLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_fineTuneLabel.setText("Tune:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_fineTuneLabel);
	m_fineTuneSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_fineTuneSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysFineTune));
	m_fineTuneSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fineTuneSlider.getProperties().set(paramIndexID, sysFineTune);
	m_fineTuneSlider.addListener(this);
	addAndMakeVisible(m_fineTuneSlider);

	yPos += rowHeight;

	m_loadBPMButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_loadBPMButton.setButtonText("LoadBPM");
	m_loadBPMButton.addListener(this);
	addAndMakeVisible(m_loadBPMButton);

	yPos += rowHeight;

	m_loadOutputsButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_loadOutputsButton.setButtonText("LoadOuts");
	m_loadOutputsButton.addListener(this);
	addAndMakeVisible(m_loadOutputsButton);

	yPos += rowHeight;

	m_randomTuneLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_randomTuneLabel.setText("RndTune:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_randomTuneLabel);
	m_randomTuneSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_randomTuneSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysRndTune));
	m_randomTuneSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_randomTuneSlider.getProperties().set(paramIndexID, sysRndTune);
	m_randomTuneSlider.addListener(this);
	addAndMakeVisible(m_randomTuneSlider);

	yPos += rowHeight;

	m_expPedalPolarityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_expPedalPolarityLabel.setText("ExprPol:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_expPedalPolarityLabel);
	m_expPedalPolarityCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> expPedalPolList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysExpPedalPolarity));;
	for (size_t i = 0; i < expPedalPolList.size(); ++i)
	{
		m_expPedalPolarityCombo.addItem(expPedalPolList[i], i + 1);
	}
	m_expPedalPolarityCombo.getProperties().set(paramIndexID, sysExpPedalPolarity);
	m_expPedalPolarityCombo.addListener(this);
	addAndMakeVisible(m_expPedalPolarityCombo);

	yPos += rowHeight;

	m_susPedal1PolarityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_susPedal1PolarityLabel.setText("Sus1Pol:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_susPedal1PolarityLabel);
	m_susPedal1PolarityCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> sus1PedalPolList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysSusPedal1Polarity));;
	for (size_t i = 0; i < sus1PedalPolList.size(); ++i)
	{
		m_susPedal1PolarityCombo.addItem(sus1PedalPolList[i], i + 1);
	}
	m_susPedal1PolarityCombo.getProperties().set(paramIndexID, sysSusPedal1Polarity);
	m_susPedal1PolarityCombo.addListener(this);
	addAndMakeVisible(m_susPedal1PolarityCombo);

	yPos += rowHeight;

	m_susPedal2PolarityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_susPedal2PolarityLabel.setText("Sus2Pol:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_susPedal2PolarityLabel);
	m_susPedal2PolarityCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> sus2PedalPolList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysSusPedal2Polarity));;
	for (size_t i = 0; i < sus2PedalPolList.size(); ++i)
	{
		m_susPedal2PolarityCombo.addItem(sus2PedalPolList[i], i + 1);
	}
	m_susPedal2PolarityCombo.getProperties().set(paramIndexID, sysSusPedal2Polarity);
	m_susPedal2PolarityCombo.addListener(this);
	addAndMakeVisible(m_susPedal2PolarityCombo);

	yPos += rowHeight;

	m_gloExprLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_gloExprLabel.setText("GloExpr:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_gloExprLabel);
	m_gloExprCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> gloExprList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysExpPedalTargetOverride));;
	for (size_t i = 0; i < gloExprList.size(); ++i)
	{
		m_gloExprCombo.addItem(gloExprList[i], i + 1);
	}
	m_gloExprCombo.getProperties().set(paramIndexID, sysExpPedalTargetOverride);
	m_gloExprCombo.addListener(this);
	addAndMakeVisible(m_gloExprCombo);

	yPos += rowHeight;

	m_gloSusLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_gloSusLabel.setText("GloSust:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_gloSusLabel);
	m_gloSusCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> gloSusList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysSusPedalTargetOverride));;
	for (size_t i = 0; i < gloSusList.size(); ++i)
	{
		m_gloSusCombo.addItem(gloSusList[i], i + 1);
	}
	m_gloSusCombo.getProperties().set(paramIndexID, sysSusPedalTargetOverride);
	m_gloSusCombo.addListener(this);
	addAndMakeVisible(m_gloSusCombo);

	yPos = vertStart;
	leftX += columnOffset;
	rightX += columnOffset;

	m_vtIntensityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_vtIntensityLabel.setText("GVCurve:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_vtIntensityLabel);
	m_vtIntensitySlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_vtIntensitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysVTIntens));
	m_vtIntensitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_vtIntensitySlider.setTextValueSuffix(" %");
	m_vtIntensitySlider.getProperties().set(paramIndexID, sysVTIntens);
	m_vtIntensitySlider.addListener(this);
	addAndMakeVisible(m_vtIntensitySlider);

	yPos += rowHeight;

	m_vtOffsetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_vtOffsetLabel.setText("GVOff:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_vtOffsetLabel);
	m_vtOffsetSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_vtOffsetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysVTOffset));
	m_vtOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_vtOffsetSlider.setTextValueSuffix(" %");
	m_vtOffsetSlider.getProperties().set(paramIndexID, sysVTOffset);
	m_vtOffsetSlider.addListener(this);
	addAndMakeVisible(m_vtOffsetSlider);

	yPos += rowHeight;

	m_atIntensityLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_atIntensityLabel.setText("GACurve:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_atIntensityLabel);
	m_atIntensitySlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_atIntensitySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysATIntens));
	m_atIntensitySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_atIntensitySlider.setTextValueSuffix(" %");
	m_atIntensitySlider.getProperties().set(paramIndexID, sysATIntens);
	m_atIntensitySlider.addListener(this);
	addAndMakeVisible(m_atIntensitySlider);

	yPos += rowHeight;

	m_atOffsetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_atOffsetLabel.setText("GAOff:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_atOffsetLabel);
	m_atOffsetSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_atOffsetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysATOffset));
	m_atOffsetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_atOffsetSlider.setTextValueSuffix(" %");
	m_atOffsetSlider.getProperties().set(paramIndexID, sysATOffset);
	m_atOffsetSlider.addListener(this);
	addAndMakeVisible(m_atOffsetSlider);

	yPos += rowHeight;

	m_loadSampleButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_loadSampleButton.setButtonText("LoadSamp");
	m_loadSampleButton.addListener(this);
	addAndMakeVisible(m_loadSampleButton);

	yPos += rowHeight;

	m_remapATLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_remapATLabel.setText("RemapAT:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_remapATLabel);
	m_remapATCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> remapATList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysRemapAT));;
	for (size_t i = 0; i < remapATList.size(); ++i)
	{
		m_remapATCombo.addItem(remapATList[i], i + 1);
	}
	m_remapATCombo.getProperties().set(paramIndexID, sysRemapAT);
	m_remapATCombo.addListener(this);
	addAndMakeVisible(m_remapATCombo);

	yPos = vertStart;
	leftX += columnOffset + extraSpaceBetweenSystemAndMidi;
	rightX += columnOffset + extraSpaceBetweenSystemAndMidi;

	m_midiChannelLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_midiChannelLabel.setText("MidiChnl:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_midiChannelLabel);
	m_midiChannelSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_midiChannelSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiChannel));
	// change zero-based range values to be 1-based
	m_midiChannelSlider.setRange(rangeInfo.minValue + 1, rangeInfo.maxValue + 1, 1);
	m_midiChannelSlider.getProperties().set(paramIndexID, sysMidiChannel);
	m_midiChannelSlider.addListener(this);
	addAndMakeVisible(m_midiChannelSlider);

	yPos += rowHeight;

	m_programChangeAllowedButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_programChangeAllowedButton.setButtonText("ProgChng");
	m_programChangeAllowedButton.addListener(this);
	addAndMakeVisible(m_programChangeAllowedButton);

	yPos += rowHeight;

	m_sendArpButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_sendArpButton.setButtonText("SendArp");
	m_sendArpButton.addListener(this);
	addAndMakeVisible(m_sendArpButton);

	yPos += rowHeight;

	m_omniButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_omniButton.setButtonText("Omni");
	m_omniButton.addListener(this);
	addAndMakeVisible(m_omniButton);

	yPos += rowHeight;

	m_localButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_localButton.setButtonText("Local");
	m_localButton.addListener(this);
	addAndMakeVisible(m_localButton);

	yPos += rowHeight;

	m_transmitSysExButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_transmitSysExButton.setButtonText("TxSysEx");
	m_transmitSysExButton.addListener(this);
	addAndMakeVisible(m_transmitSysExButton);

	yPos += rowHeight;

	m_clockSourceLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_clockSourceLabel.setText("ClkSrc:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_clockSourceLabel);
	m_clockSourceCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> clockSourceList = m_pPreset->GetParameterListText(ParamID(m_elemID, sysMidiClockSource));;
	for (size_t i = 0; i < clockSourceList.size(); ++i)
	{
		m_clockSourceCombo.addItem(clockSourceList[i], i + 1);
	}
	m_clockSourceCombo.getProperties().set(paramIndexID, sysMidiClockSource);
	m_clockSourceCombo.addListener(this);
	addAndMakeVisible(m_clockSourceCombo);

	yPos += rowHeight;

	m_volumeButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_volumeButton.setButtonText("Volume");
	m_volumeButton.addListener(this);
	addAndMakeVisible(m_volumeButton);

	yPos += rowHeight;

	m_realtimeButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_realtimeButton.setButtonText("Realtime");
	m_realtimeButton.addListener(this);
	addAndMakeVisible(m_realtimeButton);

	yPos += rowHeight;

	m_polychainButton.setBounds(leftX, yPos, rightWidth, ctrlHeight);
	m_polychainButton.setButtonText("Polychn");
	m_polychainButton.addListener(this);
	addAndMakeVisible(m_polychainButton);

	yPos = vertStart;
	leftX += columnOffset;
	rightX += columnOffset;

	m_deviceIDLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_deviceIDLabel.setText("Device ID:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_deviceIDLabel);
	m_deviceIDSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_deviceIDSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysDeviceID));
	m_deviceIDSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_deviceIDSlider.getProperties().set(paramIndexID, sysDeviceID);
	m_deviceIDSlider.addListener(this);
	addAndMakeVisible(m_deviceIDSlider);

	yPos += rowHeight;

	m_cc1Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_cc1Label.setText("CC1:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_cc1Label);
	m_cc1Slider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_cc1Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiCC1));
	m_cc1Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cc1Slider.getProperties().set(paramIndexID, sysMidiCC1);
	m_cc1Slider.addListener(this);
	addAndMakeVisible(m_cc1Slider);

	yPos += rowHeight;

	m_cc2Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_cc2Label.setText("CC2:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_cc2Label);
	m_cc2Slider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_cc2Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiCC2));
	m_cc2Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cc2Slider.getProperties().set(paramIndexID, sysMidiCC2);
	m_cc2Slider.addListener(this);
	addAndMakeVisible(m_cc2Slider);

	yPos += rowHeight;

	m_cc3Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_cc3Label.setText("CC1:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_cc3Label);
	m_cc3Slider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_cc3Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiCC3));
	m_cc3Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cc3Slider.getProperties().set(paramIndexID, sysMidiCC3);
	m_cc3Slider.addListener(this);
	addAndMakeVisible(m_cc3Slider);

	yPos += rowHeight;

	m_cc4Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_cc4Label.setText("CC4:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_cc4Label);
	m_cc4Slider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_cc4Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiCC4));
	m_cc4Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cc4Slider.getProperties().set(paramIndexID, sysMidiCC4);
	m_cc4Slider.addListener(this);
	addAndMakeVisible(m_cc4Slider);

	yPos += rowHeight;

	m_cc5Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_cc5Label.setText("CC5:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_cc5Label);
	m_cc5Slider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_cc5Slider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, sysMidiCC5));
	m_cc5Slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_cc5Slider.getProperties().set(paramIndexID, sysMidiCC5);
	m_cc5Slider.addListener(this);
	addAndMakeVisible(m_cc5Slider);


	yPos += ctrlHeight + 10;
	setSize(getWidth(), yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool)
	{
		Update();
	});
	Update();
}


SystemPanel::~SystemPanel()
{
	m_transposeSlider.removeListener(this);
	m_fineTuneSlider.removeListener(this);
	m_loadBPMButton.removeListener(this);
	m_loadOutputsButton.removeListener(this);
	m_randomTuneSlider.removeListener(this);
	m_expPedalPolarityCombo.removeListener(this);
	m_susPedal1PolarityCombo.removeListener(this);
	m_susPedal2PolarityCombo.removeListener(this);
	m_gloExprCombo.removeListener(this);
	m_gloSusCombo.removeListener(this);
	m_vtIntensitySlider.removeListener(this);
	m_vtOffsetSlider.removeListener(this);
	m_atIntensitySlider.removeListener(this);
	m_atOffsetSlider.removeListener(this);
	m_loadSampleButton.removeListener(this);
	m_remapATCombo.removeListener(this);
	m_midiChannelSlider.removeListener(this);
	m_programChangeAllowedButton.removeListener(this);
	m_sendArpButton.removeListener(this);
	m_omniButton.removeListener(this);
	m_localButton.removeListener(this);
	m_transmitSysExButton.removeListener(this);
	m_clockSourceCombo.removeListener(this);
	m_volumeButton.removeListener(this);
	m_realtimeButton.removeListener(this);
	m_polychainButton.removeListener(this);
	m_deviceIDSlider.removeListener(this);
	m_cc1Slider.removeListener(this);
	m_cc2Slider.removeListener(this);
	m_cc3Slider.removeListener(this);
	m_cc4Slider.removeListener(this);
	m_cc5Slider.removeListener(this);


}

void SystemPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	assert(
		(comboBoxThatHasChanged == &m_expPedalPolarityCombo)  ||
		(comboBoxThatHasChanged == &m_susPedal1PolarityCombo) ||
		(comboBoxThatHasChanged == &m_susPedal2PolarityCombo) ||
		(comboBoxThatHasChanged == &m_gloExprCombo) ||
		(comboBoxThatHasChanged == &m_gloSusCombo) ||
		(comboBoxThatHasChanged == &m_clockSourceCombo) ||
		(comboBoxThatHasChanged == &m_remapATCombo)
	);

	// Extract the parameter index from the slider control.
	var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = comboBoxThatHasChanged->getSelectedId() - 1;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void SystemPanel::sliderValueChanged(Slider2* pSlider)
{
	assert(
		(pSlider == &m_transposeSlider) ||
		(pSlider == &m_fineTuneSlider) ||
		(pSlider == &m_randomTuneSlider) ||
		(pSlider == &m_vtIntensitySlider) ||
		(pSlider == &m_vtOffsetSlider) ||
		(pSlider == &m_atIntensitySlider) ||
		(pSlider == &m_atOffsetSlider) ||
		(pSlider == &m_midiChannelSlider) ||
		(pSlider == &m_deviceIDSlider) ||
		(pSlider == &m_cc1Slider) ||
		(pSlider == &m_cc2Slider) ||
		(pSlider == &m_cc3Slider) ||
		(pSlider == &m_cc4Slider) ||
		(pSlider == &m_cc5Slider)
	);

	// Extract the parameter index from the slider control.
	var* pVar = pSlider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(pSlider->getValue());
		if (pSlider == &m_midiChannelSlider)
		{
			value -= 1;	// account for midi channel starting at one. The actual value in the preset is zero-based
		}
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void SystemPanel::buttonClicked(Button* button)
{
	if (button == &m_loadBPMButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysBPMOverride), m_loadBPMButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_loadOutputsButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysOutputsOverride), m_loadOutputsButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_programChangeAllowedButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysProgramChangeAllowed), m_programChangeAllowedButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_sendArpButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysSendArp), m_sendArpButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_omniButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysOmni), m_omniButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_localButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysLocal), m_localButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_transmitSysExButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysTransmitSysEx), m_transmitSysExButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_volumeButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysMidiVolumeAllowed), m_volumeButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_realtimeButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysMidiRealTime), m_realtimeButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_polychainButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysPolychain), m_polychainButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_loadSampleButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, sysLoadSample), m_loadSampleButton.getToggleState() ? 1 : 0));
	}
}

void SystemPanel::Update()
{
	m_transposeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysTranspose)), NotificationType::dontSendNotification);
	m_fineTuneSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysFineTune)), NotificationType::dontSendNotification);
	m_loadBPMButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysBPMOverride)) != 0, NotificationType::dontSendNotification);
	m_loadOutputsButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysOutputsOverride)) != 0, NotificationType::dontSendNotification);
	m_randomTuneSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysRndTune)), NotificationType::dontSendNotification);
	m_expPedalPolarityCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysExpPedalPolarity)) + 1, NotificationType::dontSendNotification);
	m_susPedal1PolarityCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysSusPedal1Polarity)) + 1, NotificationType::dontSendNotification);
	m_susPedal2PolarityCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysSusPedal2Polarity)) + 1, NotificationType::dontSendNotification);
	m_gloExprCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysExpPedalTargetOverride)) + 1, NotificationType::dontSendNotification);
	m_gloSusCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysSusPedalTargetOverride)) + 1, NotificationType::dontSendNotification);
	m_vtIntensitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysVTIntens)), NotificationType::dontSendNotification);
	m_vtOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysVTOffset)), NotificationType::dontSendNotification);
	m_atIntensitySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysATIntens)), NotificationType::dontSendNotification);
	m_atOffsetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysATOffset)), NotificationType::dontSendNotification);
	m_loadSampleButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysLoadSample)) != 0, NotificationType::dontSendNotification);
	m_midiChannelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiChannel)), NotificationType::dontSendNotification);
	m_programChangeAllowedButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysProgramChangeAllowed)) != 0, NotificationType::dontSendNotification);
	m_sendArpButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysSendArp)) != 0, NotificationType::dontSendNotification);
	m_omniButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysOmni)) != 0, NotificationType::dontSendNotification);
	m_localButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysLocal)) != 0, NotificationType::dontSendNotification);
	m_transmitSysExButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysTransmitSysEx)) != 0, NotificationType::dontSendNotification);
	m_clockSourceCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiClockSource)) + 1, NotificationType::dontSendNotification);
	m_volumeButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiVolumeAllowed)) != 0, NotificationType::dontSendNotification);
	m_realtimeButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiRealTime)) != 0, NotificationType::dontSendNotification);
	m_polychainButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID, sysPolychain)) != 0, NotificationType::dontSendNotification);
	m_deviceIDSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysDeviceID)), NotificationType::dontSendNotification);
	m_cc1Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiCC1)), NotificationType::dontSendNotification);
	m_cc2Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiCC2)), NotificationType::dontSendNotification);
	m_cc3Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiCC3)), NotificationType::dontSendNotification);
	m_cc4Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiCC4)), NotificationType::dontSendNotification);
	m_cc5Slider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, sysMidiCC5)), NotificationType::dontSendNotification);
	m_remapATCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, sysRemapAT)) + 1, NotificationType::dontSendNotification);

}


struct PerfSource
{
	int index;
	char const * name;
};

vector<PerfSource> perfSourceList
{
	{ 0x00AF, "Osc1 Shape" },
	{ 0x009F, "Osc1 Coarse" },
	{ 0x00A7, "Osc1 Fine" },
	{ 0x00B7, "Osc1 Mod1 Amt" },
	{ 0x00C3, "Osc1 Mod2 Amt" },
	{ 0x00BD, "Osc1 Mod3 Amt" },
	{ 0x00C9, "Osc1 Mod4 Amt" },
	{ 0x026F, "Osc1 Ctl1 Str" },
	{ 0x0273, "Osc1 Ctl2 Str" },
	{ 0x0277, "Osc1 Ctl3 Str" },
	{ 0x027B, "Osc1 Ctl4 Str" },
	{ 0x03FD, "Osc1 Mod1 Amt Pitch" },
	{ 0x040A, "Osc1 Mod2 Amt Pitch" },
	{ 0x040E, "Osc1 Mod3 Amt Pitch" },
	{ 0x0412, "Osc1 Mod4 Amt Pitch" },

	{ 0x00B0, "Osc2 Shape" },
	{ 0x00A0, "Osc2 Coarse" },
	{ 0x00A8, "Osc2 Fine" },
	{ 0x00B8, "Osc2 Mod1 Amt" },
	{ 0x00C4, "Osc2 Mod2 Amt" },
	{ 0x00BE, "Osc2 Mod3 Amt" },
	{ 0x00CA, "Osc2 Mod4 Amt" },
	{ 0x0270, "Osc2 Ctl1 Str" },
	{ 0x0274, "Osc2 Ctl2 Str" },
	{ 0x0278, "Osc2 Ctl3 Str" },
	{ 0x027C, "Osc2 Ctl4 Str" },
	{ 0x03FE, "Osc2 Mod1 Amt Pitch" },
	{ 0x040B, "Osc2 Mod2 Amt Pitch" },
	{ 0x040F, "Osc2 Mod3 Amt Pitch" },
	{ 0x0413, "Osc2 Mod4 Amt Pitch" },

	{ 0x00B1, "Osc3 Shape" },
	{ 0x00A1, "Osc3 Coarse" },
	{ 0x00A9, "Osc3 Fine" },
	{ 0x00B9, "Osc3 Mod1 Amt" },
	{ 0x00C5, "Osc3 Mod2 Amt" },
	{ 0x00BF, "Osc3 Mod3 Amt" },
	{ 0x00CB, "Osc3 Mod4 Amt" },
	{ 0x0271, "Osc3 Ctl1 Str" },
	{ 0x0275, "Osc3 Ctl2 Str" },
	{ 0x0279, "Osc3 Ctl3 Str" },
	{ 0x027D, "Osc3 Ctl4 Str" },
	{ 0x03FF, "Osc3 Mod1 Amt Pitch" },
	{ 0x040C, "Osc3 Mod2 Amt Pitch" },
	{ 0x0410, "Osc3 Mod3 Amt Pitch" },
	{ 0x0414, "Osc3 Mod4 Amt Pitch" },

	{ 0x00B2, "Osc4 Shape" },
	{ 0x00A2, "Osc4 Coarse" },
	{ 0x00AA, "Osc4 Fine" },
	{ 0x00BA, "Osc4 Mod1 Amt" },
	{ 0x00C6, "Osc4 Mod2 Amt" },
	{ 0x00C0, "Osc4 Mod3 Amt" },
	{ 0x00CC, "Osc4 Mod4 Amt" },
	{ 0x0272, "Osc4 Ctl1 Str" },
	{ 0x0276, "Osc4 Ctl2 Str" },
	{ 0x027A, "Osc4 Ctl3 Str" },
	{ 0x027E, "Osc4 Ctl4 Str" },
	{ 0x0400, "Osc4 Mod1 Amt Pitch" },
	{ 0x040D, "Osc4 Mod2 Amt Pitch" },
	{ 0x0411, "Osc4 Mod3 Amt Pitch" },
	{ 0x0415, "Osc4 Mod4 Amt Pitch" },

	{ 0x00DF, "Rot1 Freq" },
	{ 0x00AB, "Rot1 Fine" },
	{ 0x0222, "Rot1 XFade" },
	{ 0x0440, "Rot1 Phase" },
	{ 0x00A5, "Rot1 Input1 Lvl" },
	{ 0x00AD, "Rot1 Input2 Lvl" },
	{ 0x00C1, "Rot1 Input3 Lvl" },
	{ 0x00CD, "Rot1 Input4 Lvl" },
	{ 0x00BB, "Rot1 Mod1 Amt" },
	{ 0x00C7, "Rot1 Mod2 Amt" },
	{ 0x0291, "Rot1 Mod3 Amt" },
	{ 0x02AF, "Rot1 Mod4 Amt" },
	{ 0x029D, "Rot1 Ctl1 Str" },
	{ 0x029F, "Rot1 Ctl2 Str" },
	{ 0x02A1, "Rot1 Ctl3 Str" },
	{ 0x02A3, "Rot1 Ctl4 Str" },
	{ 0x0434, "Rot1 Mod1 Amt Pitch" },
	{ 0x0436, "Rot1 Mod2 Amt Pitch" },
	{ 0x0438, "Rot1 Mod3 Amt Pitch" },
	{ 0x043A, "Rot1 Mod4 Amt Pitch" },

	{ 0x00E0, "Rot2 Freq" },
	{ 0x00AC, "Rot2 Fine" },
	{ 0x0223, "Rot2 XFade" },
	{ 0x0441, "Rot2 Phase" },
	{ 0x00A6, "Rot2 Input1 Lvl" },
	{ 0x00AE, "Rot2 Input2 Lvl" },
	{ 0x00C2, "Rot2 Input3 Lvl" },
	{ 0x00CE, "Rot2 Input4 Lvl" },
	{ 0x00BC, "Rot2 Mod1 Amt" },
	{ 0x00C8, "Rot2 Mod2 Amt" },
	{ 0x0292, "Rot2 Mod3 Amt" },
	{ 0x02B0, "Rot2 Mod4 Amt" },
	{ 0x029E, "Rot2 Ctl1 Str" },
	{ 0x02A0, "Rot2 Ctl2 Str" },
	{ 0x02A2, "Rot2 Ctl3 Str" },
	{ 0x02A4, "Rot2 Ctl4 Str" },
	{ 0x0435, "Rot2 Mod1 Amt Pitch" },
	{ 0x0437, "Rot2 Mod2 Amt Pitch" },
	{ 0x0439, "Rot2 Mod3 Amt Pitch" },
	{ 0x043B, "Rot2 Mod4 Amt Pitch" },

	{ 0x0143, "Mix1 Input1 Lvl" },
	{ 0x0147, "Mix1 Input2 Lvl" },
	{ 0x014B, "Mix1 Input3 Lvl" },
	{ 0x014F, "Mix1 Input4 Lvl" },
	{ 0x0153, "Mix1 Output Lvl" },
	{ 0x0157, "Mix1 In1 Mod Amt" },
	{ 0x015B, "Mix1 In2 Mod Amt" },
	{ 0x015F, "Mix1 In3 Mod Amt" },
	{ 0x0163, "Mix1 In4 Mod Amt" },
	{ 0x042D, "Mix1 Out Mod Amt" },

	{ 0x0144, "Mix2 Input1 Lvl" },
	{ 0x0148, "Mix2 Input2 Lvl" },
	{ 0x014C, "Mix2 Input3 Lvl" },
	{ 0x0150, "Mix2 Input4 Lvl" },
	{ 0x0154, "Mix2 Output Lvl" },
	{ 0x0158, "Mix2 In1 Mod Amt" },
	{ 0x015C, "Mix2 In2 Mod Amt" },
	{ 0x0160, "Mix2 In3 Mod Amt" },
	{ 0x0164, "Mix2 In4 Mod Amt" },
	{ 0x042F, "Mix2 Out Mod Amt" },

	{ 0x0145, "Mix3 Input1 Lvl" },
	{ 0x0149, "Mix3 Input2 Lvl" },
	{ 0x014D, "Mix3 Input3 Lvl" },
	{ 0x0151, "Mix3 Input4 Lvl" },
	{ 0x0155, "Mix3 Output Lvl" },
	{ 0x0159, "Mix3 In1 Mod Amt" },
	{ 0x015D, "Mix3 In2 Mod Amt" },
	{ 0x0161, "Mix3 In3 Mod Amt" },
	{ 0x0165, "Mix3 In4 Mod Amt" },
	{ 0x0431, "Mix3 Out Mod Amt" },

	{ 0x0146, "Mix4 Input1 Lvl" },
	{ 0x014A, "Mix4 Input2 Lvl" },
	{ 0x014E, "Mix4 Input3 Lvl" },
	{ 0x0152, "Mix4 Input4 Lvl" },
	{ 0x0156, "Mix4 Output Lvl" },
	{ 0x015A, "Mix4 In1 Mod Amt" },
	{ 0x015E, "Mix4 In2 Mod Amt" },
	{ 0x0162, "Mix4 In3 Mod Amt" },
	{ 0x0166, "Mix4 In4 Mod Amt" },
	{ 0x0433, "Mix4 Out Mod Amt" },

	{ 0x0344, "Ifx1 Value" },
	{ 0x0346, "Ifx1 Mod Amt" },
	{ 0x0345, "Ifx1 Ctrl Str" },

	{ 0x034B, "Ifx2 Value" },
	{ 0x034D, "Ifx2 Mod Amt" },
	{ 0x034C, "Ifx2 Ctrl Str" },

	{ 0x0352, "Ifx3 Value" },
	{ 0x0354, "Ifx3 Mod Amt" },
	{ 0x0353, "Ifx3 Ctrl Str" },

	{ 0x0359, "Ifx4 Value" },
	{ 0x035B, "Ifx4 Mod Amt" },
	{ 0x035A, "Ifx4 Ctrl Str" },

	{ 0x0228, "Fil1 MM1 Mode" },
	{ 0x0230, "Fil1 Comb Mode" },
	{ 0x0193, "Fil1 Cutoff" },
	{ 0x0197, "Fil1 Res" },
	{ 0x01A7, "Fil1 Key Tracking" },
	{ 0x01AB, "Fil1 Key Center" },
	{ 0x01AF, "Fil1 Damping" },
	{ 0x04AC, "Fil1 XFade" },
	{ 0x019B, "Fil1 Mod1 Amt" },
	{ 0x019F, "Fil1 Mod2 Amt" },
	{ 0x01A3, "Fil1 Mod3 Amt" },
	{ 0x02FD, "Fil1 Mod4 Amt" },
	{ 0x02D9, "Fil1 Ctl1 Str" },
	{ 0x02DA, "Fil1 Ctl2 Str" },
	{ 0x02DB, "Fil1 Ctl3 Str" },
	{ 0x02DC, "Fil1 Ctl4 Str" },
	{ 0x03F8, "Fil1 Mod1 Amt Cutoff" },
	{ 0x0416, "Fil1 Mod2 Amt Cutoff" },
	{ 0x04A1, "Fil1 Mod3 Amt Cutoff" },
	{ 0x041E, "Fil1 Mod4 Amt Cutoff" },

	{ 0x0229, "Fil2 MM1 Mode" },
	{ 0x0231, "Fil2 Comb Mode" },
	{ 0x0194, "Fil2 Cutoff" },
	{ 0x0198, "Fil2 Res" },
	{ 0x01A8, "Fil2 Key Tracking" },
	{ 0x01AC, "Fil2 Key Center" },
	{ 0x01B0, "Fil2 Damping" },
	{ 0x04AD, "Fil2 XFade" },
	{ 0x019C, "Fil2 Mod1 Amt" },
	{ 0x01A0, "Fil2 Mod2 Amt" },
	{ 0x01A4, "Fil2 Mod3 Amt" },
	{ 0x02FE, "Fil2 Mod4 Amt" },
	{ 0x02DE, "Fil2 Ctl1 Str" },
	{ 0x02DF, "Fil2 Ctl2 Str" },
	{ 0x02E0, "Fil2 Ctl3 Str" },
	{ 0x02E1, "Fil2 Ctl4 Str" },
	{ 0x03F9, "Fil2 Mod1 Amt Cutoff" },
	{ 0x0417, "Fil2 Mod2 Amt Cutoff" },
	{ 0x04A2, "Fil2 Mod3 Amt Cutoff" },
	{ 0x041F, "Fil2 Mod4 Amt Cutoff" },

	{ 0x022A, "Fil3 MM1 Mode" },
	{ 0x0232, "Fil3 Comb Mode" },
	{ 0x0195, "Fil3 Cutoff" },
	{ 0x0199, "Fil3 Res" },
	{ 0x01A9, "Fil3 Key Tracking" },
	{ 0x01AD, "Fil3 Key Center" },
	{ 0x01B1, "Fil3 Damping" },
	{ 0x04AE, "Fil3 XFade" },
	{ 0x019D, "Fil3 Mod1 Amt" },
	{ 0x01A1, "Fil3 Mod2 Amt" },
	{ 0x01A5, "Fil3 Mod3 Amt" },
	{ 0x02FF, "Fil3 Mod4 Amt" },
	{ 0x02E2, "Fil3 Ctl1 Str" },
	{ 0x02E3, "Fil3 Ctl2 Str" },
	{ 0x02E4, "Fil3 Ctl3 Str" },
	{ 0x02E5, "Fil3 Ctl4 Str" },
	{ 0x03FA, "Fil3 Mod1 Amt Cutoff" },
	{ 0x0418, "Fil3 Mod2 Amt Cutoff" },
	{ 0x04A3, "Fil3 Mod3 Amt Cutoff" },
	{ 0x0420, "Fil3 Mod4 Amt Cutoff" },

	{ 0x022B, "Fil4 MM1 Mode" },
	{ 0x0233, "Fil4 Comb Mode" },
	{ 0x0196, "Fil4 Cutoff" },
	{ 0x019A, "Fil4 Res" },
	{ 0x01AA, "Fil4 Key Tracking" },
	{ 0x01AE, "Fil4 Key Center" },
	{ 0x01B2, "Fil4 Damping" },
	{ 0x04AF, "Fil4 XFade" },
	{ 0x019E, "Fil4 Mod1 Amt" },
	{ 0x01A2, "Fil4 Mod2 Amt" },
	{ 0x01A6, "Fil4 Mod3 Amt" },
	{ 0x0300, "Fil4 Mod4 Amt" },
	{ 0x02E6, "Fil4 Ctl1 Str" },
	{ 0x02E7, "Fil4 Ctl2 Str" },
	{ 0x02E8, "Fil4 Ctl3 Str" },
	{ 0x02E9, "Fil4 Ctl4 Str" },
	{ 0x03FB, "Fil4 Mod1 Amt Cutoff" },
	{ 0x0419, "Fil4 Mod2 Amt Cutoff" },
	{ 0x04A4, "Fil4 Mod3 Amt Cutoff" },
	{ 0x0421, "Fil4 Mod4 Amt Cutoff" },

	{ 0x02B5, "Vca1 Boost" },
	{ 0x033F, "Vca1 Level" },
	{ 0x0173, "Vca1 Pan Position" },
	{ 0x02BD, "Vca1 Mod Level" },
	{ 0x02C1, "Vca1 Mod Pan" },

	{ 0x02B6, "Vca2 Boost" },
	{ 0x0340, "Vca2 Level" },
	{ 0x0174, "Vca2 Pan Position" },
	{ 0x02BE, "Vca2 Mod Level" },
	{ 0x02C2, "Vca2 Mod Pan" },

	{ 0x02B7, "Vca3 Boost" },
	{ 0x0341, "Vca3 Level" },
	{ 0x0175, "Vca3 Pan Position" },
	{ 0x02BF, "Vca3 Mod Level" },
	{ 0x02C3, "Vca3 Mod Pan" },

	{ 0x02B8, "Vca4 Boost" },
	{ 0x0342, "Vca4 Level" },
	{ 0x0176, "Vca4 Pan Position" },
	{ 0x02C0, "Vca4 Mod Level" },
	{ 0x02C4, "Vca4 Mod Pan" },

	{ 0x00E6, "Lfo1 Rate" },
	{ 0x00F5, "Lfo1 Phase" },
	{ 0x00FA, "Lfo1 Delay Start" },
	{ 0x00FF, "Lfo1 Fade In" },
	{ 0x010E, "Lfo1 Fade Out" },
	{ 0x0338, "Lfo1 Level" },
	{ 0x0104, "Lfo1 Mod1 Amt" },
	{ 0x0109, "Lfo1 Mod2 Amt" },
	{ 0x0333, "Lfo1 Mod3 Amt" },
	{ 0x0310, "Lfo1 Ctl1 Str" },
	{ 0x0315, "Lfo1 Ctl2 Str" },
	{ 0x031A, "Lfo1 Ctl3 Str" },
	{ 0x0401, "Lfo1 Mod1 Amt Rate" },
	{ 0x0422, "Lfo1 Mod2 Amt Rate" },
	{ 0x0427, "Lfo1 Mod3 Amt Rate" },

	{ 0x00E7, "Lfo2 Rate" },
	{ 0x00F6, "Lfo2 Phase" },
	{ 0x00FB, "Lfo2 Delay Start" },
	{ 0x0100, "Lfo2 Fade In" },
	{ 0x010F, "Lfo2 Fade Out" },
	{ 0x0339, "Lfo2 Level" },
	{ 0x0105, "Lfo2 Mod1 Amt" },
	{ 0x010A, "Lfo2 Mod2 Amt" },
	{ 0x0334, "Lfo2 Mod3 Amt" },
	{ 0x0311, "Lfo2 Ctl1 Str" },
	{ 0x0316, "Lfo2 Ctl2 Str" },
	{ 0x031B, "Lfo2 Ctl3 Str" },
	{ 0x0402, "Lfo2 Mod1 Amt Rate" },
	{ 0x0423, "Lfo2 Mod2 Amt Rate" },
	{ 0x0428, "Lfo2 Mod3 Amt Rate" },

	{ 0x00E8, "Lfo3 Rate" },
	{ 0x00F7, "Lfo3 Phase" },
	{ 0x00FC, "Lfo3 Delay Start" },
	{ 0x0101, "Lfo3 Fade In" },
	{ 0x0110, "Lfo3 Fade Out" },
	{ 0x033A, "Lfo3 Level" },
	{ 0x0106, "Lfo3 Mod1 Amt" },
	{ 0x010B, "Lfo3 Mod2 Amt" },
	{ 0x0335, "Lfo3 Mod3 Amt" },
	{ 0x0312, "Lfo3 Ctl1 Str" },
	{ 0x0317, "Lfo3 Ctl2 Str" },
	{ 0x031C, "Lfo3 Ctl3 Str" },
	{ 0x0403, "Lfo3 Mod1 Amt Rate" },
	{ 0x0424, "Lfo3 Mod2 Amt Rate" },
	{ 0x0429, "Lfo3 Mod3 Amt Rate" },

	{ 0x00E9, "Lfo4 Rate" },
	{ 0x00F8, "Lfo4 Phase" },
	{ 0x00FD, "Lfo4 Delay Start" },
	{ 0x0102, "Lfo4 Fade In" },
	{ 0x0111, "Lfo4 Fade Out" },
	{ 0x033B, "Lfo4 Level" },
	{ 0x0107, "Lfo4 Mod1 Amt" },
	{ 0x010C, "Lfo4 Mod2 Amt" },
	{ 0x0336, "Lfo4 Mod3 Amt" },
	{ 0x0313, "Lfo4 Ctl1 Str" },
	{ 0x0318, "Lfo4 Ctl2 Str" },
	{ 0x031D, "Lfo4 Ctl3 Str" },
	{ 0x0404, "Lfo4 Mod1 Amt Rate" },
	{ 0x0425, "Lfo4 Mod2 Amt Rate" },
	{ 0x042A, "Lfo4 Mod3 Amt Rate" },

	{ 0x00EA, "Lfo5 Rate" },
	{ 0x00F9, "Lfo5 Phase" },
	{ 0x00FE, "Lfo5 Delay Start" },
	{ 0x0103, "Lfo5 Fade In" },
	{ 0x0112, "Lfo5 Fade Out" },
	{ 0x033C, "Lfo5 Level" },
	{ 0x0108, "Lfo5 Mod1 Amt" },
	{ 0x010E, "Lfo5 Mod2 Amt" },
	{ 0x0337, "Lfo5 Mod3 Amt" },
	{ 0x0314, "Lfo5 Ctl1 Str" },
	{ 0x0319, "Lfo5 Ctl2 Str" },
	{ 0x031E, "Lfo5 Ctl3 Str" },
	{ 0x0405, "Lfo5 Mod1 Amt Rate" },
	{ 0x0426, "Lfo5 Mod2 Amt Rate" },
	{ 0x042B, "Lfo5 Mod3 Amt Rate" },
	{ 0x042B, "Lfo5 Mod3 Amt Rate" },

	{ 0x03F2, "Env1 Delay" },
	{ 0x01BB, "Env1 Attack" },
	{ 0x01C7, "Env1 Decay" },
	{ 0x01D3, "Env1 Sustain" },
	{ 0x01DF, "Env1 Release" },
	{ 0x01C4, "Env1 Attack Slope" },
	{ 0x01D0, "Env1 Decay Slope" },
	{ 0x01DC, "Env1 Sustain Slope" },
	{ 0x01E8, "Env1 Release Slope" },
	{ 0x01C1, "Env1 Attack Mod" },
	{ 0x01CD, "Env1 Decay Mod" },
	{ 0x01D9, "Env1 Sustain Mod" },
	{ 0x01E5, "Env1 Release Mod" },
	{ 0x01EB, "Env1 Velocity" },

	{ 0x03F3, "Env2 Delay" },
	{ 0x01EE, "Env2 Attack" },
	{ 0x01FA, "Env2 Decay" },
	{ 0x0206, "Env2 Sustain" },
	{ 0x0212, "Env2 Release" },
	{ 0x01F7, "Env2 Attack Slope" },
	{ 0x0203, "Env2 Decay Slope" },
	{ 0x020F, "Env2 Sustain Slope" },
	{ 0x021B, "Env2 Release Slope" },
	{ 0x01F4, "Env2 Attack Mod" },
	{ 0x0200, "Env2 Decay Mod" },
	{ 0x020C, "Env2 Sustain Mod" },
	{ 0x0218, "Env2 Release Mod" },
	{ 0x021E, "Env2 Velocity" },

	{ 0x03F4, "Env3 Delay" },
	{ 0x01BC, "Env3 Attack" },
	{ 0x01C8, "Env3 Decay" },
	{ 0x01D4, "Env3 Sustain" },
	{ 0x01E0, "Env3 Release" },
	{ 0x01C5, "Env3 Attack Slope" },
	{ 0x01D1, "Env3 Decay Slope" },
	{ 0x01DD, "Env3 Sustain Slope" },
	{ 0x01E9, "Env3 Release Slope" },
	{ 0x01C2, "Env3 Attack Mod" },
	{ 0x01CE, "Env3 Decay Mod" },
	{ 0x01DA, "Env3 Sustain Mod" },
	{ 0x01E6, "Env3 Release Mod" },
	{ 0x01EC, "Env3 Velocity" },

	{ 0x03F5, "Env4 Delay" },
	{ 0x01EF, "Env4 Attack" },
	{ 0x01FB, "Env4 Decay" },
	{ 0x0207, "Env4 Sustain" },
	{ 0x0213, "Env4 Release" },
	{ 0x01F8, "Env4 Attack Slope" },
	{ 0x0204, "Env4 Decay Slope" },
	{ 0x0210, "Env4 Sustain Slope" },
	{ 0x021C, "Env4 Release Slope" },
	{ 0x01F5, "Env4 Attack Mod" },
	{ 0x0201, "Env4 Decay Mod" },
	{ 0x020D, "Env4 Sustain Mod" },
	{ 0x0219, "Env4 Release Mod" },
	{ 0x021F, "Env4 Velocity" },

	{ 0x03F6, "Env5 Delay" },
	{ 0x01BD, "Env5 Attack" },
	{ 0x01C9, "Env5 Decay" },
	{ 0x01D5, "Env5 Sustain" },
	{ 0x01E1, "Env5 Release" },
	{ 0x01C6, "Env5 Attack Slope" },
	{ 0x01D2, "Env5 Decay Slope" },
	{ 0x01DE, "Env5 Sustain Slope" },
	{ 0x01EA, "Env5 Release Slope" },
	{ 0x01C3, "Env5 Attack Mod" },
	{ 0x01CF, "Env5 Decay Mod" },
	{ 0x01DB, "Env5 Sustain Mod" },
	{ 0x01E7, "Env5 Release Mod" },
	{ 0x01ED, "Env5 Velocity" },

	{ 0x03F7, "Env6 Delay" },
	{ 0x01F0, "Env6 Attack" },
	{ 0x01FC, "Env6 Decay" },
	{ 0x0208, "Env6 Sustain" },
	{ 0x0214, "Env6 Release" },
	{ 0x01F9, "Env6 Attack Slope" },
	{ 0x0205, "Env6 Decay Slope" },
	{ 0x0211, "Env6 Sustain Slope" },
	{ 0x021D, "Env6 Release Slope" },
	{ 0x01F6, "Env6 Attack Mod" },
	{ 0x0202, "Env6 Decay Mod" },
	{ 0x020E, "Env6 Sustain Mod" },
	{ 0x021A, "Env6 Release Mod" },
	{ 0x0220, "Env6 Velocity" },

	{ 0x03C0, "Glide Range" },
	{ 0x03BE, "Glide Time" },
	{ 0x4E62, "Unitune" },
	{ 0x4E5A, "Transpose" },
	{ 0x03BA, "PW Up" },
	{ 0x03B7, "VT Intensity" },
	{ 0x03B8, "VT Offset" },
	{ 0x4E5D, "BPM" },
	{ 0x03BB, "PW Down" },
	{ 0x0496, "AT Intensity" },
	{ 0x0497, "AT Offset" },

	{ 0x03A3, "Arp Pattern" },
	{ 0x03A5, "Arp Note Length" },
	{ 0x03A6, "Arp Note Swing" },

	{ 0x03AC, "Seq Swing" },
	{ 0x0477, "Seq A Pattern Length" },
	{ 0x03D6, "Seq A Step 1" },
	{ 0x03D7, "Seq A Step 2" },
	{ 0x03D8, "Seq A Step 3" },
	{ 0x03D9, "Seq A Step 4" },
	{ 0x03DA, "Seq A Step 5" },
	{ 0x03DB, "Seq A Step 6" },
	{ 0x03DC, "Seq A Step 7" },
	{ 0x03DD, "Seq A Step 8" },
	{ 0x03DE, "Seq A Step 9" },
	{ 0x03DF, "Seq A Step 10" },
	{ 0x03E0, "Seq A Step 11" },
	{ 0x03E1, "Seq A Step 12" },
	{ 0x03E2, "Seq A Step 13" },
	{ 0x03E3, "Seq A Step 14" },
	{ 0x03E4, "Seq A Step 15" },
	{ 0x03E5, "Seq A Step 16" },

	{ 0x0478, "Seq B Pattern Length" },
	{ 0x0444, "Seq B Step 1" },
	{ 0x0445, "Seq B Step 2" },
	{ 0x0446, "Seq B Step 3" },
	{ 0x0447, "Seq B Step 4" },
	{ 0x0448, "Seq B Step 5" },
	{ 0x044A, "Seq B Step 6" },
	{ 0x044B, "Seq B Step 7" },
	{ 0x044C, "Seq B Step 8" },
	{ 0x044D, "Seq B Step 9" },
	{ 0x044E, "Seq B Step 10" },
	{ 0x044F, "Seq B Step 11" },
	{ 0x0450, "Seq B Step 12" },
	{ 0x0451, "Seq B Step 13" },
	{ 0x0452, "Seq B Step 14" },
	{ 0x0453, "Seq B Step 15" },
	{ 0x0454, "Seq B Step 16" },

	{ 0x0479, "Seq C Pattern Length" },
	{ 0x0455, "Seq C Step 1" },
	{ 0x0456, "Seq C Step 2" },
	{ 0x0457, "Seq C Step 3" },
	{ 0x0458, "Seq C Step 4" },
	{ 0x0459, "Seq C Step 5" },
	{ 0x045A, "Seq C Step 6" },
	{ 0x045B, "Seq C Step 7" },
	{ 0x045C, "Seq C Step 8" },
	{ 0x045D, "Seq C Step 9" },
	{ 0x045E, "Seq C Step 10" },
	{ 0x045F, "Seq C Step 11" },
	{ 0x0460, "Seq C Step 12" },
	{ 0x0461, "Seq C Step 13" },
	{ 0x0462, "Seq C Step 14" },
	{ 0x0463, "Seq C Step 15" },
	{ 0x0464, "Seq C Step 16" },

	{ 0x047A, "Seq D Pattern Length" },
	{ 0x0465, "Seq D Step 1" },
	{ 0x0466, "Seq D Step 2" },
	{ 0x0467, "Seq D Step 3" },
	{ 0x0468, "Seq D Step 4" },
	{ 0x0469, "Seq D Step 5" },
	{ 0x046A, "Seq D Step 6" },
	{ 0x046B, "Seq D Step 7" },
	{ 0x046C, "Seq D Step 8" },
	{ 0x046D, "Seq D Step 9" },
	{ 0x046E, "Seq D Step 10" },
	{ 0x046F, "Seq D Step 11" },
	{ 0x0470, "Seq D Step 12" },
	{ 0x0471, "Seq D Step 13" },
	{ 0x0472, "Seq D Step 14" },
	{ 0x0473, "Seq D Step 15" },
	{ 0x0474, "Seq D Step 16" },

	{ 0x04A4, "Ribbon Offset" },
	{ 0x04A5, "Ribbon Intensity" },

	{ 0x4E7E, "FX Chan1 Slot1" },
	{ 0x4E7F, "FX Chan1 Slot2" },
	{ 0x4E80, "FX Chan1 Slot3" },
	{ 0x4E81, "FX Chan1 Slot4" },
	{ 0x4E82, "FX Chan2 Slot1" },
	{ 0x4E83, "FX Chan2 Slot2" },
	{ 0x4E84, "FX Chan2 Slot3" },
	{ 0x4E85, "FX Chan2 Slot4" },
	{ 0x4E86, "FX Chan3 Slot1" },
	{ 0x4E87, "FX Chan3 Slot2" },
	{ 0x4E88, "FX Chan3 Slot3" },
	{ 0x4E89, "FX Chan3 Slot4" },
	{ 0x4E8A, "FX Chan4 Slot1" },
	{ 0x4E8B, "FX Chan4 Slot2" },
	{ 0x4E8C, "FX Chan4 Slot3" },
	{ 0x4E8D, "FX Chan4 Slot4" },

	{ 0x4E22, "Chorus Freq" },
	{ 0x4E23, "Chorus Depth" },
	{ 0x4E24, "Chorus Phase" },
	{ 0x4E25, "Chorus Offset" },
	{ 0x4E26, "Chorus In Level" },
	{ 0x4E27, "Chorus Feedback" },
	{ 0x4E28, "Chorus Dry" },
	{ 0x4E29, "Chorus Wet" },

	{ 0x4E31, "Phaser Freq" },
	{ 0x4E32, "Phaser Depth" },
	{ 0x4E33, "Phaser Phase" },
	{ 0x4E34, "Phaser Offset" },
	{ 0x4E35, "Phaser In Level" },
	{ 0x4E36, "Phaser Feedback" },
	{ 0x4E37, "Phaser Dry" },
	{ 0x4E38, "Phaser Wet" },

	{ 0x4E3C, "Delay Time L" },
	{ 0x4E3D, "Delay Time R" },
	{ 0x4E3E, "Delay Feedback L" },
	{ 0x4E3F, "Delay Feedback R" },
	{ 0x4E40, "Delay Damping" },
	{ 0x4E41, "Delay Dry" },
	{ 0x4E42, "Delay Wet" },

	{ 0x4E45, "EQ Freq 1" },
	{ 0x4E46, "EQ Q1" },
	{ 0x4E47, "EQ Gain1" },
	{ 0x4E48, "EQ Freq 2" },
	{ 0x4E49, "EQ Q2" },
	{ 0x4E4A, "EQ Gain2" },
	{ 0x4E4B, "EQ Freq 3" },
	{ 0x4E4C, "EQ Q3" },
	{ 0x4E4D, "EQ Gain3" },

	{ 0x03E8, "Vec1 Input1 Lvl" },
	{ 0x03E9, "Vec1 Input2 Lvl" },
	{ 0x03EA, "Vec1 Input3 Lvl" },
	{ 0x03EB, "Vec1 Input4 Lvl" },
	{ 0x036F, "Vec1 X Mod Amt" },
	{ 0x036D, "Vec1 X Mod Offset" },
	{ 0x0370, "Vec1 Y Mod Amt" },
	{ 0x036E, "Vec1 Y Mod Offset" },

	{ 0x03EC, "Vec2 Input1 Lvl" },
	{ 0x03ED, "Vec2 Input2 Lvl" },
	{ 0x03EE, "Vec2 Input3 Lvl" },
	{ 0x03EF, "Vec2 Input4 Lvl" },
	{ 0x0379, "Vec2 X Mod Amt" },
	{ 0x0377, "Vec2 X Mod Offset" },
	{ 0x037A, "Vec2 Y Mod Amt" },
	{ 0x0373, "Vec2 Y Mod Offset" },

	{ 0x0361, "AM1 Ctrl Strength" },
	{ 0x0362, "AM1 Mod Amt " },
	{ 0x0360, "AM1 Offset "},

	{ 0x0368, "AM2 Ctrl Strength" },
	{ 0x0369, "AM2 Mod Amt " },
	{ 0x0367, "AM2 Offset " },

	{ 0x0381, "Leg Time 1" },
	{ 0x0382, "Leg Level 1x" },
	{ 0x0383, "Leg Level 1y" },
	{ 0x0384, "Leg Time 2" },
	{ 0x0385, "Leg Level 2x" },
	{ 0x0386, "Leg Level 2y" },
	{ 0x0387, "Leg Time 3" },
	{ 0x0388, "Leg Level 3x" },
	{ 0x0389, "Leg Level 3y" },
	{ 0x038A, "Leg Time 4" },
	{ 0x038B, "Leg Level 4x" },
	{ 0x038C, "Leg Level 4y" },
	{ 0x038D, "Leg Time 5" },
	{ 0x038E, "Leg Level 5x" },
	{ 0x038F, "Leg Level 5y" },
	{ 0x0390, "Leg Time 6" },
	{ 0x0391, "Leg Level 6x" },
	{ 0x0392, "Leg Level 6y" },
	{ 0x0393, "Leg Time 7" },
	{ 0x0394, "Leg Level 7x" },
	{ 0x0395, "Leg Level 7y" },
	{ 0x0396, "Leg Time 8" },
	{ 0x0397, "Leg Level 8x" },
	{ 0x0398, "Leg Level 8y" },
	{ 0x0399, "Leg Slope" },
	{ 0x039D, "Leg Level Amt" },
	{ 0x039C, "Leg Time Amt" },


	{ 0x04D7, "KTab1 Prev" },
	{ 0x04CE, "KTab1 Current" },
	{ 0x04D3, "KTab1 Next" },
	{ 0x04DF, "KTab1 Prev Value" },
	{ 0x04CA, "KTab1 Current Value" },
	{ 0x04DB, "KTab1 Next Value" },
	{ 0x04CF, "KTab1 Active" },

	{ 0x04D8, "KTab2 Prev" },
//	{ 0x04CE, "KTab2 Current" },	// Same as KTab1 Current
	{ 0x04D4, "KTab2 Next" },
	{ 0x04E0, "KTab2 Prev Value" },
	{ 0x04CB, "KTab2 Current Value" },
	{ 0x04DC, "KTab2 Next Value" },
	{ 0x04D0, "KTab2 Active" },

	{ 0x04D9, "KTab3 Prev" },
//	{ 0x04CE, "KTab3 Current" },	// Same as KTab1 Current
	{ 0x04D5, "KTab3 Next" },
	{ 0x04E1, "KTab3 Prev Value" },
	{ 0x04CC, "KTab3 Current Value" },
	{ 0x04DD, "KTab3 Next Value" },
	{ 0x04D1, "KTab3 Active" },

	{ 0x04DA, "KTab4 Prev" },
//	{ 0x04CE, "KTab4 Current" },	// Same as KTab1 Current
	{ 0x04D6, "KTab4 Next" },
	{ 0x04E2, "KTab4 Prev Value" },
	{ 0x04CD, "KTab4 Current Value" },
	{ 0x04DE, "KTab4 Next Value" },
	{ 0x04D2, "KTab4 Active" },

	{ 0x050D, "Lag1 Time" },
	{ 0x050E, "Lag2 Time" },
	{ 0x050F, "Lag3 Time" },
	{ 0x0510, "Lag4 Time" },

	{ 0x0492, "EnvFol Att Time" },
	{ 0x0493, "EnvFol Rel Time" },
	{ 0x0490, "EnvFol Input Level" },
	{ 0x0491, "EnvFol Output Level" },

	{ 0x4E5B, "Master Tune" },

	{ 0x4E6F, "MIDI CC1" },
	{ 0x4E70, "MIDI CC2" },
	{ 0x4E71, "MIDI CC3" },
	{ 0x4E72, "MIDI CC4" },
	{ 0x4E73, "MIDI CC5" }
};


std::string GetPerfSourceString(int value)
{
	if (value == 0)
	{
		return "None";
	}
	auto iter = std::find_if(perfSourceList.begin(), perfSourceList.end(), [value](auto& p) { return p.index == value;});
	return (iter != perfSourceList.end()) ? iter->name : "None";
}
