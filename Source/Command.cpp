/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <vector>
#include <string>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <chrono> 
#include "Command.h"
#include "ParameterList.h"
#include "ViewModel.h"
#include "Utility.h"

Preset* Command::m_pPreset{};
ViewModel* Command::m_pViewModel{};

CommandUPtr Command::Create(std::istream& input)
{
	CommandUPtr pResult;
	CmdType cmdType;
	input.read(reinterpret_cast<char *>(&cmdType), sizeof(cmdType));
	switch (cmdType)
	{
	case CmdType::NewPreset:			pResult = std::make_unique<CmdNewPreset>(input);			break;
	case CmdType::SetPreset:			pResult = std::make_unique<CmdSetPreset>(input);			break;
	case CmdType::SetParameterValue:	pResult = std::make_unique<CmdSetParameterValue>(input);	break;
	case CmdType::SetMuted:				pResult = std::make_unique<CmdSetMuted>(input);				break;
	case CmdType::CreateConnector:		pResult = std::make_unique<CmdCreateConnector>(input);		break;
	case CmdType::RemoveConnector:		pResult = std::make_unique<CmdRemoveConnector>(input);		break;
	case CmdType::AddElement:			pResult = std::make_unique<CmdAddElement>(input);			break;
	case CmdType::RemoveElement:		pResult = std::make_unique<CmdRemoveElement>(input);		break;
	case CmdType::Macro:				pResult = std::make_unique<CmdMacro>(input);				break;
	};

	return pResult;
}

inline void Command::UpdateTime()
{
	std::time_t currentTime;
	currentTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	struct tm *pLocalTime;
	pLocalTime = localtime(&currentTime);		// Don't use localtime_r or localtime_s. One doesn't exist on Windows and the other doesn't exist on MacOS.
	m_hour = gsl::narrow<uint8_t>(pLocalTime->tm_hour);
	m_minute = gsl::narrow<uint8_t>(pLocalTime->tm_min);
	m_second = gsl::narrow<uint8_t>(pLocalTime->tm_sec);
}


/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdSetParameterValue
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdSetParameterValue::CmdSetParameterValue(const ParamID& paramID, uint32_t value)
	:m_paramID(paramID)
	,m_value(value)
{
}
CmdSetParameterValue::CmdSetParameterValue(std::istream& input)
{
	Read(input);
}


void CmdSetParameterValue::ExecuteImpl(Version version)
{
	// Finally, set the original parameter
	GetTargetPreset()->SetParameterValue(m_paramID, m_value, version);
}

std::string CmdSetParameterValue::GetAction()
{
	return" Edit Parameter";
}

std::string CmdSetParameterValue::GetDescription()
{
	return GetParameterDetail(m_paramID);
}

/** @brief	Combines the effect of two consecutive changes of the same parameter.
	
	The "other" occurs second.Thus, it actually just replaces the m_value parameter 
	with other's m_value. Unexecute will undo both actions.

	@param[in]	The command that occured second.

	@retval		true if the parameters were combined. 
	@retval		false if they could not be combined because the other command was not a CmdSetParameterValue 
				OR because there were secondary parameter changes involved in one or both commands.
*/
bool CmdSetParameterValue::CombineImpl(Command* pOther) noexcept
{
	bool result{ false };
	auto pOtherSetParamCmd = dynamic_cast<CmdSetParameterValue*> (pOther);
	if(pOtherSetParamCmd && (m_paramID == pOtherSetParamCmd->m_paramID))
	{
		m_value = pOtherSetParamCmd->m_value;
		result = true;
	}
	return result;
}

void CmdSetParameterValue::Write(std::ostream& output) const
{
	auto cmdType = CmdType::SetParameterValue;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_paramID.Write(output);
	output.write(reinterpret_cast<const char *>(&m_value), sizeof(m_value));
}
void CmdSetParameterValue::Read(std::istream& input)
{
	Command::Read(input);

	m_paramID.Read(input);
	input.read(reinterpret_cast<char *>(&m_value), sizeof(m_value));
}

/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdSetPreset
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdSetPreset::CmdSetPreset(std::vector<ParamValue>&& newParamValues)
	:m_newParamValues(std::move(newParamValues))
	,m_action("Set Preset")
{
}

CmdSetPreset::CmdSetPreset(std::istream& input)
{
	Read(input);
}

std::string CmdSetPreset::GetAction()
{
	return m_action;
}

std::string CmdSetPreset::GetDescription()
{
	std::string desc;
	static uint8_t subIndexes[] = { gloName1, gloName2, gloName3, gloName4, gloName5, gloName6, gloName7, gloName8, gloName9, gloName10,
								gloName11, gloName12, gloName13, gloName14, gloName15, gloName16, gloName17, gloName18, gloName19, gloName20 };

	// Extract the name from the parameter values.
	if (m_newName.empty())
	{
		ParamID paramID{ ElementType::Home, 0, PortType::None, 0, 0 };
		for (int i = 0; i < sizeof(subIndexes); ++i)
		{
			paramID.subIndex = subIndexes[i];
			auto iter = std::find_if(m_newParamValues.begin(), m_newParamValues.end(), [paramID](auto& p) { return p.paramID == paramID; });
			if (iter != m_newParamValues.end())
			{
				m_newName += static_cast<char>(iter->value);
			}
		}
	}
	if (!m_newName.empty())
	{
		desc += "Name: ";
		desc += m_newName;
	}

	return desc;
}

void CmdSetPreset::SetActionText(const std::string & text)
{
	m_action = text;
}

void CmdSetPreset::ExecuteImpl(Version version)
{
	GetTargetPreset()->SetPreset(m_newParamValues, version);
	util::FreeVector(m_newParamValues);	// we no longer need the param values after we execute.
}

void CmdSetPreset::Write(std::ostream& output) const
{
	assert(m_action.size() < 20);
	assert(m_newName.size() < 20);

	auto cmdType = CmdType::SetPreset;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	uint8_t len = gsl::narrow<uint8_t>(m_action.size());
	output.write(reinterpret_cast<const char *>(&len), sizeof(len));
	output.write(m_action.data(), len);

	len = gsl::narrow<uint8_t>(m_newName.size());
	output.write(reinterpret_cast<const char *>(&len), sizeof(len));
	output.write(m_newName.data(), len);
}
void CmdSetPreset::Read(std::istream& input)
{
	Command::Read(input);

	uint8_t len{};
	char buf[21] = { 0 };
	input.read(reinterpret_cast<char *>(&len), sizeof(len));
	assert(len <= 20);
	input.read(reinterpret_cast<char *>(&buf[0]), len);
	m_action = &buf[0];

	input.read(reinterpret_cast<char *>(&len), sizeof(len));
	assert(len <= 20);
	input.read(reinterpret_cast<char *>(&buf[0]), len);
	m_newName = &buf[0];
}
/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdNewPreset
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdNewPreset::CmdNewPreset()
{
}

CmdNewPreset::CmdNewPreset(std::istream & input)
{
	Read(input);
}

std::string CmdNewPreset::GetAction()
{
	return "New Preset";
}

std::string CmdNewPreset::GetDescription()
{
	return "";
}

void CmdNewPreset::ExecuteImpl(Version version)
{
	GetTargetPreset()->InitializePreset(version);
}
void CmdNewPreset::Write(std::ostream& output) const
{
	auto cmdType = CmdType::NewPreset;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);
}
void CmdNewPreset::Read(std::istream& input)
{
	Command::Read(input);
}


/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdSetMuted
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdSetMuted::CmdSetMuted(const ParamID & paramID, bool isMuted)
	:m_paramID(paramID)
	,m_isMuted(isMuted)
{
}

CmdSetMuted::CmdSetMuted(std::istream & input)
{
	Read(input);
}

void CmdSetMuted::ExecuteImpl(Version version)
{
	GetTargetPreset()->SetMuted(m_paramID, m_isMuted, version);
}

std::string CmdSetMuted::GetAction()
{
	return m_isMuted ? "Mute" : "Unmute";
}

std::string CmdSetMuted::GetDescription()
{
	return GetParameterDetail(m_paramID);
}

bool CmdSetMuted::HasEqualParamID(const CmdSetMuted & other)
{
	return m_paramID == other.m_paramID;
}

void CmdSetMuted::Write(std::ostream& output) const
{
	auto cmdType = CmdType::SetMuted;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_paramID.Write(output);
	output.write(reinterpret_cast<const char *>(&m_isMuted), sizeof(m_isMuted));
}
void CmdSetMuted::Read(std::istream& input)
{
	Command::Read(input);

	m_paramID.Read(input);
	input.read(reinterpret_cast<char *>(&m_isMuted), sizeof(m_isMuted));
}

/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdCreateConnector
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdCreateConnector::CmdCreateConnector(const PortID & startingPortID, const PortID & endingPortID)
	:m_newStartingPortID(startingPortID)
	, m_newEndingPortID(endingPortID) 
{
}

CmdCreateConnector::CmdCreateConnector(std::istream & input)
{
	Read(input);
}

std::string CmdCreateConnector::GetAction()
{
	return "Add Connection";
}

std::string CmdCreateConnector::GetDescription()
{
	const PortID& inputPortID = m_newStartingPortID.portType != PortType::Output ? m_newStartingPortID : m_newEndingPortID;
	const PortID& outputPortID = m_newStartingPortID.portType == PortType::Output ? m_newStartingPortID : m_newEndingPortID;

	std::string name = GetElementName(outputPortID.elemID) + " -> ";
	name += GetParameterDetail({ inputPortID, 4 });	// by using index 4 we avoid appending "src" on the end.
	return name;
}

void CmdCreateConnector::ExecuteImpl(Version version)
{
	GetTargetPreset()->CreateConnector(m_newStartingPortID, m_newEndingPortID, version);
}

void CmdCreateConnector::Write(std::ostream& output) const
{
	auto cmdType = CmdType::CreateConnector;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_newStartingPortID.Write(output);
	m_newEndingPortID.Write(output);
}

void CmdCreateConnector::Read(std::istream& input)
{
	Command::Read(input);

	m_newStartingPortID.Read(input);
	m_newEndingPortID.Read(input);
}


/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdRemoveConnector
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdRemoveConnector::CmdRemoveConnector(const Connector& connectorToRemove)
	:m_connectorToRemove(connectorToRemove)
{
}

CmdRemoveConnector::CmdRemoveConnector(std::istream & input)
{
	Read(input);
}

std::string CmdRemoveConnector::GetAction()
{
	return "Remove Connection";
}

std::string CmdRemoveConnector::GetDescription()
{
	const PortID& inputPortID = m_connectorToRemove.startPort.portType != PortType::Output ? m_connectorToRemove.startPort : m_connectorToRemove.endPort;
	const PortID& outputPortID = m_connectorToRemove.endPort.portType == PortType::Output ? m_connectorToRemove.endPort : m_connectorToRemove.startPort;
	
	std::string name = GetElementName(outputPortID.elemID) + " -> ";
	name += GetParameterDetail({ inputPortID, 4 });	// by using index 4 we avoid appending "src" on the end.
	return name;
}

void CmdRemoveConnector::ExecuteImpl(Version version)
{
	GetTargetPreset()->RemoveConnector(m_connectorToRemove, version);
}

void CmdRemoveConnector::Write(std::ostream& output) const
{
	auto cmdType = CmdType::RemoveConnector;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_connectorToRemove.startPort.Write(output);
	m_connectorToRemove.endPort.Write(output);
}

void CmdRemoveConnector::Read(std::istream& input)
{
	Command::Read(input);

	m_connectorToRemove.startPort.Read(input);
	m_connectorToRemove.endPort.Read(input);
}


/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdMacro
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdMacro::CmdMacro()
{
}

CmdMacro::CmdMacro(std::istream & input)
{
	Read(input);
}


std::string CmdMacro::GetAction()
{
	return m_actionName;
}

std::string CmdMacro::GetDescription()
{
	return m_details;
}

void CmdMacro::SetActionName(const std::string & nameText)
{
	m_actionName = nameText;
}

void CmdMacro::SetDetails(const std::string & detailsText)
{
	m_details = detailsText;
}

void CmdMacro::AddCommand(CommandUPtr && command)
{
	m_cmdList.push_back(std::move(command));
}

void CmdMacro::ExecuteImpl(Version version)
{
	std::for_each(m_cmdList.begin(), m_cmdList.end(), [version](auto& cmd) { cmd->Execute(version);});
}

void CmdMacro::Write(std::ostream& output) const
{
	auto cmdType = CmdType::Macro;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	assert(m_actionName.size() < 20);
	assert(m_details.size() < 20);

	uint8_t len = gsl::narrow<uint8_t>(m_actionName.size());
	output.write(reinterpret_cast<const char *>(&len), sizeof(len));
	output.write(m_actionName.data(), len);

	len = gsl::narrow<uint8_t>(m_details.size());
	output.write(reinterpret_cast<const char *>(&len), sizeof(len));
	output.write(m_details.data(), len);

	len = gsl::narrow<uint8_t>(m_cmdList.size());
	output.write(reinterpret_cast<const char *>(&len), sizeof(len));
	auto iter = m_cmdList.begin();
	while(iter++ !=  m_cmdList.end())
	{
		(*iter)->Write(output);
	}
}
void CmdMacro::Read(std::istream& input)
{
	m_cmdList.clear();

	Command::Read(input);

	uint8_t len{};
	char buf[21] = { 0 };
	input.read(reinterpret_cast<char *>(&len), sizeof(len));
	assert(len <= 20);
	input.read(reinterpret_cast<char *>(&buf[0]), len);
	m_actionName = &buf[0];

	input.read(reinterpret_cast<char *>(&len), sizeof(len));
	assert(len <= 20);
	input.read(reinterpret_cast<char *>(&buf[0]), len);
	m_details = &buf[0];

	input.read(reinterpret_cast<char *>(&len), sizeof(len));
	assert(len <= 20);
	for (int i = 0; i < len; ++i)
	{
		m_cmdList.push_back(Command::Create(input));
	}
}

/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdAddElement
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdAddElement::CmdAddElement(const ElementID& elemID, const Location& location)
	: m_elemID(elemID)
	, m_location(location)
{
}

CmdAddElement::CmdAddElement(std::istream & input)
{
	Read(input);
}

std::string CmdAddElement::GetAction()
{
	return "Add Element";
}

std::string CmdAddElement::GetDescription()
{
	return GetElementName(m_elemID);;
}

void CmdAddElement::ExecuteImpl(Version version)
{
	GetTargetViewModel()->AddElement(m_elemID, m_location, version);
}

void CmdAddElement::Write(std::ostream& output) const
{
	auto cmdType = CmdType::AddElement;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_elemID.Write(output);
	output.write(reinterpret_cast<const char *>(&m_location.x), sizeof(m_location.x));
	output.write(reinterpret_cast<const char *>(&m_location.y), sizeof(m_location.y));
}
void CmdAddElement::Read(std::istream& input)
{
	Command::Read(input);

	m_elemID.Read(input);
	input.read(reinterpret_cast<char *>(&m_location.x), sizeof(m_location.x));
	input.read(reinterpret_cast<char *>(&m_location.y), sizeof(m_location.y));
}


/////////////////////////////////////////////////////////////////////////////////
//
//
//		CmdRemoveElement
//
//
/////////////////////////////////////////////////////////////////////////////////

CmdRemoveElement::CmdRemoveElement(const ElementID& elemID)
	:m_elemID(elemID)
{
}

CmdRemoveElement::CmdRemoveElement(std::istream & input)
{
	Read(input);
}

std::string CmdRemoveElement::GetAction()
{
	return "Remove Element";
}

std::string CmdRemoveElement::GetDescription()
{
	return GetElementName(m_elemID);;
}

void CmdRemoveElement::ExecuteImpl(Version version)
{
	GetTargetViewModel()->RemoveElement(m_elemID, version);
}


void CmdRemoveElement::Write(std::ostream& output) const
{
	auto cmdType = CmdType::RemoveElement;
	output.write(reinterpret_cast<const char *>(&cmdType), sizeof(cmdType));
	Command::Write(output);

	m_elemID.Write(output);
}
void CmdRemoveElement::Read(std::istream& input)
{
	Command::Read(input);

	m_elemID.Read(input);
}

/////////////////////////////////////////////////////////////////////////////////
//
//
//		CommandHistory
//
//
/////////////////////////////////////////////////////////////////////////////////


CommandHistory::CommandHistory()
	:m_bookmarks(10, Version())
{
	auto branchTags = m_hlist.GetBranchTags();
	assert(branchTags.size() == 1);
	BranchInfo bi;
	bi.branchTag = branchTags[0];
	bi.branchName = "Start";
	m_branches.push_back(bi);
}

/** @brief	Adds a command to the command history and executes it.

	Actually, the command gets executed before adding, but "AddCommandAndExecute" sounds like a better name to me.

	@param[in]	An rvalue reference to a command. This function will take over ownership of the command 
				so it can no longer be used after calling this function.
*/
void CommandHistory::AddCommandAndExecute(CommandUPtr && pCommand)
{
	Command* pCmd = pCommand.get();	// save for use below, since the unique_ptr will get moved.

	bool doAdd{ true };
	BranchInfo bi;
	bi.branchTag = NO_BRANCH;
	if (m_hlist.GetCurrentBranchLength() > 0)
	{
		// Only combine if the current version is the most recent one added.
		if (m_hlist.GetCurrentVersion() == m_hlist.GetMaxVersion())
		{
			auto pPrevCommand = m_hlist.GetItemAtVersion(m_hlist.GetMaxVersion()).get();
			doAdd = (false == pPrevCommand->Combine(pCommand.get()));
		}
	}
	if(doAdd)
	{
		// GetBranch point in case a branch is added. The branch tag has to be the original tag.
		Version branchPointVersion = m_hlist.GetCurrentVersionBasic();	
		std::pair<bool, BranchTag> result = m_hlist.AddItem(std::move(pCommand));	//<- updates current version
		if (result.first)
		{
			bi.branchTag = result.second;
			m_branches.push_back(bi);

			BranchAddedEvent.Notify(bi.branchTag, branchPointVersion);
		}
	}

	pCmd->Execute(m_hlist.GetCurrentVersion());

	// default branch name is the time of execution.
	if (bi.branchTag != NO_BRANCH)	// if a new branch was added...
	{
		m_branches.back().branchName = pCmd->GetTime();
	}

	EditHistoryChangedEvent.Notify(bi.branchTag);
}

void CommandHistory::Undo()
{
	if (CanUndo())
	{
		m_hlist.Undo();	// Will decrement current version
		auto currentVersion = m_hlist.GetCurrentVersion();
		GoToVersionEvent.Notify(currentVersion);
		EditHistoryChangedEvent.Notify(NO_BRANCH);	// NO_BRANCH means the branch hasn't changed.
	}
}


void CommandHistory::UndoDestructive()
{
	if (m_hlist.CanUndoDestructive())
	{
		auto versionDeleted = m_hlist.GetCurrentVersion();
		if (m_hlist.UndoDestructive())
		{
			// If UndoDestructive succeeded it may have resulted in bookmarks that point to a non-exist version.
			// So so remove any invalid bookmarks.
			RemoveInvalidBookmarks();

			GoToVersionEvent.Notify(m_hlist.GetCurrentVersion());
			RemoveVersionEvent.Notify(versionDeleted);	// tell clients to remove the version.
		}
		EditHistoryChangedEvent.Notify(m_hlist.GetCurrentVersion().branchTag);
	}
}


/** @brief Sets the current version of the history relative to the given branch.

	Listeners are notified.

	@param[in] version	The version to set. This version number is relative to the branch specified.
						The valid range is from 0 to GetHistoryLength(branchTag) - 1 inclusive.
	@param[in] branchTag	The tag of the branch that the version is relative to.

*/
void CommandHistory::SetVersion(Version version)
{
	if (m_hlist.GetCurrentVersion() != version)
	{
		m_hlist.SetVersion(version);

		// The GoToVersionEvent is generally observed by the client objects who execute the commands.
		auto currentVersion = m_hlist.GetCurrentVersion();
		GoToVersionEvent.Notify(currentVersion);

		// The EditHistoryChangedEvent is intended to be observed by the UI showing the version history.
		EditHistoryChangedEvent.Notify(currentVersion.branchTag);
	}
}

void CommandHistory::SetRevision(Revision revision)
{
	SetVersion(Version(revision, m_hlist.GetCurrentVersion().branchTag));
}

Version CommandHistory::GetCurrentVersion() const
{
	return m_hlist.GetCurrentVersion();
}

Revision CommandHistory::GetCurrentRevision() const
{
	return m_hlist.GetCurrentVersion().revision;
}

BranchTag CommandHistory::GetCurrentBranchTag()
{
	return m_hlist.GetCurrentVersion().branchTag;
}
void CommandHistory::SetCurrentBranchTag(BranchTag branchTag)
{
	SetVersion(m_hlist.GetMaxVersion(branchTag));
}
std::vector<BranchInfo> CommandHistory::GetBranchList() const
{
	return m_branches;
}

/** @brief	Sets the name of the branch specified by the branch tag.

	@param[in] branchTag	The tag of the branch to set the name of. If the tag doesn't exist, this function has no effect.
	@param[in] name			The new name of the branch. The maximum length is 20 characters. (Longer names are truncated.
							Branch names do not need  to be unique because they are identified by the branch tags which 
							are unique.
*/
void CommandHistory::SetBranchName(BranchTag branchTag, const std::string & name)
{
	auto iter = std::find_if(m_branches.begin(), m_branches.end(), [branchTag](auto& info) { return info.branchTag == branchTag;});
	assert(iter != m_branches.end());
	if (iter != m_branches.end())
	{
		iter->branchName = name.substr(0, 20);	// limit to 20 characters
		EditHistoryChangedEvent.Notify(branchTag);
	}
}

std::string CommandHistory::GetBranchName(BranchTag branchTag)
{
	std::string result;
	auto iter = std::find_if(m_branches.begin(), m_branches.end(), [branchTag](auto& info) { return info.branchTag == branchTag;});
	//assert(iter != m_branches.end());
	if (iter != m_branches.end())
	{
		result = iter->branchName;
	}
	return result;
}

void CommandHistory::DeleteBranch(BranchTag branchTag)
{
	std::vector<Version> versionsToPrune;

	m_hlist.DeleteBranch(branchTag, versionsToPrune);

	// remove commands in this history
	m_branches.erase(std::remove_if(m_branches.begin(), m_branches.end(), [branchTag](BranchInfo& bi)
	{
		return bi.branchTag == branchTag;
	}));

	RemoveInvalidBookmarks();

	// Notify clients to delete and prune branches.
	for (auto& v : versionsToPrune)
	{
		BranchDeletedEvent.Notify(v.branchTag, v.revision);
	}
}

bool CommandHistory::BranchExists(BranchTag branchTagToCheck)
{
	auto branchTags = m_hlist.GetBranchTags();

	auto iter = std::find_if(branchTags.begin(), branchTags.end(), [branchTagToCheck](auto& branchTag) { return branchTag == branchTagToCheck;});
	return (iter != branchTags.end());
}

/** @brief Returns the Version that this branch originally branched off of.

	Note that the branchTag of the Version that's returned will not be the same as the branchTag
	passed in since it is the branch that this one branched off of.
	@param[in] branchTag			The tag of the branch.
									Behavior is undefined if the branch doesn't exist.
	@returns						The Version that is the branch point.
									This version will equal NO_VERSION if this is the trunk and thus no branch point.
*/
Version CommandHistory::GetBranchPoint(BranchTag branchTag) const
{
	return m_hlist.GetBranchPoint(branchTag);
}

void CommandHistory::Write(std::ostream & output)
{
	auto writeItemFunc = [](std::ostream& output, CommandUPtr& pCommand)
	{
		pCommand->Write(output);
	};
	m_hlist.Write(output, writeItemFunc);

	uint32_t numBranches = m_branches.size();
	output.write(reinterpret_cast<const char *>(&numBranches), sizeof(numBranches));
	for (auto& bi : m_branches)
	{
		uint8_t len = gsl::narrow<uint8_t>(bi.branchName.size());
		output.write(reinterpret_cast<const char *>(&len), sizeof(len));
		output.write(bi.branchName.data(), len);
		output.write(reinterpret_cast<const char *>(&bi.branchTag), sizeof(bi.branchTag));
	}
	uint32_t numBookmarks = m_bookmarks.size();
	assert(numBookmarks == 10);
	output.write(reinterpret_cast<const char *>(&numBookmarks), sizeof(numBookmarks));
	for (size_t i = 0; i < numBookmarks; ++i)
	{
		m_bookmarks[i].Write(output);
	}
}

void CommandHistory::Read(std::istream & input)
{
	auto readItemFunc = [](std::istream& input)
	{
		return Command::Create(input);
	};
	m_hlist.Read(input, readItemFunc);

	m_branches.clear();
	uint32_t numBranches;
	input.read(reinterpret_cast<char *>(&numBranches), sizeof(numBranches));
	BranchInfo bi;
	char buf[21] = {0};
	for (uint32_t i = 0; i < numBranches; ++i)
	{
		uint8_t len;
		input.read(reinterpret_cast<char *>(&len), sizeof(len));
		assert(len <= 20);
		input.read(reinterpret_cast<char *>(&buf[0]), len);
		buf[len] = 0;
		bi.branchName = &buf[0];
		input.read(reinterpret_cast<char *>(&bi.branchTag), sizeof(bi.branchTag));
		m_branches.push_back(bi);
	}

	uint32_t numBookmarks;
	input.read(reinterpret_cast<char *>(&numBookmarks), sizeof(numBookmarks));
	assert(numBookmarks == 10);
	m_bookmarks.clear();
	m_bookmarks.resize(numBookmarks);
	for (size_t i = 0; i < numBookmarks; ++i)
	{
		m_bookmarks[i].Read(input);
	}

	Version currentVersion = m_hlist.GetCurrentVersion();
	GoToVersionEvent.Notify(currentVersion);
	EditHistoryChangedEvent.Notify(currentVersion.branchTag);

}

void CommandHistory::Redo()
{
	if(m_hlist.CanRedo())
	{
		m_hlist.Redo();
		auto currentVersion = m_hlist.GetCurrentVersion();
		GoToVersionEvent.Notify(currentVersion);
		EditHistoryChangedEvent.Notify(NO_BRANCH);	// NO_BRANCH means the branch hasn't changed.
	}
}

/** @brief Returns a pointer to the Command at the specified version along the current branch.
	
	@returns A pointer to the Command at the specified index, or nullptr if the index is out of range.
*/
Command * CommandHistory::GetCommandAtVersion(Version version) const
{
	return m_hlist.GetItemAtVersion(version).get();
}

Command * CommandHistory::GetCommandAtRevision(Revision revision) const
{
	return m_hlist.GetItemAtVersion(Version(revision, m_hlist.GetCurrentVersion().branchTag)).get();
}

bool CommandHistory::CanUndo()
{
	return m_hlist.CanUndo();
}

bool CommandHistory::CanUndoDestructive()
{
	return m_hlist.CanUndoDestructive();
}

bool CommandHistory::CanRedo()
{
	return m_hlist.CanRedo();
}

/** @brief	Sets a bookmark at the current version.

	If the bookmark had been previously set, the old version is overwritten.

	@param[in]	A bookmark number between 0 and 9 inclusive.
*/
void CommandHistory::SetBookmark(int bookmarkNumber)
{
	assert(bookmarkNumber >= 0 && bookmarkNumber <= 9);
	assert(m_bookmarks.size() > static_cast<size_t>(bookmarkNumber));
	m_bookmarks[bookmarkNumber] = m_hlist.GetCurrentVersion();
	EditHistoryChangedEvent.Notify(m_hlist.GetCurrentVersion().branchTag);
}

/** @brief	Changes the current version to be the version of the bookmark.

	If the bookmark had never been set, then this function has no effect.

	@param[in]	A bookmark number between 0 and 9 inclusive.
*/
void CommandHistory::GoToBookmark(int bookmarkNumber)
{
	assert(bookmarkNumber >= 0 && bookmarkNumber <= 9);
	assert(m_bookmarks.size() > static_cast<size_t>(bookmarkNumber));
	if (m_bookmarks[bookmarkNumber].revision != INVALID_REVISION && m_bookmarks[bookmarkNumber].branchTag != NO_BRANCH)
	{
		m_hlist.SetVersion(m_bookmarks[bookmarkNumber]);
		GoToVersionEvent.Notify(m_hlist.GetCurrentVersion());
		EditHistoryChangedEvent.Notify(m_hlist.GetCurrentVersion().branchTag);
	}
}

/** @brief	Returns the Version at which the bookmark is currently set.
	@param[in]	A bookmark number between 0 and 9 inclusive.
	@returns	The Version of the specified bookmark. If the bookmark has never been set, then the 
				Version return is initialized to revision INVALID_REVISION and the branchTag to NO_BRANCH.
*/
Version CommandHistory::GetBookmark(int bookmarkNumber)
{
	assert(bookmarkNumber >= 0 && bookmarkNumber <= 9);
	assert(m_bookmarks.size() > static_cast<size_t>(bookmarkNumber));
	return m_bookmarks[bookmarkNumber];
}

/** @brief	Returns a list of bookmarks at the current version.
	@param[in]	The version to check.
	@returns	A list of bookmarks at the current version.
*/
std::vector<int> CommandHistory::GetBookmarksAtVersion(Version version)
{
	std::vector<int> list;
	for (size_t i = 1; i < m_bookmarks.size(); ++i)
	{
		if (m_bookmarks[i] == version)
		{
			list.push_back(i);
		}
	}
	if (m_bookmarks[0] == version)
	{
		list.push_back(0);
	}
	return list;
}

void  CommandHistory::RemoveInvalidBookmarks()
{
	for (auto& version : m_bookmarks)
	{
		// remove a bookmark if the branch is gone or if the revision is greater than the max revision of that branch.
		if (!BranchExists(version.branchTag) || (version.revision > m_hlist.GetMaxRevision(version.branchTag)))
		{
			version = Version();
		}
	}
}


uint32_t CommandHistory::GetHistoryLength() const 
{
	return m_hlist.GetBranchLength(GetCurrentVersion().branchTag);
}

uint32_t CommandHistory::GetHistoryLength(BranchTag branchTag) const 
{
	return m_hlist.GetBranchLength(branchTag);
}


struct ParameterName
{
	int param;
	const char* name;
};

struct ElementName
{
	ElementType elemType;
	uint8_t	count;			///< The number of instances of the element that exist.
	const char* name;
	std::vector<ParameterName> paramNames;
};

std::vector<ElementName> elementNames
{ 
	{ ElementType::System,1, "Sys",{
	{ sysTranspose , "Transpose" },
	{ sysFineTune, "Type" },
	{ sysBPMOverride, "BPM Override" },
	{ sysOutputsOverride, "Outputs Override" },
	{ sysExpPedalPolarity, "ExpPedalPolarity" },
	{ sysSusPedal1Polarity, "SusPedal1Polarity" },
	{ sysSusPedal2Polarity, "SusPedal2Polarity" },
	{ sysExpPedalTargetOverride, "ExpPedalTargetOverride" },
	{ sysSusPedalTargetOverride, "SusPedalTargetOverride" },
	{ sysRndTune, "Global RndTune" },
	{ sysVTIntens , "VTIntens" },
	{ sysVTOffset , "VTOffset" },
	{ sysATIntens , "ATIntens" },
	{ sysATOffset , "ATOffset" },
	{ sysMidiChannel, "Midi Channel" },
	{ sysProgramChangeAllowed, "Program Change Allowed" },
	{ sysSendArp, "SendArp" },
	{ sysOmni, "Omni" },
	{ sysLocal, "Local" },
	{ sysTransmitSysEx, "Transmit SysEx" },
	{ sysReceiveSysEx, "Receive SysEx" },
	{ sysMidiClockSource, "Midi Clock Source" },
	{ sysMidiVolumeAllowed, "Midi Volume Allowed" },
	{ sysMidiRealTime, "Midi RealTime" },
	{ sysMidiCC1, "Midi CC1" },
	{ sysMidiCC2, "Midi CC2" },
	{ sysMidiCC3, "Midi CC3" },
	{ sysMidiCC4, "Midi CC4" },
	{ sysMidiCC5, "Midi CC5" },
	{ sysRemapAT, "RemapAT" } } },
		
	{ ElementType::Oscillator,4,	"Osc", {
	{ oscType , "Type" },
	{ oscMM1Wave , "MM1 wave" },
	{ oscWTWave , "WT wave" },
	{ oscCEMWave , "CEM wave" },
	{ oscWAVWave , "WAV wave" },
	{ oscVSWave , "VS wave" },
	{ oscMiniWave , "Mini wave" },
	{ oscShape , "Shape" },
	{ oscCoarse , "Coarse" },
	{ oscFine , "Fine" },
	{ oscGlide , "Glide" },
	{ oscGlideRate , "Glide rate" },
	{ oscPhase , "Phase" },
	{ oscClockSync , "Clock Sync" },
	{ oscNoTrack , "No track" },
	{ oscLow , "Low" },
	{ oscCoarseSync , "Coarse (Sync)" },
	{ oscCoarseNoTrack , "Coarse (No Track)" }}},

	{ ElementType::Rotor,2,		"Rotor" , {
	{ rotCoarse , "Coarse" },
	{ rotFine , "Fine" },
	{ rotXFade , "XFade" },
	{ rotPhaseSync , "Sync" },
	{ rotPhase , "Phase" },
	{ rotClockSync , "Clock Sync" },
	{ rotNoTrack , "No track" },
	{ rotLow , "Low" },
	{ rotCoarseSync , "Coarse (Sync)" },
	{ rotCoarseNoTrack , "Coarse (No Track)" }}},

	{ ElementType::AM,2, "AM", {
	{ amAlgorithm , "Algorithm" },
	{ amOffset , "Offset" }}},

	{ ElementType::Vector,2,		"Vector" , {
	{ vecXOffset , "X offset" },
	{ vecYOffset , "Y offset" }}},

	{ ElementType::Mixer,4,		"Mixer" , {
	{ mixOutputLevel , "Output level" }}},

	{ ElementType::Filter,4,		"Filter" , {
	{ filterType , "Type" },
	{ filterMM1Mode , "MM1 mode" },
	{ filterVowel1 , "Vowel 1" },
	{ filterVowel2 , "Vowel 2" },
	{ filterVowel3 , "Vowel 3" },
	{ filterVowel4 , "Vowel 4" },
	{ filterVowel5 , "Vowel 5" },
	{ filterCutoff , "Cutoff" },
	{ filterResonance , "Resonance" },
	{ filterDamping , "Damping" },
	{ filterXFade , "XFade" },
	{ filterKeytracking , "Key tracking" },
	{ filterKeycenter , "Key center" },
	{ filterSSMMode , "SSM mode" },
	{ filterMiniMode , "Mini mode" },
	{ filterObieMode , "Obie mode" },
	{ filterCombMode , "Comb mode" },
	{ filterBypassMode , "Bypass mode" }}},

	{ ElementType::InsertFX,4,	"InsertFX" , {
	{ insFxMode , "Mode" },
	{ insFxValue , "Value" }}},

	{ ElementType::VCA,4,			"VCA" , {
	{ vcaType , "Type" },
	{ vcaBoost , "Boost" },
	{ vcaLevel , "Level" },
	{ vcaInitPan , "InitPan" }}},

	// VCAMaster has no editable parameters except enable part, which is handled as a special case in the code.
	{ ElementType::VCAMaster,1,	"" , {}},		// Note we don't ever need the name "VCAMaster" to appear.

	{ ElementType::PinkNoise,1,	"Pink Noise" },
	{ ElementType::WhiteNoise,1,	"White Noise" },
	{ ElementType::ExtInput,1,	"ExtInput" },
	{ ElementType::SPDIF_In,1,	"SPDIF_In" },
	{ ElementType::SPDIF_Out,1,	"SPDIF_Out" },

	{ ElementType::LFO,4,			"LFO" , {
	{ lfoShape , "Shape" },
	{ lfoRate , "Rate" },
	{ lfoPhase , "Phase" },
	{ lfoDelayStart , "DelayStart" },
	{ lfoFadeIn , "FadeIn" },
	{ lfoFadeOut , "FadeOut" },
	{ lfoLevel , "Level" },
	{ lfoClockSync , "ClockSync" },
	{ lfoOffset , "Offset" },
	{ lfoRetrigger , "Retrigger" },
	{ lfoRateSync , "RateSync" }}},

	{ ElementType::VibratoLFO,1,	"VibratoLFO", {
	{ vlfoShape , "Shape" },
	{ vlfoRate , "Rate" },
	{ vlfoPhase , "Phase" },
	{ vlfoDelayStart , "DelayStart" },
	{ vlfoFadeIn , "FadeIn" },
	{ vlfoFadeOut , "FadeOut" },
	{ vlfoLevel , "Level" },
	{ vlfoClockSync , "ClockSync" },
	{ vlfoOffset , "Offset" },
	{ vlfoRetrigger , "Retrigger" },
	{ vlfoRateSync , "RateSync" },
	{ vlfoModWheel , "ModWheel" },
	{ vlfoModWheelMax , "ModWheelMax" }}},

	{ ElementType::EG,6,		"EG", {
	{ egDelay , "Delay" },
	{ egAttack , "Attack" },
	{ egDecay , "Decay" },
	{ egSustain , "Sustain" },
	{ egRelease , "Release" },
	{ egASlope , "Att Slope" },
	{ egDSlope , "Dec Slope" },
	{ egSSlope , "Sus Slope" },
	{ egRSlope , "Rel Slope" },
	{ egVelocity , "Velocity" }} },

	{ ElementType::LoopEG,1,		"LoopEG" ,{
	{ legTime1 , "Time 1" },
	{ legTime2 , "Time 2" },
	{ legTime3 , "Time 3" },
	{ legTime4 , "Time 4" },
	{ legTime5 , "Time 5" },
	{ legTime6 , "Time 6" },
	{ legTime7 , "Time 7" },
	{ legTime8 , "Time 8" },
	{ legXLevel1 , "X Level 1" },
	{ legXLevel2 , "X Level 2" },
	{ legXLevel3 , "X Level 3" },
	{ legXLevel4 , "X Level 4" },
	{ legXLevel5 , "X Level 5" },
	{ legXLevel6 , "X Level 6" },
	{ legXLevel7 , "X Level 7" },
	{ legXLevel8 , "X Level 8" },
	{ legYLevel1 , "Y Level 1" },
	{ legYLevel2 , "Y Level 2" },
	{ legYLevel3 , "Y Level 3" },
	{ legYLevel4 , "Y Level 4" },
	{ legYLevel5 , "Y Level 5" },
	{ legYLevel6 , "Y Level 6" },
	{ legYLevel7 , "Y Level 7" },
	{ legYLevel8 , "Y Level 8" },
	{ legStart , "Start" },
	{ legKeyOff , "KeyOff" },
	{ legSlope , "Slope" },
	{ legSync , "Sync" },
	{ legRepeat , "Repeat" },
	{ legLoop , "Loop" } }},

	{ ElementType::Velocity,1,	"Velocity" },
	{ ElementType::Aftertouch,1,	"Aftertouch" },
	{ ElementType::Note,1,		"Note" },
	{ ElementType::ModWheel,1,	"Note" },
	{ ElementType::AT_ModWheel,1,	"AT_ModWheel" },

	{ ElementType::Ribbon,1,		"Ribbon",{
	{ ribOffset , "Offset" },
	{ ribIntensity , "Intensity" },
	{ ribHold , "Hold" },
	{ ribTouchOffset , "Touch Offset" }} },

	{ ElementType::JoyX,1,		"JoyX" },
	{ ElementType::JoyY,1,		"JoyY" },
	{ ElementType::CC,5,		"CC" },

	{ ElementType::Seq,4,		"Seq" , {
	{ seqPatternLength , "PatLength" }} },

	{ ElementType::Pedal,2,		"Pedal" },
	{ ElementType::AssignButton,2,"Assign Button" },

	{ ElementType::EnvFol,1,		"Env Follower", {
	{ envFolAttack , "Attack" },
	{ envFolRelease , "release" },
	{ envFolInput , "Input" },
	{ envFolOutput , "Output" }} },

	{ ElementType::KeyTable,4,		"Key Table" },
	{ ElementType::PolyAT,1,		"Poly Aftertouch" },

	{ ElementType::Lag,4,			"Lag", {
	{ lagTime , "Time" }} },

	{ ElementType::Breath,1,		"Breath" },
	{ ElementType::MaxValue,1,		"Max Value" },
	{ ElementType::KeyTracking,1,	"Key Tracking" },

	{ ElementType::FXChannel,4,	"FXChannel" , {
	{ fxChannelSlot1 , "Slot 1" },
	{ fxChannelSlot2 , "Slot 2" },
	{ fxChannelSlot3 , "Slot 3" },
	{ fxChannelSlot4 , "Slot 4" }} },

	{ ElementType::ChorusFlanger,1,"ChorusFlanger" ,{
	{ cfpMode , "Mode" },
	{ cfpFrequency , "Freq" },
	{ cfpDepth , "Depth" },
	{ cfpPhase , "Phase" },
	{ cfpOffset , "Offset" },
	{ cfpInLevel , "InLevel" },
	{ cfpFeedback , "Feedback" },
	{ cfpDry , "Dry" },
	{ cfpWet , "Wet" },
	{ cfpInput , "Input" }} },

	{ ElementType::Phaser,1,		"Phaser" ,{
	{ cfpMode , "Mode" },
	{ cfpFrequency , "Freq" },
	{ cfpDepth , "Depth" },
	{ cfpPhase , "Phase" },
	{ cfpOffset , "Offset" },
	{ cfpInLevel , "InLevel" },
	{ cfpFeedback , "Feedback" },
	{ cfpDry , "Dry" },
	{ cfpWet , "Wet" },
	{ cfpInput , "Input" } } },

	{ ElementType::Delay,1,		"Delay" ,{
	{ delMode , "Mode" },
	{ delTimeSecondsL , " Time L" },
	{ delTimeSecondsR , "Time R" },
	{ delTimeBeatsL , "Beats L" },
	{ delTimeBeatsR , "Beats R" },
	{ delFeedL , "Feedback L" },
	{ delFeedR , "Feedback R" },
	{ delDamping , "Damping" },
	{ delDry , "Dry" },
	{ delWet , "Wet" },
	{ delMidiClk , "MIDI Clk" },
	{ delInput , "Input" }} },

	{ ElementType::EQ,1,		"EQ" , {
	{ eqMode , "Mode" },
	{ eqFreq1 , "Freq 1" },
	{ eqQ1 , "Q 1" },
	{ eqGain1 , "Gain 1" },
	{ eqFreq2 , "Freq 2" },
	{ eqQ2 , "Q 2" },
	{ eqGain2 , "Gain 2" },
	{ eqFreq3 , "Freq 3" },
	{ eqQ3 , "Q 3" },
	{ eqGain3 , "Gain 3" },
	{ eqInput , "Input" }} },

	{ ElementType::Output,4,		"Output" },

	{ ElementType::Arpeggiator,1,	"Arp" ,{
	{ arpStepVolumeOffset , "Step Volume" },
	{ arpStepGateLengthOffset , "Step Gate Length" },
	{ arpStepGateOffset , "Step Gate" },
	{ arpMode , "Mode" },
	{ arpOctave , "Octave" },
	{ arpPattern , "Pattern" },
	{ arpResolution , "Resolution" },
	{ arpNoteLength , "Note Length" },
	{ arpVelocity , "Velocity" },
	{ arpHold , "Hold" },
	{ arpPatternLength , "Pattern Length" },
	{ arpSwing , "Swing" }} },

	{ ElementType::Home,1,		"" ,{
	{ gloPitchWheelUpRange , "PW Up Range" },
	{ gloPitchWheelDownRange , "PW Down Range" },
	{ gloBPM , "BPM" },
	{ gloGldType , "GldType" },
	{ gloGldMode , "GldMode" },
	{ gloGldRange , "GldRange" },
	{ gloGldTime , "GldTime" },
	{ gloGldRate , "GldRate" },
	{ gloPlayMode , "PlayMode" },
	{ gloLegato , "Legato" },
	{ gloUniVoice , "UniVoice" },
	{ gloNotePriority , "NotePriority" },
	{ gloEgReset , "EgReset" },
	{ gloUniTune , "UniTune" },
	{ gloOutput12 , "Output 1/2" },
	{ gloOutput34 , "Output 3/4" },
	{ gloOutput56 , "Output 4/5" },
	{ gloOutput78 , "Output 5/6" },
	{ gloUnisonButton , "UnisonButton" },
	{ gloSeqButton , "SeqButton" },
	{ gloArpButton , "ArpButton" },
	{ gloAssignState1 , "Assign 1 Button" },
	{ gloAssignState2 , "Assign 2 Button" },
	{ gloAssignFunc1 , "Assign 1" },
	{ gloAssignFunc2 , "Assign 2" },
	{ gloAssignMode1 , "AssignMode 1" },
	{ gloAssignMode2 , "AssignMode 2" },
	{ gloExpPedal , "ExpPedal" },
	{ gloSusPedal1 , "SusPedal 1" },
	{ gloSusPedal2 , "SusPedal 2" },
	{ gloCategory1 , "Category 1" },
	{ gloCategory2 , "Category 2" },
	{ gloPerfKnob1Amt , "PerfKnob 1 Amt" },
	{ gloPerfKnob2Amt , "PerfKnob 2 Amt" },
	{ gloPerfKnob3Amt , "PerfKnob 3 Amt" },
	{ gloPerfKnob4Amt , "PerfKnob 4 Amt" },
	{ gloPerfKnob5Amt , "PerfKnob 5 Amt" },
	{ gloPerfKnob1Src , "PerfKnob 1 Src" },
	{ gloPerfKnob2Src , "PerfKnob 2 Src" },
	{ gloPerfKnob3Src , "PerfKnob 3 Src" },
	{ gloPerfKnob4Src , "PerfKnob 4 Src" },
	{ gloPerfKnob5Src , "PerfKnob 5 Src" }} },

	{ ElementType::SeqGlobal,1,	"Seq" , {
	{ seqMode , "Mode" },
	{ seqDivision , "Division" },
	{ seqPattern , "Pattern" },
	{ seqSwing , "Swing" }} },

	{ ElementType::Gate,1,		"Gate" }, 

	{ ElementType::Off,0,		"Off" }

};


std::string GetElementName(const ElementID& elemID, std::vector<ElementName>::const_iterator elemIter)
{
	std::string name = elemIter->name;
	if (elemIter->count > 1 && !name.empty())
	{
		if (elemID.elemType == ElementType::Seq)
		{
			static const char* letters[4] = { " A"," B"," C"," D" };
			name += letters[elemID.elemIndex];
		}
		else
		{
			name += " " + std::to_string(elemID.elemIndex + 1);
		}
	}
	return name;
}


std::string GetElementName(const ElementID& elemID)
{
	auto elemIter = std::find_if(elementNames.begin(), elementNames.end(), [&elemID](auto& item) { return item.elemType == elemID.elemType;});
	assert(elemIter != elementNames.end());
	return GetElementName(elemID, elemIter);
}


std::string GetParameterName(const ParamID& paramID, std::vector<ElementName>::const_iterator elemIter)
{
	std::string name;

	if (paramID.portType == PortType::None)
	{
			if (paramID.elemType == ElementType::Arpeggiator && paramID.subIndex < 96)
			{
				// Handle arpeggiator steps
				name += "Step ";
				name += std::to_string((paramID.subIndex % 32) + 1) + " ";
				name += (paramID.subIndex < arpStepVolumeOffset + 32) ? "Volume" :
					(paramID.subIndex < arpStepGateLengthOffset + 32) ? "Gate Length" : "Gate";
			}
			else if (paramID.elemType == ElementType::Seq && (paramID.subIndex >= seqStep1) && (paramID.subIndex <= seqStep16))
			{
				// Handle Sequencer steps
				name += "Step ";
				name += std::to_string((paramID.subIndex - seqStep1) + 1);
			}
			else if (paramID.elemType == ElementType::KeyTable)
			{
				// Handle KeyTable key numbers
				name += "Key ";
				name += std::to_string(paramID.subIndex);
			}
			else
			{
				auto paramIter = std::find_if(elemIter->paramNames.begin(), elemIter->paramNames.end(), [paramID](auto& item) { return item.param == paramID.subIndex;});
				if (paramIter != elemIter->paramNames.end())
				{
					name += paramIter->name;
				}
			}
	}
	else
	{
		// Special case for enable part. They look like regular input source ports but we want to name them as enable part.
		if (paramID.elemType == ElementType::VCAMaster && paramID.portType == PortType::Input && paramID.subIndex == psiSource)
		{
			name += "EnablePart ";
			name += std::to_string(paramID.portIndex + 1);
		}
		else
		{
			switch (paramID.portType)
			{
			case PortType::Input:	name += "Input "; break;
			case PortType::Mod:	name += "Mod "; break;
			case PortType::Control: name += "Control "; break;
			case PortType::Output: name += "Output "; break;
			case PortType::None: assert(false); break;
			}

			name += std::to_string(paramID.portIndex + 1);

			switch (paramID.subIndex)
			{
			case 0: name += " src"; break;
			case 1: name += " amt"; break;
			case 2: name += " dest"; break;
			case 3: name += " amt(pitch)"; break;
			};
		}
	}
	return name;
}

std::string GetParameterName(const ParamID& paramID)
{
	auto elemIter = std::find_if(elementNames.begin(), elementNames.end(), [&paramID](auto& item) { return item.elemType == paramID.elemType;});
	assert(elemIter != elementNames.end());
	return GetParameterName(paramID, elemIter);
}

std::string GetParameterDetail(const ParamID& paramID)
{
	auto elemIter = std::find_if(elementNames.begin(), elementNames.end(), [&paramID](auto& item) { return item.elemType == paramID.elemType;});
	assert(elemIter != elementNames.end());

	std::string name = GetElementName({ paramID.elemType, paramID.elemIndex }, elemIter);
	name += " ";

	name += GetParameterName(paramID, elemIter);

	return name;
};
