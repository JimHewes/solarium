/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <string>
#include "EditHistory.h"
#include "Properties.h"
#include "AppCmdMgr.h"
#include "Command.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		EditHistoryCustomCell
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////


EditHistoryCustomCell::EditHistoryCustomCell(CommandHistory* pCommandHistory, CellDragFunctiontype cellDragFunction)
	:m_pCommandHistory(pCommandHistory)
	, m_cellDragFunction(cellDragFunction)
{}

inline void EditHistoryCustomCell::setRowAndColumn(const int rowNumber, const int columnId)
{
	m_rowNumber = rowNumber;
	m_columnId = columnId;
}

void EditHistoryCustomCell::paint(Graphics & g)
{
	////////////////////////////////////////////////
	//		Paint Background
	////////////////////////////////////////////////

	int width = getWidth();
	int height = getHeight();

	g.setColour(Colours::grey);
	if (m_columnId == 1)
	{
		g.drawFittedText(std::to_string(m_rowNumber + 1), 0, 0, width, height, Justification::centredLeft, 1);
	}
	else
	{
		// rowNumber might be greater than getNumRows() according to docs. So don't assume it isn't.
		if (m_rowNumber + 1 < static_cast<int>(m_pCommandHistory->GetHistoryLength()))
		{
			Command* pCmd = m_pCommandHistory->GetCommandAtRevision(gsl::narrow<Revision>(m_rowNumber + 1));
			auto currentVersion = m_pCommandHistory->GetCurrentVersion();
			auto currentRevision = static_cast<int>(currentVersion.revision);
			if (m_rowNumber < currentRevision)
			{
				g.setColour(Colours::whitesmoke);
			}
			if (m_columnId == 2)
			{
				// draw the text and the right-hand-side dividing line between cells
				g.drawFittedText(pCmd->GetAction(), 0, 0, width, height, Justification::centredLeft, 1);
				// draw the divider on the right side of the column
				//g.setColour(Colours::black.withAlpha(0.5f));
				//g.drawLine(width, 0, width, height);
			}
			else if (m_columnId == 3)
			{
				g.drawFittedText(pCmd->GetTime(), 0, 0, width, height, Justification::centredLeft, 1);
			}
			else if (m_columnId == 4)
			{
				g.drawFittedText(pCmd->GetDescription(), 0, 0, width, height, Justification::centredLeft, 1);
			}
			else if (m_columnId == 5)
			{
				auto list = m_pCommandHistory->GetBookmarksAtVersion(
					Version(gsl::narrow<Revision>(m_rowNumber + 1), currentVersion.branchTag));
				std::string str;
				for (size_t i = 0; i < list.size(); ++i)
				{
					str += std::to_string(list[i]);
					str += (i + 1 < list.size()) ? ", " : "";
				}

				const uint32 lightSkyBlue = 0xffa5b8d8;
				g.setColour(Colour(lightSkyBlue));
				g.drawFittedText(str, 0, 0, width, height, Justification::centred, 1);
			}
			else if (m_columnId == 6)
			{
				Version branchPoint = m_pCommandHistory->GetBranchPoint(currentVersion.branchTag);
				if (branchPoint != NO_VERSION)
				{
					if (gsl::narrow<Revision>(m_rowNumber + 1) == branchPoint.revision)
					{
						g.setColour(Colour(0xFF5995DB));
						g.drawFittedText(m_pCommandHistory->GetBranchName(branchPoint.branchTag), 0, 0, width, height, Justification::centred, 1);
					}
					if (gsl::narrow<Revision>(m_rowNumber + 1) == branchPoint.revision + 1)
					{
						g.setColour(Colour(0xFF5995DB));
						g.drawFittedText(m_pCommandHistory->GetBranchName(currentVersion.branchTag), 0, 0, width, height, Justification::centred, 1);
					}
				}
			}
		}
	}
	return;

}

void EditHistoryCustomCell::mouseDown(const MouseEvent & )
{
	m_pCommandHistory->SetRevision(gsl::narrow<Revision>(m_rowNumber + 1));
}

void EditHistoryCustomCell::mouseDrag(const MouseEvent & )
{
	//DBG("Mouse drag: row = " << m_rowNumber << ", y = " << event.y);
	m_cellDragFunction();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		EditHistoryListBoxModel
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////


EditHistoryListBoxModel::EditHistoryListBoxModel(CommandHistory* pCommandHistory, CellDragFunctiontype cellDragFunction)
	:m_pCommandHistory(pCommandHistory)
	, m_cellDragFunction(cellDragFunction)
{
}

 int EditHistoryListBoxModel::getNumRows(void)
{
	return m_pCommandHistory->GetHistoryLength() - 1;
}

void EditHistoryListBoxModel::paintRowBackground(Graphics &g, int rowNumber, int width, int height, bool rowIsSelected)
{
	rowNumber; height; rowIsSelected;

	// Colour colour = Colours::slateblue;
	Colour colour(46, 54, 61);
	g.fillAll(colour);

	auto currentVersion = m_pCommandHistory->GetCurrentVersion();

	// Draw a line the indicate where this branch came off of the previous one.
	auto branchPoint = m_pCommandHistory->GetBranchPoint(currentVersion.branchTag);
	if (branchPoint != NO_VERSION)
	{
		g.setColour(Colour(0xFF5995DB));
		if (rowNumber == static_cast<int>(branchPoint.revision))
		{
			// draw a divider between rows at the top
			g.drawLine(0, 0, static_cast<float>(width), 0);
		}
		else if (rowNumber + 1 == static_cast<int>(branchPoint.revision))
		{
			// draw a divider between rows at the bottom
			g.drawLine(0, static_cast<float>(height), static_cast<float>(width), static_cast<float>(height));
		}
	}

	// Draw a line the indicate the current version
	g.setColour(Colour(0xFFDB9559));
	if (rowNumber == static_cast<int>(currentVersion.revision))
	{
		// draw a divider between rows at the top
		g.drawLine(0, 0, static_cast<float>(width), 0);
	}
	else if (rowNumber + 1 == static_cast<int>(currentVersion.revision))
	{
		// draw a divider between rows at the bottom
		g.drawLine(0, static_cast<float>(height), static_cast<float>(width), static_cast<float>(height));
	}
	return;
}

void EditHistoryListBoxModel::paintCell(Graphics &g, int rowNumber, int columnId, int width, int height, bool rowIsSelected)
{
	g; rowNumber; columnId; width; height; rowIsSelected;
}

Component * EditHistoryListBoxModel::refreshComponentForCell(int rowNumber, int columnId, bool isRowSelected, Component * existingComponentToUpdate)
{
	isRowSelected;

	EditHistoryCustomCell* pCustomCell = static_cast<EditHistoryCustomCell*> (existingComponentToUpdate);
	// If an existing component is being passed-in for updating, we'll re-use it, but
	// if not, we'll have to create one.
	if (pCustomCell == nullptr)
		pCustomCell = new EditHistoryCustomCell(m_pCommandHistory, m_cellDragFunction);

	pCustomCell->setRowAndColumn(rowNumber, columnId);
	return pCustomCell;
}


EditHistoryListBox::EditHistoryListBox(EditHistoryListBoxModel * pEditHistoryListModel, CommandHistory* pCommandHistory)
	:TableListBox("EditHistoryListBox", pEditHistoryListModel)
	, m_pCommandHistory(pCommandHistory)
{
	setWantsKeyboardFocus(true);
}

//
void EditHistoryListBox::Update()
{
	this->updateContent();	//  <--- only updates if the number of rows has changed
	repaint();				//  <--- updates even if the number of rows hasn't changed.
	auto historyLength = m_pCommandHistory->GetHistoryLength();
	if (historyLength > 0)
	{
		auto currentRevision = m_pCommandHistory->GetCurrentRevision();
		scrollToEnsureRowIsOnscreen(currentRevision - 1);
	}
}


void EditHistoryListBox::mouseDown(const MouseEvent & event)
{
	event;
	auto historyLength = m_pCommandHistory->GetHistoryLength();
	if (historyLength > 0)
	{
		m_pCommandHistory->SetRevision(gsl::narrow<Revision>(historyLength - 1));
	}
}
void EditHistoryListBox::mouseDrag(const MouseEvent & event)
{
	event;
	DBG("MouseDrag in Listbox");
	OnCellDrag();
}

bool EditHistoryListBox::keyPressed(const KeyPress & key)
{
	bool result{ true };

	if (key.isKeyCode(KeyPress::upKey))
	{
		m_pCommandHistory->Undo();
	}
	else if (key.isKeyCode(KeyPress::downKey))
	{
		m_pCommandHistory->Redo();
	}
	else if (key.isKeyCode(KeyPress::endKey))
	{
		auto historyLength = m_pCommandHistory->GetHistoryLength();
		if (historyLength > 0)
		{
			m_pCommandHistory->SetRevision(gsl::narrow<Revision>(historyLength - 1));
		}
	}
	else
	{
		result = false;
	}

	return result;
}

void EditHistoryListBox::OnCellDrag()
{
	auto localMousePos = getMouseXYRelative();

	// We don't care what the X cordinate is so just ensure it's within the listbox width (i.e. zero)
	int ypos = localMousePos.getY();
	int rowNumberUnderMouse = getRowContainingPosition(0, ypos);

	DBG("localX = " << localMousePos.getX() << ", localY = " << ypos << ", width = " << getWidth());
	DBG("Row under mouse = " << rowNumberUnderMouse);

	if (rowNumberUnderMouse == 0 && ypos < 27)	// mouse is in listbox title (or above) and list is already scrolled up to lowest point.
	{
		m_pCommandHistory->SetRevision(0);
	}
	else if(rowNumberUnderMouse == -1)
	{
		// For drags that start above the listbox, don't do anything until the drag reaches the rows of the listbox.
		if (ypos >= 27)
		{
			auto historyLength = m_pCommandHistory->GetHistoryLength();
			if (historyLength > 0)
			{
				Revision revision = gsl::narrow<Revision>(ypos < 27 ? 0 : historyLength - 1);
				m_pCommandHistory->SetRevision(revision);
			}
		}
	}
	else
	{
		m_pCommandHistory->SetRevision(gsl::narrow<Revision>(rowNumberUnderMouse + 1));
	}
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		EditHistoryContentComponent
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

EditHistoryContentComponent::EditHistoryContentComponent(Preset * pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget)
	: m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	, m_pDefaultCommandTarget(pDefaultCommandTarget)
{
	assert(m_pPreset);
	assert(m_pCommandHistory);
	assert(m_pDefaultCommandTarget);

	m_branchLabel.setBounds(6,3,60,20);
	m_branchLabel.setColour(juce::Label::textColourId , Colours::whitesmoke);
	m_branchLabel.setText("Branch:", juce::NotificationType::dontSendNotification);
	addAndMakeVisible(m_branchLabel);

	m_branchComboBox.setBounds( m_branchLabel.getRight(), 3, 100, 20 );	// temporary, will be changed in resized()
	m_branchComboBox.addListener(this);
	addAndMakeVisible(m_branchComboBox);

	m_renameBranchButton.setButtonText("Rename");
	m_renameBranchButton.setBounds(m_branchComboBox.getRight() + 5,4,60,20);
	m_renameBranchButton.addListener(this);
	addAndMakeVisible(m_renameBranchButton);
	
	m_deleteBranchButton.setButtonText("Delete");
	m_deleteBranchButton.setBounds(m_renameBranchButton.getRight() + 5,4,60,20);
	m_deleteBranchButton.addListener(this);
	addAndMakeVisible(m_deleteBranchButton);

	m_pListBox = std::make_unique<EditHistoryListBox>( nullptr, pCommandHistory);
	m_pListBoxModel = std::make_unique<EditHistoryListBoxModel>(pCommandHistory, [this]() { m_pListBox->OnCellDrag(); });
	
	// Set the model afterwards instead of in the constructor so that we have a valid m_pListBox pointer for the lambda.
	m_pListBox->setModel(m_pListBoxModel.get());

	m_pListBox->setColour(ListBox::backgroundColourId, Colour(46, 54, 61));


	int propFlags = TableHeaderComponent::notSortable;
	auto& header = m_pListBox->getHeader();
	header.addColumn("#",		1, 30, 30, -1, propFlags);
	header.addColumn("Action",	2, 100, 30, -1, propFlags);
	header.addColumn("Time",	3, 100, 30, -1, propFlags);
	header.addColumn("Details",	4, 200, 30, -1, propFlags);
	header.addColumn("BM",		5, 30, 30, -1, propFlags);
	header.addColumn("Branch",	6, 70, 30, -1, propFlags);

	header.setColumnWidth(1, pPropertiesFile->getValue(propHistoryWindowCol1Width, "30").getIntValue());
	header.setColumnWidth(2, pPropertiesFile->getValue(propHistoryWindowCol2Width, "100").getIntValue());
	header.setColumnWidth(3, pPropertiesFile->getValue(propHistoryWindowCol3Width, "100").getIntValue());
	header.setColumnWidth(4, pPropertiesFile->getValue(propHistoryWindowCol4Width, "200").getIntValue());
	header.setColumnWidth(5, pPropertiesFile->getValue(propHistoryWindowCol5Width, "30").getIntValue());
	header.setColumnWidth(6, pPropertiesFile->getValue(propHistoryWindowCol6Width, "70").getIntValue());
	header.setColumnVisible(1, pPropertiesFile->getValue(propHistoryWindowCol1Visible, "1").getIntValue() != 0);
	header.setColumnVisible(2, pPropertiesFile->getValue(propHistoryWindowCol2Visible, "1").getIntValue() != 0);
	header.setColumnVisible(3, pPropertiesFile->getValue(propHistoryWindowCol3Visible, "1").getIntValue() != 0);
	header.setColumnVisible(4, pPropertiesFile->getValue(propHistoryWindowCol4Visible, "1").getIntValue() != 0);
	header.setColumnVisible(5, pPropertiesFile->getValue(propHistoryWindowCol5Visible, "1").getIntValue() != 0);
	header.setColumnVisible(6, pPropertiesFile->getValue(propHistoryWindowCol6Visible, "1").getIntValue() != 0);

	header.addListener(this);
	addAndMakeVisible(m_pListBox.get());

	setSize(pPropertiesFile->getValue(propHistoryWindowWidth).getIntValue(),
		pPropertiesFile->getValue(propHistoryWindowHeight).getIntValue());

	m_editHistoryChangedConnection = pCommandHistory->EditHistoryChangedEvent.Connect([this, pCommandHistory](BranchTag branchTag)
	{
		// If branchTag is not NO_BRANCH, it means the branch may have changed. So update the branch list.
		if (branchTag != NO_BRANCH)
		{
			this->UpdateBranchList(branchTag);
		}
		m_pListBox->Update();
	});

	UpdateBranchList(m_pCommandHistory->GetCurrentBranchTag());
}

void EditHistoryContentComponent::resized()
{
	auto lb = getLocalBounds();
	lb.removeFromTop(30);
	lb.removeFromLeft(8);
	lb.removeFromRight(8);
	lb.removeFromBottom(24);
	
	m_pListBox->setBounds(lb);
	
	m_branchComboBox.setBounds( m_branchLabel.getRight(), 3, 
		std::max(100, lb.getWidth() - 250), 20 );
	m_renameBranchButton.setBounds(m_branchComboBox.getRight() + 5,4,60,20);
	m_deleteBranchButton.setBounds(m_renameBranchButton.getRight() + 5,4,60,20);

	pPropertiesFile->setValue(propHistoryWindowWidth, getWidth());
	pPropertiesFile->setValue(propHistoryWindowHeight, getHeight());
}

void EditHistoryContentComponent::paint(Graphics &g)
{
	g.setColour(Colour(46, 54, 61));
	g.fillAll();
}

ApplicationCommandTarget* EditHistoryContentComponent::getNextCommandTarget()
{
	return m_pDefaultCommandTarget;
}

void EditHistoryContentComponent::getAllCommands(Array<CommandID>& commands)
{
	// this returns the set of all commands that this target can perform..
	const CommandID ids[] = {
		EditHistory
	};
	commands.addArray(ids, numElementsInArray(ids));
}

void EditHistoryContentComponent::getCommandInfo(CommandID commandID, juce::ApplicationCommandInfo& result)
{
	const String editCategory("Edit");

	switch (commandID)
	{
	case EditHistory:
		result.setInfo("History...", "Open the edit history window.", editCategory, 0);
		result.addDefaultKeypress('h', ModifierKeys::commandModifier);
		break;
	default:
		break;
	};
}

bool EditHistoryContentComponent::perform(const InvocationInfo & info)
{
	switch (info.commandID)
	{
	case EditHistory:
		{	// Pressing the key command for opening the Edit History Window when it's open and to the front will then close it.
			DocumentWindow *pWindow = dynamic_cast<DocumentWindow*>(getParentComponent());
			assert(pWindow);
			if (pWindow)
			{
				pWindow->closeButtonPressed();
			}
		}
		break;
	default:
		return false;
	};
	return true;
}

void EditHistoryContentComponent::mouseDown(const MouseEvent & event)
{
	// Don't do anything if mouse Y position is above the listbox.
	if (event.getMouseDownPosition().getY() > m_pListBox->getBounds().getTopLeft().getY())
	{
		auto historyLength = m_pCommandHistory->GetHistoryLength();
		if (historyLength > 0)
		{
			m_pCommandHistory->SetRevision(gsl::narrow<Revision>(historyLength - 1));
		}
	}
}

void EditHistoryContentComponent::mouseDrag(const MouseEvent & event)
{
	DBG("MouseDrag in Component");
	event;
	m_pListBox->OnCellDrag();
}

void EditHistoryContentComponent::tableColumnsChanged(juce::TableHeaderComponent *pHeader)
{
	pPropertiesFile->setValue(propHistoryWindowCol1Visible, pHeader->isColumnVisible(1));
	pPropertiesFile->setValue(propHistoryWindowCol2Visible, pHeader->isColumnVisible(2));
	pPropertiesFile->setValue(propHistoryWindowCol3Visible, pHeader->isColumnVisible(3));
	pPropertiesFile->setValue(propHistoryWindowCol4Visible, pHeader->isColumnVisible(4));
	pPropertiesFile->setValue(propHistoryWindowCol5Visible, pHeader->isColumnVisible(5));
	pPropertiesFile->setValue(propHistoryWindowCol6Visible, pHeader->isColumnVisible(6));
}

void EditHistoryContentComponent::tableColumnsResized(juce::TableHeaderComponent *pHeader)
{
	pPropertiesFile->setValue(propHistoryWindowCol1Width, pHeader->getColumnWidth(1));
	pPropertiesFile->setValue(propHistoryWindowCol2Width, pHeader->getColumnWidth(2));
	pPropertiesFile->setValue(propHistoryWindowCol3Width, pHeader->getColumnWidth(3));
	pPropertiesFile->setValue(propHistoryWindowCol4Width, pHeader->getColumnWidth(4));
	pPropertiesFile->setValue(propHistoryWindowCol5Width, pHeader->getColumnWidth(5));
	pPropertiesFile->setValue(propHistoryWindowCol6Width, pHeader->getColumnWidth(6));
}


void EditHistoryContentComponent::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	size_t indexToBranchList = static_cast<size_t>(comboBoxThatHasChanged->getSelectedId() - 1);

	auto branchList = m_pCommandHistory->GetBranchList();
	assert(indexToBranchList < branchList.size());

	BranchTag branchTag = branchList[indexToBranchList].branchTag;

	m_pCommandHistory->SetCurrentBranchTag(branchTag);
	auto length = m_pCommandHistory->GetHistoryLength(branchTag);
	m_pCommandHistory->SetVersion(Version( gsl::narrow<Revision>(length - 1), branchTag ));

	// SetVersion will trigger an EditHistoryChanged event which will update the ListBox. So no need to do it here.
	//m_pListBox->Update();
}

void EditHistoryContentComponent::buttonClicked(Button* buttonThatWasClicked)
{
	if (buttonThatWasClicked == &m_renameBranchButton)
	{
		juce::AlertWindow 	w("Rename Branch",
			"Enter the new name of the branch:",
			juce::AlertWindow::AlertIconType::NoIcon,
			this);
		w.addTextEditor("RenSS", m_branchComboBox.getText());
		auto* pEditor = w.getTextEditor("RenSS");
		assert(pEditor);
		pEditor->setInputRestrictions(20, "");
		w.addButton("OK", 1, KeyPress(KeyPress::returnKey, 0, 0));
		w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));

		if (0 != w.runModalLoop())
		{
			std::string newName = pEditor->getText().toStdString();
			m_pCommandHistory->SetBranchName(m_pCommandHistory->GetCurrentBranchTag(), newName);	// Will generate update notification.
		}
	}
	else if (buttonThatWasClicked == &m_deleteBranchButton)
	{
		juce::String str = "Are you sure you want to pernamently delete the branch named ";
		str += m_branchComboBox.getText();
		str += " ?";
		if (AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Delete Branch",
			str,
			{},
			{},
			this,
			nullptr))
		{
			size_t indexToBranchList = static_cast<size_t>(m_branchComboBox.getSelectedId() - 1);

			auto branchList = m_pCommandHistory->GetBranchList();
			assert(branchList.size() > 1);
			assert(indexToBranchList < branchList.size());

			BranchTag branchTag = branchList[indexToBranchList].branchTag;

			m_pCommandHistory->DeleteBranch(branchTag);
			UpdateBranchList(m_pCommandHistory->GetCurrentBranchTag());
			Update();
		}
	}
}

void EditHistoryContentComponent::Update()
{
	m_pListBox->Update();
}

void EditHistoryContentComponent::UpdateBranchList(BranchTag branchTag)
{
	// If NotificationType::dontSendNotification is specified in setSelectedId below, then disabling notifications 
	// will not prevent async notifications from occuring. But specifying SYNC notifications will.
	m_branchComboBox.removeListener(this);

	m_branchComboBox.clear();

	auto branchList = m_pCommandHistory->GetBranchList();
	int selectedID{ 0 };
	for (size_t i = 0; i < branchList.size(); ++i)
	{
		m_branchComboBox.addItem(branchList[i].branchName, i + 1);
		if (branchList[i].branchTag == branchTag)
		{
			selectedID = i + 1;
		}
	}
	m_branchComboBox.setSelectedId(selectedID, juce::NotificationType::sendNotificationSync);
	m_branchComboBox.addListener(this);
	
	m_deleteBranchButton.setEnabled(branchList.size() > 1);

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		EditHistoryWindow
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

EditHistoryWindow::EditHistoryWindow(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget, std::function<void(void)> closeFunction)
	: DocumentWindow("Edit History", Colour(0xFFE8E8E8), DocumentWindow::closeButton)
	, m_closeFunction(closeFunction)
{
	setUsingNativeTitleBar(true);
	setResizable(true, true);
	setVisible(true);

	// What we want is for the EditHistory window to act like a tool palette window---it's always on top of the Solarium application 
	// but not on top of other applications. JUCE has no way to do this. Setting AlwaysOnTop() puts the window on top of EVERTHING.
	setAlwaysOnTop(true);

	EditHistoryContentComponent* pEditHistoryContentComponent = new EditHistoryContentComponent(pPreset, pCommandHistory, pDefaultCommandTarget);
	setContentOwned(pEditHistoryContentComponent, true);
	// this lets the command manager use keypresses that arrive in our window to send out commands
	addKeyListener(GetGlobalCommandManager().getKeyMappings());
	GetGlobalCommandManager().registerAllCommandsForTarget(pEditHistoryContentComponent);

}

void EditHistoryWindow::moved()
{
	pPropertiesFile->setValue(propHistoryWindowLeft, getX());
	pPropertiesFile->setValue(propHistoryWindowTop, getY());
}

void EditHistoryWindow::mouseDown(const MouseEvent & event)
{
	event;
}

void EditHistoryWindow::mouseDrag(const MouseEvent & event)
{
	DBG("MouseDrag in Window");
	event;
}

void EditHistoryWindow::Update()
{
	static_cast<EditHistoryContentComponent*>(getContentComponent())->Update();
}
