/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PORTVIEW_H_INCLUDED
#define PORTVIEW_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include <list>
#include "Preset.h"
#include "Notifier.h"

class PortView;
class ConnectorView;
class ConnectionMng;

class PortViewListener
{
public:
	virtual void OnPortViewMoved(PortView* pPortViewThatMoved,Point<int> newPosition) = 0;
	virtual void OnPortViewDestoyed(PortView* pPortViewDestroyed) = 0;
};

class PortView : public Component,
	public DragAndDropTarget,
	public ComponentListener
{
public:
	PortView(PortID portID);
	~PortView();

	Point<int> GetSurfacePosition();
	PortID GetPortID();

	void paint(Graphics& g) override;
	virtual void mouseDrag(const MouseEvent& event) override;
	virtual void mouseUp(const MouseEvent& event) override;
	void OwningNodeWasMoved();
	void AddConnectorViewListener(ConnectorView* pListener);
	void RemoveConnectorViewListener(ConnectorView* pListener);

	bool isInterestedInDragSource(const SourceDetails& dragSourceDetails) override;
	void itemDropped(const SourceDetails& dragSourceDetails) override;
	void itemDragEnter(const SourceDetails& dragSourceDetails) override;
	void itemDragExit(const SourceDetails& dragSourceDetails) override;

	void parentHierarchyChanged() override;
	void componentMovedOrResized(Component& component,bool wasMoved,bool wasResized) override;
	static int GetWidth() { return m_size; }
	static int GetHeight() { return m_size; }

	bool HasConnection() {	return !m_listeners.empty(); }

private:
	void Notify();
	void OnConnectorDragStartStop(PortView* pOtherPortView);
	bool IsDropAllowed(const SourceDetails& dragSourceDetails);
	ConnectionMng* GetOwningConnectionMng();

	Notifier<void(PortView*)>::ConnectionType m_connectorDragStartStopConnection;

	ConnectionMng*		m_pConnectionMng;
	static const int	m_size = 18;	///< width and height of the PortView in pixels
	bool				m_isDragging;
	bool				m_dropAllowed;
	Point<int>			m_center;
	Colour				m_fillColor;
	Colour				m_previousFillColor;	// Used to save the old fill color between drag enter and exit.
	Image				m_image;
	std::list<ConnectorView*>	m_listeners;

	bool				m_isRegisteredWithOwningNode = false;
	PortID				m_portID;
};
using PortViewUPtr = std::unique_ptr < PortView >;


#endif  // PORTVIEW_H_INCLUDED
