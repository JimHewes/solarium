/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef CONTROLPANEL_H_INCLUDED
#define CONTROLPANEL_H_INCLUDED

#include "ViewModel.h"
#include "MiscControls.h"
#include "Preset.h"
#include "Properties.h"
#include "../JuceLibraryCode/JuceHeader.h"

class PanelButton;
class PanelButtonPreset;
class CommandHistory;
class MidiManager;

class PanelButtonSync : public PanelButton,
						public ChangeListener
{
public:
	PanelButtonSync(const std::string& label)
	: PanelButton(label)
	{
		// Register to get notifications when the Properties file changes.
		pPropertiesFile->addChangeListener(this);
		SetState(pPropertiesFile->getValue(propMidiSync) == "on");
	}

	void buttonClicked(Button* pButtonClicked) override
	{
		pButtonClicked;
		SetState(!GetState());	// toggle state
		pPropertiesFile->setValue(propMidiSync, GetState() ? "on" : "off");
		pPropertiesFile->save();
	}

	// Called when the Properties file changes
	void changeListenerCallback(ChangeBroadcaster* source) override
	{
		source;
		assert(source == pPropertiesFile.get());

		SetState(pPropertiesFile->getValue(propMidiSync) == "on");
	}
};

class ControlPanel :	public Component
{
public:
	ControlPanel(Preset* pPreset, CommandHistory* pCommandHistory, ViewModel* pViewModel, MidiManager* pMidiManager);
	void resized() override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*							m_pPreset;
	ViewModel*						m_pViewModel;
	MidiManager*					m_pMidiManager;
	bool							m_syncOn{false};

	std::unique_ptr<Label>				m_pMidiLabel;
	std::unique_ptr<Label>				m_pSendLabel;
	std::unique_ptr<Label>				m_pReceiveLabel;
	std::unique_ptr<TextButton>			m_pReceiveButton;
	std::unique_ptr<TextButton>			m_pSendButton;
	std::unique_ptr<PanelButtonSync>	m_pSyncButton;

	std::unique_ptr<PanelButtonPreset>	m_pAssign1Button;
	std::unique_ptr<PanelButtonPreset>	m_pAssign2Button;
	std::unique_ptr<PanelButtonPreset>	m_pSeqOnButton;
	std::unique_ptr<PanelButtonPreset>	m_pArpOnButton;
	std::unique_ptr<PanelButtonPreset>	m_pUnisonButton;

	std::unique_ptr<Label>				m_pEnablePartLabel;
	std::unique_ptr<PanelButtonPreset>	m_pEnablePart1Button;
	std::unique_ptr<PanelButtonPreset>	m_pEnablePart2Button;
	std::unique_ptr<PanelButtonPreset>	m_pEnablePart3Button;
	std::unique_ptr<PanelButtonPreset>	m_pEnablePart4Button;

	std::unique_ptr<PanelButton>		m_pCompareButton;
	std::unique_ptr<PanelButton>		m_pFXBypassButton;

	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
	Preset::StateChangedConnection				m_stateChangedConnection;

};


#endif  // CONTROLPANEL_H_INCLUDED
