/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <sstream>
#include <vector>
#include <string>
#include <iomanip>
#include "SequencerPanel.h"
#include "Command.h"

using std::vector;
using std::string;


String SliderSeq::getTextFromValue(double value)
{
	if (m_useRest && value > 126.5)
	{
		return "Rest";
	}
	return Slider2::getTextFromValue(value);
}

static const juce::Identifier paramIndexID = "paramIndex";

SequencerPanel::SequencerPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
	,m_stepLabels(16)
	,m_stepSliders(16)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 75;
	const int rightWidth = 105;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_patternLengthLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_patternLengthLabel.setText("PatLen:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_patternLengthLabel);
	m_patternLengthSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_patternLengthSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,0));
	m_patternLengthSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_patternLengthSlider.getProperties().set(paramIndexID, 0);
	m_patternLengthSlider.addListener(this);
	addAndMakeVisible(m_patternLengthSlider);

	std::ostringstream os;
	static char text[] = "Step n:";
	for (int step = 0; step < 16; ++step)
	{
		yPos += rowHeight;
		m_stepLabels[step].setBounds(leftX,yPos,leftWidth,ctrlHeight);
		os.str("");
		os << "Step " << step + 1 << ":";
		m_stepLabels[step].setText(os.str(),NotificationType::dontSendNotification);
		addAndMakeVisible(m_stepLabels[step]);
		m_stepSliders[step].setBounds(rightX,yPos,rightWidth,ctrlHeight);
		m_stepSliders[step].setSliderStyle(Slider2::LinearBar);
		rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, seqStep1 + step));
		m_stepSliders[step].setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_stepSliders[step].setUseRest(elemID.elemIndex == 0);	// only use the word "Rest" for sequencer A
		m_stepSliders[step].getProperties().set(paramIndexID, seqStep1 + step);
		m_stepSliders[step].addListener(this);
		addAndMakeVisible(m_stepSliders[step]);
	}

	ElementID seqCommonElemID{ ElementType::SeqGlobal,0 };

	
	yPos += rowHeight;
	m_modeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_modeLabel.setText("Mode:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_modeLabel);
	m_modeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> seqModeList = m_pPreset->GetParameterListText(ParamID(seqCommonElemID,seqMode));;
	for (size_t i = 0; i < seqModeList.size(); ++i)
	{
		m_modeCombo.addItem(seqModeList[i],i + 1);
	}
	m_modeCombo.addListener(this);
	addAndMakeVisible(m_modeCombo);

	yPos += rowHeight;
	m_divisionLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_divisionLabel.setText("Division:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_divisionLabel);
	m_divisionCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> seqDivList = m_pPreset->GetParameterListText(ParamID(seqCommonElemID,seqDivision));;
	for (size_t i = 0; i < seqDivList.size(); ++i)
	{
		m_divisionCombo.addItem(seqDivList[i],i + 1);
	}
	m_divisionCombo.addListener(this);
	addAndMakeVisible(m_divisionCombo);

	yPos += rowHeight;
	m_patternLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_patternLabel.setText("Pattern:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_patternLabel);
	m_patternSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_patternSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(seqCommonElemID,seqPattern));
	m_patternSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_patternSlider.getProperties().set(paramIndexID, seqPattern);
	m_patternSlider.addListener(this);
	addAndMakeVisible(m_patternSlider);

	yPos += rowHeight;
	m_swingLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_swingLabel.setText("Swing:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_swingLabel);
	m_swingSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_swingSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(seqCommonElemID,seqSwing));
	m_swingSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_swingSlider.setTextValueSuffix(" %");
	m_swingSlider.getProperties().set(paramIndexID, seqSwing);
	m_swingSlider.addListener(this);
	addAndMakeVisible(m_swingSlider);

	yPos += rowHeight;
	m_bpmLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_bpmLabel.setText("BPM:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_bpmLabel);
	m_bpmSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_bpmSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(ElementID(ElementType::Home, 0), gloBPM));
	m_bpmSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_bpmSlider.getProperties().set(paramIndexID, gloBPM);
	m_bpmSlider.addListener(this);
	addAndMakeVisible(m_bpmSlider);

	yPos += rowHeight;
	m_initButton.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_initButton.setButtonText("Init");
	m_initButton.addListener(this);
	addAndMakeVisible(m_initButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if ((paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex) || 
			(paramID.elemType == ElementType::SeqGlobal) ||
			(paramID.elemType == ElementType::Home && paramID.subIndex == gloBPM)
			)
		{
			Update();
		}
	});

	Update();
}

SequencerPanel::~SequencerPanel()
{
	m_patternLengthSlider.removeListener(this);
	for (int step = 0; step < 16; ++step)
	{
		m_stepSliders[step].removeListener(this);
	}
	m_modeCombo.removeListener(this);
	m_divisionCombo.removeListener(this);
	m_patternSlider.removeListener(this);
	m_swingSlider.removeListener(this);
	m_bpmSlider.removeListener(this);
}

void SequencerPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void SequencerPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	ElementID seqCommonElemID{ ElementType::SeqGlobal,0 };
	if (comboBoxThatHasChanged == &m_modeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(seqCommonElemID, seqMode), m_modeCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_divisionCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(seqCommonElemID, seqDivision), m_divisionCombo.getSelectedId() - 1));
	}
}

void SequencerPanel::sliderValueChanged(Slider2* slider)
{
	ElementID seqCommonElemID{ ElementType::SeqGlobal,0 };

	for (int step = 0; step < 16; ++step)
	{
		if (slider == &m_stepSliders[step])
		{
			m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, seqStep1 + step), lrint(m_stepSliders[step].getValue())));
		}
	}

	if (slider == &m_patternLengthSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, seqPatternLength), lrint(m_patternLengthSlider.getValue())));
	}
	else if (slider == &m_patternSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(seqCommonElemID, seqPattern), lrint(m_patternSlider.getValue())));
	}
	else if (slider == &m_swingSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(seqCommonElemID, seqSwing), lrint(m_swingSlider.getValue())));
	}
	else if (slider == &m_bpmSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(ElementID(ElementType::Home, 0), gloBPM), lrint(m_bpmSlider.getValue())));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void SequencerPanel::buttonClicked(Button* button)
{
	if (button == &m_initButton)
	{
		CmdMacroUPtr pMacroCommand = std::make_unique<CmdMacro>();
		for (int step = seqStep1; step <= seqStep16; ++step)
		{
			pMacroCommand->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, step), 0));
		}
		pMacroCommand->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, seqPatternLength), 16));
		ElementID seqCommonElemID{ ElementType::SeqGlobal,0 };
		pMacroCommand->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(seqCommonElemID, seqDivision), 1));
		pMacroCommand->SetActionName("Init Seq steps");
		m_pCommandHistory->AddCommandAndExecute(std::move(pMacroCommand));
	}
}

void SequencerPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}

	m_patternLengthSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, seqPatternLength)), NotificationType::dontSendNotification);
	for (int step = 0; step < 16; ++step)
	{
		m_stepSliders[step].setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, step + seqStep1)), NotificationType::dontSendNotification);
	}

	ElementID seqCommonElemID{ ElementType::SeqGlobal,0 };

	m_modeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(seqCommonElemID,seqMode)) + 1, NotificationType::dontSendNotification);
	m_divisionCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(seqCommonElemID,seqDivision)) + 1, NotificationType::dontSendNotification);
	m_patternSlider.setValue(m_pPreset->GetParameterValue(ParamID(seqCommonElemID,seqPattern)), NotificationType::dontSendNotification);
	m_swingSlider.setValue(m_pPreset->GetParameterValue(ParamID(seqCommonElemID,seqSwing)), NotificationType::dontSendNotification);
	m_bpmSlider.setValue(m_pPreset->GetParameterValue(ParamID(ElementID(ElementType::Home, 0),gloBPM)), NotificationType::dontSendNotification);
}