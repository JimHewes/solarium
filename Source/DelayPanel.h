/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef DELAYPANEL_H_INCLUDED
#define DELAYPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
#include "Slider2.h"
class CommandHistory;

class DelayPanel :
	public Component,
	public Slider2::Listener,
	public Button::Listener,
	public ComboBox::Listener
{
public:
	DelayPanel(Preset* pPreset, CommandHistory* pCpmmandHistory, ElementID elemID,int slot);
	virtual ~DelayPanel();
	void sliderValueChanged(Slider2* slider) override;
	void buttonClicked(Button* button) override;
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_subtitle;

	Label			m_modeLabel;
	ComboBox		m_modeCombo;
	Label			m_timeLeftLabel;
	SliderTime		m_timeLeftSlider;	///< Delay time in seconds left side. Displayed when the MIDI Clk is OFF
	ComboBox		m_beatsLeftCombo;	///< Delay time in beats left side. Displayed when the MIDI Clk is ON
	Label			m_timeRightLabel;
	SliderTime		m_timeRightSlider;	///< Delay time in seconds right side. Displayed when the MIDI Clk is OFF
	ComboBox		m_beatsRightCombo;	///< Delay time in beats right side. Displayed when the MIDI Clk is ON
	Label			m_feedbackLeftLabel;
	SliderProp		m_feedbackLeftSlider;
	Label			m_feedbackRightLabel;
	SliderProp		m_feedbackRightSlider;
	Label			m_dampingLabel;
	SliderProp		m_dampingSlider;
	Label			m_dryLabel;
	SliderProp		m_drySlider;
	Label			m_wetLabel;
	SliderProp		m_wetSlider;
	ToggleButton	m_midiClockButton;	///< Turn sync to MIDI clock on or off.

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


#endif  // DELAYPANEL_H_INCLUDED
