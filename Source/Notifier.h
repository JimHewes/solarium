/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef NOTIFIER_H_INCLUDED
#define NOTIFIER_H_INCLUDED
#include <list>
#include <memory>
#include <functional>


/** The two lines below are needed because the long definition we'll give below is a template _specialzation_ of Notifier.
	So we need a main declaration for the specialization to be a specialization of. It doesn't have to be defined
	itself because we're never going to use it in a general way, only the specialization. We just need to first tell
	the compiler that Notifier is some kind of template.\n
	\n
	I got information about how to implement this template from the following link:\n
	\n
	http://stackoverflow.com/questions/28033251/can-you-extract-types-from-template-parameter-function-signature
*/
template<typename S>
class Notifier;

/** Notifier is an implementation of the Observer pattern which is similar to Signals/Slots.

	I wanted a way to implement the Observer pattern that required writing minimal code, mostly for the subject
	but also for the observer. It should be not to complicated. This implementation has the following advantages
	over the GoF Observer implementation:
	- does not require inheritance by either the subject or observer. Fewer classes.
	- observer and subject can safely be destructed in any order.
	- the subject can add event notification by adding as little as two lines. It doesn't have to keep track of the observers.
	- disconnection is automatic when the connection is destroyed (but the observer can manually disconnect at any time.)

	There are the following disadvantages:
	- The observer needs a connection for each instance of the subject. So if you are observing a one hundred 
	Widgets to see if they change, you need one hundred connections.
	- In the GoF observer impementation, the Listener class that the observer inherits from may contain more than one
	virtual function. So the observer can register for multiple events from the subject with one registration call.
	Notifier requires a separate connection for each event.

	Note that once a connection is disconnected, that same connection can no longer be re-connected, so it's no 
	longer useful. But a new connection can be created by making a new call to Connect(), which has the same effect.
*/
template<typename R, typename... Args>
class Notifier<R(Args...)>
{
public:
	Notifier() {}

	using CallbackType = std::function<R(Args...)>;
	using MyType = Notifier<R(Args...)>;

	/** ConnectionImpl is the package that gets stored in a list with the subject and contains the callback.

		The observer does not access this directly. It's internal to the Notifier. Instead, the observer
		accesses the Connection struct.
	*/
	struct ConnectionImpl
	{
		CallbackType cb;
		ConnectionImpl(MyType* pNotifier, CallbackType callback) : m_pNotifier(pNotifier), cb(callback) {};
		MyType* m_pNotifier;
	};

	using ConnectionImplWPtr = std::weak_ptr < ConnectionImpl >;
	using ConnectionImplSPtr = std::shared_ptr < ConnectionImpl >;

	/** Connection exists as just a wrapper for the weak pointer to the ConnectionImpl.

		The purpose of the Connection wrapper is just so that the observer doesn't have
		to lock the weak pointer to disconnect the connection. Instead they only have to
		call Disconnect() on the Connection, or let it go out of scope. It makes the observer
		code slightly cleaner.
		What is returned to the observer is a unique pointer to a Connection. When the pointer
		goes out of scope, the Connection is destructed and the connection is automatically
		disconnected (unless the subject is already gone.) Since a unique pointer cannot be copied,
		the connection cannot be unintentionally disconnected by Connection's destructor, say, 
		during a copy operation.
	*/
	struct Connection
	{
	private:
		ConnectionImplWPtr connection;
	public:
		Connection() {}
		~Connection() { Disconnect(); }
		Connection(ConnectionImplWPtr conn) : connection(conn) {};
		void Disconnect()
		{
			if (ConnectionImplSPtr c = connection.lock())
			{
				c->m_pNotifier->Disconnect(c.get());
			}
		}
	};

	/** Subjects can use this ConnectionType definition to provide a better-named type for observers to use.
		This is the type that will be returned from Connect().
	*/
	using ConnectionType = std::unique_ptr<Connection>;

	/** Observers call Connected to get a connection to the Subject.

		@param[in] callback		A function that will be called back when the event occurs. It must be some
								function or function object that can be wrapped by std::function<>.
		@returns	a std::unique_ptr to a Connection. The connection will remain active as long as the
					pointer is in scope or until the observer calls the Disconnect() function of the Connection.
	*/
	ConnectionType Connect(CallbackType callback)
	{
		m_notifyList.push_back(std::make_shared < ConnectionImpl>(this, callback));
		return std::make_unique<Connection>(m_notifyList.back());
	}

	void Disconnect(ConnectionImpl* pConnImpl)
	{
		m_notifyList.remove_if([pConnImpl](const ConnectionImplSPtr& conn)
		{
			return pConnImpl == conn.get();
		});
	}
	void Notify(Args... args)
	{
		for (auto& connection : m_notifyList)
		{
			connection->cb(std::forward<Args>(args)...);
		}
	}

	std::list< ConnectionImplSPtr>	m_notifyList;
};

#endif  // NOTIFIER_H_INCLUDED

