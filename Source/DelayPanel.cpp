/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include "DelayPanel.h"
#include "Command.h"

static const juce::Identifier paramIndexID = "paramIndex";

DelayPanel::DelayPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID,int slot)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	, m_pCommandHistory(pCommandHistory)
{

	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;


	m_subtitle.setFont(Font(17.0f));
	m_subtitle.setBounds(leftX,yPos,rightX + rightWidth,ctrlHeight);
	static char text[] = "Slot n: Delay";
	text[5] = char(0x31 + slot);
	m_subtitle.setText(text, NotificationType::dontSendNotification);
	addAndMakeVisible(m_subtitle);

	yPos += rowHeight;

	m_modeLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
	m_modeLabel.setText("Mode:", NotificationType::dontSendNotification);
	addAndMakeVisible(m_modeLabel);

	m_modeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> delModeList = m_pPreset->GetParameterListText(ParamID(elemID, delMode));;
	for (size_t i = 0; i < delModeList.size(); ++i)
	{
		m_modeCombo.addItem(delModeList[i], i + 1);
	}
	m_modeCombo.addListener(this);
	addAndMakeVisible(m_modeCombo);

	yPos += rowHeight;
	m_timeLeftLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_timeLeftLabel.setText("TimeL:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_timeLeftLabel);
	m_timeLeftSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_timeLeftSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delTimeSecondsL));
	m_timeLeftSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_timeLeftSlider.getProperties().set(paramIndexID, delTimeSecondsL);
	m_timeLeftSlider.addListener(this);
	addAndMakeVisible(m_timeLeftSlider);
	
	// The "beats" combo is positioned in the same place as the "seconds" slider and each is made visible depending on the MIDI Clk setting.
	m_beatsLeftCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> delBeatsLeftList = m_pPreset->GetParameterListText(ParamID(elemID, delTimeBeatsL));;
	for (size_t i = 0; i < delBeatsLeftList.size(); ++i)
	{
		m_beatsLeftCombo.addItem(delBeatsLeftList[i], i + 1);
	}
	m_beatsLeftCombo.addListener(this);
	addChildComponent(m_beatsLeftCombo);		// don't make visible until MIDI Clk is set to ON


	yPos += rowHeight;
	m_timeRightLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_timeRightLabel.setText("TimeR:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_timeRightLabel);
	m_timeRightSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_timeRightSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delTimeSecondsR));
	m_timeRightSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_timeRightSlider.getProperties().set(paramIndexID, delTimeSecondsR);
	m_timeRightSlider.addListener(this);
	addAndMakeVisible(m_timeRightSlider);

	// The "beats" combo is positioned in the same place as the "seconds" slider and each is made visible depending on the MIDI Clk setting.
	m_beatsRightCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> delBeatsRightList = m_pPreset->GetParameterListText(ParamID(elemID, delTimeBeatsR));;
	for (size_t i = 0; i < delBeatsRightList.size(); ++i)
	{
		m_beatsRightCombo.addItem(delBeatsRightList[i], i + 1);
	}
	m_beatsRightCombo.addListener(this);
	addChildComponent(m_beatsRightCombo);		// don't make visible until MIDI Clk is set to ON

	yPos += rowHeight;
	m_feedbackLeftLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_feedbackLeftLabel.setText("FeedL:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_feedbackLeftLabel);
	m_feedbackLeftSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_feedbackLeftSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delFeedL));
	m_feedbackLeftSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_feedbackLeftSlider.setTextValueSuffix(" %");
	m_feedbackLeftSlider.getProperties().set(paramIndexID, delFeedL);
	m_feedbackLeftSlider.addListener(this);
	addAndMakeVisible(m_feedbackLeftSlider);

	yPos += rowHeight;
	m_feedbackRightLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_feedbackRightLabel.setText("FeedR:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_feedbackRightLabel);
	m_feedbackRightSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_feedbackRightSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delFeedR));
	m_feedbackRightSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_feedbackRightSlider.setTextValueSuffix(" %");
	m_feedbackRightSlider.getProperties().set(paramIndexID, delFeedR);
	m_feedbackRightSlider.addListener(this);
	addAndMakeVisible(m_feedbackRightSlider);


	yPos += rowHeight;
	m_dampingLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_dampingLabel.setText("Damping:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_dampingLabel);
	m_dampingSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_dampingSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delDamping));
	m_dampingSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_dampingSlider.setTextValueSuffix(" %");
	m_dampingSlider.getProperties().set(paramIndexID, delDamping);
	m_dampingSlider.addListener(this);
	addAndMakeVisible(m_dampingSlider);

	yPos += rowHeight;
	m_dryLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_dryLabel.setText("Dry:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_dryLabel);
	m_drySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_drySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delDry));
	m_drySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_drySlider.setTextValueSuffix(" %");
	m_drySlider.getProperties().set(paramIndexID, delDry);
	m_drySlider.addListener(this);
	addAndMakeVisible(m_drySlider);

	yPos += rowHeight;
	m_wetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_wetLabel.setText("Wet:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_wetLabel);
	m_wetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_wetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,delWet));
	m_wetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_wetSlider.setTextValueSuffix(" %");
	m_wetSlider.getProperties().set(paramIndexID, delWet);
	m_wetSlider.addListener(this);
	addAndMakeVisible(m_wetSlider);

	yPos += rowHeight;
	m_midiClockButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_midiClockButton.setButtonText("MIDI clock");
	m_midiClockButton.addListener(this);
	addAndMakeVisible(m_midiClockButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

DelayPanel::~DelayPanel()
{
	m_modeCombo.removeListener(this);
	m_timeLeftSlider.removeListener(this);
	m_timeRightSlider.removeListener(this);
	m_beatsLeftCombo.removeListener(this);
	m_beatsLeftCombo.removeListener(this);
	m_feedbackLeftSlider.removeListener(this);
	m_feedbackRightSlider.removeListener(this);
	m_dampingSlider.removeListener(this);
	m_drySlider.removeListener(this);
	m_wetSlider.removeListener(this);
	m_midiClockButton.removeListener(this);
}

void DelayPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_timeLeftSlider) ||
		(slider == &m_timeRightSlider) ||
		(slider == &m_feedbackLeftSlider) ||
		(slider == &m_feedbackRightSlider) ||
		(slider == &m_dampingSlider) ||
		(slider == &m_drySlider) ||
		(slider == &m_wetSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void DelayPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}


// This function gets called after the button has been clicked and the state is changed.
void DelayPanel::buttonClicked(Button* button)
{
	if (button == &m_midiClockButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, delMidiClk), m_midiClockButton.getToggleState() ? 1 : 0));
	}
}

void DelayPanel::comboBoxChanged(ComboBox * comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_modeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, delMode), m_modeCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_beatsLeftCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, delTimeBeatsL), m_beatsLeftCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_beatsRightCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, delTimeBeatsR), m_beatsRightCombo.getSelectedId() - 1));
	}

}

void DelayPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_modeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,delMode)) + 1 ,NotificationType::dontSendNotification);
	m_timeLeftSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delTimeSecondsL)), NotificationType::dontSendNotification);
	m_timeRightSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delTimeSecondsR)), NotificationType::dontSendNotification);
	m_beatsLeftCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, delTimeBeatsL)) + 1, NotificationType::dontSendNotification);
	m_beatsRightCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, delTimeBeatsR)) + 1, NotificationType::dontSendNotification);
	m_feedbackLeftSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delFeedL)), NotificationType::dontSendNotification);
	m_feedbackRightSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delFeedR)), NotificationType::dontSendNotification);
	m_dampingSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delDamping)), NotificationType::dontSendNotification);
	m_drySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delDry)), NotificationType::dontSendNotification);
	m_wetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,delWet)), NotificationType::dontSendNotification);
	bool MIDIClkOn = m_pPreset->GetParameterValue(ParamID(m_elemID, delMidiClk)) != 0;
	m_midiClockButton.setToggleState(MIDIClkOn, NotificationType::dontSendNotification);

	m_timeLeftSlider.setVisible(!MIDIClkOn);
	m_timeRightSlider.setVisible(!MIDIClkOn);
	m_beatsLeftCombo.setVisible(MIDIClkOn);
	m_beatsRightCombo.setVisible(MIDIClkOn);

}
