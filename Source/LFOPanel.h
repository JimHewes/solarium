/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef LFOPANEL_H_INCLUDED
#define LFOPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
#include "Slider2.h"

class CommandHistory;

class LFODelaySlider : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
};


class LFOPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener,
	public Button::Listener
{
public:
	LFOPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~LFOPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_shapeLabel;
	ComboBox		m_shapeCombo;
	Label			m_rateLabel;
	SliderFreq500Hz	m_rateSlider;
	ComboBox		m_rateSyncCombo;
	Label			m_phaseLabel;
	SliderProp		m_phaseSlider;
	Label			m_delayStartLabel;
	LFODelaySlider	m_delayStartSlider;
	Label			m_fadeInLabel;
	LFODelaySlider	m_fadeInSlider;
	Label			m_fadeOutLabel;
	LFODelaySlider	m_fadeOutSlider;
	Label			m_levelLabel;
	SliderProp		m_levelSlider;
	ToggleButton	m_clockSyncButton;
	ToggleButton	m_offsetButton;
	ToggleButton	m_retriggerButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


class VibratoLFOPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener,
	public Button::Listener
{
public:
	VibratoLFOPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~VibratoLFOPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_shapeLabel;
	ComboBox		m_shapeCombo;
	Label			m_rateLabel;
	SliderFreq500Hz	m_rateSlider;
	ComboBox		m_rateSyncCombo;
	ToggleButton	m_modWheelButton;
	Label			m_modWheelMaxLabel;
	SliderProp		m_modWheelMaxSlider;
	Label			m_phaseLabel;
	SliderProp		m_phaseSlider;
	Label			m_delayStartLabel;
	LFODelaySlider	m_delayStartSlider;
	Label			m_fadeInLabel;
	LFODelaySlider	m_fadeInSlider;
	Label			m_fadeOutLabel;
	LFODelaySlider	m_fadeOutSlider;
	Label			m_levelLabel;
	SliderProp		m_levelSlider;
	ToggleButton	m_clockSyncButton;
	ToggleButton	m_offsetButton;
	ToggleButton	m_retriggerButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};



#endif  // LFOPANEL_H_INCLUDED
