/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <sstream>
#include "ChoFlaPhaPanel.h"
#include "Command.h"

static const juce::Identifier paramIndexID = "paramIndex";

ChoFlaPhaPanel::ChoFlaPhaPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID,int slot)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{

	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	
	m_subtitle.setFont(Font(17.0f));
	m_subtitle.setBounds(leftX,yPos,rightX + rightWidth,ctrlHeight);
	std::ostringstream os;
	os << "Slot " << slot + 1 << (m_elemID.elemType == ElementType::ChorusFlanger ? ": Chorus/Flanger" : ": Phaser");
	m_subtitle.setText(os.str(),NotificationType::dontSendNotification);
	addAndMakeVisible(m_subtitle);

	yPos += rowHeight;
	m_modeButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_modeButton.setButtonText("Mode");
	m_modeButton.addListener(this);
	addAndMakeVisible(m_modeButton);

	yPos += rowHeight;
	m_frequencyLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_frequencyLabel.setText("Freq:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_frequencyLabel);
	m_frequencySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_frequencySlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpFrequency));
	m_frequencySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_frequencySlider.setTextValueSuffix(" Hz");
	m_frequencySlider.getProperties().set(paramIndexID, cfpFrequency);
	m_frequencySlider.addListener(this);
	addAndMakeVisible(m_frequencySlider);

	yPos += rowHeight;
	m_depthLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_depthLabel.setText("Depth:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_depthLabel);
	m_depthSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_depthSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpDepth));
	m_depthSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_depthSlider.setTextValueSuffix(" %");
	m_depthSlider.getProperties().set(paramIndexID, cfpDepth);
	m_depthSlider.addListener(this);
	addAndMakeVisible(m_depthSlider);

	yPos += rowHeight;
	m_phaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseLabel.setText("Phase:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseLabel);
	m_phaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_phaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpPhase));
	m_phaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_phaseSlider.setTextValueSuffix(" deg");
	m_phaseSlider.getProperties().set(paramIndexID, cfpPhase);
	m_phaseSlider.addListener(this);
	addAndMakeVisible(m_phaseSlider);

	yPos += rowHeight;
	m_offsetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_offsetLabel.setText("Offset:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_offsetLabel);

	// The type of slider is different between the Chorus/Flanger and Phaser. Instead of creating 
	// two whole separtate classes just to handle this, just add exception cases for it.
	if (m_elemID.elemType == ElementType::ChorusFlanger)
	{
		m_pOffsetSlider = &m_choFlaOffsetSlider;
	}
	else if (m_elemID.elemType == ElementType::Phaser)
	{
		m_pOffsetSlider = &m_phaOffsetSlider;
	}
	m_pOffsetSlider->setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_pOffsetSlider->setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpOffset));
	m_pOffsetSlider->setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	if (m_elemID.elemType == ElementType::Phaser)
	{
		m_pOffsetSlider->setTextValueSuffix(" Hz");
	}
	m_pOffsetSlider->getProperties().set(paramIndexID, cfpOffset);
	m_pOffsetSlider->addListener(this);
	addAndMakeVisible(*m_pOffsetSlider);

	yPos += rowHeight;
	m_inLevelLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_inLevelLabel.setText("InLevel:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_inLevelLabel);
	m_inLevelSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_inLevelSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpInLevel));
	m_inLevelSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_inLevelSlider.setTextValueSuffix(" %");
	m_inLevelSlider.getProperties().set(paramIndexID, cfpInLevel);
	m_inLevelSlider.addListener(this);
	addAndMakeVisible(m_inLevelSlider);

	yPos += rowHeight;
	m_feedbackLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_feedbackLabel.setText("Feedback:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_feedbackLabel);
	m_feedbackSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_feedbackSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpFeedback));
	m_feedbackSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_feedbackSlider.setTextValueSuffix(" %");
	m_feedbackSlider.getProperties().set(paramIndexID, cfpFeedback);
	m_feedbackSlider.addListener(this);
	addAndMakeVisible(m_feedbackSlider);

	yPos += rowHeight;
	m_dryLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_dryLabel.setText("Dry:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_dryLabel);
	m_drySlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_drySlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpDry));
	m_drySlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_drySlider.setTextValueSuffix(" %");
	m_drySlider.getProperties().set(paramIndexID, cfpDry);
	m_drySlider.addListener(this);
	addAndMakeVisible(m_drySlider);

	yPos += rowHeight;
	m_wetLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_wetLabel.setText("Wet:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_wetLabel);
	m_wetSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_wetSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID,cfpWet));
	m_wetSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_wetSlider.setTextValueSuffix(" %");
	m_wetSlider.getProperties().set(paramIndexID, cfpWet);
	m_wetSlider.addListener(this);
	addAndMakeVisible(m_wetSlider);


	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

ChoFlaPhaPanel::~ChoFlaPhaPanel()
{
	m_modeButton.removeListener(this);
	m_frequencySlider.removeListener(this);
	m_depthSlider.removeListener(this);
	m_phaseSlider.removeListener(this);
	m_pOffsetSlider->removeListener(this);
	m_inLevelSlider.removeListener(this);
	m_feedbackSlider.removeListener(this);
	m_drySlider.removeListener(this);
	m_wetSlider.removeListener(this);
}

void ChoFlaPhaPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void ChoFlaPhaPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_frequencySlider) ||
		(slider == &m_depthSlider) ||
		(slider == &m_phaseSlider) ||
		(slider == m_pOffsetSlider) ||
		(slider == &m_inLevelSlider) ||
		(slider == &m_feedbackSlider) ||
		(slider == &m_drySlider) ||
		(slider == &m_wetSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void ChoFlaPhaPanel::buttonClicked(Button* button)
{
	if (button == &m_modeButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, cfpMode), m_modeButton.getToggleState() ? 1 : 0));
	}
}


void ChoFlaPhaPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_modeButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpMode)) != 0,NotificationType::dontSendNotification);
	m_frequencySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpFrequency)), NotificationType::dontSendNotification);
	m_depthSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpDepth)), NotificationType::dontSendNotification);
	m_phaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpPhase)), NotificationType::dontSendNotification);
	m_pOffsetSlider->setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpOffset)), NotificationType::dontSendNotification);
	m_inLevelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpInLevel)), NotificationType::dontSendNotification);
	m_feedbackSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpFeedback)), NotificationType::dontSendNotification);
	m_drySlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpDry)), NotificationType::dontSendNotification);
	m_wetSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,cfpWet)), NotificationType::dontSendNotification);

}
