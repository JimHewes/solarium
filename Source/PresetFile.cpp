/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <cmath>	// needed for Mac only
#include "Preset.h"
#include "Command.h"
#include "PresetFile.h"

using std::string;
using std::vector;

const int32_t PRST = 0x54535250;
const int32_t gInf = 0x666E4967;
const int32_t gPit = 0x74695067;
const int32_t gVib = 0x62695667;
const int32_t gOsc = 0x63734F67;
const int32_t gEnv = 0x766E4567;
const int32_t gRot = 0x746F5267;
const int32_t gMix = 0x78694D67;
const int32_t gLfo = 0x6F664C67;
const int32_t gFil = 0x6C694667;
const int32_t gVca = 0x61635667;
const int32_t gPan = 0x6E615067;
const int32_t gIfx = 0x78664967;
const int32_t gAmp = 0x706D4167;
const int32_t gVmx = 0x786D5667;
const int32_t gLeg = 0x67654C67;
const int32_t gArp = 0x70724167;
const int32_t gSeq = 0x71655367;
const int32_t gGfx = 0x78664767;

const int32_t inf1 = 0x31666E69;
const int32_t pit1 = 0x31746970;
const int32_t vib1 = 0x31626976;
const int32_t osc1 = 0x3163736F;
const int32_t osc2 = 0x3263736F;
const int32_t osc3 = 0x3363736F;
const int32_t osc4 = 0x3463736F;
const int32_t mod1 = 0x31646F6D;
const int32_t mod2 = 0x32646F6D;
const int32_t mod3 = 0x33646F6D;
const int32_t mod4 = 0x34646F6D;
const int32_t mod5 = 0x35646F6D;
const int32_t env1 = 0x31766E65;
const int32_t env2 = 0x32766E65;
const int32_t env3 = 0x33766E65;
const int32_t env4 = 0x34766E65;
const int32_t env5 = 0x35766E65;
const int32_t env6 = 0x36766E65;
const int32_t rot1 = 0x31746F72;
const int32_t rot2 = 0x32746F72;
const int32_t lfo1 = 0x316F666C;
const int32_t lfo2 = 0x326F666C;
const int32_t lfo3 = 0x336F666C;
const int32_t lfo4 = 0x346F666C;
const int32_t lfo5 = 0x356F666C;
const int32_t mix1 = 0x3178696D;
const int32_t mix2 = 0x3278696D;
const int32_t mix3 = 0x3378696D;
const int32_t mix4 = 0x3478696D;
const int32_t fil1 = 0x316C6966;
const int32_t fil2 = 0x326C6966;
const int32_t fil3 = 0x336C6966;
const int32_t fil4 = 0x346C6966;
const int32_t vca1 = 0x31616376;
const int32_t vca2 = 0x32616376;
const int32_t vca3 = 0x33616376;
const int32_t vca4 = 0x34616376;
const int32_t pan1 = 0x316E6170;
const int32_t pan2 = 0x326E6170;
const int32_t pan3 = 0x336E6170;
const int32_t pan4 = 0x346E6170;
const int32_t ifx1 = 0x31786669;
const int32_t ifx2 = 0x32786669;
const int32_t ifx3 = 0x33786669;
const int32_t ifx4 = 0x34786669;
const int32_t amp1 = 0x31706D61;
const int32_t amp2 = 0x32706D61;
const int32_t vmx1 = 0x31786D76;
const int32_t vmx2 = 0x32786D76;
const int32_t leg1 = 0x3167656C;
const int32_t arp1 = 0x31707261;
const int32_t seq1 = 0x31716573;
const int32_t gfx1 = 0x31786667;


void ReadGInf(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gInfChunkLength = *ptr++;
	int32_t* pEnd = ptr + gInfChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		*ptr++;	// inf1 chunk length
		switch (chunkID)
		{
		case inf1:	// "inf1"

			char* pChar = reinterpret_cast<char*>(ptr);
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName1 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName2 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName3 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName4 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName5 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName6 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName7 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName8 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName9 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName10 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName11 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName12 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName13 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName14 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName15 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName16 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName17 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName18 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName19 }, *pChar++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloName20 }, *pChar++ });

			ptr += 20 / sizeof(int32_t);	// advance ptr past preset name
			ptr += 4; // skip unknown values 
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloCategory1 }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloCategory2 }, *ptr++ });
			ptr += 16; // skip unknown values 
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob1Amt }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob2Amt }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob3Amt }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob4Amt }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob5Amt }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob1Src }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob2Src }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob3Src }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob4Src }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPerfKnob5Src }, *ptr++ });
			break;
		};
	}
}

void ReadGPit(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gPitChunkLength = *ptr++;
	int32_t* pEnd = ptr + gPitChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		*ptr++;		// pit1 chunk length
		switch (chunkID)
		{
		case pit1:	// "pit1"
			ptr += 4; // skip unknown values 
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPitchWheelUpRange }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloUnisonButton }, *ptr++ });
			ptr += 2; // skip unknown values 
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPitchWheelDownRange }, *ptr++ });
			break;
		};
	}
}

void ReadGVib(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gVibChunkLength = *ptr++;
	int32_t* pEnd = ptr + gVibChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		*ptr++;		// vib1 chunk length
		switch (chunkID)
		{
		case vib1:	// "vib1"
			paramValues.push_back({ { ElementType::VibratoLFO,0,PortType::None,0,vlfoModWheel }, *ptr++ });
			paramValues.push_back({ { ElementType::VibratoLFO,0,PortType::None,0,vlfoModWheelMax }, *ptr++ });
			break;
		};
	}
}


static const int ctrlSource = 0;
static const int ctrlStrength = 1;
static const int port1 = 0;
static const int port2 = 1;
static const int port3 = 2;
static const int port4 = 3;
static const int port5 = 4;
static const int inpSource = 0;
static const int inpLevel = 1;

#if 0
enum class ValueType : uint8_t { Enum, Num };

struct FileParam
{
	ParamID		paramID;
	ValueType	valueType;
};

vector<FileParam> modParams
{
	{ { ElementType::Off, 0, PortType::Control, port1, ctrlStrength }, ValueType::Num  },
	{ { ElementType::Off, 0, PortType::Control, port2, ctrlStrength }, ValueType::Num  },
	{ { ElementType::Off, 0, PortType::Control, port3, ctrlStrength }, ValueType::Num },
	{ { ElementType::Off, 0, PortType::Control, port4, ctrlStrength }, ValueType::Num  }
};
#endif


void ReadMod(int32_t* ptr, vector<ParamValue>& paramValues, ElementType elemType, int index)
{
	paramValues.push_back({ { elemType,index,PortType::Control, port1, ctrlStrength }, *ptr++ });
	int32_t modAmt1 = *ptr++;
	paramValues.push_back({ { elemType,index,PortType::Control, port2, ctrlStrength }, *ptr++ });
	int32_t modAmt2 = *ptr++;
	paramValues.push_back({ { elemType,index,PortType::Control, port3, ctrlStrength }, *ptr++ });
	int32_t modAmt3 = *ptr++;

	int32_t ctrlStr4 = *ptr++;
	int32_t modAmt4 = *ptr++;
	paramValues.push_back({ { elemType,index,PortType::Mod, port1, psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port2, psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port3, psiSource }, *ptr++ / 4 });
	int32_t modSrc4 = *ptr++ / 4;
	paramValues.push_back({ { elemType,index,PortType::Control, port1, ctrlSource }, *ptr++ / 4 });
	paramValues.push_back({ { elemType,index,PortType::Control, port2, ctrlSource }, *ptr++ / 4 });
	paramValues.push_back({ { elemType,index,PortType::Control, port3, ctrlSource }, *ptr++ / 4 });
	int32_t ctrlSrc4 = *ptr++ / 4;
	int32_t modDest1 = *ptr++ / 4;
	int32_t modDest2 = *ptr++ / 4;
	int32_t modDest3 = *ptr++ / 4;
	int32_t modDest4 = *ptr++ / 4;
	paramValues.push_back({ { elemType,index,PortType::Mod, port1, psiAmtPitch }, *ptr++ });
	paramValues.push_back({ { elemType,index,PortType::Mod, port2, psiAmtPitch }, *ptr++ });
	paramValues.push_back({ { elemType,index,PortType::Mod, port3, psiAmtPitch }, *ptr++ });
	int32_t modAmtPit4 = *ptr++ ;

	// When the Oscillator mod destination is Shape, we store the destination value as a range 
	// of -1000 to +1000 (from -100 to +100) to match that of the LinFM destination range.
	if (elemType == ElementType::Oscillator)
	{
		if (modDest1 == 3)	{ modAmt1 *= 10; }
		if (modDest2 == 3)	{ modAmt2 *= 10; }
		if (modDest3 == 3)	{ modAmt3 *= 10; }
		if (modDest4 == 3)	{ modAmt4 *= 10; }
	}

	paramValues.push_back({ { elemType,index,PortType::Mod, port1, psiAmount }, modAmt1 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port2, psiAmount }, modAmt2 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port3, psiAmount }, modAmt3 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port1, psiDest }, modDest1 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port2, psiDest }, modDest2 });
	paramValues.push_back({ { elemType,index,PortType::Mod, port3, psiDest }, modDest3 });

	if (elemType != ElementType::LFO && elemType != ElementType::VibratoLFO)
	{
		paramValues.push_back({ { elemType,index,PortType::Control, port4, ctrlStrength }, ctrlStr4 });
		paramValues.push_back({ { elemType,index,PortType::Mod, port4, psiAmount }, modAmt4 });
		paramValues.push_back({ { elemType,index,PortType::Mod, port4, psiSource }, modSrc4 });
		paramValues.push_back({ { elemType,index,PortType::Control, port4, ctrlSource }, ctrlSrc4 });
		paramValues.push_back({ { elemType,index,PortType::Mod, port4, psiDest }, modDest4 });
		paramValues.push_back({ { elemType,index,PortType::Mod, port4, psiAmtPitch }, modAmtPit4 });
	}
}

void ReadOsc(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscCoarseNoTrack }, *ptr++ });	// coarse tuning when No Track is enabled 
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscType }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscMM1Wave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscWTWave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscCEMWave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscWAVWave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscVSWave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscMiniWave }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscFine }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscShape }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscPhase }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscNoTrack }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscGlide }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscGlideRate }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::Input,0,0 }, *ptr++ / 4 });
	ptr += 2;	// skip unknown values
	paramValues.push_back({ { ElementType::Oscillator, index, PortType::None,0, oscCoarse }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscLow }, *ptr++ });
	paramValues.push_back({ { ElementType::Oscillator,index,PortType::None,0,oscClockSync }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Oscillator, index, PortType::None,0, oscCoarseSync }, *ptr++ });	// coarse tuning when Clock Sync is enabled 

}

void ReadGOsc(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case osc1:	ReadOsc(ptr, paramValues, 0);	break;		// "osc1"
		case osc2:	ReadOsc(ptr, paramValues, 1);	break;		// "osc2"
		case osc3:	ReadOsc(ptr, paramValues, 2);	break;		// "osc3"
		case osc4:	ReadOsc(ptr, paramValues, 3);	break;		// "osc4"
		case mod1:	ReadMod(ptr, paramValues, ElementType::Oscillator, 0);	break;		// "mod1"
		case mod2:	ReadMod(ptr, paramValues, ElementType::Oscillator, 1);	break;		// "mod2"
		case mod3:	ReadMod(ptr, paramValues, ElementType::Oscillator, 2);	break;		// "mod3"
		case mod4:	ReadMod(ptr, paramValues, ElementType::Oscillator, 3);	break;		// "mod4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadEnv(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egDelay }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egAttack }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egDecay }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egSustain }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egRelease }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egASlope }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egDSlope }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egSSlope }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egRSlope }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::None,0,egVelocity }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,0,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,1,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,2,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,3,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,0,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,1,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,2,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::EG, index, PortType::Mod,3,psiSource }, *ptr++ / 4 });

}

void ReadGEnv(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case env1:	ReadEnv(ptr, paramValues, 0);	break;		// "env1"
		case env2:	ReadEnv(ptr, paramValues, 1);	break;		// "env2"
		case env3:	ReadEnv(ptr, paramValues, 2);	break;		// "env3"
		case env4:	ReadEnv(ptr, paramValues, 3);	break;		// "env4"
		case env5:	ReadEnv(ptr, paramValues, 4);	break;		// "env5"
		case env6:	ReadEnv(ptr, paramValues, 5);	break;		// "env6"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadRot(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,0,1 }, *ptr++ });	// input level
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,1,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,2,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,3,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotCoarseNoTrack }, *ptr++ });	// coarse tuning when NoTrack is enabled 
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotFine }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotXFade }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotPhase }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotNoTrack }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,0,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,1,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,2,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::Input,3,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotPhaseSync }, *ptr++ / 4 });
	ptr += 2;	// skip unknown values
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotCoarse }, *ptr++ });	// coarse tuning when no buttons are enabled
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotLow }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotClockSync }, *ptr++ });
	paramValues.push_back({ { ElementType::Rotor, index, PortType::None,0, rotCoarseSync }, *ptr++ });	// coarse tuning when clock sync is enabled 
}

void ReadGRot(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case rot1:	ReadRot(ptr, paramValues, 0);	break;		// "rot1"
		case rot2:	ReadRot(ptr, paramValues, 1);	break;		// "rot2"
		case mod1:	ReadMod(ptr, paramValues, ElementType::Rotor, 0);	break;		// "mod1"
		case mod2:	ReadMod(ptr, paramValues, ElementType::Rotor, 1);	break;		// "mod2"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadMix(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index >= 0 && index < 4);
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,0,1 }, *ptr++ });	// input level
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,1,1 }, *ptr++ });	// input level
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,2,1 }, *ptr++ });	// input level
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,3,1 }, *ptr++ });	// input level
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,0,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,1,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,2,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,3,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,0,0 }, *ptr++ / 4 });	// input source
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,1,0 }, *ptr++ / 4 });	// input source
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,2,0 }, *ptr++ / 4 });	// input source
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Input,3,0 }, *ptr++ / 4 });	// input source
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,0,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,1,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,2,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,3,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::None,0,mixOutputLevel }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,4,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Mixer, index, PortType::Mod,4,psiSource }, *ptr++ / 4 });

}

void ReadLfo(int32_t* ptr, vector<ParamValue>& paramValues, ElementType elemType, int index)
{
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoRate }, *ptr++ });	// coarse tuning when No Track is enabled 
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoShape }, *ptr++ / 4 });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoPhase }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoRetrigger }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoFadeIn }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoFadeOut }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoDelayStart }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoOffset }, *ptr++ });
	ptr += 1;	// skip unknown value
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoLevel }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoClockSync }, *ptr++ });
	paramValues.push_back({ { elemType, index, PortType::None,0,lfoRateSync }, *ptr++ });
}

void ReadGLfo(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case lfo1:	ReadLfo(ptr, paramValues, ElementType::LFO, 0);	break;			// "lfo1"
		case lfo2:	ReadLfo(ptr, paramValues, ElementType::LFO, 1);	break;			// "lfo2"
		case lfo3:	ReadLfo(ptr, paramValues, ElementType::LFO, 2);	break;			// "lfo3"
		case lfo4:	ReadLfo(ptr, paramValues, ElementType::LFO, 3);	break;			// "lfo4"
		case lfo5:	ReadLfo(ptr, paramValues, ElementType::VibratoLFO, 0);	break;	// "lfo5"
		case mod1:	ReadMod(ptr, paramValues, ElementType::LFO, 0);	break;			// "mod1"
		case mod2:	ReadMod(ptr, paramValues, ElementType::LFO, 1);	break;			// "mod2"
		case mod3:	ReadMod(ptr, paramValues, ElementType::LFO, 2);	break;			// "mod3"
		case mod4:	ReadMod(ptr, paramValues, ElementType::LFO, 3);	break;			// "mod4"
		case mod5:	ReadMod(ptr, paramValues, ElementType::VibratoLFO, 0);	break;	// "mod5"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadGMix(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case mix1:	ReadMix(ptr, paramValues, 0);	break;		// "mix1"
		case mix2:	ReadMix(ptr, paramValues, 1);	break;		// "mix2"
		case mix3:	ReadMix(ptr, paramValues, 2);	break;		// "mix3"
		case mix4:	ReadMix(ptr, paramValues, 3);	break;		// "mix4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadFil(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterType }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterMM1Mode }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterVowel1 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterVowel2 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterVowel3 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterVowel4 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterVowel5 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterResonance }, *ptr++ });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterDamping }, *ptr++ });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterCutoff }, *ptr++ });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterKeycenter }, *ptr++ });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterKeytracking }, *ptr++ });
	paramValues.push_back({ { ElementType::Filter, index, PortType::Input,0,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterXFade }, *ptr++ });
	ptr += 2;	// unknown values
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterObieMode }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Filter, index, PortType::None,0,filterCombMode }, *ptr++ / 4 });

}

void ReadGFil(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case fil1:	ReadFil(ptr, paramValues, 0);	break;		// "fil1"
		case fil2:	ReadFil(ptr, paramValues, 1);	break;		// "fil2"
		case fil3:	ReadFil(ptr, paramValues, 2);	break;		// "fil3"
		case fil4:	ReadFil(ptr, paramValues, 3);	break;		// "fil4"
		case mod1:	ReadMod(ptr, paramValues, ElementType::Filter, 0);	break;		// "mod1"
		case mod2:	ReadMod(ptr, paramValues, ElementType::Filter, 1);	break;		// "mod2"
		case mod3:	ReadMod(ptr, paramValues, ElementType::Filter, 2);	break;		// "mod3"
		case mod4:	ReadMod(ptr, paramValues, ElementType::Filter, 3);	break;		// "mod4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadVca(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index >= 0 && index < 4);
	paramValues.push_back({ { ElementType::VCA, index, PortType::None,0,vcaType }, *ptr++ });
	paramValues.push_back({ { ElementType::VCA, index, PortType::None,0,vcaBoost }, *ptr++ });

	paramValues.push_back({ { ElementType::VCA, index, PortType::Mod, 0, psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::VCA, index, PortType::Mod, 0, psiSource }, *ptr++ /4 });

	paramValues.push_back({ { ElementType::VCA, index, PortType::Input,0,0 }, *ptr++ / 4});
	paramValues.push_back({ { ElementType::VCA, index, PortType::None,0,vcaLevel }, *ptr++ });
}

void ReadGVca(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case vca1:	ReadVca(ptr, paramValues, 0);	break;		// "vca1"
		case vca2:	ReadVca(ptr, paramValues, 1);	break;		// "vca2"
		case vca3:	ReadVca(ptr, paramValues, 2);	break;		// "vca3"
		case vca4:	ReadVca(ptr, paramValues, 3);	break;		// "vca4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadPan(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index >= 0 && index < 4);
	paramValues.push_back({ { ElementType::VCA, index, PortType::None,0,vcaInitPan }, *ptr++ });
	paramValues.push_back({ { ElementType::VCA, index, PortType::Mod, 1, psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::VCA, index, PortType::Mod, 1, psiSource }, *ptr++ /4 });

	// VCAMaster inputs are really Enable Part parameters.
	paramValues.push_back({ { ElementType::VCAMaster, 0, PortType::Input, index, psiSource}, (*ptr++ == 0) ? 1 : 0 }); // For enable part, 0 = On, 1 = Off
}

void ReadGPan(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case pan1:	ReadPan(ptr, paramValues, 0);	break;		// "pan1"
		case pan2:	ReadPan(ptr, paramValues, 1);	break;		// "pan2"
		case pan3:	ReadPan(ptr, paramValues, 2);	break;		// "pan3"
		case pan4:	ReadPan(ptr, paramValues, 3);	break;		// "pan4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadIfx(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index >= 0 && index < 4);
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::None,0,insFxMode }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::None,0,insFxValue }, *ptr++ });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::Control,0,ctrlStrength }, *ptr++ });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::Mod,0, psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::Input,0,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::Mod,0,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::InsertFX, index, PortType::Control,0,ctrlSource }, *ptr++ / 4 });
}

void ReadGIfx(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case ifx1:	ReadIfx(ptr, paramValues, 0);	break;		// "ifx1"
		case ifx2:	ReadIfx(ptr, paramValues, 1);	break;		// "ifx2"
		case ifx3:	ReadIfx(ptr, paramValues, 2);	break;		// "ifx3"
		case ifx4:	ReadIfx(ptr, paramValues, 3);	break;		// "ifx4"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadAmp(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index == 0 || index == 1);
	paramValues.push_back({ { ElementType::AM,index,PortType::None,0,amAlgorithm }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::AM,index,PortType::None,0,amOffset }, *ptr++ });
	paramValues.push_back({ { ElementType::AM,index,PortType::Control,0,ctrlStrength }, *ptr++ });
	paramValues.push_back({ { ElementType::AM,index,PortType::Mod,0,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::AM,index,PortType::Input,0,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::AM,index,PortType::Mod,0,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::AM,index,PortType::Control,0, ctrlSource }, *ptr++ / 4 });
}

void ReadGAmp(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case amp1:	ReadAmp(ptr, paramValues, 0);	break;		// "amp1"
		case amp2:	ReadAmp(ptr, paramValues, 1);	break;		// "amp2"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadVmx(int32_t* ptr, vector<ParamValue>& paramValues, int index)
{
	assert(index == 0 || index == 1);
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,0,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,1,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,2,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,3,1 }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::None,0,vecXOffset }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::None,0,vecYOffset }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Mod,0,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Mod,1,psiAmount }, *ptr++ });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,0,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,1,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,2,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Input,3,0 }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Mod,0,psiSource }, *ptr++ / 4 });
	paramValues.push_back({ { ElementType::Vector, index, PortType::Mod,1,psiSource }, *ptr++ / 4 });
}

void ReadGVmx(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case vmx1:	ReadVmx(ptr, paramValues, 0);	break;		// "vmx1"
		case vmx2:	ReadVmx(ptr, paramValues, 1);	break;		// "vmx2"
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadGLeg(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case leg1:	// "leg1"
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime1 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel1 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel1 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime2 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel2 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel2 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime3 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel3 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel3 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime4 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel4 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel4 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime5 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel5 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel5 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime6 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel6 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel6 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime7 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel7 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel7 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legTime8 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legXLevel8 }, *ptr++ });
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legYLevel8 }, *ptr++ });

			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legSlope }, *ptr++ });			// slope
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legLoop }, *ptr++ });			// loop
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legRepeat }, *ptr++ / 4 });	// repeat
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legStart }, *ptr++ / 4 });		// start
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legKeyOff }, *ptr++ / 4 });	// KeyOff

			paramValues.push_back({ { ElementType::LoopEG,0,PortType::Mod,1,psiAmount }, *ptr++ });			// mod 1 amount (time)
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::Mod,0,psiAmount }, *ptr++ });			// mod 0 amount (level)
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::Mod,1,psiSource }, *ptr++ / 4 });		// mod 1 source (time)
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::Mod,0,psiSource }, *ptr++ / 4 });		// mod 0 source (level)
			ptr += 8;	// skip unknown values
			paramValues.push_back({ { ElementType::LoopEG,0,PortType::None,0,legSync }, *ptr++ / 4});

			break;
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadGArp(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case arp1:	// "arp1"
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpOctave }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpSwing }, *ptr++ });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpResolution }, *ptr++ });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpNoteLength}, *ptr++});
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpHold }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpPatternLength }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpPattern }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloArpButton }, *ptr++ / 4 });
			int fileVersion = *ptr++;
			if (fileVersion != 1)
			{
				assert(false);
			}

			// The Preset class keeps separate paramaters for volume, gate length and gate enable.
			// So we need to separate them first.
			for (int i = 0; i < 32; ++i)
			{
				int32_t val = *ptr++;
				int32_t stepVolume = val & 0x7F;
				int32_t gateLength = (val >> 8) & 0x0FFF;
				int32_t gateEnable = (val >> 20) & 1;
				paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0, i + arpStepVolumeOffset }, stepVolume });
				paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0, i + arpStepGateLengthOffset }, gateLength });
				paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0, i + arpStepGateOffset }, gateEnable });
			}

			paramValues.push_back({ { ElementType::Arpeggiator,0,PortType::None,0,arpVelocity }, *ptr++ / 4 });

		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}


void ReadGSeq(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gOscChunkLength = *ptr++;
	int32_t* pEnd = ptr + gOscChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		int32_t chunkLength = *ptr++;
		switch (chunkID)
		{
		case seq1:	// "seq1"

			paramValues.push_back({ { ElementType::SeqGlobal,0,PortType::None,0,seqMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::SeqGlobal,0,PortType::None,0,seqSwing }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::SeqGlobal,0,PortType::None,0,seqDivision }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::SeqGlobal,0,PortType::None,0,seqPattern }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloSeqButton }, *ptr++ });
			// seq steps
			for (int track = 0; track < 4; track++)
			{
				for (int step = 1; step < 17; step++)
				{
					paramValues.push_back({ { ElementType::Seq,track,PortType::None,0,step }, *ptr++ });
				}
			}
			for (int track = 0; track < 4; track++)
			{
				paramValues.push_back({ { ElementType::Seq,track,PortType::None,0,seqPatternLength }, *ptr++ });
			}
			break;
		};
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk
	}
}

void ReadGGfx(int32_t* ptr, vector<ParamValue>& paramValues)
{
	int32_t gGfxChunkLength = *ptr++;
	int32_t* pEnd = ptr + gGfxChunkLength / sizeof(int32_t);
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		*ptr++;		// gfx1 chunk length
		switch (chunkID)
		{
		case gfx1:	// "gfx1"
			paramValues.push_back({ { ElementType::Output,0,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpFrequency }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpDepth }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpOffset }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpInLevel }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpFeedback }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpDry }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpWet }, *ptr++ });
			paramValues.push_back({ { ElementType::ChorusFlanger,0,PortType::None,0,cfpPhase }, *ptr++ });
			//paramValues.push_back({ { ElementType::FXChannel,0,PortType::Input,0,0 }, *ptr++ });
			ptr += 3; // skip unknown values 
			
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpFrequency }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpDepth }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpOffset }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpInLevel }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpFeedback }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpDry }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpWet }, *ptr++ });
			paramValues.push_back({ { ElementType::Phaser,0,PortType::None,0,cfpPhase }, *ptr++ });
			ptr += 3; // skip unknown values 

			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delTimeSecondsL }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delTimeSecondsR }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delFeedR }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delFeedL }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delDamping }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delDry }, *ptr++ });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delWet }, *ptr++ });
			ptr += 1; // skip unknown value 
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delMidiClk }, *ptr++ });
			ptr += 1; // skip unknown value 

			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqFreq1 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqQ1 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqGain1 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqFreq2 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqQ2 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqGain2 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqFreq3 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqQ3 }, *ptr++ });
			paramValues.push_back({ { ElementType::EQ,0,PortType::None,0,eqGain3 }, *ptr++ });
			ptr += 3; // skip unknown values 

			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delTimeBeatsL }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Delay,0,PortType::None,0,delTimeBeatsR }, *ptr++ / 4 });

			ptr += 1; // skip Transpose. This is now in the System element.
			//paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloTranspose }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloBPM }, *ptr++ });
			ptr += 1; // skip unknown value 
			ptr += 2; // skip VT intensity and Offset. These are now in the System element.
			//paramValues.push_back({ { ElementType::System,0,PortType::None,0,sysVTIntens }, *ptr++ });
			//paramValues.push_back({ { ElementType::System,0,PortType::None,0,sysVTOffset }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloNotePriority }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloPlayMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloUniVoice }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloUniTune }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloGldType }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloGldTime }, *ptr++ });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloGldRate }, *ptr++ });
			ptr += 1; // skip unknown value 
			ptr += 2; // skip AT intensity and Offset. These are now in the System element.
			//paramValues.push_back({ { ElementType::System,0,PortType::None,0,sysATIntens }, *ptr++ });
			//paramValues.push_back({ { ElementType::System,0,PortType::None,0,sysATOffset }, *ptr++ });
			ptr += 1; // skip unknown value 
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloEgReset }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloLegato }, *ptr++ / 4 });
			ptr += 2; // skip unknown values 

			paramValues.push_back({ { ElementType::EnvFol,0,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::EnvFol,0,PortType::None,0,envFolInput }, *ptr++ });
			paramValues.push_back({ { ElementType::EnvFol,0,PortType::None,0,envFolAttack }, *ptr++ });
			paramValues.push_back({ { ElementType::EnvFol,0,PortType::None,0,envFolRelease }, *ptr++ });
			paramValues.push_back({ { ElementType::EnvFol,0,PortType::None,0,envFolOutput }, *ptr++ });

			paramValues.push_back({ { ElementType::Output,1,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Output,2,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Output,3,PortType::Input,0,0 }, *ptr++ / 4 });
			ptr += 1; // skip unknown value 

			paramValues.push_back({ { ElementType::FXChannel,0,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::FXChannel,1,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::FXChannel,2,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::FXChannel,3,PortType::Input,0,0 }, *ptr++ / 4 });

			paramValues.push_back({ { ElementType::FXChannel,0,PortType::None,0,fxChannelSlot1 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,0,PortType::None,0,fxChannelSlot2 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,0,PortType::None,0,fxChannelSlot3 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,0,PortType::None,0,fxChannelSlot4 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,1,PortType::None,0,fxChannelSlot1 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,1,PortType::None,0,fxChannelSlot2 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,1,PortType::None,0,fxChannelSlot3 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,1,PortType::None,0,fxChannelSlot4 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,2,PortType::None,0,fxChannelSlot1 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,2,PortType::None,0,fxChannelSlot2 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,2,PortType::None,0,fxChannelSlot3 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,2,PortType::None,0,fxChannelSlot4 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,3,PortType::None,0,fxChannelSlot1 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,3,PortType::None,0,fxChannelSlot2 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,3,PortType::None,0,fxChannelSlot3 }, *ptr++ });
			paramValues.push_back({ { ElementType::FXChannel,3,PortType::None,0,fxChannelSlot4 }, *ptr++ });
			
			paramValues.push_back({ { ElementType::Ribbon,0,PortType::None,0,ribOffset }, *ptr++ });
			paramValues.push_back({ { ElementType::Ribbon,0,PortType::None,0,ribIntensity }, *ptr++ });
			paramValues.push_back({ { ElementType::Ribbon,0,PortType::None,0,ribHold }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Ribbon,0,PortType::None,0,ribTouchOffset}, *ptr++ / 4});

			//ptr += 4; // skip unknown values 

			ptr += 2;		// skip over "KEYTAB" plus two bytes
			for (int table = 0; table < 4; table++)
			{
				for (int index = 0; index < 128; index++)
				{
					float value = *reinterpret_cast<float*>(ptr);
					if (value != -1.0)	// -1 means the value is interpolated.
					{
						// The float values are in 0.1 of a percent. Multiply here so that we can use integers.
						value *= 1000.0;
					}
					int valueInt = lrint(value);	// round off to avoid any float inaccuracies
					paramValues.push_back({ { ElementType::KeyTable,table,PortType::None,0, index }, valueInt });
					ptr += 2;
				}
			}

			// Where is Glide Rate?
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloGldMode }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloGldRange }, *ptr++ });

			paramValues.push_back({ { ElementType::Lag,0,PortType::None,0,lagTime }, *ptr++ });
			paramValues.push_back({ { ElementType::Lag,1,PortType::None,0,lagTime }, *ptr++ });
			paramValues.push_back({ { ElementType::Lag,2,PortType::None,0,lagTime }, *ptr++ });
			paramValues.push_back({ { ElementType::Lag,3,PortType::None,0,lagTime }, *ptr++ });
			paramValues.push_back({ { ElementType::Lag,0,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Lag,1,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Lag,2,PortType::Input,0,0 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Lag,3,PortType::Input,0,0 }, *ptr++ / 4 });

			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloExpPedal }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloSusPedal1 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloSusPedal2 }, *ptr++ / 4 });
			ptr += 3; // skip unknown values (No these are not pedal polarities. Those are not saved.)
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloAssignFunc1 }, *ptr++ / 4 });	// Assign1 Function
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloAssignFunc2 }, *ptr++ / 4 });	// Assign2 Function
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloAssignMode1 }, *ptr++ / 4 });
			paramValues.push_back({ { ElementType::Home,0,PortType::None,0,gloAssignMode2 }, *ptr++ / 4 });

			break;
		};
	}
}

void LoadPresetFile(const string& filePath, CommandHistory* pCommandHistory)
{
	std::ifstream inFile;
	inFile.open(filePath, std::ios::in | std::ios::binary);

	if (!inFile)
	{
		string msg = "Cannot open the file: ";
		msg += filePath;
		throw std::runtime_error(msg);
	}

	// Get file length
	inFile.seekg(0, std::ios::end);
	int fileLength = static_cast<int>(inFile.tellg());
	inFile.seekg(0);

	// Sanity check. Make sure it's a reasonable size
	if (fileLength > 20000)
	{
		throw std::runtime_error("File is invalid. Wrong size.");
	}

	vector<uint8_t>	buf(fileLength);

	// Since preset files are relatively small, reading the whole thing into a buffer first is probably more efficient.
	inFile.read(reinterpret_cast<char*>(&buf[0]), fileLength);
	if (!inFile)
	{
		throw std::runtime_error("Reading the file failed.");
	}
	inFile.close();

	// Validate
	int32_t* ptr = reinterpret_cast<int32_t*>(&buf[0]);
	if (PRST != *ptr++ ||		// "PRST"
		0x000025E8 != *ptr++ ||		// file size 
		0x00000001 != *ptr++)		// version number ?
	{
		throw std::runtime_error("File header is wrong. This doesn't look like a valid preset file.");
	}
	
	vector<ParamValue> paramValues;
	int32_t* pEnd = reinterpret_cast<int32_t*>(&buf[0] + buf.size());
	while (ptr < pEnd)
	{
		int32_t chunkID = *ptr++;
		switch (chunkID)
		{
		case gInf:	ReadGInf(ptr, paramValues);	break;
		case gPit:	ReadGPit(ptr, paramValues);	break;
		case gVib:	ReadGVib(ptr, paramValues);	break;
		case gOsc:	ReadGOsc(ptr, paramValues);	break;
		case gEnv:	ReadGEnv(ptr, paramValues);	break;
		case gRot:	ReadGRot(ptr, paramValues);	break;
		case gMix:	ReadGMix(ptr, paramValues);	break;
		case gLfo:	ReadGLfo(ptr, paramValues);	break;
		case gFil:	ReadGFil(ptr, paramValues);	break;
		case gVca:	ReadGVca(ptr, paramValues);	break;
		case gPan:	ReadGPan(ptr, paramValues);	break;
		case gIfx:	ReadGIfx(ptr, paramValues);	break;
		case gAmp:	ReadGAmp(ptr, paramValues);	break;
		case gVmx:	ReadGVmx(ptr, paramValues);	break;
		case gLeg:	ReadGLeg(ptr, paramValues);	break;
		case gArp:	ReadGArp(ptr, paramValues);	break;
		case gSeq:	ReadGSeq(ptr, paramValues);	break;
		case gGfx:	ReadGGfx(ptr, paramValues);	break;
		default:
			//assert(false);	// unknown chunk
			break;
		};
		int32_t chunkLength = *ptr++;
		assert(chunkLength >= 2 * sizeof(int32_t));
		ptr += chunkLength / sizeof(int32_t);	// next chunk

	};

	auto cmd = std::make_unique<CmdSetPreset>(std::move(paramValues));
	cmd->SetActionText("Load Preset");
	pCommandHistory->AddCommandAndExecute(std::move(cmd));

}
//============================================================================================
//
//
//
//
//
//
//============================================================================================


void WriteGInf(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gInf";
	int32_t size = 0x009C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "inf1";
	size = 0x0094;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	char* ptrName = reinterpret_cast<char*>(&buf[0]);

	string name = pPreset->GetName();
	for (size_t i = 0; i < 20; ++i)
	{
		*ptrName++ = (i < name.length()) ? name[i] : ' ';
	}
	int32_t* ptr = &buf[5];	// start after name
	ptr += 4;	// skip unknown
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloCategory1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloCategory2 });
	ptr += 16;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob1Amt });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob2Amt });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob3Amt });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob4Amt });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob5Amt });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob1Src });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob2Src });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob3Src });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob4Src });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPerfKnob5Src });

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGPit(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gPit";
	int32_t size = 0x002C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "pit1";
	size = 0x0024;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	ptr += 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPitchWheelUpRange });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloUnisonButton });
	ptr += 2;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPitchWheelDownRange });

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGVib(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gVib";
	int32_t size = 0x0010;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "vib1";
	size = 0x0008;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	int32_t val = pPreset->GetParameterValue({ ElementType::VibratoLFO,0,PortType::None,0,vlfoModWheel });
	outStr.write(reinterpret_cast<char*>(&val), sizeof(val));
	val = pPreset->GetParameterValue({ ElementType::VibratoLFO,0,PortType::None,0,vlfoModWheelMax });
	outStr.write(reinterpret_cast<char*>(&val), sizeof(val));
}


void WriteMod(Preset* pPreset, std::ostream& outStr, ElementType elemType, int index)
{
	bool isLFO = elemType == ElementType::LFO || elemType == ElementType::VibratoLFO;

	int32_t size = (isLFO ? 0x005C : 0x0060) ;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	int32_t modDest1 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port1, psiDest });
	int32_t modDest2 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port2, psiDest });
	int32_t modDest3 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port3, psiDest });
	int32_t modDest4 = isLFO ? 0 : pPreset->GetParameterValue({ elemType, index, PortType::Mod, port4, psiDest });

	int32_t modAmt1 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port1, psiAmount });
	int32_t modAmt2 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port2, psiAmount });
	int32_t modAmt3 = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port3, psiAmount });
	int32_t modAmt4 = isLFO ? 0 : pPreset->GetParameterValue({ elemType, index, PortType::Mod, port4, psiAmount });

	// When the Oscillator mod destination is Shape, we save the destination value as a range 
	// of -100 to +100 (from -1000 to +1000).
	if (elemType == ElementType::Oscillator)
	{
		if (modDest1 == 3) { modAmt1 /= 10; }
		if (modDest2 == 3) { modAmt2 /= 10; }
		if (modDest3 == 3) { modAmt3 /= 10; }
		if (modDest4 == 3) { modAmt4 /= 10; }
	}

	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port1, ctrlStrength });
	*ptr++ = modAmt1;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port2, ctrlStrength });
	*ptr++ = modAmt2;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port3, ctrlStrength });
	*ptr++ = modAmt3;
	*ptr++ = isLFO ? 0 : pPreset->GetParameterValue({ elemType, index, PortType::Control, port4, ctrlStrength });
	*ptr++ = modAmt4;

	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port1, psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port2, psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port3, psiSource }) * 4;
	*ptr++ = isLFO ? 0 : pPreset->GetParameterValue({ elemType, index, PortType::Mod, port4, psiSource }) * 4;

	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port1, ctrlSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port2, ctrlSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Control, port3, ctrlSource }) * 4;
	*ptr++ = isLFO ? 0 : pPreset->GetParameterValue({ elemType, index, PortType::Control, port4, ctrlSource }) * 4;

	*ptr++ = modDest1 * 4;
	*ptr++ = modDest2 * 4;
	*ptr++ = modDest3 * 4;
	*ptr++ = isLFO ? 0 : modDest4 * 4;

	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port1, psiAmtPitch });
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port2, psiAmtPitch });
	*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port3, psiAmtPitch });
	if (!isLFO)
	{
		*ptr++ = pPreset->GetParameterValue({ elemType, index, PortType::Mod, port4, psiAmtPitch });
	}
	
	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteOsc(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0054;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];
	
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscCoarseNoTrack });	// coarse tuning when No Track is enabled 
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscType }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscMM1Wave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscWTWave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscCEMWave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscWAVWave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscVSWave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscMiniWave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscFine });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscShape });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscPhase });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscNoTrack }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscGlide }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscGlideRate }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::Input,0,0 }) * 4;
	ptr += 2; 
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscCoarse });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscLow });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscClockSync }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Oscillator,index,PortType::None,0,oscCoarseSync });	// coarse tuning when Clock Sync is enabled 
	
	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGOsc(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gOsc";
	int32_t size = 0x0310;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "osc1";
	WriteOsc(pPreset, outStr, 0);
	outStr << "osc2";
	WriteOsc(pPreset, outStr, 1);
	outStr << "osc3";
	WriteOsc(pPreset, outStr, 2);
	outStr << "osc4";
	WriteOsc(pPreset, outStr, 3);
	outStr << "mod1";
	WriteMod(pPreset, outStr, ElementType::Oscillator, 0);
	outStr << "mod2";
	WriteMod(pPreset, outStr, ElementType::Oscillator, 1);
	outStr << "mod3";
	WriteMod(pPreset, outStr, ElementType::Oscillator, 2);
	outStr << "mod4";
	WriteMod(pPreset, outStr, ElementType::Oscillator, 3);
}



void WriteEnv(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0048;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egDelay });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egAttack });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egDecay });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egSustain });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egRelease });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egASlope });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egDSlope });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egSSlope });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egRSlope });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::None,0,egVelocity });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,0,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,2,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,3,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,0,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,1,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,2,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::EG, index, PortType::Mod,3,psiSource }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGEnv(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gEnv";
	int32_t size = 0x01E0;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "env1";
	WriteEnv(pPreset, outStr, 0);
	outStr << "env2";
	WriteEnv(pPreset, outStr, 1);
	outStr << "env3";
	WriteEnv(pPreset, outStr, 2);
	outStr << "env4";
	WriteEnv(pPreset, outStr, 3);
	outStr << "env5";
	WriteEnv(pPreset, outStr, 4);
	outStr << "env6";
	WriteEnv(pPreset, outStr, 5);
}

void WriteFil(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0048;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterType }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterMM1Mode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterVowel1 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterVowel2 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterVowel3 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterVowel4 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterVowel5 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterResonance });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterDamping });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterCutoff });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterKeycenter });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterKeytracking });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::Input,0,0 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterXFade });
	ptr += 2;	// unknown values
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterObieMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Filter, index, PortType::None,0,filterCombMode }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGFil(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gFil";
	int32_t size = 0x02E0;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "fil1";
	WriteFil(pPreset, outStr, 0);
	outStr << "fil2";
	WriteFil(pPreset, outStr, 1);
	outStr << "fil3";
	WriteFil(pPreset, outStr, 2);
	outStr << "fil4";
	WriteFil(pPreset, outStr, 3);
	outStr << "mod1";
	WriteMod(pPreset, outStr, ElementType::Filter, 0);
	outStr << "mod2";
	WriteMod(pPreset, outStr, ElementType::Filter, 1);
	outStr << "mod3";
	WriteMod(pPreset, outStr, ElementType::Filter, 2);
	outStr << "mod4";
	WriteMod(pPreset, outStr, ElementType::Filter, 3);
}

void WriteVca(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0018;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::None,0,vcaType });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::None,0,vcaBoost });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::Mod,port1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::Mod,port1,psiSource}) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::Input,port1,inpSource });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA,index,PortType::None,0,vcaLevel });

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGVca(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gVca";
	int32_t size = 0x0080;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "vca1";
	WriteVca(pPreset, outStr, 0);
	outStr << "vca2";
	WriteVca(pPreset, outStr, 1);
	outStr << "vca3";
	WriteVca(pPreset, outStr, 2);
	outStr << "vca4";
	WriteVca(pPreset, outStr, 3);
}


void WriteMix(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x004C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port1,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port2,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port3,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port4,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port2,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port3,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port4,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port1,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port2,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port3,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Input,port4,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port1,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port2,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port3,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port4,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::None,0,mixOutputLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port5,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Mixer,index,PortType::Mod,port5,psiSource }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGMix(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gMix";
	int32_t size = 0x0150;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "mix1";
	WriteMix(pPreset, outStr, 0);
	outStr << "mix2";
	WriteMix(pPreset, outStr, 1);
	outStr << "mix3";
	WriteMix(pPreset, outStr, 2);
	outStr << "mix4";
	WriteMix(pPreset, outStr, 3);
}


void WriteLfo(Preset* pPreset, std::ostream& outStr, ElementType elemType, int index)
{
	int32_t size = 0x0030;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoRate });	// coarse tuning when No Track is enabled 
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoShape }) * 4;
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoPhase });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoRetrigger });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoFadeIn });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoFadeOut });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoDelayStart });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoOffset }) ;
	ptr += 1;	// skip unknown value
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoLevel });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoClockSync });
	*ptr++ = pPreset->GetParameterValue({ elemType,index,PortType::None,0,lfoRateSync });

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGLfo(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gLfo";
	int32_t size = 0x030C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "lfo1";
	WriteLfo(pPreset, outStr, ElementType::LFO, 0);
	outStr << "lfo2";
	WriteLfo(pPreset, outStr, ElementType::LFO, 1);
	outStr << "lfo3";
	WriteLfo(pPreset, outStr, ElementType::LFO, 2);
	outStr << "lfo4";
	WriteLfo(pPreset, outStr, ElementType::LFO, 3);
	outStr << "lfo5";
	WriteLfo(pPreset, outStr, ElementType::VibratoLFO, 0);
	outStr << "mod1";
	WriteMod(pPreset, outStr, ElementType::LFO, 0);
	outStr << "mod2";
	WriteMod(pPreset, outStr, ElementType::LFO, 1);
	outStr << "mod3";
	WriteMod(pPreset, outStr, ElementType::LFO, 2);
	outStr << "mod4";
	WriteMod(pPreset, outStr, ElementType::LFO, 3);
	outStr << "mod5";
	WriteMod(pPreset, outStr, ElementType::VibratoLFO, 0);
}


void WriteRot(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0050;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port1,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port2,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port3,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port4,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotCoarseNoTrack });	// coarse tuning when NoTrack is enabled 
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotFine });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotXFade });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotPhase });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotNoTrack }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port1,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port2,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port3,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::Input,port4,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotPhaseSync }) * 4;
	ptr += 2;	// skip unknown values
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotCoarse });	// coarse tuning when no buttons are enabled
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotLow });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotClockSync });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Rotor,index,PortType::None,0,rotCoarseSync });	// coarse tuning when clock sync is enabled 

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGRot(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gRot";
	int32_t size = 0x0180;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "rot1";
	WriteRot(pPreset, outStr, 0);
	outStr << "rot2";
	WriteRot(pPreset, outStr, 1);
	outStr << "mod1";
	WriteMod(pPreset, outStr, ElementType::Rotor, 0);
	outStr << "mod2";
	WriteMod(pPreset, outStr, ElementType::Rotor, 1);
}


void WritePan(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0010;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA, index, PortType::None,0,vcaInitPan });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA, index, PortType::Mod, 1, psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCA, index, PortType::Mod, 1, psiSource }) * 4;

	// VCAMaster inputs are really Enable Part parameters.
	*ptr++ = pPreset->GetParameterValue({ ElementType::VCAMaster, 0, PortType::Input, index, psiSource }) == 0 ? 1 : 0;	// 0 = ON, 1 = OFF

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGPan(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gPan";
	int32_t size = 0x0060;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "pan1";
	WritePan(pPreset, outStr, 0);
	outStr << "pan2";
	WritePan(pPreset, outStr, 1);
	outStr << "pan3";
	WritePan(pPreset, outStr, 2);
	outStr << "pan4";
	WritePan(pPreset, outStr, 3);
}

void WriteIfx(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x001C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::None,0,insFxMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::None,0,insFxValue });
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::Control,port1,ctrlStrength });
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::Mod,port1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::Input,port1,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::Mod,port1,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::InsertFX, index, PortType::Control,port1,ctrlSource }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGIfx(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gIfx";
	int32_t size = 0x0090;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "ifx1";
	WriteIfx(pPreset, outStr, 0);
	outStr << "ifx2";
	WriteIfx(pPreset, outStr, 1);
	outStr << "ifx3";
	WriteIfx(pPreset, outStr, 2);
	outStr << "ifx4";
	WriteIfx(pPreset, outStr, 3);
}


void WriteAmp(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x001C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::None,0,amAlgorithm }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::None,0,amOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::Control,port1,ctrlStrength });
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::Mod,port1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::Input,port1,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::Mod,port1,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::AM, index, PortType::Control,port1,ctrlSource }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGAmp(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gAmp";
	int32_t size = 0x0048;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "amp1";
	WriteAmp(pPreset, outStr, 0);
	outStr << "amp2";
	WriteAmp(pPreset, outStr, 1);
}

void WriteVmx(Preset* pPreset, std::ostream& outStr, int index)
{
	int32_t size = 0x0038;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));	// write size

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port1,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port2,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port3,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port4,inpLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::None,0,vecXOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::None,0,vecYOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Mod,port1,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Mod,port2,psiAmount });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port1,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port2,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port3,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Input,port4,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Mod,port1,psiSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Vector, index, PortType::Mod,port2,psiSource }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGVmx(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gVmx";
	int32_t size = 0x0080;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(size));

	outStr << "vmx1";
	WriteVmx(pPreset, outStr, 0);
	outStr << "vmx2";
	WriteVmx(pPreset, outStr, 1);
}

void WriteGLeg(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gLeg";
	int32_t size = 0x00B0;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "leg1";
	size = 0x00A8;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime5 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel5 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel5 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime6 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel6 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel6 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime7 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel7 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel7 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legTime8 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legXLevel8 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legYLevel8 });

	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legSlope });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legLoop });
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legRepeat }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legStart }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legKeyOff }) * 4;

	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::Mod,port1,psiAmount });			// mod 1 amount (time)
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::Mod,port2,psiAmount });			// mod 0 amount (level)
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::Mod,port1,psiSource }) * 4;	// mod 1 source (time)
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::Mod,port2,psiSource }) * 4;	// mod 0 source (level)
	ptr += 8;	// skip unknown values
	*ptr++ = pPreset->GetParameterValue({ ElementType::LoopEG,0,PortType::None,0,legSync }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}


void WriteGArp(Preset* pPreset, std::ostream& outStr)
{
	pPreset;

	outStr << "gArp";
	int32_t size = 0x00B4;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "arp1";
	size = 0x00AC;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];
	ptr;

	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpOctave }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpSwing });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpResolution }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpNoteLength });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpHold }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpPatternLength }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpPattern }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloArpButton }) * 4;
	*ptr++ = 1;	// arp pattern file version?

	// The Preset class keeps separate paramaters for volume, gate length and gate enable.
	// So we need to combine them into a single value before writing them.
	for (int i = 0; i < 32; ++i)
	{
		int32_t stepVolume = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0, i + arpStepVolumeOffset });
		int32_t gateLength = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0, i + arpStepGateLengthOffset });
		int32_t gateEnable = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0, i + arpStepGateOffset });
		int32_t val = (gateEnable << 20) | (gateLength << 8) | stepVolume;
		*ptr++ = val;
	}

	*ptr++ = pPreset->GetParameterValue({ ElementType::Arpeggiator,0,PortType::None,0,arpVelocity }) * 4;

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}


void WriteGSeq(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gSeq";
	int32_t size = 0x012C;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "seq1";
	size = 0x0124;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::SeqGlobal,0,PortType::None,0,seqMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::SeqGlobal,0,PortType::None,0,seqSwing }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::SeqGlobal,0,PortType::None,0,seqDivision }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::SeqGlobal,0,PortType::None,0,seqPattern }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloSeqButton }) * 4;

	// seq steps
	for (int track = 0; track < 4; track++)
	{
		for (int step = 1; step < 17; step++)
		{
			*ptr++ = pPreset->GetParameterValue({ ElementType::Seq,track,PortType::None,0,step });
		}
	}
	for (int track = 0; track < 4; track++)
	{
		*ptr++ = pPreset->GetParameterValue({ ElementType::Seq,track,PortType::None,0,seqPatternLength });
	}

	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}

void WriteGGfx(Preset* pPreset, std::ostream& outStr)
{
	outStr << "gGfx";
	int32_t size = 0x1200;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	outStr << "gfx1";
	size = 0x11F8;
	outStr.write(reinterpret_cast<char*>(&size), sizeof(int32_t));

	vector<int32_t> buf(size / sizeof(int32_t));
	int32_t* ptr = &buf[0];

	*ptr++ = pPreset->GetParameterValue({ ElementType::Output,0,PortType::Input,0,0 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpFrequency });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpDepth });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpInLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpFeedback });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpDry });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpWet });
	*ptr++ = pPreset->GetParameterValue({ ElementType::ChorusFlanger,0,PortType::None,0,cfpPhase });
	ptr += 3; // skip unknown values 

	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpFrequency });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpDepth });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpInLevel });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpFeedback });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpDry });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpWet });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Phaser,0,PortType::None,0,cfpPhase });
	ptr += 3; // skip unknown values 

	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delTimeSecondsL });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delTimeSecondsR });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delFeedR });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delFeedL });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delDamping });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delDry });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delWet });
	ptr += 1; // skip unknown value 
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delMidiClk });
	ptr += 1; // skip unknown value 

	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqFreq1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqQ1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqGain1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqFreq2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqQ2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqGain2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqFreq3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqQ3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EQ,0,PortType::None,0,eqGain3 });
	ptr += 3; // skip unknown values 

	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delTimeBeatsL }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Delay,0,PortType::None,0,delTimeBeatsR }) * 4;

	*ptr++ = pPreset->GetParameterValue({ ElementType::System,0,PortType::None,0,sysTranspose });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloBPM });
	ptr += 1; // skip unknown value 
	*ptr++ = pPreset->GetParameterValue({ ElementType::System,0,PortType::None,0,sysVTIntens });
	*ptr++ = pPreset->GetParameterValue({ ElementType::System,0,PortType::None,0,sysVTOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloNotePriority }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloPlayMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloUniVoice }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloUniTune });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloGldType }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloGldTime });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloGldRate });
	ptr += 1; // skip unknown value 
	*ptr++ = pPreset->GetParameterValue({ ElementType::System,0,PortType::None,0,sysATIntens });
	*ptr++ = pPreset->GetParameterValue({ ElementType::System,0,PortType::None,0,sysATOffset });
	ptr += 1; // skip unknown value 
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloEgReset }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloLegato }) * 4;
	ptr += 2; // skip unknown values 

	*ptr++ = pPreset->GetParameterValue({ ElementType::EnvFol,0,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::EnvFol,0,PortType::None,0,envFolInput });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EnvFol,0,PortType::None,0,envFolAttack });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EnvFol,0,PortType::None,0,envFolRelease });
	*ptr++ = pPreset->GetParameterValue({ ElementType::EnvFol,0,PortType::None,0,envFolOutput });

	*ptr++ = pPreset->GetParameterValue({ ElementType::Output,1,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Output,2,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Output,3,PortType::Input,0,inpSource }) * 4;
	ptr += 1; // skip unknown value 

	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,0,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,1,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,2,PortType::Input,0,inpSource }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,3,PortType::Input,0,inpSource }) * 4;

	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,0,PortType::None,0,fxChannelSlot1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,0,PortType::None,0,fxChannelSlot2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,0,PortType::None,0,fxChannelSlot3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,0,PortType::None,0,fxChannelSlot4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,1,PortType::None,0,fxChannelSlot1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,1,PortType::None,0,fxChannelSlot2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,1,PortType::None,0,fxChannelSlot3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,1,PortType::None,0,fxChannelSlot4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,2,PortType::None,0,fxChannelSlot1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,2,PortType::None,0,fxChannelSlot2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,2,PortType::None,0,fxChannelSlot3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,2,PortType::None,0,fxChannelSlot4 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,3,PortType::None,0,fxChannelSlot1 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,3,PortType::None,0,fxChannelSlot2 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,3,PortType::None,0,fxChannelSlot3 });
	*ptr++ = pPreset->GetParameterValue({ ElementType::FXChannel,3,PortType::None,0,fxChannelSlot4 });
	
	*ptr++ = pPreset->GetParameterValue({ ElementType::Ribbon,0,PortType::None,0,ribOffset });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Ribbon,0,PortType::None,0,ribIntensity });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Ribbon,0,PortType::None,0,ribHold }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Ribbon,0,PortType::None,0,ribTouchOffset }) * 4;

	*ptr++ = 0x5459454B;	// "KEYT"
	*ptr++ = 0x00004241;	// "AB"

	for (int table = 0; table < 4; table++)
	{
		int32_t* pPreviousActive = nullptr;
		for (int index = 0; index < 128; index++)
		{
			float valueFloat = static_cast<float>(pPreset->GetParameterValue({ ElementType::KeyTable,table,PortType::None,0, index }));
			if (valueFloat != -1.0)
			{
				// The values are saved in units of 0.1 of a percent. So divide by 1000.
				valueFloat /= 1000.0;

				// Value is active. So point the previous active element to this one (if there is one).
				if (pPreviousActive)
				{
					*pPreviousActive = index;
				}
				pPreviousActive = ptr + 1;
			}

			*reinterpret_cast<float*>(ptr) = valueFloat;
			ptr++;

			*ptr++ = -1;	// initialize next active element to -1. Might be changed later by pPreviousActive.
		}
	}

	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloGldMode }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloGldRange });

	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,0,PortType::None,0,lagTime });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,1,PortType::None,0,lagTime });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,2,PortType::None,0,lagTime });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,3,PortType::None,0,lagTime });
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,0,PortType::Input,0,lagTime }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,1,PortType::Input,0,lagTime }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,2,PortType::Input,0,lagTime }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Lag,3,PortType::Input,0,lagTime }) * 4;

	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloExpPedal }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloSusPedal1 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloSusPedal2 }) * 4;
	ptr += 3; // skip unknown values (No these are not pedal polarities. Those are not saved.)
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloAssignFunc1 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloAssignFunc2 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloAssignMode1 }) * 4;
	*ptr++ = pPreset->GetParameterValue({ ElementType::Home,0,PortType::None,0,gloAssignMode2 }) * 4;


	outStr.write(reinterpret_cast<char*>(&buf[0]), size);
}


void SavePresetFile(const string& filePath, Preset* pPreset)
{
	std::ofstream outFile;
	outFile.open(filePath, std::ios::out | std::ios::binary | std::ios::trunc);

	if (!outFile)
	{
		string msg = "Cannot open the file: ";
		msg += filePath;
		throw std::runtime_error(msg);
	}

	outFile << "PRST";

	int32_t val = 0x000025E8;
	outFile.write(reinterpret_cast<char*>(&val), sizeof(val));
	val = 1;	// version number
	outFile.write(reinterpret_cast<char*>(&val), sizeof(val));

	WriteGInf(pPreset, outFile);
	WriteGPit(pPreset, outFile);
	WriteGVib(pPreset, outFile);
	WriteGOsc(pPreset, outFile);
	WriteGEnv(pPreset, outFile);
	WriteGFil(pPreset, outFile);
	WriteGVca(pPreset, outFile);
	WriteGMix(pPreset, outFile);
	WriteGLfo(pPreset, outFile);
	WriteGRot(pPreset, outFile);
	WriteGPan(pPreset, outFile);
	WriteGIfx(pPreset, outFile);
	WriteGAmp(pPreset, outFile);
	WriteGVmx(pPreset, outFile);
	WriteGLeg(pPreset, outFile);
	WriteGArp(pPreset, outFile);
	WriteGSeq(pPreset, outFile);
	WriteGGfx(pPreset, outFile);


	outFile.close();
}
