/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef FLOWVIEW_H_INCLUDED
#define FLOWVIEW_H_INCLUDED


#include <cassert>

#include "../JuceLibraryCode/JuceHeader.h"
#include "ElementView.h"
#include "ConnectorView.h"
#include "Preset.h"
#include "ViewModel.h"
#include "Notifier.h"

class SelectionRect : public Component
{
public:
	virtual void paint(Graphics& g) override;
};
using SelectionRectUPtr = std::unique_ptr < SelectionRect > ;

//===========================================================================
//
//		Surface
//
//===========================================================================
class Surface : public Component,
	public ConnectionMng,
	public DragAndDropContainer,
	public ComponentListener
{
public:
	Surface(Preset* pPreset, CommandHistory* pCommandHistory, ViewModel* pViewModel);
	virtual ~Surface();

	void Update();

	// Note that the Preset surface is actually transparent so that we don't need to spend 
	// time on a Fill operation. Instead we set the background color of the main window.
	//void paint(Graphics& g) override
	//{
	//	g.fillAll(Colour(0xffffffff));
	//}

	void mouseDown(const MouseEvent & event) override;
	void mouseUp(const MouseEvent & event) override;
	void mouseDrag(const MouseEvent& event) override;
	void mouseEnter(const MouseEvent & event) override;
	void mouseExit(const MouseEvent & event) override;
	void modifierKeysChanged(const ModifierKeys& modifiers) override;

	/* Functions reimplemented from the ConnectionMng interface  */
	void OnCreateDragConnector(PortView* pStartingPortView, PortView* pEndingPortView) override;
	void OnMoveDragConnector(PortView* pPortView, Point<int> newPosition) override;
	void FinalizeDrop(PortView* pPortView, Point<int> newPosition) override;
	
	virtual void dragOperationEnded(const DragAndDropTarget::SourceDetails &) override;

	void CollapseAll();
	void ExpandAll();
	void RemoveSelected();
	void SelectAll();

	Notifier<void(bool)> ExpandCollapseEvent;
	using ExpandCollapseConnection = decltype(ExpandCollapseEvent)::ConnectionType;	///< ExpandCollapseEvent.Connect() returns this type

	Notifier<void(const Point<int>&)> SurfaceDragEvent;
	using SurfaceDragConnection = decltype(SurfaceDragEvent)::ConnectionType;	///< SurfaceDragEvent.Connect() returns this type

	Notifier<void(PortView*)> ConnectorDragStartStopEvent;
	using ConnectorDragStartStopConnection = decltype(ConnectorDragStartStopEvent)::ConnectionType;	///< ConnectorDragStartStopEvent.Connect() returns this type


	ElementView* FindViewFromModel(const ElementID& elemID) const;
	Component* FindViewFromModel(const Connector& connector) const;
	Component* FindPortViewFromID(const PortID& id) const;

	class SurfacePopupMenuHandler : public juce::ModalComponentManager::Callback
	{
	public:
		SurfacePopupMenuHandler(Surface* pSurface, Point<int> position)
			: m_pSurface(pSurface), m_position(position)
		{}

		virtual void modalStateFinished(int returnValue) override;

	private:
		Point<int> m_position;
		Surface* m_pSurface;
	};
	void Clear();

	void componentMovedOrResized(Component& component, bool	wasMoved, bool wasResized) override;

private:
	void RemoveConnectorView(ConnectorView* pConnectorViewToRemove);
	void RemoveElementView(ElementView* pElementViewToRemove);
	void SelectElementsThatIntersectRectangle(Rectangle<int> selectionRect);
	void DoLayout();

	Preset*								m_pPreset;
	CommandHistory*						m_pCommandHistory;
	ViewModel*							m_pViewModel;
	/** @brief Contains the ElementViews that are currently active based on the ViewModel. This list is updated each time the Update() function is called.*/
	std::list<ElementViewUPtr>			m_elementViewList;		
	std::list<ConnectorViewUPtr>		m_connectorViewList;
	ConnectorViewUPtr					m_pDragConnectorView;
	std::list<MenuItem>					m_menulist;
	bool								m_moveDragInProgress{ false };
	bool								m_selectDragInProgress{ false };
	SelectionRectUPtr					m_selectionRect;
	Point<int>							m_lastMouseDragPosition;
	bool								m_enableLayout{ true };
	ViewModel::VisibleElementsChangedConnection m_visibleElementsChangedConnection;
	std::function<void(int, int, bool)>	m_moveSelectedElementsBy;
	SelectionModel<ElementID>::SelectionChangedConnection	m_elementSelectionChangedConnection;
	SelectionModel<Connector>::SelectionChangedConnection	m_connectorSelectionChangedConnection;
};
using SurfaceUPtr = std::unique_ptr < Surface >;


class SurfaceViewport :	public Viewport
{
public:
	void SetSurface(Surface* pSurface)
	{
		assert(pSurface != nullptr);
		if (pSurface)
		{
			m_SurfaceDragConnection = pSurface->SurfaceDragEvent.Connect([this](const Point<int>& delta)
			{
				// The surface has been dragged.
				Point<int> viewPos = getViewPosition();
				setViewPosition(viewPos - delta);
			});
		}
	}

	// For debugging.
	// void resized() override
	// {
	// 	Viewport::resized();
	// 	Rectangle<int> bnds = getBounds();
	// 	DBG(" VP bounds x = " << bnds.getX() << ", y = " << bnds.getY() << ", width = " << bnds.getWidth() << "height = " << bnds.getHeight());
	// };

private:
	Surface::SurfaceDragConnection m_SurfaceDragConnection;
};

#endif  // FLOWVIEW_H_INCLUDED
