/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef OSCILLATORPANEL_H_INCLUDED
#define OSCILLATORPANEL_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
class CommandHistory;

class OscillatorPanel : 
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener,
	public Button::Listener
	//public PresetListener
{
public:
	OscillatorPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID);
	virtual ~OscillatorPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	Label			m_oscTypeLabel;
	ComboBox		m_oscTypeCombo;
	Label			m_waveLabel;
	std::vector<ComboBox>	m_waveCombos;
	Label			m_shapeLabel;
	SliderProp		m_shapeSlider;
	Label			m_coarseLabel;
	SliderProp		m_coarseSlider;
	ComboBox		m_coarseSyncCombo;
	SliderFreq20KHz	m_coarseNoTrackSlider;
	Label			m_fineLabel;
	SliderProp		m_fineSlider;
	ToggleButton	m_glideButton;
	SliderTime		m_glideRateSlider;
	Label			m_phaseLabel;
	SliderProp		m_phaseSlider;
	ToggleButton	m_clockSyncButton;
	ToggleButton	m_noTrackButton;
	ToggleButton	m_lowButton;
	
	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;

};




#endif  // OSCILLATORPANEL_H_INCLUDED
