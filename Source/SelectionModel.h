/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SELECTIONMODEL_H_INCLUDED
#define SELECTIONMODEL_H_INCLUDED

#include <functional>
#include <algorithm>
#include <set>
#include <list>
#include <memory>
#include "Notifier.h"


template<class T>
class SelectionModel : public std::set<T>
{
public:
	// These are needed for Mac
	using std::set<T>::begin;
	using std::set<T>::end;
	using std::set<T>::empty;
	using std::set<T>::find;
	using std::set<T>::insert;
	using std::set<T>::erase;
	using std::set<T>::clear;

	void Select(T pSelectedItem);
	void SelectAppend(T pSelectedItem);
	void DeSelect(T selectedItem);
	void ToggleSelection(T selectedItem);
	bool IsSelected(T pItem) const throw();
	bool HasSelection() const throw();
	void ForEachSelected(std::function<void(const T&)> func) const;

	void Clear();
	T GetFirstSelection();

	Notifier<void(void)> selectionChangedEvent;
	using SelectionChangedConnection = Notifier<void(void)>::ConnectionType;	///< For convenience. selectionChangedEvent.Connect() returns this type

};


template<class T>
void SelectionModel<T>::Select(T selectedItem)
{
	if (find(selectedItem) == end())	// if the selected item is not in the set (is not already selected)
	{
		clear();
		insert(selectedItem);
		selectionChangedEvent.Notify();
	}
}

template<class T>
void SelectionModel<T>::SelectAppend(T selectedItem)
{
	if (find(selectedItem) == end())	// if the selected item is not in the set (is not already selected)
	{
		insert(selectedItem);
		selectionChangedEvent.Notify();
	}
}

template<class T>
void SelectionModel<T>::DeSelect(T selectedItem)
{
	auto iter = find(selectedItem);
	if (iter != end())	// if the selected item is in the set (is selected)
	{
		erase(iter);
		selectionChangedEvent.Notify();
	}
}


template<class T>
void SelectionModel<T>::ToggleSelection(T selectedItem)
{
	auto iter = find(selectedItem);
	if (iter != end())	// if the selected item is in the set (is selected)
	{
		erase(iter);
	}
	else
	{
		insert(selectedItem);
	}
	selectionChangedEvent.Notify();
}

template<class T>
bool SelectionModel<T>::IsSelected(T item) const throw()
{
	auto iter = find(item);
	return iter != end();
}

template<class T>
void SelectionModel<T>::Clear()
{
	clear();
	selectionChangedEvent.Notify();
}

template<class T>
bool SelectionModel<T>::HasSelection() const throw()
{
	return !empty();
}

template<class T>
void SelectionModel<T>::ForEachSelected(std::function<void (const T& )> func) const
{
	std::for_each(begin(),end(),func);
}

template<class T>
T SelectionModel<T>::GetFirstSelection()
{
	assert(!empty());
	return *begin();
}

#endif  // SELECTIONMODEL_H_INCLUDED
