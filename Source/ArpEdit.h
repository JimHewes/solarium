/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef ARPEDIT_H_INCLUDED
#define ARPEDIT_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include <vector>
#include <memory>
#include <functional>
#include "Slider2.h"
#include "MiscControls.h"
#include "Preset.h"
#include "Command.h"

using std::vector;

class SliderGateLength : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class ArpStep : public Component,
				public Slider2::Listener,
				public Button::Listener
{
public:
	ArpStep(int stepIndex, Preset* pPreset, CommandHistory* pCommandHistory);
	void resized() override;
	void paint(Graphics& g) override;
	void sliderValueChanged(Slider2* slider) override;
	void buttonClicked(Button *) override;

private:
	void OnParameterChanged(const ParamID& paramID, int value, bool isMuted);
	void Update();

	int					m_stepIndex;
	Preset*				m_pPreset{};
	CommandHistory*		m_pCommandHistory;
	Label				m_stepNumberLabel;
	Slider2				m_volumeSlider;
	SliderGateLength	m_gateLengthSlider;
	TextButton			m_gateButton;
	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;

};
using ArpStepUPtr = std::unique_ptr<ArpStep>;


class ArpControlsComponent : public Component,
							public ComboBox::Listener,
							public Slider2::Listener
{
public:
	ArpControlsComponent(Preset* pPreset, CommandHistory* pCommandHistory);
	void resized() override;
	void sliderValueChanged(Slider2* slider) override;
	void comboBoxChanged(ComboBox* comboBoxThatHasChanged) override;
	void Reset(CmdMacro* pCmdMacro);

private:
	void OnParameterChanged(const ParamID& paramID, int value, bool isMuted);
	void Update();

	Preset*			m_pPreset{};
	CommandHistory*	m_pCommandHistory;
	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;

	Label	m_bpmLabel;
	Label	m_modeLabel;
	Label	m_octaveLabel;
	Label	m_patternLabel;
	Label	m_resolutionLabel;
	Label	m_noteLengthLabel;
	Label	m_velocityLabel;
	Label	m_patternLengthLabel;
	Label	m_swingLabel;

	Slider2				m_bpmSlider;
	ComboBox			m_modeComboBox;
	ComboBox			m_octaveComboBox;
	Label				m_patternText;
	ComboBox			m_resolutionComboBox;
	Slider2				m_noteLengthSlider;
	ComboBox			m_velocityComboBox;
	ComboBox			m_patternLengthComboBox;
	SliderSwing			m_swingSlider;
	PanelButtonPreset	m_holdButton;

};

class ArpStepMask : public Component
{
public:
	void paint(Graphics& g) override
	{
		g.fillAll(Colour(0x30000000));
	}
	bool hitTest(int x, int y) override 
	{
		x; y;
		return false;
	}
};
using ArpStepMaskUPtr = std::unique_ptr<ArpStepMask>;

class ArpStepsComponent : public Component
{
public:
	ArpStepsComponent(Preset* pPreset, CommandHistory* pCommandHistory);
	void resized() override;
	void Update();

	static const uint8_t flgVolume = 1;
	static const uint8_t flgGateLength = 2;
	static const uint8_t flgGate = 4;
	void Reset(CmdMacro* pCmdMacro, uint8_t flags);

private:
	Preset*					m_pPreset{};
	vector<ArpStepUPtr>		m_arpSteps;
	vector<ArpStepMaskUPtr> m_arpStepMasks;

	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};

/** @brief The ArpContentComponent occupies the entire Arp Edit window and is where all the content is placed.
*/
class ArpContentComponent : public Component,
							public ApplicationCommandTarget
{
public:
	ArpContentComponent(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget);
	ArpContentComponent(const ArpContentComponent&) = delete;
	ArpContentComponent& operator=(const ArpContentComponent&) = delete;
	void resized() override;

	// The following methods support the ApplicationCommandTarget interface
	ApplicationCommandTarget* getNextCommandTarget() override;
	void getAllCommands(Array<CommandID>& commands) override;
	void getCommandInfo(CommandID commandID, ApplicationCommandInfo& result) override;
	bool perform(const InvocationInfo& info) override;

private:
	void DoFileOpen();
	void DoFileSave();
	void DoFileSaveAs();
	void SavePatternFile();

	Preset*								m_pPreset{};
	CommandHistory*						m_pCommandHistory;
	ApplicationCommandTarget*			m_pDefaultCommandTarget;
	ArpControlsComponent				m_arpControlComponent;
	ArpStepsComponent					m_arpStepComponent;
	std::unique_ptr<MenuBarModel>		m_pArpMenuModel;
	std::unique_ptr<MenuBarComponent>	m_pArpMenuComponent;
	std::string							m_currentFilePath;
};


class ArpMenuBarModel : public MenuBarModel
{
public:
	ArpMenuBarModel()
	{	}

	StringArray getMenuBarNames() override;
	PopupMenu getMenuForIndex(int menuIndex, const String& menuName) override;
	void menuItemSelected(int menuItemID, int topLevelMenuIndex) override;
private:
};

class ArpEditWindow :	public DocumentWindow
{
public:
	ArpEditWindow(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget, std::function<void(void)> closeFunction);
	ArpEditWindow(const ArpEditWindow&) = delete;
	void moved() override;

	void closeButtonPressed() override
	{
		m_closeFunction();
	}

private:
	std::function<void(void)> m_closeFunction;
};




#endif  // ARPEDIT_H_INCLUDED
