/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <math.h>
#include <sstream>
#include <iomanip>
#include <cassert>
#include "gsl_util.h"
#include "ElementView.h"
#include "Preset.h"
#include "AppCmdMgr.h"
#include "Surface.h"
#include "Command.h"

using std::vector;
using std::string;
using gsl::narrow;


ArrowButton2::ArrowButton2(const String& name,float arrowDirectionInRadians,Colour arrowColour)
	: Button(name),colour(arrowColour)
{
	path.addTriangle(0.0f,0.0f,0.0f,1.0f,1.0f,0.5f);
	path.applyTransform(AffineTransform::rotation(float_Pi * 2.0f * arrowDirectionInRadians,0.5f,0.5f));
}

ArrowButton2::~ArrowButton2() {}

void ArrowButton2::FlipArrowDirection()
{
	float arrowDirectionInRadians = 0.50f;
	path.applyTransform(AffineTransform::rotation(float_Pi * 2.0f * arrowDirectionInRadians,0.5f,0.5f));
}


void ArrowButton2::paintButton(Graphics& g,bool /*isMouseOverButton*/,bool isButtonDown)
{
	Path p(path);

	const float offset = isButtonDown ? 1.0f : 0.0f;
	p.applyTransform(path.getTransformToScaleToFit(offset,offset,getWidth() - 3.0f,getHeight() - 3.0f,false));

	//DropShadow(Colours::black.withAlpha(0.3f),isButtonDown ? 2 : 4,Point<int>()).drawForPath(g,p);

	g.setColour(colour);
	g.fillPath(p);
}

String SliderLevel::getTextFromValue(double value)
{
	std::ostringstream os;
	if (m_precision > 0)
	{
		os << std::fixed << std::showpoint << std::setprecision(m_precision);
	}
	os << value / m_divisor;
	return os.str();
}

double SliderLevel::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	int valInt = lrint(value * m_divisor);
	return valInt;
}

void SliderLevel::SetDivisor(uint_fast16_t divisor)
{
	m_divisor = divisor;
	updateText();
}
void SliderLevel::SetPrecision(uint_fast8_t precision)
{
	m_precision = precision;
	updateText();
}


PortLevelControl::PortLevelControl(Preset* pPreset, CommandHistory* pCommandHistory, PortID portID,Rectangle<int> bounds)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_portID(portID)
{
	m_portView = std::make_unique<PortView>(portID);


	ParamID levelParam = ParamID(m_portID, psiAmount);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(levelParam);
	if (rangeInfo.minValue != rangeInfo.maxValue)		// min == max == 0 is a special case flag meaning no amount slider
	{
		m_slider.setSliderStyle(Slider2::LinearBar);
		m_slider.setColour(Slider2::ColourIds::thumbColourId,
			(portID.portType == PortType::Control) ? Colours::wheat : Colour(0xffa5d8b8));
		UpdateSliderFromModel();
		m_slider.addListener(this);
		addAndMakeVisible(&m_slider);
	}

	addAndMakeVisible(m_portView.get());

	m_label = std::to_string(m_portID.portIndex + 1);
	if (m_portID.portType == PortType::Mod)
	{
		auto modDestinations = m_pPreset->GetModDestinationNameList(m_portID.elemID, m_portID.portIndex);
		if (modDestinations.size() > 1)
		{
			addAndMakeVisible(&m_modDestination);
			PopulateDestCombo();
			UpdateDestComboFromModel();
			// m_modDestination.addListener(this);	// UpdateDestComboFromModel() will add listener so we don't need to here.
		}
		else
		{
			// If there's only one mod destination, there's no reason to have a combo box for it, 
			// so just add the text of the one choice.
			m_label += " " + modDestinations[0];
		}
	}
	else if (m_portID.portType == PortType::Input && m_portID.elemID.elemType != ElementType::VCAMaster)
	{
		m_label += " Level";
	}
	else if (m_portID.portType == PortType::Control)
	{
		m_label += " Mod";
	}

	m_parameterChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool) { OnParameterChanged(paramID, int16_t(value)); });

	// When an entire preset changes, we don't get events for individual parameters. Instead we get one event that 
	// multiple parameters changed. In this case we need to ensure the combobox choices are correct for the filter 
	// mod destinations because different filter types have a different list of destinations.
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool) 
	{
		if (m_portID.elemID.elemType == ElementType::Filter && m_portID.portType == PortType::Mod)
		{
			PopulateDestCombo();
			UpdateDestComboFromModel();
		}
	});

	setBounds(bounds);
}

PortLevelControl::~PortLevelControl()
{
	m_slider.removeListener(this);
	m_modDestination.removeListener(this);
	m_parameterChangedConnection->Disconnect();
	m_multipleParametersChangedConnection->Disconnect();
}

void PortLevelControl::OnParameterChanged(const ParamID& paramID, int16_t value)
{
	// if (m_portID.elemID.elemType == ElementType::Filter && m_portID.portType == PortType::Control && paramID.paramIndex == PortParam::level)
	// {
	// 	DBG("Setting Filter " << m_portID.elemID.elemIndex << " Control port index " << m_portID.portIndex << " strength to " << (int)value);
	// }

	// If the filter type changed, then we need to update the choice of destinations.
	if (m_portID.elemID.elemType == ElementType::Filter && paramID.elemType == ElementType::Filter && paramID.portType == PortType::None && paramID.subIndex == filterType && m_portID.portType == PortType::Mod)
	{
		PopulateDestCombo();
		UpdateDestComboFromModel();
	}

	// If the level/amount or destination changed, then we need to update the slider. 
	if (m_portID.elemID.elemType == paramID.elemType && m_portID.elemID.elemIndex == paramID.elemIndex &&
		m_portID.portType == paramID.portType && m_portID.portIndex == paramID.portIndex)
	{
		if (paramID.subIndex == psiAmount || paramID.subIndex == psiAmtPitch || paramID.subIndex == psiAmtShape)
		{
			UpdateSliderFromModel();
		}
		else if (paramID.subIndex == psiDest)
		{
			m_modDestination.setSelectedId(value + 1, NotificationType::dontSendNotification);

			// We also have to update the slider because changing the destination may mean that the Mod amount comes from a different parameter.
			UpdateDestCombo(value);
			UpdateSliderFromModel();
		}
	}
}

/** @brief Called by the JUCE framework when the user changes the modulation's destination combobox in a PortLevelControl.

	@param[in]	comboBoxThatHasChanged	A pointer to the ComboBox that was changed.
*/
void PortLevelControl::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	comboBoxThatHasChanged;
	int selectedID = m_modDestination.getSelectedId();
	assert(selectedID > 0);
	m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_portID, psiDest), selectedID - 1));
}

int PortLevelControl::GetLevelType()
{
	int levelType = psiAmount;
	if(m_portID.portType == PortType::Mod)
	{
		if (m_portID.elemID.elemType == ElementType::Oscillator &&
			m_pPreset->GetParameterValue(ParamID(m_portID, psiDest)) == 3)	// destination == shape
		{
			levelType = psiAmtShape;
		}

		else if ((m_portID.elemID.elemType == ElementType::Oscillator || 
			m_portID.elemID.elemType == ElementType::Rotor || 
			m_portID.elemID.elemType == ElementType::LFO || 
			m_portID.elemID.elemType == ElementType::VibratoLFO || 
			m_portID.elemID.elemType == ElementType::Filter) && 
			(m_pPreset->GetParameterValue(ParamID(m_portID, psiDest)) == 1))	// destination == pitch
		{
			levelType = psiAmtPitch;
		}
	}

	return levelType;
}

void PortLevelControl::UpdateSliderFromModel()
{
	//DBG("Entered UpdateSliderFromModel: " << GetParameterDetail(ParamID(m_portID)));

	// For debugging.
	// if (m_portID.elemID.elemType == ElementType::Filter && m_portID.portType == PortType::Control)
	// {
	// 	DBG("Setting Filter " << m_portID.elemID.elemIndex << " Control port index " << m_portID.portIndex << " strength to " << m_pPreset->GetParameterValueDbl(levelParam));
	// }

	// The levelType may be psiAmount, psiAmtPitch, or psiAmtShape depending on the destination of the modulation in some element types.
	ParamID levelParam = ParamID(m_portID, GetLevelType());

	auto rangeInfo = m_pPreset->GetParameterRangeInfo(levelParam);
	if (rangeInfo.minValue != rangeInfo.maxValue)		// min == max == 0 is a special case flag meaning there's no amount slider
	{
		// setRange causes the slider to get repainted. So make sure you set the precision first.
		m_slider.SetDivisor(rangeInfo.divisor);
		m_slider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_slider.setValue(m_pPreset->GetParameterValue(levelParam), NotificationType::dontSendNotification);
	
		// The precision is always rangeInfo.divisor / 10 except for special cases.
		auto precision = narrow<uint_fast8_t>((rangeInfo.divisor + 5) / 10);

		// special cases.
		// The mod amounts of these elements are displayed with one extra zero when the mod destinations are set to the pitch-related setting.
		if ((levelParam.elemType == ElementType::Oscillator || levelParam.elemType == ElementType::LFO || 
			levelParam.elemType == ElementType::VibratoLFO || levelParam.elemType == ElementType::Rotor || 
			levelParam.elemType == ElementType::Filter) && levelParam.portType == PortType::Mod)
		{
			ParamID destParam = levelParam;
			destParam.subIndex = psiDest;
			auto destValue = m_pPreset->GetParameterValue(destParam);
			if (destValue == 1)		// All the above elementa have the pitch-related setting as index 1.
			{
				precision += 1;	// show one extra zero
			}
		}
		m_slider.SetPrecision(precision);


		// For debugging.
		// if (m_portID.elemID.elemType == ElementType::Oscillator && m_portID.portType == PortType::Mod)
		// {
		// 	DBG("CBC Setting slider for OSC " << m_portID.elemID.elemIndex << " mod port index " << m_portID.portIndex << " to " << m_pPreset->GetParameterValueDbl(levelParam));
		// 	DBG("CBC Range max to " << maxDbl << " and step to " << stepDbl);
		// }
	}
}


/** @brief	This exists as a separate function to support changes to the filter type, which may require changing the 
			list of items in the destination combobox.
*/
void PortLevelControl::PopulateDestCombo()
{
	//DBG("Entered PopulateDestCombo: " << GetParameterDetail(ParamID(m_portID)));

	auto modDestinations = m_pPreset->GetModDestinationNameList(m_portID.elemID, m_portID.portIndex);
	int numModDestinations = modDestinations.size();

	assert(numModDestinations > 1);

	if (numModDestinations > 1)
	{
		// Temporarily disable the notifications while we re-populate the combobox.
		// This is necessarily when, for example, filter type is changed.
		m_modDestination.removeListener(this);
		m_modDestination.clear();
		for (int i = 0; i < numModDestinations; ++i)
		{
			m_modDestination.addItem(modDestinations[i], i + 1);
		}
		m_modDestination.addListener(this);
	}
}

void PortLevelControl::UpdateDestComboFromModel()
{
	//DBG("Entered UpdateDestComboFromModel: " << GetParameterDetail(ParamID(m_portID)));

	int destValue = m_pPreset->GetParameterValue(ParamID(m_portID, psiDest));
	UpdateDestCombo(destValue);
}

void PortLevelControl::UpdateDestCombo(int destValue)
{
	//DBG("Entered UpdateDestCombo: " << GetParameterDetail(ParamID(m_portID)));

	if (m_portID.portType == PortType::Mod)
	{
		// The number of mod destinations might be reduced when, say, then filter type is changed.
		// Note that if the destination index is reduced as a result of the filter type changing, this is not counted as 
		// a parameter change because the Solaris does this automatically on it's own. We are merely paralleling that behavior.
		//destValue = std::min(destValue, numModDestinations - 1);

		if (destValue + 1 != m_modDestination.getSelectedId())
		{
			// If NotificationType::dontSendNotification is specified here, then even disabling notifications will not prevent  
			// async notifications from occuring. But specifying SYNC notifications will.
			m_modDestination.removeListener(this);
			m_modDestination.setSelectedId(destValue + 1, NotificationType::sendNotificationSync);
			m_modDestination.addListener(this);
		}

	}
}

void PortLevelControl::UpdateFromModel()
{
	//DBG("Entered UpdateDestCombo: " << GetParameterDetail(ParamID(m_portID)));

	if (m_modDestination.getNumItems() > 0)
	{
		m_modDestination.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_portID, psiDest)) + 1, NotificationType::dontSendNotification);
	}

	UpdateSliderFromModel();
}


void PortLevelControl::sliderValueChanged(Slider2* slider)
{
	ParamID levelParam = ParamID(m_portID, GetLevelType());
	m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(levelParam, lrint(slider->getValue())));
}


void PortLevelControl::SetSliderColor(const Colour& color)
{
	m_slider.setColour(Slider::thumbColourId,color);
}


void PortLevelControl::resized()
{
	int portViewWidth = m_portView->getWidth();
	Rectangle<int> sliderBounds;
	Rectangle<int> lb = getLocalBounds();
	m_portView->setTopLeftPosition(0, (lb.getHeight() - m_portView->getHeight()) / 2);

	ParamID levelParam = ParamID(m_portID, psiAmount);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(levelParam);
	if (rangeInfo.minValue != rangeInfo.maxValue)
	{
		sliderBounds.setLeft(portViewWidth);
		sliderBounds.setWidth(int((lb.getWidth() - portViewWidth) * 0.53f));
		sliderBounds.setHeight(lb.getHeight() + 1);
		sliderBounds.setY(0);
		m_slider.setBounds(sliderBounds);
	}
	else
	{
		sliderBounds.setWidth(0);
	}

	m_textArea = lb;
	m_textArea.removeFromLeft(portViewWidth + m_slider.getWidth() + 5);	// Indent by 5 because we're going to left justify

	Rectangle<int> cb = m_textArea;
	cb.removeFromLeft(10);	// Indent combobox by 10. This give the number a square area.
	m_modDestination.setBounds(cb);
}


void PortLevelControl::paint(Graphics& g)
{
	g.setColour(Colours::black);
	Rectangle<int> lb = getLocalBounds();
	g.drawLine(0,0,(float)lb.getWidth(),0,1);
	g.drawLine(0,lb.getHeight() + 0.5f,(float)lb.getWidth(),lb.getHeight() + 0.5f,1);
	//g.drawRect(getLocalBounds());

	g.setFont(Font(13.0f));
	g.setColour(Colours::black);

	g.drawText(m_label,m_textArea,Justification::left,false);
}


PortLevelDivider::PortLevelDivider(const Rectangle<int>& bounds,const std::string& text)
	:m_text(text)
{
	setBounds(bounds);

	m_pArrowButton = std::make_unique<ArrowButton2>("Arrow",0.75f,Colour(Colours::grey));
	auto b = bounds;
	b = b.removeFromRight(b.getHeight());
	b.reduce(1,3);
	b.setY(b.getY() + 1);
	m_pArrowButton->setBounds(b);
	m_pArrowButton->addListener(this);
	addAndMakeVisible(m_pArrowButton.get());

	MakeExpandCollapseAllEventConnection();
}

void PortLevelDivider::parentHierarchyChanged()
{
	MakeExpandCollapseAllEventConnection();
}

void PortLevelDivider::MakeExpandCollapseAllEventConnection()
{
	Surface* pSurface = findParentComponentOfClass<Surface>();
	if (pSurface)
	{
		m_expandCollapseConnection = pSurface->ExpandCollapseEvent.Connect([this](bool isCollapse)
		{
			OnExpandCollapseAll(isCollapse);
		});
	}
}


void PortLevelDivider::AddListener(PortLevelDivider::Listener* listener)
{
	if (m_listeners.end() == std::find(m_listeners.begin(),m_listeners.end(),listener))
	{
		m_listeners.push_back(listener);
	}
}

void PortLevelDivider::RemoveListener(PortLevelDivider::Listener* listener)
{
	m_listeners.remove(listener);
}


void PortLevelDivider::Notify()
{
	for (auto listener : m_listeners)
	{
		listener->OnCollapseStateChanged();
	}
}

void PortLevelDivider::buttonClicked(Button* button)
{
	button;
	m_pArrowButton->FlipArrowDirection();
	m_isCollapsed = !m_isCollapsed;
	Notify();
}


void PortLevelDivider::OnExpandCollapseAll(bool isCollapse)
{
	if (m_isCollapsed != isCollapse)
	{
		m_pArrowButton->FlipArrowDirection();
	}

	m_isCollapsed = isCollapse;
	Notify();
}


void PortLevelDivider::paint(Graphics& g)
{
	Rectangle<int> lb = getLocalBounds();

	g.setColour(Colours::lightgrey);
	g.fillRect(lb);

	g.setFont(Font(13.0f));
	g.setColour(Colours::black);
	g.drawRect(lb,1);
	lb.setLeft(lb.getX() + 10);
	g.drawText(m_text,lb,Justification::left,false);
}

TitleControl::TitleControl(Preset* pPreset,ElementID elemID,const Rectangle<int>& bounds, std::string text)
	:m_text(std::move(text))
{
	setBounds(bounds);
	setInterceptsMouseClicks(false,true);

	int numInputPorts = pPreset->GetNumberOfPorts(elemID, PortType::Input);
	int numOutputPorts = pPreset->GetNumberOfPorts(elemID, PortType::Output);

	if (numInputPorts == 1)
	{
		m_pInputPortView = std::make_unique<PortView>(PortID(elemID,PortType::Input, 0));
		m_pInputPortView->setTopLeftPosition(Point<int>(0,
			lrint(bounds.getHeight() / 2 - m_pInputPortView->getHeight() / 2)));
		addAndMakeVisible(m_pInputPortView.get());
	}

	if (numOutputPorts == 1)
	{
		m_pOutputPortView = std::make_unique<PortView>(PortID(elemID,PortType::Output,0));
		m_pOutputPortView->setTopLeftPosition(Point<int>(
			lrint(bounds.getWidth() - m_pOutputPortView->getWidth()),
			lrint(bounds.getHeight() / 2 - m_pOutputPortView->getHeight() / 2)));
		addAndMakeVisible(m_pOutputPortView.get());
	}
}


void TitleControl::paint(Graphics& g)
{

	g.setFont(Font(16.0f));
	g.setColour(Colours::black);
	g.drawText(m_text,getLocalBounds(),Justification::centred,true);

	int portViewHeight = PortView::GetHeight();
	int portViewWidth = PortView::GetWidth();

	g.setFont(Font(9.5f));

	if (m_pOutputPortView)
	{
		Rectangle<float> outtextarea = getLocalBounds().toFloat();
		outtextarea = outtextarea.removeFromTop((getHeight() - portViewHeight) / 2.0f);
		outtextarea = outtextarea.removeFromRight((float)portViewWidth);
		g.drawText("out",outtextarea,Justification::centred,true);
	}

	if (m_pInputPortView)
	{
		Rectangle<float> intextarea = Rectangle<float>((float)portViewHeight,(getHeight() - portViewHeight) / 2.0f);
		g.drawText("in",intextarea,Justification::centred,true);
	}
}

LoopEGTitleControl::LoopEGTitleControl(Preset* pPreset,ElementID elemID,const Rectangle<int>& bounds,const std::string& text)
	:m_text(text)
{
	pPreset;
	elemID;
	setBounds(bounds);
	setInterceptsMouseClicks(false,true);

	ElementID elemLoopEGX = ElementID(ElementType::LoopEG,0);
	ElementID elemLoopEGY = ElementID(ElementType::LoopEG,0);
	m_pOutputPortViewX = std::make_unique<PortView>(PortID(elemLoopEGX,PortType::Output,0));
	m_pOutputPortViewY = std::make_unique<PortView>(PortID(elemLoopEGY,PortType::Output,1));


	int x = bounds.getWidth() - PortView::GetWidth();
	int space = bounds.getHeight() - 2 * PortView::GetHeight();
	m_pOutputPortViewX->setTopLeftPosition(x, space / 3);
	m_pOutputPortViewY->setTopLeftPosition(x,space * 2 / 3 + PortView::GetWidth());

	addAndMakeVisible(m_pOutputPortViewX.get());
	addAndMakeVisible(m_pOutputPortViewY.get());
}


void LoopEGTitleControl::paint(Graphics& g)
{

	g.setFont(Font(16.0f));
	g.setColour(Colours::black);
	g.drawText(m_text,getLocalBounds(),Justification::centred,true);


	g.setFont(Font(14.0f));

	Point<int> pos = m_pOutputPortViewX->getPosition();
	pos.addXY(-12,0);
	auto area = Rectangle<int>(pos.getX(),pos.getY(),12,PortView::GetHeight());
	g.drawText("X",area.toFloat(),Justification::centred,true);


	pos = m_pOutputPortViewY->getPosition();
	pos.addXY(-12,0);
	area = Rectangle<int>(pos.getX(),pos.getY(),12,PortView::GetHeight());
	g.drawText("Y",area.toFloat(),Justification::centred,true);

}

OscillatorTitleControl::OscillatorTitleControl(Preset* pPreset, ElementID elemID, const Rectangle<int>& bounds, std::string text)
	:m_elemID(elemID), m_text(std::move(text)), m_pPreset(pPreset)
{
	setBounds(bounds);
	setInterceptsMouseClicks(false, true);

	int numInputPorts = pPreset->GetNumberOfPorts(elemID, PortType::Input);
	int numOutputPorts = pPreset->GetNumberOfPorts(elemID, PortType::Output);

	if (numInputPorts == 1)
	{
		m_pInputPortView = std::make_unique<PortView>(PortID(elemID, PortType::Input, 0));
		m_pInputPortView->setTopLeftPosition(Point<int>(0,
			lrint(bounds.getHeight() / 2 - m_pInputPortView->getHeight() / 2)));
		addAndMakeVisible(m_pInputPortView.get());
	}

	if (numOutputPorts == 1)
	{
		m_pOutputPortView = std::make_unique<PortView>(PortID(elemID, PortType::Output, 0));
		m_pOutputPortView->setTopLeftPosition(Point<int>(
			lrint(bounds.getWidth() - m_pOutputPortView->getWidth()),
			lrint(bounds.getHeight() / 2 - m_pOutputPortView->getHeight() / 2)));
		addAndMakeVisible(m_pOutputPortView.get());
	}

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted)
	{
		value;
		isMuted;

		if (paramID.elemType == ElementType::Oscillator && paramID.portType == PortType::None && paramID.subIndex == oscType)
		{
			// When the osc type changes, we need to recheck the number of input ports because the input 
			// port is used as a sync input and only one osc type supports sync.
			UpdateInputPort();
			//repaint();
		}
	});
}

void OscillatorTitleControl::UpdateInputPort()
{
	int numInputPorts = m_pPreset->GetNumberOfPorts(m_elemID, PortType::Input);

	if (numInputPorts == 1 && m_pInputPortView == nullptr)
	{
		auto bnds = getBounds();
		m_pInputPortView = std::make_unique<PortView>(PortID(m_elemID, PortType::Input, 0));
		m_pInputPortView->setTopLeftPosition(Point<int>(0,
			lrint(bnds.getHeight() / 2 - m_pInputPortView->getHeight() / 2)));
		addAndMakeVisible(m_pInputPortView.get());
	}
	else if (numInputPorts == 0 && m_pInputPortView)
	{
		m_pInputPortView.reset();
	}
}


void OscillatorTitleControl::paint(Graphics& g)
{
	g.setFont(Font(16.0f));
	g.setColour(Colours::black);
	g.drawText(m_text, getLocalBounds(), Justification::centred, true);

	int portViewHeight = PortView::GetHeight();
	int portViewWidth = PortView::GetWidth();

	g.setFont(Font(9.5f));

	if (m_pOutputPortView)
	{
		Rectangle<float> outtextarea = getLocalBounds().toFloat();
		outtextarea = outtextarea.removeFromTop((getHeight() - portViewHeight) / 2.0f);
		outtextarea = outtextarea.removeFromRight((float)portViewWidth);
		g.drawText("out", outtextarea, Justification::centred, true);
	}

	Rectangle<float> intextarea = Rectangle<float>((float)portViewHeight, (getHeight() - portViewHeight) / 2.0f);
	if (m_pInputPortView)
	{
		g.drawText("sync" , intextarea, Justification::centred, true);
	}
}


FxSlotControl::FxSlotControl(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID, int slot, Rectangle<int> bounds)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
	,m_slot((uint8_t)slot)
{
	vector<string> effectNameList = m_pPreset->GetParameterListText(ParamID(m_elemID,m_slot));;
	for (size_t i = 0; i < effectNameList.size(); ++i)
	{
		m_effectsCombo.addItem(effectNameList[i],i + 1);
	}
	m_effectsCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, m_slot)) + 1, NotificationType::dontSendNotification);
	m_effectsCombo.addListener(this);
	addAndMakeVisible(&m_effectsCombo);

	assert(m_slot >= 0 && m_slot < 4);
	m_label = "Slot ";
	m_label += char(0x31 + slot);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			// Update the effect that currently in this slot.
			m_effectsCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, m_slot)) + 1, NotificationType::dontSendNotification);
		}
	});

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool)
	{
		// Update the effect that currently in this slot.
		m_effectsCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, m_slot)) + 1, NotificationType::dontSendNotification);
	});
	setBounds(bounds);
}

FxSlotControl::~FxSlotControl()
{
	m_effectsCombo.removeListener(this);
}

void FxSlotControl::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_effectsCombo)
	{
		int selectedID = m_effectsCombo.getSelectedId();
		assert(selectedID > 0);
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, m_slot), selectedID - 1));
	}
}

void FxSlotControl::resized()
{
	Rectangle<int> lb = getLocalBounds();

	m_textArea = lb;
	m_textArea.removeFromRight(lb.getWidth() / 2);

	Rectangle<int> cb = lb;
	cb.removeFromLeft(m_textArea.getWidth());
	m_effectsCombo.setBounds(cb);
}


void FxSlotControl::paint(Graphics& g)
{
	g.setColour(Colours::black);
	Rectangle<int> lb = getLocalBounds();
	g.drawLine(0,0,(float)lb.getWidth(),0,1);
	g.drawLine(0,lb.getHeight() + 0.5f,(float)lb.getWidth(),lb.getHeight() + 0.5f,1);
	//g.drawRect(getLocalBounds());

	g.setFont(Font(13.0f));
	g.setColour(Colours::black);

	g.drawText(m_label,m_textArea,Justification::centred,false);
}

void FxSlotControl::focusOfChildComponentChanged(FocusChangeType cause)
{
	cause;
	if (hasKeyboardFocus(true))
	{
		ElementView* pParent = dynamic_cast<ElementView*>(getParentComponent());
		assert(pParent);
		if (pParent)
		{
			pParent->SetFocusedSlot(m_slot);
		}

		// We also want to make this the selected element.
		ElementView* pOwningElementView = findParentComponentOfClass<ElementView>();
		if (pOwningElementView)
		{
			pOwningElementView->SelectThisView();
		}
	}
}


ControlPackage::ControlPackage(Preset* pPreset, CommandHistory* pCommandHistory,PortID portID, int width, int unitHeight)
{
	m_unitHeight = unitHeight;

	int numPorts = pPreset->GetNumberOfPorts(portID.elemID, portID.portType);

	int totalHeightUnits = numPorts + 1;	// +1 for divider
	setSize(width,totalHeightUnits * m_unitHeight);

	Rectangle<int> area(width,unitHeight);

	string dividerText = (portID.portType == PortType::Input) ? "Inputs" : (portID.portType == PortType::Mod) ? "Mod" : "Control";

	m_portLevelDivider = std::make_unique<PortLevelDivider>(area,dividerText);
	
	m_portLevelDivider->AddListener(this);

	addAndMakeVisible(m_portLevelDivider.get());
	area.setY(area.getY() + m_unitHeight);
	for (uint8_t i = 0; i < numPorts; ++i)
	{
		portID.portIndex = i;
		m_panels.push_back(std::make_unique<PortLevelControl>(pPreset, pCommandHistory, portID, area));
		addAndMakeVisible(m_panels.back().get());
		area.setY(area.getY() + m_unitHeight);
	}

	// A function to decide the sort order of control package rows, putting the 
	// rows At the top that have active connections. This is used when the 
	// element is collapsed and unused rows at the botton aren't shown.
	m_collapsedSort = [](const PortLevelControlUPtr& p1,const PortLevelControlUPtr& p2)
	{
		if (p1->HasConnection() && !p2->HasConnection())
		{
			return true;
		}
		else if (!p1->HasConnection() && p2->HasConnection())
		{
			return false;
		}
		else
		{
			return p1->GetPortID().portIndex < p2->GetPortID().portIndex;
		}
	};

	// A sort function that puts the rows in order of index, for when they are all shown.
	m_expandedSort = [](const PortLevelControlUPtr& p1,const PortLevelControlUPtr& p2)
	{
		return p1->GetPortID().portIndex < p2->GetPortID().portIndex;
	};
}

int ControlPackage::GetHeightInUnits()
{
	return getHeight() / m_unitHeight;
}

void ControlPackage::OnCollapseStateChanged()
{
	bool collapsed = m_portLevelDivider->IsStateCollapsed();
	
	// sort the port level controls depending on the collapsed state.
	// When collapsed, all connected ports are first followed by all unconnected ports.
	// When expanded, ports are sorted in order of index regardless of whether there are connections or not.
	int numPortsToShow = 0;
	if (collapsed)
	{
		m_panels.sort(m_collapsedSort);

		// Count the number of connected ports, that how many we want to show.
		numPortsToShow = std::count_if(m_panels.begin(),m_panels.end(),[](const PortLevelControlUPtr& p){ return p->HasConnection(); });
	}
	else
	{
		m_panels.sort(m_expandedSort);
		numPortsToShow = m_panels.size();
	}

	// Adjust positions according to sort order.
	Point<int> pos(0,m_unitHeight);
	for (auto& p : m_panels)
	{
		p->setTopLeftPosition(pos);
		pos.addXY(0,m_unitHeight);
	}

	setSize(getWidth(),(numPortsToShow + 1) * m_unitHeight);	// +1 accounts for the port level divider

	Notify();
}

void ControlPackage::AddListener(ControlPackage::Listener* listener)
{
	if (m_listeners.end() == std::find(m_listeners.begin(),m_listeners.end(),listener))
	{
		m_listeners.push_back(listener);
	}
}

void ControlPackage::RemoveListener(ControlPackage::Listener* listener)
{
	m_listeners.remove(listener);
}

void ControlPackage::UpdateFromModel()
{
	for(auto& pPortLevelControl : m_panels)
	{
		pPortLevelControl->UpdateFromModel();
	};
}


void ControlPackage::Notify()
{
	for (auto listener : m_listeners)
	{
		listener->OnCollapseStateChanged();
	}
}



ElementView::ElementView(Preset* pModel, CommandHistory* pCommandHistory, ViewModel* pViewModel, ElementID elemID, std::function<void(int, int, bool)> moveSelectedElementsBy)
			:m_pModel(pModel)
			,m_pViewModel(pViewModel)
			,m_elemID(elemID)
			,m_moveSelectedElementsBy(moveSelectedElementsBy)
{
	const int width = 208;
	const int height = 180;	// temporary height. Will be recalculated below.

	Location loc = m_pViewModel->GetLocation(m_elemID);
	setBounds(loc.x, loc.y, width, height);


	int numInputs = pModel->GetNumberOfPorts(elemID, PortType::Input);
	int numMods = pModel->GetNumberOfPorts(elemID, PortType::Mod);
	int numControls = pModel->GetNumberOfPorts(elemID, PortType::Control);
	int numOutputs = pModel->GetNumberOfPorts(elemID, PortType::Output);

	// We don't want the spaces between output ports to be less than a third of a unit.
	int titleHeightUnits = static_cast<int>(std::ceil(((numOutputs + 1) / 3.0)  + numOutputs));

	// If there is an input, mod, or control, then the height should be at least 2.
	if (numInputs + numMods + numControls > 0)
	{
		titleHeightUnits = std::max(2, titleHeightUnits);
	}

	int totalHeightUnits = titleHeightUnits;	// for title
	// if (elemID.elemType == ElementType::LoopEG)
	// {
	// 	totalHeightUnits = titleHeightUnits = 3;
	// 	totalHeightUnits += (numMods > 0) ? numMods + 1 : 0;
	// 
	// }
	if (elemID.elemType == ElementType::FXChannel)
	{
		totalHeightUnits += 4;	// For the four slots
	}
	else
	{
		totalHeightUnits += (numInputs > 1) ? numInputs + 1 : 0;
		totalHeightUnits += (numMods > 0) ? numMods + 1 : 0;
		totalHeightUnits += (numControls > 0) ? numControls + 1 : 0;
	}


	setSize(getWidth(),lrint((2 * GetBorderInset()) + (totalHeightUnits * m_unitHeight)));
	auto area = getLocalBounds();
	area.reduce(int(GetBorderInset()),int(GetBorderInset()));

	area.setHeight(titleHeightUnits * m_unitHeight);
	if (elemID.elemType == ElementType::LoopEG)
	{
		m_pTitleControlLoopEG = std::make_unique<LoopEGTitleControl>(pModel,elemID,area, m_pViewModel->GetElementFriendlyName(elemID));
		addAndMakeVisible(m_pTitleControlLoopEG.get());
	}
	else if (elemID.elemType == ElementType::Oscillator)
	{
		m_pTitleControlOscillator = std::make_unique<OscillatorTitleControl>(pModel, elemID, area, m_pViewModel->GetElementFriendlyName(elemID));
		addAndMakeVisible(m_pTitleControlOscillator.get());
	}
	else
	{
		m_pTitleControl = std::make_unique<TitleControl>(pModel,elemID,area, m_pViewModel->GetElementFriendlyName(elemID));
		addAndMakeVisible(m_pTitleControl.get());
	}
	area.setHeight(m_unitHeight);
	area.setY(area.getY() + titleHeightUnits * m_unitHeight);

	if (numInputs > 1)
	{
		auto cp = std::make_unique<ControlPackage>(pModel, pCommandHistory, PortID(elemID,PortType::Input,0),area.getWidth(),m_unitHeight);
		cp->setTopLeftPosition(area.getX(),area.getY());
		area.setY(area.getY() + (cp->GetHeightInUnits() * m_unitHeight));
		addAndMakeVisible(cp.get());
		cp->AddListener(this);
		m_controlPackages.push_back(std::move(cp));
	}
	if (numMods > 0)
	{
		auto cp = std::make_unique<ControlPackage>(pModel, pCommandHistory, PortID(elemID,PortType::Mod,0),area.getWidth(),m_unitHeight);
		cp->setTopLeftPosition(area.getX(),area.getY());
		area.setY(area.getY() + (cp->GetHeightInUnits() * m_unitHeight));
		addAndMakeVisible(cp.get());
		cp->AddListener(this);
		m_controlPackages.push_back(std::move(cp));
	}
	if (numControls > 0)
	{
		auto cp = std::make_unique<ControlPackage>(pModel, pCommandHistory, PortID(elemID,PortType::Control,0),area.getWidth(),m_unitHeight);
		cp->setTopLeftPosition(area.getX(),area.getY());
		area.setY(area.getY() + (cp->GetHeightInUnits() * m_unitHeight));
		addAndMakeVisible(cp.get());
		cp->AddListener(this);
		m_controlPackages.push_back(std::move(cp));
	}

	if (elemID.elemType == ElementType::FXChannel)
	{
		for (int i = 0; i < 4; ++i)
		{
			FxSlotControlUPtr pSlotControl = std::make_unique<FxSlotControl>(pModel, pCommandHistory, elemID,i,area);
			addAndMakeVisible(pSlotControl.get());
			m_panels.push_back(std::move(pSlotControl));
			area.setY(area.getY() + m_unitHeight);
		}
	}

}

ElementView::~ElementView()
{
	m_pTitleControl.reset();
	m_pTitleControlLoopEG.reset();
	m_pTitleControlOscillator.reset();
	m_panels.clear();
	m_controlPackages.clear();
}

void ElementView::OnCollapseStateChanged()
{
	UpdateSize();
}

void ElementView::UpdateSize()
{
	auto area = getLocalBounds();
	area.reduce(int(GetBorderInset()),int(GetBorderInset()));
	int totalHeightUnits = (m_elemID.elemType == ElementType::LoopEG) ? 3 : 2;	// for title
	
	Point<int> pos(area.getX(),area.getY() + totalHeightUnits * m_unitHeight);


	for (auto& cp : m_controlPackages)
	{
		cp->setTopLeftPosition(pos);
		totalHeightUnits += cp->GetHeightInUnits();
		pos.addXY(0,cp->GetHeightInUnits() * m_unitHeight);
	}
	setSize(getWidth(),(totalHeightUnits * m_unitHeight) + (2 * (int)GetBorderInset()));

}

void ElementView::resized()
{
	m_area = getLocalBounds().toFloat();
	m_shapeArea = m_area;
	m_shapeArea.reduce(GetBorderInset(),GetBorderInset());

	// We'd like to draw a rounded rectangle here but JUCE always starts drawing the path from the top left.
	// We want the path to start from the top middle. So instead we draw it using lines and then round the 
	// corners afterward.
	Path shapePath;
	shapePath.startNewSubPath(juce::Point<float>(m_area.getCentreX(),m_shapeArea.getY()));
	shapePath.lineTo(m_shapeArea.getTopRight());
	shapePath.lineTo(m_shapeArea.getBottomRight());
	shapePath.lineTo(m_shapeArea.getBottomLeft());
	shapePath.lineTo(m_shapeArea.getTopLeft());
	shapePath.closeSubPath();
	m_shapePath = shapePath.createPathWithRoundedCorners(m_cornerRadius);
}

void ElementView::paint(Graphics& g)
{

	if (m_pViewModel->ElementViewSelectionModel().IsSelected(m_elemID))
	{
		//g.setColour(Colour(0x80D0E6FF));
		g.setColour(Colour(0x80FFA0A0));
		g.fillRoundedRectangle(m_area,5);
	}

	ColourGradient grad(
		Colours::white,float(getWidth() / 2),GetBorderInset(),
		Colour(0xB06FB9FF),float(getWidth() / 2),getHeight() - (GetBorderInset() / 2),false);

	g.setGradientFill(grad);
	g.fillRect(m_shapeArea);

	g.setColour(Colour(0xff808080));
	g.strokePath(m_shapePath,PathStrokeType(2.0f));

}

float ElementView::GetBorderInset()
{
	return 10.0f;
}

void ElementView::SelectThisView(bool append)
{
	if (append)
	{
		m_pViewModel->ElementViewSelectionModel().SelectAppend(m_elemID);
	}
	else
	{
		m_pViewModel->ElementViewSelectionModel().Select(m_elemID);
	}
}

void ElementView::mouseDown(const MouseEvent& event)
{
	//DBG("Mouse down on Node.");
	bool append = event.mods.isCommandDown();	// selections are appended if CTRL/Apple key is down.
	if (append)
	{
		m_pViewModel->ElementViewSelectionModel().ToggleSelection(m_elemID);	// does not clear other selected elements
	}
	else
	{
		m_pViewModel->ElementViewSelectionModel().Select(m_elemID);		// clears any other selected elements
	}

	if (event.mods.isPopupMenu())	// for Windows this is right-click
	{
		// For now, no context menu for Home And System/Midi. But in future versions those elements will not be 
		// needed and this if statement can be removed.
		if (false == m_pViewModel->ElementViewSelectionModel().IsSelected(ElementID(ElementType::Home, 0)))
		{

			PopupMenu mainMenu;

			mainMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditMute, "Toggle Mute");
			mainMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditRemove, "Remove");
			mainMenu.show(0, 0, 0, 0, nullptr);

			// Since there is no async handler given, the result returned from show() is not zero but is the ID of the command chosen;

		}
	}

	toFront(false);
}

/** @brief Move the ElementView to a location which is a specified x/y distance from its saved location.

	This doesn't move an ElementView permanently...that is, it doesn't save the new location.
*/
void ElementView::MoveBy(int x,int y)
{
	auto location = m_pViewModel->GetLocation(m_elemID);
	int xPos = location.x;
	int yPos = location.y;

	// Limit position top and left
	int newXPos = xPos + x;
	int newYPos = yPos + y;
	if (newXPos < 0) newXPos = 0;
	if (newYPos < 0) newYPos = 0;

	// Limit position right and bottom
	int maxXPos = getParentComponent()->getWidth() - getWidth();
	int maxYPos = getParentComponent()->getHeight() - getHeight();
	if (newXPos > maxXPos) newXPos = maxXPos;
	if (newYPos > maxYPos) newYPos = maxYPos;

	setTopLeftPosition(newXPos,newYPos);
}

void ElementView::mouseDrag(const MouseEvent& event)
{
	//DBG("Mouse drag on Node.");

	// If the mouse down resulted in this element being deselected (by holding the Command key) we don't 
	// want to move the selected elements but rather the one that got clicked on.
	if (!m_pViewModel->ElementViewSelectionModel().IsSelected(m_elemID))
	{
		MoveBy(event.getDistanceFromDragStartX(), event.getDistanceFromDragStartY());
	}
	else
	{
		m_moveSelectedElementsBy(event.getDistanceFromDragStartX(), event.getDistanceFromDragStartY(), false);
	}
	m_isDragging = true;
}

void ElementView::mouseUp(const MouseEvent& event)
{

	if (m_isDragging)
	{
		// If the mouse down resulted in this element being deselected (by holding the Command key) we don't 
		// want to move the selected elements but rather the one that got clicked on.
		if (!m_pViewModel->ElementViewSelectionModel().IsSelected(m_elemID))
		{
			MoveBy(event.getDistanceFromDragStartX(), event.getDistanceFromDragStartY());
			GetViewModel()->SetLocation(m_elemID, Location{ gsl::narrow<int16_t>(getX()), gsl::narrow<int16_t>(getY()) });
		}
		else
		{
			m_moveSelectedElementsBy(event.getDistanceFromDragStartX(), event.getDistanceFromDragStartY(), true);
		}

		m_isDragging = false;
	}
}

void ElementView::SetFocusedSlot(int focusedSlot)
{
	m_pViewModel->SetFocusedSlot(m_elemID.elemIndex, focusedSlot);
}

void ElementView::UpdateFromModel()
{
	for(auto& pControlPackage : m_controlPackages)
	{
		pControlPackage->UpdateFromModel(); 
	};
}
