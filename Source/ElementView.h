/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef ELEMENTVIEW_H_INCLUDED
#define ELEMENTVIEW_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include <string>
#include <list>
#include <cassert>
#include "Preset.h"
#include "ViewModel.h"
#include "PortView.h"
#include "Notifier.h"
#include "Slider2.h"
class CommandHistory;

//==============================================================================
/**
	A button with an arrow in it.

	@see Button
*/
class JUCE_API  ArrowButton2 : public Button
{
public:
	//==============================================================================
	/** Creates an ArrowButton.

	@param buttonName       the name to give the button
	@param arrowDirection   the direction the arrow should point in, where 0.0 is
	pointing right, 0.25 is down, 0.5 is left, 0.75 is up
	@param arrowColour      the colour to use for the arrow
	*/
	ArrowButton2(const String& buttonName,
		float arrowDirection,
		Colour arrowColour);

	/** Destructor. */
	~ArrowButton2();

	/** @internal */
	void paintButton(Graphics&,bool isMouseOverButton,bool isButtonDown) override;


	void FlipArrowDirection();
private:
	Colour colour;
	Path path;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ArrowButton2)
};

class SliderLevel : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
	void SetDivisor(uint_fast16_t divisor);
	void SetPrecision(uint_fast8_t precision);
private:
	uint_fast16_t m_divisor{ divisor1 };
	uint_fast8_t m_precision{ 0 };
};

class PortLevelControl : public Component,
	public ComboBox::Listener,
	public Slider2::Listener
{
public:
	PortLevelControl(Preset* pPreset, CommandHistory* pCommandHistory, PortID portID,Rectangle<int> bounds);
	~PortLevelControl();
	void resized() override;
	void paint(Graphics& g) override;
	void SetSliderColor(const Colour& color);
	void comboBoxChanged(ComboBox* comboBoxThatHasChanged) override;
	void sliderValueChanged(Slider2* slider) override;
	bool HasConnection() { return (m_portView) ? m_portView->HasConnection() : false; }
	PortID GetPortID() { return m_portID; }
	void OnParameterChanged(const ParamID& paramID, int16_t value);
	void UpdateFromModel();
private:

	void UpdateSliderFromModel();
	void UpdateDestCombo(int value);
	void UpdateDestComboFromModel();
	void PopulateDestCombo();
	int GetLevelType();

	SliderLevel		m_slider;
	std::string		m_destinationName;
	PortViewUPtr	m_portView;			///< Location where connections are made
	std::string		m_label;
	Rectangle<int>	m_textArea;
	ComboBox		m_modDestination;

	PortID			m_portID;
	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;

	Preset::ParameterChangedConnection m_parameterChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};
using PortLevelControlUPtr = std::unique_ptr < PortLevelControl >;


class PortLevelDivider :	public Component,
							public Button::Listener
{
public:

	class Listener
	{
	public:
		virtual void OnCollapseStateChanged() = 0;
	};

	PortLevelDivider(const Rectangle<int>& bounds,const std::string& text);
	void paint(Graphics& g) override;
	void buttonClicked(Button* button) override;
	bool IsStateCollapsed() { return m_isCollapsed; };
	void AddListener(PortLevelDivider::Listener* listener);
	void RemoveListener(PortLevelDivider::Listener* listener);
	void parentHierarchyChanged() override;

private:
	void MakeExpandCollapseAllEventConnection();
	void OnExpandCollapseAll(bool isCollapse);
	void Notify();
	std::string		m_text;
	bool			m_isCollapsed{ false };
	std::unique_ptr<ArrowButton2>			m_pArrowButton;
	std::list<Listener*>	m_listeners;

	Notifier<void(bool)>::ConnectionType m_expandCollapseConnection;
};
using PortLevelDividerUPtr = std::unique_ptr < PortLevelDivider >;


class TitleControl : public Component
{
public:
	TitleControl(Preset* pPreset,ElementID elemID, const Rectangle<int>& bounds, std::string text);
	void paint(Graphics& g) override;

private:
	std::string		m_text;
	PortViewUPtr	m_pOutputPortView;			///< Output PortView
	PortViewUPtr	m_pInputPortView;			///< Input PortView
	Preset*			m_pPreset;

};
using TitleControlUPtr = std::unique_ptr < TitleControl >;


class LoopEGTitleControl : public Component
{
public:
	LoopEGTitleControl(Preset* pPreset,ElementID elemID,const Rectangle<int>& bounds,const std::string& text);
	void paint(Graphics& g) override;

private:
	std::string		m_text;
	PortViewUPtr	m_pOutputPortViewX;			///< Output PortViewX
	PortViewUPtr	m_pOutputPortViewY;			///< Output PortViewY
	Preset*			m_pPreset;

};
using LoopEGTitleControlUPtr = std::unique_ptr < LoopEGTitleControl >;

class OscillatorTitleControl : public Component
{
public:
	OscillatorTitleControl(Preset* pPreset, ElementID elemID, const Rectangle<int>& bounds, std::string text);
	void paint(Graphics& g) override;

private:
	void UpdateInputPort();

	std::string		m_text;
	PortViewUPtr	m_pOutputPortView;			///< Output PortView
	PortViewUPtr	m_pInputPortView;			///< Input PortView
	Preset*			m_pPreset;
	ElementID		m_elemID;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};
using OscillatorTitleControlUPtr = std::unique_ptr < OscillatorTitleControl >;


class FxSlotControl : public Component,
	public ComboBox::Listener
{
public:
	FxSlotControl(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID,int slot, Rectangle<int> bounds);
	~FxSlotControl();
	void resized() override;
	void paint(Graphics& g) override;
	void comboBoxChanged(ComboBox* comboBoxThatHasChanged) override;
	virtual void focusOfChildComponentChanged(FocusChangeType cause) override;

private:
	std::string		m_destinationName;
	ElementID		m_elemID;
	uint8_t			m_slot;
	std::string		m_label;
	Rectangle<int>	m_textArea;
	ComboBox		m_effectsCombo;

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	Preset::ParameterChangedConnection				m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection		m_multipleParametersChangedConnection;
};
using FxSlotControlUPtr = std::unique_ptr < FxSlotControl >;

class ControlPackage : public Component,
					public PortLevelDivider::Listener
{
public:
	ControlPackage(Preset* pPreset, CommandHistory* pCommandHistory,PortID portID,int width,int unitHeight = 18);
	int GetHeightInUnits();
	void OnCollapseStateChanged() override;
	class Listener
	{
		public:
		virtual void OnCollapseStateChanged() = 0;
	};
	void AddListener(ControlPackage::Listener* listener);
	void RemoveListener(ControlPackage::Listener* listener);

	void UpdateFromModel();

private:
	void Notify();

	int		m_unitHeight;
	PortLevelDividerUPtr	m_portLevelDivider;
	std::list<PortLevelControlUPtr>	m_panels;
	std::list<Listener*>	m_listeners;

	using PortControlSort = std::function < bool(const PortLevelControlUPtr& p1,const PortLevelControlUPtr& p2) > ;
	PortControlSort m_collapsedSort;
	PortControlSort m_expandedSort;

};
using ControlPackageUPtr = std::unique_ptr < ControlPackage >;


class ElementView : public Component,
					public ControlPackage::Listener
{
public:
	ElementView(Preset* pModel, CommandHistory* pCommandHistory, ViewModel* pViewModel, ElementID elemID, std::function<void(int,int, bool)> moveSelectedElementsBy );
	virtual ~ElementView();

	void resized() override;
	void paint(Graphics& g) override;
	void mouseDown(const MouseEvent& event) override;
	void mouseDrag(const MouseEvent& event) override;
	void mouseUp(const MouseEvent& event) override;
	void MoveBy(int x,int y);

	virtual float GetBorderInset();

	void SelectThisView(bool append = false);

	virtual ElementID GetElementID()	{ return m_elemID; }
	virtual ViewModel* GetViewModel() { return m_pViewModel; }
	virtual Preset* GetModel()	{ return m_pModel; }

	void SetFocusedSlot(int focusedSlot);
	void OnCollapseStateChanged() override;
	void UpdateFromModel();

	Notifier<void(int slotIndex)> FxFocusedSlotChangedEvent;
	using FxFocusedSlotChangedConnection = decltype(FxFocusedSlotChangedEvent)::ConnectionType;	///< FxFocusedSlotChangedEvent.Connect() returns this type

private:
	void UpdateSize();

	const int						m_unitHeight{ 18 };
	bool							m_isDragging{ false };	///< Will be set to true when this ElementView is in the process of being dragged.
	ElementID						m_elemID;
	Preset*							m_pModel;
	ViewModel*						m_pViewModel;

	Rectangle<float>				m_area;
	Rectangle<float>				m_shapeArea;
	Path							m_shapePath;
	const float						m_cornerRadius = 0.0f;

	TitleControlUPtr				m_pTitleControl;
	LoopEGTitleControlUPtr			m_pTitleControlLoopEG;
	OscillatorTitleControlUPtr		m_pTitleControlOscillator;
	std::vector<std::unique_ptr<Component>>	m_panels;
	std::vector<ControlPackageUPtr>	m_controlPackages;
	std::function<void(int, int, bool)>	m_moveSelectedElementsBy;
};
using ElementViewUPtr = std::unique_ptr < ElementView >;


#endif  // ELEMENTVIEW_H_INCLUDED
