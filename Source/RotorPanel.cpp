/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <math.h>
#include <cassert>
#include <vector>
#include <string>
#include "RotorPanel.h"
#include "Preset.h"
#include "Command.h"

using std::vector;
using std::string;

static const juce::Identifier paramIndexID = "paramIndex";

RotorPanel::RotorPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
{

	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_coarseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_coarseLabel.setText("Coarse:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_coarseLabel);
	m_coarseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_coarseSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,rotCoarse));
	m_coarseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_coarseSlider.getProperties().set(paramIndexID, rotCoarse);
	m_coarseSlider.addListener(this);
	addAndMakeVisible(m_coarseSlider);

	m_coarseSyncCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	vector<string> rotCoarseSyncList = m_pPreset->GetParameterListText(ParamID(elemID, rotCoarseSync));;
	for (size_t i = 0; i < rotCoarseSyncList.size(); ++i)
	{
		m_coarseSyncCombo.addItem(rotCoarseSyncList[i], i + 1);
	}
	m_coarseSyncCombo.addListener(this);
	addAndMakeVisible(m_coarseSyncCombo);

	m_coarseNoTrackSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_coarseNoTrackSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, rotCoarseNoTrack));
	m_coarseNoTrackSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_coarseNoTrackSlider.getProperties().set(paramIndexID, rotCoarseNoTrack);
	m_coarseNoTrackSlider.addListener(this);
	addAndMakeVisible(m_coarseNoTrackSlider);

	yPos += rowHeight;
	m_fineLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fineLabel.setText("Fine:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fineLabel);
	m_fineSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fineSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,rotFine));
	m_fineSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fineSlider.getProperties().set(paramIndexID, rotFine);
	m_fineSlider.addListener(this);
	addAndMakeVisible(m_fineSlider);

	yPos += rowHeight;
	m_xFadeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_xFadeLabel.setText("xFade:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_xFadeLabel);
	m_xFadeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_xFadeSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,rotXFade));
	m_xFadeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_xFadeSlider.getProperties().set(paramIndexID, rotXFade);
	m_xFadeSlider.addListener(this);
	addAndMakeVisible(m_xFadeSlider);

	yPos += rowHeight;
	m_phaseSyncLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseSyncLabel.setText("Sync:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseSyncLabel);
	m_phaseSyncCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> phaseSyncList = m_pPreset->GetParameterListText(ParamID(elemID,rotPhaseSync));
	for (size_t i = 0; i < phaseSyncList.size(); ++i)
	{
		m_phaseSyncCombo.addItem(phaseSyncList[i],i + 1);
	}
	m_phaseSyncCombo.addListener(this);
	addAndMakeVisible(m_phaseSyncCombo);

	yPos += rowHeight;
	m_phaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseLabel.setText("Phase:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseLabel);
	m_phaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_phaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,rotPhase));
	m_phaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_phaseSlider.setTextValueSuffix(" deg");
	m_phaseSlider.getProperties().set(paramIndexID, rotPhase);
	m_phaseSlider.addListener(this);
	addAndMakeVisible(m_phaseSlider);

	yPos += rowHeight;
	m_clockSyncButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_clockSyncButton.setButtonText("Clock Sync");
	m_clockSyncButton.addListener(this);
	addAndMakeVisible(m_clockSyncButton);

	yPos += rowHeight;
	m_noTrackButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_noTrackButton.setButtonText("No Track");
	m_noTrackButton.addListener(this);
	addAndMakeVisible(m_noTrackButton);

	yPos += rowHeight;
	m_lowButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_lowButton.setButtonText("Low");
	m_lowButton.addListener(this);
	addAndMakeVisible(m_lowButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}


RotorPanel::~RotorPanel()
{
	m_phaseSyncCombo.removeListener(this);
	m_coarseSlider.removeListener(this);
	m_coarseSyncCombo.removeListener(this);
	m_coarseNoTrackSlider.removeListener(this);
	m_fineSlider.removeListener(this);
	m_phaseSlider.removeListener(this);
	m_clockSyncButton.removeListener(this);
	m_noTrackButton.removeListener(this);
	m_lowButton.removeListener(this);
}

void RotorPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void RotorPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_phaseSyncCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, rotPhaseSync), m_phaseSyncCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_coarseSyncCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, rotCoarseSync), m_coarseSyncCombo.getSelectedId() - 1));
	}
}

void RotorPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_coarseSlider) ||
		(slider == &m_coarseNoTrackSlider) ||
		(slider == &m_fineSlider) ||
		(slider == &m_xFadeSlider) ||
		(slider == &m_phaseSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}


// This function gets called after the button has been clicked and the state is changed.
void RotorPanel::buttonClicked(Button* button)
{
	if (button == &m_clockSyncButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, rotClockSync), m_clockSyncButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_noTrackButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, rotNoTrack), m_noTrackButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_lowButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, rotLow), m_lowButton.getToggleState() ? 1 : 0));
	}

	// Update needs to be called here because the button state may need to be reversed as a result of 
	// what the Preset class determines. For example, if you turn on the Low switch and then send that 
	// command to Preset, the change will be ignored if MidiClockSync is on. So you need the botton 
	// to be turned back off. The call to Update is an easy way to do it.
	Update();
}

void RotorPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_phaseSyncCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,rotPhaseSync)) + 1, NotificationType::dontSendNotification);
	m_coarseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, rotCoarse)), NotificationType::dontSendNotification);
	m_coarseSyncCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, rotCoarseSync)) + 1, NotificationType::dontSendNotification);
	m_coarseNoTrackSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,rotCoarseNoTrack)), NotificationType::dontSendNotification);
	m_fineSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,rotFine)), NotificationType::dontSendNotification);
	m_phaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,rotPhase)), NotificationType::dontSendNotification);
	m_xFadeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,rotXFade)), NotificationType::dontSendNotification);

	bool clockSyncOn = m_pPreset->GetParameterValue(ParamID(m_elemID, rotClockSync)) != 0;
	bool noTrackOn = m_pPreset->GetParameterValue(ParamID(m_elemID, rotNoTrack)) != 0;
	bool lowOn = m_pPreset->GetParameterValue(ParamID(m_elemID, rotLow)) != 0;
	m_clockSyncButton.setToggleState(clockSyncOn,NotificationType::dontSendNotification);
	m_noTrackButton.setToggleState(noTrackOn && !clockSyncOn,NotificationType::dontSendNotification);
	m_lowButton.setToggleState(lowOn,NotificationType::dontSendNotification);

	m_coarseSyncCombo.setVisible(clockSyncOn);
	m_coarseNoTrackSlider.setVisible(!clockSyncOn && noTrackOn);
	m_coarseSlider.setVisible(!clockSyncOn && !noTrackOn);
}

