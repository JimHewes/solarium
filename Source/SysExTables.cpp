/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "SysExTables.h"
#include "ParameterList.h"


uint8_t sysByteCount[SYS_MIDI_BLOCK_SIZE] =
{
	1, 2, 0, 1, 1, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1
};

ParamID sysParamID[SYS_MIDI_BLOCK_SIZE] =
{
	{ ElementType::System,	0, PortType::None, 0, sysTranspose },
	{ ElementType::System,	0, PortType::None, 0, sysFineTune },
	{ ElementType::System,	0, PortType::None, 0, sysFineTune },
	{ ElementType::System,	0, PortType::None, 0, sysBPMOverride },
	{ ElementType::System,	0, PortType::None, 0, sysOutputsOverride },
	{ ElementType::System,	0, PortType::None, 0, sysExpPedalPolarity },
	{ ElementType::System,	0, PortType::None, 0, sysSusPedal1Polarity },
	{ ElementType::System,	0, PortType::None, 0, sysSusPedal2Polarity },
	{ ElementType::System,	0, PortType::None, 0, sysExpPedalTargetOverride },
	{ ElementType::System,	0, PortType::None, 0, sysSusPedalTargetOverride },
	{ ElementType::System,	0, PortType::None, 0, sysRndTune },
	{ ElementType::System,	0, PortType::None, 0, sysRndTune },
	{ ElementType::System,	0, PortType::None, 0, sysVTIntens },
	{ ElementType::System,	0, PortType::None, 0, sysVTIntens },
	{ ElementType::System,	0, PortType::None, 0, sysVTOffset },
	{ ElementType::System,	0, PortType::None, 0, sysVTOffset },
	{ ElementType::System,	0, PortType::None, 0, sysATIntens },
	{ ElementType::System,	0, PortType::None, 0, sysATIntens },
	{ ElementType::System,	0, PortType::None, 0, sysATOffset },
	{ ElementType::System,	0, PortType::None, 0, sysATOffset },
	{ ElementType::System,	0, PortType::None, 0, sysLoadSample },
	{ ElementType::System,	0, PortType::None, 0, sysMidiChannel },
	{ ElementType::System,	0, PortType::None, 0, sysProgramChangeAllowed },
	{ ElementType::System,	0, PortType::None, 0, sysSendArp },
	{ ElementType::System,	0, PortType::None, 0, sysOmni },
	{ ElementType::System,	0, PortType::None, 0, sysLocal },
	{ ElementType::System,	0, PortType::None, 0, sysTransmitSysEx },
	{ ElementType::System,	0, PortType::None, 0, sysMidiClockSource },
	{ ElementType::System,	0, PortType::None, 0, sysMidiVolumeAllowed },
	{ ElementType::System,	0, PortType::None, 0, sysMidiRealTime },
	{ ElementType::System,	0, PortType::None, 0, sysPolychain },
	{ ElementType::System,	0, PortType::None, 0, sysDeviceID },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC1 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC1 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC2 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC2 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC3 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC3 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC4 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC4 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC5 },
	{ ElementType::System,	0, PortType::None, 0, sysMidiCC5 },
	{ ElementType::System,	0, PortType::None, 0, sysRemapAT }
};

uint8_t oscByteCount[OSC_MIDI_BLOCK_SIZE] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 3, 0, 0, 2, 0, 2, 0, 1, 1, 3, 0, 0, 1, 1, 1, 0, 0, 0,    2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 2, 0,
};

ParamID oscParamID[4][OSC_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::Oscillator,	0, PortType::None, 0, oscType },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscMM1Wave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscWTWave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCEMWave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscWAVWave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscVSWave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscMiniWave },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCoarse },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCoarseSync },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCoarseNoTrack }, // MSB
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCoarseNoTrack },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscCoarseNoTrack }, // LSB
		{ ElementType::Oscillator,	0, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	0, PortType::Input, 0, psiSource },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscGlide },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscGlideRate }, // MSB
		{ ElementType::Oscillator,	0, PortType::None, 0, oscGlideRate },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscGlideRate }, // LSB
		{ ElementType::Oscillator,	0, PortType::None, 0, oscClockSync },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscNoTrack },
		{ ElementType::Oscillator,	0, PortType::None, 0, oscLow },
		{ ElementType::Off,			0, PortType::None, 0, 0 },
		{ ElementType::Off,			0, PortType::None, 0, 0 },
		{ ElementType::Off,			0, PortType::None, 0, 0 },

		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	0, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiDest },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmtShape },
		{ ElementType::Oscillator,	0, PortType::Mod,		0, psiAmtShape },

		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	1, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiDest },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmtShape },
		{ ElementType::Oscillator,	0, PortType::Mod,		1, psiAmtShape },

		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	2, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiDest },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmtShape },
		{ ElementType::Oscillator,	0, PortType::Mod,		2, psiAmtShape },

		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	3, psiSource },
		{ ElementType::Oscillator,	0, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiDest },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmount },		// LinFM
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmtShape },
		{ ElementType::Oscillator,	0, PortType::Mod,		3, psiAmtShape }
	},
	{
		{ ElementType::Oscillator,	1, PortType::None, 0, oscType },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscMM1Wave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscWTWave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCEMWave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscWAVWave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscVSWave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscMiniWave },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCoarse },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCoarseSync },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCoarseNoTrack }, // MSB
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCoarseNoTrack },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscCoarseNoTrack }, // LSB
		{ ElementType::Oscillator,	1, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	1, PortType::Input, 0, psiSource },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscGlide },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscGlideRate }, // MSB
		{ ElementType::Oscillator,	1, PortType::None, 0, oscGlideRate },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscGlideRate }, // LSB
		{ ElementType::Oscillator,	1, PortType::None, 0, oscClockSync },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscNoTrack },
		{ ElementType::Oscillator,	1, PortType::None, 0, oscLow },
		{ ElementType::Off,			1, PortType::None, 0, 0 },
		{ ElementType::Off,			1, PortType::None, 0, 0 },
		{ ElementType::Off,			1, PortType::None, 0, 0 },

		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	0, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiDest },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmtShape },
		{ ElementType::Oscillator,	1, PortType::Mod,		0, psiAmtShape },

		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	1, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiDest },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmtShape },
		{ ElementType::Oscillator,	1, PortType::Mod,		1, psiAmtShape },

		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	2, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiDest },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmtShape },
		{ ElementType::Oscillator,	1, PortType::Mod,		2, psiAmtShape },

		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	3, psiSource },
		{ ElementType::Oscillator,	1, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiDest },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmtShape },
		{ ElementType::Oscillator,	1, PortType::Mod,		3, psiAmtShape }
	},
	{
		{ ElementType::Oscillator,	2, PortType::None, 0, oscType },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscMM1Wave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscWTWave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCEMWave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscWAVWave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscVSWave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscMiniWave },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCoarse },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCoarseSync },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCoarseNoTrack }, // MSB
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCoarseNoTrack },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscCoarseNoTrack }, // LSB
		{ ElementType::Oscillator,	2, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	2, PortType::Input, 0, psiSource },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscGlide },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscGlideRate }, // MSB
		{ ElementType::Oscillator,	2, PortType::None, 0, oscGlideRate },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscGlideRate }, // LSB
		{ ElementType::Oscillator,	2, PortType::None, 0, oscClockSync },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscNoTrack },
		{ ElementType::Oscillator,	2, PortType::None, 0, oscLow },
		{ ElementType::Off,			2, PortType::None, 0, 0 },
		{ ElementType::Off,			2, PortType::None, 0, 0 },
		{ ElementType::Off,			2, PortType::None, 0, 0 },

		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	0, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiDest },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmtShape },
		{ ElementType::Oscillator,	2, PortType::Mod,		0, psiAmtShape },

		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	1, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiDest },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmtShape },
		{ ElementType::Oscillator,	2, PortType::Mod,		1, psiAmtShape },

		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	2, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiDest },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmtShape },
		{ ElementType::Oscillator,	2, PortType::Mod,		2, psiAmtShape },

		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	3, psiSource },
		{ ElementType::Oscillator,	2, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiDest },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmtShape },
		{ ElementType::Oscillator,	2, PortType::Mod,		3, psiAmtShape }
	},
	{
		{ ElementType::Oscillator,	3, PortType::None, 0, oscType },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscMM1Wave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscWTWave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCEMWave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscWAVWave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscVSWave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscMiniWave },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCoarse },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscFine },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCoarseSync },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCoarseNoTrack }, // MSB
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCoarseNoTrack },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscCoarseNoTrack }, // LSB
		{ ElementType::Oscillator,	3, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscShape },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscPhase },
		{ ElementType::Oscillator,	3, PortType::Input, 0, psiSource },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscGlide },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscGlideRate }, // MSB
		{ ElementType::Oscillator,	3, PortType::None, 0, oscGlideRate },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscGlideRate }, // LSB
		{ ElementType::Oscillator,	3, PortType::None, 0, oscClockSync },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscNoTrack },
		{ ElementType::Oscillator,	3, PortType::None, 0, oscLow },
		{ ElementType::Off,			3, PortType::None, 0, 0 },
		{ ElementType::Off,			3, PortType::None, 0, 0 },
		{ ElementType::Off,			3, PortType::None, 0, 0 },

		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	0, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Control,	0, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiDest },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmtShape },
		{ ElementType::Oscillator,	3, PortType::Mod,		0, psiAmtShape },

		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	1, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Control,	1, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiDest },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmtShape },
		{ ElementType::Oscillator,	3, PortType::Mod,		1, psiAmtShape },

		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	2, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Control,	2, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiDest },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmtShape },
		{ ElementType::Oscillator,	3, PortType::Mod,		2, psiAmtShape },

		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	3, psiSource },
		{ ElementType::Oscillator,	3, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Control,	3, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiDest },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmount },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmtShape },
		{ ElementType::Oscillator,	3, PortType::Mod,		3, psiAmtShape }
	},
};

uint8_t rotByteCount[ROT_MIDI_BLOCK_SIZE] =
{
	2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 1, 2, 0, 1, 3, 0, 0, 2, 0, 1, 2, 0, 1, 1, 1,    2, 0, 1, 2, 0, 1, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0,   2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
};

ParamID rotParamID[2][ROT_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::Rotor,	0, PortType::Input, 0, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 0, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 0, psiAmount },
		{ ElementType::Rotor,	0, PortType::Input, 1, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 1, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 1, psiAmount },
		{ ElementType::Rotor,	0, PortType::Input, 2, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 2, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 2, psiAmount },
		{ ElementType::Rotor,	0, PortType::Input, 3, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 3, psiSource },
		{ ElementType::Rotor,	0, PortType::Input, 3, psiAmount },
		{ ElementType::Rotor,	0, PortType::None, 0, rotCoarse },
		{ ElementType::Rotor,	0, PortType::None, 0, rotFine },
		{ ElementType::Rotor,	0, PortType::None, 0, rotFine },
		{ ElementType::Rotor,	0, PortType::None, 0, rotCoarseSync },
		{ ElementType::Rotor,	0, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	0, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	0, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	0, PortType::None, 0, rotXFade },
		{ ElementType::Rotor,	0, PortType::None, 0, rotXFade },
		{ ElementType::Rotor,	0, PortType::None, 0, rotPhaseSync },
		{ ElementType::Rotor,	0, PortType::None, 0, rotPhase },
		{ ElementType::Rotor,	0, PortType::None, 0, rotPhase },
		{ ElementType::Rotor,	0, PortType::None, 0, rotClockSync },
		{ ElementType::Rotor,	0, PortType::None, 0, rotNoTrack },
		{ ElementType::Rotor,	0, PortType::None, 0, rotLow },

		{ ElementType::Rotor,	0, PortType::Mod,		0, psiSource },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	0, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	0, psiAmount },
		{ ElementType::Rotor,	0, PortType::Control,	0, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiDest },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		0, psiAmount },

		{ ElementType::Rotor,	0, PortType::Mod,		1, psiSource },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	1, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	1, psiAmount },
		{ ElementType::Rotor,	0, PortType::Control,	1, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiDest },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		1, psiAmount },

		{ ElementType::Rotor,	0, PortType::Mod,		2, psiSource },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	2, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	2, psiAmount },
		{ ElementType::Rotor,	0, PortType::Control,	2, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiDest },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		2, psiAmount },

		{ ElementType::Rotor,	0, PortType::Mod,		3, psiSource },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	3, psiSource },
		{ ElementType::Rotor,	0, PortType::Control,	3, psiAmount },
		{ ElementType::Rotor,	0, PortType::Control,	3, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiDest },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiAmount },
		{ ElementType::Rotor,	0, PortType::Mod,		3, psiAmount },
	},
	{
		{ ElementType::Rotor,	1, PortType::Input, 0, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 0, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 0, psiAmount },
		{ ElementType::Rotor,	1, PortType::Input, 1, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 1, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 1, psiAmount },
		{ ElementType::Rotor,	1, PortType::Input, 2, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 2, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 2, psiAmount },
		{ ElementType::Rotor,	1, PortType::Input, 3, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 3, psiSource },
		{ ElementType::Rotor,	1, PortType::Input, 3, psiAmount },
		{ ElementType::Rotor,	1, PortType::None, 0, rotCoarse },
		{ ElementType::Rotor,	1, PortType::None, 0, rotFine },
		{ ElementType::Rotor,	1, PortType::None, 0, rotFine },
		{ ElementType::Rotor,	1, PortType::None, 0, rotCoarseSync },
		{ ElementType::Rotor,	1, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	1, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	1, PortType::None, 0, rotCoarseNoTrack },
		{ ElementType::Rotor,	1, PortType::None, 0, rotXFade },
		{ ElementType::Rotor,	1, PortType::None, 0, rotXFade },
		{ ElementType::Rotor,	1, PortType::None, 0, rotPhaseSync },
		{ ElementType::Rotor,	1, PortType::None, 0, rotPhase },
		{ ElementType::Rotor,	1, PortType::None, 0, rotPhase },
		{ ElementType::Rotor,	1, PortType::None, 0, rotClockSync },
		{ ElementType::Rotor,	1, PortType::None, 0, rotNoTrack },
		{ ElementType::Rotor,	1, PortType::None, 0, rotLow },

		{ ElementType::Rotor,	1, PortType::Mod,		0, psiSource },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	0, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	0, psiAmount },
		{ ElementType::Rotor,	1, PortType::Control,	0, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiDest },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		0, psiAmount },

		{ ElementType::Rotor,	1, PortType::Mod,		1, psiSource },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	1, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	1, psiAmount },
		{ ElementType::Rotor,	1, PortType::Control,	1, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiDest },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		1, psiAmount },

		{ ElementType::Rotor,	1, PortType::Mod,		2, psiSource },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	2, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	2, psiAmount },
		{ ElementType::Rotor,	1, PortType::Control,	2, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiDest },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		2, psiAmount },

		{ ElementType::Rotor,	1, PortType::Mod,		3, psiSource },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	3, psiSource },
		{ ElementType::Rotor,	1, PortType::Control,	3, psiAmount },
		{ ElementType::Rotor,	1, PortType::Control,	3, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiDest },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiAmtPitch },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiAmount },
		{ ElementType::Rotor,	1, PortType::Mod,		3, psiAmount },
	}
};

uint8_t vecByteCount[VEC_MIDI_BLOCK_SIZE] =
{
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 2, 0, 2, 0, 1,	// vector 1
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 2, 0, 2, 0, 1,	// vector 2
	1, 2, 0, 2, 0, 2, 0, 1, 1, 2, 0,	// AM 1
	1, 2, 0, 2, 0, 2, 0, 1, 1, 2, 0,	// AM 2
};

ParamID vecParamID[VEC_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Vector,	0, PortType::Input, 0, psiSource },		// Vector 1 Input 1
	{ ElementType::Vector,	0, PortType::Input, 0, psiSource },		// Vector 1 Input 1
	{ ElementType::Vector,	0, PortType::Input, 0, psiAmount },		// Vector 1 Level 1 MSB
	{ ElementType::Vector,	0, PortType::Input, 0, psiAmount },		// Vector 1 Level 1 LSB
	{ ElementType::Vector,	0, PortType::Input, 1, psiSource },		// Vector 1 Input 2
	{ ElementType::Vector,	0, PortType::Input, 1, psiSource },		// Vector 1 Input 2
	{ ElementType::Vector,	0, PortType::Input, 1, psiAmount },		// Vector 1 Level 2 MSB
	{ ElementType::Vector,	0, PortType::Input, 1, psiAmount },		// Vector 1 Level 2 LSB
	{ ElementType::Vector,	0, PortType::Input, 2, psiSource },		// Vector 1 Input 3
	{ ElementType::Vector,	0, PortType::Input, 2, psiSource },		// Vector 1 Input 3
	{ ElementType::Vector,	0, PortType::Input, 2, psiAmount },		// Vector 1 Level 3 MSB
	{ ElementType::Vector,	0, PortType::Input, 2, psiAmount },		// Vector 1 Level 3 LSB
	{ ElementType::Vector,	0, PortType::Input, 3, psiSource },		// Vector 1 Input 4
	{ ElementType::Vector,	0, PortType::Input, 3, psiSource },		// Vector 1 Input 4
	{ ElementType::Vector,	0, PortType::Input, 3, psiAmount },		// Vector 1 Level 4 MSB
	{ ElementType::Vector,	0, PortType::Input, 3, psiAmount },		// Vector 1 Level 4 LSB
	{ ElementType::Vector,	0, PortType::Mod,	0, psiSource },		// Vector 1 Mod Source X
	{ ElementType::Vector,	0, PortType::Mod,	0, psiSource },		// Vector 1 Mod Source X
	{ ElementType::Vector,	0, PortType::Mod,	0, psiAmount },		// Vector 1 Mod Amount X MSB
	{ ElementType::Vector,	0, PortType::Mod,	0, psiAmount },		// Vector 1 Mod Amount X LSB
	{ ElementType::Vector,	0, PortType::None,	0, vecXOffset },	// Vector 1 Offset X,
	{ ElementType::Vector,	0, PortType::Mod,	1, psiSource },		// Vector 1 Mod Source Y
	{ ElementType::Vector,	0, PortType::Mod,	1, psiSource },		// Vector 1 Mod Source Y
	{ ElementType::Vector,	0, PortType::Mod,	1, psiAmount },		// Vector 1 Mod Amount Y MSB
	{ ElementType::Vector,	0, PortType::Mod,	1, psiAmount },		// Vector 1 Mod Amount Y LSB
	{ ElementType::Vector,	0, PortType::None,	0, vecYOffset },	// Vector 1 Offset Y,

	{ ElementType::Vector,	1, PortType::Input, 0, psiSource },		// Vector 2 Input 1
	{ ElementType::Vector,	1, PortType::Input, 0, psiSource },		// Vector 2 Input 1
	{ ElementType::Vector,	1, PortType::Input, 0, psiAmount },		// Vector 2 Level 1 MSB
	{ ElementType::Vector,	1, PortType::Input, 0, psiAmount },		// Vector 2 Level 1 LSB
	{ ElementType::Vector,	1, PortType::Input, 1, psiSource },		// Vector 2 Input 2
	{ ElementType::Vector,	1, PortType::Input, 1, psiSource },		// Vector 2 Input 2
	{ ElementType::Vector,	1, PortType::Input, 1, psiAmount },		// Vector 2 Level 2 MSB
	{ ElementType::Vector,	1, PortType::Input, 1, psiAmount },		// Vector 2 Level 2 LSB
	{ ElementType::Vector,	1, PortType::Input, 2, psiSource },		// Vector 2 Input 3
	{ ElementType::Vector,	1, PortType::Input, 2, psiSource },		// Vector 2 Input 3
	{ ElementType::Vector,	1, PortType::Input, 2, psiAmount },		// Vector 2 Level 3 MSB
	{ ElementType::Vector,	1, PortType::Input, 2, psiAmount },		// Vector 2 Level 3 LSB
	{ ElementType::Vector,	1, PortType::Input, 3, psiSource },		// Vector 2 Input 4
	{ ElementType::Vector,	1, PortType::Input, 3, psiSource },		// Vector 2 Input 4
	{ ElementType::Vector,	1, PortType::Input, 3, psiAmount },		// Vector 2 Level 4 MSB
	{ ElementType::Vector,	1, PortType::Input, 3, psiAmount },		// Vector 2 Level 4 LSB
	{ ElementType::Vector,	1, PortType::Mod,	0, psiSource },		// Vector 2 Mod Source X
	{ ElementType::Vector,	1, PortType::Mod,	0, psiSource },		// Vector 2 Mod Source X
	{ ElementType::Vector,	1, PortType::Mod,	0, psiAmount },		// Vector 2 Mod Amount X MSB
	{ ElementType::Vector,	1, PortType::Mod,	0, psiAmount },		// Vector 2 Mod Amount X LSB
	{ ElementType::Vector,	1, PortType::None,	0, vecXOffset },	// Vector 2 Offset X,
	{ ElementType::Vector,	1, PortType::Mod,	1, psiSource },		// Vector 2 Mod Source Y
	{ ElementType::Vector,	1, PortType::Mod,	1, psiSource },		// Vector 2 Mod Source Y
	{ ElementType::Vector,	1, PortType::Mod,	1, psiAmount },		// Vector 2 Mod Amount Y MSB
	{ ElementType::Vector,	1, PortType::Mod,	1, psiAmount },		// Vector 2 Mod Amount Y LSB
	{ ElementType::Vector,	1, PortType::None,	0, vecYOffset },	// Vector 2 Offset Y,

	{ ElementType::AM,		0, PortType::None,		0, amAlgorithm },	// AM 1 Algorithm
	{ ElementType::AM,		0, PortType::Input,		0, psiSource },		// AM 1 Carrier
	{ ElementType::AM,		0, PortType::Input,		0, psiSource },		// AM 1 Carrier
	{ ElementType::AM,		0, PortType::Mod,		0, psiSource },		// AM 1 Mod
	{ ElementType::AM,		0, PortType::Mod,		0, psiSource },		// AM 1 Mod
	{ ElementType::AM,		0, PortType::Mod,		0, psiAmount },		// AM 1 Mod Amount MSB
	{ ElementType::AM,		0, PortType::Mod,		0, psiAmount },		// AM 1 Mod Amount LSB
	{ ElementType::AM,		0, PortType::None,		0, amOffset },		// AM 1 Offset
	{ ElementType::AM,		0, PortType::Control,	0, psiSource },		// AM 1 Mod Control Source
	{ ElementType::AM,		0, PortType::Control,	0, psiAmount },		// AM 1 Mod Control Strength MSB
	{ ElementType::AM,		0, PortType::Control,	0, psiAmount },		// AM 1 Mod Control Strength LSB

	{ ElementType::AM,		1, PortType::None,		0, amAlgorithm },	// AM 2 Algorithm
	{ ElementType::AM,		1, PortType::Input,		0, psiSource },		// AM 2 Carrier
	{ ElementType::AM,		1, PortType::Input,		0, psiSource },		// AM 2 Carrier
	{ ElementType::AM,		1, PortType::Mod,		0, psiSource },		// AM 2 Mod
	{ ElementType::AM,		1, PortType::Mod,		0, psiSource },		// AM 2 Mod
	{ ElementType::AM,		1, PortType::Mod,		0, psiAmount },		// AM 2 Mod Amount MSB
	{ ElementType::AM,		1, PortType::Mod,		0, psiAmount },		// AM 2 Mod Amount LSB
	{ ElementType::AM,		1, PortType::None,		0, amOffset },		// AM 2 Offset
	{ ElementType::AM,		1, PortType::Control,	0, psiSource },		// AM 2 Mod Control Source
	{ ElementType::AM,		1, PortType::Control,	0, psiAmount },		// AM 2 Mod Control Strength MSB
	{ ElementType::AM,		1, PortType::Control,	0, psiAmount },		// AM 2 Mod Control Strength LSB

};

uint8_t vcaByteCount[VCA_MIDI_BLOCK_SIZE] =
{
	2, 0, 1, 2, 0, 2, 0, 1, 2, 0, 2, 0, 2, 0, 2, 0, // VCA 1
	2, 0, 1, 2, 0, 2, 0, 1, 2, 0, 2, 0, 2, 0, 2, 0, // VCA 2
	2, 0, 1, 2, 0, 2, 0, 1, 2, 0, 2, 0, 2, 0, 2, 0, // VCA 3
	2, 0, 1, 2, 0, 2, 0, 1, 2, 0, 2, 0, 2, 0, 2, 0, // VCA 4
	1, 1, 1, 1,
	2, 0, 3, 0, 0, 2, 0, 3, 0, 0, 2, 0, 3, 0, 0, 2, 0, 3, 0, 0,
	2, 0, 2, 0, 2, 0, 3, 0, 0, 3, 0, 0	// Envelope follower
};

ParamID vcaParamID[VCA_MIDI_BLOCK_SIZE] =
{
	{ ElementType::VCA,		0, PortType::Input,	0, psiSource },		// VCA 1 Input
	{ ElementType::VCA,		0, PortType::Input,	0, psiSource },		// VCA 1 Input
	{ ElementType::VCA,		0, PortType::None,	0, vcaType },		// VCA 1 Type
	{ ElementType::VCA,		0, PortType::None,	0, vcaBoost },		// VCA 1 Boost MSB
	{ ElementType::VCA,		0, PortType::None,	0, vcaBoost },		// VCA 1 Boost LSB
	{ ElementType::VCA,		0, PortType::None,	0, vcaLevel },		// VCA 1 Level MSB
	{ ElementType::VCA,		0, PortType::None,	0, vcaLevel },		// VCA 1 Level LSB
	{ ElementType::VCA,		0, PortType::None,	0, vcaInitPan },	// VCA 1 InitPan
	{ ElementType::VCA,		0, PortType::Mod,	0, psiSource },		// VCA 1 Mod 1 Source (Level)
	{ ElementType::VCA,		0, PortType::Mod,	0, psiSource },		// VCA 1 Mod 1 Source (Level)
	{ ElementType::VCA,		0, PortType::Mod,	0, psiAmount },		// VCA 1 Mod 1 Amount (Level) MSB
	{ ElementType::VCA,		0, PortType::Mod,	0, psiAmount },		// VCA 1 Mod 1 Amount (Level) LSB
	{ ElementType::VCA,		0, PortType::Mod,	1, psiSource },		// VCA 1 Mod 2 Source (Pan)
	{ ElementType::VCA,		0, PortType::Mod,	1, psiSource },		// VCA 1 Mod 2 Source (Pan)
	{ ElementType::VCA,		0, PortType::Mod,	1, psiAmount },		// VCA 1 Mod 2 Amount (Pan) MSB
	{ ElementType::VCA,		0, PortType::Mod,	1, psiAmount },		// VCA 1 Mod 2 Amount (Pan) LSB

	{ ElementType::VCA,		1, PortType::Input,	0, psiSource },		// VCA 2 Input
	{ ElementType::VCA,		1, PortType::Input,	0, psiSource },		// VCA 2 Input
	{ ElementType::VCA,		1, PortType::None,	0, vcaType },		// VCA 2 Type
	{ ElementType::VCA,		1, PortType::None,	0, vcaBoost },		// VCA 2 Boost MSB
	{ ElementType::VCA,		1, PortType::None,	0, vcaBoost },		// VCA 2 Boost LSB
	{ ElementType::VCA,		1, PortType::None,	0, vcaLevel },		// VCA 2 Level MSB
	{ ElementType::VCA,		1, PortType::None,	0, vcaLevel },		// VCA 2 Level LSB
	{ ElementType::VCA,		1, PortType::None,	0, vcaInitPan },	// VCA 2 InitPan
	{ ElementType::VCA,		1, PortType::Mod,	0, psiSource },		// VCA 2 Mod 1 Source (Level)
	{ ElementType::VCA,		1, PortType::Mod,	0, psiSource },		// VCA 2 Mod 1 Source (Level)
	{ ElementType::VCA,		1, PortType::Mod,	0, psiAmount },		// VCA 2 Mod 1 Amount (Level) MSB
	{ ElementType::VCA,		1, PortType::Mod,	0, psiAmount },		// VCA 2 Mod 1 Amount (Level) LSB
	{ ElementType::VCA,		1, PortType::Mod,	1, psiSource },		// VCA 2 Mod 2 Source (Pan)
	{ ElementType::VCA,		1, PortType::Mod,	1, psiSource },		// VCA 2 Mod 2 Source (Pan)
	{ ElementType::VCA,		1, PortType::Mod,	1, psiAmount },		// VCA 2 Mod 2 Amount (Pan) MSB
	{ ElementType::VCA,		1, PortType::Mod,	1, psiAmount },		// VCA 2 Mod 2 Amount (Pan) LSB

	{ ElementType::VCA,		2, PortType::Input,	0, psiSource },		// VCA 3 Input
	{ ElementType::VCA,		2, PortType::Input,	0, psiSource },		// VCA 3 Input
	{ ElementType::VCA,		2, PortType::None,	0, vcaType },		// VCA 3 Type
	{ ElementType::VCA,		2, PortType::None,	0, vcaBoost },		// VCA 3 Boost MSB
	{ ElementType::VCA,		2, PortType::None,	0, vcaBoost },		// VCA 3 Boost LSB
	{ ElementType::VCA,		2, PortType::None,	0, vcaLevel },		// VCA 3 Level MSB
	{ ElementType::VCA,		2, PortType::None,	0, vcaLevel },		// VCA 3 Level LSB
	{ ElementType::VCA,		2, PortType::None,	0, vcaInitPan },	// VCA 3 InitPan
	{ ElementType::VCA,		2, PortType::Mod,	0, psiSource },		// VCA 3 Mod 1 Source (Level)
	{ ElementType::VCA,		2, PortType::Mod,	0, psiSource },		// VCA 3 Mod 1 Source (Level)
	{ ElementType::VCA,		2, PortType::Mod,	0, psiAmount },		// VCA 3 Mod 1 Amount (Level) MSB
	{ ElementType::VCA,		2, PortType::Mod,	0, psiAmount },		// VCA 3 Mod 1 Amount (Level) LSB
	{ ElementType::VCA,		2, PortType::Mod,	1, psiSource },		// VCA 3 Mod 2 Source (Pan)
	{ ElementType::VCA,		2, PortType::Mod,	1, psiSource },		// VCA 3 Mod 2 Source (Pan)
	{ ElementType::VCA,		2, PortType::Mod,	1, psiAmount },		// VCA 3 Mod 2 Amount (Pan) MSB
	{ ElementType::VCA,		2, PortType::Mod,	1, psiAmount },		// VCA 3 Mod 2 Amount (Pan) LSB

	{ ElementType::VCA,		3, PortType::Input,	0, psiSource },		// VCA 4 Input
	{ ElementType::VCA,		3, PortType::Input,	0, psiSource },		// VCA 4 Input
	{ ElementType::VCA,		3, PortType::None,	0, vcaType },		// VCA 4 Type
	{ ElementType::VCA,		3, PortType::None,	0, vcaBoost },		// VCA 4 Boost MSB
	{ ElementType::VCA,		3, PortType::None,	0, vcaBoost },		// VCA 4 Boost LSB
	{ ElementType::VCA,		3, PortType::None,	0, vcaLevel },		// VCA 4 Level MSB
	{ ElementType::VCA,		3, PortType::None,	0, vcaLevel },		// VCA 4 Level LSB
	{ ElementType::VCA,		3, PortType::None,	0, vcaInitPan },	// VCA 4 InitPan
	{ ElementType::VCA,		3, PortType::Mod,	0, psiSource },		// VCA 4 Mod 1 Source (Level)
	{ ElementType::VCA,		3, PortType::Mod,	0, psiSource },		// VCA 4 Mod 1 Source (Level)
	{ ElementType::VCA,		3, PortType::Mod,	0, psiAmount },		// VCA 4 Mod 1 Amount (Level) MSB
	{ ElementType::VCA,		3, PortType::Mod,	0, psiAmount },		// VCA 4 Mod 1 Amount (Level) LSB
	{ ElementType::VCA,		3, PortType::Mod,	1, psiSource },		// VCA 4 Mod 2 Source (Pan)
	{ ElementType::VCA,		3, PortType::Mod,	1, psiSource },		// VCA 4 Mod 2 Source (Pan)
	{ ElementType::VCA,		3, PortType::Mod,	1, psiAmount },		// VCA 4 Mod 2 Amount (Pan) MSB
	{ ElementType::VCA,		3, PortType::Mod,	1, psiAmount },		// VCA 4 Mod 2 Amount (Pan) LSB

	{ ElementType::VCAMaster,	0, PortType::Input,	0, psiSource },	// Enable Part 1
	{ ElementType::VCAMaster,	0, PortType::Input,	1, psiSource },	// Enable Part 2
	{ ElementType::VCAMaster,	0, PortType::Input,	2, psiSource },	// Enable Part 3
	{ ElementType::VCAMaster,	0, PortType::Input,	3, psiSource },	// Enable Part 4

	{ ElementType::Lag,	0, PortType::Input,	0, psiSource },	// Lag 1 Input
	{ ElementType::Lag,	0, PortType::Input,	0, psiSource },	// Lag 1 Input
	{ ElementType::Lag,	0, PortType::None,	0, lagTime },  	// Lag 1 Time MSB
	{ ElementType::Lag,	0, PortType::None,	0, lagTime },  	// Lag 1 Time
	{ ElementType::Lag,	0, PortType::None,	0, lagTime },  	// Lag 1 Time LSB
	{ ElementType::Lag,	1, PortType::Input,	0, psiSource },	// Lag 2 Input
	{ ElementType::Lag,	1, PortType::Input,	0, psiSource },	// Lag 2 Input
	{ ElementType::Lag,	1, PortType::None,	0, lagTime },  	// Lag 2 Time MSB
	{ ElementType::Lag,	1, PortType::None,	0, lagTime },  	// Lag 2 Time
	{ ElementType::Lag,	1, PortType::None,	0, lagTime },  	// Lag 2 Time LSB
	{ ElementType::Lag,	2, PortType::Input,	0, psiSource },	// Lag 3 Input
	{ ElementType::Lag,	2, PortType::Input,	0, psiSource },	// Lag 3 Input
	{ ElementType::Lag,	2, PortType::None,	0, lagTime },  	// Lag 3 Time MSB
	{ ElementType::Lag,	2, PortType::None,	0, lagTime },  	// Lag 3 Time
	{ ElementType::Lag,	2, PortType::None,	0, lagTime },  	// Lag 3 Time LSB
	{ ElementType::Lag,	3, PortType::Input,	0, psiSource },	// Lag 4 Input
	{ ElementType::Lag,	3, PortType::Input,	0, psiSource },	// Lag 4 Input
	{ ElementType::Lag,	3, PortType::None,	0, lagTime },  	// Lag 4 Time MSB
	{ ElementType::Lag,	3, PortType::None,	0, lagTime },  	// Lag 4 Time
	{ ElementType::Lag,	3, PortType::None,	0, lagTime },  	// Lag 4 Time LSB

	{ ElementType::EnvFol,	0, PortType::Input,	0, psiSource },
	{ ElementType::EnvFol,	0, PortType::Input,	0, psiSource },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolInput },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolInput },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolOutput },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolOutput },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolAttack },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolAttack },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolAttack },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolRelease },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolRelease },
	{ ElementType::EnvFol,	0, PortType::None,	0, envFolRelease },
};

uint8_t filByteCount[FIL_MIDI_BLOCK_SIZE] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
};

ParamID filParamID[4][FIL_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::Filter,	0, PortType::None,		0, filterType },
		{ ElementType::Filter,	0, PortType::None,		0, filterMM1Mode },
		{ ElementType::Filter,	0, PortType::None,		0, filterObieMode },
		{ ElementType::Filter,	0, PortType::None,		0, filterCombMode },
		{ ElementType::Filter,	0, PortType::None,		0, filterVowel1 },
		{ ElementType::Filter,	0, PortType::None,		0, filterVowel2 },
		{ ElementType::Filter,	0, PortType::None,		0, filterVowel3 },
		{ ElementType::Filter,	0, PortType::None,		0, filterVowel4 },
		{ ElementType::Filter,	0, PortType::None,		0, filterVowel5 },
		{ ElementType::Filter,	0, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	0, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	0, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	0, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	0, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	0, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	0, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	0, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	0, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	0, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	0, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	0, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	0, PortType::None,		0, filterKeycenter },
		{ ElementType::Filter,	0, PortType::Mod,		0, psiSource },		// Filter 1 Mod 1 Source
		{ ElementType::Filter,	0, PortType::Mod,		0, psiSource },		// Filter 1 Mod 1 Source
		{ ElementType::Filter,	0, PortType::Control,	0, psiSource },		// Filter 1 Mod 1 Control Source
		{ ElementType::Filter,	0, PortType::Control,	0, psiAmount },		// Filter 1 Mod 1 Control Strength
		{ ElementType::Filter,	0, PortType::Control,	0, psiAmount },		// Filter 1 Mod 1 Control Strength
		{ ElementType::Filter,	0, PortType::Mod,		0, psiDest },		// Filter 1 Mod 1 Dest
		{ ElementType::Filter,	0, PortType::Mod,		0, psiAmtPitch },	// Filter 1 Mod 1 Amount Pitch MSB
		{ ElementType::Filter,	0, PortType::Mod,		0, psiAmtPitch },	// Filter 1 Mod 1 Amount Pitch LSB
		{ ElementType::Filter,	0, PortType::Mod,		0, psiAmount },		// Filter 1 Mod 1 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	0, PortType::Mod,		0, psiAmount },		// Filter 1 Mod 1 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	0, PortType::Mod,		1, psiSource },		// Filter 1 Mod 2 Source
		{ ElementType::Filter,	0, PortType::Mod,		1, psiSource },		// Filter 1 Mod 2 Source
		{ ElementType::Filter,	0, PortType::Control,	1, psiSource },		// Filter 1 Mod 2 Control Source
		{ ElementType::Filter,	0, PortType::Control,	1, psiAmount },		// Filter 1 Mod 2 Control Strength
		{ ElementType::Filter,	0, PortType::Control,	1, psiAmount },		// Filter 1 Mod 2 Control Strength
		{ ElementType::Filter,	0, PortType::Mod,		1, psiDest },		// Filter 1 Mod 2 Dest
		{ ElementType::Filter,	0, PortType::Mod,		1, psiAmtPitch },	// Filter 1 Mod 2 Amount Pitch MSB
		{ ElementType::Filter,	0, PortType::Mod,		1, psiAmtPitch },	// Filter 1 Mod 2 Amount Pitch LSB
		{ ElementType::Filter,	0, PortType::Mod,		1, psiAmount },		// Filter 1 Mod 2 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	0, PortType::Mod,		1, psiAmount },		// Filter 1 Mod 2 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	0, PortType::Mod,		2, psiSource },		// Filter 1 Mod 3 Source
		{ ElementType::Filter,	0, PortType::Mod,		2, psiSource },		// Filter 1 Mod 3 Source
		{ ElementType::Filter,	0, PortType::Control,	2, psiSource },		// Filter 1 Mod 3 Control Source
		{ ElementType::Filter,	0, PortType::Control,	2, psiAmount },		// Filter 1 Mod 3 Control Strength
		{ ElementType::Filter,	0, PortType::Control,	2, psiAmount },		// Filter 1 Mod 3 Control Strength
		{ ElementType::Filter,	0, PortType::Mod,		2, psiDest },		// Filter 1 Mod 3 Dest
		{ ElementType::Filter,	0, PortType::Mod,		2, psiAmtPitch },	// Filter 1 Mod 3 Amount Pitch MSB
		{ ElementType::Filter,	0, PortType::Mod,		2, psiAmtPitch },	// Filter 1 Mod 3 Amount Pitch LSB
		{ ElementType::Filter,	0, PortType::Mod,		2, psiAmount },		// Filter 1 Mod 3 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	0, PortType::Mod,		2, psiAmount },		// Filter 1 Mod 3 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	0, PortType::Mod,		3, psiSource },		// Filter 1 Mod 4 Source
		{ ElementType::Filter,	0, PortType::Mod,		3, psiSource },		// Filter 1 Mod 4 Source
		{ ElementType::Filter,	0, PortType::Control,	3, psiSource },		// Filter 1 Mod 4 Control Source
		{ ElementType::Filter,	0, PortType::Control,	3, psiAmount },		// Filter 1 Mod 4 Control Strength
		{ ElementType::Filter,	0, PortType::Control,	3, psiAmount },		// Filter 1 Mod 4 Control Strength
		{ ElementType::Filter,	0, PortType::Mod,		3, psiDest },		// Filter 1 Mod 4 Dest
		{ ElementType::Filter,	0, PortType::Mod,		3, psiAmtPitch },	// Filter 1 Mod 4 Amount Pitch MSB
		{ ElementType::Filter,	0, PortType::Mod,		3, psiAmtPitch },	// Filter 1 Mod 4 Amount Pitch LSB
		{ ElementType::Filter,	0, PortType::Mod,		3, psiAmount },		// Filter 1 Mod 4 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	0, PortType::Mod,		3, psiAmount },		// Filter 1 Mod 4 Amount Res/XFade/Damping LSB
	},
	{
		{ ElementType::Filter,	1, PortType::None,		0, filterType },
		{ ElementType::Filter,	1, PortType::None,		0, filterMM1Mode },
		{ ElementType::Filter,	1, PortType::None,		0, filterObieMode },
		{ ElementType::Filter,	1, PortType::None,		0, filterCombMode },
		{ ElementType::Filter,	1, PortType::None,		0, filterVowel1 },
		{ ElementType::Filter,	1, PortType::None,		0, filterVowel2 },
		{ ElementType::Filter,	1, PortType::None,		0, filterVowel3 },
		{ ElementType::Filter,	1, PortType::None,		0, filterVowel4 },
		{ ElementType::Filter,	1, PortType::None,		0, filterVowel5 },
		{ ElementType::Filter,	1, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	1, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	1, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	1, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	1, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	1, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	1, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	1, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	1, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	1, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	1, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	1, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	1, PortType::None,		0, filterKeycenter },
		{ ElementType::Filter,	1, PortType::Mod,		0, psiSource },		// Filter 2 Mod 1 Source
		{ ElementType::Filter,	1, PortType::Mod,		0, psiSource },		// Filter 2 Mod 1 Source
		{ ElementType::Filter,	1, PortType::Control,	0, psiSource },		// Filter 2 Mod 1 Control Source
		{ ElementType::Filter,	1, PortType::Control,	0, psiAmount },		// Filter 2 Mod 1 Control Strength
		{ ElementType::Filter,	1, PortType::Control,	0, psiAmount },		// Filter 2 Mod 1 Control Strength
		{ ElementType::Filter,	1, PortType::Mod,		0, psiDest },		// Filter 2 Mod 1 Dest
		{ ElementType::Filter,	1, PortType::Mod,		0, psiAmtPitch },	// Filter 2 Mod 1 Amount Pitch MSB
		{ ElementType::Filter,	1, PortType::Mod,		0, psiAmtPitch },	// Filter 2 Mod 1 Amount Pitch LSB
		{ ElementType::Filter,	1, PortType::Mod,		0, psiAmount },		// Filter 2 Mod 1 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	1, PortType::Mod,		0, psiAmount },		// Filter 2 Mod 1 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	1, PortType::Mod,		1, psiSource },		// Filter 2 Mod 2 Source
		{ ElementType::Filter,	1, PortType::Mod,		1, psiSource },		// Filter 2 Mod 2 Source
		{ ElementType::Filter,	1, PortType::Control,	1, psiSource },		// Filter 2 Mod 2 Control Source
		{ ElementType::Filter,	1, PortType::Control,	1, psiAmount },		// Filter 2 Mod 2 Control Strength
		{ ElementType::Filter,	1, PortType::Control,	1, psiAmount },		// Filter 2 Mod 2 Control Strength
		{ ElementType::Filter,	1, PortType::Mod,		1, psiDest },		// Filter 2 Mod 2 Dest
		{ ElementType::Filter,	1, PortType::Mod,		1, psiAmtPitch },	// Filter 2 Mod 2 Amount Pitch MSB
		{ ElementType::Filter,	1, PortType::Mod,		1, psiAmtPitch },	// Filter 2 Mod 2 Amount Pitch LSB
		{ ElementType::Filter,	1, PortType::Mod,		1, psiAmount },		// Filter 2 Mod 2 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	1, PortType::Mod,		1, psiAmount },		// Filter 2 Mod 2 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	1, PortType::Mod,		2, psiSource },		// Filter 2 Mod 3 Source
		{ ElementType::Filter,	1, PortType::Mod,		2, psiSource },		// Filter 2 Mod 3 Source
		{ ElementType::Filter,	1, PortType::Control,	2, psiSource },		// Filter 2 Mod 3 Control Source
		{ ElementType::Filter,	1, PortType::Control,	2, psiAmount },		// Filter 2 Mod 3 Control Strength
		{ ElementType::Filter,	1, PortType::Control,	2, psiAmount },		// Filter 2 Mod 3 Control Strength
		{ ElementType::Filter,	1, PortType::Mod,		2, psiDest },		// Filter 2 Mod 3 Dest
		{ ElementType::Filter,	1, PortType::Mod,		2, psiAmtPitch },	// Filter 2 Mod 3 Amount Pitch MSB
		{ ElementType::Filter,	1, PortType::Mod,		2, psiAmtPitch },	// Filter 2 Mod 3 Amount Pitch LSB
		{ ElementType::Filter,	1, PortType::Mod,		2, psiAmount },		// Filter 2 Mod 3 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	1, PortType::Mod,		2, psiAmount },		// Filter 2 Mod 3 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	1, PortType::Mod,		3, psiSource },		// Filter 2 Mod 4 Source
		{ ElementType::Filter,	1, PortType::Mod,		3, psiSource },		// Filter 2 Mod 4 Source
		{ ElementType::Filter,	1, PortType::Control,	3, psiSource },		// Filter 2 Mod 4 Control Source
		{ ElementType::Filter,	1, PortType::Control,	3, psiAmount },		// Filter 2 Mod 4 Control Strength
		{ ElementType::Filter,	1, PortType::Control,	3, psiAmount },		// Filter 2 Mod 4 Control Strength
		{ ElementType::Filter,	1, PortType::Mod,		3, psiDest },		// Filter 2 Mod 4 Dest
		{ ElementType::Filter,	1, PortType::Mod,		3, psiAmtPitch },	// Filter 2 Mod 4 Amount Pitch MSB
		{ ElementType::Filter,	1, PortType::Mod,		3, psiAmtPitch },	// Filter 2 Mod 4 Amount Pitch LSB
		{ ElementType::Filter,	1, PortType::Mod,		3, psiAmount },		// Filter 2 Mod 4 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	1, PortType::Mod,		3, psiAmount },		// Filter 2 Mod 4 Amount Res/XFade/Damping LSB
	},
	{
		{ ElementType::Filter,	2, PortType::None,		0, filterType },
		{ ElementType::Filter,	2, PortType::None,		0, filterMM1Mode },
		{ ElementType::Filter,	2, PortType::None,		0, filterObieMode },
		{ ElementType::Filter,	2, PortType::None,		0, filterCombMode },
		{ ElementType::Filter,	2, PortType::None,		0, filterVowel1 },
		{ ElementType::Filter,	2, PortType::None,		0, filterVowel2 },
		{ ElementType::Filter,	2, PortType::None,		0, filterVowel3 },
		{ ElementType::Filter,	2, PortType::None,		0, filterVowel4 },
		{ ElementType::Filter,	2, PortType::None,		0, filterVowel5 },
		{ ElementType::Filter,	2, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	2, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	2, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	2, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	2, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	2, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	2, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	2, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	2, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	2, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	2, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	2, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	2, PortType::None,		0, filterKeycenter },
		{ ElementType::Filter,	2, PortType::Mod,		0, psiSource },		// Filter 3 Mod 1 Source
		{ ElementType::Filter,	2, PortType::Mod,		0, psiSource },		// Filter 3 Mod 1 Source
		{ ElementType::Filter,	2, PortType::Control,	0, psiSource },		// Filter 3 Mod 1 Control Source
		{ ElementType::Filter,	2, PortType::Control,	0, psiAmount },		// Filter 3 Mod 1 Control Strength
		{ ElementType::Filter,	2, PortType::Control,	0, psiAmount },		// Filter 3 Mod 1 Control Strength
		{ ElementType::Filter,	2, PortType::Mod,		0, psiDest },		// Filter 3 Mod 1 Dest
		{ ElementType::Filter,	2, PortType::Mod,		0, psiAmtPitch },	// Filter 3 Mod 1 Amount Pitch MSB
		{ ElementType::Filter,	2, PortType::Mod,		0, psiAmtPitch },	// Filter 3 Mod 1 Amount Pitch LSB
		{ ElementType::Filter,	2, PortType::Mod,		0, psiAmount },		// Filter 3 Mod 1 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	2, PortType::Mod,		0, psiAmount },		// Filter 3 Mod 1 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	2, PortType::Mod,		1, psiSource },		// Filter 3 Mod 2 Source
		{ ElementType::Filter,	2, PortType::Mod,		1, psiSource },		// Filter 3 Mod 2 Source
		{ ElementType::Filter,	2, PortType::Control,	1, psiSource },		// Filter 3 Mod 2 Control Source
		{ ElementType::Filter,	2, PortType::Control,	1, psiAmount },		// Filter 3 Mod 2 Control Strength
		{ ElementType::Filter,	2, PortType::Control,	1, psiAmount },		// Filter 3 Mod 2 Control Strength
		{ ElementType::Filter,	2, PortType::Mod,		1, psiDest },		// Filter 3 Mod 2 Dest
		{ ElementType::Filter,	2, PortType::Mod,		1, psiAmtPitch },	// Filter 3 Mod 2 Amount Pitch MSB
		{ ElementType::Filter,	2, PortType::Mod,		1, psiAmtPitch },	// Filter 3 Mod 2 Amount Pitch LSB
		{ ElementType::Filter,	2, PortType::Mod,		1, psiAmount },		// Filter 3 Mod 2 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	2, PortType::Mod,		1, psiAmount },		// Filter 3 Mod 2 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	2, PortType::Mod,		2, psiSource },		// Filter 3 Mod 3 Source
		{ ElementType::Filter,	2, PortType::Mod,		2, psiSource },		// Filter 3 Mod 3 Source
		{ ElementType::Filter,	2, PortType::Control,	2, psiSource },		// Filter 3 Mod 3 Control Source
		{ ElementType::Filter,	2, PortType::Control,	2, psiAmount },		// Filter 3 Mod 3 Control Strength
		{ ElementType::Filter,	2, PortType::Control,	2, psiAmount },		// Filter 3 Mod 3 Control Strength
		{ ElementType::Filter,	2, PortType::Mod,		2, psiDest },		// Filter 3 Mod 3 Dest
		{ ElementType::Filter,	2, PortType::Mod,		2, psiAmtPitch },	// Filter 3 Mod 3 Amount Pitch MSB
		{ ElementType::Filter,	2, PortType::Mod,		2, psiAmtPitch },	// Filter 3 Mod 3 Amount Pitch LSB
		{ ElementType::Filter,	2, PortType::Mod,		2, psiAmount },		// Filter 3 Mod 3 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	2, PortType::Mod,		2, psiAmount },		// Filter 3 Mod 3 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	2, PortType::Mod,		3, psiSource },		// Filter 3 Mod 4 Source
		{ ElementType::Filter,	2, PortType::Mod,		3, psiSource },		// Filter 3 Mod 4 Source
		{ ElementType::Filter,	2, PortType::Control,	3, psiSource },		// Filter 3 Mod 4 Control Source
		{ ElementType::Filter,	2, PortType::Control,	3, psiAmount },		// Filter 3 Mod 4 Control Strength
		{ ElementType::Filter,	2, PortType::Control,	3, psiAmount },		// Filter 3 Mod 4 Control Strength
		{ ElementType::Filter,	2, PortType::Mod,		3, psiDest },		// Filter 3 Mod 4 Dest
		{ ElementType::Filter,	2, PortType::Mod,		3, psiAmtPitch },	// Filter 3 Mod 4 Amount Pitch MSB
		{ ElementType::Filter,	2, PortType::Mod,		3, psiAmtPitch },	// Filter 3 Mod 4 Amount Pitch LSB
		{ ElementType::Filter,	2, PortType::Mod,		3, psiAmount },		// Filter 3 Mod 4 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	2, PortType::Mod,		3, psiAmount },		// Filter 3 Mod 4 Amount Res/XFade/Damping LSB
	},
	{
		{ ElementType::Filter,	3, PortType::None,		0, filterType },
		{ ElementType::Filter,	3, PortType::None,		0, filterMM1Mode },
		{ ElementType::Filter,	3, PortType::None,		0, filterObieMode },
		{ ElementType::Filter,	3, PortType::None,		0, filterCombMode },
		{ ElementType::Filter,	3, PortType::None,		0, filterVowel1 },
		{ ElementType::Filter,	3, PortType::None,		0, filterVowel2 },
		{ ElementType::Filter,	3, PortType::None,		0, filterVowel3 },
		{ ElementType::Filter,	3, PortType::None,		0, filterVowel4 },
		{ ElementType::Filter,	3, PortType::None,		0, filterVowel5 },
		{ ElementType::Filter,	3, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	3, PortType::None,		0, filterCutoff },
		{ ElementType::Filter,	3, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	3, PortType::None,		0, filterResonance },
		{ ElementType::Filter,	3, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	3, PortType::None,		0, filterDamping },
		{ ElementType::Filter,	3, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	3, PortType::None,		0, filterXFade },
		{ ElementType::Filter,	3, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	3, PortType::Input,		0, psiSource },
		{ ElementType::Filter,	3, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	3, PortType::None,		0, filterKeytracking },
		{ ElementType::Filter,	3, PortType::None,		0, filterKeycenter },
		{ ElementType::Filter,	3, PortType::Mod,		0, psiSource },		// Filter 4 Mod 1 Source
		{ ElementType::Filter,	3, PortType::Mod,		0, psiSource },		// Filter 4 Mod 1 Source
		{ ElementType::Filter,	3, PortType::Control,	0, psiSource },		// Filter 4 Mod 1 Control Source
		{ ElementType::Filter,	3, PortType::Control,	0, psiAmount },		// Filter 4 Mod 1 Control Strength
		{ ElementType::Filter,	3, PortType::Control,	0, psiAmount },		// Filter 4 Mod 1 Control Strength
		{ ElementType::Filter,	3, PortType::Mod,		0, psiDest },		// Filter 4 Mod 1 Dest
		{ ElementType::Filter,	3, PortType::Mod,		0, psiAmtPitch },	// Filter 4 Mod 1 Amount Pitch MSB
		{ ElementType::Filter,	3, PortType::Mod,		0, psiAmtPitch },	// Filter 4 Mod 1 Amount Pitch LSB
		{ ElementType::Filter,	3, PortType::Mod,		0, psiAmount },		// Filter 4 Mod 1 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	3, PortType::Mod,		0, psiAmount },		// Filter 4 Mod 1 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	3, PortType::Mod,		1, psiSource },		// Filter 4 Mod 2 Source
		{ ElementType::Filter,	3, PortType::Mod,		1, psiSource },		// Filter 4 Mod 2 Source
		{ ElementType::Filter,	3, PortType::Control,	1, psiSource },		// Filter 4 Mod 2 Control Source
		{ ElementType::Filter,	3, PortType::Control,	1, psiAmount },		// Filter 4 Mod 2 Control Strength
		{ ElementType::Filter,	3, PortType::Control,	1, psiAmount },		// Filter 4 Mod 2 Control Strength
		{ ElementType::Filter,	3, PortType::Mod,		1, psiDest },		// Filter 4 Mod 2 Dest
		{ ElementType::Filter,	3, PortType::Mod,		1, psiAmtPitch },	// Filter 4 Mod 2 Amount Pitch MSB
		{ ElementType::Filter,	3, PortType::Mod,		1, psiAmtPitch },	// Filter 4 Mod 2 Amount Pitch LSB
		{ ElementType::Filter,	3, PortType::Mod,		1, psiAmount },		// Filter 4 Mod 2 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	3, PortType::Mod,		1, psiAmount },		// Filter 4 Mod 2 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	3, PortType::Mod,		2, psiSource },		// Filter 4 Mod 3 Source
		{ ElementType::Filter,	3, PortType::Mod,		2, psiSource },		// Filter 4 Mod 3 Source
		{ ElementType::Filter,	3, PortType::Control,	2, psiSource },		// Filter 4 Mod 3 Control Source
		{ ElementType::Filter,	3, PortType::Control,	2, psiAmount },		// Filter 4 Mod 3 Control Strength
		{ ElementType::Filter,	3, PortType::Control,	2, psiAmount },		// Filter 4 Mod 3 Control Strength
		{ ElementType::Filter,	3, PortType::Mod,		2, psiDest },		// Filter 4 Mod 3 Dest
		{ ElementType::Filter,	3, PortType::Mod,		2, psiAmtPitch },	// Filter 4 Mod 3 Amount Pitch MSB
		{ ElementType::Filter,	3, PortType::Mod,		2, psiAmtPitch },	// Filter 4 Mod 3 Amount Pitch LSB
		{ ElementType::Filter,	3, PortType::Mod,		2, psiAmount },		// Filter 4 Mod 3 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	3, PortType::Mod,		2, psiAmount },		// Filter 4 Mod 3 Amount Res/XFade/Damping LSB
		{ ElementType::Filter,	3, PortType::Mod,		3, psiSource },		// Filter 4 Mod 4 Source
		{ ElementType::Filter,	3, PortType::Mod,		3, psiSource },		// Filter 4 Mod 4 Source
		{ ElementType::Filter,	3, PortType::Control,	3, psiSource },		// Filter 4 Mod 4 Control Source
		{ ElementType::Filter,	3, PortType::Control,	3, psiAmount },		// Filter 4 Mod 4 Control Strength
		{ ElementType::Filter,	3, PortType::Control,	3, psiAmount },		// Filter 4 Mod 4 Control Strength
		{ ElementType::Filter,	3, PortType::Mod,		3, psiDest },		// Filter 4 Mod 4 Dest
		{ ElementType::Filter,	3, PortType::Mod,		3, psiAmtPitch },	// Filter 4 Mod 4 Amount Pitch MSB
		{ ElementType::Filter,	3, PortType::Mod,		3, psiAmtPitch },	// Filter 4 Mod 4 Amount Pitch LSB
		{ ElementType::Filter,	3, PortType::Mod,		3, psiAmount },		// Filter 4 Mod 4 Amount Res/XFade/Damping MSB
		{ ElementType::Filter,	3, PortType::Mod,		3, psiAmount },		// Filter 4 Mod 4 Amount Res/XFade/Damping LSB
	},
};
		
uint8_t mixByteCount[MIX_MIDI_BLOCK_SIZE] =
{
	2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 1,	// Mixer 1
	2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 1,	// Mixer 2
	2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 1,	// Mixer 3
	2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 2, 0, 1,	// Mixer 4
};

ParamID mixParamID[MIX_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Mixer,	0, PortType::Input, 0, psiSource },		// Mixer 1 Input 1 Source
	{ ElementType::Mixer,	0, PortType::Input, 0, psiSource },		// Mixer 1 Input 1 Source
	{ ElementType::Mixer,	0, PortType::Input, 0, psiAmount },		// Mixer 1 Input 1 Level
	{ ElementType::Mixer,	0, PortType::Mod,	0, psiSource },		// Mixer 1 Input 1 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	0, psiSource },		// Mixer 1 Input 1 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	0, psiAmount },		// Mixer 1 Input 1 Mod Amount
	{ ElementType::Mixer,	0, PortType::Input, 1, psiSource },		// Mixer 1 Input 2 Source
	{ ElementType::Mixer,	0, PortType::Input, 1, psiSource },		// Mixer 1 Input 2 Source
	{ ElementType::Mixer,	0, PortType::Input, 1, psiAmount },		// Mixer 1 Input 2 Level
	{ ElementType::Mixer,	0, PortType::Mod,	1, psiSource },		// Mixer 1 Input 2 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	1, psiSource },		// Mixer 1 Input 2 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	1, psiAmount },		// Mixer 1 Input 2 Mod Amount
	{ ElementType::Mixer,	0, PortType::Input, 2, psiSource },		// Mixer 1 Input 3 Source
	{ ElementType::Mixer,	0, PortType::Input, 2, psiSource },		// Mixer 1 Input 3 Source
	{ ElementType::Mixer,	0, PortType::Input, 2, psiAmount },		// Mixer 1 Input 3 Level
	{ ElementType::Mixer,	0, PortType::Mod,	2, psiSource },		// Mixer 1 Input 3 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	2, psiSource },		// Mixer 1 Input 3 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	2, psiAmount },		// Mixer 1 Input 3 Mod Amount
	{ ElementType::Mixer,	0, PortType::Input, 3, psiSource },		// Mixer 1 Input 4 Source
	{ ElementType::Mixer,	0, PortType::Input, 3, psiSource },		// Mixer 1 Input 4 Source
	{ ElementType::Mixer,	0, PortType::Input, 3, psiAmount },		// Mixer 1 Input 4 Level
	{ ElementType::Mixer,	0, PortType::Mod,	3, psiSource },		// Mixer 1 Input 4 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	3, psiSource },		// Mixer 1 Input 4 Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	3, psiAmount },		// Mixer 1 Input 4 Mod Amount
	{ ElementType::Mixer,	0, PortType::None,	0, mixOutputLevel },// Mixer 1 Output Level MSB
	{ ElementType::Mixer,	0, PortType::None,	0, mixOutputLevel },// Mixer 1 Output Level LSB
	{ ElementType::Mixer,	0, PortType::Mod,	4, psiSource },		// Mixer 1 Output Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	4, psiSource },		// Mixer 1 Output Mod Source
	{ ElementType::Mixer,	0, PortType::Mod,	4, psiAmount },		// Mixer 1 Output Mod Amount

	{ ElementType::Mixer,	1, PortType::Input, 0, psiSource },		// Mixer 2 Input 1 Source
	{ ElementType::Mixer,	1, PortType::Input, 0, psiSource },		// Mixer 2 Input 1 Source
	{ ElementType::Mixer,	1, PortType::Input, 0, psiAmount },		// Mixer 2 Input 1 Level
	{ ElementType::Mixer,	1, PortType::Mod,	0, psiSource },		// Mixer 2 Input 1 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	0, psiSource },		// Mixer 2 Input 1 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	0, psiAmount },		// Mixer 2 Input 1 Mod Amount
	{ ElementType::Mixer,	1, PortType::Input, 1, psiSource },		// Mixer 2 Input 2 Source
	{ ElementType::Mixer,	1, PortType::Input, 1, psiSource },		// Mixer 2 Input 2 Source
	{ ElementType::Mixer,	1, PortType::Input, 1, psiAmount },		// Mixer 2 Input 2 Level
	{ ElementType::Mixer,	1, PortType::Mod,	1, psiSource },		// Mixer 2 Input 2 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	1, psiSource },		// Mixer 2 Input 2 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	1, psiAmount },		// Mixer 2 Input 2 Mod Amount
	{ ElementType::Mixer,	1, PortType::Input, 2, psiSource },		// Mixer 2 Input 3 Source
	{ ElementType::Mixer,	1, PortType::Input, 2, psiSource },		// Mixer 2 Input 3 Source
	{ ElementType::Mixer,	1, PortType::Input, 2, psiAmount },		// Mixer 2 Input 3 Level
	{ ElementType::Mixer,	1, PortType::Mod,	2, psiSource },		// Mixer 2 Input 3 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	2, psiSource },		// Mixer 2 Input 3 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	2, psiAmount },		// Mixer 2 Input 3 Mod Amount
	{ ElementType::Mixer,	1, PortType::Input, 3, psiSource },		// Mixer 2 Input 4 Source
	{ ElementType::Mixer,	1, PortType::Input, 3, psiSource },		// Mixer 2 Input 4 Source
	{ ElementType::Mixer,	1, PortType::Input, 3, psiAmount },		// Mixer 2 Input 4 Level
	{ ElementType::Mixer,	1, PortType::Mod,	3, psiSource },		// Mixer 2 Input 4 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	3, psiSource },		// Mixer 2 Input 4 Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	3, psiAmount },		// Mixer 2 Input 4 Mod Amount
	{ ElementType::Mixer,	1, PortType::None,	0, mixOutputLevel },// Mixer 2 Output Level MSB
	{ ElementType::Mixer,	1, PortType::None,	0, mixOutputLevel },// Mixer 2 Output Level LSB
	{ ElementType::Mixer,	1, PortType::Mod,	4, psiSource },		// Mixer 2 Output Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	4, psiSource },		// Mixer 2 Output Mod Source
	{ ElementType::Mixer,	1, PortType::Mod,	4, psiAmount },		// Mixer 2 Output Mod Amount

	{ ElementType::Mixer,	2, PortType::Input, 0, psiSource },		// Mixer 3 Input 1 Source
	{ ElementType::Mixer,	2, PortType::Input, 0, psiSource },		// Mixer 3 Input 1 Source
	{ ElementType::Mixer,	2, PortType::Input, 0, psiAmount },		// Mixer 3 Input 1 Level
	{ ElementType::Mixer,	2, PortType::Mod,	0, psiSource },		// Mixer 3 Input 1 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	0, psiSource },		// Mixer 3 Input 1 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	0, psiAmount },		// Mixer 3 Input 1 Mod Amount
	{ ElementType::Mixer,	2, PortType::Input, 1, psiSource },		// Mixer 3 Input 2 Source
	{ ElementType::Mixer,	2, PortType::Input, 1, psiSource },		// Mixer 3 Input 2 Source
	{ ElementType::Mixer,	2, PortType::Input, 1, psiAmount },		// Mixer 3 Input 2 Level
	{ ElementType::Mixer,	2, PortType::Mod,	1, psiSource },		// Mixer 3 Input 2 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	1, psiSource },		// Mixer 3 Input 2 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	1, psiAmount },		// Mixer 3 Input 2 Mod Amount
	{ ElementType::Mixer,	2, PortType::Input, 2, psiSource },		// Mixer 3 Input 3 Source
	{ ElementType::Mixer,	2, PortType::Input, 2, psiSource },		// Mixer 3 Input 3 Source
	{ ElementType::Mixer,	2, PortType::Input, 2, psiAmount },		// Mixer 3 Input 3 Level
	{ ElementType::Mixer,	2, PortType::Mod,	2, psiSource },		// Mixer 3 Input 3 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	2, psiSource },		// Mixer 3 Input 3 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	2, psiAmount },		// Mixer 3 Input 3 Mod Amount
	{ ElementType::Mixer,	2, PortType::Input, 3, psiSource },		// Mixer 3 Input 4 Source
	{ ElementType::Mixer,	2, PortType::Input, 3, psiSource },		// Mixer 3 Input 4 Source
	{ ElementType::Mixer,	2, PortType::Input, 3, psiAmount },		// Mixer 3 Input 4 Level
	{ ElementType::Mixer,	2, PortType::Mod,	3, psiSource },		// Mixer 3 Input 4 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	3, psiSource },		// Mixer 3 Input 4 Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	3, psiAmount },		// Mixer 3 Input 4 Mod Amount
	{ ElementType::Mixer,	2, PortType::None,	0, mixOutputLevel },// Mixer 3 Output Level MSB
	{ ElementType::Mixer,	2, PortType::None,	0, mixOutputLevel },// Mixer 3 Output Level LSB
	{ ElementType::Mixer,	2, PortType::Mod,	4, psiSource },		// Mixer 3 Output Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	4, psiSource },		// Mixer 3 Output Mod Source
	{ ElementType::Mixer,	2, PortType::Mod,	4, psiAmount },		// Mixer 3 Output Mod Amount

	{ ElementType::Mixer,	3, PortType::Input, 0, psiSource },		// Mixer 4 Input 1 Source
	{ ElementType::Mixer,	3, PortType::Input, 0, psiSource },		// Mixer 4 Input 1 Source
	{ ElementType::Mixer,	3, PortType::Input, 0, psiAmount },		// Mixer 4 Input 1 Level
	{ ElementType::Mixer,	3, PortType::Mod,	0, psiSource },		// Mixer 4 Input 1 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	0, psiSource },		// Mixer 4 Input 1 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	0, psiAmount },		// Mixer 4 Input 1 Mod Amount
	{ ElementType::Mixer,	3, PortType::Input, 1, psiSource },		// Mixer 4 Input 2 Source
	{ ElementType::Mixer,	3, PortType::Input, 1, psiSource },		// Mixer 4 Input 2 Source
	{ ElementType::Mixer,	3, PortType::Input, 1, psiAmount },		// Mixer 4 Input 2 Level
	{ ElementType::Mixer,	3, PortType::Mod,	1, psiSource },		// Mixer 4 Input 2 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	1, psiSource },		// Mixer 4 Input 2 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	1, psiAmount },		// Mixer 4 Input 2 Mod Amount
	{ ElementType::Mixer,	3, PortType::Input, 2, psiSource },		// Mixer 4 Input 3 Source
	{ ElementType::Mixer,	3, PortType::Input, 2, psiSource },		// Mixer 4 Input 3 Source
	{ ElementType::Mixer,	3, PortType::Input, 2, psiAmount },		// Mixer 4 Input 3 Level
	{ ElementType::Mixer,	3, PortType::Mod,	2, psiSource },		// Mixer 4 Input 3 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	2, psiSource },		// Mixer 4 Input 3 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	2, psiAmount },		// Mixer 4 Input 3 Mod Amount
	{ ElementType::Mixer,	3, PortType::Input, 3, psiSource },		// Mixer 4 Input 4 Source
	{ ElementType::Mixer,	3, PortType::Input, 3, psiSource },		// Mixer 4 Input 4 Source
	{ ElementType::Mixer,	3, PortType::Input, 3, psiAmount },		// Mixer 4 Input 4 Level
	{ ElementType::Mixer,	3, PortType::Mod,	3, psiSource },		// Mixer 4 Input 4 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	3, psiSource },		// Mixer 4 Input 4 Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	3, psiAmount },		// Mixer 4 Input 4 Mod Amount
	{ ElementType::Mixer,	3, PortType::None,	0, mixOutputLevel },// Mixer 4 Output Level MSB
	{ ElementType::Mixer,	3, PortType::None,	0, mixOutputLevel },// Mixer 4 Output Level LSB
	{ ElementType::Mixer,	3, PortType::Mod,	4, psiSource },		// Mixer 4 Output Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	4, psiSource },		// Mixer 4 Output Mod Source
	{ ElementType::Mixer,	3, PortType::Mod,	4, psiAmount },		// Mixer 4 Output Mod Amount
};

uint8_t insByteCount[INS_MIDI_BLOCK_SIZE] =
{
	1, 2, 0, 1, 2, 0, 2, 0, 1, 2, 0,	// Infx 1
	1, 2, 0, 1, 2, 0, 2, 0, 1, 2, 0,
	1, 2, 0, 1, 2, 0, 2, 0, 1, 2, 0,
	1, 2, 0, 1, 2, 0, 2, 0, 1, 2, 0,	// Infx 4
};

ParamID insParamID[INS_MIDI_BLOCK_SIZE] =
{
	{ ElementType::InsertFX,	0, PortType::None,		0, insFxMode },		// InsFx 1 Mode
	{ ElementType::InsertFX,	0, PortType::Input,		0, psiSource },		// InsFx 1 Input
	{ ElementType::InsertFX,	0, PortType::Input,		0, psiSource },		// InsFx 1 Input
	{ ElementType::InsertFX,	0, PortType::None,		0, insFxValue },	// InsFx 1 Value,
	{ ElementType::InsertFX,	0, PortType::Mod,		0, psiSource },		// InsFx 1 Mod Source
	{ ElementType::InsertFX,	0, PortType::Mod,		0, psiSource },		// InsFx 1 Mod Source
	{ ElementType::InsertFX,	0, PortType::Mod,		0, psiAmount },		// InsFx 1 Mod Amount MSB
	{ ElementType::InsertFX,	0, PortType::Mod,		0, psiAmount },		// InsFx 1 Mod Amount LSB
	{ ElementType::InsertFX,	0, PortType::Control,	0, psiSource },		// InsFx 1 Control Source
	{ ElementType::InsertFX,	0, PortType::Control,	0, psiAmount },		// InsFx 1 Control Strength MSB
	{ ElementType::InsertFX,	0, PortType::Control,	0, psiAmount },		// InsFx 1 Control Strength LSB

	{ ElementType::InsertFX,	1, PortType::None,		0, insFxMode },		// InsFx 2 Mode
	{ ElementType::InsertFX,	1, PortType::Input,		0, psiSource },		// InsFx 2 Input
	{ ElementType::InsertFX,	1, PortType::Input,		0, psiSource },		// InsFx 2 Input
	{ ElementType::InsertFX,	1, PortType::None,		0, insFxValue },	// InsFx 2 Value,
	{ ElementType::InsertFX,	1, PortType::Mod,		0, psiSource },		// InsFx 2 Mod Source
	{ ElementType::InsertFX,	1, PortType::Mod,		0, psiSource },		// InsFx 2 Mod Source
	{ ElementType::InsertFX,	1, PortType::Mod,		0, psiAmount },		// InsFx 2 Mod Amount MSB
	{ ElementType::InsertFX,	1, PortType::Mod,		0, psiAmount },		// InsFx 2 Mod Amount LSB
	{ ElementType::InsertFX,	1, PortType::Control,	0, psiSource },		// InsFx 2 Control Source
	{ ElementType::InsertFX,	1, PortType::Control,	0, psiAmount },		// InsFx 2 Control Strength MSB
	{ ElementType::InsertFX,	1, PortType::Control,	0, psiAmount },		// InsFx 2 Control Strength LSB

	{ ElementType::InsertFX,	2, PortType::None,		0, insFxMode },		// InsFx 3 Mode
	{ ElementType::InsertFX,	2, PortType::Input,		0, psiSource },		// InsFx 3 Input
	{ ElementType::InsertFX,	2, PortType::Input,		0, psiSource },		// InsFx 3 Input
	{ ElementType::InsertFX,	2, PortType::None,		0, insFxValue },	// InsFx 3 Value,
	{ ElementType::InsertFX,	2, PortType::Mod,		0, psiSource },		// InsFx 3 Mod Source
	{ ElementType::InsertFX,	2, PortType::Mod,		0, psiSource },		// InsFx 3 Mod Source
	{ ElementType::InsertFX,	2, PortType::Mod,		0, psiAmount },		// InsFx 3 Mod Amount MSB
	{ ElementType::InsertFX,	2, PortType::Mod,		0, psiAmount },		// InsFx 3 Mod Amount LSB
	{ ElementType::InsertFX,	2, PortType::Control,	0, psiSource },		// InsFx 3 Control Source
	{ ElementType::InsertFX,	2, PortType::Control,	0, psiAmount },		// InsFx 3 Control Strength MSB
	{ ElementType::InsertFX,	2, PortType::Control,	0, psiAmount },		// InsFx 3 Control Strength LSB

	{ ElementType::InsertFX,	3, PortType::None,		0, insFxMode },		// InsFx 4 Mode
	{ ElementType::InsertFX,	3, PortType::Input,		0, psiSource },		// InsFx 4 Input
	{ ElementType::InsertFX,	3, PortType::Input,		0, psiSource },		// InsFx 4 Input
	{ ElementType::InsertFX,	3, PortType::None,		0, insFxValue },	// InsFx 4 Value,
	{ ElementType::InsertFX,	3, PortType::Mod,		0, psiSource },		// InsFx 4 Mod Source
	{ ElementType::InsertFX,	3, PortType::Mod,		0, psiSource },		// InsFx 4 Mod Source
	{ ElementType::InsertFX,	3, PortType::Mod,		0, psiAmount },		// InsFx 4 Mod Amount MSB
	{ ElementType::InsertFX,	3, PortType::Mod,		0, psiAmount },		// InsFx 4 Mod Amount LSB
	{ ElementType::InsertFX,	3, PortType::Control,	0, psiSource },		// InsFx 4 Control Source
	{ ElementType::InsertFX,	3, PortType::Control,	0, psiAmount },		// InsFx 4 Control Strength MSB
	{ ElementType::InsertFX,	3, PortType::Control,	0, psiAmount },		// InsFx 4 Control Strength LSB
};

uint8_t lfoByteCount[LFO_MIDI_BLOCK_SIZE] =
{
	1, 3, 0, 0, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 1, 1,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
	2, 0, 1, 2, 0, 1, 2, 0, 2, 0,
};

ParamID lfoParamID[5][LFO_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::LFO,	0, PortType::None,		0, lfoShape },
		{ ElementType::LFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	0, PortType::None,		0, lfoRateSync },
		{ ElementType::LFO,	0, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	0, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	0, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	0, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	0, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	0, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	0, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	0, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	0, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	0, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	0, PortType::None,		0, lfoClockSync },
		{ ElementType::LFO,	0, PortType::None,		0, lfoOffset },
		{ ElementType::LFO,	0, PortType::None,		0, lfoRetrigger },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	0, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	0, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiDest },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	1, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	0, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiDest },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	2, psiSource },
		{ ElementType::LFO,	0, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	0, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiDest },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiAmount },
		{ ElementType::LFO,	0, PortType::Mod,		2, psiAmount },
	},
	{
		{ ElementType::LFO,	1, PortType::None,		0, lfoShape },
		{ ElementType::LFO,	1, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	1, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	1, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	1, PortType::None,		0, lfoRateSync },
		{ ElementType::LFO,	1, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	1, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	1, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	1, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	1, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	1, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	1, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	1, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	1, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	1, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	1, PortType::None,		0, lfoClockSync },
		{ ElementType::LFO,	1, PortType::None,		0, lfoOffset },
		{ ElementType::LFO,	1, PortType::None,		0, lfoRetrigger },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	0, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	1, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiDest },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	1, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	1, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiDest },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	2, psiSource },
		{ ElementType::LFO,	1, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	1, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiDest },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiAmount },
		{ ElementType::LFO,	1, PortType::Mod,		2, psiAmount },
	},
	{
		{ ElementType::LFO,	2, PortType::None,		0, lfoShape },
		{ ElementType::LFO,	2, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	2, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	2, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	2, PortType::None,		0, lfoRateSync },
		{ ElementType::LFO,	2, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	2, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	2, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	2, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	2, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	2, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	2, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	2, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	2, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	2, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	2, PortType::None,		0, lfoClockSync },
		{ ElementType::LFO,	2, PortType::None,		0, lfoOffset },
		{ ElementType::LFO,	2, PortType::None,		0, lfoRetrigger },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	0, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	2, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiDest },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	1, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	2, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiDest },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	2, psiSource },
		{ ElementType::LFO,	2, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	2, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiDest },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiAmount },
		{ ElementType::LFO,	2, PortType::Mod,		2, psiAmount },
	},
	{
		{ ElementType::LFO,	3, PortType::None,		0, lfoShape },
		{ ElementType::LFO,	3, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	3, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	3, PortType::None,		0, lfoRate },
		{ ElementType::LFO,	3, PortType::None,		0, lfoRateSync },
		{ ElementType::LFO,	3, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	3, PortType::None,		0, lfoPhase },
		{ ElementType::LFO,	3, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	3, PortType::None,		0, lfoDelayStart },
		{ ElementType::LFO,	3, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	3, PortType::None,		0, lfoFadeIn },
		{ ElementType::LFO,	3, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	3, PortType::None,		0, lfoFadeOut },
		{ ElementType::LFO,	3, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	3, PortType::None,		0, lfoLevel },
		{ ElementType::LFO,	3, PortType::None,		0, lfoClockSync },
		{ ElementType::LFO,	3, PortType::None,		0, lfoOffset },
		{ ElementType::LFO,	3, PortType::None,		0, lfoRetrigger },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	0, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	3, PortType::Control,	0, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiDest },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		0, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	1, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	3, PortType::Control,	1, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiDest },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		1, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	2, psiSource },
		{ ElementType::LFO,	3, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	3, PortType::Control,	2, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiDest },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiAmount },
		{ ElementType::LFO,	3, PortType::Mod,		2, psiAmount },
	},
	{
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoShape },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoRate },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoRateSync },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoPhase },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoPhase },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoDelayStart },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoDelayStart },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoFadeIn },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoFadeIn },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoFadeOut },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoFadeOut },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoLevel },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoLevel },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoClockSync },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoOffset },
		{ ElementType::VibratoLFO,	0, PortType::None,		0, lfoRetrigger },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	0, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	0, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Control,	0, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiDest },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	1, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	1, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Control,	1, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiDest },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	2, psiSource },
		{ ElementType::VibratoLFO,	0, PortType::Control,	2, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Control,	2, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiDest },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiAmtPitch },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiAmount },
		{ ElementType::VibratoLFO,	0, PortType::Mod,		2, psiAmount },
	},
};

uint8_t envByteCount[ENV_MIDI_BLOCK_SIZE] =
{
	3, 0, 0, 3, 0, 0, 3, 0, 0, 2, 0, 3, 0, 0, 2, 0, 2, 0, 3, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0,
};

ParamID envParamID[6][ENV_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::EG,	0, PortType::None,		0, egDelay },
		{ ElementType::EG,	0, PortType::None,		0, egDelay },
		{ ElementType::EG,	0, PortType::None,		0, egDelay },
		{ ElementType::EG,	0, PortType::None,		0, egAttack },
		{ ElementType::EG,	0, PortType::None,		0, egAttack },
		{ ElementType::EG,	0, PortType::None,		0, egAttack },
		{ ElementType::EG,	0, PortType::None,		0, egDecay },
		{ ElementType::EG,	0, PortType::None,		0, egDecay },
		{ ElementType::EG,	0, PortType::None,		0, egDecay },
		{ ElementType::EG,	0, PortType::None,		0, egSustain },
		{ ElementType::EG,	0, PortType::None,		0, egSustain },
		{ ElementType::EG,	0, PortType::None,		0, egRelease },
		{ ElementType::EG,	0, PortType::None,		0, egRelease },
		{ ElementType::EG,	0, PortType::None,		0, egRelease },
		{ ElementType::EG,	0, PortType::None,		0, egASlope },
		{ ElementType::EG,	0, PortType::None,		0, egASlope },
		{ ElementType::EG,	0, PortType::None,		0, egDSlope },
		{ ElementType::EG,	0, PortType::None,		0, egDSlope },
		{ ElementType::EG,	0, PortType::None,		0, egSSlope },
		{ ElementType::EG,	0, PortType::None,		0, egSSlope },
		{ ElementType::EG,	0, PortType::None,		0, egSSlope },
		{ ElementType::EG,	0, PortType::None,		0, egRSlope },
		{ ElementType::EG,	0, PortType::None,		0, egRSlope },
		{ ElementType::EG,	0, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	0, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	0, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	0, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	0, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	0, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	0, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	0, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	0, PortType::None,		0, egVelocity },
		{ ElementType::EG,	0, PortType::None,		0, egVelocity },
	},
	{
		{ ElementType::EG,	1, PortType::None,		0, egDelay },
		{ ElementType::EG,	1, PortType::None,		0, egDelay },
		{ ElementType::EG,	1, PortType::None,		0, egDelay },
		{ ElementType::EG,	1, PortType::None,		0, egAttack },
		{ ElementType::EG,	1, PortType::None,		0, egAttack },
		{ ElementType::EG,	1, PortType::None,		0, egAttack },
		{ ElementType::EG,	1, PortType::None,		0, egDecay },
		{ ElementType::EG,	1, PortType::None,		0, egDecay },
		{ ElementType::EG,	1, PortType::None,		0, egDecay },
		{ ElementType::EG,	1, PortType::None,		0, egSustain },
		{ ElementType::EG,	1, PortType::None,		0, egSustain },
		{ ElementType::EG,	1, PortType::None,		0, egRelease },
		{ ElementType::EG,	1, PortType::None,		0, egRelease },
		{ ElementType::EG,	1, PortType::None,		0, egRelease },
		{ ElementType::EG,	1, PortType::None,		0, egASlope },
		{ ElementType::EG,	1, PortType::None,		0, egASlope },
		{ ElementType::EG,	1, PortType::None,		0, egDSlope },
		{ ElementType::EG,	1, PortType::None,		0, egDSlope },
		{ ElementType::EG,	1, PortType::None,		0, egSSlope },
		{ ElementType::EG,	1, PortType::None,		0, egSSlope },
		{ ElementType::EG,	1, PortType::None,		0, egSSlope },
		{ ElementType::EG,	1, PortType::None,		0, egRSlope },
		{ ElementType::EG,	1, PortType::None,		0, egRSlope },
		{ ElementType::EG,	1, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	1, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	1, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	1, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	1, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	1, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	1, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	1, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	1, PortType::None,		0, egVelocity },
		{ ElementType::EG,	1, PortType::None,		0, egVelocity }, },
	{
		{ ElementType::EG,	2, PortType::None,		0, egDelay },
		{ ElementType::EG,	2, PortType::None,		0, egDelay },
		{ ElementType::EG,	2, PortType::None,		0, egDelay },
		{ ElementType::EG,	2, PortType::None,		0, egAttack },
		{ ElementType::EG,	2, PortType::None,		0, egAttack },
		{ ElementType::EG,	2, PortType::None,		0, egAttack },
		{ ElementType::EG,	2, PortType::None,		0, egDecay },
		{ ElementType::EG,	2, PortType::None,		0, egDecay },
		{ ElementType::EG,	2, PortType::None,		0, egDecay },
		{ ElementType::EG,	2, PortType::None,		0, egSustain },
		{ ElementType::EG,	2, PortType::None,		0, egSustain },
		{ ElementType::EG,	2, PortType::None,		0, egRelease },
		{ ElementType::EG,	2, PortType::None,		0, egRelease },
		{ ElementType::EG,	2, PortType::None,		0, egRelease },
		{ ElementType::EG,	2, PortType::None,		0, egASlope },
		{ ElementType::EG,	2, PortType::None,		0, egASlope },
		{ ElementType::EG,	2, PortType::None,		0, egDSlope },
		{ ElementType::EG,	2, PortType::None,		0, egDSlope },
		{ ElementType::EG,	2, PortType::None,		0, egSSlope },
		{ ElementType::EG,	2, PortType::None,		0, egSSlope },
		{ ElementType::EG,	2, PortType::None,		0, egSSlope },
		{ ElementType::EG,	2, PortType::None,		0, egRSlope },
		{ ElementType::EG,	2, PortType::None,		0, egRSlope },
		{ ElementType::EG,	2, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	2, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	2, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	2, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	2, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	2, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	2, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	2, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	2, PortType::None,		0, egVelocity },
		{ ElementType::EG,	2, PortType::None,		0, egVelocity }, },
	{
		{ ElementType::EG,	3, PortType::None,		0, egDelay },
		{ ElementType::EG,	3, PortType::None,		0, egDelay },
		{ ElementType::EG,	3, PortType::None,		0, egDelay },
		{ ElementType::EG,	3, PortType::None,		0, egAttack },
		{ ElementType::EG,	3, PortType::None,		0, egAttack },
		{ ElementType::EG,	3, PortType::None,		0, egAttack },
		{ ElementType::EG,	3, PortType::None,		0, egDecay },
		{ ElementType::EG,	3, PortType::None,		0, egDecay },
		{ ElementType::EG,	3, PortType::None,		0, egDecay },
		{ ElementType::EG,	3, PortType::None,		0, egSustain },
		{ ElementType::EG,	3, PortType::None,		0, egSustain },
		{ ElementType::EG,	3, PortType::None,		0, egRelease },
		{ ElementType::EG,	3, PortType::None,		0, egRelease },
		{ ElementType::EG,	3, PortType::None,		0, egRelease },
		{ ElementType::EG,	3, PortType::None,		0, egASlope },
		{ ElementType::EG,	3, PortType::None,		0, egASlope },
		{ ElementType::EG,	3, PortType::None,		0, egDSlope },
		{ ElementType::EG,	3, PortType::None,		0, egDSlope },
		{ ElementType::EG,	3, PortType::None,		0, egSSlope },
		{ ElementType::EG,	3, PortType::None,		0, egSSlope },
		{ ElementType::EG,	3, PortType::None,		0, egSSlope },
		{ ElementType::EG,	3, PortType::None,		0, egRSlope },
		{ ElementType::EG,	3, PortType::None,		0, egRSlope },
		{ ElementType::EG,	3, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	3, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	3, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	3, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	3, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	3, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	3, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	3, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	3, PortType::None,		0, egVelocity },
		{ ElementType::EG,	3, PortType::None,		0, egVelocity }, },
	{
		{ ElementType::EG,	4, PortType::None,		0, egDelay },
		{ ElementType::EG,	4, PortType::None,		0, egDelay },
		{ ElementType::EG,	4, PortType::None,		0, egDelay },
		{ ElementType::EG,	4, PortType::None,		0, egAttack },
		{ ElementType::EG,	4, PortType::None,		0, egAttack },
		{ ElementType::EG,	4, PortType::None,		0, egAttack },
		{ ElementType::EG,	4, PortType::None,		0, egDecay },
		{ ElementType::EG,	4, PortType::None,		0, egDecay },
		{ ElementType::EG,	4, PortType::None,		0, egDecay },
		{ ElementType::EG,	4, PortType::None,		0, egSustain },
		{ ElementType::EG,	4, PortType::None,		0, egSustain },
		{ ElementType::EG,	4, PortType::None,		0, egRelease },
		{ ElementType::EG,	4, PortType::None,		0, egRelease },
		{ ElementType::EG,	4, PortType::None,		0, egRelease },
		{ ElementType::EG,	4, PortType::None,		0, egASlope },
		{ ElementType::EG,	4, PortType::None,		0, egASlope },
		{ ElementType::EG,	4, PortType::None,		0, egDSlope },
		{ ElementType::EG,	4, PortType::None,		0, egDSlope },
		{ ElementType::EG,	4, PortType::None,		0, egSSlope },
		{ ElementType::EG,	4, PortType::None,		0, egSSlope },
		{ ElementType::EG,	4, PortType::None,		0, egSSlope },
		{ ElementType::EG,	4, PortType::None,		0, egRSlope },
		{ ElementType::EG,	4, PortType::None,		0, egRSlope },
		{ ElementType::EG,	4, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	4, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	4, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	4, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	4, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	4, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	4, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	4, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	4, PortType::None,		0, egVelocity },
		{ ElementType::EG,	4, PortType::None,		0, egVelocity }, },
	{
		{ ElementType::EG,	5, PortType::None,		0, egDelay },
		{ ElementType::EG,	5, PortType::None,		0, egDelay },
		{ ElementType::EG,	5, PortType::None,		0, egDelay },
		{ ElementType::EG,	5, PortType::None,		0, egAttack },
		{ ElementType::EG,	5, PortType::None,		0, egAttack },
		{ ElementType::EG,	5, PortType::None,		0, egAttack },
		{ ElementType::EG,	5, PortType::None,		0, egDecay },
		{ ElementType::EG,	5, PortType::None,		0, egDecay },
		{ ElementType::EG,	5, PortType::None,		0, egDecay },
		{ ElementType::EG,	5, PortType::None,		0, egSustain },
		{ ElementType::EG,	5, PortType::None,		0, egSustain },
		{ ElementType::EG,	5, PortType::None,		0, egRelease },
		{ ElementType::EG,	5, PortType::None,		0, egRelease },
		{ ElementType::EG,	5, PortType::None,		0, egRelease },
		{ ElementType::EG,	5, PortType::None,		0, egASlope },
		{ ElementType::EG,	5, PortType::None,		0, egASlope },
		{ ElementType::EG,	5, PortType::None,		0, egDSlope },
		{ ElementType::EG,	5, PortType::None,		0, egDSlope },
		{ ElementType::EG,	5, PortType::None,		0, egSSlope },
		{ ElementType::EG,	5, PortType::None,		0, egSSlope },
		{ ElementType::EG,	5, PortType::None,		0, egSSlope },
		{ ElementType::EG,	5, PortType::None,		0, egRSlope },
		{ ElementType::EG,	5, PortType::None,		0, egRSlope },
		{ ElementType::EG,	5, PortType::Mod,		0, psiSource },
		{ ElementType::EG,	5, PortType::Mod,		1, psiSource },
		{ ElementType::EG,	5, PortType::Mod,		2, psiSource },
		{ ElementType::EG,	5, PortType::Mod,		3, psiSource },
		{ ElementType::EG,	5, PortType::Mod,		0, psiAmount },
		{ ElementType::EG,	5, PortType::Mod,		1, psiAmount },
		{ ElementType::EG,	5, PortType::Mod,		2, psiAmount },
		{ ElementType::EG,	5, PortType::Mod,		3, psiAmount },
		{ ElementType::EG,	5, PortType::None,		0, egVelocity },
		{ ElementType::EG,	5, PortType::None,		0, egVelocity }, },
};

uint8_t legByteCount[LEG_MIDI_BLOCK_SIZE] =
{
	1, 1, 2, 0, 1, 2, 0, 1, 1, 2, 0, 1, 2, 0,
	3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,  3, 0, 0, 1, 2, 0, 2, 0,
};

ParamID legParamID[LEG_MIDI_BLOCK_SIZE] =
{
	{ ElementType::LoopEG,	0, PortType::None,	0, legStart },
	{ ElementType::LoopEG,	0, PortType::None,	0, legKeyOff },
	{ ElementType::LoopEG,	0, PortType::None,	0, legSlope },
	{ ElementType::LoopEG,	0, PortType::None,	0, legSlope },
	{ ElementType::LoopEG,	0, PortType::None,	0, legSync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legRepeat },
	{ ElementType::LoopEG,	0, PortType::None,	0, legRepeat },
	{ ElementType::LoopEG,	0, PortType::None,	0, legLoop },
	{ ElementType::LoopEG,	0, PortType::Mod,	0, psiSource },
	{ ElementType::LoopEG,	0, PortType::Mod,	0, psiAmount },
	{ ElementType::LoopEG,	0, PortType::Mod,	0, psiAmount },
	{ ElementType::LoopEG,	0, PortType::Mod,	1, psiSource },
	{ ElementType::LoopEG,	0, PortType::Mod,	1, psiAmount },
	{ ElementType::LoopEG,	0, PortType::Mod,	1, psiAmount },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime1Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel1 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel1 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime2Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel2 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel2 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime3Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel3 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel3 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime4Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel4 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel4 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime5Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel5 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel5 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime6Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel6 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel6 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime7Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel7 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel7 },

	{ ElementType::LoopEG,	0, PortType::None,	0, legTime8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legTime8Sync },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legXLevel8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel8 },
	{ ElementType::LoopEG,	0, PortType::None,	0, legYLevel8 },
};

uint8_t seqByteCount[SEQ_MIDI_BLOCK_SIZE] =
{
	1, 1, 2, 0, 1, 1, 1, 1, 1
};

ParamID seqParamID[SEQ_MIDI_BLOCK_SIZE] =
{
	{ ElementType::SeqGlobal,	0, PortType::None,	0, seqMode },
	{ ElementType::SeqGlobal,	0, PortType::None,	0, seqPattern },
	{ ElementType::SeqGlobal,	0, PortType::None,	0, seqSwing },
	{ ElementType::SeqGlobal,	0, PortType::None,	0, seqSwing },
	{ ElementType::SeqGlobal,	0, PortType::None,	0, seqDivision },

	{ ElementType::Seq,	0, PortType::None,	0, seqPatternLength },
	{ ElementType::Seq,	1, PortType::None,	0, seqPatternLength },
	{ ElementType::Seq,	2, PortType::None,	0, seqPatternLength },
	{ ElementType::Seq,	3, PortType::None,	0, seqPatternLength },
};

uint8_t seqStepByteCount[SEQSTEP_MIDI_BLOCK_SIZE] =
{
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
};

ParamID seqStepParamID[SEQSTEP_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Seq,	0, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep16 },
	{ ElementType::Seq,	0, PortType::None,	0, seqStep16 },

	{ ElementType::Seq,	1, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep16 },
	{ ElementType::Seq,	1, PortType::None,	0, seqStep16 },

	{ ElementType::Seq,	2, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep16 },
	{ ElementType::Seq,	2, PortType::None,	0, seqStep16 },

	{ ElementType::Seq,	3, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep1 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep2 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep3 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep4 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep5 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep6 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep7 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep8 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep9 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep10 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep11 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep12 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep13 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep14 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep15 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep16 },
	{ ElementType::Seq,	3, PortType::None,	0, seqStep16 },

};

uint8_t namByteCount[NAME_MIDI_BLOCK_SIZE] =
{
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,	// name
	1, 1,	// categories
};

ParamID namParamID[NAME_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Home,		0, PortType::None,	0, gloName1 },		// Preset Name 1
	{ ElementType::Home,		0, PortType::None,	0, gloName1 },		// Preset Name 1
	{ ElementType::Home,		0, PortType::None,	0, gloName2 },		// Preset Name 2
	{ ElementType::Home,		0, PortType::None,	0, gloName2 },		// Preset Name 2
	{ ElementType::Home,		0, PortType::None,	0, gloName3 },		// Preset Name 3
	{ ElementType::Home,		0, PortType::None,	0, gloName3 },		// Preset Name 3
	{ ElementType::Home,		0, PortType::None,	0, gloName4 },		// Preset Name 4
	{ ElementType::Home,		0, PortType::None,	0, gloName4 },		// Preset Name 4
	{ ElementType::Home,		0, PortType::None,	0, gloName5 },		// Preset Name 5
	{ ElementType::Home,		0, PortType::None,	0, gloName5 },		// Preset Name 5
	{ ElementType::Home,		0, PortType::None,	0, gloName6 },		// Preset Name 6
	{ ElementType::Home,		0, PortType::None,	0, gloName6 },		// Preset Name 6
	{ ElementType::Home,		0, PortType::None,	0, gloName7 },		// Preset Name 7
	{ ElementType::Home,		0, PortType::None,	0, gloName7 },		// Preset Name 7
	{ ElementType::Home,		0, PortType::None,	0, gloName8 },		// Preset Name 8
	{ ElementType::Home,		0, PortType::None,	0, gloName8 },		// Preset Name 8
	{ ElementType::Home,		0, PortType::None,	0, gloName9 },		// Preset Name 9
	{ ElementType::Home,		0, PortType::None,	0, gloName9 },		// Preset Name 9
	{ ElementType::Home,		0, PortType::None,	0, gloName10 },		// Preset Name 10
	{ ElementType::Home,		0, PortType::None,	0, gloName10 },		// Preset Name 10
	{ ElementType::Home,		0, PortType::None,	0, gloName11 },		// Preset Name 11
	{ ElementType::Home,		0, PortType::None,	0, gloName11 },		// Preset Name 11
	{ ElementType::Home,		0, PortType::None,	0, gloName12 },		// Preset Name 12
	{ ElementType::Home,		0, PortType::None,	0, gloName12 },		// Preset Name 12
	{ ElementType::Home,		0, PortType::None,	0, gloName13 },		// Preset Name 13
	{ ElementType::Home,		0, PortType::None,	0, gloName13 },		// Preset Name 13
	{ ElementType::Home,		0, PortType::None,	0, gloName14 },		// Preset Name 14
	{ ElementType::Home,		0, PortType::None,	0, gloName14 },		// Preset Name 14
	{ ElementType::Home,		0, PortType::None,	0, gloName15 },		// Preset Name 15
	{ ElementType::Home,		0, PortType::None,	0, gloName15 },		// Preset Name 15
	{ ElementType::Home,		0, PortType::None,	0, gloName16 },		// Preset Name 16
	{ ElementType::Home,		0, PortType::None,	0, gloName16 },		// Preset Name 16
	{ ElementType::Home,		0, PortType::None,	0, gloName17 },		// Preset Name 17
	{ ElementType::Home,		0, PortType::None,	0, gloName17 },		// Preset Name 17
	{ ElementType::Home,		0, PortType::None,	0, gloName18 },		// Preset Name 18
	{ ElementType::Home,		0, PortType::None,	0, gloName18 },		// Preset Name 18
	{ ElementType::Home,		0, PortType::None,	0, gloName19 },		// Preset Name 19
	{ ElementType::Home,		0, PortType::None,	0, gloName19 },		// Preset Name 19
	{ ElementType::Home,		0, PortType::None,	0, gloName20 },		// Preset Name 20
	{ ElementType::Home,		0, PortType::None,	0, gloName20 },		// Preset Name 20

	{ ElementType::Home,		0, PortType::None,	0, gloCategory1 },		// Preset Category 1
	{ ElementType::Home,		0, PortType::None,	0, gloCategory2 },		// Preset Category 2
};

uint8_t comByteCount[COM_MIDI_BLOCK_SIZE] =
{
	3, 0, 0, 3, 0, 0, 3, 0, 0, 3, 0, 0, 3, 0, 0, // performance controls. handled separately
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0,				// performance knob values
	1, 1, 1, 2, 0, 2, 0, 2, 0, 1, 1, 1, 1,
	3, 0, 0, 2, 0, 2, 0, 1, 1, 1, 1, 1, 2, 0, 1, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1, 1, 1, 1, 1,
	2, 1,
	1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0
};

ParamID comParamID[COM_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob1Src },		// Performance Knob 1 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob1Src },		// Performance Knob 1 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob1Src },		// Performance Knob 1 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob2Src },		// Performance Knob 2 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob2Src },		// Performance Knob 2 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob2Src },		// Performance Knob 2 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob3Src },		// Performance Knob 3 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob3Src },		// Performance Knob 3 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob3Src },		// Performance Knob 3 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob4Src },		// Performance Knob 4 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob4Src },		// Performance Knob 4 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob4Src },		// Performance Knob 4 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob5Src },		// Performance Knob 5 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob5Src },		// Performance Knob 5 Target
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob5Src },		// Performance Knob 5 Target

	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob1Amt },		// Performance Knob 1 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob1Amt },		// Performance Knob 1 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob2Amt },		// Performance Knob 2 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob2Amt },		// Performance Knob 2 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob3Amt },		// Performance Knob 3 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob3Amt },		// Performance Knob 3 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob4Amt },		// Performance Knob 4 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob4Amt },		// Performance Knob 4 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob5Amt },		// Performance Knob 5 Value
	{ ElementType::Home,		0, PortType::None,	0, gloPerfKnob5Amt },		// Performance Knob 5 Value

	{ ElementType::Home,		0, PortType::None,	0, gloPitchWheelUpRange },
	{ ElementType::Home,		0, PortType::None,	0, gloPitchWheelDownRange },
	{ ElementType::VibratoLFO,	0, PortType::None,	0, vlfoModWheel },
	{ ElementType::VibratoLFO,	0, PortType::None,	0, vlfoModWheelMax },
	{ ElementType::VibratoLFO,	0, PortType::None,	0, vlfoModWheelMax },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribOffset },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribOffset },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribIntensity },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribIntensity },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribHold },
	{ ElementType::Ribbon,		0, PortType::None,	0, ribTouchOffset },
	{ ElementType::Home,		0, PortType::None,	0, gloGldType },
	{ ElementType::Home,		0, PortType::None,	0, gloGldMode },

	{ ElementType::Home,		0, PortType::None,	0, gloGldTime },
	{ ElementType::Home,		0, PortType::None,	0, gloGldTime },
	{ ElementType::Home,		0, PortType::None,	0, gloGldTime },
	{ ElementType::Home,		0, PortType::None,	0, gloGldRate },
	{ ElementType::Home,		0, PortType::None,	0, gloGldRate },
	{ ElementType::Home,		0, PortType::None,	0, gloGldRange },
	{ ElementType::Home,		0, PortType::None,	0, gloGldRange },
	{ ElementType::Home,		0, PortType::None,	0, gloPlayMode },
	{ ElementType::Home,		0, PortType::None,	0, gloLegato },
	{ ElementType::Home,		0, PortType::None,	0, gloNotePriority },
	{ ElementType::Home,		0, PortType::None,	0, gloEgReset },
	{ ElementType::Home,		0, PortType::None,	0, gloUniVoice },
	{ ElementType::Home,		0, PortType::None,	0, gloUniTune },
	{ ElementType::Home,		0, PortType::None,	0, gloUniTune },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonButton },
	{ ElementType::Home,		0, PortType::None,	0, gloRndTune },
	{ ElementType::Home,		0, PortType::None,	0, gloRndTune },
	{ ElementType::Home,		0, PortType::None,	0, gloArpButton },
	{ ElementType::Home,		0, PortType::None,	0, gloSeqButton },
	{ ElementType::Home,		0, PortType::None,	0, gloExpPedal },
	{ ElementType::Home,		0, PortType::None,	0, gloSusPedal1 },
	{ ElementType::Home,		0, PortType::None,	0, gloSusPedal2 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignFunc1 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignFunc2 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignMode1 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignMode2 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignState1 },
	{ ElementType::Home,		0, PortType::None,	0, gloAssignState2 },
	{ ElementType::Home,		0, PortType::None,	0, gloBPM },
	{ ElementType::Home,		0, PortType::None,	0, gloBPM },
	{ ElementType::Home,		0, PortType::None,	0, gloVTIntens },
	{ ElementType::Home,		0, PortType::None,	0, gloVTIntens },
	{ ElementType::Home,		0, PortType::None,	0, gloVTOffset },
	{ ElementType::Home,		0, PortType::None,	0, gloVTOffset },
	{ ElementType::Home,		0, PortType::None,	0, gloATIntens },
	{ ElementType::Home,		0, PortType::None,	0, gloATIntens },
	{ ElementType::Home,		0, PortType::None,	0, gloATOffset },
	{ ElementType::Home,		0, PortType::None,	0, gloATOffset },

	{ ElementType::Output,		0, PortType::Input,	0, psiSource },
	{ ElementType::Output,		1, PortType::Input,	0, psiSource },
	{ ElementType::Output,		2, PortType::Input,	0, psiSource },
	{ ElementType::Output,		3, PortType::Input,	0, psiSource },
	{ ElementType::SPDIF_Out,	0, PortType::Input,	0, psiSource },
	{ ElementType::Home,		0, PortType::None,	0, gloSamplePool },
	{ ElementType::Home,		0, PortType::None,	0, gloSamplePool },
	{ ElementType::Home,		0, PortType::None,	0, gloPatchChain },

	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNumberOfNotes },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote0 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote0 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote1 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote1 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote2 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote2 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote3 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote3 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote4 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote4 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote5 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote5 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote6 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote6 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote7 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote7 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote8 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote8 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote9 },
	{ ElementType::Home,		0, PortType::None,	0, gloUnisonStackNote9 },
};

uint8_t effByteCount[EFF_MIDI_BLOCK_SIZE] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,					// Chorus/flanger
	1, 2, 0, 2, 0, 2, 0, 3, 0, 0, 2, 0, 2, 0, 2, 0, 2, 0, 				// Phaser
	1, 3, 0, 0, 3, 0, 0, 1, 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1,			// Delay
	1, 3, 0, 0, 2, 0, 2, 0, 3, 0, 0, 2, 0, 2, 0, 3, 0, 0, 2, 0, 2, 0	// EQ
};

ParamID effParamID[EFF_MIDI_BLOCK_SIZE] =
{
	{ ElementType::FXChannel,	0, PortType::Input,	0, psiSource },
	{ ElementType::FXChannel,	0, PortType::None,	0, fxChannelSlot1 },
	{ ElementType::FXChannel,	0, PortType::None,	0, fxChannelSlot2 },
	{ ElementType::FXChannel,	0, PortType::None,	0, fxChannelSlot3 },
	{ ElementType::FXChannel,	0, PortType::None,	0, fxChannelSlot4 },
	{ ElementType::FXChannel,	1, PortType::Input,	0, psiSource },
	{ ElementType::FXChannel,	1, PortType::None,	0, fxChannelSlot1 },
	{ ElementType::FXChannel,	1, PortType::None,	0, fxChannelSlot2 },
	{ ElementType::FXChannel,	1, PortType::None,	0, fxChannelSlot3 },
	{ ElementType::FXChannel,	1, PortType::None,	0, fxChannelSlot4 },
	{ ElementType::FXChannel,	2, PortType::Input,	0, psiSource },
	{ ElementType::FXChannel,	2, PortType::None,	0, fxChannelSlot1 },
	{ ElementType::FXChannel,	2, PortType::None,	0, fxChannelSlot2 },
	{ ElementType::FXChannel,	2, PortType::None,	0, fxChannelSlot3 },
	{ ElementType::FXChannel,	2, PortType::None,	0, fxChannelSlot4 },
	{ ElementType::FXChannel,	3, PortType::Input,	0, psiSource },
	{ ElementType::FXChannel,	3, PortType::None,	0, fxChannelSlot1 },
	{ ElementType::FXChannel,	3, PortType::None,	0, fxChannelSlot2 },
	{ ElementType::FXChannel,	3, PortType::None,	0, fxChannelSlot3 },
	{ ElementType::FXChannel,	3, PortType::None,	0, fxChannelSlot4 },

	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpMode },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpFrequency },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpFrequency },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpDepth },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpDepth },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpPhase },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpPhase },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpOffset },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpOffset },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpInLevel },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpInLevel },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpFeedback },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpFeedback },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpDry },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpDry },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpWet },
	{ ElementType::ChorusFlanger,	0, PortType::None,	0, cfpWet },

	{ ElementType::Phaser,	0, PortType::None,	0, cfpMode },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpFrequency },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpFrequency },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpDepth },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpDepth },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpPhase },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpPhase },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpOffset },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpOffset },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpOffset },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpInLevel },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpInLevel },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpFeedback },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpFeedback },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpDry },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpDry },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpWet },
	{ ElementType::Phaser,	0, PortType::None,	0, cfpWet },

	{ ElementType::Delay,	0, PortType::None,	0, delMode },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsL },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsL },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsL },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsR },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsR },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeSecondsR },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeBeatsL },
	{ ElementType::Delay,	0, PortType::None,	0, delTimeBeatsR },
	{ ElementType::Delay,	0, PortType::None,	0, delFeedL },
	{ ElementType::Delay,	0, PortType::None,	0, delFeedL },
	{ ElementType::Delay,	0, PortType::None,	0, delFeedR },
	{ ElementType::Delay,	0, PortType::None,	0, delFeedR },
	{ ElementType::Delay,	0, PortType::None,	0, delDamping },
	{ ElementType::Delay,	0, PortType::None,	0, delDamping },
	{ ElementType::Delay,	0, PortType::None,	0, delDry },
	{ ElementType::Delay,	0, PortType::None,	0, delDry },
	{ ElementType::Delay,	0, PortType::None,	0, delWet },
	{ ElementType::Delay,	0, PortType::None,	0, delWet },
	{ ElementType::Delay,	0, PortType::None,	0, delMidiClk },

	{ ElementType::EQ,	0, PortType::None,	0, eqMode },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain1 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain1 },

	{ ElementType::EQ,	0, PortType::None,	0, eqFreq2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain2 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain2 },

	{ ElementType::EQ,	0, PortType::None,	0, eqFreq3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqFreq3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqQ3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain3 },
	{ ElementType::EQ,	0, PortType::None,	0, eqGain3 },
};

uint8_t arpByteCount[ARP_MIDI_BLOCK_SIZE] =
{
	1, 1, 1, 2, 0, 1, 1, 1, 1, 1
};

ParamID arpParamID[ARP_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpMode },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpResolution },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpPattern },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpSwing },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpSwing },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpOctave },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpVelocity },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpNoteLength },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpPatternLength },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, arpHold },
};

uint8_t arpStepByteCount[ARPSTEP_MIDI_BLOCK_SIZE] =
{
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
};

ParamID arpStepParamID[ARPSTEP_MIDI_BLOCK_SIZE] =
{
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 0 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 0 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 1 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 1 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 2 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 2 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 3 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 3 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 4 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 4 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 5 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 5 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 6 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 6 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 7 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 7 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 8 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 8 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 9 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 9 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 10 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 10 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 11 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 11 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 12 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 12 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 13 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 13 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 14 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 14 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 15 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 15 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 16 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 16 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 17 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 17 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 18 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 18 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 19 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 19 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 20 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 20 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 21 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 21 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 22 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 22 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 23 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 23 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 24 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 24 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 25 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 25 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 26 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 26 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 27 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 27 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 28 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 28 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 29 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 29 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 30 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 30 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 31 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 31 },

	{ ElementType::Arpeggiator,	0, PortType::None,	0, 32 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 33 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 34 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 35 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 36 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 37 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 38 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 39 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 40 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 41 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 42 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 43 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 44 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 45 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 46 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 47 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 48 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 49 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 40 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 51 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 52 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 53 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 54 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 55 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 56 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 57 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 58 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 59 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 50 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 61 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 62 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 63 },

	{ ElementType::Arpeggiator,	0, PortType::None,	0, 64 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 65 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 66 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 67 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 68 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 69 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 70 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 71 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 72 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 73 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 74 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 75 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 76 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 77 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 78 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 79 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 80 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 81 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 82 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 83 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 84 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 85 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 86 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 87 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 88 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 89 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 90 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 91 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 92 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 93 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 94 },
	{ ElementType::Arpeggiator,	0, PortType::None,	0, 95 },
};

uint8_t keyByteCount[KEY_MIDI_BLOCK_SIZE] =
{
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0,
	2, 0, 2, 0, 2, 0, 2, 0,
};

ParamID keyParamID[8][KEY_MIDI_BLOCK_SIZE] =
{
	{
		{ ElementType::KeyTable,	0, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 63 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 63 }
	},

	{
		{ ElementType::KeyTable,	0, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 127 },
		{ ElementType::KeyTable,	0, PortType::None, 0, 127 }
	},

	{
		{ ElementType::KeyTable,	1, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 63 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 63 }
	},

	{
		{ ElementType::KeyTable,	1, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 127 },
		{ ElementType::KeyTable,	1, PortType::None, 0, 127 }
	},

	{
		{ ElementType::KeyTable,	2, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 63 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 63 }
	},

	{
		{ ElementType::KeyTable,	2, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 127 },
		{ ElementType::KeyTable,	2, PortType::None, 0, 127 }
	},

	{
		{ ElementType::KeyTable,	3, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 0 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 1 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 2 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 3 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 4 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 5 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 6 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 7 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 8 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 9 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 10 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 11 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 12 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 13 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 14 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 15 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 16 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 17 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 18 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 19 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 20 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 21 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 22 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 23 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 24 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 25 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 26 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 27 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 28 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 29 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 30 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 31 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 32 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 33 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 34 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 35 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 36 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 37 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 38 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 39 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 40 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 41 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 42 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 43 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 44 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 45 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 46 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 47 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 48 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 49 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 50 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 51 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 52 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 53 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 54 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 55 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 56 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 57 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 58 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 59 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 60 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 61 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 62 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 63 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 63 }
	},

	{
		{ ElementType::KeyTable,	3, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 64 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 65 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 66 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 67 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 68 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 69 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 70 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 71 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 72 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 73 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 74 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 75 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 76 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 77 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 78 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 79 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 80 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 81 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 82 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 83 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 84 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 85 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 86 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 87 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 88 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 89 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 90 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 91 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 92 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 93 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 94 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 95 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 96 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 97 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 98 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 99 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 100 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 101 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 102 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 103 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 104 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 105 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 106 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 107 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 108 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 109 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 110 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 111 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 112 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 113 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 114 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 115 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 116 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 117 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 118 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 119 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 120 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 121 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 122 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 123 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 124 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 125 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 126 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 127 },
		{ ElementType::KeyTable,	3, PortType::None, 0, 127 }
	},

};

// These are used for consistency. There really aren't any bytes in a frame message.
uint8_t frameByteCount[1] = { 0 };
ParamID frameParamID[1] = {  };