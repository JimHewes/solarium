/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SETTINGSPANEL_H_INCLUDED
#define SETTINGSPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include "Preset.h"
#include "ViewModel.h"
#include "TimeSlider.h"
#include "Notifier.h"
#include "Slider2.h"

class ElementView;
class CommandHistory;


class SliderCutoff : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
};

class SliderInitPan : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class SliderDeviceID : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class SettingsPanel : public Component, public Slider2Listener
{
public:
	SettingsPanel(Preset* pPreset, CommandHistory* pCommandHistory, ViewModel* pViewModel, SelectionModel<ElementID>* pSelectionModel);
	virtual ~SettingsPanel();
	void sliderValueChanged(Slider2 *slider) override;

	void resized() override;
	void paint(Graphics& g) override;
	void OnSelectionChanged();

	Notifier<void(double zoom)> ZoomChangedEvent;
	using ZoomChangedConnection = decltype(ZoomChangedEvent)::ConnectionType;	///< ZoomChangedEvent.Connect() returns this type

private:
	Label							m_titleLabel;
	Label							m_wiresLabel;
	Slider2							m_wireSlider;
	Label							m_zoomLabel;
	Slider2							m_zoomSlider;
	std::unique_ptr<Component>		m_pPanel;
	Viewport						m_viewport;
	Preset*							m_pPreset;
	CommandHistory*					m_pCommandHistory;
	ViewModel*						m_pViewModel;
	SelectionModel<ElementID>*		m_pSelectionModel;
	SelectionModel<ElementID>::SelectionChangedConnection	m_selectionConnection;
};


class MultiSelectPanel : public Component
{
public:
	MultiSelectPanel(std::vector<std::string>& elementNames);
	void paint(Graphics& g) override;
private:
	std::vector<Label>		m_elemLabels;
};

class AMPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener
{
public:
	AMPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~AMPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	Label			m_algorithmLabel;
	ComboBox		m_algorithmCombo;
	Label			m_offsetLabel;
	SliderProp		m_offsetSlider;
	
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};

class VectorPanel :
	public Component,
	public Slider2::Listener
{
public:
	VectorPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~VectorPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*				m_pPreset;
	CommandHistory*		m_pCommandHistory;
	ElementID			m_elemID;
	Label				m_xOffsetLabel;
	SliderProp			m_xOffsetSlider;
	Label				m_yOffsetLabel;
	SliderProp			m_yOffsetSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};

class MixerPanel :
	public Component,
	public Slider2::Listener
{
public:
	MixerPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~MixerPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	Label			m_OutputLevelLabel;
	SliderProp		m_OutputLevelSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


class FilterPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener
{
public:
	FilterPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~FilterPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*				m_pPreset;
	CommandHistory*		m_pCommandHistory;
	ElementID			m_elemID;
	Label				m_filterTypeLabel;
	ComboBox			m_filterTypeCombo;
	Label				m_modeLabel;
	std::vector<ComboBox>	m_modeCombos;
	//ComboBox			m_modeCombo;
	Label				m_cutoffLabel;
	SliderCutoff		m_cutoffSlider;
	Label				m_resonanceLabel;
	SliderProp			m_resonanceSlider;
	Label				m_dampingLabel;
	SliderProp			m_dampingSlider;
	Label				m_xFadeLabel;
	SliderProp			m_xFadeSlider;
	Label				m_keytrackingLabel;
	SliderProp			m_keytrackingSlider;
	Label				m_keycenterLabel;
	SliderProp			m_keycenterSlider;

	using LabelUPtr = std::unique_ptr<juce::Label>;
	using ComboBoxUPtr = std::unique_ptr<juce::ComboBox>;
	std::vector<LabelUPtr>		m_vowelLabels;
	std::vector<ComboBoxUPtr>	m_vowelComboboxes;
	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};


class InsertFXPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener
{
public:
	InsertFXPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~InsertFXPanel();
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	Label			m_modeLabel;
	ComboBox		m_modeCombo;
	Label			m_valueLabel;
	SliderProp		m_valueSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};

class VCAPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener
{
public:
	VCAPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~VCAPanel();
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_typeLabel;
	ComboBox		m_typeCombo;
	Label			m_boostLabel;
	SliderProp		m_boostSlider;
	Label			m_levelLabel;
	SliderProp		m_levelSlider;
	Label			m_panLabel;
	SliderInitPan	m_panSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


class LagPanel :
	public Component,
	public Slider2::Listener
{
public:
	LagPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~LagPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_timeLabel;
	SliderTime		m_timeSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


class EnvFolPanel :
	public Component,
	public Slider2::Listener
{
public:
	EnvFolPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~EnvFolPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_attackLabel;
	SliderTime		m_attackSlider;
	Label			m_releaseLabel;
	SliderTime		m_releaseSlider;
	Label			m_inputLabel;
	SliderProp		m_inputSlider;
	Label			m_outputLabel;
	SliderProp		m_outputSlider;
	Preset::ParameterChangedConnection		m_paramChangedConnection;
};


class RibbonPanel :
	public Component,
	public Slider2::Listener,
	public Button::Listener
{
public:
	RibbonPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~RibbonPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	Label			m_offsetLabel;
	SliderProp		m_offsetSlider;
	Label			m_intensityLabel;
	SliderProp		m_intensitySlider;
	ToggleButton	m_holdButton;
	ToggleButton	m_touchOffButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};

class RndTuneSlider : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class GlobalAtVelSlider : public SliderProp
{
public:
	String getTextFromValue(double value) override;
};

class HomePanel :
	public Component,
	public Slider2::Listener,
	public ComboBox::Listener,
	public Button::Listener,
	public TextEditor::Listener
{
public:
	HomePanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID);
	virtual ~HomePanel();
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void sliderValueChanged(Slider2* slider) override;
	void buttonClicked(Button* button) override;
	void textEditorReturnKeyPressed(TextEditor &) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_glideTypeLabel;
	ComboBox		m_glideTypeCombo;
	Label			m_glideModeLabel;
	ComboBox		m_glideModeCombo;
	Label			m_glideTimeLabel;
	SliderTime		m_glideTimeSlider;
	Label			m_glideRateLabel;
	SliderProp		m_glideRateSlider;
	Label			m_glideRangeLabel;
	SliderProp		m_glideRangeSlider;
	Label			m_playModeLabel;
	ComboBox		m_playModeCombo;
	Label			m_legatoLabel;
	ComboBox		m_legatoCombo;
	Label			m_uniVoiceLabel;
	ComboBox		m_uniVoiceCombo;
	Label			m_uniTuneLabel;
	SliderProp		m_uniTuneSlider;
	Label			m_rndTuneLabel;
	RndTuneSlider	m_rndTuneSlider;
	Label			m_notePriLabel;
	ComboBox		m_notePriCombo;
	Label			m_egResetLabel;
	ComboBox		m_egResetCombo;
	Label			m_pitchWheelUpLabel;
	SliderProp		m_pitchWheelUpSlider;
	Label			m_pitchWheelDownLabel;
	SliderProp		m_pitchWheelDownSlider;
	Label			m_vtIntensityLabel;
	SliderProp		m_vtIntensitySlider;
	Label			m_vtOffsetLabel;
	SliderProp		m_vtOffsetSlider;
	Label			m_atIntensityLabel;
	SliderProp		m_atIntensitySlider;
	Label			m_atOffsetLabel;
	SliderProp		m_atOffsetSlider;
	Label			m_expPedalLabel;
	ComboBox		m_expPedalCombo;
	Label			m_susPedal1Label;
	ComboBox		m_susPedal1Combo;
	Label			m_susPedal2Label;
	ComboBox		m_susPedal2Combo;
	Label			m_assign1Label;
	ComboBox		m_assign1Combo;
	Label			m_assign2Label;
	ComboBox		m_assign2Combo;
	Label			m_assignMode1Label;
	ComboBox		m_assignMode1Combo;
	Label			m_assignMode2Label;
	ComboBox		m_assignMode2Combo;
	ToggleButton	m_patchChainButton;
	Label			m_samplePoolLabel;
	TextEditor		m_samplePoolEdit;

	std::vector<Label>		m_perfLabels;
	std::vector<Slider2>	m_perfSliders;

	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};


class SystemPanel :
	public Component,
	public Slider2::Listener,
	public ComboBox::Listener,
	public Button::Listener
{
public:
	SystemPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID);
	virtual ~SystemPanel();
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void sliderValueChanged(Slider2* slider) override;
	void buttonClicked(Button* button) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;


	Label			m_transposeLabel;
	SliderProp		m_transposeSlider;
	Label			m_fineTuneLabel;
	SliderProp		m_fineTuneSlider;
	ToggleButton	m_loadBPMButton;
	ToggleButton	m_loadOutputsButton;
	Label			m_randomTuneLabel;
	SliderProp		m_randomTuneSlider;
	Label			m_expPedalPolarityLabel;
	ComboBox		m_expPedalPolarityCombo;
	Label			m_susPedal1PolarityLabel;
	ComboBox		m_susPedal1PolarityCombo;
	Label			m_susPedal2PolarityLabel;
	ComboBox		m_susPedal2PolarityCombo;
	Label			m_gloExprLabel;
	ComboBox		m_gloExprCombo;
	Label			m_gloSusLabel;
	ComboBox		m_gloSusCombo;
	Label				m_vtIntensityLabel;
	GlobalAtVelSlider	m_vtIntensitySlider;
	Label				m_vtOffsetLabel;
	GlobalAtVelSlider	m_vtOffsetSlider;
	Label				m_atIntensityLabel;
	GlobalAtVelSlider	m_atIntensitySlider;
	Label				m_atOffsetLabel;
	GlobalAtVelSlider	m_atOffsetSlider;
	ToggleButton	m_loadSampleButton;

	Label			m_remapATLabel;
	ComboBox		m_remapATCombo;


	Label			m_midiChannelLabel;
	SliderProp		m_midiChannelSlider;
	ToggleButton	m_programChangeAllowedButton;
	ToggleButton	m_sendArpButton;
	ToggleButton	m_omniButton;
	ToggleButton	m_localButton;
	ToggleButton	m_transmitSysExButton;
	Label			m_clockSourceLabel;
	ComboBox		m_clockSourceCombo;
	ToggleButton	m_volumeButton;
	ToggleButton	m_realtimeButton;
	ToggleButton	m_polychainButton;
	Label			m_deviceIDLabel;
	SliderDeviceID	m_deviceIDSlider;
	Label			m_cc1Label;
	SliderProp		m_cc1Slider;
	Label			m_cc2Label;
	SliderProp		m_cc2Slider;
	Label			m_cc3Label;
	SliderProp		m_cc3Slider;
	Label			m_cc4Label;
	SliderProp		m_cc4Slider;
	Label			m_cc5Label;
	SliderProp		m_cc5Slider;


	Preset::ParameterChangedConnection			m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};

#endif  // SETTINGSPANEL_H_INCLUDED
