/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef APPCMDMGR_H_INCLUDED
#define APPCMDMGR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>

ApplicationCommandManager& GetGlobalCommandManager();

enum CommandIDs
{
	FileNew = 0x2000,
	FileOpen = 0x2001,
	FileClose = 0x2002,
	FileSave = 0x2003,
	FileSaveAs = 0x2004,
	EditAdd = 0x2010,
	EditRemove = 0x2011,
	EditMute = 2012,
	EditUnMuteAll = 2013,
	EditSelectAll = 2014,
	EditExpandAll = 0x2015,
	EditCollapseAll = 0x2016,
	EditArp = 0x2017,
	EditHistory = 0x2018,
	EditUndo = 0x2019,
	EditUndoDestructive = 0x2020,
	EditRedo = 0x2021,
	EditPreferences = 0x2022,
	EditSetBookmark1 = 0x2030,
	EditSetBookmark2 = 0x2031,
	EditSetBookmark3 = 0x2032,
	EditSetBookmark4 = 0x2033,
	EditSetBookmark5 = 0x2034,
	EditSetBookmark6 = 0x2035,
	EditSetBookmark7 = 0x2036,
	EditSetBookmark8 = 0x2037,
	EditSetBookmark9 = 0x2038,
	EditSetBookmark0 = 0x2039,
	EditGoToBookmark1 = 0x2040,
	EditGoToBookmark2 = 0x2041,
	EditGoToBookmark3 = 0x2042,
	EditGoToBookmark4 = 0x2043,
	EditGoToBookmark5 = 0x2044,
	EditGoToBookmark6 = 0x2045,
	EditGoToBookmark7 = 0x2046,
	EditGoToBookmark8 = 0x2047,
	EditGoToBookmark9 = 0x2048,
	EditGoToBookmark0 = 0x2049,
	EditName = 0x2050,
	MIDISendSystem = 0x2051,
	MIDIReceiveSystem = 0x2052, 
	MIDISendPreset = 0x2053,
	MIDIReceivePreset = 0x2054,
	MIDISync = 0x2055,
	MIDISetup = 0x2056,
	HelpAbout = 0x2060,
	ResetArpVolumes = 0x2200,
	ResetArpGateLengths = 0x2201,
	ResetArpGates = 0x2203,
	ResetArpParameters = 0x2204,

	DebugDumpPreset = 2300,
	DebugSaveHistory = 2301,
	DebugLoadHistory = 2302
};

#endif  // APPCMDMGR_H_INCLUDED
