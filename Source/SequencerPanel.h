/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SEQUENCERPANEL_H_INCLUDED
#define SEQUENCERPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "Slider2.h"
#include "MiscControls.h"
class CommandHistory;

class SliderSeq : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	void setUseRest(bool useRest) {	m_useRest = useRest; }
private:
	bool m_useRest;
};


class SequencerPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener,
	public Button::Listener
{
public:
	SequencerPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID);
	virtual ~SequencerPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_patternLengthLabel;
	SliderProp		m_patternLengthSlider;

	std::vector<Label>		m_stepLabels;
	std::vector<SliderSeq>	m_stepSliders;

	Label			m_modeLabel;
	ComboBox		m_modeCombo;
	Label			m_divisionLabel;
	ComboBox		m_divisionCombo;
	Label			m_patternLabel;
	SliderProp		m_patternSlider;
	Label			m_swingLabel;
	SliderSwing		m_swingSlider;
	Label			m_bpmLabel;
	SliderProp		m_bpmSlider;
	TextButton		m_initButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};



#endif  // SEQUENCERPANEL_H_INCLUDED
