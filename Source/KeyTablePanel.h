/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef KEYTABLE_H_INCLUDED
#define KEYTABLE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "Slider2.h"
class CommandHistory;

class SliderKeyTable : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
};


class KeyTablePanel :
	public Component,
	public Slider2::Listener,
	public Button::Listener
{
public:
	KeyTablePanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~KeyTablePanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void sliderDragEnded(Slider2*) override;
	virtual void buttonClicked(Button *) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label		m_prevLabel;
	Label		m_prevData;
	Label		m_prevValueLabel;
	Label		m_prevValueData;

	Label		m_currentKeyLabel;
	SliderProp	m_currentKeySlider;
	Label		m_currentValueLabel;
	SliderKeyTable	m_currentValueSlider;

	Label		m_nextLabel;
	Label		m_nextData;
	Label		m_nextValueLabel;
	Label		m_nextValueData;

	TextButton	m_initCurrentValueButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
};



#endif  // KEYTABLE_H_INCLUDED
