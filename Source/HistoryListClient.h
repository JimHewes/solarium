/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/


#ifndef HISTORYLISTCLIENT_H_INCLUDED
#define HISTORYLISTCLIENT_H_INCLUDED
#include "HistoryList.h"
#include <vector>
#include <unordered_map>

struct Branch
{
	Version parentVersion{};
	bool	active{ true };

	void Write(std::ostream& output)
	{
		parentVersion.Write(output);
		output.write(reinterpret_cast<const char *>(&active), sizeof(active));
	}
	void Read(std::istream& input)
	{
		parentVersion.Read(input);
		input.read(reinterpret_cast<char *>(&active), sizeof(active));
	}
};

/**
	A HistoryListClient only contain those branches for which it has edits. (But a HistoryListSet 
	contains every Branch that the main HistoryList does.)

	Any class using HistoryListClient must initialize this by adding an item with revision=0 and 
	branchTag=0 to guarantee there is always Some version to undo to or to use if there are never any 
	later version added. This revision 0 might be overwritten later.
*/
template<typename ItemType>
class HistoryListClient
{
	struct HistoryItem
	{
		Revision revision;
		ItemType value;
		void Write(std::ostream& output, std::function<void(std::ostream&, ItemType)> writeItemFunc)
		{
			output.write(reinterpret_cast<const char *>(&revision), sizeof(revision));
			writeItemFunc(output, value);
		}
		void Read(std::istream& input, std::function<void(std::istream&, ItemType&)> readItemFunc)
		{
			input.read(reinterpret_cast<char *>(&revision), sizeof(revision));
			readItemFunc(input, value);
		}	
	};

	std::unordered_map<BranchTag, std::vector<HistoryItem>> itemLists;

public:
	void AddItem(Version version, ItemType item)
	{
		auto& list = itemLists[version.branchTag];	// Note that this will add the branch key if it doesn't already exist.
		if (!list.empty())
		{
			if (list.back().revision == version.revision)
			{
				list.back().value = item;	// overwrite previous
			}
			else
			{
				// if the previous item is the same, then no reason to save this one.
				// Only save if they are different.
				if (item != list.back().value)
				{
					list.push_back({ version.revision, item });	// add new
				}
			}
		}
		else
		{
			// if the branch is empty, then alway save. 
			list.push_back({ version.revision, item });
		}
	}

	/** @brief Retrieves a copy of the item at the specified version.

		If the item was not changed at the version given, the function looks for the most recent state 
		(most recent version) of the item along the same branch and returns that. (The history must always 
		contain at least one version as a sparting point.)
		
		@param[in] version	The version to search for.
		@param[in] branches	A reference to the list of all branches in the history, each identifying the 
							previous branchpoint it started from. This list is used to follow the chain 
							of branches back as far as needed to find what was the the current state of 
							the item at the version requested.
		@returns The a copy of the item that represents the state of the item at the requested version.
	*/
	ItemType GetItemAtVersion(Version version, const std::vector<Branch>& branches) const
	{
		ItemType result{};
		bool found{ false };

		BranchTag branchTag = version.branchTag;
		const Branch* pBranch = &branches[branchTag];
		Revision targetRevision = version.revision;

		// If this client has edits in this branch, then pretend the branchPointRevision is the last revision.
		// In otherwords, in the first list we search, we'll take _any_ revision less than or equal to the target revision.
		Revision branchPointRevision = (itemLists.count(branchTag) > 0 && !itemLists.at(branchTag).empty() ) ? 
											itemLists.at(branchTag).back().revision : INVALID_REVISION;
		assert(itemLists.size() > 0);
		while (!found)
		{
			if (itemLists.count(branchTag) > 0)
			{
				const auto& itemList = itemLists.at(branchTag);
				if (itemList.size() > 1)
				{
					found = false;
				}
				if (!itemList.empty() && itemList[0].revision <= targetRevision)
				{
					// At this point we should have the itemList where the revision should be if it exists,
					// or the next earliest revison if it doesn't.

					// Find greatest revision that is less than or equal to compareRevision.
					// It must be in this itemList because:
					// 1. The revision we want is greater than or equal to the first item in this list (or we would have continued looping).
					// 2. The revision we want is less than the first item in the previous list. So the revision we want must be equal to 
					//		or less than the branch point of this list.
					// So, start at the branch point and find the first revsion that is equal to or less than the one we're looking for
					auto compareRevision = std::min(targetRevision, branchPointRevision);
					// Looking backwards, find the first revision that is less than or equal to both the revision we're looking for AND the branch point.
					auto iter = std::find_if(itemList.crbegin(), itemList.crend(), [compareRevision](const HistoryItem& item)
					{
						return item.revision <= compareRevision;
					});
					if (iter != itemList.crend())
					{
						result = (*iter).value;
						found = true;
					}
				}
			}
			if (!found)
			{
				// We should have found some value before we get to the start of the history.
				assert(pBranch->parentVersion.branchTag != NO_BRANCH);
				branchPointRevision = pBranch->parentVersion.revision;
				branchTag = pBranch->parentVersion.branchTag;
				pBranch = &branches[branchTag];
			}
		};

		return result;
	}

	/** @brief	Removes (deletes) an item from a branch if it exists. It must be the last item in the branch.

		Note that the revision must also be the last revision in the branch overall (not just in this client) 
		and might not exist in this	particular client history list. The main HistoryList should assure that it's 
		the last in the overall branch.

		@param[in]	The version of the item to remove. This must be the last revision stored in the branch.
					If it is not, this function has no effect.
	*/
	void RemoveVersion(Version version)
	{
		// If the branch exists in this client and has at least one version in it.
		if (itemLists.count(version.branchTag) > 0 && !itemLists.at(version.branchTag).empty())
		{
			assert(itemLists.at(version.branchTag).back().revision <= version.revision); // There should be no item with a higher revision.
			if (itemLists.at(version.branchTag).back().revision == version.revision)
			{
				itemLists.at(version.branchTag).pop_back();
			}
		}
	}

	/** @brief	Returns true if none of the branches have any items stored.
	*/
	bool IsEmpty()
	{
		return itemLists.end() == std::find_if(itemLists.begin(), itemLists.end(), [this](auto& historyItemList)
		{
			return !historyItemList.second.empty();
		});
	}

	/** @brief Deletes items from a branch if they are above a given revision.
	*/
	void DeleteBranch(BranchTag branchTag, Revision maxRevision)
	{
		if (itemLists.count(branchTag) > 0)
		{
			auto& list = itemLists.at(branchTag);
			list.erase(std::remove_if(list.begin(), list.end(), [maxRevision](HistoryItem& hi)
			{
				return hi.revision > maxRevision;
			}), list.end());
		}
	}

	void Write(std::ostream& output, std::function<void(std::ostream&, ItemType)> writeItemFunc)
	{
		auto branchCount = static_cast<uint32_t>(itemLists.size());
		output.write(reinterpret_cast<const char *>(&branchCount), sizeof(branchCount));
		for (auto& branch : itemLists)
		{
			BranchTag branchTag = branch.first;
			output.write(reinterpret_cast<const char *>(&branchTag), sizeof(branchTag));
			auto historyItemCount = static_cast<uint32_t>(branch.second.size());
			output.write(reinterpret_cast<const char *>(&historyItemCount), sizeof(historyItemCount));
			for (auto& historyItem : branch.second)
			{
				historyItem.Write(output, writeItemFunc);
			}
		}
	}
	void Read(std::istream& input, std::function<void(std::istream&, ItemType&)> readItemFunc)
	{
		itemLists.clear();

		uint32_t branchCount{};
		input.read(reinterpret_cast<char *>(&branchCount), sizeof(branchCount));
		for (uint32_t i = 0; i < branchCount; ++i)
		{
			BranchTag branchTag{};
			input.read(reinterpret_cast<char *>(&branchTag), sizeof(branchTag));
			auto& list = itemLists[branchTag];	// Add the branch

			uint32_t historyItemCount{};
			input.read(reinterpret_cast<char *>(&historyItemCount), sizeof(historyItemCount));

			for (uint32_t j = 0; j < historyItemCount; ++j)
			{
				HistoryItem histItem;
				histItem.Read(input, readItemFunc);
				list.push_back(histItem);
			}
		}
	}
};

/** A collection of HistoryListClients that contains a manages of branches common to all clients.

	History list clients need to know the full branch information of the undo history if they are to go to any 
	arbitrary version. The purpose of having HistoryListSet is to avoid storing a complete copy of branch 
	information in each client. Instead it is stored once in HistoryListSet and passed to a HistoryLisClient when needed.
	A HistoryListSet contains every Branch that the main HistoryList does. But each HistoryListClient only 
	contain those branches for which it has edits.
*/
template<typename KeyType, typename ItemType>
class HistoryListSet
{
	// Since not every edit is stored, we need to know information about where the branch points are in the history.
	// If we stored this with each item, that would be redundant and waste a lot of space when there are a lot of items.
	// So we store and maintain this once in HistoryListSet.
	std::vector<Branch> branches;
	std::unordered_map<KeyType, HistoryListClient<ItemType>>	clients;

public:
	HistoryListSet()
	{
		branches.resize(1);
		branches[0].parentVersion = Version(INVALID_REVISION, 0);
		branches[0].active = true;
	}
	void AddItem(Version version, KeyType key, ItemType item)
	{
		clients[key].AddItem(version, item);
	}

	void AddBranch(BranchTag branchTag, Version parentVersion)
	{
		if (branches.size() < static_cast<size_t>(branchTag + 1))
		{
			branches.resize(branchTag + 1);
		}
		branches[branchTag].parentVersion = parentVersion;
		branches[branchTag].active = true;
	}

	ItemType GetItemAtVersion(Version version, KeyType key)
	{
		return clients[key].GetItemAtVersion(version, branches);
	}

	void RemoveVersion(Version version)
	{
		for (auto& historyListClient : clients)
		{
			historyListClient.second.RemoveVersion(version);
		}
	}

	/** @brief	Returns true if none of the keys have any items stored for any branches
	*/
	bool IsEmpty(KeyType key)
	{
		assert(clients.count(key) > 0);
		return clients[key].IsEmpty();
	}

	void DeleteBranch(BranchTag branchTag, Revision maxRevision)
	{
		branches[branchTag].active = false;

		for (auto& historyListClient : clients)
		{
			historyListClient.second.DeleteBranch(branchTag, maxRevision);
		}
	}

	void Write(std::ostream& output, 
		std::function<void(std::ostream&, KeyType)> writeKeyFunc,
		std::function<void(std::ostream&, ItemType)> writeItemFunc)
	{
		auto branchCount = static_cast<uint32_t>(branches.size());
		output.write(reinterpret_cast<const char *>(&branchCount), sizeof(branchCount));
		for (auto& branch : branches)
		{
			branch.Write(output);
		}

		auto clientCount = static_cast<uint32_t>(clients.size());
		output.write(reinterpret_cast<const char *>(&clientCount), sizeof(clientCount));
		for (auto& client : clients)
		{
			writeKeyFunc(output, client.first);
			client.second.Write(output, writeItemFunc);
		}
	}

	void Read(std::istream& input, 
		std::function<void(std::istream&, KeyType&)> readKeyFunc,
		std::function<void(std::istream&, ItemType&)> readItemFunc)
	{
		clients.clear();
		branches.clear();

		uint32_t branchCount{};
		input.read(reinterpret_cast<char *>(&branchCount), sizeof(branchCount));
		for (uint32_t i = 0; i < branchCount; ++i)
		{
			Branch branch;
			branch.Read(input);
			branches.push_back(branch);
		}
		
		uint32_t clientCount{};
		input.read(reinterpret_cast<char *>(&clientCount), sizeof(clientCount));
		for (uint32_t i = 0; i < clientCount; ++i)
		{
			KeyType key;
			readKeyFunc(input, key);
			auto& client = clients[key];	// add 
			client.Read(input, readItemFunc);
		}
	}

};


template<typename ItemType>
class HistoryListSingle
{
	HistoryListClient<ItemType>	client;
	std::vector<Branch> branches;

public:
	HistoryListSingle()
	{
		branches.resize(1);
		branches[0].parentVersion = Version(INVALID_REVISION, 0);
		branches[0].active = true;
	}

	void AddItem(Version version, ItemType item)
	{
		client.AddItem(version, item);
	}

	void AddBranch(BranchTag branchTag, Version parentVersion)
	{
		if (branches.size() < static_cast<size_t>(branchTag + 1))
		{
			branches.resize(branchTag + 1);
		}
		branches[branchTag].parentVersion = parentVersion;
		branches[branchTag].active = true;
	}

	ItemType GetItemAtVersion(Version version)
	{
		return client.GetItemAtVersion(version, branches);
	}

	void RemoveVersion(Version version)
	{
		client.RemoveVersion(version);
	}

	bool IsEmpty()
	{
		return 	client.IsEmpty();
	}

	void DeleteBranch(BranchTag branchTag, Revision maxRevision)
	{
		branches[branchTag].active = false;
		client.DeleteBranch(branchTag, maxRevision);
	}

	void Write(std::ostream& output, std::function<void(std::ostream&, ItemType)> writeItemFunc)
	{
		auto branchCount = static_cast<uint32_t>(branches.size());
		output.write(reinterpret_cast<const char *>(&branchCount), sizeof(branchCount));
		for (auto& branch : branches)
		{
			branch.Write(output);
		}

		client.Write(output, writeItemFunc);
	}

	void Read(std::istream& input, std::function<void(std::istream&, ItemType&)> readItemFunc)
	{
		branches.clear();

		uint32_t branchCount{};
		input.read(reinterpret_cast<char *>(&branchCount), sizeof(branchCount));
		for (uint32_t i = 0; i < branchCount; ++i)
		{
			Branch branch;
			branch.Read(input);
			branches.push_back(branch);
		}
		
		client.Read(input, readItemFunc);
	}

};

#endif  // HISTORYLISTCLIENT_H_INCLUDED
