/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef FXCHANNELPANEL_H_INCLUDED
#define FXCHANNELPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "ViewModel.h"

class CommandHistory;

class FXChannelPanel :	public Component
{
public:
	FXChannelPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID, ViewModel* pViewModel);
	virtual ~FXChannelPanel();
	void resized() override;

	void OnFocusedSlotChanged(int slotIndex);
private:
	void Update();
	ViewModel::FxFocusedSlotChangedConnection	m_focusedSlotChangedConnection;
	Preset::ParameterChangedConnection			m_paramChangedConnection;

	int				m_focusedSlotIndex{ 0 };
	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;
	std::unique_ptr<Component>	m_pPanel;
};



class EffectOffPanel :	public Component
{
public:
	EffectOffPanel(ElementID elemID,int slot);
	virtual ~EffectOffPanel();
	void paint(Graphics& g) override;
private:
	Label			m_subtitle;
	ElementID		m_elemID;

};



#endif  // FXCHANNELPANEL_H_INCLUDED
