/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include "ConnectorView.h"
#include "PortView.h"
#include "ElementView.h"
#include "AppCmdMgr.h"
#include "Properties.h"
#include "gsl_assert.h"

ConnectorView::ConnectorView(Connector connector, PortView* pStartingPortView, PortView* pEndingPortView, ViewModel* pViewModel, Preset* pPreset)
	:m_pStartingPortView(pStartingPortView)
	,m_pEndingPortView(pEndingPortView)
	,m_connector(connector)
	,m_pViewModel(pViewModel)
	,m_pPreset(pPreset)
	,m_readOnly(false)
	,m_wireOpacity(0xFF)
{
	Expects(pStartingPortView != nullptr);

	if (m_pStartingPortView)
	{
		m_pStartingPortView->AddConnectorViewListener(this);
		m_startingPositionSurfaceCoords = m_pStartingPortView->GetSurfacePosition();

		PortID startPortID = m_pStartingPortView->GetPortID();

		if (m_pStartingPortView->GetPortID().portType != PortType::Output)
		{
			m_isMuted = m_pPreset->GetMuted(ParamID(m_pStartingPortView->GetPortID()));
		}

	}
	if (m_pEndingPortView)
	{
		m_endingPositionSurfaceCoords = m_pEndingPortView->GetSurfacePosition();
		m_pEndingPortView->AddConnectorViewListener(this);

		PortID endPortID = m_pEndingPortView->GetPortID();
		if (m_pEndingPortView->GetPortID().portType != PortType::Output)
		{
			m_isMuted = m_pPreset->GetMuted(ParamID(m_pEndingPortView->GetPortID()));
		}
	}
	else
	{
		m_endingPositionSurfaceCoords = m_startingPositionSurfaceCoords;
	}

	if (m_pStartingPortView && m_pEndingPortView)
	{
		m_readOnly = !m_pViewModel->CanBeRemoved(m_connector);
	}


	int opacityPercent = pPropertiesFile->getIntValue(propWireOpacity, 100);
	m_wireOpacity = static_cast<uint8_t>((opacityPercent * 255) / 100);
	int thresholdPercent = pPropertiesFile->getIntValue(propWireThreshold, 80);
	m_wireThreshold = static_cast<uint8_t>((thresholdPercent * 255) / 100);

	pPropertiesFile->addChangeListener(this);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted) {	OnParameterChanged(paramID, value, isMuted); });

	UpdateBounds();
}

ConnectorView::~ConnectorView()
{
	if (pPropertiesFile)
	{
		pPropertiesFile->removeChangeListener(this);
	}
	if (m_pStartingPortView)
	{
		m_pStartingPortView->RemoveConnectorViewListener(this);
	}
	if (m_pEndingPortView)
	{
		m_pEndingPortView->RemoveConnectorViewListener(this);
	}
}

void ConnectorView::SetEndingPortView(PortView* pEndingPortView)
{
	// We could allow m_pEndingPortView to be set more than once, but currently there's no need to.
	// So any time that happens is currently a mistake.
	Expects(m_pEndingPortView == nullptr);

	m_pEndingPortView = pEndingPortView;
	if (m_pEndingPortView)
	{
		m_endingPositionSurfaceCoords = m_pEndingPortView->GetSurfacePosition();
		m_pEndingPortView->AddConnectorViewListener(this);

		if (m_pEndingPortView->GetPortID().portType != PortType::Output)
		{
			m_isMuted = m_pPreset->GetMuted(ParamID(m_pEndingPortView->GetPortID()));
		}
	}

	// No need to update m_readOnly here because any connector that starts out with no end port (m_pEndingPortView == nullptr) 
	// is one that is in the process of being added by the user. And any connector that can be added by the user can also be removed.
	UpdateBounds();

	Ensures(m_pEndingPortView != nullptr);
}

void ConnectorView::UpdateBounds()
{
	path.clear();
	path.startNewSubPath((float)m_startingPositionSurfaceCoords.getX(), (float)m_startingPositionSurfaceCoords.getY());
	path.quadraticTo(
		(m_startingPositionSurfaceCoords.getX() + m_endingPositionSurfaceCoords.getX() + 3) / 2.0f,	// the +3 helps to keep perfectly vertical lines visible by adding a slight curve.
		(m_startingPositionSurfaceCoords.getY() + m_endingPositionSurfaceCoords.getY() + 100) / 2.0f,	// +100 gives a droop to the connector line, so it looks something like a cable.
		(float)m_endingPositionSurfaceCoords.getX(),
		(float)m_endingPositionSurfaceCoords.getY());
	auto rect = path.getBounds();	// get smallest rectangle that contains all the points in the path.
	setBounds(rect.getSmallestIntegerContainer());	// convert to integer and then set.
}


/** Called when either the starting or ending port moved; The ConnectorView gets recalculated.

	@param pPortViewThatMoved	A pointer that identified that this is either the m_pStartingPortView or the m_pEndingPortView.
								Note that this can be nullptr when the Connector is being created by a drag operation and does not
								have an endingConnector yet.

	@param newPosition	This is the position that is the center of the PortView in terms of Surface coordinates.

*/
void ConnectorView::OnPortViewMoved(PortView* pPortViewThatMoved, Point<int> newPosition)
{
	if (pPortViewThatMoved == m_pStartingPortView)
	{
		m_startingPositionSurfaceCoords = newPosition;
	}
	else if (pPortViewThatMoved == m_pEndingPortView)
	{
		m_endingPositionSurfaceCoords = newPosition;
	}
	UpdateBounds();

	// It seems no call to repaint() is needed here. Perhaps setting bounds retriggers it automatically.
}

/**	This must be called by the PortView that this ConnectorView is listening to so that the ConnectorView will no longer try to access it.

	This function must not throw because it will be called from within a PortView's destructor.

	@param[in] pPortViewDestroyed	A pointer to the PortView that is about to be destroyed. This Connector should no longer 
									call any of that PortView's functions.
*/
void ConnectorView::OnPortViewDestoyed(PortView* pPortViewDestroyed) noexcept
{
	if (pPortViewDestroyed == m_pStartingPortView)
	{
		m_pStartingPortView = nullptr;
	}
	else if (pPortViewDestroyed == m_pEndingPortView)
	{
		m_pEndingPortView = nullptr;
	}
}

bool ConnectorView::ConnectedElementIsSelected()
{
	bool result = false;
	if (m_pViewModel->ElementViewSelectionModel().HasSelection())
	{
		ElementID selectedElemID = m_pViewModel->ElementViewSelectionModel().GetFirstSelection();
		if (m_connector.startPort.elemID == selectedElemID || m_connector.endPort.elemID == selectedElemID)
		{
			result = true;
		}
	}
	return result;
}

void ConnectorView::paint(Graphics& g)
{

	Rectangle<int> area = getLocalBounds();
	Rectangle<float> areaf((float)area.getWidth(), (float)area.getHeight());
	//float selectionInset = (float)GetSelectionInset();
	//g.setColour(Colour(0x80D0E6FF));
	//g.drawRect(areaf, 1);
	//areaf.reduce(5.0f, 5.0f);
	//g.drawRect(areaf, 1);


	Colour normalColor = Colour(m_wireOpacity << 24);	// black
	Colour audioColor = Colour((m_wireOpacity << 24) | 0x00008000);	// green
	//Colour selElemColor = Colour((m_wireOpacity << 24) | 0x00FF0000);	// red
	Colours::black;

	// We don't count connections to Lag as audio even though they could be. Typically they're not.
	Colour col(normalColor);
	if ((m_connector.startPort.elemID.elemType != ElementType::Lag && m_connector.startPort.portType == PortType::Input) || 
		(m_connector.startPort.elemID.elemType != ElementType::Lag && m_connector.endPort.portType == PortType::Input))
	{
		col = audioColor;
	}
	// connectors to oscillator sync inputs are black.
	if ((m_connector.startPort.elemID.elemType == ElementType::Oscillator && m_connector.startPort.portType == PortType::Input) ||
		(m_connector.endPort.elemID.elemType == ElementType::Oscillator && m_connector.endPort.portType == PortType::Input))
	{
		col = normalColor;
	}
	if (ConnectedElementIsSelected())
	{
		col = Colours::red;
	}

	// We know that the connector line needs to be drawn from the starting point to the ending point.
	// But we start out with those points in terms of Surface coordinates. We need to convert those 
	// to coordinates inside the local Connector component.
	Point<int> ConnectorPosition = getPosition();
	Point<int> startingPosLocal = m_startingPositionSurfaceCoords - ConnectorPosition;
	Point<int> endingPosLocal = m_endingPositionSurfaceCoords - ConnectorPosition;

	path.clear();
	path.startNewSubPath((float)startingPosLocal.getX(), (float)startingPosLocal.getY());
	path.quadraticTo(
		(startingPosLocal.getX() + endingPosLocal.getX()) / 2.0f,
		(startingPosLocal.getY() + endingPosLocal.getY() + 100) / 2.0f,
		(float)endingPosLocal.getX(),
		(float)endingPosLocal.getY());
	//path.quadraticTo(150, 50, 400, 400);
	if (m_pViewModel->ConnectorSelectionModel().IsSelected(m_connector))
	{
		g.setColour(Colour(0xD090A6FF));
		g.strokePath(path, PathStrokeType(6.0f));
	}
	g.setColour(m_readOnly ? Colours::grey : col);

	if (m_isMuted)
	{
		Path dashedPath;
		PathStrokeType dashedStrokeType(2.0f);
		float segmentLength[2] = { 10.f, 8.f };
		dashedStrokeType.createDashedStroke(dashedPath, path, segmentLength, 2);
		g.strokePath(dashedPath, PathStrokeType(1.0f));
	}
	else
	{
		g.strokePath(path, PathStrokeType(2.0f));
	}
}

bool ConnectorView::hitTest(int x, int y)
{
	bool result = false;

	if (!m_readOnly && (m_wireOpacity >= m_wireThreshold || ConnectedElementIsSelected()))
	{
		//DBG("Hit test at location x = " << x << ", y = " << y);
		Point<float> nearest;
		Point<float> target((float)x,(float)y);
		path.getNearestPoint(target,nearest);
		Point<float> startPoint = path.getPointAlongPath(0);
		Point<float> endPoint = path.getPointAlongPath(path.getLength());


		//DBG("   Nearest location on path is x = " << nearest.getX() << ", y = " << nearest.getY());

		float xDiff = (target.getX() > nearest.getX()) ? target.getX() - nearest.getX() : nearest.getX() - target.getX();
		float yDiff = (target.getY() > nearest.getY()) ? target.getY() - nearest.getY() : nearest.getY() - target.getY();
		if (xDiff < 7.0f && yDiff < 7.0f)
		{
			// check if it's near the start or end points
			xDiff = (startPoint.getX() > nearest.getX()) ? startPoint.getX() - nearest.getX() : nearest.getX() - startPoint.getX();
			yDiff = (startPoint.getY() > nearest.getY()) ? startPoint.getY() - nearest.getY() : nearest.getY() - startPoint.getY();
			if (xDiff > 7.0f || yDiff > 7.0f)
			{
				xDiff = (endPoint.getX() > nearest.getX()) ? endPoint.getX() - nearest.getX() : nearest.getX() - endPoint.getX();
				yDiff = (endPoint.getY() > nearest.getY()) ? endPoint.getY() - nearest.getY() : nearest.getY() - endPoint.getY();
				if (xDiff > 7.0f || yDiff > 7.0f)
				{
					result = true;
				}
			}
		}
	}
	return result;
}

void ConnectorView::mouseDown(const MouseEvent& event)
{
	event;
	DBG("Mouse down on ConnectionView.");

	m_pViewModel->ConnectorSelectionModel().Select(m_connector);

	if (event.mods.isRightButtonDown())
	{
		PopupMenu mainMenu;
		//mainMenu.addItem(1, "Remove");

		mainMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditMute, "Toggle Mute");
		mainMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditUnMuteAll, "Unmute All");
		mainMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditRemove, "Remove");
		mainMenu.show(0, 0, 0, 0, nullptr);

		// Since there is no async handler given, the result returned from show() is not zero but is the ID of the command chosen;
	}
}

void ConnectorView::changeListenerCallback(ChangeBroadcaster * source)
{
	if (source == pPropertiesFile.get())
	{	
		int opacityPercent = pPropertiesFile->getIntValue(propWireOpacity, 100);
		m_wireOpacity = static_cast<uint8_t>((opacityPercent * 255) / 100);
		int thresholdPercent = pPropertiesFile->getIntValue(propWireThreshold, 80);
		m_wireThreshold = static_cast<uint8_t>((thresholdPercent * 255) / 100);

		// If this connector is selected...
		if ((m_pViewModel->ConnectorSelectionModel().IsSelected(m_connector)) && 
			// ...and the opacity is below the threshold...
			(m_wireOpacity < m_wireThreshold ))
		{
			// then deselect this connector
			m_pViewModel->ConnectorSelectionModel().Clear();
		}
		repaint();
	}
}

void ConnectorView::OnParameterChanged(const ParamID& paramID, int , bool isMuted)
{
	// Only care about input port types
	if (paramID.portType != PortType::None && paramID.portType != PortType::Output &&
		m_pStartingPortView != nullptr && m_pEndingPortView != nullptr)
	{
		// If one end of this connector view is connected to the Input, Mod, or Control port/parameter that changed...
		if ((ParamID(m_pStartingPortView->GetPortID()) == paramID) || ParamID(m_pEndingPortView->GetPortID()) == paramID)
		{
			m_isMuted = isMuted;
			repaint();
		}
	}
}
