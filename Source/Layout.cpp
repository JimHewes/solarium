/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <vector>
#include <algorithm>
#include <cassert>
#include "Layout.h"


using std::vector;

struct GridLocation
{
	ElementID	elemID;
	uint8_t		column;
	uint8_t		order;
};

vector<GridLocation> GridItems
{
//	{ { ElementType::System, 0 },		0, 0 },
	{ { ElementType::Home, 0 },		0, 0 },
	{ { ElementType::Velocity, 0},		0, 1 },
	{ { ElementType::Aftertouch, 0 },	0, 2 },
	{ { ElementType::ModWheel, 0 },		0, 3 },
	{ { ElementType::AT_ModWheel, 0 },	0, 4 },
	{ { ElementType::Ribbon, 0 },		0, 5 },
	{ { ElementType::Ribbon, 1 },		0, 6 },
	{ { ElementType::JoyX, 0 },			0, 7 },
	{ { ElementType::JoyY, 0 },			0, 8 },
	{ { ElementType::KeyTracking, 0 },	0, 9 },
	{ { ElementType::Note, 0 },			0, 10 },
	{ { ElementType::PolyAT, 0 },		0, 11 },
	{ { ElementType::MaxValue, 0 },		0, 12 },
	{ { ElementType::Breath, 0 },		0, 13 },
	{ { ElementType::WhiteNoise, 0 },	0, 14 },
	{ { ElementType::PinkNoise, 0 },	0, 15 },
	{ { ElementType::Gate, 0 },			0, 16 },
	{ { ElementType::ExtInput, 0 },		0, 17 },
	{ { ElementType::ExtInput, 1 },		0, 18 },
	{ { ElementType::ExtInput, 2 },		0, 19 },
	{ { ElementType::ExtInput, 3 },		0, 20 },
	{ { ElementType::ExtInput, 4 },		0, 21 },	// Ext Input 1/2
	{ { ElementType::ExtInput, 5 },		0, 22 },	// Ext Input 3/4
	{ { ElementType::SPDIF_In, 0 },		0, 23 },	// S/P-DIF Left
	{ { ElementType::SPDIF_In, 1 },		0, 24 },	// S/P-DIF Right
	{ { ElementType::SPDIF_In, 2 },		0, 25 },	// S/P-DIF Left & Right
	{ { ElementType::AssignButton, 0},	0, 26 },
	{ { ElementType::AssignButton, 1},	0, 27 },
	{ { ElementType::Pedal, 0 },		0, 28 },
	{ { ElementType::Pedal, 1 },		0, 29 },
	{ { ElementType::CC, 0 },			0, 30 },
	{ { ElementType::CC, 1 },			0, 31 },
	{ { ElementType::CC, 2 },			0, 32 },
	{ { ElementType::CC, 3 },			0, 33 },
	{ { ElementType::CC, 4 },			0, 34 },
	{ { ElementType::Seq, 0 },			0, 35 },
	{ { ElementType::Seq, 1 },			0, 36 },
	{ { ElementType::Seq, 2 },			0, 37 },
	{ { ElementType::Seq, 3 },			0, 38 },
	{ { ElementType::KeyTable, 0 },		0, 39 },
	{ { ElementType::KeyTable, 1 },		0, 40 },
	{ { ElementType::KeyTable, 2 },		0, 41 },
	{ { ElementType::KeyTable, 3 },		0, 42 },

	{ { ElementType::LFO, 0 },			1, 0 },
	{ { ElementType::LFO, 1 },			1, 1 },
	{ { ElementType::LFO, 2 },			1, 2 },
	{ { ElementType::LFO, 3 },			1, 3 },
	{ { ElementType::VibratoLFO, 0 },	1, 4 },

	{ { ElementType::Oscillator, 0 },	2, 0 },
	{ { ElementType::Oscillator, 1 },	2, 1 },
	{ { ElementType::Oscillator, 2 },	2, 2 },
	{ { ElementType::Oscillator, 3 },	2, 3 },
	{ { ElementType::Lag, 0 },			2, 4 },
	{ { ElementType::Lag, 1 },			2, 5 },
	{ { ElementType::Lag, 2 },			2, 6 },
	{ { ElementType::Lag, 3},			2, 7 },

	{ { ElementType::Rotor, 0 },	3, 0 },
	{ { ElementType::Rotor, 1 },	3, 1 },
	{ { ElementType::Vector, 0 },	3, 2 },
	{ { ElementType::Vector, 1 },	3, 3 },
	{ { ElementType::AM, 0 },		3, 4 },
	{ { ElementType::AM, 1 },		3, 5 },
	{ { ElementType::EG, 0 },		3, 6 },
	{ { ElementType::EG, 1 },		3, 7 },


	{ { ElementType::Mixer, 0 },	4, 0 },
	{ { ElementType::Mixer, 1 },	4, 1 },
	{ { ElementType::Mixer, 2 },	4, 2 },
	{ { ElementType::Mixer, 3 },	4, 3 },
	{ { ElementType::LoopEG, 0 },	4, 4 },
	{ { ElementType::EG, 2 },		4, 5 },


	{ { ElementType::Filter, 0},	5, 0 },
	{ { ElementType::Filter, 1},	5, 1 },
	{ { ElementType::Filter, 2},	5, 2 },
	{ { ElementType::Filter, 3},	5, 3 },
	{ { ElementType::EnvFol, 0},	5, 4 },
	{ { ElementType::EG, 3 },		5, 5 },

	{ { ElementType::InsertFX, 0 },	6, 0 },
	{ { ElementType::InsertFX, 1 },	6, 1 },
	{ { ElementType::InsertFX, 2 },	6, 2 },
	{ { ElementType::InsertFX, 3},	6, 3 },
	{ { ElementType::EG, 4 },		6, 4 },

	{ { ElementType::VCA, 0 },		7, 0 },
	{ { ElementType::VCA, 1 },		7, 1 },
	{ { ElementType::VCA, 2 },		7, 2 },
	{ { ElementType::VCA, 3 },		7, 3 },


	{ { ElementType::VCAMaster, 0},		8, 0 },
	{ { ElementType::EG, 5},			8, 1 },
	{ { ElementType::FXChannel, 0 },	8, 2 },
	{ { ElementType::FXChannel, 1 },	8, 3 },
	{ { ElementType::FXChannel, 2 },	8, 4 },
	{ { ElementType::FXChannel, 3},		8, 5 },
	{ { ElementType::Output, 0},		8, 6 },
	{ { ElementType::Output, 1},		8, 7 },
	{ { ElementType::Output, 2},		8, 8 },
	{ { ElementType::Output, 3},		8, 9 },
	{ { ElementType::SPDIF_Out, 0},		8, 10 }
};

struct ElementSortable
{
	ElementID elemID;
	uint16_t height;
	uint8_t order;
};

std::vector<ElementOutput> ArrangeElements(std::vector<ElementInput> elements, int elementWidth)
{
	std::vector<std::vector<ElementSortable>> gridList(9);

	for (auto& elem : elements)
	{
		auto iter = std::find_if(GridItems.begin(), GridItems.end(), [&elem](auto& gl)
		{	return gl.elemID == elem.elemID; });

		assert(iter != GridItems.end());
		assert(iter->column < gridList.size());

		if (iter != GridItems.end())
		{
			gridList[iter->column].push_back(ElementSortable{ elem.elemID, elem.height, iter->order });
		}
	}

	uint32_t xPos = 0;

	std::vector<ElementOutput> result;
	// Sort columns according to the sort order given in the GridItems list.
	for (auto& column : gridList)
	{
		std::sort(column.begin(), column.end(), [](const ElementSortable& a, const ElementSortable& b)
		{	return a.order < b.order; });

		// assign XY location
		uint32_t yPos = 0;
		for (auto& elem : column)
		{
			result.push_back(ElementOutput{ elem.elemID, xPos, yPos });
			yPos += elem.height;
		}
		if (column.size() > 0)
		{
			xPos += elementWidth;
		}
	}

	return result;
}
