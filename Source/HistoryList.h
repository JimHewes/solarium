/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef HISTORYLIST_H_INCLUDED
#define HISTORYLIST_H_INCLUDED

#include <vector>
#include <memory>
#include <string>
#include <iterator>
#include <cassert>
#include "gsl_assert.h"
#include "gsl_util.h"
#include "Notifier.h"
#include "Utility.h"

using Revision = uint16_t;
using BranchTag = uint16_t;

const Revision INVALID_REVISION = std::numeric_limits<Revision>::max();
const BranchTag NO_BRANCH = std::numeric_limits<BranchTag>::max();


struct Version
{
	Version() : Version(INVALID_REVISION, NO_BRANCH) {}
	Version(Revision revision_, BranchTag branchTag_) : revision(revision_), branchTag(branchTag_) {}
	Revision revision;
	BranchTag branchTag;

	void Write(std::ostream& output)
	{
		output.write(reinterpret_cast<const char *>(&revision), sizeof(revision));
		output.write(reinterpret_cast<const char *>(&branchTag), sizeof(branchTag));
	}
	void Read(std::istream& input)
	{
		input.read(reinterpret_cast<char *>(&revision), sizeof(revision));
		input.read(reinterpret_cast<char *>(&branchTag), sizeof(branchTag));
	}
};
const Version NO_VERSION{ INVALID_REVISION, NO_BRANCH };

inline bool operator == (const Version &lhs,const Version &rhs)
{
	return	lhs.revision == rhs.revision &&	lhs.branchTag == rhs.branchTag;
}
inline bool operator != (const Version &lhs, const Version &rhs)
{
	return !(lhs == rhs);
}

/** @brief HistoryList a list of undo-able items. Intended for use with one or more instances of HistoryListClient.

	I created HistoryList and HistoryListClient to support the undo feature of Solarium with branching. 
	Here I describe the goals and structure.\n
	\n
	Goals:\n
	- Navigate quickly to earlier versions. Don't undo every version along the way.
	- Support branching.
	- Memory efficient for the typical way Solarium will be used.

	Given that branching is required, it might seem that a good way to structure the history would be to use linked 
	lists and pointers to nodes. But navigating to arbitrary versions would normally require searching sequentially 
	through the list. Further, if every edit is a node then pointers for that would take up at least an extra four 
	bytes per edit.\n
	\n
	Instead of keeping all undo data in one place, each class maintains it's own undo information based on the 
	version number. (So perhaps this makes it more scalable than a central undo.)
	The class HistoryList is the controlling class and there must be only one instance of this. This 
	is where the commands are stored. The class HistoryListClient is used by classes that need to save their 
	history data (undo data). There may be more than one of these and they follow changes of HistoryList.\n
	\n
	When there is some change in the HistoryList---e.g. a command is added or a navagation occurs---a notification 
	is sent out to all the HistoryListClient classes to make the change or go to the specified version.
	In Solarium there are only two instances of HistoryListClient: in the Preset class and ViewModel class.\n
	\n
	HistoryList\n
	\n
	In Solarium this was intended to store Command-based classes but was written as a template so it might end up being 
	reusable elsewhere. HistoryList consists of an array of branches. The array is indexed by BranchTag which are values 
	assigned in order as branches are added. So BranchTags are not stored because they are the index. \n
	\n
	Each branch holds a list (vector) of edits, i.e. commands. In Solarium these are commands similar to those in the 
	Gang-of-Four Command design pattern except that they're only executed once. One main reason for holding on to them 
	in the history is to keep the timestamp and description of each command. This list is indexed by version, and since 
	the HistoryList contains every version they are consecutive in the list. Like the branch array version numbers don't 
	need to be stored.\n
	\n
	Version\n
	\n
	Besides holding all the branches, and the edits for each branch, the HistoryList also maintains the current version.
	A Version actually has two parts, a BranchTag to identify which branch the edit is in, and a Revision which is the 
	"version" within the branch. Revisions do not restart counting at zero when a new branch is created. Rather they 
	continue counting from the previous version. So take for example a Version that has BranchTag 4 and Revision 45. If 
	a new branch 5 were created that branched off of this Version it would have BranchTag 5 and Revision 46. Later there 
	could be a new branch 12 that branches off from the same place. This Version would have BranchTag 12 and Revision 46. 
	So Versions can have the same Revision number but different BranchTag numbers.
	Also, note that the original branchpoint Version (i.e. BranchTag 4 and Revision 45) and all prior Versions are now 
	also considered to be in branch 5 and branch 12. A "branchpoint" is a Version where a branch has occurred.\n
	\n
	Parents\n
	\n
	Each branch has parentVersion. This identifies the Version that it branched off of. It's the place this branched 
	diverged from some other branch. So you'll notince that there's a way to traverse backward along a branch but no 
	easy way to traverse forward. For the current purposes of this program it's not a problem. And it saves space.\n
	\n
	HistoryListClient\n
	\n
	The structure of HistoryListClient is similar to that of HistoryList. Instead of storing commands, it stores the state 
	of the item (i.e. a copy of it) it's managing at a particular version. However, HistoryListClient only needs to store 
	edit history when the item it's managing changes. So like HistoryList it has a list of branches. Unlike HistoryList it 
	doesn't have to store a branch if that branch doesn't have any edits. So a BranchTag can't be used as an index to a 
	list; instead a BranchTag is used as a key to a map.\n
	\n
	Since HistoryListClient only contain those changes made to the item managed, it takes a bit of effort to find the 
	state that the item was in when you want to go to a particular version in history. If and item was not changed at 
	a particular version then there won't be any entry for that version, which is almost always the case. So you need 
	to search backwards along the branch to find the last time that item was changed to get that state the item should 
	be in at the requested version. To do this you need information for the entire branch chain. It can be costly to keep 
	a copy of this branch chain in every HistoryListClient since there may be a lot of clients (which is the case in Solarium) 
	and the inforation is the same anyway and would be redundant. So this is the reason for the HistoryListSet class. 
	HistoryListSet manages the branch chain for a collection of HistoryListClients. this is passed to HistoryListClient 
	when reverting to some earlier version.

*/
template<typename ItemType>
class HistoryList
{
private:

	struct Branch
	{
		Branch() : Branch({ INVALID_REVISION, NO_BRANCH }) {}
		Branch(Version parentVersion_) : parentVersion(parentVersion_) {}
		Version					parentVersion{};
		std::vector<ItemType>	items;
		bool active{ true };

		Revision minRevision() const 
		{
			return (parentVersion.revision == INVALID_REVISION) ? 0 : parentVersion.revision + 1;
		}

		void Write(std::ostream & output, std::function<void(std::ostream&, ItemType&)> writeItemFunc)
		{
			parentVersion.Write(output);
			auto itemCount = static_cast<uint32_t>(items.size());
			output.write(reinterpret_cast<const char *>(&itemCount), sizeof(itemCount));
			for (auto& item : items)
			{
				writeItemFunc(output, item);
			}
			output.write(reinterpret_cast<const char *>(&active), sizeof(active));
		}

		void Read(std::istream& input, std::function<ItemType(std::istream&)> readItemFunc)
		{
			items.clear();
			parentVersion.Read(input);
			uint32_t itemCount;
			input.read(reinterpret_cast<char *>(&itemCount), sizeof(itemCount));
			for (size_t i = 0; i < itemCount; ++i)
			{
				items.push_back(readItemFunc(input));
			}
			input.read(reinterpret_cast<char *>(&active), sizeof(active));
		}
	};

	Version currentVersion{INVALID_REVISION, 0};
	std::vector<Branch> branches;

private:

	/** @brief Determines if the given version is less than the last version in its branch.
		
		The branch must not be empty.
		@param[in] version	The version to check. This must not be greater than the last version in the branch.
		@returns			true if the version is the last in a branch, otherwise false.
	*/
	inline bool IsLastInBranch(Version version) const
	{
		assert(version.branchTag < branches.size());
		assert(!branches[version.branchTag].items.empty());
		assert(version.revision <= (branches[version.branchTag].minRevision() + branches[version.branchTag].items.size() - 1));

		auto& branch = branches[version.branchTag];
		bool result = (version.revision == (branch.minRevision() + branch.items.size() - 1));
		return result;
	}

public:
	HistoryList()
	{
		// Add a "root" Branch with no parent version.
		branches.push_back(Branch());
	}

	/** @brief	Adds an item to the history list based on the current version position.

		If the current version is not the maximum version, a new branch will be added. The new version will then
		branch off of what was the current version.

		@param[in] item		The item to add.
		@returns			A pair. The first in the pair is a bool which is true if a new branch was started. false if otherwise.
							The second of the pair is the tag of the branch that was added. This is only valid if the bool is true.
	*/
	std::pair<bool, BranchTag> AddItem(ItemType&& item)
	{
		bool newBranchWasStarted{ false };
		BranchTag newBranchTag{ NO_BRANCH };

		// Is the current version the maximum version of a branch?
		if (currentVersion == GetMaxVersion())
		{
			branches[currentVersion.branchTag].items.push_back(std::move(item));
		}
		else
		{
			// No. So we need to create a new branch and add the item there
			auto parentVersion = GetCurrentVersionBasic();
			branches.push_back(Branch(parentVersion));	// add branch
			branches.back().items.push_back(std::move(item));

			newBranchWasStarted = true;
			newBranchTag = static_cast<BranchTag>(branches.size() - 1);

			currentVersion.branchTag = newBranchTag;
		}
		++currentVersion.revision;
		return std::make_pair(newBranchWasStarted, newBranchTag);
	}

	/** @brief Returns the number of items in a branch based on specified branch tag.

		The zero-based maximum branch version will be one less than the length.

		@param[in] branchTag			The tag of the branch.
		@returns						The number of items in the branch.
		@throws std::invalid_argument	If the branch tag doesn't exist in the history.
	*/
	inline uint32_t GetBranchLength(BranchTag branchTag) const
	{
		return branches[branchTag].minRevision() + branches[branchTag].items.size();
	}

	inline uint32_t GetCurrentBranchLength() const
	{
		return GetBranchLength(currentVersion.branchTag);
	}

	/** @brief Returns the Version that this branch originally branched off of.
	
		Note that the branchTag of the Version that's returned will not be the same as the branchTag 
		passed in since it is the branch that this one branched off of.
		@param[in] branchTag			The tag of the branch.
										Behavior is undefined if the branch doesn't exist.
		@returns						The Version that is the branch point.
										This version will equal NO_VERSION if this is the trunk and thus no branch point.
	*/
	Version GetBranchPoint(BranchTag branchTag) const
	{
		return branches[branchTag].parentVersion;
	}

	/** @brief Returns a reference to the item at the given version based, optionally based on specified branch tag.
		@param[in] Version			The version of the item to get. Valid range is 0 to GetMaxVersion() inclusive.
	*/
	const ItemType& GetItemAtVersion(Version version) const
	{
		assert(version.branchTag < branches.size());
		assert(version.revision < (branches[version.branchTag].minRevision() + branches[version.branchTag].items.size()));
		const Branch* pBranch = &branches[version.branchTag];

		while (version.revision < pBranch->minRevision())
		{
			pBranch = &branches[pBranch->parentVersion.branchTag];
		};
		assert((version.revision >= pBranch->minRevision()) && version.revision < (pBranch->minRevision() + pBranch->items.size()));

		// Now (version.revision >= branch.minRevision) must be true and version.revision is < the minRevision of the next branch.
		const ItemType& result = pBranch->items[version.revision - pBranch->minRevision()];
		return result;
	}
	
	/** @brief	Gets the current version translated in terms of the original branch tag for this revision.

		Used when creating a new branch to get a branch point.
		@returns The current version translated in terms of the original branch tag for this revision.
	*/
	Version GetCurrentVersionBasic() const
	{
		if (currentVersion.revision == INVALID_REVISION)
		{
			return currentVersion;
		}
		Version version = currentVersion;
		assert(version.branchTag < branches.size());
		assert(version.revision < (branches[version.branchTag].minRevision() + branches[version.branchTag].items.size()));
		const Branch* pBranch = &branches[version.branchTag];

		while (version.revision < pBranch->minRevision())
		{
			pBranch = &branches[pBranch->parentVersion.branchTag];
		};
		assert((version.revision >= pBranch->minRevision()) && version.revision < (pBranch->minRevision() + pBranch->items.size()));

		return Version(currentVersion.revision, gsl::narrow<BranchTag>(pBranch - &branches[0]));
	}

	Version GetCurrentVersion() const
	{
		return currentVersion;
	}

	Revision GetCurrentRevision() const
	{
		return currentVersion.revision;
	}

	void SetVersion(Version version)
	{
		assert(version.revision < branches[version.branchTag].minRevision() + branches[version.branchTag].items.size());
		currentVersion = version;
	}

	std::vector<Version> GetParentChain(Version version)
	{
		std::vector<Version> result;
		auto& branch = branches[version.branchTag];

		while (branch.parentVersion != NO_VERSION)
		{
			result.push_back(branch.parentVersion);
			branch = branches[branch.parentVersion.branchTag];
		}
		return result;
	}

	/** @brief Returns the maximum revision for a branch.

		@returns	the branch's maximum revision.
	*/
	inline Revision GetMaxRevision(BranchTag branchTag) const
	{
		assert(branchTag < branches.size());
		uint32_t length = GetBranchLength(branchTag);
		return length > 0 ? gsl::narrow<Revision>(length - 1) : INVALID_REVISION;
	}

	/** @brief Returns the maximum version for a branch.

		@returns	the branch's maximum version.
	*/
	inline Version GetMaxVersion(BranchTag branchTag) const
	{
		return{ GetMaxRevision(branchTag), branchTag };
	}

	/** @brief Returns the maximum version for the current branch.

		@returns	the current branch's maximum version.
	*/
	inline Version GetMaxVersion() const
	{
		return GetMaxVersion(currentVersion.branchTag);
	}

	bool CanUndo() const
	{
		return (currentVersion.revision != 0);
	}

	/** @brief Determines if the most recent revision in the current branch can be undone and deleted.

		A destructive undo can only undo revisions as far as its branch point. (The branch point revision
		is probably in the middle of a different branch and so can't be removed.)
		A destructive undo can cause a branch to have no revisions. In that case the current version
		will be the branch point but with the empty branchTag.
	*/
	inline bool CanUndoDestructive() const
	{
		// Is there another branch that has the current version in it?
		bool isUsed = false;
		for (auto& branch : branches)
		{
			if ((currentVersion.branchTag == branch.parentVersion.branchTag) && 
				(currentVersion.revision <= branch.parentVersion.revision))
			{
				isUsed = true;
				break;
			}
		}

		return 	!isUsed && 
				!branches[currentVersion.branchTag].items.empty() &&
				!(currentVersion.branchTag == 0 && currentVersion.revision == 0) && 
				IsLastInBranch(currentVersion);
	}

	std::vector<BranchTag> GetBranchTags() const
	{
		std::vector<BranchTag> branchTags;
		for (BranchTag i = 0; i < branches.size(); ++i)
		{
			if (branches[i].active)
			{
				branchTags.push_back(i);
			}
		}
		return branchTags;
	}

	void Undo()
	{
		if (currentVersion.revision > 0)
		{
			--currentVersion.revision;
		}
	}

	bool UndoDestructive()
	{
		bool wasUndone = false;
		if (CanUndoDestructive())
		{
			branches[currentVersion.branchTag].items.pop_back();
			--currentVersion.revision;
			wasUndone = true;
		}
		return wasUndone;
	}

	/** @brief	Determines if an undo operation can be performed on the history (from the current version) along the specified branch path.

		@returns	true if an undo operation can be performed, false otherwise.
	*/
	inline bool CanRedo() const
	{
		return static_cast<uint32_t>(currentVersion.revision + 1) < GetBranchLength(currentVersion.branchTag);
	}

	/** @brief	Moved the history's current version to the next version along the given branch.

		If there is no next version (i.e. CanRedo() returns false) then this function has no effect.

	*/
	void Redo()
	{
		if (CanRedo())
		{
			++currentVersion.revision;
		}
	}

	/** @brief	Permanently delete a branch and it's revisions from the undo history.

		A branch is not really deleted but only made inactive. A branch could be a parent 
		of some other branch which is still in use. So this will only delete revisions that 
		are after all branch point.\n
		\n
		Deleting a branch may result in other branches being pruned, because they are no longer depended upon 
		by the branch just deleted. So this function returns a list of branches that can be pruned and 
		the revision they can be pruned back to. We can use DeleteBranch(), here and in the clients, to  
		prune a branch as well as delete one because it has the same effect and it doesn't hurt to call it multiple times.\n
		\n
		If the current version was in the branch that was deleted, it

		@param[in]	branchTag		The tag identifying the branch to delete.
		@param[out] versionsToPrune	A list of branches that can be deleted/pruned in the clients. 
									For each version in the list, the branch is the branch deleted. The revision is the new maximum revision 
									in that branch. All revisions greater than this can be deleted.
	*/
	void DeleteBranch(BranchTag branchTag, std::vector<Version>& versionsToPrune)
	{
		// We want to know what if the highest revision in this branch that any other 
		// branch splits off of.
		Revision maxRev = (branches[branchTag].parentVersion.revision == INVALID_REVISION) ? 0 :  branches[branchTag].parentVersion.revision;
		for (auto& branch : branches)
		{
			if (branch.parentVersion.branchTag == branchTag)
			{
				maxRev = std::max(maxRev,branch.parentVersion.revision );
			}
		}
		assert((branches[branchTag].parentVersion.revision == INVALID_REVISION) ||	// <---- Special case for first branch
			(maxRev >= branches[branchTag].parentVersion.revision && maxRev <= (branches[branchTag].parentVersion.revision + branches[branchTag].items.size())));
		Revision newListSize = maxRev - branches[branchTag].parentVersion.revision;
		assert(newListSize <= branches[branchTag].items.size());
		branches[branchTag].items.resize(newListSize);
		branches[branchTag].active = false;
		versionsToPrune.push_back(Version(maxRev, branchTag));


		// Now if this branch has no more revisions, then it's no longer dependent on its parent. If the 
		// parent branch is also inactive, it may be possible to prune some more revisions off of that parent 
		// branch (if this branch pointed to a higher revision there than any other branch did.)
		// The easiest way to deal with that is to just delete that branch again. It doesn't hurt to call DeleteBranch 
		// on it again and this will work recursively.
		if (branches[branchTag].items.empty())
		{
			util::FreeVector(branches[branchTag].items);
			branches[branchTag].parentVersion.revision = INVALID_REVISION;
			auto parentBranchTag = branches[branchTag].parentVersion.branchTag;
			branches[branchTag].parentVersion.branchTag = NO_BRANCH;
			if (branches[parentBranchTag].active == false)
			{
				DeleteBranch(parentBranchTag, versionsToPrune);
			}
		}

		// What was the current version probably doesn't exist anymore. So choose a new one.
		currentVersion.branchTag = GetBranchTags().back();
		currentVersion.revision = GetMaxRevision(currentVersion.branchTag);
	}

	void Write(std::ostream& output, std::function<void(std::ostream&, ItemType&)> writeItemFunc)
	{
		currentVersion.Write(output);

		uint32_t numBranches = static_cast<uint32_t>(branches.size());
		output.write(reinterpret_cast<const char *>(&numBranches), sizeof(numBranches));
		for (auto& branch : branches)
		{
			branch.Write(output, writeItemFunc);
		}
	}

	void Read(std::istream& input, std::function<ItemType(std::istream&)> readItemFunc)
	{
		branches.clear();

		currentVersion.Read(input);

		uint32_t numBranches;
		input.read(reinterpret_cast<char *>(&numBranches), sizeof(numBranches));
		branches.resize(numBranches);	// Can't copy Branches, so allocate default Branches and then read them in.
		for (size_t i = 0; i < numBranches; ++i)
		{
			branches[i].Read(input, readItemFunc);
		}
	}
};

#endif  // HISTORYLIST_H_INCLUDED
