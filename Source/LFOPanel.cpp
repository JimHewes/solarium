/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <sstream>
#include <iomanip>
#include <math.h>
#include "LFOPanel.h"
#include "Preset.h"
#include "Command.h"

using std::vector;
using std::string;


String LFODelaySlider::getTextFromValue(double value)
{
	// Value range from 0 to 100 in steps of 100 ms. So, 0 to 10.0 seconds.
	std::ostringstream os;
	if (value == 0)
	{
		os << "0.0 ms";
	}
	else if (value < 10)
	{
		os << std::fixed << std::setprecision(1) << value * 100 << " ms";
	}
	else
	{
		os << std::fixed << std::setprecision(1) << value/10.0 << " sec";
	}
	return os.str().c_str();
}

double LFODelaySlider::getValueFromText(const String & text)
{
	double value = text.getDoubleValue();
	int valueInt = lrint(value * 10.0);
	return valueInt;
}

static const juce::Identifier paramIndexID = "paramIndex";

LFOPanel::LFOPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	, m_pCommandHistory(pCommandHistory)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 75;
	const int rightWidth = 105;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_shapeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_shapeLabel.setText("Shape:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_shapeLabel);
	m_shapeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> shapeList = m_pPreset->GetParameterListText(ParamID(elemID,lfoShape));;
	for (size_t i = 0; i < shapeList.size(); ++i)
	{
		m_shapeCombo.addItem(shapeList[i],i + 1);
	}
	m_shapeCombo.addListener(this);
	addAndMakeVisible(m_shapeCombo);

	yPos += rowHeight;
	m_rateLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_rateLabel.setText("Rate:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_rateLabel);
	m_rateSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_rateSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoRate));
	m_rateSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_rateSlider.setSkewFactor(0.28);
	m_rateSlider.getProperties().set(paramIndexID, lfoRate);
	m_rateSlider.addListener(this);
	addAndMakeVisible(m_rateSlider);

	m_rateSyncCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> rateSyncList = m_pPreset->GetParameterListText(ParamID(elemID, lfoRateSync));;
	for (size_t i = 0; i < rateSyncList.size(); ++i)
	{
		m_rateSyncCombo.addItem(rateSyncList[i], i + 1);
	}
	m_rateSyncCombo.addListener(this);
	addAndMakeVisible(m_rateSyncCombo);

	yPos += rowHeight;
	m_phaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseLabel.setText("Phase:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseLabel);
	m_phaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_phaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoPhase));
	m_phaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_phaseSlider.getProperties().set(paramIndexID, lfoPhase);
	m_phaseSlider.addListener(this);
	addAndMakeVisible(m_phaseSlider);

	yPos += rowHeight;
	m_delayStartLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_delayStartLabel.setText("DelStrt:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_delayStartLabel);
	m_delayStartSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_delayStartSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoDelayStart));
	m_delayStartSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_delayStartSlider.getProperties().set(paramIndexID, lfoDelayStart);
	m_delayStartSlider.addListener(this);
	addAndMakeVisible(m_delayStartSlider);

	yPos += rowHeight;
	m_fadeInLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fadeInLabel.setText("FadeIn:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fadeInLabel);
	m_fadeInSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fadeInSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoFadeIn));
	m_fadeInSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fadeInSlider.getProperties().set(paramIndexID, lfoFadeIn);
	m_fadeInSlider.addListener(this);
	addAndMakeVisible(m_fadeInSlider);

	yPos += rowHeight;
	m_fadeOutLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fadeOutLabel.setText("FadeOut:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fadeOutLabel);
	m_fadeOutSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fadeOutSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoFadeOut));
	m_fadeOutSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fadeOutSlider.getProperties().set(paramIndexID, lfoFadeOut);
	m_fadeOutSlider.addListener(this);
	addAndMakeVisible(m_fadeOutSlider);


	yPos += rowHeight;
	m_levelLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_levelLabel.setText("Level:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_levelLabel);
	m_levelSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_levelSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,lfoLevel));
	m_levelSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_levelSlider.setTextValueSuffix(" %");
	m_levelSlider.getProperties().set(paramIndexID, lfoLevel);
	m_levelSlider.addListener(this);
	addAndMakeVisible(m_levelSlider);

	yPos += rowHeight;
	m_clockSyncButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_clockSyncButton.setButtonText("Clock Sync");
	m_clockSyncButton.addListener(this);
	addAndMakeVisible(m_clockSyncButton);

	yPos += rowHeight;
	m_offsetButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_offsetButton.setButtonText("Offset");
	m_offsetButton.addListener(this);
	addAndMakeVisible(m_offsetButton);

	yPos += rowHeight;
	m_retriggerButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_retriggerButton.setButtonText("Retrigger");
	m_retriggerButton.addListener(this);
	addAndMakeVisible(m_retriggerButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	Update();
}

LFOPanel::~LFOPanel()
{
	m_shapeCombo.removeListener(this);
	m_rateSlider.removeListener(this);
	m_rateSyncCombo.removeListener(this);
	m_phaseSlider.removeListener(this);
	m_delayStartSlider.removeListener(this);
	m_fadeInSlider.removeListener(this);
	m_fadeOutSlider.removeListener(this);
	m_levelSlider.removeListener(this);
	m_clockSyncButton.removeListener(this);
	m_offsetButton.removeListener(this);
	m_retriggerButton.removeListener(this);
}

void LFOPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void LFOPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_shapeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoShape), m_shapeCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_rateSyncCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoRateSync), m_rateSyncCombo.getSelectedId() - 1));
	}
}

void LFOPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_rateSlider) ||
		(slider == &m_phaseSlider) ||
		(slider == &m_delayStartSlider) ||
		(slider == &m_fadeInSlider) ||
		(slider == &m_fadeOutSlider) ||
		(slider == &m_levelSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void LFOPanel::buttonClicked(Button* button)
{
	if (button == &m_clockSyncButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoClockSync), m_clockSyncButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_offsetButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoOffset), m_offsetButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_retriggerButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoRetrigger), m_retriggerButton.getToggleState() ? 1 : 0));
	}
}

void LFOPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_shapeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoShape)) + 1, NotificationType::dontSendNotification);
	m_rateSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoRate)), NotificationType::dontSendNotification);
	m_rateSyncCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, lfoRateSync)) + 1, NotificationType::dontSendNotification);
	m_phaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoPhase)), NotificationType::dontSendNotification);
	m_delayStartSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoDelayStart)), NotificationType::dontSendNotification);
	m_fadeInSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoFadeIn)), NotificationType::dontSendNotification);
	m_fadeOutSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoFadeOut)), NotificationType::dontSendNotification);
	m_levelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoLevel)), NotificationType::dontSendNotification);

	bool clockSyncOn = m_pPreset->GetParameterValue(ParamID(m_elemID, lfoClockSync)) != 0;
	m_clockSyncButton.setToggleState(clockSyncOn,NotificationType::dontSendNotification);
	m_offsetButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoOffset)) != 0,NotificationType::dontSendNotification);
	m_retriggerButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,lfoRetrigger)) != 0,NotificationType::dontSendNotification);

	m_rateSlider.setVisible(!clockSyncOn);
	m_rateSyncCombo.setVisible(clockSyncOn);
}


VibratoLFOPanel::VibratoLFOPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_elemID(elemID)
	, m_pCommandHistory(pCommandHistory)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 75;
	const int rightWidth = 105;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = 0;

	m_shapeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_shapeLabel.setText("Shape:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_shapeLabel);
	m_shapeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> shapeList = m_pPreset->GetParameterListText(ParamID(elemID,vlfoShape));;
	for (size_t i = 0; i < shapeList.size(); ++i)
	{
		m_shapeCombo.addItem(shapeList[i],i + 1);
	}
	m_shapeCombo.addListener(this);
	addAndMakeVisible(m_shapeCombo);

	yPos += rowHeight;
	m_rateLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_rateLabel.setText("Rate:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_rateLabel);
	m_rateSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_rateSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoRate));
	m_rateSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_rateSlider.setSkewFactor(0.28);
	m_rateSlider.getProperties().set(paramIndexID, lfoRate);
	m_rateSlider.addListener(this);
	addAndMakeVisible(m_rateSlider);

	m_rateSyncCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> rateSyncList = m_pPreset->GetParameterListText(ParamID(elemID, lfoRateSync));;
	for (size_t i = 0; i < rateSyncList.size(); ++i)
	{
		m_rateSyncCombo.addItem(rateSyncList[i], i + 1);
	}
	m_rateSyncCombo.addListener(this);
	addAndMakeVisible(m_rateSyncCombo);

	yPos += rowHeight;
	m_modWheelButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_modWheelButton.setButtonText("Mod Wheel");
	m_modWheelButton.addListener(this);
	addAndMakeVisible(m_modWheelButton);
	
	yPos += rowHeight;
	m_modWheelMaxLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_modWheelMaxLabel.setText("MWhl Max:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_modWheelMaxLabel);
	m_modWheelMaxSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_modWheelMaxSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoModWheelMax));
	m_modWheelMaxSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_modWheelMaxSlider.setTextValueSuffix(" %");
	m_modWheelMaxSlider.getProperties().set(paramIndexID, vlfoModWheelMax);
	m_modWheelMaxSlider.addListener(this);
	addAndMakeVisible(m_modWheelMaxSlider);

	yPos += rowHeight;
	m_phaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseLabel.setText("Phase:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseLabel);
	m_phaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_phaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoPhase));
	m_phaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_phaseSlider.getProperties().set(paramIndexID, lfoPhase);
	m_phaseSlider.addListener(this);
	addAndMakeVisible(m_phaseSlider);

	yPos += rowHeight;
	m_delayStartLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_delayStartLabel.setText("DelStrt:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_delayStartLabel);
	m_delayStartSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_delayStartSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoDelayStart));
	m_delayStartSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_delayStartSlider.getProperties().set(paramIndexID, lfoDelayStart);
	m_delayStartSlider.addListener(this);
	addAndMakeVisible(m_delayStartSlider);

	yPos += rowHeight;
	m_fadeInLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fadeInLabel.setText("FadeIn:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fadeInLabel);
	m_fadeInSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fadeInSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoFadeIn));
	m_fadeInSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fadeInSlider.getProperties().set(paramIndexID, lfoFadeIn);
	m_fadeInSlider.addListener(this);
	addAndMakeVisible(m_fadeInSlider);

	yPos += rowHeight;
	m_fadeOutLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fadeOutLabel.setText("FadeOut:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fadeOutLabel);
	m_fadeOutSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fadeOutSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoFadeOut));
	m_fadeOutSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fadeOutSlider.getProperties().set(paramIndexID, lfoFadeOut);
	m_fadeOutSlider.addListener(this);
	addAndMakeVisible(m_fadeOutSlider);

	yPos += rowHeight;
	m_levelLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_levelLabel.setText("Level:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_levelLabel);
	m_levelSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_levelSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,vlfoLevel));
	m_levelSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_levelSlider.setTextValueSuffix(" %");
	m_levelSlider.getProperties().set(paramIndexID, lfoLevel);
	m_levelSlider.addListener(this);
	addAndMakeVisible(m_levelSlider);

	yPos += rowHeight;
	m_clockSyncButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_clockSyncButton.setButtonText("Clock Sync");
	m_clockSyncButton.addListener(this);
	addAndMakeVisible(m_clockSyncButton);

	yPos += rowHeight;
	m_offsetButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_offsetButton.setButtonText("Offset");
	m_offsetButton.addListener(this);
	addAndMakeVisible(m_offsetButton);

	yPos += rowHeight;
	m_retriggerButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_retriggerButton.setButtonText("Retrigger");
	m_retriggerButton.addListener(this);
	addAndMakeVisible(m_retriggerButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(), yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	Update();
}

VibratoLFOPanel::~VibratoLFOPanel()
{
	m_shapeCombo.removeListener(this);
	m_rateSlider.removeListener(this);
	m_rateSyncCombo.removeListener(this);
	m_modWheelButton.removeListener(this);
	m_modWheelMaxSlider.removeListener(this);
	m_phaseSlider.removeListener(this);
	m_delayStartSlider.removeListener(this);
	m_fadeInSlider.removeListener(this);
	m_fadeOutSlider.removeListener(this);
	m_levelSlider.removeListener(this);
	m_clockSyncButton.removeListener(this);
	m_offsetButton.removeListener(this);
	m_retriggerButton.removeListener(this);
}


void VibratoLFOPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_shapeCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoShape), m_shapeCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_rateSyncCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoRateSync), m_rateSyncCombo.getSelectedId() - 1));
	}
}

void VibratoLFOPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_rateSlider) ||
		(slider == &m_modWheelMaxSlider) ||
		(slider == &m_phaseSlider) ||
		(slider == &m_delayStartSlider) ||
		(slider == &m_fadeInSlider) ||
		(slider == &m_fadeOutSlider) ||
		(slider == &m_levelSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void VibratoLFOPanel::buttonClicked(Button* button)
{
	if (button == &m_modWheelButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vlfoModWheel), m_modWheelButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_clockSyncButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, vlfoClockSync), m_clockSyncButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_offsetButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoOffset), m_offsetButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_retriggerButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, lfoRetrigger), m_retriggerButton.getToggleState() ? 1 : 0));
	}
}

void VibratoLFOPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	m_shapeCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoShape)) + 1, NotificationType::dontSendNotification);
	m_rateSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoRate)), NotificationType::dontSendNotification);
	m_rateSyncCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, vlfoRateSync)) + 1, NotificationType::dontSendNotification);
	m_modWheelButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoModWheel)) != 0,NotificationType::dontSendNotification);
	m_modWheelMaxSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoModWheelMax)), NotificationType::dontSendNotification);
	m_phaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoPhase)), NotificationType::dontSendNotification);
	m_delayStartSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoDelayStart)), NotificationType::dontSendNotification);
	m_fadeInSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoFadeIn)), NotificationType::dontSendNotification);
	m_fadeOutSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoFadeOut)), NotificationType::dontSendNotification);
	m_levelSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoLevel)), NotificationType::dontSendNotification);

	bool clockSyncOn = m_pPreset->GetParameterValue(ParamID(m_elemID, vlfoClockSync)) != 0;
	m_clockSyncButton.setToggleState(clockSyncOn ,NotificationType::dontSendNotification);
	m_offsetButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoOffset)) != 0,NotificationType::dontSendNotification);
	m_retriggerButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,vlfoRetrigger)) != 0,NotificationType::dontSendNotification);

	m_rateSlider.setVisible(!clockSyncOn);
	m_rateSyncCombo.setVisible(clockSyncOn);
}
