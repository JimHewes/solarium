/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef LOOPEGPANEL_H_INCLUDED
#define LOOPEGPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
#include "Slider2.h"
class CommandHistory;

class LoopEGPanel :
	public Component,
	public ComboBox::Listener,
	public Slider2::Listener,
	public Button::Listener
{
public:
	LoopEGPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID);
	virtual ~LoopEGPanel();
	virtual void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	virtual void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;


	std::vector<Label>		m_timeLabels;
	std::vector<SliderTime>	m_timeSliders;
	std::vector<ComboBox>	m_timeSlidersSync;
	std::vector<Label>		m_xLevelLabels;
	std::vector<Slider2>	m_xLevelSliders;
	std::vector<Label>		m_yLevelLabels;
	std::vector<Slider2>	m_yLevelSliders;

	Label			m_startLabel;
	ComboBox		m_startCombo;
	Label			m_keyOffLabel;
	ComboBox		m_KeyOffCombo;
	Label			m_slopeLabel;
	SliderProp		m_slopeSlider;
	ToggleButton	m_clockSyncButton;
	Label			m_repeatLabel;
	ComboBox		m_repeatCombo;
	ToggleButton	m_loopButton;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};



#endif  // LOOPEGPANEL_H_INCLUDED
