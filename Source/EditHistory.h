/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef EDITHISTORY_H_INCLUDED
#define EDITHISTORY_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <functional>
#include "Command.h"


using CellDragFunctiontype = std::function<void()>;

/** @brief A custom cell type that occupies every cell of the EditHistory table listbox.

	The only purpose of having a custom cell type is so that we can capture mouse drag events.
*/
class EditHistoryCustomCell : public Component
{
public:
	EditHistoryCustomCell(CommandHistory* pCommandHistory, CellDragFunctiontype cellDragFunction);
	
	void setRowAndColumn(const int rowNumber, const int columnId);
	void paint(Graphics &g) override;
	void mouseDown(const MouseEvent &  event) override;
	void mouseDrag(const MouseEvent &  event) override;

private:
	int m_rowNumber;
	int m_columnId;
	CommandHistory*	m_pCommandHistory;
	CellDragFunctiontype m_cellDragFunction;
};

class EditHistoryListBoxModel : public TableListBoxModel, public MouseListener
{
public:
	EditHistoryListBoxModel(CommandHistory* pCommandHistory, CellDragFunctiontype cellDragFunction);
	int getNumRows(void) override;
	void paintRowBackground(Graphics &g, int rowNumber, int width, int height, bool rowIsSelected) override;
	void paintCell(Graphics &g, int rowNumber, int columnId, int width, int height, bool rowIsSelected) override;
	Component* refreshComponentForCell(int rowNumber, int columnId, bool isRowSelected, Component* existingComponentToUpdate) override;

private:
	CommandHistory*	m_pCommandHistory;
	CellDragFunctiontype m_cellDragFunction;
};

class EditHistoryListBox : public TableListBox
{
public:
	EditHistoryListBox(EditHistoryListBoxModel* pEditHistoryListModel, CommandHistory* pCommandHistory);
	void mouseDown(const MouseEvent &  event) override;
	void mouseDrag(const MouseEvent &  event) override;
	bool keyPressed(const KeyPress & key) override;
	void OnCellDrag();
	void Update();

private:

	CommandHistory*	m_pCommandHistory;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		EditHistoryContentComponent
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////


/** @brief The ArpContentComponent occupies the entire Arp Edit window and is where all the content is placed.
*/
class EditHistoryContentComponent : public Component,
									public ApplicationCommandTarget,
									public TableHeaderComponent::Listener,
									public ComboBox::Listener,
									public Button::Listener
{
public:
	EditHistoryContentComponent(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget);
	EditHistoryContentComponent(const EditHistoryContentComponent&) = delete;
	EditHistoryContentComponent& operator=(const EditHistoryContentComponent&) = delete;
	void resized() override;
	void paint(Graphics &) override;

	// The following methods support the ApplicationCommandTarget interface
	ApplicationCommandTarget* getNextCommandTarget() override;
	void getAllCommands(Array<CommandID>& commands) override;
	void getCommandInfo(CommandID commandID, ApplicationCommandInfo& result) override;
	bool perform(const InvocationInfo& info) override;
	void mouseDown(const MouseEvent &  event) override;
	void mouseDrag(const MouseEvent &  event) override;

	// The following methods support the TableHeaderListener interface
	void tableColumnsChanged(juce::TableHeaderComponent *) override;
	void tableSortOrderChanged(juce::TableHeaderComponent *) override {};
	void tableColumnsResized(juce::TableHeaderComponent *) override;

	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void buttonClicked(Button* buttonThatWasClicked) override;
	void Update();

private:
	void UpdateBranchList(BranchTag branchTag);

	Preset*						m_pPreset{};
	CommandHistory*				m_pCommandHistory;
	ApplicationCommandTarget*	m_pDefaultCommandTarget;
	std::unique_ptr<EditHistoryListBox>			m_pListBox;
	std::unique_ptr<EditHistoryListBoxModel>	m_pListBoxModel;
	CommandHistory::EditHistoryChangedConnection m_editHistoryChangedConnection;
	Label						m_branchLabel;
	ComboBox					m_branchComboBox;
	TextButton					m_renameBranchButton;
	TextButton					m_deleteBranchButton;
};



class EditHistoryWindow : public DocumentWindow
{
public:
	EditHistoryWindow(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget, std::function<void(void)> closeFunction);
	EditHistoryWindow(const EditHistoryWindow&) = delete;
	void moved() override;
	void mouseDown(const MouseEvent &  event) override;
	void mouseDrag(const MouseEvent &  event) override;

	void closeButtonPressed() override
	{
		m_closeFunction();
	}

	void Update();
private:
	std::function<void(void)> m_closeFunction;
};



#endif  // EDITHISTORY_H_INCLUDED
