/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include "ControlPanel.h"
#include "AppCmdMgr.h"
#include "MidiManager.h"

extern const unsigned char imageLedOn[];
extern const unsigned char imageLedOff[];


ControlPanel::ControlPanel(Preset * pPreset, CommandHistory* pCommandHistory, ViewModel * pViewModel, MidiManager* pMidiManager)
	:m_pPreset(pPreset)
	,m_pViewModel(pViewModel)
	,m_pMidiManager(pMidiManager)
{
	ApplicationCommandManager* pAppCmdMgr = &GetGlobalCommandManager();
	assert(pAppCmdMgr);

	m_pMidiLabel = std::make_unique<Label>();
	m_pMidiLabel->setText("MIDI", NotificationType::dontSendNotification);
	m_pMidiLabel->setJustificationType(Justification::centred);
	addAndMakeVisible(m_pMidiLabel.get());

	m_pSendButton = std::make_unique<TextButton>("SendButton");
	m_pSendButton->setButtonText({});
	m_pSendButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
	m_pSendButton->setColour(TextButton::buttonColourId, Colour(0xff677b81));
	m_pSendButton->setColour(TextButton::buttonOnColourId, Colour(0xff95fffb));
	m_pSendButton->setCommandToTrigger(pAppCmdMgr, MIDISendPreset, false);
	addAndMakeVisible(m_pSendButton.get());

	m_pSendLabel = std::make_unique<Label>();
	m_pSendLabel->setText("Send", NotificationType::dontSendNotification);
	m_pSendLabel->setJustificationType(Justification::centred);
	addAndMakeVisible(m_pSendLabel.get());

	m_pReceiveButton = std::make_unique<TextButton>("ReceiveButton");
	m_pReceiveButton->setButtonText({});
	m_pReceiveButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
	m_pReceiveButton->setColour(TextButton::buttonColourId, Colour(0xff677b81));
	m_pReceiveButton->setColour(TextButton::buttonOnColourId, Colour(0xff95fffb));
	m_pReceiveButton->setCommandToTrigger(pAppCmdMgr, MIDIReceivePreset, false);
	addAndMakeVisible(m_pReceiveButton.get());

	m_pReceiveLabel = std::make_unique<Label>();
	m_pReceiveLabel->setText("Recv", NotificationType::dontSendNotification);
	m_pReceiveLabel->setJustificationType(Justification::centred);
	addAndMakeVisible(m_pReceiveLabel.get());

	m_pSyncButton = std::make_unique<PanelButtonSync>("Sync");
	addAndMakeVisible(m_pSyncButton.get());


	ParamID paramID(ElementID(ElementType::Home, 0), 0);
	
	paramID.subIndex = gloAssignState1;
	m_pAssign1Button = std::make_unique<PanelButtonPreset>("Assign 1", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pAssign1Button.get());
	
	paramID.subIndex = gloAssignState2;
	m_pAssign2Button = std::make_unique<PanelButtonPreset>("Assign 2", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pAssign2Button.get());
	
	paramID.subIndex = gloUnisonButton;
	m_pUnisonButton = std::make_unique<PanelButtonPreset>("Unison", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pUnisonButton.get());

	paramID.subIndex = gloSeqButton;
	m_pSeqOnButton = std::make_unique<PanelButtonPreset>("Seq On", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pSeqOnButton.get());

	paramID.subIndex = gloArpButton;
	m_pArpOnButton = std::make_unique<PanelButtonPreset>("Arp On", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pArpOnButton.get());

	m_pEnablePartLabel = std::make_unique<Label>();
	m_pEnablePartLabel->setText("Enable Part", NotificationType::dontSendNotification);
	m_pEnablePartLabel->setJustificationType(Justification::centred);
	addAndMakeVisible(m_pEnablePartLabel.get());

	paramID.elemType = ElementType::VCAMaster;
	paramID.portType = PortType::Input;
	paramID.subIndex = psiSource;
	paramID.portIndex = 0;
	m_pEnablePart1Button = std::make_unique<PanelButtonPreset>("1", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pEnablePart1Button.get());
	
	//paramID.subIndex = vcamEnablePart2Button;
	paramID.portIndex = 1;
	m_pEnablePart2Button = std::make_unique<PanelButtonPreset>("2", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pEnablePart2Button.get());

	//paramID.subIndex = vcamEnablePart3Button;
	paramID.portIndex = 2;
	m_pEnablePart3Button = std::make_unique<PanelButtonPreset>("3", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pEnablePart3Button.get());

	//paramID.subIndex = vcamEnablePart4Button;
	paramID.portIndex = 3;
	m_pEnablePart4Button = std::make_unique<PanelButtonPreset>("4", paramID, m_pPreset, pCommandHistory);
	addAndMakeVisible(m_pEnablePart4Button.get());

	auto doCompare = [this](PanelButton* pPanelButton)
	{
		if (m_pPreset)
		{
			m_pPreset->SetState(PresetState::Compare, !pPanelButton->GetState() );
		}
	};
	m_pCompareButton = std::make_unique<PanelButton>("Compare", doCompare);
	addAndMakeVisible(m_pCompareButton.get());

	auto doFXBypass = [this](PanelButton* pPanelButton)
	{
		if (m_pPreset)
		{
			m_pPreset->SetState(PresetState::FXBypass, !pPanelButton->GetState());
		}
	};
	m_pFXBypassButton = std::make_unique<PanelButton>("FX Byp", doFXBypass);
	addAndMakeVisible(m_pFXBypassButton.get());

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent(
		[this](Version, bool) { Update(); });

	m_stateChangedConnection = m_pPreset->SubscribeToStateChangedEvent(
		[this](PresetState stateType, bool enabled)
	{
		PanelButton* pButton = (stateType == PresetState::Compare) ? m_pCompareButton.get() : m_pFXBypassButton.get();
		pButton->SetState(enabled);
	});

}


void ControlPanel::Update()
{
	int value;

	value = m_pPreset->GetParameterValue(m_pAssign1Button->GetParamID());
	m_pAssign1Button->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pAssign2Button->GetParamID());
	m_pAssign2Button->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pUnisonButton->GetParamID());
	m_pUnisonButton->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pSeqOnButton->GetParamID());
	m_pSeqOnButton->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pArpOnButton->GetParamID());
	m_pArpOnButton->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pEnablePart1Button->GetParamID());
	m_pEnablePart1Button->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pEnablePart2Button->GetParamID());
	m_pEnablePart2Button->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pEnablePart3Button->GetParamID());
	m_pEnablePart3Button->SetState(value != 0);

	value = m_pPreset->GetParameterValue(m_pEnablePart4Button->GetParamID());
	m_pEnablePart4Button->SetState(value != 0);
}


void ControlPanel::resized()
{
	const int posY = 30;
	int posX = 12;
	const int incX = 50;

	m_pSendButton->setBounds(posX + (46 - 32) / 2, posY + 18, 32, 20);
	m_pSendLabel->setBounds(posX, posY + 40, 46, 14);
	posX += incX;
	m_pMidiLabel->setBounds(posX, 11, 46, 14);
	m_pReceiveButton->setBounds(posX + (46 - 32) / 2, posY+ 18, 32, 20);
	m_pReceiveLabel->setBounds(posX, posY + 40, 46, 14);
	posX += incX;
	m_pSyncButton->setTopLeftPosition(posX, posY);

	posX = 200;
	m_pAssign1Button->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pAssign2Button->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pUnisonButton->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pSeqOnButton->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pArpOnButton->setTopLeftPosition(posX, posY);


	posX = 500;
	m_pEnablePartLabel->setBounds(posX + 59, 11, 80, 14);
	m_pEnablePart1Button->setTopLeftPosition(posX, 30);
	posX += incX;
	m_pEnablePart2Button->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pEnablePart3Button->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pEnablePart4Button->setTopLeftPosition(posX, posY);

	posX += incX;
	posX += incX;
	m_pCompareButton->setTopLeftPosition(posX, posY);
	posX += incX;
	m_pFXBypassButton->setTopLeftPosition(posX, posY);
}	

void ControlPanel::paint(Graphics & g)
{
	g.fillAll(Colour(0xffC0C0C0));
	g.setColour(Colour(0xff606060));
	g.drawRect(getLocalBounds(), 2);

	Rectangle<int> r;
	r.setBounds(18, 18, 48, 2);
	g.fillRect(r);
	r.setBounds(104, 18, 46, 2);
	g.fillRect(r);

	r.setBounds(208, 18, 234, 2);
	g.fillRect(r);

	r.setBounds(508, 18, 50, 2);
	g.fillRect(r);
	r.setBounds(640, 18, 50, 2);
	g.fillRect(r);
}





