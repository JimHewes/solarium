/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <sstream>
#include <fstream>
#include <iomanip>
#include <cassert>
#include "gsl_util.h"
#include "AppCmdMgr.h"
#include "ArpEdit.h"
#include "Properties.h"
#include "Command.h"

using gsl::narrow;
extern void MakeBackupFile(std::string& currentFilepath);
static const juce::Identifier paramIndexID = "paramIndex";

String SliderGateLength::getTextFromValue(double value)
{
	assert(value >= 1 && value <= 40);

	std::ostringstream os;
	os << std::fixed << std::showpoint << std::setprecision(1) << (value / 10.0);
	return os.str();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpStep
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

ArpStep::ArpStep(int stepIndex, Preset* pPreset, CommandHistory* pCommandHistory)
	:m_stepIndex(stepIndex)
	, m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
{
	m_stepNumberLabel.setText(String(stepIndex + 1), NotificationType::dontSendNotification);
	m_stepNumberLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_stepNumberLabel);

	m_volumeSlider.setSliderStyle(Slider2::LinearBarVertical);
	ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, arpStepVolumeOffset + stepIndex };
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(paramID);
	m_volumeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_volumeSlider.setDefaultValue(100);
	m_volumeSlider.addListener(this);
	addAndMakeVisible(m_volumeSlider);

	m_gateLengthSlider.setSliderStyle(Slider2::LinearBar);
	paramID.subIndex = narrow<uint8_t>(arpStepGateLengthOffset + stepIndex);
	rangeInfo = m_pPreset->GetParameterRangeInfo(paramID);
	m_gateLengthSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_gateLengthSlider.addListener(this);
	addAndMakeVisible(m_gateLengthSlider);

	m_gateButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
	m_gateButton.addListener(this);
	m_gateButton.setColour(TextButton::buttonOnColourId, Colour(0xff95fffb));
	addAndMakeVisible(m_gateButton);

	Update();

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted) {	OnParameterChanged(paramID, value, isMuted); });

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool) { Update(); });

}

void ArpStep::resized()
{
	Rectangle<int> area(getLocalBounds());
	auto w = area.getWidth();
	auto h = area.getHeight();

	int gateLengthHeight = lrint(h * 0.1);	// Gate length slider is 10% of total height
	int gateHeight = lrint(h * 0.1);		// Gate on/off button is 10% of total height
	int vgap = lrint(h * 0.03);				// The vertical gap between controls.
	int stepNumberHeight = lrint(h * 0.1);	// step number label is 8%
	int volumeHeight = h - stepNumberHeight - gateLengthHeight - gateHeight - (vgap * 3);	// The volume slider height is whatever is left over.
	
	int hgap1 = lrint(w * 0.20);
	int volumeWidth = w - (hgap1 * 2);

	int hgap2 = lrint(w * 0.10);
	int gateLengthWidth = w - (hgap2 * 2);

	int gateWidth = gateLengthWidth;

	m_stepNumberLabel.setBounds(0, 0, w, stepNumberHeight);
	m_volumeSlider.setBounds(hgap1, stepNumberHeight, volumeWidth, volumeHeight);
	m_gateLengthSlider.setBounds(hgap2, stepNumberHeight + volumeHeight + vgap * 1, gateLengthWidth, gateLengthHeight);
	m_gateButton.setBounds(hgap2, stepNumberHeight + volumeHeight + gateLengthHeight + vgap * 2, gateWidth, gateHeight);
}

void ArpStep::paint(Graphics & g)
{
	g.fillAll(Colour(0xffcacaca));
	g.setColour(Colours::black);
	Rectangle<int> lb = getLocalBounds();
	g.drawLine(0, 0, static_cast<float>(lb.getWidth()), 0, 1);

	g.setColour(Colour(0xffa0a0a0));
	g.drawLine(0, 0, 0, static_cast<float>(lb.getHeight()), 1);

}

void ArpStep::sliderValueChanged(Slider2 * slider)
{
	ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, m_stepIndex };
	if (slider == &m_volumeSlider)
	{
		paramID.subIndex += arpStepVolumeOffset;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, lrint(m_volumeSlider.getValue())));
	}
	else if (slider == &m_gateLengthSlider)
	{
		paramID.subIndex += arpStepGateLengthOffset;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, lrint(m_gateLengthSlider.getValue())));
	}
}

void ArpStep::buttonClicked(Button *)
{
	ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, m_stepIndex + arpStepGateOffset};

	int currentValue = m_pPreset->GetParameterValue(paramID);
	currentValue = (currentValue & 1) ^ 1;
	m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, currentValue));

	Colour buttonColor(currentValue ? 0xffa5d8b8 : 0xff677b81);
	m_gateButton.setColour(TextButton::buttonColourId, buttonColor);
}

void ArpStep::OnParameterChanged(const ParamID & paramID, int value, bool isMuted)
{
	isMuted;
	if (paramID.elemType == ElementType::Arpeggiator)
	{
		if (paramID.subIndex == arpStepVolumeOffset + m_stepIndex)
		{
			m_volumeSlider.setValue(value, NotificationType::dontSendNotification);
		}
		else if (paramID.subIndex == arpStepGateLengthOffset + m_stepIndex)
		{
			m_gateLengthSlider.setValue(value, NotificationType::dontSendNotification);
		}
		else if (paramID.subIndex == arpStepGateOffset + m_stepIndex)
		{
			Colour buttonColor(value ? 0xffa5d8b8 : 0xff677b81);
			m_gateButton.setColour(TextButton::buttonColourId, buttonColor);
		}
	}
}

void ArpStep::Update()
{
	ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, 0 };

	paramID.subIndex = narrow<uint8_t>(arpStepVolumeOffset + m_stepIndex);
	m_volumeSlider.setValue(m_pPreset->GetParameterValue(paramID), NotificationType::dontSendNotification);

	paramID.subIndex = narrow<uint8_t>(arpStepGateLengthOffset + m_stepIndex);
	m_gateLengthSlider.setValue(m_pPreset->GetParameterValue(paramID), NotificationType::dontSendNotification);

	paramID.subIndex = narrow<uint8_t>(arpStepGateOffset + m_stepIndex);
	int gateValue = m_pPreset->GetParameterValue(paramID);

	Colour buttonColor(gateValue ? 0xffa5d8b8 : 0xff677b81);
	m_gateButton.setColour(TextButton::buttonColourId, buttonColor);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpControlsComponent
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

ArpControlsComponent::ArpControlsComponent(Preset * pPreset, CommandHistory* pCommandHistory)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	, m_holdButton("Hold", ParamID({{ElementType::Arpeggiator, 0}, arpHold}), pPreset, pCommandHistory)
{

	ElementID elemID(ElementType::Arpeggiator, 0);

	m_bpmLabel.setText("BPM", NotificationType::dontSendNotification);
	m_bpmLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_bpmLabel);
	m_bpmSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(ElementID(ElementType::Home, 0), gloBPM));
	m_bpmSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_bpmSlider.setDefaultValue(60);
	m_bpmSlider.addListener(this);
	addAndMakeVisible(m_bpmSlider);
	
	m_modeLabel.setText("Mode", NotificationType::dontSendNotification);
	m_modeLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_modeLabel);
	std::vector<std::string> modeTypeList = m_pPreset->GetParameterListText(ParamID(elemID, arpMode));;
	for (size_t i = 0; i < modeTypeList.size(); ++i)
	{
		m_modeComboBox.addItem(modeTypeList[i], i + 1);
	}
	m_modeComboBox.getProperties().set(paramIndexID, arpMode);
	m_modeComboBox.addListener(this);
	addAndMakeVisible(m_modeComboBox);

	m_octaveLabel.setText("Octave", NotificationType::dontSendNotification);
	m_octaveLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_octaveLabel);
	std::vector<std::string> octaveTypeList = m_pPreset->GetParameterListText(ParamID(elemID, arpOctave));;
	for (size_t i = 0; i < octaveTypeList.size(); ++i)
	{
		m_octaveComboBox.addItem(octaveTypeList[i], i + 1);
	}
	m_octaveComboBox.getProperties().set(paramIndexID, arpOctave);
	m_octaveComboBox.addListener(this);
	addAndMakeVisible(m_octaveComboBox);

	m_patternLabel.setText("Pattern", NotificationType::dontSendNotification);
	m_patternLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_patternLabel);
	m_patternText.setFont(Font(15.00f, Font::plain));
	m_patternText.setJustificationType(Justification::centred);
	m_patternText.setEditable(false, false, false);
	m_patternText.setColour(Label::backgroundColourId, Colour(0xffb0b0b0));
	m_patternText.setColour(TextEditor::textColourId, Colours::black);
	m_patternText.setColour(TextEditor::backgroundColourId, Colour(0x00000000));
	addAndMakeVisible(m_patternText);

	m_resolutionLabel.setText("Resolution", NotificationType::dontSendNotification);
	m_resolutionLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_resolutionLabel);
	std::vector<std::string> resTypeList = m_pPreset->GetParameterListText(ParamID(elemID, arpResolution));;
	for (size_t i = 0; i < resTypeList.size(); ++i)
	{
		m_resolutionComboBox.addItem(resTypeList[i], i + 1);
	}
	m_resolutionComboBox.getProperties().set(paramIndexID, arpResolution);
	m_resolutionComboBox.addListener(this);
	addAndMakeVisible(m_resolutionComboBox);

	m_noteLengthLabel.setText("Note Length", NotificationType::dontSendNotification);
	m_noteLengthLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_noteLengthLabel);
	m_noteLengthSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, arpNoteLength));
	m_noteLengthSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_noteLengthSlider.addListener(this);
	addAndMakeVisible(m_noteLengthSlider);

	m_velocityLabel.setText("Velocity", NotificationType::dontSendNotification);
	m_velocityLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_velocityLabel);
	std::vector<std::string> velocityTypeList = m_pPreset->GetParameterListText(ParamID(elemID, arpVelocity));;
	for (size_t i = 0; i < velocityTypeList.size(); ++i)
	{
		m_velocityComboBox.addItem(velocityTypeList[i], i + 1);
	}
	m_velocityComboBox.getProperties().set(paramIndexID, arpVelocity);
	m_velocityComboBox.addListener(this);
	addAndMakeVisible(m_velocityComboBox);
	
	m_patternLengthLabel.setText("Pattern Length", NotificationType::dontSendNotification);
	m_patternLengthLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_patternLengthLabel);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, arpPatternLength));
	for (int i = rangeInfo.minValue + 1; i <= rangeInfo.maxValue + 1; ++i)
	{
		m_patternLengthComboBox.addItem(String(i), i);
	}
	m_patternLengthComboBox.getProperties().set(paramIndexID, arpPatternLength);
	m_patternLengthComboBox.addListener(this);
	addAndMakeVisible(m_patternLengthComboBox);

	m_swingLabel.setText("Swing", NotificationType::dontSendNotification);
	m_swingLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(m_swingLabel);
	m_swingSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, arpSwing));
	m_swingSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_swingSlider.setTextValueSuffix(" %");
	m_swingSlider.addListener(this);
	addAndMakeVisible(m_swingSlider);

	addAndMakeVisible(m_holdButton);

	Update();

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted) 
	{	OnParameterChanged(paramID, value, isMuted); });

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool) 
	{ Update(); });


}

void ArpControlsComponent::resized()
{
	Rectangle<int> area(getLocalBounds());
	auto w = area.getWidth();
	auto h = area.getHeight();

	//int width = lrint(w * 0.1);
	int height = lrint(h * 0.35);
	int labelY = lrint(h * 0.1);
	int ctrlY = lrint(h * 0.5);
	int gap = lrint(w / 80.0);

	int bpmLeft = gap;
	int bpmWidth = lrint(w * 0.09);
	m_bpmLabel.setBounds(bpmLeft, labelY, bpmWidth, height);
	m_bpmSlider.setBounds(bpmLeft, ctrlY, bpmWidth, height);

	int modeLeft = bpmLeft + bpmWidth + gap;
	int modeWidth = lrint(w * 0.09);
	m_modeLabel.setBounds(modeLeft, labelY, modeWidth, height);
	m_modeComboBox.setBounds(modeLeft, ctrlY, modeWidth, height);

	int octaveLeft = modeLeft + modeWidth + gap;
	int octaveWidth = lrint(w * 0.06);
	m_octaveLabel.setBounds(octaveLeft, labelY, octaveWidth, height);
	m_octaveComboBox.setBounds(octaveLeft, ctrlY, octaveWidth, height);

	int patternLeft = octaveLeft + octaveWidth + gap;
	int patternWidth = lrint(w * 0.06);
	m_patternLabel.setBounds(patternLeft, labelY, patternWidth, height);
	m_patternText.setBounds(patternLeft, ctrlY, patternWidth, height);

	int resLeft = patternLeft + patternWidth + gap;
	int resWidth = lrint(w * 0.09);
	m_resolutionLabel.setBounds(resLeft, labelY, resWidth, height);
	m_resolutionComboBox.setBounds(resLeft, ctrlY, resWidth, height);

	int noteLengthLeft = resLeft + resWidth + gap;
	int noteLengthWidth = lrint(w * 0.1);
	m_noteLengthLabel.setBounds(noteLengthLeft, labelY, noteLengthWidth, height);
	m_noteLengthSlider.setBounds(noteLengthLeft, ctrlY, noteLengthWidth, height);

	int velocityLeft = noteLengthLeft + noteLengthWidth + gap;
	int velocityWidth = lrint(w * 0.10);
	m_velocityLabel.setBounds(velocityLeft, labelY, velocityWidth, height);
	m_velocityComboBox.setBounds(velocityLeft, ctrlY, velocityWidth, height);

	int patternLengthLeft = velocityLeft + velocityWidth + gap;
	int patternLengthWidth = lrint(w * 0.11);
	m_patternLengthLabel.setBounds(patternLengthLeft, labelY, patternLengthWidth, height);
	m_patternLengthComboBox.setBounds(patternLengthLeft + (patternLengthWidth / 4), ctrlY, patternLengthWidth /2, height);

	int swingLeft = patternLengthLeft + patternLengthWidth + gap;
	int swingWidth = lrint(w * 0.10);
	m_swingLabel.setBounds(swingLeft, labelY, swingWidth, height);
	m_swingSlider.setBounds(swingLeft, ctrlY, swingWidth, height);

	int holdLeft = swingLeft + swingWidth + (gap * 2);
	int holdWidth = lrint(w * 0.04);
	m_holdButton.setBounds(holdLeft, labelY, holdWidth, lrint(height * 2.3));

}

void ArpControlsComponent::sliderValueChanged(Slider2 * slider)
{
	ElementID elemID(ElementType::Arpeggiator, 0);

	if (slider == &m_bpmSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(ElementID(ElementType::Home, 0), gloBPM), lrint(m_bpmSlider.getValue())));
	}
	else if (slider == &m_noteLengthSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpNoteLength), lrint(m_noteLengthSlider.getValue())));
	}
	else if (slider == &m_swingSlider)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpSwing), lrint(m_swingSlider.getValue())));
	}
}

void ArpControlsComponent::comboBoxChanged(ComboBox * comboBoxThatHasChanged)
{
	assert(
		(comboBoxThatHasChanged == &m_modeComboBox) ||
		(comboBoxThatHasChanged == &m_octaveComboBox) ||
		(comboBoxThatHasChanged == &m_resolutionComboBox) ||
		(comboBoxThatHasChanged == &m_velocityComboBox) ||
		(comboBoxThatHasChanged == &m_patternLengthComboBox));

	ElementID elemID(ElementType::Arpeggiator, 0);

	// Extract the parameter index from the slider control.
	var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(elemID, *pVar);
		int value = comboBoxThatHasChanged->getSelectedId() - 1;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void ArpControlsComponent::Reset(CmdMacro* pCmdMacro)
{
	assert(pCmdMacro);
	ElementID elemID(ElementType::Arpeggiator, 0);
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpMode), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpOctave), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpResolution), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpVelocity), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpPatternLength), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpNoteLength), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpSwing), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpHold), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, arpPattern), 0));
	pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(ElementID(ElementType::Home, 0), gloBPM), 0));
}

void ArpControlsComponent::OnParameterChanged(const ParamID & paramID, int value, bool isMuted)
{
	paramID; value; isMuted;
	Update();
}

void ArpControlsComponent::Update()
{
	ElementID elemID(ElementType::Arpeggiator, 0);

	m_bpmSlider.setValue(m_pPreset->GetParameterValue(ParamID(ElementID(ElementType::Home, 0), gloBPM)), NotificationType::dontSendNotification);
	m_modeComboBox.setSelectedId(m_pPreset->GetParameterValue(ParamID(elemID, arpMode)) + 1, NotificationType::dontSendNotification);
	m_octaveComboBox.setSelectedId(m_pPreset->GetParameterValue(ParamID(elemID, arpOctave)) + 1, NotificationType::dontSendNotification);
	int patternValue = m_pPreset->GetParameterValue(ParamID(elemID, arpPattern));
	m_patternText.setText(patternValue == 0 ? "User" : String(patternValue), NotificationType::dontSendNotification);
	m_resolutionComboBox.setSelectedId(m_pPreset->GetParameterValue(ParamID(elemID, arpResolution)) + 1, NotificationType::dontSendNotification);
	m_noteLengthSlider.setValue(m_pPreset->GetParameterValue(ParamID(elemID, arpNoteLength)), NotificationType::dontSendNotification);
	m_velocityComboBox.setSelectedId(m_pPreset->GetParameterValue(ParamID(elemID, arpVelocity)) + 1, NotificationType::dontSendNotification);
	m_patternLengthComboBox.setSelectedId(m_pPreset->GetParameterValue(ParamID(elemID, arpPatternLength)) + 1, NotificationType::dontSendNotification);
	m_swingSlider.setValue(m_pPreset->GetParameterValue(ParamID(elemID, arpSwing)), NotificationType::dontSendNotification);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpStepsComponent
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

ArpStepsComponent::ArpStepsComponent(Preset * pPreset, CommandHistory* pCommandHistory)
	:m_pPreset(pPreset)
{
	// Create all 32 arpeggiator steps. They will always exist, but only sometimes be visible.
	for (int i = 0; i < 32; ++i)
	{
		auto pStep = std::make_unique<ArpStep>(i, pPreset, pCommandHistory);
		//pStep->setBounds((i%16) * pStep->getWidth(), (i/16) * pStep->getHeight(), pStep->getWidth(), pStep->getHeight());
		addAndMakeVisible(pStep.get());
		m_arpSteps.push_back(std::move(pStep));
		
		auto pStepMask = std::make_unique<ArpStepMask>();
		addAndMakeVisible(pStepMask.get());
		m_arpStepMasks.push_back(std::move(pStepMask));
	}

	Update();

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int value, bool isMuted) 
	{
		value; isMuted;
		ElementID elemID(ElementType::Arpeggiator, 0);
		if (paramID.elemType == ElementType::Arpeggiator && paramID.subIndex == arpPatternLength)
		{
			Update();
		}
	});

	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool) { Update(); });

}

void ArpStepsComponent::resized()
{
	//int patternLength = m_pPreset->GetParameterValue(ParamID(ElementID(ElementType::Arpeggiator, 0), arpPatternLength));
	//int numRows = 1 + (patternLength / 16);

	Rectangle<int> area(getLocalBounds());
	auto stepWidth = area.getWidth() / 16;
	auto stepHeight = area.getHeight() / 2;

	for (size_t i = 0; i < m_arpSteps.size(); ++i)
	{
		m_arpSteps[i]->setBounds((i % 16) * stepWidth, (i / 16) * stepHeight, stepWidth, stepHeight);
		m_arpStepMasks[i]->setBounds((i % 16) * stepWidth, (i / 16) * stepHeight, stepWidth, stepHeight);
	}

}

void ArpStepsComponent::Update()
{
	int patternLength = m_pPreset->GetParameterValue(ParamID(ElementID(ElementType::Arpeggiator, 0), arpPatternLength));

	for (size_t i = 0; i < m_arpSteps.size(); ++i)
	{
		m_arpStepMasks[i]->setVisible(static_cast<int>(i) > patternLength);
	}
	resized();
}

void ArpStepsComponent::Reset(CmdMacro* pCmdMacro, uint8_t resetflags)
{
	assert((resetflags & 0xF8) == 0);	// only allow the first three bit flags

	assert(pCmdMacro);
	ElementID elemID(ElementType::Arpeggiator, 0);

	if (resetflags & ArpStepsComponent::flgVolume)
	{
		for (size_t i = 0; i < m_arpSteps.size(); ++i)
		{
			pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, i + arpStepVolumeOffset), 100));
		}
	}
	if (resetflags & ArpStepsComponent::flgGateLength)
	{
		for (size_t i = 0; i < m_arpSteps.size(); ++i)
		{
			pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, i + arpStepGateLengthOffset), 1));
		}
	}
	if (resetflags & ArpStepsComponent::flgGate)
	{
		for (size_t i = 0; i < m_arpSteps.size(); ++i)
		{
			pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(ParamID(elemID, i + arpStepGateOffset), true));
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpContentComponent
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

ArpContentComponent::ArpContentComponent(Preset * pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget)
	: m_arpControlComponent(pPreset, pCommandHistory)
	, m_arpStepComponent(pPreset, pCommandHistory)
	, m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	, m_pDefaultCommandTarget(pDefaultCommandTarget)
{
	addAndMakeVisible(m_arpControlComponent);
	addAndMakeVisible(m_arpStepComponent);

	m_pArpMenuModel = std::make_unique<ArpMenuBarModel>();
	m_pArpMenuComponent = std::make_unique<MenuBarComponent>(m_pArpMenuModel.get());
	addAndMakeVisible(m_pArpMenuComponent.get());

	setSize(pPropertiesFile->getValue(propArpWindowWidth).getIntValue(),
			pPropertiesFile->getValue(propArpWindowHeight).getIntValue());
}

void ArpContentComponent::resized()
{
	Rectangle<int> area(getLocalBounds());
	if (area.getWidth() % 16 != 0)
	{
		// This will caused resized() to be called again with a size that is a multiple of 16, and the path below will be taken.
		setSize(((area.getWidth() + 15) / 16) * 16, area.getHeight());
	}
	else
	{
		m_pArpMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));

		int controlCompHeight = lrint(getHeight() * 0.10);
		m_arpControlComponent.setBounds(area.removeFromTop(controlCompHeight));
		m_arpStepComponent.setBounds(area);
		//m_arpControlComponent.setBounds(0, 0, getWidth(), controlCompHeight);
		//m_arpStepComponent.setBounds(0, controlCompHeight, getWidth(), getHeight() - controlCompHeight);

		pPropertiesFile->setValue(propArpWindowWidth, getWidth());
		pPropertiesFile->setValue(propArpWindowHeight, getHeight());
	}
}

ApplicationCommandTarget* ArpContentComponent::getNextCommandTarget()
{
	//return findFirstTargetParentComponent();
	return m_pDefaultCommandTarget;
}

void ArpContentComponent::getAllCommands(Array<CommandID>& commands)
{
	// this returns the set of all commands that this target can perform..
	const CommandID ids[] = {
		FileNew ,
		FileOpen,
		FileSave,
		FileSaveAs,
		ResetArpVolumes,
		ResetArpGateLengths,
		ResetArpGates,
		ResetArpParameters,
		EditArp
	};
	commands.addArray(ids, numElementsInArray(ids));
}

void ArpContentComponent::getCommandInfo(CommandID commandID, juce::ApplicationCommandInfo& result)
{
	const String fileCategory("File");
	const String resetCategory("Reset");
	const String editCategory("Edit");

	switch (commandID)
	{
	case FileNew:
		result.setInfo("New", "Clears all arpeggiator settings and starts new.", fileCategory, 0);
		result.addDefaultKeypress('n', ModifierKeys::commandModifier);
		break;
	case FileOpen:
		result.setInfo("Open", "Opens an arpeggiator pattern file.", fileCategory, 0);
		result.addDefaultKeypress('o', ModifierKeys::commandModifier);
		break;
	case FileSave:
		result.setInfo("Save", "Saves the current arpeggiator pattern to a disk file.", fileCategory, 0);
		result.addDefaultKeypress('s', ModifierKeys::commandModifier);
		break;
	case FileSaveAs:
		result.setInfo("Save As", "Saves the current arpeggiator pattern to a new disk file.", fileCategory, 0);
		result.addDefaultKeypress('s', ModifierKeys::shiftModifier | ModifierKeys::commandModifier);
		break;
	case ResetArpVolumes:
		result.setInfo("Step Volumes", "Reset all arpeggiator volumes.", resetCategory, 0);
		result.addDefaultKeypress('v', ModifierKeys::commandModifier);
		break;
	case ResetArpGateLengths:
		result.setInfo("Step Lengths", "Reset all arpeggiator step lengths.", resetCategory, 0);
		result.addDefaultKeypress('l', ModifierKeys::commandModifier);
		break;
	case ResetArpGates:
		result.setInfo("Step Gates", "Reset all arpeggiator gates.", resetCategory, 0);
		result.addDefaultKeypress('g', ModifierKeys::commandModifier);
		break;
	case ResetArpParameters:
		result.setInfo("Settings", "Reset all arpeggiator general settings.", resetCategory, 0);
		result.addDefaultKeypress('m', ModifierKeys::commandModifier);
		break;
	case EditArp:
		result.setInfo("Arp Editor...", "Open the arpeggiator edit window.", editCategory, 0);
		result.addDefaultKeypress('e', ModifierKeys::commandModifier);
		break;
	default:
		break;
	};
}

bool ArpContentComponent::perform(const InvocationInfo & info)
{
	switch (info.commandID)
	{
	case FileNew:
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
			m_arpStepComponent.Reset(pCmdMacro.get(), ArpStepsComponent::flgVolume | ArpStepsComponent::flgGateLength | ArpStepsComponent::flgGate);
			m_arpControlComponent.Reset(pCmdMacro.get());
			pCmdMacro->SetActionName("Arp New");
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}
		break;
	case FileOpen:
		DoFileOpen();
		break;
	case FileSave:
		DoFileSave();
		break;
	case FileSaveAs:
		DoFileSaveAs();
		break;
	case EditArp:
		{	// Pressing the key command for opening the Arp Editor when it's open and to the front will then close it.
			DocumentWindow *pWindow = dynamic_cast<DocumentWindow*>(getParentComponent());
			pWindow->closeButtonPressed();
		}
		break;
	case ResetArpVolumes:
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
			m_arpStepComponent.Reset(pCmdMacro.get(), ArpStepsComponent::flgVolume);
			pCmdMacro->SetActionName("Reset Arp");
			pCmdMacro->SetDetails("Step Volumes");
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}
		break;
	case ResetArpGateLengths:
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
			m_arpStepComponent.Reset(pCmdMacro.get(), ArpStepsComponent::flgGateLength);
			pCmdMacro->SetActionName("Reset Arp");
			pCmdMacro->SetDetails("Step Lengths");
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}
		break;
	case ResetArpGates:
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
			m_arpStepComponent.Reset(pCmdMacro.get(), ArpStepsComponent::flgGate);
			pCmdMacro->SetActionName("Reset Arp");
			pCmdMacro->SetDetails("Step Gates");
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}
		break;
	case ResetArpParameters:
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
			m_arpControlComponent.Reset(pCmdMacro.get());
			pCmdMacro->SetActionName("Reset Arp");
			pCmdMacro->SetDetails("Settings");
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}
		break;
	default:
		return false;
	};
	return true;
}


/** @brief Loads a pattern file (.pat) into the editor.

	Pattern files do not contain all the arpeggiator settings. They only contain the step information 
	and the pattern length.\n
	\n
	Note on the pattern length: \n
	Pattern files specify the pattern length by setting the high bit of the step value in the last step.
	There must be only one step with the high bit set.
	When the Solaris reads a pattern file from the CF card, it recognizes this high bit. That is, immediately 
	after loading it will PLAY the pattern length according to the high bit that is set. But it does not set 
	the Pattern Length parameter, as you can see in the display, and it does not save the correct pattern 
	length to the preset file when you save the preset. So if you save the preset, switch to a different preset 
	and then switch back, the pattern length is then incorrect. It's not the one from the pattern file but it's 
	the original value that was shown on the Solaris display.
*/
void ArpContentComponent::DoFileOpen()
{
	File lastDirectory(pPropertiesFile->getValue(propLastArpDirectory));

	FileChooser fc("Choose an arp pattern file to open...",
		lastDirectory,
		"*.pat",
		true);
	if (fc.browseForFileToOpen())
	{
		File fileChosen = fc.getResult();
		String filepathChosen = fileChosen.getFullPathName();

		// Save the last directory that was chosen.
		if (filepathChosen.endsWith(fileChosen.getFileName()))
		{
			String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
			pPropertiesFile->setValue(propLastArpDirectory, var(lastDirStr));
		}

		File inFile(filepathChosen);
		try
		{
			m_currentFilePath = filepathChosen.toStdString();
			if (inFile.getSize() >= 132)
			{
				FileInputStream inFileStream(inFile);

				// The first 4-byte integer in the file should be a 1. (I assume this is the version number of the file format).
				uint32_t version;
				inFileStream.read(reinterpret_cast<void*>(&version), sizeof(version));
				if (version == 1)
				{
					ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, 0 };
					int patternLength = 31;

					vector<uint32_t> buf(32);
					inFileStream.read(reinterpret_cast<void*>(&buf[0]), buf.size() * sizeof(uint32_t));
					for (size_t i = 0; i < buf.size(); ++i)
					{
						uint32_t value = buf[i];
						if (value & 0x80000000)
						{
							patternLength = i;
						}

						// The reason std::min is used in the following lines is because the original Solaris pattern files contain values 
						// that are beyond the maximum allowed for volume and gate length. Since these were 0xFF and 0xFFF, they might have been
						// intended to be -1. But I don't know what -1 would mean. So anyway, just clamp it.

						uint32_t volume = std::min(127U, value & 0xFF);
						// For some reason, gate length step sizes of 0.1 are represented in the Solaris as steps of 6.5.
						// Rounding works here (instead of casting) because we only ever deal with fractions of x.5 and we want those to always be rounded up.
						uint32_t gateLength = std::min(40L, lrint(((value >> 8) & 0x0FFF) / 6.5));
						uint32_t gate = (value >> 20) & 1;

						paramID.subIndex = narrow<uint8_t>(arpStepVolumeOffset + i);
						m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, volume));
						paramID.subIndex = narrow<uint8_t>(arpStepGateLengthOffset + i);
						m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, gateLength));
						paramID.subIndex = narrow<uint8_t>(arpStepGateOffset + i);
						m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, gate));
					}

					paramID.subIndex = narrow<uint8_t>(arpPatternLength);
					m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, patternLength));

				}
				else
				{
					AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Open File Error", "The version number in the file is not correct.", "OK");
				}
			}
			else
			{
				AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Open File Error", "The file is not the correct size", "OK");
			}
		}
		catch (std::exception& e)
		{
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Open File Error", e.what(), "OK");
		}
	}
}

void ArpContentComponent::DoFileSave()
{
	if (m_currentFilePath.empty())
	{
		DoFileSaveAs();
	}
	else
	{
		SavePatternFile();
	}
}


juce::String GetUniqueArpFilename(const juce::String& directoryPath)
{
	assert(File(directoryPath).isDirectory());

	juce::String path;
	int i;
	for (i = 0; i < 10000; ++i)
	{
		path = File::addTrailingSeparator(directoryPath);
		path += "arp" + String(std::to_string(i)) + ".pat";

		if (!File(path).exists())
		{
			break;
		}
	}

	// Return a default if amazingly there are 10000 pattern files.
	if (i == 10000)
	{
		path = File::addTrailingSeparator(directoryPath);
		path += "newfile.pat";
	}

	return path.toStdString();
}



void ArpContentComponent::DoFileSaveAs()
{
	File directoryToSaveTo = m_currentFilePath.empty() ?
		File(pPropertiesFile->getValue(propLastArpDirectory)) :
		File(m_currentFilePath).getParentDirectory();

	juce::String initialBrowseFile = GetUniqueArpFilename(directoryToSaveTo.getFullPathName());

	FileChooser fc("Choose an arp pattern file to save...",
		File(initialBrowseFile),
		"*.pat",
		true);

	if (fc.browseForFileToSave(true))
	{
		File fileChosen = fc.getResult();
		String filepathChosen = fileChosen.getFullPathName();

		// save the last directory browsed
		if (filepathChosen.endsWith(fileChosen.getFileName()))
		{
			String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
			pPropertiesFile->setValue(propLastArpDirectory, var(lastDirStr));
		}

		// Ensure the filename always ends with the right extension.
		if (!filepathChosen.endsWith(".pat"))
		{
			filepathChosen += ".pat";
		}

		m_currentFilePath = filepathChosen.toStdString();

		SavePatternFile();
	}
}

void ArpContentComponent::SavePatternFile()
{
	try
	{
		MakeBackupFile(m_currentFilePath);

		std::ofstream outFile(m_currentFilePath, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!outFile)
		{
			throw std::runtime_error("Failed to open output file");
		}
		uint32_t version = 1;
		outFile.write(reinterpret_cast<char*>(&version), sizeof(version));

		ParamID paramID{ ElementType::Arpeggiator, 0, PortType::None, 0, 0 };

		// Note patternLength parameter starts at 0. So the real pattern length is one greater than the parameter value here.
		paramID.subIndex = arpPatternLength;
		int patternLength = m_pPreset->GetParameterValue(paramID);

		for (int i = 0; i < 32; ++i)
		{
			paramID.subIndex = narrow<uint8_t>(arpStepVolumeOffset + i);
			uint32_t volume = m_pPreset->GetParameterValue(paramID);

			// For some reason, gate length step sizes of 0.1 are represented in the Solaris as steps of 6.5.
			paramID.subIndex = narrow<uint8_t>(arpStepGateLengthOffset + i);
			uint32_t gateLength = static_cast<uint32_t>(6.5f * m_pPreset->GetParameterValue(paramID));

			paramID.subIndex = narrow<uint8_t>(arpStepGateOffset + i);
			uint32_t gate = m_pPreset->GetParameterValue(paramID);

			uint32_t composite = ((gate & 1) << 20) | ((gateLength & 0xFFF) << 8) | (volume & 0xFF);
			if (i == patternLength)
			{
				composite |= 0x80000000;
			}

			outFile.write(reinterpret_cast<char*>(&composite), sizeof(composite));
		}
	}
	catch (std::exception& e)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Save File Error", e.what(), "OK");
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpEditWindow
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

ArpEditWindow::ArpEditWindow(Preset* pPreset, CommandHistory* pCommandHistory, ApplicationCommandTarget* pDefaultCommandTarget, std::function<void(void)> closeFunction)
	: DocumentWindow("Arp Editor", Colour(0xFFE8E8E8), DocumentWindow::closeButton)
	, m_closeFunction(closeFunction)
{
	setUsingNativeTitleBar(true);
	setResizable(true, false);
	setVisible(true);

	ArpContentComponent* pArpContentComponent = new ArpContentComponent(pPreset, pCommandHistory, pDefaultCommandTarget);
	setContentOwned(pArpContentComponent, true);
	// this lets the command manager use keypresses that arrive in our window to send out commands
	addKeyListener(GetGlobalCommandManager().getKeyMappings());
	GetGlobalCommandManager().registerAllCommandsForTarget(pArpContentComponent);

}

void ArpEditWindow::moved()
{
	pPropertiesFile->setValue(propArpWindowLeft, getX());
	pPropertiesFile->setValue(propArpWindowTop, getY());
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//		ArpContentComponent Menu
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

StringArray ArpMenuBarModel::getMenuBarNames()
{
	const char* const names[] = { "File", "Reset", nullptr };

	return StringArray(names);
}

PopupMenu ArpMenuBarModel::getMenuForIndex(int menuIndex, const String& /*menuName*/)
{
	PopupMenu menu;
	ApplicationCommandManager* pAppCmdMgr = &GetGlobalCommandManager();

	if (menuIndex == 0)
	{
		menu.addCommandItem(pAppCmdMgr, FileNew);
		menu.addCommandItem(pAppCmdMgr, FileOpen);
		//menu.addCommandItem(pAppCmdMgr, FileClose);
		menu.addCommandItem(pAppCmdMgr, FileSave);
		menu.addCommandItem(pAppCmdMgr, FileSaveAs);

	}
	else if (menuIndex == 1)
	{
		menu.addCommandItem(pAppCmdMgr, ResetArpParameters);
		menu.addCommandItem(pAppCmdMgr, ResetArpVolumes);
		menu.addCommandItem(pAppCmdMgr, ResetArpGateLengths);
		menu.addCommandItem(pAppCmdMgr, ResetArpGates);
	}

	return menu;
}

void ArpMenuBarModel::menuItemSelected(int menuItemID, int /*topLevelMenuIndex*/)
{
	int nothing;
	nothing = menuItemID;
}


