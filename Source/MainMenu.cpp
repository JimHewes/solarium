/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "MainMenu.h"
#include "AppCmdMgr.h"
#include "MainComponent.h"

StringArray MainMenuBarModel::getMenuBarNames()
{
#if defined(_DEBUG)
	const char* const names[] = { "File", "Edit", "MIDI", "Help", "Debug", nullptr };
#else
	const char* const names[] = { "File", "Edit", "MIDI", "Help", nullptr };
#endif

	return StringArray(names);
}

PopupMenu MainMenuBarModel::getMenuForIndex(int menuIndex, const String& /*menuName*/)
{

	PopupMenu menu;
	ApplicationCommandManager* pAppCmdMgr = &GetGlobalCommandManager();

	if (menuIndex == 0)
	{
		menu.addCommandItem(pAppCmdMgr, FileNew);
		menu.addCommandItem(pAppCmdMgr, FileOpen);
		//menu.addCommandItem(pAppCmdMgr, FileClose);
		menu.addCommandItem(pAppCmdMgr, FileSave);
		menu.addCommandItem(pAppCmdMgr, FileSaveAs);

	}
	else if (menuIndex == 1)
	{
		menu.addCommandItem(pAppCmdMgr, EditUndo);
		menu.addCommandItem(pAppCmdMgr, EditUndoDestructive);
		menu.addCommandItem(pAppCmdMgr, EditRedo);

		PopupMenu setBookmarkSubMenu;
		PopupMenu goToBookmarkSubMenu;
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark1);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark2);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark3);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark4);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark5);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark6);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark7);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark8);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark9);
		setBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditSetBookmark0);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark1);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark2);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark3);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark4);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark5);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark6);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark7);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark8);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark9);
		goToBookmarkSubMenu.addCommandItem(pAppCmdMgr, EditGoToBookmark0);

		menu.addSubMenu ("Set Bookmark", setBookmarkSubMenu);
		menu.addSubMenu ("GoTo Bookmark", goToBookmarkSubMenu);
		menu.addCommandItem(pAppCmdMgr, EditHistory);
		menu.addSeparator();
		menu.addCommandItem(pAppCmdMgr, EditAdd);
		menu.addCommandItem(pAppCmdMgr, EditRemove);
		menu.addCommandItem(pAppCmdMgr, EditMute);
		menu.addCommandItem(pAppCmdMgr, EditUnMuteAll);
		menu.addCommandItem(pAppCmdMgr, EditSelectAll);
		menu.addCommandItem(pAppCmdMgr, EditExpandAll);
		menu.addCommandItem(pAppCmdMgr, EditCollapseAll);
		menu.addCommandItem(pAppCmdMgr, EditArp);
		menu.addCommandItem(pAppCmdMgr, EditName);
		menu.addSeparator();
		menu.addCommandItem(pAppCmdMgr, EditPreferences);
	}
	else if (menuIndex == 2)
	{
		menu.addCommandItem(pAppCmdMgr, MIDISendPreset);
		menu.addCommandItem(pAppCmdMgr, MIDIReceivePreset);
		menu.addCommandItem(pAppCmdMgr, MIDISync);
		menu.addSeparator();
		menu.addCommandItem(pAppCmdMgr, MIDISendSystem);
		menu.addCommandItem(pAppCmdMgr, MIDIReceiveSystem);
		menu.addSeparator();
		menu.addCommandItem(pAppCmdMgr, MIDISetup);
	}
	else if (menuIndex == 3)
	{
		menu.addCommandItem(pAppCmdMgr, HelpAbout);
	}
#if defined(_DEBUG)
	else if (menuIndex == 4)
	{
		menu.addCommandItem(pAppCmdMgr, DebugDumpPreset);
		menu.addCommandItem(pAppCmdMgr, DebugSaveHistory);
		menu.addCommandItem(pAppCmdMgr, DebugLoadHistory);
	}
#endif

	return menu;
}

void MainMenuBarModel::menuItemSelected(int menuItemID, int /*topLevelMenuIndex*/)
{
	int nothing;
	nothing = menuItemID;
}