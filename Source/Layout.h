/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef LAYOUT_H_INCLUDED
#define LAYOUT_H_INCLUDED

#include <vector>
#include "Preset.h"

struct ElementInput
{
	ElementID elemID;
	uint16_t height;
};


struct ElementOutput
{
	ElementID elemID;
	uint32_t xPos;
	uint32_t yPos;
};


std::vector<ElementOutput> ArrangeElements(std::vector<ElementInput> elements, int elementWidth);


#endif  // LAYOUT_H_INCLUDED
