/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <fstream>
//#include <algorithm>
//#include <iterator>	// for std::back_inserter

#define IN_MAINCOMPONENT_CPP
#include "Properties.h"

#include "MainComponent.h"
#include "AppCmdMgr.h"
#include "MainMenu.h"
#include "SettingsPanel.h"
#include "ControlPanel.h"
#include "MidiManager.h"
#include "PresetFile.h"

using std::string;
using std::make_unique;

//==============================================================================
MainContentComponent::MainContentComponent()
	:m_tabbedComponent(TabbedButtonBar::TabsAtTop)
{
    setSize (960, 540);

	LookAndFeel::setDefaultLookAndFeel(&lookAndFeel);

	m_propertiesOptions.applicationName = "Solarium";
	m_propertiesOptions.folderName = "Solarium";
	m_propertiesOptions.filenameSuffix = "config";
	m_propertiesOptions.storageFormat = PropertiesFile::storeAsXML;
	m_propertiesOptions.osxLibrarySubFolder = "Application Support/Solarium";	// needed for Mac 

	pPropertiesFile = make_unique<PropertiesFile>(m_propertiesOptions);
	DBG("Configuration file:");
	DBG(pPropertiesFile->getFile().getFullPathName());
	m_defaultProperties.setValue(String(propMainWindowWidth), var(1185));
	m_defaultProperties.setValue(String(propMainWindowHeight), var(714));
	m_defaultProperties.setValue(String(propZoom), var(100.0));
	m_defaultProperties.setValue(String(propMidiInDeviceName), var(""));
	m_defaultProperties.setValue(String(propMidiInDeviceID), var("0"));
	m_defaultProperties.setValue(String(propMidiOutDeviceName), var(""));
	m_defaultProperties.setValue(String(propMidiOutDeviceID), var("0"));
	m_defaultProperties.setValue(String(propDeviceID), var("0"));
	m_defaultProperties.setValue(String(propMidiSync), var("off"));
	m_defaultProperties.setValue(String(propSliderReset), var("1"));
	m_defaultProperties.setValue(String(propSliderEdit), var("2"));
	m_defaultProperties.setValue(String(propLastDirectory), File::getCurrentWorkingDirectory().getFullPathName());
	m_defaultProperties.setValue(String(propLastArpDirectory), File::getCurrentWorkingDirectory().getFullPathName());
	m_defaultProperties.setValue(String(propArpWindowWidth), 800);
	m_defaultProperties.setValue(String(propArpWindowHeight), 500);
	auto primaryDisplay = Desktop::getInstance().getDisplays().getPrimaryDisplay();
	Rectangle<int> screenRect;
	if (primaryDisplay != nullptr)
	{
		screenRect = primaryDisplay->userArea;
	}
	else
	{
		assert(false);
	}
	m_defaultProperties.setValue(String(propArpWindowLeft), (screenRect.getWidth() - 800) / 2);
	m_defaultProperties.setValue(String(propArpWindowTop), (screenRect.getHeight() - 500) / 2);
	m_defaultProperties.setValue(String(propHistoryWindowWidth), 537);
	m_defaultProperties.setValue(String(propHistoryWindowHeight), 383);
	m_defaultProperties.setValue(String(propHistoryWindowLeft), (screenRect.getWidth() - 537) / 2);
	m_defaultProperties.setValue(String(propHistoryWindowTop), (screenRect.getHeight() - 383) / 2);
	m_defaultProperties.setValue(String(propHistoryWindowCol1Width), 30);
	m_defaultProperties.setValue(String(propHistoryWindowCol2Width), 118);
	m_defaultProperties.setValue(String(propHistoryWindowCol3Width), 70);
	m_defaultProperties.setValue(String(propHistoryWindowCol4Width), 178);
	m_defaultProperties.setValue(String(propHistoryWindowCol5Width), 30);
	m_defaultProperties.setValue(String(propHistoryWindowCol6Width), 70);
	m_defaultProperties.setValue(String(propHistoryWindowCol1Visible), true);
	m_defaultProperties.setValue(String(propHistoryWindowCol2Visible), true);
	m_defaultProperties.setValue(String(propHistoryWindowCol3Visible), true);
	m_defaultProperties.setValue(String(propHistoryWindowCol4Visible), true);
	m_defaultProperties.setValue(String(propHistoryWindowCol5Visible), true);
	m_defaultProperties.setValue(String(propHistoryWindowCol6Visible), true);
	pPropertiesFile->setFallbackPropertySet(&m_defaultProperties);

	setSize(pPropertiesFile->getValue(propMainWindowWidth).getIntValue(), pPropertiesFile->getValue(propMainWindowHeight).getIntValue());


	m_pMainMenuModel = make_unique<MainMenuBarModel>();
	m_pMainMenuComponent = make_unique<MenuBarComponent>(m_pMainMenuModel.get());

	Rectangle<int> area(getLocalBounds());
	m_pMainMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));
	addAndMakeVisible(m_pMainMenuComponent.get());

	m_pCommandHistory = make_unique<CommandHistory>();

	m_pPreset = make_unique<Preset>(m_pCommandHistory.get());
	m_pViewModel = make_unique<ViewModel>(m_pPreset.get(), m_pCommandHistory.get());
	Command::SetTargetPreset(m_pPreset.get());
	Command::SetTargetViewModel(m_pViewModel.get());

	m_pSurface = make_unique<Surface>(m_pPreset.get(), m_pCommandHistory.get(), m_pViewModel.get());
	m_pSurface->setBounds(0, 0, 2000, 2000);

	//
	// The Surface is where the elements are placed. The Surface is put into 
	// another Component m_pSurfaceContainer which can be scrolled around within the viewport.
	// The surface is what gets scaled (zoomed). The m_pSurfaceContainer does not get scaled.
	m_pSurfaceContainer.setBounds(0, 0, 2000, 2000);
	m_pSurfaceContainer.addAndMakeVisible(m_pSurface.get(), false);
	m_surfaceViewport.SetSurface(m_pSurface.get());
	m_surfaceViewport.setViewedComponent(&m_pSurfaceContainer, false);
	m_surfaceViewport.setScrollBarsShown(true, true);
	m_pSurface->setTransform(AffineTransform().scaled(1.0f));

	m_pSystemPanel = std::make_unique<SystemPanel>(m_pPreset.get(), m_pCommandHistory.get(), ElementID(ElementType::System, 0));

	m_tabbedComponent.addTab("Preset", Colour(0xffE0E0E0), &m_surfaceViewport, false);
	m_tabbedComponent.addTab("System/MIDI", Colour(0xffE0E0E0), m_pSystemPanel.get(), false);
	addAndMakeVisible(m_tabbedComponent);

	m_pSettingsPanel = make_unique<SettingsPanel>(m_pPreset.get(), m_pCommandHistory.get(), m_pViewModel.get(), &m_pViewModel->ElementViewSelectionModel());
	addAndMakeVisible(m_pSettingsPanel.get());

	m_midiManager = make_unique<MidiManager>(m_pPreset.get(), m_pCommandHistory.get());

	m_pControlPanel = make_unique<ControlPanel>(m_pPreset.get(), m_pCommandHistory.get(), m_pViewModel.get(), m_midiManager.get());
	addAndMakeVisible(m_pControlPanel.get());
	
	OnZoomChanged(pPropertiesFile->getValue(propZoom).getDoubleValue());	// set the initial zoom level from the properties file.
	m_zoomChangedConnection = m_pSettingsPanel->ZoomChangedEvent.Connect([this](double zoom) { OnZoomChanged(zoom); });

	// Change the name in the titlebar if the preset changes.
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool)
	{
		string title("Solarium");
		string name = m_pPreset->GetName();
		if (!name.empty())
		{
			title += " - " + name;
		}
		
		getTopLevelComponent()->setName(title);
	});

	// Start off as if a FileNew command was done. It will be version 0 in the edit history and the furthest back that you can undo to.
	DoNew();

	resized();	// This is just to get the layout manager to do its layout and get things drawn.
}

MainContentComponent::~MainContentComponent()
{

	// MainMenuComponent has to be deleted before MainMenuModel otherwise a crash will occur when the model tries to 
	// remove a listener (the MenuComponent) and it no longer exists.
	m_pMainMenuComponent.reset();
	m_midiManager.reset();
	pPropertiesFile.reset();
}

void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
	if (m_pMainMenuComponent && m_pSettingsPanel && m_pControlPanel)
	{
		Rectangle<int> area(getLocalBounds());

		pPropertiesFile->setValue(propMainWindowWidth, area.getWidth());
		pPropertiesFile->setValue(propMainWindowHeight, area.getHeight());

		m_pMainMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));
		m_pSettingsPanel->setBounds(area.removeFromRight(220));
		m_pControlPanel->setBounds(area.removeFromBottom(100));
		m_tabbedComponent.setBounds(area);

	}
	
	//Rectangle<int> bounds = getBounds();
	//DBG("bounds x = " << bounds.getX() << ", y = " << bounds.getY() << ", width = " << bounds.getWidth() << "height = " << bounds.getHeight());
}

void MainContentComponent::OnZoomChanged(double zoom)
{
	m_pSurface->setTransform(AffineTransform().scaled(float(zoom/100)));

	auto lb = m_pSurface->getLocalBounds();

	int dim = int(2000 * zoom / 100);
	m_pSurfaceContainer.setBounds(0, 0, dim, dim);
}


//==============================================================================
// The following methods implement the ApplicationCommandTarget interface, allowing
// this window to publish a set of actions it can perform, and which can be mapped
// onto menus, keypresses, etc.

ApplicationCommandTarget* MainContentComponent::getNextCommandTarget()
{
	// this will return the next parent component that is an ApplicationCommandTarget (in this
	// case, there probably isn't one, but it's best to use this method in your own apps).
	return findFirstTargetParentComponent();
}

void MainContentComponent::getAllCommands(Array<CommandID>& commands)
{
	// this returns the set of all commands that this target can perform..
	const CommandID ids[] = {
		FileNew ,
		FileOpen,
		//FileClose,
		FileSave,
		FileSaveAs,
		//EditAdd,
		EditRemove,
		EditMute,
		EditUnMuteAll,
		EditSelectAll,
		EditExpandAll,
		EditCollapseAll,
		EditArp,
		EditHistory,
		EditUndo,
		EditUndoDestructive,
		EditSetBookmark1,
		EditSetBookmark2,
		EditSetBookmark3,
		EditSetBookmark4,
		EditSetBookmark5,
		EditSetBookmark6,
		EditSetBookmark7,
		EditSetBookmark8,
		EditSetBookmark9,
		EditSetBookmark0,
		EditGoToBookmark1,
		EditGoToBookmark2,
		EditGoToBookmark3,
		EditGoToBookmark4,
		EditGoToBookmark5,
		EditGoToBookmark6,
		EditGoToBookmark7,
		EditGoToBookmark8,
		EditGoToBookmark9,
		EditGoToBookmark0,
		EditRedo,
		EditPreferences,
		EditName,
		MIDISendSystem,
		MIDIReceiveSystem,
		MIDISendPreset,
		MIDIReceivePreset,
		MIDISync,
		MIDISetup,
		HelpAbout
#if defined(_DEBUG)
		,DebugDumpPreset,
		DebugSaveHistory,
		DebugLoadHistory
#endif
	};
	commands.addArray(ids, numElementsInArray(ids));
}

void MainContentComponent::getCommandInfo(CommandID commandID, juce::ApplicationCommandInfo& result)
{
	const String fileCategory("File");
	const String editCategory("Edit");
	const String midiCategory("MIDI");

	auto BookmarkName = [this](int bookmarkNumber) -> std::string
	{
		string s;
		Version ver = m_pCommandHistory->GetBookmark(bookmarkNumber);
		s = std::to_string(bookmarkNumber);
		s += ": ";
		if (ver.revision == INVALID_REVISION)
		{
			s += "empty";
		}
		else
		{
			s += m_pCommandHistory->GetBranchName(ver.branchTag);
			s += " [";
			s += std::to_string(ver.revision);
			s += "]";
		}
		return s;
	};

	switch (commandID)
	{
	case FileNew:
		result.setInfo("New","Clears any existing patch and starts new.", fileCategory,0);
		result.addDefaultKeypress('n', ModifierKeys::commandModifier);
		break;
	case FileOpen:
		result.setInfo("Open","Opens a patch file.", fileCategory,0);
		result.addDefaultKeypress('o', ModifierKeys::commandModifier);
		break;
	case FileSave:
		result.setInfo("Save","Saves the current patch to a disk file.", fileCategory,0);
		result.addDefaultKeypress('s', ModifierKeys::commandModifier);
		break;
	case FileSaveAs:
		result.setInfo("Save As","Saves the current patch to a new disk file.", fileCategory,0);
		result.addDefaultKeypress('s', ModifierKeys::shiftModifier | ModifierKeys::commandModifier);
		break;
	case EditRemove:
		result.setInfo("Remove Selected", "Removes selected elements in the View.", editCategory, 0);
		result.addDefaultKeypress(KeyPress::deleteKey, ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPadDelete, ModifierKeys::noModifiers);
		{
			bool canBeRemoved = (m_pViewModel->ConnectorSelectionModel().HasSelection() == false) ||
									m_pViewModel->CanBeRemoved(m_pViewModel->ConnectorSelectionModel().GetFirstSelection());

			auto& elemList = m_pViewModel->ElementViewSelectionModel();
			if(elemList.HasSelection())
			{
				auto pViewModel = m_pViewModel.get();
				if (elemList.end() != std::find_if(elemList.begin(), elemList.end(), [pViewModel](auto& elemID) {return !pViewModel->CanBeRemoved(elemID);}))
				{
					canBeRemoved = false;
				}
			}

			result.setActive(canBeRemoved);
		}
		break;
	case EditMute:
		result.setInfo("Toggle Mute", "Toggles the mute status of the selected wire int the View.", editCategory, 0);
		{
			bool canBeRemoved{ false };
			if (m_pViewModel->ConnectorSelectionModel().HasSelection())
			{
				canBeRemoved = m_pViewModel->CanBeRemoved(m_pViewModel->ConnectorSelectionModel().GetFirstSelection());
			}
			else if (m_pViewModel->ElementViewSelectionModel().HasSelection())
			{
				// For all the elements selected, if there is at least one output connector that can be removed (and thus muted)
				// then allow the toggle mute command.
				for (auto& elemID : m_pViewModel->ElementViewSelectionModel())
				{
					auto outputConnectors = m_pPreset->GetOutputConnectorsOfElement(elemID);
					for (auto& conn : outputConnectors)
					{
						if (m_pViewModel->CanBeRemoved(conn))
						{
							canBeRemoved = true;
							break;
						}
					}
					if (canBeRemoved)
					{
						break;
					}
				}
			}
			result.setActive(canBeRemoved);
		}
		result.addDefaultKeypress('m', ModifierKeys::commandModifier);
		break;
	case EditUnMuteAll:
		result.setInfo("UnMute All", "UnMutes all wires that are muted.", editCategory, 0);
		result.addDefaultKeypress('m', ModifierKeys::shiftModifier | ModifierKeys::commandModifier);
		break;
	case EditSelectAll:
		result.setInfo("Select All","Selects all elements in the View.", editCategory,0);
		result.addDefaultKeypress('a',ModifierKeys::commandModifier);
		break;
	case EditExpandAll:
		result.setInfo("Expand All","Expands all panels of all elements in the View.", editCategory,0);
		result.addDefaultKeypress('x',ModifierKeys::commandModifier);
		break;
	case EditCollapseAll:
		result.setInfo("Collapse All","Collapses all panels of all elements in the View to only those ports that have connections.", editCategory,0);
		result.addDefaultKeypress('c',ModifierKeys::commandModifier);
		break;
	case EditArp:
		result.setInfo("Arp Editor...", "Open the arpeggiator edit window.", editCategory, 0);
		result.addDefaultKeypress('e', ModifierKeys::commandModifier);
		break;
	case EditHistory:
		result.setInfo("History...", "Open the edit history window.", editCategory, 0);
		result.addDefaultKeypress('h', ModifierKeys::commandModifier);
		break;
	case EditUndo:
		result.setInfo("Undo", "Undo the last edit of the preset.", editCategory, 0);
		result.setActive(m_pCommandHistory->CanUndo());
		result.addDefaultKeypress('z', ModifierKeys::commandModifier);
		break;
	case EditUndoDestructive:
		result.setInfo("Undo!", "Undo the last edit and delete it.", editCategory, 0);
		result.setActive(m_pCommandHistory->CanUndoDestructive());
		result.addDefaultKeypress('z', ModifierKeys::shiftModifier | ModifierKeys::commandModifier);
		break;
	case EditRedo:
		result.setInfo("Redo", "Redo the edit that was most recently undone.", editCategory, 0);
		result.setActive(m_pCommandHistory->CanRedo());
		result.addDefaultKeypress('y', ModifierKeys::commandModifier);
		break;
	case EditPreferences:
		result.setInfo("Preferences", "Set application preferences.", editCategory, 0);
		result.addDefaultKeypress('p', ModifierKeys::commandModifier);
		break;
	case EditName:
		result.setInfo("Edit Name...", "Edit Name and Categories.", editCategory, 0);
		result.addDefaultKeypress('f', ModifierKeys::commandModifier);
		break;
	case EditSetBookmark1:		result.setInfo("Bookmark 1", "Set bookmark 1.", editCategory, 0);	result.addDefaultKeypress('1', ModifierKeys::commandModifier);	break;
	case EditSetBookmark2:		result.setInfo("Bookmark 2", "Set bookmark 2.", editCategory, 0);	result.addDefaultKeypress('2', ModifierKeys::commandModifier);	break;
	case EditSetBookmark3:		result.setInfo("Bookmark 3", "Set bookmark 3.", editCategory, 0);	result.addDefaultKeypress('3', ModifierKeys::commandModifier);	break;
	case EditSetBookmark4:		result.setInfo("Bookmark 4", "Set bookmark 4.", editCategory, 0);	result.addDefaultKeypress('4', ModifierKeys::commandModifier);	break;
	case EditSetBookmark5:		result.setInfo("Bookmark 5", "Set bookmark 5.", editCategory, 0);	result.addDefaultKeypress('5', ModifierKeys::commandModifier);	break;
	case EditSetBookmark6:		result.setInfo("Bookmark 6", "Set bookmark 6.", editCategory, 0);	result.addDefaultKeypress('6', ModifierKeys::commandModifier);	break;
	case EditSetBookmark7:		result.setInfo("Bookmark 7", "Set bookmark 7.", editCategory, 0);	result.addDefaultKeypress('7', ModifierKeys::commandModifier);	break;
	case EditSetBookmark8:		result.setInfo("Bookmark 8", "Set bookmark 8.", editCategory, 0);	result.addDefaultKeypress('8', ModifierKeys::commandModifier);	break;
	case EditSetBookmark9:		result.setInfo("Bookmark 9", "Set bookmark 9.", editCategory, 0);	result.addDefaultKeypress('9', ModifierKeys::commandModifier);	break;
	case EditSetBookmark0:		result.setInfo("Bookmark 0", "Set bookmark 0.", editCategory, 0);	result.addDefaultKeypress('0', ModifierKeys::commandModifier);	break;

	case EditGoToBookmark0:		
		result.setInfo(BookmarkName(0), "Go to bookmark 0.", editCategory, 0);	
		result.addDefaultKeypress('0', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad0, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark1:		
		result.setInfo(BookmarkName(1), "Go to bookmark 1.", editCategory, 0);	
		result.addDefaultKeypress('1', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad1, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark2:		
		result.setInfo(BookmarkName(2), "Go to bookmark 2.", editCategory, 0);	
		result.addDefaultKeypress('2', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad2, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark3:		
		result.setInfo(BookmarkName(3), "Go to bookmark 3.", editCategory, 0);	
		result.addDefaultKeypress('3', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad3, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark4:
		result.setInfo(BookmarkName(4), "Go to bookmark 4.", editCategory, 0);	
		result.addDefaultKeypress('4', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad4, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark5:
		result.setInfo(BookmarkName(5), "Go to bookmark 5.", editCategory, 0);	
		result.addDefaultKeypress('5', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad5, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark6:
		result.setInfo(BookmarkName(6), "Go to bookmark 6.", editCategory, 0);	
		result.addDefaultKeypress('6', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad6, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark7:
		result.setInfo(BookmarkName(7), "Go to bookmark 7.", editCategory, 0);	
		result.addDefaultKeypress('7', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad7, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark8:
		result.setInfo(BookmarkName(8), "Go to bookmark 8.", editCategory, 0);	
		result.addDefaultKeypress('8', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad8, ModifierKeys::noModifiers);
		break;
	case EditGoToBookmark9:
		result.setInfo(BookmarkName(9), "Go to bookmark 9.", editCategory, 0);	
		result.addDefaultKeypress('9', ModifierKeys::noModifiers);
		result.addDefaultKeypress(KeyPress::numberPad9, ModifierKeys::noModifiers);
		break;
	case MIDISendSystem:
		result.setInfo("Send System", "Sends all System/MIDI parameters to the Solaris via MIDI.", midiCategory, 0);
		result.addDefaultKeypress('d', ModifierKeys::commandModifier);
		break;
	case MIDIReceiveSystem:
		result.setInfo("Receive System", "Retrieves all System/MIDI from the Solaris via MIDI.", midiCategory, 0);
		result.addDefaultKeypress('i', ModifierKeys::commandModifier);
		break;
	case MIDISendPreset:
		result.setInfo("Send Preset", "Sends the edited preset to the Solaris via MIDI.", midiCategory, 0);
		result.addDefaultKeypress('t', ModifierKeys::commandModifier);
		break;
	case MIDIReceivePreset:
		result.setInfo("Receive Preset", "Retrieves the current Solaris preset via MIDI.", midiCategory, 0);
		result.addDefaultKeypress('r', ModifierKeys::commandModifier);
		break;
	case MIDISync:
		result.setInfo("Toggle Midi Sync", "Toggle the MIDI sync state.", midiCategory, 0);
		result.addDefaultKeypress('q', ModifierKeys::commandModifier);
		break;
	case MIDISetup:
		result.setInfo("Setup","Allows you to set the input and output MIDI devices.",midiCategory,0);
		result.addDefaultKeypress('d',ModifierKeys::commandModifier);
		break;
	case HelpAbout:
		result.setInfo("About", "About Solarium.", "Help", 0);
		result.addDefaultKeypress('b', ModifierKeys::commandModifier);
		break;
#if defined(_DEBUG)
	case DebugDumpPreset:
		result.setInfo("Dump Preset", "Dump the current preset paramaters to a text file in the current directory.", "Debug", 0);
		break;
	case DebugSaveHistory:
		result.setInfo("Save History", "Saves the undo history to a file in the current directory.", "Debug", 0);
		break;
	case DebugLoadHistory:
		result.setInfo("Load History", "Loads the undo history from a file.", "Debug", 0);
		break;
#endif
	default:
		break;
	};
}

bool MainContentComponent::perform(const InvocationInfo& info)
{
	switch (info.commandID)
	{
	case FileNew:
		DoNew();
		break;
	case FileOpen:
		DoOpen();
		break;
	case FileSave:
		DoSave();
		break;
	case FileSaveAs:
		DoSaveAs();
		break;
	case EditRemove:
		m_pSurface->RemoveSelected();
		break;
	case EditMute:
		DoToggleMuteSelected();
		break;
	case EditUnMuteAll:
		DoUnMuteAll();
		break;
	case EditSelectAll:
		m_pSurface->SelectAll();
		break;
	case EditExpandAll:
		m_pSurface->ExpandAll();
		break;
	case EditCollapseAll:
		m_pSurface->CollapseAll();
		break;
	case EditArp:
		OpenArpEditWindow();
		break;
	case EditHistory:
		OpenEditHistoryWindow();
		break;
	case EditUndo:
		if (m_pCommandHistory->CanUndo())
		{
			m_pCommandHistory->Undo();
		}
		break;
	case EditUndoDestructive:
		if (m_pCommandHistory->CanUndoDestructive())
		{
			m_pCommandHistory->UndoDestructive();
		}
		break;
	case EditRedo:
		if (m_pCommandHistory->CanRedo())
		{
			m_pCommandHistory->Redo();
		}
		break;
	case EditPreferences:
		DoPreferences();
		break;
	case EditName:
		DoEditName();
		break;

	case EditSetBookmark1:	DoSetBookmark(1);	break;
	case EditSetBookmark2:	DoSetBookmark(2);	break;
	case EditSetBookmark3:	DoSetBookmark(3);	break;
	case EditSetBookmark4:	DoSetBookmark(4);	break;
	case EditSetBookmark5:	DoSetBookmark(5);	break;
	case EditSetBookmark6:	DoSetBookmark(6);	break;
	case EditSetBookmark7:	DoSetBookmark(7);	break;
	case EditSetBookmark8:	DoSetBookmark(8);	break;
	case EditSetBookmark9:	DoSetBookmark(9);	break;
	case EditSetBookmark0:	DoSetBookmark(0);	break;
	
	case EditGoToBookmark1:	m_pCommandHistory->GoToBookmark(1);	break;
	case EditGoToBookmark2:	m_pCommandHistory->GoToBookmark(2);	break;
	case EditGoToBookmark3:	m_pCommandHistory->GoToBookmark(3);	break;
	case EditGoToBookmark4:	m_pCommandHistory->GoToBookmark(4);	break;
	case EditGoToBookmark5:	m_pCommandHistory->GoToBookmark(5);	break;
	case EditGoToBookmark6:	m_pCommandHistory->GoToBookmark(6);	break;
	case EditGoToBookmark7:	m_pCommandHistory->GoToBookmark(7);	break;
	case EditGoToBookmark8:	m_pCommandHistory->GoToBookmark(8);	break;
	case EditGoToBookmark9:	m_pCommandHistory->GoToBookmark(9);	break;
	case EditGoToBookmark0:	m_pCommandHistory->GoToBookmark(0);	break;

	case MIDISendSystem:
		m_midiManager->DoSendSystem();
		break;
	case MIDIReceiveSystem:
		m_midiManager->DoReceiveSystem();
		break;
	case MIDISendPreset:
		DoMidiSend();
		break;
	case MIDIReceivePreset:
		DoMidiReceive();
		break;
	case MIDISync:
		DoToggleMidiSync();
		break;
	case MIDISetup:
		DoMidiSetup();
		break;
	case HelpAbout:
		DoHelpAbout();
		break;
#if (_DEBUG)
	case DebugDumpPreset:
		DoDumpPreset();
		break;
	case DebugSaveHistory:
		DoSaveHistory();
		break;
	case DebugLoadHistory:
		DoLoadHistory();
		break;
#endif
	default:
		return false;
	};

	return true;

}

void MainContentComponent::DoHelpAbout()
{
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"About Solarium",
		"Solarium version 0.13 by Jim Hewes. Copyright (c) 2015-2019.",
		"OK");
}

void MainContentComponent::DoNew()
{
	bool okToNew = true;

#if 0
	if (m_preset.IsModified()) /* If current preset was not saved since it was last modified */
	{
		okToNew = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Changes not saved!",
			"Your current preset has not been saved since it was last modified. Are you sure you want to delete the current preset and lose your changes?",
			String::empty,
			String::empty,
			this,
			nullptr);

	}
#endif

	if (okToNew)
	{
		m_pSurface->Clear();
		m_currentFilePath.clear();
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdNewPreset>());
		m_pSurface->Update();
	}
}

void MainContentComponent::DoOpen()
{
	bool okToOpen = true;
#if 0
	if (m_preset.IsModified())  /* If current preset was not saved since it was last modified */
	{
		okToOpen = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Changes not saved!",
			"Your current preset has not been saved since it was last modified. Are you sure you want to open the new file and lose your changes?",
			String::empty,
			String::empty,
			this,
			nullptr);
	}
#endif

	if (okToOpen)
	{
		File lastDirectory(pPropertiesFile->getValue(propLastDirectory));

		FileChooser fc("Choose a file to open...",
			lastDirectory,
			"*.pre",
			true);

		if (fc.browseForFileToOpen())
		{
			File fileChosen = fc.getResult();
			String filepathChosen = fileChosen.getFullPathName();

			// Save the last directory that was chosen.
			if (filepathChosen.endsWith(fileChosen.getFileName()))
			{
				String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
				pPropertiesFile->setValue(propLastDirectory,var(lastDirStr));
			}

			// This section is the equivalent of New
			m_pViewModel->ConnectorSelectionModel().Clear();	// Be careful to use Clear(), not clear(). The latter does not notify listeners.
			m_pViewModel->ElementViewSelectionModel().Clear();	// Be careful to use Clear(), not clear().
			m_pSurface->Clear();
			m_pSurface->Update();

			try
			{
				LoadPresetFile(filepathChosen.toStdString(), m_pCommandHistory.get());
				m_pSurface->Update();
				m_currentFilePath = filepathChosen.toStdString();
			}
			catch (std::runtime_error& e)
			{
				AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Open File Error", e.what(), "OK");
			}

		}
	}
}

void MakeBackupFile(string& currentFilepath)
{
	string backupFilepath = currentFilepath + ".bak";
	juce::File backupFile(backupFilepath);
	if (backupFile.exists() && !backupFile.isDirectory())
	{
		backupFile.deleteFile();
	}
	juce::File copyFromFile(currentFilepath);
	copyFromFile.copyFileTo(backupFile);
}

void MainContentComponent::DoSave()
{
#if 1
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"Save File",
		"Saving files to be implemented later.",
		"OK");
#else

	if (m_currentFilePath.empty())
	{
		DoSaveAs();
	}
	else
	{
		string msg = "Save Preset to file  ";
		msg += m_currentFilePath;
		msg += " ?";

		if (AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Save Preset",
			msg,
			String::empty,
			String::empty,
			this,
			nullptr))
		{
			MakeBackupFile(m_currentFilePath);
			SavePresetFile(m_currentFilePath, &m_preset);
		}
	}
#endif
}

void MainContentComponent::DoSaveAs()
{
#if 1
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"Save File",
		"Saving files to be implemented later.",
		"OK");
#else

	String locationToSaveTo = m_currentFilePath.empty() ? pPropertiesFile->getValue(propLastDirectory) : m_currentFilePath;
	File initialBrowseFile(locationToSaveTo);

	// Change this later to be something meaningful
	String defaultFilenameForSaveAs = "newfile";

	if (initialBrowseFile.isDirectory())
	{
		initialBrowseFile = File(locationToSaveTo + defaultFilenameForSaveAs + ".pre");
	}

	FileChooser fc("Choose a file to save...",
		initialBrowseFile,
		"*.pre",
		true);

	if (fc.browseForFileToSave(true))
	{
		File fileChosen = fc.getResult();
		String filepathChosen = fileChosen.getFullPathName();

		// save the last directory browsed
		if (filepathChosen.endsWith(fileChosen.getFileName()))
		{
			String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
			pPropertiesFile->setValue(propLastDirectory, var(lastDirStr));
		}

		// Ensure the filename always ends with the right extension.
		if (!filepathChosen.endsWith(".pre"))
		{
			filepathChosen += ".pre";
		}

		m_currentFilePath = filepathChosen.toStdString();

		MakeBackupFile(m_currentFilePath);
		SavePresetFile(m_currentFilePath, &m_preset);
	}
#endif
}

void MainContentComponent::DoMidiSetup()
{
	AlertWindow w("MIDI Setup",
		"",
		AlertWindow::NoIcon);

	MidiDeviceInfo	midiInCurrentInfo;
	midiInCurrentInfo.name = pPropertiesFile->getValue(propMidiInDeviceName);
	midiInCurrentInfo.identifier = pPropertiesFile->getValue(propMidiInDeviceID);

	MidiDeviceInfo	midiOutCurrentInfo;
	midiOutCurrentInfo.name = pPropertiesFile->getValue(propMidiOutDeviceName);
	midiOutCurrentInfo.identifier = pPropertiesFile->getValue(propMidiOutDeviceID);

	int  midiDeviceID = pPropertiesFile->getValue(propDeviceID).getIntValue();

	int midiInCurrentIndex = 0;
	int midiOutCurrentIndex = 0;
	String midiInComboName = "MidiIn";
	String midiOutComboName = "MidiOut";
	String deviceIDName = "DeviceID";
	String noDevicesFound = "No devices found";

	Array<MidiDeviceInfo> midiInDeviceInfos = MidiInput::getAvailableDevices();
	Array<MidiDeviceInfo> midiOutDeviceInfos = MidiOutput::getAvailableDevices();

	bool enable = true;
	midiInCurrentIndex = std::max(0, midiInDeviceInfos.indexOf(midiInCurrentInfo));
	if (midiInDeviceInfos.size() == 0)
	{
		midiInDeviceInfos.add(MidiDeviceInfo(noDevicesFound, noDevicesFound));
		enable = false;
	}
	std::vector<int> ids;

	// Extract just the names to an array so we can pass it to the combo box.
	StringArray midiInNames;
	for (auto const& info : midiInDeviceInfos)
	{
		midiInNames.add(info.name);
	}

	w.addComboBox(midiInComboName, midiInNames, "Midi In");
	ComboBox* pMidiInCombo = w.getComboBoxComponent(midiInComboName);
	assert(pMidiInCombo);
	pMidiInCombo->setSelectedItemIndex(midiInCurrentIndex);
	pMidiInCombo->setEnabled(enable);

	enable = true;
	midiOutCurrentIndex = std::max(0, midiOutDeviceInfos.indexOf(midiOutCurrentInfo));

	if (midiOutDeviceInfos.size() == 0)
	{
		midiOutDeviceInfos.add(MidiDeviceInfo(noDevicesFound, noDevicesFound));
		enable = false;
	}

	// Extract just the names to an array so we can pass it to the combo box.
	StringArray midiOutNames;
	for (auto const& info : midiOutDeviceInfos)
	{
		midiOutNames.add(info.name);
	}
	w.addComboBox(midiOutComboName,midiOutNames,"Midi Out");
	ComboBox* pMidiOutCombo = w.getComboBoxComponent(midiOutComboName);
	assert(pMidiOutCombo);
	pMidiOutCombo->setSelectedItemIndex(midiOutCurrentIndex);
	pMidiOutCombo->setEnabled(enable);

	w.addComboBox(deviceIDName,StringArray::fromTokens("0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15",false),"Device ID");
	ComboBox* pDeviceIDCombo = w.getComboBoxComponent(deviceIDName);
	pDeviceIDCombo->setSelectedItemIndex(midiDeviceID);

	w.addButton("Ok",1,KeyPress(KeyPress::returnKey,0,0));
	w.addButton("Cancel",0,KeyPress(KeyPress::escapeKey,0,0));

	if (w.runModalLoop() != 0) // if 'Ok' button clicked
	{
		if (pMidiInCombo->isEnabled())
		{
			String midiInNewName = pMidiInCombo->getText();
			auto pNewInfo = std::find_if(midiInDeviceInfos.begin(), midiInDeviceInfos.end(), [&midiInNewName](const MidiDeviceInfo& info) { return info.name == midiInNewName; });
			if (pNewInfo != midiOutDeviceInfos.end())
			{
				pPropertiesFile->setValue(String(propMidiInDeviceName), var(pNewInfo->name));
				pPropertiesFile->setValue(String(propMidiInDeviceID), var(pNewInfo->identifier));
			}
			else
			{
				pPropertiesFile->setValue(String(propMidiInDeviceName), var(""));
				pPropertiesFile->setValue(String(propMidiInDeviceID), var(""));
			}
		}
		if (pMidiOutCombo ->isEnabled())
		{
			String midiOutNewName = pMidiOutCombo->getText();
			auto pNewInfo = std::find_if(midiOutDeviceInfos.begin(), midiOutDeviceInfos.end(), [&midiOutNewName](const MidiDeviceInfo& info) { return info.name == midiOutNewName; });
			if (pNewInfo != midiOutDeviceInfos.end())
			{
				pPropertiesFile->setValue(String(propMidiOutDeviceName), var(pNewInfo->name));
				pPropertiesFile->setValue(String(propMidiOutDeviceID), var(pNewInfo->identifier));
			}
			else
			{
				pPropertiesFile->setValue(String(propMidiOutDeviceName), var(""));
				pPropertiesFile->setValue(String(propMidiOutDeviceID), var(""));
			}
		}

		String deviceIDString = pDeviceIDCombo->getText();
		pPropertiesFile->setValue(String(propDeviceID), var(deviceIDString));

		pPropertiesFile->save();
	}
}

class WireThresholdInputFilter : public juce::TextEditor::InputFilter
{
public:
	/** This method is called whenever text is entered into the editor.
		An implementation of this class should check the input string,
		and return an edited version of it that should be used.
	*/
	virtual String filterNewText(TextEditor& ed, const String& newInput)
	{
		String t = newInput.retainCharacters("0123456789");
		auto selectedRange = ed.getHighlightedRegion();
		String currentText = ed.getText();

		if (t.isNotEmpty())
		{
			if (selectedRange.isEmpty())
			{
				currentText = currentText.replaceSection(ed.getCaretPosition(), 0, t);
			}
			else
			{
				currentText = currentText.replaceSection(selectedRange.getStart(), selectedRange.getLength(), t);
			}

			int val = currentText.getIntValue();
			if (currentText.length() > 3 ||			// limit length to three characters.
				val < 0 || val > 100)				// Limit to range 0 - 100
			{
				t.clear();	// clear the new input.
			}
		}

		return t;
	}
};


class PreferencesComponent : public Component, public Button::Listener
{
public:
	PreferencesComponent()
	{
		assert(pPropertiesFile != nullptr);

		setSize(360, 190);

		const int leftX = 10;
		const int leftWidth = 100;
		const int rightX = 120;
		const int rightWidth = 110;
		const int ctrlHeight = 20;
		const int space = 12;
		const int rowHeight = ctrlHeight + space;
		int yPos = 30;

		auto osType = juce::SystemStats::getOperatingSystemType();
		String cClick = osType == juce::SystemStats::OperatingSystemType::MacOSX ? "Cmd-click" : "Ctrl-click";

		m_sliderResetLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_sliderResetLabel.setText("Slider reset:", NotificationType::dontSendNotification);
		m_sliderResetLabel.setTooltip("Set the type of mouseclick to reset sliders to zero.");

		addAndMakeVisible(m_sliderResetLabel);

		m_sliderResetComboBox.setBounds(rightX, yPos, rightWidth, ctrlHeight);
		m_sliderResetComboBox.addItem("Off", 1);
		m_sliderResetComboBox.addItem(cClick, 2);
		m_sliderResetComboBox.addItem("Double-click", 3);
		int sliderReset = pPropertiesFile->getIntValue(propSliderReset);
		m_sliderResetComboBox.setSelectedId(sliderReset + 1);
		addAndMakeVisible(m_sliderResetComboBox);

		yPos += rowHeight;
		m_sliderEditLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_sliderEditLabel.setText("Slider edit:", NotificationType::dontSendNotification);
		m_sliderEditLabel.setTooltip("Set the type of mouseclick to edit slider values directly.");
		addAndMakeVisible(m_sliderEditLabel);

		m_sliderEditComboBox.setBounds(rightX, yPos, rightWidth, ctrlHeight);
		m_sliderEditComboBox.addItem("Off", 1);
		m_sliderEditComboBox.addItem(cClick, 2);
		m_sliderEditComboBox.addItem("Double-click", 3);
		int sliderEdit = pPropertiesFile->getIntValue(propSliderEdit);
		m_sliderEditComboBox.setSelectedId(sliderEdit + 1);
		addAndMakeVisible(m_sliderEditComboBox);

		yPos += rowHeight;
		m_wireThresholdLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_wireThresholdLabel.setText("Wire threshold:", NotificationType::dontSendNotification);
		m_wireThresholdLabel.setTooltip("Set the opacity above which wires become selectable. Valid values are 0 to 100.");
		addAndMakeVisible(m_wireThresholdLabel);

		m_wireThresholdEdit.setBounds(rightX, yPos, 50, ctrlHeight);
		m_wireThresholdEdit.setMultiLine(false);
		m_wireThresholdEdit.setReturnKeyStartsNewLine(false);
		m_wireThresholdEdit.setReadOnly(false);
		m_wireThresholdEdit.setScrollbarsShown(true);
		m_wireThresholdEdit.setCaretVisible(true);
		m_wireThresholdEdit.setPopupMenuEnabled(true);
		m_wireThresholdEdit.setInputFilter(new WireThresholdInputFilter(), true);
		m_wireThresholdEdit.setText(pPropertiesFile->getValue(propWireThreshold, "100"));
		addAndMakeVisible(m_wireThresholdEdit);


		m_okButton.setButtonText("Ok");
		m_okButton.addListener(this);
		addAndMakeVisible(m_okButton);

		m_cancelButton.setButtonText("Cancel");
		m_cancelButton.addListener(this);
		addAndMakeVisible(m_cancelButton);

		auto bnds = getLocalBounds();
		int buttonWidth = 80;
		int buttonHeight = 25;
		int spaceBetweenButtons = 22;

		m_okButton.setBounds(
			(bnds.getWidth() / 2) - buttonWidth - (spaceBetweenButtons/2), 
			bnds.getHeight() - buttonHeight - 15, 
			buttonWidth, 
			buttonHeight);
		m_cancelButton.setBounds((
			bnds.getWidth() / 2) + (spaceBetweenButtons / 2), 
			bnds.getHeight() - buttonHeight - 15, 
			buttonWidth, 
			buttonHeight);
	}

	void buttonClicked(Button* buttonThatWasClicked) override
	{
		if (buttonThatWasClicked == &m_okButton && pPropertiesFile != nullptr)
		{
			pPropertiesFile->setValue(propSliderReset, m_sliderResetComboBox.getSelectedId() - 1);
			pPropertiesFile->setValue(propSliderEdit, m_sliderEditComboBox.getSelectedId() - 1);
			pPropertiesFile->setValue(propWireThreshold, m_wireThresholdEdit.getTextValue() );
		}

		if (DialogWindow* dw = findParentComponentOfClass<DialogWindow>())
		{
			dw->exitModalState(buttonThatWasClicked == &m_okButton ? 0 : 1);
		}
	}

private:
	Label		m_sliderResetLabel;
	ComboBox	m_sliderResetComboBox;
	Label		m_sliderEditLabel;
	ComboBox	m_sliderEditComboBox;
	Label		m_wireThresholdLabel;
	TextEditor	m_wireThresholdEdit;

	TextButton	m_okButton;
	TextButton	m_cancelButton;
};

class EditNameInputFilter : public juce::TextEditor::InputFilter
{
public:
	/** This method is called whenever text is entered into the editor.
		An implementation of this class should check the input string,
		and return an edited version of it that should be used.
	*/
	virtual String filterNewText(TextEditor& ed, const String& newInput)
	{
		String t;
		auto pChar = newInput.getCharPointer();
		for (int i = 0; i < newInput.length(); ++i)
		{
			if (*pChar >= 32 && *pChar <= 126)
			{
				t += *pChar;
			}
		}

		if (t.isNotEmpty())
		{
			auto selectedRange = ed.getHighlightedRegion();
			String currentText = ed.getText();
			String proposedText;
			if (selectedRange.isEmpty())
			{
				proposedText = currentText.replaceSection(ed.getCaretPosition(), 0, t);
			}
			else
			{
				proposedText = currentText.replaceSection(selectedRange.getStart(), selectedRange.getLength(), t);
			}

			if (proposedText.length() > 20)			// limit length to 20 characters.
			{
				t = t.dropLastCharacters(proposedText.length() - 20);
			}
		}

		return t;
	}
};


class EditNameComponent : public Component, public Button::Listener   //, public ComboBox::Listener
{

public:
	EditNameComponent(Preset* pPreset, CommandHistory* pCommandHistory)
		:m_pPreset(pPreset)
		, m_pCommandHistory(pCommandHistory)

	{
		setSize(360, 190);

		const int leftX = 10;
		const int leftWidth = 100;
		const int rightX = 120;
		const int rightWidth = 110;
		const int ctrlHeight = 22;
		const int space = 12;
		const int rowHeight = ctrlHeight + space;
		int yPos = 30;


		m_nameLabel.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_nameLabel.setText("Preset Name:", NotificationType::dontSendNotification);
		m_nameLabel.setTooltip("Set the preset name. Maximum length is 20 characters.");
		addAndMakeVisible(m_nameLabel);

		m_nameEdit.setBounds(rightX, yPos, 200, ctrlHeight);
		m_nameEdit.setMultiLine(false);
		m_nameEdit.setReturnKeyStartsNewLine(false);
		m_nameEdit.setReadOnly(false);
		m_nameEdit.setScrollbarsShown(true);
		m_nameEdit.setCaretVisible(true);
		m_nameEdit.setPopupMenuEnabled(true);
		m_nameEdit.setInputFilter(new EditNameInputFilter(), true);
		String name = m_pPreset->GetName();
		m_nameEdit.setText(name.trimEnd());
		m_nameEdit.setSelectAllWhenFocused(true);
		addAndMakeVisible(m_nameEdit);

		yPos += rowHeight;
		m_category1Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_category1Label.setText("Category 1:", NotificationType::dontSendNotification);
		m_category1Label.setTooltip("Set the first category.");
		addAndMakeVisible(m_category1Label);

		m_category1ComboBox.setBounds(rightX, yPos, rightWidth, ctrlHeight);
		ParamID paramID(ElementID(ElementType::Home, 0), gloCategory1);
		std::vector<std::string> cat1List = m_pPreset->GetParameterListText(paramID);
		for (size_t i = 0; i < cat1List.size(); ++i)
		{
			m_category1ComboBox.addItem(cat1List[i], i + 1);
		}
		int val = m_pPreset->GetParameterValue(paramID);
		m_category1ComboBox.setSelectedId(val + 1);
		//m_category1ComboBox.addListener(this);
		addAndMakeVisible(m_category1ComboBox);

		yPos += rowHeight;
		m_category2Label.setBounds(leftX, yPos, leftWidth, ctrlHeight);
		m_category2Label.setText("Category 2:", NotificationType::dontSendNotification);
		m_category2Label.setTooltip("Set the second category.");
		addAndMakeVisible(m_category2Label);

		m_category2ComboBox.setBounds(rightX, yPos, rightWidth, ctrlHeight);
		paramID.subIndex = gloCategory2;
		std::vector<std::string> cat2List = m_pPreset->GetParameterListText(paramID);
		for (size_t i = 0; i < cat2List.size(); ++i)
		{
			m_category2ComboBox.addItem(cat2List[i], i + 1);
		}
		val = m_pPreset->GetParameterValue(paramID);
		m_category2ComboBox.setSelectedId(val + 1);
		//m_category2ComboBox.addListener(this);
		addAndMakeVisible(m_category2ComboBox);




		m_okButton.setButtonText("Ok");
		m_okButton.addListener(this);
		addAndMakeVisible(m_okButton);

		m_cancelButton.setButtonText("Cancel");
		m_cancelButton.addListener(this);
		addAndMakeVisible(m_cancelButton);

		auto bnds = getLocalBounds();
		int buttonWidth = 80;
		int buttonHeight = 25;
		int spaceBetweenButtons = 22;

		m_okButton.setBounds(
			(bnds.getWidth() / 2) - buttonWidth - (spaceBetweenButtons / 2),
			bnds.getHeight() - buttonHeight - 15,
			buttonWidth,
			buttonHeight);
		m_cancelButton.setBounds((
			bnds.getWidth() / 2) + (spaceBetweenButtons / 2),
			bnds.getHeight() - buttonHeight - 15,
			buttonWidth,
			buttonHeight);

	}


	void buttonClicked(Button* buttonThatWasClicked) override
	{
		static uint8_t subIndexes[] = { gloName1, gloName2, gloName3, gloName4, gloName5, gloName6, gloName7, gloName8, gloName9, gloName10,
			gloName11, gloName12, gloName13, gloName14, gloName15, gloName16, gloName17, gloName18, gloName19, gloName20 };
		
		if (buttonThatWasClicked == &m_okButton)
		{
			CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();

			ParamID paramID(ElementID(ElementType::Home, 0), 0);
			String name = m_nameEdit.getText();
			name = name.paddedRight(' ', 20);	// pad out to 20 chars
			for (int i = 0; i < 20; ++i)
			{
				paramID.subIndex = subIndexes[i];
				pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(paramID, name[i]));
			}

			paramID.subIndex = gloCategory1;
			pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(paramID, m_category1ComboBox.getSelectedId() - 1));

			paramID.subIndex = gloCategory2;
			pCmdMacro->AddCommand(std::make_unique<CmdSetParameterValue>(paramID, m_category2ComboBox.getSelectedId() - 1));

			pCmdMacro->SetActionName("Edit Name and Categories");
			// If there is only one element selected, the details text is the element's name.
			pCmdMacro->SetDetails(name.toStdString());
			m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
		}

		if (DialogWindow* dw = findParentComponentOfClass<DialogWindow>())
		{
			dw->exitModalState(buttonThatWasClicked == &m_okButton ? 0 : 1);
		}
	}

	//void comboBoxChanged(ComboBox * comboBoxThatHasChanged) override
	//{

	//}

private:
	Preset*			m_pPreset;
	CommandHistory*	m_pCommandHistory;

	Label		m_nameLabel;
	TextEditor	m_nameEdit;
	Label		m_category1Label;
	ComboBox	m_category1ComboBox;
	Label		m_category2Label;
	ComboBox	m_category2ComboBox;

	TextButton	m_okButton;
	TextButton	m_cancelButton;
};



void MainContentComponent::OpenArpEditWindow()
{
	if (m_pArpEditWindow == nullptr)
	{
		m_pArpEditWindow = std::make_unique<ArpEditWindow>(m_pPreset.get(), m_pCommandHistory.get(), this, [this]() { m_pArpEditWindow.reset(); });
		assert(m_pArpEditWindow);
		m_pArpEditWindow->setTopLeftPosition(pPropertiesFile->getValue(propArpWindowLeft).getIntValue(),
									pPropertiesFile->getValue(propArpWindowTop).getIntValue());
	}
	else
	{
		m_pArpEditWindow->toFront(true);
	}
}

void MainContentComponent::OpenEditHistoryWindow()
{
	if (m_pEditHistoryWindow == nullptr)
	{
		m_pEditHistoryWindow = std::make_unique<EditHistoryWindow>(m_pPreset.get(), m_pCommandHistory.get(), this, [this]() { m_pEditHistoryWindow.reset(); });
		assert(m_pEditHistoryWindow);
		m_pEditHistoryWindow->setTopLeftPosition(pPropertiesFile->getValue(propHistoryWindowLeft).getIntValue(),
			pPropertiesFile->getValue(propHistoryWindowTop).getIntValue());
	}
	else
	{
		m_pEditHistoryWindow.reset();	// close window by destructing the pointer.
	}
}


void MainContentComponent::DoPreferences()
{
	DialogWindow::LaunchOptions launchOptions;

	launchOptions.content.setOwned(new PreferencesComponent());
	launchOptions.componentToCentreAround = this;
	launchOptions.dialogTitle = "Preferences";
	launchOptions.dialogBackgroundColour = Colours::lightgrey;
	launchOptions.escapeKeyTriggersCloseButton = true;
	launchOptions.useNativeTitleBar = true;
	launchOptions.resizable = false;

	launchOptions.launchAsync();

}

/** @brief	Sets a bookmark at the current branch and revision.

	If the bookmark number has already been set, an overwrite warning is displayed.

	@param[in] bookmarkNumber	The number of the bookmark to set. Valid values are 0 to 9 inclusive.
*/
void MainContentComponent::DoSetBookmark(int bookmarkNumber)
{
	assert(bookmarkNumber >= 0 && bookmarkNumber <= 9);

	bool doSet{ true };
	Version bm = m_pCommandHistory->GetBookmark(bookmarkNumber);
	if (bm != NO_VERSION && bm != m_pCommandHistory->GetCurrentVersion())
	{
		std::string str1 = "Move bookmark ";
		str1 += std::to_string(bookmarkNumber) + " ?";

		std::string str2 = "Bookmark ";
		str2 += std::to_string(bookmarkNumber);
		str2 += " is currently at branch [";
		str2 += m_pCommandHistory->GetBranchName(bm.branchTag);
		str2 += "] version ";
		str2 += std::to_string(bm.revision);

		doSet = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon, str1, str2, {}, {}, this, nullptr);
	}
	else
	{
		assert(bm == NO_VERSION || bm == m_pCommandHistory->GetCurrentVersion());
	}

	if (doSet)
	{
		m_pCommandHistory->SetBookmark(bookmarkNumber);
	}
}

void MainContentComponent::DoToggleMidiSync()
{
	bool syncOn = (pPropertiesFile->getValue(propMidiSync) == "on");
	syncOn = !syncOn;
	pPropertiesFile->setValue(propMidiSync, syncOn ? "on" : "off");
	pPropertiesFile->save();
}

void MainContentComponent::DoToggleMuteSelected()
{
	vector<Connector> connectorList;

	if (m_pViewModel->ConnectorSelectionModel().HasSelection())
	{
		connectorList.push_back(m_pViewModel->ConnectorSelectionModel().GetFirstSelection());
	}
	else if (m_pViewModel->ElementViewSelectionModel().HasSelection())
	{
		for (auto&elemID : m_pViewModel->ElementViewSelectionModel())
		{
			auto outputConnectors = m_pPreset->GetOutputConnectorsOfElement(elemID);
			std::copy(outputConnectors.begin(), outputConnectors.end(), std::back_inserter(connectorList));
		}
	}

	if (connectorList.size() == 1)
	{
		PortID portToToggle = connectorList[0].startPort.portType == PortType::Output ? connectorList[0].endPort : connectorList[0].startPort;

		ParamID paramID(portToToggle);
		bool currentMutedState = m_pPreset->GetMuted(paramID);
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetMuted>(paramID, !currentMutedState));
	}
	else if (connectorList.size() > 1)
	{
		// sort and then remove duplicates.
		std::sort(connectorList.begin(), connectorList.end());
		auto last = std::unique(connectorList.begin(), connectorList.end());	// move duplicate elements to the end of the array
		connectorList.erase(last, connectorList.end());			// remove the duplicate elements from the end

		CmdMacroUPtr pCmdMacro = std::make_unique<CmdMacro>();
		for (auto& conn : connectorList)
		{
			PortID portToToggle = conn.startPort.portType == PortType::Output ? conn.endPort : conn.startPort;

			ParamID paramID(portToToggle);
			bool currentMutedState = m_pPreset->GetMuted({ portToToggle });
			pCmdMacro->AddCommand(std::make_unique<CmdSetMuted>(paramID, !currentMutedState));
		}
		pCmdMacro->SetActionName("Toggle Mute");
		// If there is only one element selected, the details text is the element's name.
		pCmdMacro->SetDetails(	(m_pViewModel->ElementViewSelectionModel().size() == 1) ? 
					m_pViewModel->GetElementFriendlyName(m_pViewModel->ElementViewSelectionModel().GetFirstSelection()) : 
					"Multiple");
		m_pCommandHistory->AddCommandAndExecute(std::move(pCmdMacro));
	}
}

void MainContentComponent::DoUnMuteAll()
{
	auto mutedList = m_pPreset->GetAllMuted();

	if (mutedList.size() == 1)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetMuted>(mutedList[0], false));
	}
	else if (mutedList.size() > 1)
	{
		CmdMacroUPtr pMacroCommand = std::make_unique<CmdMacro>();
		for (auto& paramID : mutedList)
		{
			pMacroCommand->AddCommand(std::make_unique<CmdSetMuted>(paramID, false));
		}
		pMacroCommand->SetActionName("Unmute All");
		m_pCommandHistory->AddCommandAndExecute(std::move(pMacroCommand));
	}
}


class ProgressComponent :	public Component,
							public TextButton::Listener
{
public:
	ProgressComponent(double& progressValue, std::function<void(void)> cancelCallback)
		:m_cancelCallback(cancelCallback)
	{
		//setSize(300, 200);

		m_pProgressBar = std::make_unique<ProgressBar>(progressValue);
		addAndMakeVisible(m_pProgressBar.get());

		m_pCancelButton = std::make_unique<TextButton>("Cancel");
		m_pCancelButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
		m_pCancelButton->setColour(TextButton::buttonColourId, Colour(0xff677b81));
		m_pCancelButton->setColour(TextButton::buttonOnColourId, Colour(0xff95fffb));
		m_pCancelButton->addListener(this);
		addAndMakeVisible(m_pCancelButton.get());

	}
	~ProgressComponent()
	{
		m_cancelCallback();
	}

	void resized() override
	{
		m_pProgressBar->setBounds(20, 20, getWidth() - 40, 24);
		m_pCancelButton->setBounds(getWidth() - 80, getHeight() - 38, 60, 24);
	}

	void buttonClicked(juce::Button *) override
	{
		m_cancelCallback();
	}

private:
	std::unique_ptr<ProgressBar>	m_pProgressBar;
	std::unique_ptr<TextButton>		m_pCancelButton;
	std::function<void(void)>		m_cancelCallback;
};


void MainContentComponent::DoMidiReceive()
{
	m_progress = 0; // range 0 to 1.0
	int progressDlgWidth = 300;
	int progressDlgHeight = 110;
	m_continueMidiTransfer = true;

	auto cancelCallback = [this]()
	{
		m_continueMidiTransfer = false;
	};

	DialogWindow::LaunchOptions launchOptions;
	launchOptions.componentToCentreAround = this;
	launchOptions.content.set( new ProgressComponent(m_progress, cancelCallback), true);
	launchOptions.content->setSize(progressDlgWidth, progressDlgHeight);
	launchOptions.escapeKeyTriggersCloseButton = true;
	launchOptions.dialogBackgroundColour = Colours::lightgrey;
	launchOptions.useNativeTitleBar = true;
	launchOptions.resizable = false;
	launchOptions.dialogTitle = "Receive Preset";

	m_pProgressDialogWindow = launchOptions.launchAsync();
	m_pProgressDialogWindow->setSize(progressDlgWidth, progressDlgHeight);

	auto callback = [this](double progress, bool isDone)
	{
		m_progress = progress;
		if (isDone)
		{
			m_pProgressDialogWindow->exitModalState(0);
		}
		return m_continueMidiTransfer;
	};

	m_currentFilePath.clear();
	m_pViewModel->ClearVisibleElements();
	m_midiManager->DoReceivePreset(callback);
}

void MainContentComponent::DoMidiSend()
{
	m_progress = 0; // range 0 to 1.0
	int progressDlgWidth = 300;
	int progressDlgHeight = 110;
	m_continueMidiTransfer = true;

	auto cancelCallback = [this]()
	{
		m_continueMidiTransfer = false;
	};

	DialogWindow::LaunchOptions launchOptions;
	launchOptions.componentToCentreAround = this;
	launchOptions.content.set(new ProgressComponent(m_progress, cancelCallback), true);
	launchOptions.content->setSize(progressDlgWidth, progressDlgHeight);
	launchOptions.escapeKeyTriggersCloseButton = true;
	launchOptions.dialogBackgroundColour = Colours::lightgrey;
	launchOptions.useNativeTitleBar = true;
	launchOptions.resizable = false;
	launchOptions.dialogTitle = "Send Preset";

	m_pProgressDialogWindow = launchOptions.launchAsync();
	m_pProgressDialogWindow->setSize(progressDlgWidth, progressDlgHeight);

	auto callback = [this](double progress, bool isDone)
	{
		m_progress = progress;
		if (isDone || !m_continueMidiTransfer)
		{
			m_pProgressDialogWindow->exitModalState(0);
			m_continueMidiTransfer = false;
		}
		return m_continueMidiTransfer;
	};

	m_midiManager->DoSendPreset(callback);
}

void MainContentComponent::DoEditName()
{
	DialogWindow::LaunchOptions launchOptions;

	launchOptions.content.setOwned(new EditNameComponent(m_pPreset.get(), m_pCommandHistory.get()));
	launchOptions.componentToCentreAround = this;
	launchOptions.dialogTitle = "Edit Name and Categories";
	launchOptions.dialogBackgroundColour = Colours::lightgrey;
	launchOptions.escapeKeyTriggersCloseButton = true;
	launchOptions.useNativeTitleBar = true;
	launchOptions.resizable = false;

	launchOptions.launchAsync();

}

#if (_DEBUG)
void MainContentComponent::DoDumpPreset()
{
	// create a filename that doesn't already exist.
	String filename;
	for (int i = 1; i < 1000; ++i)
	{
		filename = "";
		filename << File::getCurrentWorkingDirectory().getFullPathName() << File::getSeparatorString() << "presetDump" << i << ".txt";
		if (!File(filename).existsAsFile())
		{
			break;
		}
	}
	assert(filename.isNotEmpty());
	std::ofstream outFile(filename.toStdString().c_str());
	m_pPreset->Dump(outFile);
	outFile.close();
}
void MainContentComponent::DoSaveHistory()
{
	// create a filename that doesn't already exist.
	String filename;
	for (int i = 1; i < 1000; ++i)
	{
		filename = "";
		filename << File::getCurrentWorkingDirectory().getFullPathName() << File::getSeparatorString() << "UndoHistory" << i << ".bin";
		if (!File(filename).existsAsFile())
		{
			break;
		}
	}
	assert(filename.isNotEmpty());
	std::ofstream outFile(filename.toStdString().c_str(), std::ios::binary | std::ios::out);
	m_pPreset->Write(outFile);
	m_pViewModel->Write(outFile);
	m_pCommandHistory->Write(outFile);
	outFile.close();
}

void MainContentComponent::DoLoadHistory()
{
	File curDir = File::getCurrentWorkingDirectory();
	FileChooser fc("Choose a file to open...", curDir, "*.bin", true);
	if (fc.browseForFileToOpen())
	{
		File fileChosen = fc.getResult();
		String filepathChosen = fileChosen.getFullPathName();
		std::ifstream inFile(filepathChosen.toStdString().c_str(), std::ios::binary | std::ios::in);
		m_pPreset->Read(inFile);
		m_pViewModel->Read(inFile);
		m_pCommandHistory->Read(inFile);
		inFile.close();

		if (m_pEditHistoryWindow != nullptr)
		{
			m_pEditHistoryWindow->Update();
		}
	}

}
#endif

LookAndFeelSol::LookAndFeelSol()
	:LookAndFeel_V3()
{
	// Override certain colors.

	//const uint32 textButtonColour = 0xff87cefa;	// lightskyblue
	//const uint32 textHighlightColour = 0xff87cefa;

	const uint32 textButtonColour = 0xffa5b8d8;	// lightskyblue
	const uint32 textHighlightColour = 0xffa5b8d8;

	setColour(TextButton::buttonColourId, Colour(textButtonColour));
	setColour(TextEditor::highlightColourId, Colour(textHighlightColour));
	setColour(TextEditor::focusedOutlineColourId, Colour(textButtonColour));
	setColour(PopupMenu::highlightedBackgroundColourId, Colour(textButtonColour));
	setColour(ComboBox::buttonColourId, Colour(textButtonColour));
	setColour(Slider::thumbColourId, Colour(textHighlightColour));
	setColour(Slider::textBoxHighlightColourId, Colour(textHighlightColour));
	setColour(ProgressBar::foregroundColourId, Colour(textButtonColour));
	setColour(DirectoryContentsDisplayComponent::highlightColourId, Colour(textHighlightColour));
	setColour(0x1004502, Colour(textHighlightColour)); /*CodeEditorComponent::highlightColourId*/ 
}


/** @brief In this override of drawToggleButton, the objective is to make the radio button background 
			gray instead of colored when it's not checked so that it isn't confused with a button that is checked.
*/
void LookAndFeelSol::drawToggleButton(Graphics& g, ToggleButton& button, bool isMouseOverButton, bool isButtonDown)
{
	//const uint32 lightSkyBlue = 0xff87cefa;
	const uint32 lightSkyBlue = 0xffa5b8d8;
	const uint32 gray = 0xFF808080;

	setColour(TextButton::buttonColourId, Colour(button.getToggleState() ? lightSkyBlue : gray));
	LookAndFeel_V2::drawToggleButton(g, button, isMouseOverButton, isButtonDown);

	// For non-ToggleButtons we want the color to stay as blue. So put it back after we draw ToggleButton.
	setColour(TextButton::buttonColourId, Colour(lightSkyBlue));
}

#if 0
/** @brief This function is copied from LookAndFeel_V3. The intent is to remove the ugly "shiny" look from V2.
*/
void LookAndFeelSol::drawMenuBarBackground(Graphics& g, int width, int height,
	bool, MenuBarComponent& menuBar)
{
	const Colour colour(menuBar.findColour(PopupMenu::backgroundColourId));

	Rectangle<int> r(width, height);

	g.setColour(colour.contrasting(0.15f));
	g.fillRect(r.removeFromTop(1));
	g.fillRect(r.removeFromBottom(1));

	g.setGradientFill(ColourGradient(colour, 0, 0, colour.darker(0.08f), 0, (float)height, false));
	g.fillRect(r);
}
#endif
