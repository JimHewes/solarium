/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

#include <cstdio>
#include <vector>

#include <iosfwd> 
#include "gsl_util.h"
#include "Preset.h"
#include "Notifier.h"
#include "HistoryList.h"

class CmdSetParameterValue;
class ViewModel;

struct BranchInfo
{
	BranchTag	branchTag;
	std::string	branchName;
};

enum class CmdType : uint8_t
{ 
	NewPreset = 0,
	SetPreset = 1,
	SetParameterValue = 2,
	SetMuted = 3,
	CreateConnector = 4, 
	RemoveConnector = 5,
	AddElement = 6,
	RemoveElement = 7,
	Macro = 8
};

/** @brief Command is the base class for all commands that are undo-able.

	Currently it's possible for any class to call Execute or Unexecute because they are public members.
	This would bypass the command history list and screw up the sequence. I could attempt to prevent this 
	by making Execute and Unexecute as private members and making CommandHistory a friend class, or 
	some such other way.
	But for now, it's not enforced. Just don't call those functions unless you're CommandHistory or CmdMaco!!
*/
class Command
{
public:
	virtual ~Command() {};
	void Execute(Version version)
	{
		if (m_hour == UINT8_MAX)	// max means that time was not updated yet
		{
			UpdateTime();
		}
		ExecuteImpl(version);
	}

	virtual std::string GetAction() { return "Unknown operation"; };
	virtual std::string GetDescription() { return "Unknown operation"; };
	bool Combine(Command* pOther)
	{
		bool result = CombineImpl(pOther);
		if (result)
		{
			UpdateTime();
		}
		return result;
	};
	std::string GetTime()
	{
		if (m_hour == UINT8_MAX)	// max means that time was not updated yet
		{
			UpdateTime();
		}
		if (m_hour > 12)
		{
			m_hour -= 12;
		}
		char buffer[12];
		snprintf(buffer, 12, "%d:%02d:%02d", m_hour, m_minute, m_second);
		return &buffer[0];
	}
	static void SetTargetPreset(Preset* pPreset)
	{
		m_pPreset = pPreset;
	}
	static Preset* GetTargetPreset()
	{
		return m_pPreset;
	}
	static void SetTargetViewModel(ViewModel* pViewModel)
	{
		m_pViewModel = pViewModel;
	}
	static ViewModel* GetTargetViewModel()
	{
		return m_pViewModel;
	}

	virtual void Write(std::ostream& output) const
	{
		output.write(reinterpret_cast<const char *>(&m_hour), sizeof(m_hour));
		output.write(reinterpret_cast<const char *>(&m_minute), sizeof(m_minute));
		output.write(reinterpret_cast<const char *>(&m_second), sizeof(m_second));
	}
	virtual void Read(std::istream& input)
	{
		input.read(reinterpret_cast<char *>(&m_hour), sizeof(m_hour));
		input.read(reinterpret_cast<char *>(&m_minute), sizeof(m_minute));
		input.read(reinterpret_cast<char *>(&m_second), sizeof(m_second));
	}

	static std::unique_ptr<Command> Create(std::istream& input);

private:
	void UpdateTime();

	uint8_t m_hour{ UINT8_MAX };	// Max means that the time isn't initialized yet
	uint8_t m_minute{ UINT8_MAX };	// Max means that the time isn't initialized yet
	uint8_t m_second{ UINT8_MAX };	// Max means that the time isn't initialized yet
	static Preset* m_pPreset;
	static ViewModel* m_pViewModel;

protected:
	virtual void ExecuteImpl(Version version) = 0;
	virtual bool CombineImpl(Command* pOther)
	{
		pOther; return false; 
	};

};
using CommandUPtr = std::unique_ptr<Command>;

class CmdMacro : public Command
{
public:
	CmdMacro();
	CmdMacro(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;
	void SetActionName(const std::string& nameText);
	void SetDetails(const std::string& detailsText);
	virtual void AddCommand(CommandUPtr&& command);

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:
	std::string				m_actionName;
	std::string				m_details;
	std::list<CommandUPtr>	m_cmdList;
};
using CmdMacroUPtr = std::unique_ptr<CmdMacro>;

class CmdSetParameterValue : public Command
{
public:
	CmdSetParameterValue(const ParamID& paramID, uint32_t value);
	CmdSetParameterValue(std::istream& input);

	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	bool CombineImpl(Command* pOther) noexcept override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:
	ParamID	m_paramID;
	int32_t	m_value;
};
using CmdSetParameterValueUPtr = std::unique_ptr<CmdSetParameterValue>;

class CmdSetPreset : public Command
{
public:
	CmdSetPreset(std::vector<ParamValue>&& newParamValues);
	CmdSetPreset(std::istream& input);

	std::string GetAction() override;
	std::string GetDescription() override;
	void SetActionText(const std::string& desc);

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:

	std::string m_action;
	std::string m_newName;
	std::vector<ParamValue> m_newParamValues;
};
using CmdSetPresetUPtr = std::unique_ptr<CmdSetPreset>;

class CmdNewPreset : public Command
{
public:
	CmdNewPreset();
	CmdNewPreset(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;
};
using CmdSetPresetUPtr = std::unique_ptr<CmdSetPreset>;


class CmdSetMuted : public Command
{
public:
	CmdSetMuted(const ParamID& paramID, bool isMuted);
	CmdSetMuted(std::istream& input);

	std::string GetAction() override;
	std::string GetDescription() override;

	bool HasEqualParamID(const CmdSetMuted& other);

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:
	ParamID	m_paramID;
	bool	m_isMuted;
};
using CmdSetParameterValueUPtr = std::unique_ptr<CmdSetParameterValue>;


class CmdCreateConnector : public Command
{
public:
	CmdCreateConnector(const PortID& startingPortID, const PortID& endingPortID);
	CmdCreateConnector(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:

	PortID	m_newStartingPortID;
	PortID	m_newEndingPortID;
};
using CmdCreateConnectorUPtr = std::unique_ptr<CmdCreateConnector>;

class CmdRemoveConnector : public Command
{
public:
	CmdRemoveConnector(const Connector& connectorToRemove);
	CmdRemoveConnector(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:
	Connector	m_connectorToRemove;
};
using CmdRemoveConnectorUPtr = std::unique_ptr<CmdRemoveConnector>;


struct Location
{
	int16_t x;
	int16_t y;
	Location(int16_t x_ = 0, int16_t y_ = 0) : x(x_), y(y_) {}
};

class CmdAddElement : public Command
{
public:
	CmdAddElement(const ElementID& elemID, const Location& location);
	CmdAddElement(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:

	ElementID	m_elemID;
	Location	m_location;
};
using CmdAddElementUPtr = std::unique_ptr<CmdAddElement>;

class CmdRemoveElement : public Command
{
public:
	CmdRemoveElement(const ElementID& elemIDn);
	CmdRemoveElement(std::istream& input);
	std::string GetAction() override;
	std::string GetDescription() override;

protected:
	void ExecuteImpl(Version version) override;
	void Write(std::ostream& output) const override;
	void Read(std::istream& input) override;

private:
	ElementID	m_elemID;
};
using CmdRemoveElementUPtr = std::unique_ptr<CmdRemoveElement>;


class CommandHistory
{
public:
	CommandHistory();
	CommandHistory(const CommandHistory& ) = delete;			// disallow copy
	CommandHistory& operator=(const CommandHistory&) = delete;	// disallow assignment

	void AddCommandAndExecute(CommandUPtr&& command);
	void Undo();
	void UndoDestructive();
	void Redo();
	uint32_t GetHistoryLength() const;
	uint32_t GetHistoryLength(BranchTag branchTag) const;
	bool CanUndo();
	bool CanUndoDestructive();
	bool CanRedo();
	void SetBookmark(int bookmarkNumber);
	void GoToBookmark(int bookmarkNumber);
	Version GetBookmark(int bookmarkNumber);
	std::vector<int> GetBookmarksAtVersion(Version version);

	void SetVersion(Version version);
	void SetRevision(Revision revision);
	Version GetCurrentVersion() const;
	Revision GetCurrentRevision() const;
	Command * GetCommandAtVersion(Version version) const;
	Command * GetCommandAtRevision(Revision revision) const;

	BranchTag GetCurrentBranchTag();
	void SetCurrentBranchTag(BranchTag branchTag);
	std::vector<BranchInfo> GetBranchList() const;
	void SetBranchName(BranchTag branchTag, const std::string& name);
	std::string GetBranchName(BranchTag branchTag);
	void DeleteBranch(BranchTag branchTag);
	Version GetBranchPoint(BranchTag branchTag) const;

	void Write(std::ostream& output);
	void Read(std::istream& input);

	Notifier<void(Version version)> GoToVersionEvent;
	using GoToVersionConnection = decltype(GoToVersionEvent)::ConnectionType;	///< GoToVersionEvent.Connect() returns this type

	Notifier<void(BranchTag)> EditHistoryChangedEvent;
	using EditHistoryChangedConnection = decltype(EditHistoryChangedEvent)::ConnectionType;	///< EditHistoryChangedEvent.Connect() returns this type

	// Side note for BranchAddedEvent: it might make sense sometime in the future to combine, or wrap these history changes around the command so there is only one message sent to the target.
	Notifier<void(BranchTag newBranchTag, Version parentVersion)> BranchAddedEvent;
	using BranchAddedConnection = decltype(BranchAddedEvent)::ConnectionType;	///< BranchAddedEvent.Connect() returns this type

	Notifier<void(BranchTag branchTag, Revision maxRevision)> BranchDeletedEvent;
	using BranchDeletedConnection = decltype(BranchDeletedEvent)::ConnectionType;	///< BranchDeletedEvent.Connect() returns this type

	/** @brief Notifies HistoryListSecondary instances to remove a version, which must be the most recent version.
			This is the result of a destructive undo operation.
	*/
	Notifier<void(Version version)> RemoveVersionEvent;
	using RemoveVersionConnection = decltype(RemoveVersionEvent)::ConnectionType;	///< RemoveVersionEvent.Connect() returns this type


private:
	void RemoveInvalidBookmarks();
	bool BranchExists(BranchTag branchTag);

	HistoryList<CommandUPtr> m_hlist;
	
	/**	A list that associates all the branch tags with convenient names.
		
		Maintaining names for branch tags is really a GUI thing and CommandHistory shouldn't need to care about them.
		But there are a couple of reason for keeping this list here. One, the EditWindow only exists while it's open 
		but we want the names to persist as long as the program. Two, it's convenient for the CommandHistory to 
		be the one to add entries to the list, giving a default name (branch creation time) when it is added.
	*/
	std::vector<BranchInfo>	m_branches;
	std::vector<Version>	m_bookmarks;
};


std::string GetElementName(const ElementID& elemType);
std::string GetParameterName(const ParamID& paramID);
std::string GetParameterDetail(const ParamID& paramID);

#endif  // COMMAND_H_INCLUDED
