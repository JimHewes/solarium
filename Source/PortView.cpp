/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <algorithm>
#include "PortView.h"
#include "ElementView.h"
#include "ConnectorView.h"
#include "Surface.h"

using std::string;

PortView::PortView(PortID portID)
	: m_isDragging(false)
	,m_fillColor(Colours::white)
	,m_portID(portID)
{
	setBounds(0,0,m_size,m_size);

	// Attempt to get owning ConnectionMng interface, but don't fail if you can't.
	// The portView may be added to the parent component later.
	m_pConnectionMng = GetOwningConnectionMng();

	Surface* pSurface = findParentComponentOfClass<Surface>();
	if (pSurface != nullptr)
	{
		m_connectorDragStartStopConnection = pSurface->ConnectorDragStartStopEvent.Connect([this](PortView* pOtherPortView)
		{
			OnConnectorDragStartStop(pOtherPortView);
		});
	}
}

PortView::~PortView()
{
	// Notify all listeners that I'm going bye-bye and don't try to call me again.
	for (auto& pl : m_listeners)
	{
		pl->OnPortViewDestoyed(this);
	}
	ElementView* pOwningElementView = findParentComponentOfClass<ElementView>();
	if (pOwningElementView)
	{
		pOwningElementView->removeComponentListener(this);
	}
}

void PortView::OnConnectorDragStartStop(PortView* pOtherPortView)
{
	if (pOtherPortView)
	{
		ElementView* pParent = findParentComponentOfClass<ElementView>();
		Preset* pModel = pParent->GetModel();
		if (pModel->CanBeConnected(m_portID,pOtherPortView->GetPortID()))
		{
			m_fillColor = Colour(0xFFFFFFA8);
		}
	}
	else
	{
		m_fillColor = Colours::white;
	}
	repaint();
}

void PortView::parentHierarchyChanged()
{
	m_pConnectionMng = GetOwningConnectionMng();

	ElementView* pOwningElementView = findParentComponentOfClass<ElementView>();
	if (!m_isRegisteredWithOwningNode)
	{
		if (pOwningElementView)
		{
			pOwningElementView->addComponentListener(this);
			m_isRegisteredWithOwningNode = true;
		}
	}
	else
	{
		if (pOwningElementView == nullptr)
		{
			m_isRegisteredWithOwningNode = false;
		}
	}

	Surface* pSurface = findParentComponentOfClass<Surface>();
	if (pSurface)
	{
		m_connectorDragStartStopConnection = pSurface->ConnectorDragStartStopEvent.Connect([this](PortView* pOtherPortView)
		{
			OnConnectorDragStartStop(pOtherPortView);
		});
	}
}

// Here it is assumed that Surface implements ConnectionMng interface.
ConnectionMng* PortView::GetOwningConnectionMng()
{
	Component* comp = this;
	while ((comp != nullptr) && (nullptr == dynamic_cast<Surface*>(comp)))
	{
		comp = comp->getParentComponent();
	}
	return dynamic_cast<ConnectionMng*>(comp);	// casting a nullptr just gives a nullptr
}

/** Gets the position of the center of the PortView in terms of Surface coordinates.

	This can be called by Connectors to get the position they attach to.\n
	\n
	The PortView may be within a heirarchy of components. This function will recursively add
	the position of each parent until it reaches the Surface component.

	@returns The position of the center of the PortView in terms of Surface coordinates.
*/
Point<int> PortView::GetSurfacePosition()
{
	Component* comp = this;
	Point<int> pos;
	do
	{
		pos += comp->getPosition();
		comp = comp->getParentComponent();

	} while ((comp != nullptr) && (nullptr == dynamic_cast<Surface*>(comp)));

	m_center.setX(pos.getX() + (getWidth() / 2));
	m_center.setY(pos.getY() + (getHeight() / 2));

	return m_center;
}

PortID PortView::GetPortID()
{
	return m_portID;
}


void PortView::OwningNodeWasMoved()
{
	// we are not dragging so don't call OnMoveDragConnector();
	//m_pConnectionMng->OnMoveDragConnector(this, GetSurfacePosition());
	GetSurfacePosition();
	Notify();
}

void PortView::componentMovedOrResized(Component& component,bool wasMoved,bool wasResized)
{
	component;
	wasResized;
	wasMoved;

	// we are not dragging so don't call OnMoveDragConnector();
	//m_pConnectionMng->OnMoveDragConnector(this, GetSurfacePosition());

	// It might seem as if we only need to do the following lines if "wasMoved" is true.
	// And in fact "WasMoved" is true when the whole element gets moved. But when the 
	// input/modualtion/control sections get collapsed or expanded, this function actually 
	// gets called with only "wasResized" true because it happens as a result of the element 
	// changing size due to the expansion or collapse. So to cause the connector to follow 
	// the port position we need to also do the following when "wasResized" is true.
	GetSurfacePosition();
	Notify();
}


void PortView::paint(Graphics& g)
{
	Rectangle<float> areaf = getLocalBounds().toFloat();
	//areaf.reduce(1, 1);

	g.setColour(m_fillColor);
	//g.fillEllipse(areaf);
	g.fillRect(areaf);
	g.setColour(Colours::black);
	//g.drawEllipse(areaf, 1);
	g.drawRect(areaf,1);

	// Draw a smaller solid rectangle in the middle when this portView has a connector connected to it.
	if (!m_listeners.empty())
	{
		if (m_listeners.front()->IsReadOnly())
		{
			g.setColour(Colours::grey);
		}
		auto areaf2 = areaf;
		areaf2.reduce(6,6);
		g.fillRect(areaf2);
	}
}

void PortView::mouseDrag(const MouseEvent& event)
{
	//DBG("Mouse drag on PortView. dX = " << x << ", dY = " << y);

	if (m_pConnectionMng == nullptr)
	{
		return;	// dragging has no effect if the PortView is not a child of the surface.
	}

	if (m_isDragging == false)
	{
		// call surface to CreateConnection()
		m_pConnectionMng->OnCreateDragConnector(this,nullptr);
		m_pConnectionMng->OnMoveDragConnector(this,GetSurfacePosition());
		m_isDragging = true;
	}

	Point<int> newPoint = m_center + event.getOffsetFromDragStart();
	m_pConnectionMng->OnMoveDragConnector(nullptr,newPoint);
	//DBG("Mouse drag new position. dX = " << newPoint.getX() << ", dY = " << newPoint.getY());

	if (DragAndDropContainer* const dragContainer = DragAndDropContainer::findParentDragContainerFor(this))
	{
		dragContainer->startDragging("PortView", this, ScaledImage(m_image));
	}
}

void PortView::mouseUp(const MouseEvent& event)
{
	event;
	if (m_isDragging)
	{
		m_isDragging = false;
		DBG("Mouse Up on Source PortView");
	}
	else
	{
		DBG("Mouse Up on Dest PortView");
	}
}

void PortView::AddConnectorViewListener(ConnectorView* pListener)
{
	auto iter = std::find_if(m_listeners.begin(),m_listeners.end(),[pListener](ConnectorView* l) { return pListener == l; });
	if (iter == m_listeners.end())
	{
		m_listeners.push_back(pListener);
	}
}

void PortView::RemoveConnectorViewListener(ConnectorView* pListener)
{
	m_listeners.remove_if([pListener](ConnectorView* l) { return pListener == l; });

	repaint(); // this is needed to clear away the little inner rectangle when a drag-drop does not complete successfully.
}

void PortView::Notify()
{
	for (auto& pl : m_listeners)
	{
		pl->OnPortViewMoved(this,m_center);
	}
}

bool PortView::IsDropAllowed(const SourceDetails& dragSourceDetails)
{
	// Only accepts drags from other PortViews
	if (dragSourceDetails.description != "PortView" || m_pConnectionMng == nullptr)
	{
		return false;
	}

	PortView* targetPortView = (PortView*)dragSourceDetails.sourceComponent.get();
	PortID targetPortID = targetPortView->GetPortID();

	ElementView* pParent = findParentComponentOfClass<ElementView>();
	Preset* pModel = pParent->GetModel();
	return pModel->CanBeConnected(m_portID, targetPortID);
}


bool PortView::isInterestedInDragSource(const SourceDetails& dragSourceDetails)
{
	bool dropAllowed = IsDropAllowed(dragSourceDetails);

	DBG("PortView is" << (dropAllowed ? " " : " NOT ") << "interested in a drop.");
	return dropAllowed;
}


void PortView::itemDropped(const SourceDetails& dragSourceDetails)
{
	dragSourceDetails;
	DBG("PortView item is dropped.");

	if (m_pConnectionMng != nullptr)// dragging has no effect if the PortView is not a child of the surface.
	{
		m_pConnectionMng->FinalizeDrop(this,GetSurfacePosition());
		m_fillColor = Colours::white;
		repaint();
	}
}


// Note that this will never get called if isInterestedInDragSource returns false.
void PortView::itemDragEnter(const SourceDetails& dragSourceDetails)
{
	DBG("itemDragEnter.");

	bool dropAllowed = IsDropAllowed(dragSourceDetails);

	m_previousFillColor = m_fillColor;
	m_fillColor = dropAllowed ? Colours::green : Colours::red;
	repaint();

}

void PortView::itemDragExit(const SourceDetails& dragSourceDetails)
{
	DBG("itemDragExit.");
	dragSourceDetails;
	m_fillColor = m_previousFillColor;
	repaint();
}
