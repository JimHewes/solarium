/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MISCCONTROLS_H_INCLUDED
#define MISCCONTROLS_H_INCLUDED
#include <string>
#include "Preset.h"
#include "Slider2.h"
class CommandHistory;

class SliderSwing : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
};


#if 0
class PanelButton : public Component,
					public Button::Listener
{
public:
	PanelButton(const std::string& label);
	void resized() override;
	void SetState(bool isOn);
	bool GetState();

private:
	std::unique_ptr<TextButton>		m_pButton;
	std::unique_ptr<ImageComponent>	m_pLed;
	std::unique_ptr<Label>			m_pLabel;
	bool m_isOn{ false };
};
#endif // 0


class PanelButton : public Component,
					public Button::Listener
{
public:
	PanelButton(const std::string& label, std::function<void(PanelButton*)> buttonPressFunction = nullptr);
	void resized() override;
	void SetState(bool isOn);
	bool GetState();
	void buttonClicked(Button*) override;

private:
	std::unique_ptr<TextButton>		m_pButton;
	std::unique_ptr<ImageComponent>	m_pLed;
	std::unique_ptr<Label>			m_pLabel;
	bool m_isOn{ false };
	std::function<void(PanelButton*)> m_buttonPressFunction;
};


class PanelButtonPreset : public PanelButton
{
public:
	PanelButtonPreset(const std::string& label, const ParamID& paramID, Preset * pPreset, CommandHistory* pCommandHistory);
	ParamID GetParamID() { return m_paramID; }
	void buttonClicked(Button*) override;

private:
	Preset::ParameterChangedConnection		m_paramChangedConnection;
	Preset*							m_pPreset;
	CommandHistory*					m_pCommandHistory;
	ParamID							m_paramID;
};

const int imageLedOnSize = 264;
const int imageLedOffSize = 267;

#endif  // MISCCONTROLS_H_INCLUDED
