/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <math.h>
#include <cassert>
#include "OscillatorPanel.h"
#include "Preset.h"
#include "Command.h"

using std::vector;
static const juce::Identifier paramIndexID = "paramIndex";

OscillatorPanel::OscillatorPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
	,m_waveCombos(7)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 75;
	const int rightWidth = 105;
	const int ctrlHeight = 20;
	const int space = 10;
	const int rowHeight = ctrlHeight + space;
	int yPos = space;

	m_oscTypeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_oscTypeLabel.setText("Type:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_oscTypeLabel);
	m_oscTypeCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	std::vector<std::string> oscTypeList = m_pPreset->GetParameterListText(ParamID(elemID,oscType));;
	for (size_t i = 0; i < oscTypeList.size(); ++i)
	{
		m_oscTypeCombo.addItem(oscTypeList[i],i + 1);
	}
	m_oscTypeCombo.getProperties().set(paramIndexID, oscType);
	m_oscTypeCombo.addListener(this);
	addAndMakeVisible(m_oscTypeCombo);

	yPos += rowHeight;
	m_waveLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_waveLabel.setText("Wave:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_waveLabel);

	// It might seem better to have only one combobox for the waves and populate it depending on the current osc type.
	// But it seems that when you clear, repopulate, and then select an item in the combobox, the combobox triggers a 
	// juce notification that we don't want to happen, even though we specified not to send one. That in turn causes 
	// a superflous CmdSetParameterValue command to be sent and added to the edit history.
	// So, that's why there is one combobox for each type of oscillator here.
	m_waveCombos[0].setBounds(rightX, yPos, rightWidth, ctrlHeight);	// First initialize the Off option
	m_waveCombos[0].addItem("None", 1);
	m_waveCombos[0].setSelectedId(1, NotificationType::dontSendNotification);
	m_waveCombos[0].addListener(this);
	addChildComponent(m_waveCombos[0]);
	for (size_t i = 1; i < oscTypeList.size(); ++i)	// now do the remaining oscillator types
	{
		m_waveCombos[i].setBounds(rightX, yPos, rightWidth, ctrlHeight);
		std::vector<std::string> waveList = m_pPreset->GetParameterListText(ParamID(m_elemID, i));
		for (size_t j = 0; j < waveList.size(); ++j)
		{
			m_waveCombos[i].addItem(waveList[j], j + 1);
		}
		m_waveCombos[i].getProperties().set(paramIndexID, static_cast<int>(i));
		m_waveCombos[i].addListener(this);
		addChildComponent(m_waveCombos[i]);
	}

	yPos += rowHeight;
	m_shapeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_shapeLabel.setText("Shape:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_shapeLabel);
	m_shapeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_shapeSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,oscShape));
	m_shapeSlider.setRange(rangeInfo.minValue , rangeInfo.maxValue, 1);
	m_shapeSlider.setTextValueSuffix(" %");
	m_shapeSlider.getProperties().set(paramIndexID, oscShape);
	m_shapeSlider.addListener(this);
	addAndMakeVisible(m_shapeSlider);

	yPos += rowHeight;
	m_coarseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_coarseLabel.setText("Coarse:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_coarseLabel);

	m_coarseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_coarseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,oscCoarse));
	m_coarseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_coarseSlider.getProperties().set(paramIndexID, oscCoarse);
	m_coarseSlider.addListener(this);
	addAndMakeVisible(m_coarseSlider);

	m_coarseSyncCombo.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	std::vector<std::string> oscCoarseSyncList = m_pPreset->GetParameterListText(ParamID(elemID, oscCoarseSync));;
	for (size_t i = 0; i < oscCoarseSyncList.size(); ++i)
	{
		m_coarseSyncCombo.addItem(oscCoarseSyncList[i], i + 1);
	}
	m_coarseSyncCombo.getProperties().set(paramIndexID, oscCoarseSync);
	m_coarseSyncCombo.addListener(this);
	addAndMakeVisible(m_coarseSyncCombo);

	m_coarseNoTrackSlider.setBounds(rightX, yPos, rightWidth, ctrlHeight);
	m_coarseNoTrackSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID, oscCoarseNoTrack));
	m_coarseNoTrackSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_coarseNoTrackSlider.getProperties().set(paramIndexID, oscCoarseNoTrack);
	m_coarseNoTrackSlider.addListener(this);
	addAndMakeVisible(m_coarseNoTrackSlider);


	yPos += rowHeight;
	m_fineLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_fineLabel.setText("Fine:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_fineLabel);
	m_fineSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_fineSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,oscFine));
	m_fineSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_fineSlider.getProperties().set(paramIndexID, oscFine);
	m_fineSlider.addListener(this);
	addAndMakeVisible(m_fineSlider);

	yPos += rowHeight;
	m_clockSyncButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_clockSyncButton.setButtonText("Clock Sync");
	m_clockSyncButton.addListener(this);
	addAndMakeVisible(m_clockSyncButton);

	yPos += rowHeight;
	m_noTrackButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_noTrackButton.setButtonText("No Track");
	m_noTrackButton.addListener(this);
	addAndMakeVisible(m_noTrackButton);

	yPos += rowHeight;
	m_lowButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_lowButton.setButtonText("Low");
	m_lowButton.addListener(this);
	addAndMakeVisible(m_lowButton);

	yPos += rowHeight;
	m_glideButton.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_glideButton.setButtonText("Glide:");
	m_glideButton.addListener(this);
	addAndMakeVisible(m_glideButton);
	m_glideRateSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_glideRateSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,oscGlideRate));
	m_glideRateSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_glideRateSlider.setTextValueSuffix(" sec");
	m_glideRateSlider.getProperties().set(paramIndexID, oscGlideRate);
	m_glideRateSlider.addListener(this);
	addAndMakeVisible(m_glideRateSlider);

	yPos += rowHeight;
	m_phaseLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_phaseLabel.setText("Phase:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_phaseLabel);
	m_phaseSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_phaseSlider.setSliderStyle(Slider2::LinearBar);
	rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,oscPhase));
	m_phaseSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_phaseSlider.setTextValueSuffix(" deg");
	m_phaseSlider.getProperties().set(paramIndexID, oscPhase);
	m_phaseSlider.addListener(this);
	addAndMakeVisible(m_phaseSlider);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int , bool ) 
	{ 
		if(paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	m_multipleParametersChangedConnection = m_pPreset->SubscribeToMultipleParametersChangedEvent([this](Version, bool)
	{
		Update();
	});

	Update();
}


OscillatorPanel::~OscillatorPanel()
{
	m_oscTypeCombo.removeListener(this);
	m_shapeSlider.removeListener(this);
	m_coarseSlider.removeListener(this);
	m_coarseSyncCombo.removeListener(this);
	m_coarseNoTrackSlider.removeListener(this);
	m_fineSlider.removeListener(this);
	m_glideRateSlider.removeListener(this);
	m_phaseSlider.removeListener(this);
	m_clockSyncButton.removeListener(this);
	m_noTrackButton.removeListener(this);
	m_lowButton.removeListener(this);
	m_glideButton.removeListener(this);
	std::vector<std::string> oscTypeList = m_pPreset->GetParameterListText(ParamID(m_elemID, oscType));;
	for (size_t i = 0; i < oscTypeList.size(); ++i)
	{
		m_waveCombos[i].removeListener(this);
	}
}

void OscillatorPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void OscillatorPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	assert(
		(comboBoxThatHasChanged == &m_oscTypeCombo) ||
		(comboBoxThatHasChanged == &m_coarseSyncCombo) ||
		(comboBoxThatHasChanged == &m_waveCombos[1]) ||
		(comboBoxThatHasChanged == &m_waveCombos[2]) ||
		(comboBoxThatHasChanged == &m_waveCombos[3]) ||
		(comboBoxThatHasChanged == &m_waveCombos[4]) ||
		(comboBoxThatHasChanged == &m_waveCombos[5]) ||
		(comboBoxThatHasChanged == &m_waveCombos[6]));

	// Extract the parameter index from the slider control.
	var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = comboBoxThatHasChanged->getSelectedId() - 1;
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

void OscillatorPanel::sliderValueChanged(Slider2* slider)
{
	assert(
		(slider == &m_shapeSlider) ||
		(slider == &m_coarseSlider) ||
		(slider == &m_coarseNoTrackSlider) ||
		(slider == &m_fineSlider) ||
		(slider == &m_glideRateSlider) ||
		(slider == &m_phaseSlider));

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		int coarseOffset = 0;
		if ((int)*pVar == oscCoarse && m_lowButton.getToggleState())	// For the coarse slider, Maintain range of -60 to + 60 whether Low is on or not.
		{
			coarseOffset = 60;
		}
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value + coarseOffset));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void OscillatorPanel::buttonClicked(Button* button)
{
	if (button == &m_glideButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, oscGlide), m_glideButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_clockSyncButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, oscClockSync), m_clockSyncButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_noTrackButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, oscNoTrack), m_noTrackButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_lowButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, oscLow), m_lowButton.getToggleState() ? 1 : 0));
	}

	// Update needs to be called here because the button state may need to be reversed as a result of 
	// what the Preset class determines. For example, if you turn on the Low switch and then send that 
	// command to Preset, the change will be ignored if MidiClockSync is on. So you need the botton 
	// to be turned back off. The call to Update is an easy way to do it.
	Update();
}

void OscillatorPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;
	}
	int currentOscType = m_pPreset->GetParameterValue(ParamID(m_elemID,oscType));
	m_oscTypeCombo.setSelectedId(currentOscType + 1, NotificationType::dontSendNotification);

	m_waveCombos[0].setVisible(currentOscType == 0);
	std::vector<std::string> oscTypeList = m_pPreset->GetParameterListText(ParamID(m_elemID, oscType));;
	for (int i = 1; i < static_cast<int>(oscTypeList.size()); ++i)
	{
		m_waveCombos[i].setVisible(currentOscType == i);
		m_waveCombos[i].setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, i)) + 1, NotificationType::dontSendNotification);
	}

	m_shapeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,oscShape)), NotificationType::dontSendNotification);
	m_coarseSyncCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID, oscCoarseSync)) + 1, NotificationType::dontSendNotification);
	m_coarseNoTrackSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID, oscCoarseNoTrack)), NotificationType::dontSendNotification);
	m_fineSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,oscFine)), NotificationType::dontSendNotification);
	m_glideRateSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,oscGlideRate)), NotificationType::dontSendNotification);
	m_phaseSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,oscPhase)), NotificationType::dontSendNotification);

	bool clockSyncOn = m_pPreset->GetParameterValue(ParamID(m_elemID, oscClockSync)) != 0;
	bool noTrackOn = m_pPreset->GetParameterValue(ParamID(m_elemID, oscNoTrack)) != 0;
	bool lowOn = m_pPreset->GetParameterValue(ParamID(m_elemID, oscLow)) != 0;
	m_clockSyncButton.setToggleState(clockSyncOn, NotificationType::dontSendNotification);
	m_noTrackButton.setToggleState(noTrackOn && !clockSyncOn, NotificationType::dontSendNotification);
	m_lowButton.setToggleState(lowOn, NotificationType::dontSendNotification);
	m_glideButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,oscGlide)) != 0, NotificationType::dontSendNotification);

	// Oscillator phase doesn't exist for all oscillator types so only show it conditionally.
	m_phaseLabel.setVisible(currentOscType == 1);
	m_phaseSlider.setVisible(currentOscType == 1);
	m_coarseSyncCombo.setVisible(clockSyncOn);
	m_coarseNoTrackSlider.setVisible(!clockSyncOn && noTrackOn);
	m_coarseSlider.setVisible(!clockSyncOn && !noTrackOn);

	// The range of the normal Coarse parameter is always the same. It's just displayed differently if the Low button is enabled.
	int coarseOffset = lowOn ? -60 : 0;
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(m_elemID, oscCoarse));
	m_coarseSlider.setRange(rangeInfo.minValue + coarseOffset, rangeInfo.maxValue + coarseOffset, 1);
	m_coarseSlider.setValue(coarseOffset + m_pPreset->GetParameterValue(ParamID(m_elemID, oscCoarse)), NotificationType::dontSendNotification);
}
