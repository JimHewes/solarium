/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <string>
#include <math.h>
#include "gsl_util.h"
#include "Surface.h"
#include "Layout.h"
#include "AppCmdMgr.h"
#include "Command.h"

using std::string;


void SelectionRect::paint(Graphics& g)
{
	auto area = getLocalBounds();
	g.setColour(Colour(0x20333333));
	g.fillRect(area);
}


Surface::Surface(Preset* pPreset, CommandHistory* pCommandHistory, ViewModel* pViewModel)
	:m_pPreset(pPreset)
	, m_pCommandHistory(pCommandHistory)
	, m_pViewModel(pViewModel)
{
	assert(m_pPreset);
	assert(m_pViewModel);
	setComponentID("Surface");

	m_visibleElementsChangedConnection = m_pViewModel->VisibleElementsChangedEvent.Connect([this]() 
	{
		Update();
	});

	///////////////////////// Set up Element View Observer ///////////////////////////
	auto elementViewSelectionChangedCallback = [this]() {
		if (m_pViewModel->ElementViewSelectionModel().HasSelection() && m_pViewModel->ConnectorSelectionModel().HasSelection())
		{
			m_pViewModel->ConnectorSelectionModel().Clear();
		}
		repaint();
	};
	m_elementSelectionChangedConnection = m_pViewModel->ElementViewSelectionModel().selectionChangedEvent.Connect(elementViewSelectionChangedCallback);

	///////////////////////// Set up Connector Observer ///////////////////////////////////
	auto connectorSelectionChangedCallback = [this]() {
		if (m_pViewModel->ConnectorSelectionModel().HasSelection() && m_pViewModel->ElementViewSelectionModel().HasSelection())
		{
			m_pViewModel->ElementViewSelectionModel().Clear();
		}
		repaint();
	};
	m_connectorSelectionChangedConnection = m_pViewModel->ConnectorSelectionModel().selectionChangedEvent.Connect(connectorSelectionChangedCallback);

	// This function is used to drag multiple selected elements. It's called by the one element that's being dragged by the mouse.
	m_moveSelectedElementsBy = [this](int deltaX, int deltaY, bool doSave)
	{
		for (auto& elemID : m_pViewModel->ElementViewSelectionModel())
		{
			auto iter = std::find_if(m_elementViewList.begin(), m_elementViewList.end(), [elemID](auto& pElementView) { return elemID == pElementView->GetElementID();});
			if (iter != m_elementViewList.end())
			{
				(*iter)->MoveBy(deltaX, deltaY);
				if (doSave)
				{
					m_pViewModel->SetLocation(elemID, Location{ gsl::narrow<int16_t>((*iter)->getX()), gsl::narrow<int16_t>((*iter)->getY()) });
				}
			}

		}
	};
	Update();

}

Surface::~Surface()
{
	// Make sure you delete ElementViews first before the the listener lists gets deleted.
	// This is because the ElementViews might try to remove themselves as listeners when they get 
	// deleted and if the listener list has already been deleted first you'll get an assertion.
	m_connectorViewList.clear();
	m_elementViewList.clear();
	m_elementSelectionChangedConnection->Disconnect();
	m_connectorSelectionChangedConnection->Disconnect();
}

Component* Surface::FindPortViewFromID(const PortID& id) const
{
	// Define the recursive part for the search as a lambda. Note we cannot declare it using "auto" since it is recursive.
	std::function<Component*(Component*, const PortID&)> FindPortViewFromIDRec = [&FindPortViewFromIDRec](Component* comp, const PortID& id) ->Component*
	{
		PortView* pPortView = dynamic_cast<PortView*>(comp);
		if (pPortView)
		{
			if (pPortView->GetPortID() == id)
			{
				return comp;
			}
		}
		int numChildren = comp->getNumChildComponents();
		for (int i = 0; i < numChildren; ++i)
		{
			Component* childComp = FindPortViewFromIDRec(comp->getChildComponent(i), id);
			if (childComp)
			{
				return childComp;
			}
		}

		return nullptr;
	};


	for (auto& pnv : m_elementViewList)
	{
		Component* comp = FindPortViewFromIDRec(pnv.get(), id);
		if (comp)
		{
			return comp;
		}
	}
	return nullptr;
}

/** Given the ID of an element, will return a pointer to the ElementView associated with it.

	@param[in] elemID		The ID of the element to get the pointer of.
	@returns				A pointer to the ElementView associated with the given element ID.
*/
ElementView* Surface::FindViewFromModel(const ElementID& elemID) const
{
	ElementView* result{nullptr};

	auto iter = std::find_if(m_elementViewList.begin(),m_elementViewList.end(),[elemID](const ElementViewUPtr& p) { return p->GetElementID() == elemID; });
	if (iter != m_elementViewList.end())
	{
		result = iter->get();
	}
	return result;
}

/** Given a connector, will return a pointer to the ConnectorView associated with it.

	@param[in] connector	The connector for which the pointer of the ConnectorView is returned.
	@returns				A pointer to the ConnectorView associated with the given connector.
*/
Component* Surface::FindViewFromModel(const Connector& connector) const
{
	Component* result{};
	auto iter = std::find_if(m_connectorViewList.begin(),m_connectorViewList.end(),[&connector](const ConnectorViewUPtr& p) { return p->GetModel() == connector; });
	if (iter != m_connectorViewList.end())
	{
		result = iter->get();
	}
	return result;
}

bool ContainsConnector(const Connector& connector, std::vector<Connector>& conectorList)
{
	auto iter = std::find_if(conectorList.begin(), conectorList.end(), [&connector](const Connector& c) { return c == connector; });
	return iter != conectorList.end();
}

void Surface::Update()
{
	m_connectorViewList.clear();

	auto newConnectorList = m_pPreset->GetConnectorList();

	// Order is:  remove Connectors, add ElementViews, remove ElementViews, add Connectors
	std::vector<ConnectorView*> connectorViewsToRemove;
	for (auto& pConnectorView : m_connectorViewList)
	{
		if (!ContainsConnector(pConnectorView->GetModel(), newConnectorList))
		{
			connectorViewsToRemove.push_back(pConnectorView.get());
		}
	}
	for (auto pConnectorView : connectorViewsToRemove)
	{
		RemoveConnectorView(pConnectorView);
	}

	auto elemList = m_pViewModel->GetVisibleElementList();

	for (auto elemItem : elemList)
	{
		ElementView* pElementView = FindViewFromModel(elemItem);
		if (nullptr == pElementView)
		{
			ElementViewUPtr pNewElementView = std::make_unique<ElementView>(m_pPreset, m_pCommandHistory, m_pViewModel, elemItem, m_moveSelectedElementsBy);
			pNewElementView->addComponentListener(this);
			addAndMakeVisible(pNewElementView.get());
			m_elementViewList.push_back(std::move(pNewElementView));
		}
		else
		{
			// Element doesn't need to be added, but its location might be different.
			// So update that.
			Location loc = m_pViewModel->GetLocation(elemItem);
			pElementView->setTopLeftPosition(loc.x,loc.y);

			// Update the settings in the ElementView.
			pElementView->UpdateFromModel();
		}
	}

	std::vector<ElementView*> elementViewsToRemove;
	for (auto& pElementView : m_elementViewList)
	{
		if (!m_pViewModel->IsElementVisible(pElementView->GetElementID()))
		{
			elementViewsToRemove.push_back(pElementView.get());	// should we just delete it here?
		}
	}

	for (auto pElementView : elementViewsToRemove)
	{
		RemoveElementView(pElementView);
	}

	for (auto connector : newConnectorList)
	{
		if (nullptr == FindViewFromModel(connector))
		{
			if (m_pViewModel->IsElementVisible(connector.startPort.elemID) && m_pViewModel->IsElementVisible(connector.endPort.elemID))
			{
				PortView* pEndingPortView = dynamic_cast<PortView*>(FindPortViewFromID(connector.endPort));
				PortView* pStartingPortView = dynamic_cast<PortView*>(FindPortViewFromID(connector.startPort));

				ConnectorViewUPtr pNewConnectorView = std::make_unique<ConnectorView>(connector, pStartingPortView, pEndingPortView, m_pViewModel, m_pPreset);
				pNewConnectorView->setAlwaysOnTop(true);
				addAndMakeVisible(pNewConnectorView.get());
				m_connectorViewList.push_back(std::move(pNewConnectorView));
			}
		}
	}

	DoLayout();
	repaint();
}

void Surface::DoLayout()
{
	std::vector<ElementInput> elemInputs;

	// Convert the list all active elements to the input structs needed to call the layout function.
	auto elemList = m_pViewModel->GetVisibleElementList();
	int width = 0;
	for (auto& elemID : elemList)
	{
		ElementView* pElemView = FindViewFromModel(elemID);
		uint16_t height = uint16_t(pElemView->getHeight());
		width = pElemView->getWidth();
		elemInputs.push_back(ElementInput{ elemID, height });
	}

	std::vector<ElementOutput> elemOutputs = ArrangeElements(elemInputs, width);

	// Reposition the elements
	for (auto& elem : elemOutputs)
	{
		ElementView* pElemView = FindViewFromModel(elem.elemID);
		pElemView->setTopLeftPosition(elem.xPos, elem.yPos);
		m_pViewModel->SetLocation(elem.elemID, Location{ gsl::narrow<int16_t>(elem.xPos), gsl::narrow<int16_t>(elem.yPos) });
	}
}

void Surface::SurfacePopupMenuHandler::modalStateFinished(int returnValue)
{
	if (returnValue > 0)	// 0 means the menu was dismissed without choosing anything.
	{
		auto iter = std::find_if(m_pSurface->m_menulist.begin(),
			m_pSurface->m_menulist.end(),
			[returnValue](const MenuItem& item) { return item.user == returnValue; });

		if (iter != m_pSurface->m_menulist.end())
		{
			m_pSurface->m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdAddElement>(iter->elemID, Location{ gsl::narrow<int16_t>(m_position.getX()), gsl::narrow<int16_t>( m_position.getY()) }));
			m_pSurface->Update();
		}
		else
		{
			assert(returnValue == EditUnMuteAll);	// The only other value expect now is EditUnMuteAll
		}
	}
}

void Surface::mouseDown(const MouseEvent & event)
{
	//DBG("Mouse down on Surface.");


	if (event.mods.isRightButtonDown())
	{

		PopupMenu topMenu;
		PopupMenu nodeMenu;
		PopupMenu instanceMenu;		
		m_menulist = m_pViewModel->GenerateMenuItemList();

		string currentSubMenuName;
		int itemResultID = 1;
		for (auto& item : m_menulist)
		{
			if (currentSubMenuName != item.groupFriendlyname)
			{
				if (instanceMenu.getNumItems() > 0)
				{
					nodeMenu.addSubMenu(currentSubMenuName, instanceMenu);
					instanceMenu.clear();
				}
				currentSubMenuName = item.groupFriendlyname;
			}
			if (item.groupFriendlyname.empty())
			{
				nodeMenu.addItem(itemResultID, item.itemFriendlyname);
			}
			else
			{
				instanceMenu.addItem(itemResultID, item.itemFriendlyname);
			}
			item.user = itemResultID;
			++itemResultID;
		}

		// Handle the case where the last item in the menu is a group.
		if (instanceMenu.getNumItems() > 0)
		{
			nodeMenu.addSubMenu(currentSubMenuName,instanceMenu);
		}

		topMenu.addSubMenu("Add", nodeMenu);
		topMenu.addCommandItem(&GetGlobalCommandManager(), CommandIDs::EditUnMuteAll, "Unmute All");
		const int result = topMenu.show(0, 0, 0, 0, new SurfacePopupMenuHandler(this, event.getPosition()));

		result;
		assert(result == 0);
	}

}

void Surface::mouseUp(const MouseEvent & event)
{
	event;
	setMouseCursor(MouseCursor::NormalCursor);

	bool smallDistance = std::abs(event.getDistanceFromDragStartX()) < 3 && std::abs(event.getDistanceFromDragStartY()) < 3;
	// Clear the selection if there wasn't a drag, or if the drag was very small. The reason for this is because 
	// it can be hard for the user to avoid very small drags and thus hard to deselect elements.
	if ((!m_moveDragInProgress || smallDistance) && !m_selectDragInProgress)
	{
		m_pViewModel->ElementViewSelectionModel().Clear();
		m_pViewModel->ConnectorSelectionModel().Clear();
	}
	m_moveDragInProgress = false;

	if (m_selectDragInProgress)
	{
		SelectElementsThatIntersectRectangle(m_selectionRect->getBounds());

		removeChildComponent(m_selectionRect.get());
		m_selectionRect.reset();
		m_selectDragInProgress = false;
	}
}

void Surface::mouseDrag(const MouseEvent& event)
{
	// Note that when doing the move-drag we must refer to screen position, not the position 
	// in the surface. That's because the surface will move around which affects the 
	// position of the event. But the screen position is a stable reference.

	Point<int> offsetFromLastMouseDragPosition;

	if (m_moveDragInProgress)
	{
		// Handle the second and subsequent drag-move events.
		offsetFromLastMouseDragPosition = event.getScreenPosition() - m_lastMouseDragPosition;
		m_lastMouseDragPosition = event.getScreenPosition();
	}
	else if (m_selectDragInProgress)
	{
		// Handle the second and subsequent drag-select events.
		// Create a rectangle from two points to we don't have to figure out which corner is which.
		Point<int> p1(event.getMouseDownX(),event.getMouseDownY());
		Point<int> p2(event.getMouseDownX() + event.getDistanceFromDragStartX(),
			event.getMouseDownY() + event.getDistanceFromDragStartY());
		Rectangle<int> r(p1,p2);
		m_selectionRect->setBounds(r);
	}
	else
	{
		// No kind of drag is in progress yet. Figure out which type.
		if (event.mods.isCommandDown())
		{
			m_selectionRect = std::make_unique<SelectionRect>();
			// Create a rectangle from two points to we don't have to figure out which corner is which.
			Point<int> p1(event.getMouseDownX(), event.getMouseDownY());
			Point<int> p2(	event.getMouseDownX() + event.getDistanceFromDragStartX(),
							event.getMouseDownY() + event.getDistanceFromDragStartY());
			Rectangle<int> r(p1,p2);
			m_selectionRect->setBounds(r);
			addAndMakeVisible(m_selectionRect.get());
			m_selectDragInProgress = true;
			setMouseCursor(MouseCursor::CrosshairCursor);
		}
		else
		{
			// handle the first drag event after mouse down.
			offsetFromLastMouseDragPosition = Point<int>(0,0);
			m_lastMouseDragPosition = event.getScreenPosition();
			setMouseCursor(MouseCursor::DraggingHandCursor);
			m_moveDragInProgress = true;
		}
	}
	if (m_moveDragInProgress)
	{
		SurfaceDragEvent.Notify(offsetFromLastMouseDragPosition);
	}
}


void Surface::mouseEnter(const MouseEvent & event)
{
	setMouseCursor(event.mods.isCommandDown() ? MouseCursor::CrosshairCursor : MouseCursor::NormalCursor);
}
void Surface::mouseExit(const MouseEvent & event)
{
	event;
	setMouseCursor(MouseCursor::NormalCursor);
}

void Surface::modifierKeysChanged(const ModifierKeys& modifiers)
{
	if (!m_moveDragInProgress && !m_selectDragInProgress)
	{
		setMouseCursor(modifiers.isCommandDown() ? MouseCursor::CrosshairCursor : MouseCursor::NormalCursor);
	}
}


void Surface::OnCreateDragConnector(PortView* pStartingPortView, PortView* pEndingPortView)
{
	DBG("CreateConnection called.");

	// If there is any existing Connector referred to by m_pDragConnectorView (and there shouldn't be) 
	// remove it from the Surface.
	if (m_pDragConnectorView)
	{
		removeChildComponent(m_pDragConnectorView.get());	// Ok, it's silly to have code for a condition I say shouldn't happen....so sue me.
	}
	m_pDragConnectorView = std::make_unique<ConnectorView>(Connector{},pStartingPortView,pEndingPortView, m_pViewModel, m_pPreset );
	addAndMakeVisible(m_pDragConnectorView.get());

	ConnectorDragStartStopEvent.Notify(pStartingPortView);
}

void Surface::OnMoveDragConnector(PortView* pPortView, Point<int> newPosition)
{
	//DBG("OnMoveConnection called.");
	if (m_pDragConnectorView)
	{
		m_pDragConnectorView->OnPortViewMoved(pPortView, newPosition);
	}
}

void Surface::FinalizeDrop(PortView* pPortView, Point<int> newPosition)
{
	newPosition;
	ConnectorDragStartStopEvent.Notify(nullptr);

	PortID startingNodeID = m_pDragConnectorView->GetStartingPortView()->GetPortID();
	PortID endingNodeID = pPortView->GetPortID();
	m_pDragConnectorView.reset();
	m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdCreateConnector>(startingNodeID, endingNodeID));
	Update();

}

void Surface::dragOperationEnded(const DragAndDropTarget::SourceDetails &)
{
	m_pDragConnectorView.reset();	// clear it if it exists. This happens when a drag and drop of a new connector doesn't succeed.
	ConnectorDragStartStopEvent.Notify(nullptr);
}

/** Selects all elements that overlap with the given rectangle.

	This function is used for selecting multiple elements by dragging the mouse to create 
	a rectangle. That rectangle is passed to this function to select all the elements.
	Elements do not have to completely overlap, only partially,\n
	This function does not deselect any existing elements first; it appends the newly 
	selected elements to the already selected elements.

	@param[in] selectionRect	The rectangle to compare against. Elements that overlap this rectangle 
								will be selected.
*/
void Surface::SelectElementsThatIntersectRectangle(Rectangle<int> selectionRect)
{
	for (auto& elem : m_elementViewList)
	{
		auto elemRect = elem->getBounds();
		if (selectionRect.intersects(elemRect))
		{
			m_pViewModel->ElementViewSelectionModel().SelectAppend(elem->GetElementID());
		}
	}
}


/** Removes all elements that are currently selected.

	Removing an element means that it's made inactive and is no longer visible.
	But it still exists in the model.
*/
void Surface::RemoveSelected()
{
	// Clear selected elements first. Other parts of the program might react to the selection being cleared 
	// and try to access the ElementView one more time.

	// We no longer remove the elements as child components. Some child components search for the Surface 
	// through the parent heirarchy to remove themselves as listeners. If they get removed from the heirarchy 
	// first then they can't do that and listener lists are left with dangling pointers.
	// So we depend on the JUCE framework to remove deleted components from the parent-child heirarchy.
	// (We could fix that by passing the Surface pointer down to all child components, but 
	// it's working fine for now the way it is.)


	// Removing elements can remove them from the selection model and therefore invalidate the iterator.
	// So we need to first make a copy of all element IDs that we want to remove.
	auto elementSelections = m_pViewModel->ElementViewSelectionModel();
	if (elementSelections.size() == 1)
	{
		auto elemID = elementSelections.GetFirstSelection();
		if (elemID.elemType != ElementType::VCAMaster && !(elemID.elemType == ElementType::EG && elemID.elemIndex == 5))
		{
			m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdRemoveElement>(elemID));
		}
	}
	else if (elementSelections.size() > 1)
	{
		CmdMacroUPtr pMacroCommand = std::make_unique<CmdMacro>();
		for (auto& elemID : elementSelections)
		{
			if (elemID.elemType != ElementType::VCAMaster && !(elemID.elemType == ElementType::EG && elemID.elemIndex == 5))
			{
				pMacroCommand->AddCommand(std::make_unique<CmdRemoveElement>(elemID));
			}
		}
		pMacroCommand->SetActionName("Remove Elements");
		pMacroCommand->SetDetails("Multiple");
		m_pCommandHistory->AddCommandAndExecute(std::move(pMacroCommand));
	}
	m_pViewModel->ElementViewSelectionModel().Clear();

	auto connectorSelections = m_pViewModel->ConnectorSelectionModel();
	if (connectorSelections.size() == 1)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdRemoveConnector>(connectorSelections.GetFirstSelection()));
	}
	else if(connectorSelections.size() > 1)
	{
		CmdMacroUPtr pMacroCommand = std::make_unique<CmdMacro>();

		for (auto c : connectorSelections)
		{
			pMacroCommand->AddCommand(std::make_unique<CmdRemoveConnector>(c));
		}
		pMacroCommand->SetActionName("Remove Connectors");
		pMacroCommand->SetDetails("Multiple");
		m_pCommandHistory->AddCommandAndExecute(std::move(pMacroCommand));
	}
	m_pViewModel->ConnectorSelectionModel().Clear();
	Update();
}

void Surface::CollapseAll()
{
	m_enableLayout = false;
	ExpandCollapseEvent.Notify(true);		// Signal all listeners (essentially PortLevelDividers) to collapse.
	m_enableLayout = true;
	DoLayout();
	repaint();
};
void Surface::ExpandAll()
{
	m_enableLayout = false;
	ExpandCollapseEvent.Notify(false);	// Signal all listeners (essentially PortLevelDividers) to expand.
	m_enableLayout = true;
	DoLayout();
	repaint();
}

void Surface::SelectAll()
{
	for(auto& pElementView : m_elementViewList) 
	{
		m_pViewModel->ElementViewSelectionModel().SelectAppend(pElementView->GetElementID());
	};
}

void Surface::RemoveConnectorView(ConnectorView* pConnectorViewToRemove)
{
	m_connectorViewList.remove_if([&pConnectorViewToRemove](ConnectorViewUPtr& cv){ return cv.get() == pConnectorViewToRemove; });
}
void Surface::RemoveElementView(ElementView* pElementViewToRemove)
{
	m_elementViewList.remove_if([&pElementViewToRemove](ElementViewUPtr& nv){ return nv.get() == pElementViewToRemove; });
}

void Surface::Clear()
{
	m_connectorViewList.clear();
	m_elementViewList.clear();
	m_pViewModel->Initialize();

}


void Surface::componentMovedOrResized(Component & component, bool wasMoved, bool wasResized)
{
	component; wasMoved;
	if (m_enableLayout && wasResized)
	{
		DoLayout();
	}
}
