/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PARAMETERLIST_H_INCLUDED
#define PARAMETERLIST_H_INCLUDED

#include <vector>
#include <iostream>
#include <cassert>


/** Defines the element types that are used in the Solaris; an element is the name used for a distinct module.

	The Loop EG element is odd because it has two outputs. So there are two more ElementTypes
	defined that are LoopEgX and LoopEgY. These are not actual elements but are used in the 
	ElementID of the X and Y output ports so that they can be treated as if they were separate 
	elements.
*/
enum class ElementType : uint8_t
{
	Off,
	Oscillator,
	Rotor,
	AM,
	Vector,
	Mixer,
	Filter,
	InsertFX,
	VCA,
	VCAMaster,
	PinkNoise,
	WhiteNoise,
	ExtInput,
	SPDIF_In,
	SPDIF_Out,
	LFO,
	VibratoLFO,
	EG,
	LoopEG,
	Velocity,
	Aftertouch,
	Note,
	ModWheel,
	AT_ModWheel,
	Ribbon,
	JoyX,
	JoyY,
	CC,
	Seq,
	Pedal,
	AssignButton,
	EnvFol,
	KeyTable,
	PolyAT,
	Lag,
	Breath,
	MaxValue,
	KeyTracking,
	FXChannel,
	ChorusFlanger,
	Phaser,
	Delay,
	EQ,
	Output,
	Arpeggiator,
	Home,
	SeqGlobal,
	Gate,
	System
};

/** Convert an enum to its underlying type. (It helps make things a little more readable.)

	typename is necessary in the return type because at this point in compilation, since the compiler 
	doesn't know what E is, it doesn't know if "std::underlying_type<E>::type" is a value or a type.
	It assumes it's a value. So you have to tell it that it's a type by using typename.
*/
template<typename E>
auto to_int(E e) -> typename std::underlying_type<E>::type
{
	assert(std::is_enum<E>::value);
	return static_cast<typename std::underlying_type<E>::type>(e);	// "typename" is required on this line for Mac but not for VS2015
}

struct ElementID
{
	ElementType		elemType;
	uint8_t			elemIndex;

	ElementID(){}
	ElementID(ElementType aElemType, int aElemIndex);

	void Write(std::ostream& output) const;
	void Read(std::istream& input);
};

bool operator == (const ElementID &lhs,const ElementID &rhs);
bool operator < (const ElementID lhs,const ElementID rhs);	// required in order to put ElementIDs into std:set (SelectionModel)

namespace std 
{
	// Create custom hasher for the ElementID to be used as a key.
	template <>
	struct hash<ElementID>
	{
		std::size_t operator()(const ElementID& elemID) const
		{
			// take advantage of the fact that we know the element type and element index are both less than 256.
			return to_int(elemID.elemType) * 256 + elemID.elemIndex;
		}
	};
}




enum class PortType : uint8_t { None,Input,Mod,Control,Output };
struct PortID
{
	ElementID	elemID;
	PortType	portType;
	uint8_t		portIndex{ 0 };	// It's important to zero this so that when portIndex is unused (in output ports), comparisons for equality work correctly.

	PortID() {}
	PortID(ElementID aElemID, PortType aPortType, int aPortIndex = 0);
	void Write(std::ostream& output) const;
	void Read(std::istream& input);
};

bool operator == (const PortID& lhs, const PortID& rhs);
bool operator < (const PortID& lhs, const PortID& rhs);

struct SourceID
{
	SourceID() : elemID(ElementType::Off, 0), outputPortIndex(0) {}
	SourceID(ElementID elemID_, int outputPortIndex_ = 0) : elemID(elemID_), outputPortIndex(outputPortIndex_) {}
	SourceID(PortID portID) : elemID(portID.elemID), outputPortIndex(portID.portIndex) {}
	bool IsConnected() { return elemID.elemType != ElementType::Off; }
	ElementID	elemID;
	int			outputPortIndex;
};
using SourceListType = std::vector < SourceID >;
bool operator == (const SourceID& lhs, const SourceID& rhs);
bool operator != (const SourceID& lhs, const SourceID& rhs);


using EnumListType = std::vector < const char* >;

// A parameter is represented as (i.e. should be displayed as) the value divided by the divisor.
// We could have just specified a "precision" instead of a divisor, but the oddball is the swing parameter 
// which gets incremented by 0.2%. So by specifying a divisor, we can universally use increments of 1 for all parameters.
constexpr uint_fast16_t divisor1 = 1;
constexpr uint_fast16_t divisor5 = 5;		///< divisor5 used for the swing parameters
constexpr uint_fast16_t divisor10 = 10;
constexpr uint_fast16_t divisor100 = 100;
constexpr uint_fast16_t divisor1000 = 1000;
constexpr uint_fast16_t divisor10000 = 10000;

struct Range
{
	Range(int minValue_, int maxValue_, uint_fast16_t divisor_ = divisor1)
		:minValue(minValue_), maxValue(maxValue_), divisor(divisor_) {}
	Range() {}
	int				minValue{ 0 };
	int				maxValue{ 0 };
	uint_fast16_t	divisor{ divisor1 };
};

// "psi" stands for "port sub index". These are constants meant to be used with the subindex member of the ParamID  classes.
constexpr int psiSource = 0;
constexpr int psiAmount = 1;	// For oscillators this is LinFM
constexpr int psiDest = 2;
constexpr int psiAmtPitch = 3;
constexpr int psiAmtShape = 4;

struct ParamID
{
	ElementType	elemType;
	uint8_t		elemIndex;
	PortType	portType;
	uint8_t		portIndex;
	uint8_t		subIndex;	// index within port if there is a port, otherwise index within element's parameter list.

	ParamID() {}

	ParamID(PortID portID, int subIndex_ = 0);
	ParamID(ElementType	aElemType, int aElemIndex, PortType aPortType, int aPortIndex, int subIndex_ = 0);
	ParamID(ElementID elemID, PortType aPortType, int aPortIndex, int subIndex_ = 0);
	ParamID(ElementID elemID, int subIndex_);	// convenient for setting parameters

	ElementID GetElementID() 
	{
		return{ elemType, elemIndex };
	}
	
	void Write(std::ostream& output) const;
	void Read(std::istream& input);

};

namespace std 
{
	// Create custom hasher for the ParamID to be used as a key.
	template <>
	struct hash<ParamID>
	{
		std::size_t operator()(const ParamID& paramID) const
		{
			// Take advantage of the fact that we know the element index and port index are both less than 16
			// to fit the whole thing into 32 bits.
			return	(to_int(paramID.elemType) << 24) | 
					(to_int(paramID.portType) << 16) | 
					(paramID.elemIndex << (8 + 4) ) | 
					(paramID.portIndex << 8) | 
					paramID.subIndex;
		}
	};
}
inline bool operator == (const ParamID &lhs,const ParamID &rhs)
{
	return	lhs.elemType == rhs.elemType && 
			lhs.elemIndex == rhs.elemIndex && 
			lhs.portType == rhs.portType && 
			lhs.portIndex == rhs.portIndex && 
			lhs.subIndex == rhs.subIndex;
}
inline bool operator != (const ParamID &lhs, const ParamID &rhs)
{
	return !(lhs == rhs);
}
inline bool operator < (const ParamID &lhs, const ParamID &rhs)
{
	return std::hash<ParamID>()(lhs) < std::hash<ParamID>()(rhs);
}




enum class ValueType { Enum, Num };

struct ParamInfo
{
	ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, ::Range range_);
	ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, EnumListType* enumList_);
	ParamInfo(PortType portType_, int portIndex_, int subIndex_, ValueType valueType_, SourceListType* srcList_);

	ValueType	valueType;
	::Range		range;
	PortType	portType;
	uint8_t		portIndex;
	uint8_t		subIndex;	/// index within the port if this is a port. (i.e. portType != PortType::None)
							/// index within element's parameter list. (i.e. portType == PortType::None)
	union
	{
		const EnumListType*		enumList;
		const SourceListType*	srcList;
	};
};

struct ParamAddress
{
	// Note that a constructor in a struct prohibits using an initializer list to initilize the struct in C++98.
	ParamAddress(uint8_t hi_, uint8_t mid_, uint8_t lo_) : hi(hi_), mid(mid_), lo(lo_) {}
	ParamAddress() : hi(0xFF), mid(0xFF), lo(0xFF) {}
	bool IsValid() { return hi != 0xFF; }
	uint8_t hi;
	uint8_t mid;
	uint8_t lo;
};

struct ElementItem
{
	ElementItem(ElementType elementType_, int numElements_, std::vector<ParamInfo> elementParamList_, std::vector<std::vector<ParamAddress>> paramAddressList_, std::vector<uint8_t> countList_)
		:elementType(elementType_) , numElements(numElements_), elementParamList(elementParamList_), paramAddressList(paramAddressList_), countList(countList_)
	{}
	ElementItem(ElementType elementType_, int numElements_, std::vector<ParamInfo> elementParamList_, std::vector<std::vector<ParamAddress>> paramAddressList_, std::vector<uint8_t> countList_, std::vector<std::vector<int>> initList_)
		:elementType(elementType_) , numElements(numElements_), elementParamList(elementParamList_), paramAddressList(paramAddressList_), countList(countList_), initList(initList_)
	{}
	ElementType elementType;
	int numElements;
	std::vector<ParamInfo> elementParamList;
	std::vector<std::vector<ParamAddress> > paramAddressList;
	std::vector<uint8_t> countList;				///< byte count for ParamAddress
	std::vector<std::vector<int> > initList;	///< default values
};

extern std::vector<ElementItem> masterParamList;


const ParamInfo* GetParamInfo(const ParamID& paramID);
ParamAddress GetSysExAddressFromParamID(const ParamID& paramID);



extern const EnumListType offWaveNames;
extern const EnumListType MM1WaveNames;
extern const EnumListType CEMWaveNames;
extern const EnumListType WAVWaveNames;
extern const EnumListType WTWaveNames;
extern const EnumListType VSWaveNames;
extern const EnumListType MiniWaveNames;

extern const EnumListType BypassModeNames;
extern const EnumListType MM1ModeNames;
extern const EnumListType SSMModeNames;
extern const EnumListType MiniModeNames;
extern const EnumListType ObieModeNames;
extern const EnumListType CombModeNames;
extern const EnumListType VocalModeNames;

// Parameter indices
const int oscType = 0;
const int oscMM1Wave = 1;
const int oscWTWave = 2;
const int oscCEMWave = 3;
const int oscWAVWave = 4;
const int oscVSWave = 5;
const int oscMiniWave = 6;
const int oscShape = 7;
const int oscCoarse = 8;
const int oscFine = 9;
const int oscGlide = 10;
const int oscGlideRate = 11;
const int oscPhase = 12;
const int oscClockSync = 13;
const int oscNoTrack = 14;
const int oscLow = 15;
const int oscCoarseSync = 16;
const int oscCoarseNoTrack = 17;

const int rotCoarse = 0;
const int rotFine = 1;
const int rotXFade = 2;
const int rotPhaseSync = 3;
const int rotPhase = 4;
const int rotClockSync = 5;
const int rotNoTrack = 6;
const int rotLow = 7;
const int rotCoarseSync = 8;
const int rotCoarseNoTrack = 9;

const int amAlgorithm =	0;
const int amOffset =	1;

const int vecXOffset =	0;
const int vecYOffset =	1;

const int mixOutputLevel = 0;

const int filterType =			0;
const int filterMM1Mode =		1;// make sure these five mode parameters start at 1 and are in consecutive order.
const int filterSSMMode =		2;
const int filterMiniMode =		3;
const int filterObieMode =		4;
const int filterCombMode =		5;
const int filterBypassMode =	6;
const int filterVowel1 =		7;	// make sure these five filterVowelx parameters are in consecutive order.
const int filterVowel2 =		8;
const int filterVowel3 =		9;
const int filterVowel4 =		10;
const int filterVowel5 =		11;
const int filterCutoff =		12;
const int filterResonance =		13;
const int filterDamping =		14;
const int filterXFade =			15;
const int filterKeytracking =	16;
const int filterKeycenter =		17;

const int insFxMode =	0;
const int insFxValue =	1;

const int vcaType =		0;
const int vcaBoost =	1;
const int vcaLevel =	2;
const int vcaInitPan =	3;

const int lfoShape =		0;
const int lfoRate =			1;
const int lfoPhase =		2;
const int lfoDelayStart =	3;
const int lfoFadeIn =		4;
const int lfoFadeOut =		5;
const int lfoLevel =		6;
const int lfoClockSync =	7;
const int lfoOffset =		8;
const int lfoRetrigger =	9;
const int lfoRateSync =		10;

const int vlfoShape =		0;
const int vlfoRate =		1;
const int vlfoPhase =		2;
const int vlfoDelayStart =	3;
const int vlfoFadeIn =		4;
const int vlfoFadeOut =		5;
const int vlfoLevel =		6;
const int vlfoClockSync =	7;
const int vlfoOffset =		8;
const int vlfoRetrigger =	9;
const int vlfoRateSync =	10;
const int vlfoModWheel =	11;
const int vlfoModWheelMax =	12;

const int egDelay =		0;
const int egAttack =	1;
const int egDecay =		2;
const int egSustain =	3;
const int egRelease =	4;
const int egASlope =	5;
const int egDSlope =	6;
const int egSSlope =	7;
const int egRSlope =	8;
const int egVelocity =	9;

const int legTime1 = 0;
const int legTime2 = 1;
const int legTime3 = 2;
const int legTime4 = 3;
const int legTime5 = 4;
const int legTime6 = 5;
const int legTime7 = 6;
const int legTime8 = 7;
const int legXLevel1 = 8;
const int legXLevel2 = 9;
const int legXLevel3 = 10;
const int legXLevel4 = 11;
const int legXLevel5 = 12;
const int legXLevel6 = 13;
const int legXLevel7 = 14;
const int legXLevel8 = 15;
const int legYLevel1 = 16;
const int legYLevel2 = 17;
const int legYLevel3 = 18;
const int legYLevel4 = 19;
const int legYLevel5 = 20;
const int legYLevel6 = 21;
const int legYLevel7 = 22;
const int legYLevel8 = 23;

const int legSlope =	24;
const int legLoop =		25;
const int legRepeat =	26;
const int legStart =	27;
const int legKeyOff =	28;
const int legSync =		29;

const int legTime1Sync = 30;
const int legTime2Sync = 31;
const int legTime3Sync = 32;
const int legTime4Sync = 33;
const int legTime5Sync = 34;
const int legTime6Sync = 35;
const int legTime7Sync = 36;
const int legTime8Sync = 37;

const int lagTime = 0;

const int envFolAttack = 0;
const int envFolRelease = 1;
const int envFolInput = 2;
const int envFolOutput = 3;

const int fxChannelSlot1 = 0;
const int fxChannelSlot2 = 1;
const int fxChannelSlot3 = 2;
const int fxChannelSlot4 = 3;

const int ribIntensity = 0;
const int ribOffset = 1;
const int ribHold = 2;
const int ribTouchOffset = 3;

// cfp stands for ChorusFlanger/Phaser since the indices are the same for both effects
const int cfpMode = 0;
const int cfpFrequency = 1;
const int cfpDepth = 2;
const int cfpPhase = 3;
const int cfpOffset = 4;
const int cfpInLevel = 5;
const int cfpFeedback = 6;
const int cfpDry = 7;
const int cfpWet = 8;
const int cfpInput = 9;

const int delMode = 0;
const int delTimeSecondsL = 1;
const int delTimeSecondsR = 2;
const int delTimeBeatsL = 3;
const int delTimeBeatsR = 4;
const int delFeedL = 5;
const int delFeedR = 6;
const int delDamping = 7;
const int delDry = 8;
const int delWet = 9;
const int delMidiClk = 10;
const int delInput = 11;

const int eqMode = 0;
const int eqFreq1 = 1;
const int eqQ1 = 2;
const int eqGain1 = 3;
const int eqFreq2 = 4;
const int eqQ2 = 5;
const int eqGain2 = 6;
const int eqFreq3 = 7;
const int eqQ3 = 8;
const int eqGain3 = 9;
const int eqInput = 10;

// ElementType::Seq
const int seqPatternLength = 0;
const int seqStep1 = 1;
const int seqStep2 = 2;
const int seqStep3 = 3;
const int seqStep4 = 4;
const int seqStep5 = 5;
const int seqStep6 = 6;
const int seqStep7 = 7;
const int seqStep8 = 8;
const int seqStep9 = 9;
const int seqStep10 = 10;
const int seqStep11 = 11;
const int seqStep12 = 12;
const int seqStep13 = 13;
const int seqStep14 = 14;
const int seqStep15 = 15;
const int seqStep16 = 16;

// ElementType::SeqGlobal
const int seqMode = 0;
const int seqDivision = 1;
const int seqPattern = 2;
const int seqSwing = 3;

const int arpStepVolumeOffset = 0;
const int arpStepGateLengthOffset = 32;
const int arpStepGateOffset = 64;
const int arpMode = 96;
const int arpOctave = 97;
const int arpPattern = 98;
const int arpResolution = 99;
const int arpNoteLength = 100;
const int arpVelocity = 101;
const int arpHold = 102;
const int arpPatternLength = 103;
const int arpSwing = 104;


const int gloName1 = 0;
const int gloName2 = 1;
const int gloName3 = 2;
const int gloName4 = 3;
const int gloName5 = 4;
const int gloName6 = 5;
const int gloName7 = 6;
const int gloName8 = 7;
const int gloName9 = 8;
const int gloName10 = 9;
const int gloName11 = 10;
const int gloName12 = 11;
const int gloName13 = 12;
const int gloName14 = 13;
const int gloName15 = 14;
const int gloName16 = 15;
const int gloName17 = 16;
const int gloName18 = 17;
const int gloName19 = 18;
const int gloName20 = 19;

const int gloPitchWheelUpRange = 20;
const int gloPitchWheelDownRange = 21;
const int gloBPM = 22;
const int gloGldType = 23;
const int gloGldMode = 24;
const int gloGldRange = 25;
const int gloGldTime = 26;
const int gloGldRate = 27;
const int gloPlayMode = 28;
const int gloLegato = 29;
const int gloUniVoice = 30;
const int gloNotePriority = 31;
const int gloEgReset = 32;
const int gloUniTune = 33;
const int gloOutput12 = 34;
const int gloOutput34 = 35;
const int gloOutput56 = 36;
const int gloOutput78 = 37;
const int gloOutputSPDIF = 38;
const int gloUnisonButton = 39;
const int gloRndTune = 40;
const int gloSeqButton = 41;
const int gloArpButton = 42;
const int gloAssignState1 = 43;
const int gloAssignState2 = 44;
const int gloAssignFunc1 = 45;
const int gloAssignFunc2 = 46;
const int gloAssignMode1 = 47;
const int gloAssignMode2 = 48;
const int gloExpPedal = 49;
const int gloSusPedal1 = 50;
const int gloSusPedal2 = 51;

const int gloVTIntens = 52;
const int gloVTOffset = 53;
const int gloATIntens = 54;
const int gloATOffset = 55;

const int gloCategory1 = 56;
const int gloCategory2 = 57;
const int gloPerfKnob1Amt = 58;
const int gloPerfKnob2Amt = 59;
const int gloPerfKnob3Amt = 60;
const int gloPerfKnob4Amt = 61;
const int gloPerfKnob5Amt = 62;
const int gloPerfKnob1Src = 63;
const int gloPerfKnob2Src = 64;
const int gloPerfKnob3Src = 65;
const int gloPerfKnob4Src = 66;
const int gloPerfKnob5Src = 67;
const int gloUnisonStackNumberOfNotes = 68;
const int gloUnisonStackNote0 = 69;
const int gloUnisonStackNote1 = 70;
const int gloUnisonStackNote2 = 71;
const int gloUnisonStackNote3 = 72;
const int gloUnisonStackNote4 = 73;
const int gloUnisonStackNote5 = 74;
const int gloUnisonStackNote6 = 75;
const int gloUnisonStackNote7 = 76;
const int gloUnisonStackNote8 = 77;
const int gloUnisonStackNote9 = 78;
const int gloSamplePool = 79;
const int gloPatchChain = 80;


const int sysTranspose = 0;
const int sysFineTune = 1;
const int sysBPMOverride = 2;
const int sysOutputsOverride = 3;
const int sysExpPedalPolarity = 4;
const int sysSusPedal1Polarity = 5;
const int sysSusPedal2Polarity = 6;
const int sysExpPedalTargetOverride = 7;
const int sysSusPedalTargetOverride = 8;
const int sysRndTune = 9;
const int sysVTIntens = 10;
const int sysVTOffset = 11;
const int sysATIntens = 12;
const int sysATOffset = 13;
const int sysLoadSample = 14;
const int sysMidiChannel = 15;
const int sysProgramChangeAllowed = 16;
const int sysSendArp = 17;
const int sysOmni = 18;
const int sysLocal = 19;
const int sysTransmitSysEx = 20;
const int sysReceiveSysEx = 21;
const int sysMidiClockSource = 22;
const int sysMidiVolumeAllowed = 23;
const int sysMidiRealTime = 24;
const int sysPolychain = 25;
const int sysDeviceID = 26;
const int sysMidiCC1 = 27;
const int sysMidiCC2 = 28;
const int sysMidiCC3 = 29;
const int sysMidiCC4 = 30;
const int sysMidiCC5 = 31;
const int sysRemapAT = 32;



extern const SourceListType insertFx0SourceList;
extern const SourceListType insertFx1SourceList;
extern const SourceListType insertFx2SourceList;
extern const SourceListType insertFx3SourceList;

extern const SourceListType vca0SourceList;
extern const SourceListType vca1SourceList;
extern const SourceListType vca2SourceList;
extern const SourceListType vca3SourceList;

extern const SourceListType vcaMasterSourceList0;
extern const SourceListType vcaMasterSourceList1;
extern const SourceListType vcaMasterSourceList2;
extern const SourceListType vcaMasterSourceList3;

extern const EnumListType combModDestList;
extern const EnumListType vocalModDestList;
extern const EnumListType fltModDestList;
extern const EnumListType egAttackDestList;
extern const EnumListType egDecayDestList;
extern const EnumListType egSustainDestList;
extern const EnumListType egReleaseDestList;
extern const EnumListType vecModDestList0;
extern const EnumListType vecModDestList1;
extern const EnumListType outputDestList;
extern const EnumListType levelDestList;
extern const EnumListType insertFXModDestList;
extern const EnumListType levelDestList;
extern const EnumListType panDestList;
extern const EnumListType amModDestList;
extern const EnumListType loopEgModDest0;
extern const EnumListType loopEgModDest1;


#endif  // PARAMETERLIST_H_INCLUDED
