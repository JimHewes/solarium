/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef EQPANEL_H_INCLUDED
#define EQPANEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Preset.h"
#include "TimeSlider.h"
#include "Slider2.h"
#include "Command.h"


class QSlider : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
};

class EQPanel :
	public Component,
	public Slider2::Listener,
	public Button::Listener
{
public:
	EQPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID,int slot);
	virtual ~EQPanel();
	virtual void sliderValueChanged(Slider2* slider) override;
	virtual void buttonClicked(Button* button) override;
	void paint(Graphics& g) override;

private:
	void Update();

	Preset*			m_pPreset;
	CommandHistory* m_pCommandHistory;
	ElementID		m_elemID;

	Label			m_subtitle;

	ToggleButton	m_modeButton;
	Label			m_freq1Label;
	SliderFreq20KHz	m_freq1Slider;
	Label			m_q1Label;
	QSlider			m_q1Slider;
	Label			m_gain1Label;
	SliderProp		m_gain1Slider;

	Label			m_freq2Label;
	SliderFreq20KHz	m_freq2Slider;
	Label			m_q2Label;
	QSlider			m_q2Slider;
	Label			m_gain2Label;
	SliderProp		m_gain2Slider;

	Label			m_freq3Label;
	SliderFreq20KHz	m_freq3Slider;
	Label			m_q3Label;
	QSlider			m_q3Slider;
	Label			m_gain3Label;
	SliderProp		m_gain3Slider;

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};



#endif  // EQPANEL_H_INCLUDED
