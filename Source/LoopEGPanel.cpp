/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "LoopEGPanel.h"
#include <vector>
#include <string>
#include <cassert>
#include "Command.h"

using std::vector;
using std::string;
static const juce::Identifier paramIndexID = "paramIndex";


LoopEGPanel::LoopEGPanel(Preset* pPreset, CommandHistory* pCommandHistory,ElementID elemID)
	:m_pPreset(pPreset)
	,m_pCommandHistory(pCommandHistory)
	,m_elemID(elemID)
	,m_timeLabels(8)
	,m_timeSliders(8)
	,m_timeSlidersSync(8)
	,m_xLevelLabels(8)
	,m_xLevelSliders(8)
	,m_yLevelLabels(8)
	,m_yLevelSliders(8)
{
	const int leftX = 10;
	const int leftWidth = 70;
	const int rightX = 75;
	const int rightWidth = 105;
	const int ctrlHeight = 20;
	const int space = 6;
	const int rowHeight = ctrlHeight + space;
	int yPos = 10 - rowHeight;


	for (int i = 0; i < 8; ++i)
	{
		yPos += rowHeight;
		static char timeText[] = "Time n:";
		timeText[5] = char(0x31 + i);
		m_timeLabels[i].setBounds(leftX,yPos,leftWidth,ctrlHeight);
		m_timeLabels[i].setText(&timeText[0], NotificationType::dontSendNotification);
		addAndMakeVisible(m_timeLabels[i]);

		m_timeSliders[i].setBounds(rightX,yPos,rightWidth,ctrlHeight);
		m_timeSliders[i].setSliderStyle(Slider2::LinearBar);
		auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,legTime1 + i));
		m_timeSliders[i].setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_timeSliders[i].getProperties().set(paramIndexID, legTime1 + i);
		m_timeSliders[i].addListener(this);
		addAndMakeVisible(m_timeSliders[i]);

		m_timeSlidersSync[i].setBounds(rightX,yPos,rightWidth,ctrlHeight);
		std::vector<std::string> timeSyncList = m_pPreset->GetParameterListText(ParamID(elemID, legTime1Sync));;
		for (size_t j = 0; j < timeSyncList.size(); ++j)
		{
			m_timeSlidersSync[i].addItem(timeSyncList[j], j + 1);
		}
		m_timeSlidersSync[i].getProperties().set(paramIndexID, legTime1Sync + i);
		m_timeSlidersSync[i].addListener(this);
		addAndMakeVisible(m_timeSlidersSync[i]);
	}
	yPos += 2 * space;

	for (int i = 0; i < 8; ++i)
	{
		yPos += rowHeight;
		static char timeText[] = "Level nx:";
		timeText[6] = char(0x31 + i);
		m_xLevelLabels[i].setBounds(leftX,yPos,leftWidth,ctrlHeight);
		m_xLevelLabels[i].setText(&timeText[0],NotificationType::dontSendNotification);
		addAndMakeVisible(m_xLevelLabels[i]);

		m_xLevelSliders[i].setBounds(rightX,yPos,rightWidth,ctrlHeight);
		m_xLevelSliders[i].setSliderStyle(Slider2::LinearBar);
		auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,legXLevel1 + i));
		m_xLevelSliders[i].setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_xLevelSliders[i].getProperties().set(paramIndexID, legXLevel1 + i);
		m_xLevelSliders[i].addListener(this);
		addAndMakeVisible(m_xLevelSliders[i]);
	}
	yPos += 2* space;

	for (int i = 0; i < 8; ++i)
	{
		yPos += rowHeight;
		static char timeText[] = "Level ny:";
		timeText[6] = char(0x31 + i);
		m_yLevelLabels[i].setBounds(leftX,yPos,leftWidth,ctrlHeight);
		m_yLevelLabels[i].setText(&timeText[0],NotificationType::dontSendNotification);
		addAndMakeVisible(m_yLevelLabels[i]);

		m_yLevelSliders[i].setBounds(rightX,yPos,rightWidth,ctrlHeight);
		m_yLevelSliders[i].setSliderStyle(Slider2::LinearBar);
		auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,legYLevel1 + i));
		m_yLevelSliders[i].setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
		m_yLevelSliders[i].getProperties().set(paramIndexID, legYLevel1 + i);
		m_yLevelSliders[i].addListener(this);
		addAndMakeVisible(m_yLevelSliders[i]);
	}

	yPos += rowHeight + space;
	m_startLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_startLabel.setText("Start:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_startLabel);
	m_startCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> startList = m_pPreset->GetParameterListText(ParamID(m_elemID,legStart));;
	for (size_t i = 0; i < startList.size(); ++i)
	{
		m_startCombo.addItem(startList[i],i + 1);
	}
	m_startCombo.addListener(this);
	addAndMakeVisible(m_startCombo);

	yPos += rowHeight;
	m_keyOffLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_keyOffLabel.setText("Keyoff:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_keyOffLabel);
	m_KeyOffCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> keyoffList = m_pPreset->GetParameterListText(ParamID(m_elemID,legKeyOff));;
	for (size_t i = 0; i < keyoffList.size(); ++i)
	{
		m_KeyOffCombo.addItem(keyoffList[i],i + 1);
	}
	m_KeyOffCombo.addListener(this);
	addAndMakeVisible(m_KeyOffCombo);

	yPos += rowHeight;
	m_slopeLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_slopeLabel.setText("Slope:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_slopeLabel);
	m_slopeSlider.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	m_slopeSlider.setSliderStyle(Slider2::LinearBar);
	auto rangeInfo = m_pPreset->GetParameterRangeInfo(ParamID(elemID,legSlope));
	m_slopeSlider.setRange(rangeInfo.minValue, rangeInfo.maxValue, 1);
	m_slopeSlider.getProperties().set(paramIndexID, legSlope);
	m_slopeSlider.addListener(this);
	addAndMakeVisible(m_slopeSlider);

	yPos += rowHeight;
	m_repeatLabel.setBounds(leftX,yPos,leftWidth,ctrlHeight);
	m_repeatLabel.setText("Repeat:",NotificationType::dontSendNotification);
	addAndMakeVisible(m_repeatLabel);
	m_repeatCombo.setBounds(rightX,yPos,rightWidth,ctrlHeight);
	vector<string> repeatList = m_pPreset->GetParameterListText(ParamID(m_elemID,legRepeat));;
	for (size_t i = 0; i < repeatList.size(); ++i)
	{
		m_repeatCombo.addItem(repeatList[i], i + 1);
	}
	m_repeatCombo.addListener(this);
	addAndMakeVisible(m_repeatCombo);

	yPos += rowHeight;
	m_loopButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_loopButton.setButtonText("Loop");
	m_loopButton.addListener(this);
	addAndMakeVisible(m_loopButton);


	yPos += rowHeight;
	m_clockSyncButton.setBounds(leftX,yPos,rightWidth,ctrlHeight);
	m_clockSyncButton.setButtonText("Sync");
	m_clockSyncButton.addListener(this);
	addAndMakeVisible(m_clockSyncButton);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);

	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});
	Update();
}

LoopEGPanel::~LoopEGPanel()
{

	for (int i = 0; i < 8; ++i)
	{
		m_timeSliders[i].removeListener(this);
		m_timeSlidersSync[i].removeListener(this);
		m_xLevelSliders[i].removeListener(this);
		m_yLevelSliders[i].removeListener(this);
	}

	m_startCombo.removeListener(this);
	m_KeyOffCombo.removeListener(this);
	m_slopeSlider.removeListener(this);
	m_clockSyncButton.removeListener(this);
	m_repeatCombo.removeListener(this);
	m_loopButton.removeListener(this);
}

void LoopEGPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}

void LoopEGPanel::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_startCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, legStart), m_startCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_KeyOffCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, legKeyOff), m_KeyOffCombo.getSelectedId() - 1));
	}
	else if (comboBoxThatHasChanged == &m_repeatCombo)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, legRepeat), m_repeatCombo.getSelectedId() - 1));
	}
	else
	{
		// Extract the parameter index from the combobox control.
		var* pVar = comboBoxThatHasChanged->getProperties().getVarPointer(paramIndexID);
		if (pVar)
		{
			ParamID paramID(m_elemID, *pVar);
			int value = comboBoxThatHasChanged->getSelectedId() - 1;
			m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
		}
	}
}

void LoopEGPanel::sliderValueChanged(Slider2* slider)
{
	assert(slider);

	// Extract the parameter index from the slider control.
	var* pVar = slider->getProperties().getVarPointer(paramIndexID);
	if (pVar)
	{
		ParamID paramID(m_elemID, *pVar);
		int value = lrint(slider->getValue());
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(paramID, value));
	}
}

// This function gets called after the button has been clicked and the state is changed.
void LoopEGPanel::buttonClicked(Button* button)
{
	if (button == &m_clockSyncButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, legSync), m_clockSyncButton.getToggleState() ? 1 : 0));
	}
	else if (button == &m_loopButton)
	{
		m_pCommandHistory->AddCommandAndExecute(std::make_unique<CmdSetParameterValue>(ParamID(m_elemID, legLoop), m_loopButton.getToggleState() ? 1 : 0));
	}
}

void LoopEGPanel::Update()
{
	if (m_pPreset == nullptr)
	{
		assert(false);
		return;

	}

	for (int i = 0; i < 8; ++i)
	{
		m_timeSliders[i].setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,legTime1 + i)), NotificationType::dontSendNotification);
		m_timeSlidersSync[i].setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,legTime1Sync + i)) + 1, NotificationType::dontSendNotification);
		m_xLevelSliders[i].setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,legXLevel1 + i)), NotificationType::dontSendNotification);
		m_yLevelSliders[i].setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,legYLevel1 + i)), NotificationType::dontSendNotification);
	}

	bool clockSyncOn = m_pPreset->GetParameterValue(ParamID(m_elemID, legSync)) != 0;

	m_startCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,legStart)) + 1, NotificationType::dontSendNotification);
	m_KeyOffCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,legKeyOff)) + 1, NotificationType::dontSendNotification);
	m_repeatCombo.setSelectedId(m_pPreset->GetParameterValue(ParamID(m_elemID,legRepeat)) + 1, NotificationType::dontSendNotification);
	m_slopeSlider.setValue(m_pPreset->GetParameterValue(ParamID(m_elemID,legSlope)), NotificationType::dontSendNotification);
	m_clockSyncButton.setToggleState(clockSyncOn != 0,NotificationType::dontSendNotification);
	m_loopButton.setToggleState(m_pPreset->GetParameterValue(ParamID(m_elemID,legLoop)) != 0,NotificationType::dontSendNotification);

	for (int i = 0; i < 8; ++i)
	{
		m_timeSliders[i].setVisible(!clockSyncOn);
		m_timeSlidersSync[i].setVisible(clockSyncOn);
	}

}
