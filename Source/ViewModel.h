/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef VIEWMODEL_H_INCLUDED
#define VIEWMODEL_H_INCLUDED

#include <list>
#include <iostream>
#include <memory>
#include <unordered_map>
#include "Preset.h"
#include "Notifier.h"
#include "Command.h"
#include "SelectionModel.h"
#include "HistoryListClient.h"

struct InvItemView
{
	std::string elemTypeFriendlyname;
	ElementType	elemType;
	uint8_t	count;				///< The number of instances of the element that exist.
	uint8_t	numOutputPorts;		///< The number of output ports for this node type. Either 0 or 1.
};


using MenuItem = struct
{
	std::string groupFriendlyname;
	std::string itemFriendlyname;
	ElementID elemID;
	int user;
};


/** An ElemItem is a model used by the UI to represent an Element. An element is some "module" or 
	modulation source in the Solaris. Examples of ElemItems: Oscillator 1, Oscillator 3, Filter 2, 
	Lag Processor 1, aftertouch, External Input 4.
	A list of ElemItems is built at startup by looking at the inventory list.
	The purpose of ElemItems and the view model is to have a representation of the onscreen state that 
	can be saved and reloaded, but it is information irrelevant to the main model (Preset).
*/
struct ElemItem
{
	ElementID		elemID;
	Location		location;

	/**	An enabled element is an element such that at least one of the VCAs it reaches is enabled.
		Another way to say it is that it is part of the "tree" of elements that make up the 
		currently-enabled parts.*/
	bool	enabled{ false };

	/**	An element that is added was manually added by the usser rather than added as a result of being 
		included in a preset that was loaded of retrieved from MIDI. Added elements are not made invisible 
		just because they are not in the tree of enabled elements. The idea is that if a user manually 
		adds an element they want it to stay visible until they delete it. */
	bool	added{ false };

	/**	An ephemeral element is an element that is a child of an added element. Although it is not included 
		in a part, it is still visible. But it becomes invisible when parameters are changed such that the element 
		is no longer a child of any added or enabled element (orphaned). This way, elements are 
		"cleaned up" from the display when the user changes parameters on the Solaris and sync is enabled. */
	bool	ephemeral{ false };

	/** An element that is visible is shown in the UI. This generally includes any enabled, added, or ephemeral elements. */
	bool	visible{ false };

	std::string		groupFriendlyname;
	std::string		itemFriendlyname;
};


/** @brief	ElementHistoryItem contains the data that is stored in the undo history for ElementView objects.

	What is stored are only those things that are NOT normally determined by reading the data from the Preset.
	these are things independent of the Preset data. So this includes Element location and added and ephemeral status.
	It doesn't include enabled status which is obtained from the Preset model.
	Why? If the UI is to store the state of things, when what about all the sliders in the panels? It doesn't make sense 
	for them to store anything when they are just representing the Preset parameters. So along the same lines, anything 
	in the UI whose state comes from the Preset should not have its history saved. But things like added or ephemeral state 
	have nothing to do with the Preset data and are totaly within the ViewModel.
*/
struct ElementHistoryItem
{
	const uint8_t ADDED = 1;
	const uint8_t EPHEMERAL = 2;

	ElementHistoryItem(bool added, bool ephemeral, Location location_) 
		: state((added ? ADDED : uint8_t{ 0 } ) | (ephemeral ? EPHEMERAL : uint8_t{ 0 })), location(location_) {}
	ElementHistoryItem(bool added, bool ephemeral) 
		: state((added ? ADDED : uint8_t{ 0 }) | (ephemeral ? EPHEMERAL : uint8_t{ 0 }))
	{
	}
	ElementHistoryItem(const ElementHistoryItem& other) = default;
	ElementHistoryItem() = default;
	ElementHistoryItem& operator=(const ElementHistoryItem& other)
	{
		location = other.location;
		state = other.state;
		return *this;
	};

	Location location;	///< The location of the element onscreen
	uint8_t state;		///< A bitise representation of the bools added and ephemeral so that they take up less space in history.
	bool GetAdded() { return 0 != (state & ADDED);	}
	bool GetEphemeral() { return 0 != (state & EPHEMERAL);	}
};
inline bool operator==(const ElementHistoryItem& lhs, const ElementHistoryItem& rhs);
inline bool operator!=(const ElementHistoryItem& lhs, const ElementHistoryItem& rhs);

using ElementHistory = HistoryListSet<ElementID, ElementHistoryItem>;

class ViewModel
{
public:
	ViewModel(Preset* pPreset, CommandHistory* pCommandHistory);
	void Initialize();
	void AddElement(ElementID elemID, Location locationHint, Version version);
	void RemoveElement(ElementID elementToRemove, Version version);
	bool CanBeRemoved(Connector connector);
	bool CanBeRemoved(const ElementID& elemID);

	std::list<MenuItem> GenerateMenuItemList();
	bool IsElementVisible(ElementID elemID);
	std::vector<ElementID> GetVisibleElementList() const;
	void ClearVisibleElements();
	void ClearAddedAndEphemeralElements(Version version);

	void UpdateEphemeral(Version version);
	void UpdateEnabled();
	void UpdateVisible();

	std::string GetElementFriendlyName(const ElementID& elemID);
	ElemItem* GetElement(ElementID elemID);
	void SetFocusedSlot(int channelIndex, int slotIndex);
	int GetFocusedSlot(int channelIndex);
	void SetLocation(ElementID elemID, Location location);
	Location GetLocation(ElementID elemID);

	void Write(std::ostream& output);
	void Read(std::istream& input);

	SelectionModel<ElementID>& ElementViewSelectionModel() { return m_elementViewSelectionModel; }
	SelectionModel<Connector>& ConnectorSelectionModel() { return m_connectorSelectionModel; }

	Notifier<void(void)> VisibleElementsChangedEvent;
	using VisibleElementsChangedConnection = decltype(VisibleElementsChangedEvent)::ConnectionType;	///< VisibleElementsChangedEvent.Connect() returns this type

	Notifier<void(int channelIndex, int slotIndex)> FxFocusedSlotChangedEvent;
	using FxFocusedSlotChangedConnection = decltype(FxFocusedSlotChangedEvent)::ConnectionType;	///< FxFocusedSlotChangedEvent.Connect() returns this type

private:
	Preset*				m_pPreset{ nullptr };
	std::list<ElemItem>	m_elemItemList;
	ElementHistory		m_elementHistory;
	Preset::EnabledChangedConnection m_enabledchangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;
	Preset::ParameterChangedConnection m_parameterChangedConnection;
	CommandHistory::BranchAddedConnection m_branchAddedConnection;
	CommandHistory::BranchDeletedConnection m_branchDeletedConnection;
	CommandHistory::GoToVersionConnection m_goToVersionConnection;
	CommandHistory::RemoveVersionConnection m_removeVersionConnection;

	SelectionModel<ElementID>	m_elementViewSelectionModel;
	SelectionModel<Connector>	m_connectorSelectionModel;

	std::vector<int> m_focusedSlots;	///< For each FX channel, holds the index of the slot that is focused (or was last focused)
};

using ViewModelUPtr = std::unique_ptr < ViewModel >;

#endif  // VIEWMODEL_H_INCLUDED
