/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef TIMESLIDER_H_INCLUDED
#define TIMESLIDER_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include "Slider2.h"

class SliderTime : public SliderProp
{
public:
	SliderTime();
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;
	void mouseWheelMove(const MouseEvent&, const MouseWheelDetails&) override;
private:
	juce::Time lastMouseWheelTime;
	double getMouseWheelDelta(double value, double wheelAmount);
};

class SliderFreq20KHz : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;

};

class SliderFreq500Hz : public SliderProp
{
public:
	String getTextFromValue(double value) override;
	double getValueFromText(const String & text) override;

};


#endif  // TIMESLIDER_H_INCLUDED
