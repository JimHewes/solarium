/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Surface.h"
#include "ViewModel.h"
#include "SettingsPanel.h"
#include "ArpEdit.h"
#include "EditHistory.h"
#include "Command.h"

class MidiManager;
class SettingsPanel;
class ControlPanel;

/** @brief LookAndFeelSol is a class that derives from LookAndFeel_V2 in order to change 
	certain colors and to draw ToggleButtons (radio buttons) in different color when not checked.
*/
class LookAndFeelSol : public LookAndFeel_V3
{
public:
	LookAndFeelSol();
	void drawToggleButton(Graphics& g, ToggleButton& button, bool isMouseOverButton, bool isButtonDown) override;
};

/** @brief The MainContentComponent occupies the entire main window and is where all the content is placed.
*/
class MainContentComponent :	public Component,
								public ApplicationCommandTarget
{
public:
    //==============================================================================
	MainContentComponent();
    ~MainContentComponent();

    void resized() override;

	// The following methods support the ApplicationCommandTarget interface
	ApplicationCommandTarget* getNextCommandTarget() override;
	void getAllCommands(Array<CommandID>& commands) override;
	void getCommandInfo(CommandID commandID, ApplicationCommandInfo& result) override;
	bool perform(const InvocationInfo& info) override;
	
	void OnZoomChanged(double zoom);
	bool IsModified();
private:
	void DoHelpAbout();
	void DoNew();
	void DoOpen();
	void DoSave();
	void DoSaveAs();
	void OpenArpEditWindow();
	void OpenEditHistoryWindow();
	void DoPreferences();
	void DoSetBookmark(int bookmarkNumber);
	void DoMidiSetup();
	void DoToggleMidiSync();
	void DoToggleMuteSelected();
	void DoUnMuteAll();
	void DoMidiReceive();
	void DoMidiSend();
	void DoEditName();

#if (_DEBUG)
	void DoDumpPreset();
	void DoSaveHistory();
	void DoLoadHistory();
#endif
	double m_progress;			///< Used for the MIDI send and receive progress. Range 0 to 1.0
	bool m_continueMidiTransfer;
	DialogWindow* m_pProgressDialogWindow{};


	std::unique_ptr<MenuBarComponent>		m_pMainMenuComponent;
	std::unique_ptr<MenuBarModel>			m_pMainMenuModel;
	SettingsPanel::ZoomChangedConnection	m_zoomChangedConnection;
	Preset::MultipleParametersChangedConnection m_multipleParametersChangedConnection;

	std::unique_ptr<Preset>			m_pPreset;
	std::unique_ptr<CommandHistory>	m_pCommandHistory;
	ViewModelUPtr					m_pViewModel;
	SurfaceUPtr						m_pSurface;
	Component						m_pSurfaceContainer;
	SurfaceViewport					m_surfaceViewport;
	TabbedComponent					m_tabbedComponent;
	Viewport						m_globalViewport;
	Viewport						m_systemViewport;
	std::unique_ptr<HomePanel>	m_pGlobalPanel;
	std::unique_ptr<SystemPanel>	m_pSystemPanel;

	std::unique_ptr<SettingsPanel>		m_pSettingsPanel;
	std::unique_ptr<ControlPanel>		m_pControlPanel;
	std::unique_ptr<ArpEditWindow>		m_pArpEditWindow;
	std::unique_ptr<EditHistoryWindow>	m_pEditHistoryWindow;

	LookAndFeelSol					lookAndFeel;
	
	PropertySet						m_defaultProperties;
	PropertiesFile::Options			m_propertiesOptions;
	std::string						m_currentFilePath;
	std::unique_ptr<MidiManager>	m_midiManager;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// Not currently used
inline bool MainContentComponent::IsModified()
{
	return m_pPreset->IsModified();
}

#endif  // MAINCOMPONENT_H_INCLUDED
