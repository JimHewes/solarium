/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef CONNECTORVIEW_H_INCLUDED
#define CONNECTORVIEW_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include "PortView.h"
#include "ViewModel.h"
#include "Preset.h"


class ConnectionMng
{
public:
	// If the ending portview is NULL, it means there is no connection to the end yet. The user is dragging to create it.
	// In this case, the starting portView will call OnMoveDragConnector with a NULL pointer.
	// The Connection understands that to mean the end location.
	virtual void OnCreateDragConnector(PortView* pStartingPortView, PortView* pEndingPortView) = 0;
	virtual void OnMoveDragConnector(PortView* pPortView, Point<int> newPosition) = 0;
	virtual void FinalizeDrop(PortView* pPortView, Point<int> newPosition) = 0;
};


class ConnectorView :	public Component,
						public ChangeListener

{
public:
	ConnectorView(Connector connector, PortView* pStartingPortView, PortView* pEndingPortView, ViewModel* pViewModel, Preset* pPreset );
	virtual ~ConnectorView();

	void paint(Graphics& g) override;
	bool hitTest(int x, int y) override;
	void mouseDown(const MouseEvent& event) override;
	void changeListenerCallback(ChangeBroadcaster* source) override;

	void OnPortViewMoved(PortView* pPortViewThatMoved, Point<int> newPosition);
	virtual void OnPortViewDestoyed(PortView* pPortViewDestroyed) noexcept;
	void SetEndingPortView(PortView* pEndingPortView);
	PortView* GetStartingPortView()	const { return m_pStartingPortView; }

	Connector GetModel() const { return m_connector; }

	bool IsReadOnly() const { return m_readOnly; }

private:
	void UpdateBounds();
	bool ConnectedElementIsSelected();
	void OnParameterChanged(const ParamID& paramID, int value, bool isMuted);

	Connector	m_connector;
	ViewModel*	m_pViewModel{};		///< A pointer to the object where this ConnectorView reports selection.
	Preset*		m_pPreset{};
	Path		path;
	PortView*	m_pStartingPortView;
	PortView*	m_pEndingPortView;
	Point<int>	m_startingPositionSurfaceCoords;
	Point<int>	m_endingPositionSurfaceCoords;
	bool		m_readOnly;
	uint8_t		m_wireOpacity;
	uint8_t		m_wireThreshold;
	bool		m_isMuted{ false };

	Preset::ParameterChangedConnection		m_paramChangedConnection;
};
using ConnectorViewUPtr = std::unique_ptr < ConnectorView >;

#endif  // CONNECTORVIEW_H_INCLUDED
