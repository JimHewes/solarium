/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include "FXChannelPanel.h"
#include "ChoFlaPhaPanel.h"
#include "DelayPanel.h"
#include "EQPanel.h"
#include "Command.h"


FXChannelPanel::FXChannelPanel(Preset* pPreset, CommandHistory* pCommandHistory, ElementID elemID, ViewModel* pViewModel)
		:m_pPreset(pPreset)
		,m_elemID(elemID)
		,m_pCommandHistory(pCommandHistory)
{
	assert(m_pPreset);
	assert(pViewModel);
	assert(m_elemID.elemType == ElementType::FXChannel);
	
	m_focusedSlotIndex = pViewModel->GetFocusedSlot(m_elemID.elemIndex);

	// By listening to the ElementView the OnElementViewChanged() function will
	// get called when the keyboard focus changes slots.
	// This can also get called just when the Solarium window regains focus, but that's OK.
	m_focusedSlotChangedConnection = pViewModel->FxFocusedSlotChangedEvent.Connect([this](int channelIndex, int slotIndex)
	{
		if (channelIndex == m_elemID.elemIndex)
		{
			OnFocusedSlotChanged(slotIndex);
		}
	});

	// Listen to the slot parameter of this FXChannel so that if the user changes the effect in that 
	// slot, ParameterChangedEvent will occur and we'll show the panel for that effect.
	m_paramChangedConnection = m_pPreset->SubscribeToParameterChangedEvent(
		[this](const ParamID& paramID, int, bool)
	{
		if (paramID.elemType == m_elemID.elemType && paramID.elemIndex == m_elemID.elemIndex)
		{
			Update();
		}
	});

	Update();
}

FXChannelPanel::~FXChannelPanel()
{
	if (m_pPanel)
	{
		removeChildComponent(m_pPanel.get());
	}
}

void FXChannelPanel::OnFocusedSlotChanged(int slotIndex)
{
	m_focusedSlotIndex = slotIndex;
	Update();
}

void FXChannelPanel::Update()
{
	// Find out from the preset model which FX type is selected for that slot.
	int value = m_pPreset->GetParameterValue(ParamID(m_elemID.elemType, m_elemID.elemIndex, PortType::None, 0, m_focusedSlotIndex));

	if (m_pPanel)
	{
		removeChildComponent(m_pPanel.get());
	}

	switch (value)
	{
	case 0:		// Off
		m_pPanel = std::make_unique<EffectOffPanel>(ElementID(ElementType::Off,0), m_focusedSlotIndex);
		DBG("Adding Effects OFF panel.");
		break;
	case 1:		// Chorus/Flanger
		m_pPanel = std::make_unique<ChoFlaPhaPanel>(m_pPreset, m_pCommandHistory,ElementID(ElementType::ChorusFlanger,0), m_focusedSlotIndex);
		DBG("Adding ChoFlanger panel.");
		break;
	case 2:		// Phaser
		m_pPanel = std::make_unique<ChoFlaPhaPanel>(m_pPreset, m_pCommandHistory,ElementID(ElementType::Phaser,0), m_focusedSlotIndex);
		DBG("Adding Phaser panel.");
		break;
	case 3:		// Delay
		m_pPanel = std::make_unique<DelayPanel>(m_pPreset, m_pCommandHistory,ElementID(ElementType::Delay,0), m_focusedSlotIndex);
		DBG("Adding Delay panel.");
		break;
	case 4:		// EQ
		m_pPanel = std::make_unique<EQPanel>(m_pPreset, m_pCommandHistory,ElementID(ElementType::EQ,0), m_focusedSlotIndex);
		DBG("Adding EQ panel.");
		break;
	default:
		m_pPanel.reset();
	};

	if (m_pPanel)
	{
		auto lb = getLocalBounds();
		lb.setHeight(m_pPanel->getHeight());
		m_pPanel->setBounds(lb);
		setBounds(lb);	// Also set this FXChannelPanel to the same size as the child panel so that the containing viewport works right.
		addAndMakeVisible(m_pPanel.get());
	}

}
void FXChannelPanel::resized()
{
	if (m_pPanel)
	{
		auto lb = getLocalBounds();
		lb.setHeight(m_pPanel->getHeight());
		m_pPanel->setBounds(lb);
		setBounds(lb);	// Also set this FXChannelPanel to the same size as the child panel so that the containing viewport works right.
	}
}

EffectOffPanel::EffectOffPanel(ElementID elemID,int slot)
	:m_elemID(elemID)
{
	assert(slot >= 0 && slot < 4);

	const int leftX = 10;
	const int rightX = 90;
	const int rightWidth = 90;
	const int ctrlHeight = 20;
	const int space = 10;
	int yPos = space;

	m_subtitle.setFont(Font(17.0f));
	m_subtitle.setBounds(leftX,yPos,rightX + rightWidth,ctrlHeight);
	static char text[] = "Slot n: Off";
	text[5] = char(0x31 + slot);
	m_subtitle.setText(&text[0] ,NotificationType::dontSendNotification);
	addAndMakeVisible(m_subtitle);

	yPos += ctrlHeight + 10;
	setSize(getWidth(),yPos);
}

EffectOffPanel::~EffectOffPanel()
{
}

void EffectOffPanel::paint(Graphics& g)
{
	g.fillAll(Colour(0xffD0D0D0));
}
