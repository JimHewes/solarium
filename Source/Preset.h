/*
  ================================================================================

  This file is part of Solarium, an editor for the John Bowen Solaris synthesizer.
  Copyright (c) 2016 - Jim Hewes

  Solarium is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Solarium is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Solarium.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PRESET_H_INCLUDED
#define PRESET_H_INCLUDED

#if defined(_DEBUG)
#include <iosfwd>	// forward declaration for ostream
#endif
#include <vector>
#include "Notifier.h"
#include "ParameterList.h"
#include "HistoryList.h"

struct Connector
{
	PortID	startPort;
	PortID	endPort;
};
bool operator == (const Connector &lhs,const Connector &rhs);
bool operator < (const Connector& lhs,const Connector& rhs);	// required in order to put Connector into std:set (e.g. SelectionModel)


struct ParamValue
{
	ParamID paramID;
	int		value;
};
bool operator == (const ParamValue &lhs, const ParamValue &rhs);

enum PresetState { Compare, FXBypass };

struct PresetImpl;
class CommandHistory;

class Preset
{
public:
	Preset(CommandHistory* pCommandHistory);
	~Preset();

	void InitializePreset(Version version);
	std::string GetName() const;
	void SetPreset(const std::vector<ParamValue>& paramValues, Version version);
	std::vector<ParamValue> GetPreset() const;
	int GetNumberOfPorts(ElementID elemID,PortType portType) const;
	static int GetNumberOfParametersInElement(ElementType elemType);
	::Range GetParameterRangeInfo(const ParamID& paramID) const;
	std::vector<std::string> GetParameterListText(const ParamID& paramID);
	std::vector<std::string> GetModDestinationNameList(ElementID elemID, unsigned int modPortIndex);
	
	void SetMuted(const ParamID& paramID, bool isMuted, Version version);
	bool GetMuted(const ParamID& paramID) const;
	std::vector<ParamID> GetAllMuted();
	
	int32_t GetParameterValue(const ParamID& paramID) const;
	void SetParameterValue(const ParamID& paramID, int32_t value, Version version);
	
	Connector CreateConnector(const PortID& startingPortID,const PortID& endingPortID, Version version);
	void RemoveConnector(const Connector& connectorToRemove, Version version);
	std::vector<Connector> GetConnectorList() const;
	bool CanBeConnected(const PortID& sourcePortID,const PortID& targetPortID);
	std::vector<Connector> GetOutputConnectorsOfElement(const ElementID& elemID);
	std::vector<ElementID> GetElementTree();
	std::vector<ElementID> GetElementTree(ElementID elemID);

	void SetState(PresetState stateType, bool enabled);
	bool GetState(PresetState stateType);

	void Write(std::ostream& output);
	void Read(std::istream& input);

	using EnabledChangedEventType = Notifier<void(Version)>;
	using EnabledChangedConnection = EnabledChangedEventType::ConnectionType;
	EnabledChangedEventType::ConnectionType SubscribeToEnabledChangedEvent(EnabledChangedEventType::CallbackType callback);

	using ParameterChangedEventType = Notifier<void(const ParamID& paramID, int value, bool isMuted)>;
	using ParameterChangedConnection = ParameterChangedEventType::ConnectionType;
	ParameterChangedConnection SubscribeToParameterChangedEvent(ParameterChangedEventType::CallbackType callback);

	using MultipleParametersChangedEventType = Notifier<void(Version, bool)>;
	using MultipleParametersChangedConnection = MultipleParametersChangedEventType::ConnectionType;	///< MultipleParametersChangedEvent.Connect() returns this type
	MultipleParametersChangedConnection SubscribeToMultipleParametersChangedEvent(MultipleParametersChangedEventType::CallbackType callback);
	
	using StateChangedEventType = Notifier<void(PresetState stateType, bool enabled)>;
	using StateChangedConnection = StateChangedEventType::ConnectionType;
	StateChangedConnection SubscribeToStateChangedEvent(StateChangedEventType::CallbackType callback);


	bool IsModified();		///< not currently used

#if defined(_DEBUG)
	void Dump(std::ostream& outStr);
#endif

private:
	std::unique_ptr<PresetImpl> m_pImpl;
};


#endif  // PRESET_H_INCLUDED
